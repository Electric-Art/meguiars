﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using WebMarkupMin.AspNet4.Mvc;

namespace Kawa.Controllers
{
    [MinifyHtml]
    public class StoresController : SiteController
    {
        // GET: Stores
        public ActionResult Index(string id, string postcode)
        {
            SiteViewModel vm = new SiteViewModel() { section = "Stores", postCode = postcode };

            loadViewModel(vm);
            return View(vm);
        }

        public JsonResult ShowLocations(int? id, MapModel model)
        {
            runExpire();
            

            List<Store> stores = _storeRep.listAll().Where(s => s.lat <= model.nelat && s.lat >= model.swlat &&
                                                                            s.lng <= model.nelng && s.lng >= model.swlng &&
                                                                            (string.IsNullOrEmpty(model.state) || s.state.Equals(model.state))).ToList();

            /*
             * Make sure “Caruso’s Vitamin World” is at the top of results when you search "2165"
             */
            if(stores.Any(s => s.ID==881))
            {
                var special = stores.Single(s => s.ID == 881);
                stores.Remove(special);
                stores.Insert(0, special);
            }

            var result = new
            {
                data = stores
            };
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}