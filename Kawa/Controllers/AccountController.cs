﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Generic;
using WebMarkupMin.AspNet4.Mvc;
using Newtonsoft.Json;

namespace Kawa.Controllers
{
    [MinifyHtml]
    [Authorize]
    public class AccountController : SiteController
    {
        private AccountService _accountService;
        
        public AccountService AccountService
        {
            get
            {
                if (_accountService == null)
                {
                    _accountService = new AccountService();
                }
                return _accountService;
            }
            protected set
            {
                _accountService = value;
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangeDetailsSuccess,
            ChangePasswordSuccess,
            ChangeEmailSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        public AccountController()
            : base()
        {

        }

         //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);

            return View(new LoginViewModel());
        }
        [AllowAnonymous]
        public ActionResult Signup(string code)
        {
            Response.Cookies.Add(new HttpCookie("reg_channel") { Value = code, Expires = System.DateTime.Now.AddDays(1) });

            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);

            return View("Register", new RegisterViewModel() { fullregister = true, Channel = code });
        }



        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);

            if (!ModelState.IsValid)
            {
                return model.js ? (ActionResult)PartialView("_Login", model) : View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await AccountService.SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    var user = AccountService.UserManager.FindByName(model.Email);
                    if (AccountService.UserManager.IsInRole(user.Id, AdminRole))
                        return RedirectToLocal(returnUrl);
                    
                    var account = _personRep.listAllAccounts().Single(a => a.UserName == model.Email);
                    if (account.isBlocked)
                    {
                        ModelState.AddModelError("", "Sorry your account is blocked please contact an admin.");
                        return model.js ? (ActionResult)PartialView("_Login", model) : View(model);
                    }
                    if (model.ProductID.HasValue || model.PageID.HasValue)
                    {
                        if (!_personRep.listAllFavourites().Any(f => f.AccountID == account.ID &&
                            (
                                (model.ProductID.HasValue && f.ProductID == model.ProductID) ||
                                (model.ProductID.HasValue && f.PageID == model.PageID)
                            )
                            ))
                        {
                            var favourite = new Favourite()
                            {
                                AccountID = account.ID,
                                ProductID = model.ProductID,
                                PageID = model.PageID,
                                createDate = DateTime.Now
                            };
                            _personRep.saveFavourite(favourite);
                        }
                    }

                    MoveAnySessionBasedFavsToAccount(account.ID);

                    return model.js ? (ActionResult)Content("logged") : RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return model.js ? (ActionResult)Content("lockout") : View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    /*PortService portService = new PortService();
                    if (portService.CheckForPortAccount(model))
                    {
                        var reguser = new ApplicationUser { UserName = model.Email, Email = model.Email };
                        var regresult = await AccountService.UserManager.CreateAsync(reguser, model.Password);
                        if (regresult.Succeeded)
                        {
                            await AccountService.SignInManager.SignInAsync(reguser, isPersistent: false, rememberBrowser: false);
                            return model.js ? (ActionResult)Content("logged") : RedirectToLocal(returnUrl);
                        }
                        ModelState.AddModelError("", "There has been a problem updating your account. Please contact support. #Error 1001");
                        return model.js ? (ActionResult)PartialView("_Login", model) : View(model);
                    }
                    */
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return model.js ? (ActionResult)PartialView("_Login",model) : View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await AccountService.SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await AccountService.SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent:  model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

      
        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult LoginOrRegister(int? ProductID, int? PageID)
        {
            AccountViewModel avm = new AccountViewModel()
            {
                Login = new LoginViewModel() { js = true },
                Register = new RegisterViewModel() { js = true, fullregister=true, Channel = findChannel() }
            };

            if (ProductID.HasValue || PageID.HasValue)
            {
                avm.favourite = new Favourite()
                {
                    ProductID = ProductID,
                    PageID = PageID
                };
            }

            return View(avm);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            SiteViewModel vm = new SiteViewModel()
            {
                
            };
            loadViewModel(vm);

            return View(new RegisterViewModel() { fullregister = true, Channel= findChannel() });
        }

        public void MoveAnySessionBasedFavsToAccount(int accountID)
        {
            try
            {
                ///then move any saved favs across if they have any.
                int guestAccountID = _personRep.getGuest().ID;
                string guestSession = OrderService.getFavSession();
                List<Favourite> favs = _personRep.listAllFavourites().Where(f => f.AccountID == guestAccountID && f.Session == guestSession).ToList();

                foreach (Favourite f in favs)
                {
                    var favourite = new Favourite()
                    {
                        AccountID = accountID,
                        ProductID = f.ProductID,
                        PageID = f.PageID,
                        createDate = DateTime.Now
                    };
                    _personRep.saveFavourite(favourite);
                }

                _personRep.ResetSessionFavs(guestSession);
            }
            catch { }

        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);

            if (model.DateOfBirthDay.HasValue || model.DateOfBirthMonth.HasValue || model.DateOfBirthYear.HasValue)
            {
                DateTime date;
                if (DateTime.TryParse(model.DateOfBirthYear + "-" + model.DateOfBirthMonth + "-" + model.DateOfBirthDay, out date))
                    model.DateOfBirth = date;
                else
                    ModelState.AddModelError("DateOfBirth", "Date of Birth is not valid.");
            }

            if (_personRep.listAllAccounts().Any(x => x.UserName.ToLower() == model.Email.ToLower()))
            {
                ModelState.AddModelError("", "An account with this email already exists, please log in or register with a different email address.");
            }

            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await AccountService.UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var account = new Account()
                    {
                        UserName = user.UserName,
                        createDate = DateTime.Now,
                    };

                   
                        account.firstName = model.FirstName;
                        account.lastName = model.LastName;
                        account.channel = model.Channel;
                    if(model.VoucherCode != null)
                    {
                        Promotion voucher = new Promotion() { code = model.VoucherCode };
                        ////Order o = OrderService.getNewDefaultOrder(null, true);
                        voucher = _orderRep.listAllPromotions().Where(x => x.code == voucher.code).FirstOrDefault();
                        Order o = OrderService.getCartOrder(true);
                        if (voucher.ProductID!=null)
                        {
                            Product p = _productRep.listAllProducts(false,true).Where(x => x.ID == voucher.ProductID).FirstOrDefault();
                            ////auto add this product to the users cart
                            Basket b = new Basket();
                            b.VariantID = p.variant.ID;
                            b.Quantity = 1;
                            var resultx = OrderService.addBasket(b, Request.UserHostAddress);

                            o = OrderService.getCartOrder(false);

                        }
                        
                        ///Order order = OrderService.getCartOrder();

                        var r = PromotionService.checkVoucher(voucher, o);

                        ///Response.Cookies.Add(new HttpCookie("reg_code") { Value = model.VoucherCode, Expires = System.DateTime.Now.AddDays(1) });
                    }

                    if (model.fullregister)
                    {
                        account.gender = model.Gender;                        
                        if (model.DateOfBirth.HasValue)
                            account.dateOfBirth = model.DateOfBirth;
                    }

                    _personRep.saveAccount(account);

                    if (model.ProductID.HasValue || model.PageID.HasValue)
                    {
                        if (!_personRep.listAllFavourites().Any(f => f.AccountID == account.ID &&
                            (
                                (model.ProductID.HasValue && f.ProductID == model.ProductID) ||
                                (model.ProductID.HasValue && f.PageID == model.PageID)
                            )
                            ))
                        {
                            var favourite = new Favourite()
                            {
                                AccountID = account.ID,
                                ProductID = model.ProductID,
                                PageID = model.PageID,
                                createDate = DateTime.Now
                            };
                            _personRep.saveFavourite(favourite);
                        }
                    }

                    MoveAnySessionBasedFavsToAccount(account.ID);

                    string[] newcustomer = new string[] { "customercode=" + OrderService.getCustomerCode((int)account.ID), "FirstName=" + model.FirstName, "LastName=" + model.LastName, "Phone=00-000-000", "recipientcode=NA", "webpassword=" + model.Password, "email=" + model.Email };
                    var propellaRegistrationResult = PropellaService.GetGPIWebServiceData(new PropellaService.WebServiceDataRequest() { Args = newcustomer, Function = "Create_Customer" });
                    if(model.isMailer)
                    {
                        await MailChimpService.AddEmailToListAsync(model.Email, model.FirstName, model.LastName);
                    }
                    await AccountService.SignInManager.SignInAsync(user, isPersistent:false, rememberBrowser:false);
                  

                    return model.js ? (ActionResult)Content("registered") : RedirectToAction("Index", "Account");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return model.js ? (ActionResult)PartialView("_Register" + (model.fullregister ? "FullRegister" : ""), model) : View(model);
            //return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await AccountService.UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);

            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);

            if (ModelState.IsValid)
            {
                var user = await AccountService.UserManager.FindByNameAsync(model.Email);

               

                if (user == null /* || !(await AccountService.UserManager.IsEmailConfirmedAsync(user.Id)) */)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await AccountService.UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                //await AccountService.UserManager.SendEmailAsync(user.Id, "Reset Password", "");

                MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], model.Email)
                {
                    Subject = "Reset your password at meguiars.co.nz",
                    IsBodyHtml = true
                };
                //msg.Bcc.Add(ConfigurationManager.AppSettings["ShopBCC"]);
                msg.Body = string.Format("<html><body>{0}</body>","Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                using (var client = new SmtpClient())
                {
                    client.Send(msg);
                }

                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await AccountService.UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await AccountService.UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            SiteViewModel vm = new SiteViewModel();
            loadViewModel(vm);
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await AccountService.SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await AccountService.UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await AccountService.SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await AccountService.SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await AccountService.UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await AccountService.UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await AccountService.SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            OrderService.createSession();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "ChangePassword" };
            loadViewModel(vm);

            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "ChangePassword" };
            loadViewModel(vm);

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await AccountService.UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await AccountService.UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await AccountService.SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        //
        // GET: /Manage/ChangeEmail
        public ActionResult ChangeEmail()
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "ChangeEmail", account=getAccount() };
            loadViewModel(vm);

            return View();
        }

        //
        // POST: /Manage/ChangeEmail
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeEmail(ChangeEmailViewModel model)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "ChangeEmail", account = getAccount() };
            loadViewModel(vm);

            if (_personRep.listAllAccounts().Any(x => x.UserName.ToLower() == model.NewEmailAddress.ToLower()))
            {
                ModelState.AddModelError("", "Sorry, an account with this email already exists.");
            }

            if (ModelState.IsValid)
            {
                var oldid = User.Identity.GetUserId();
               // var resulttestpass = await AccountService.SignInManager.PasswordSignInAsync(getAccount().UserName, model.Password, false, shouldLockout: false);
                var passuser = await AccountService.UserManager.FindAsync(User.Identity.Name, model.Password);
                if (passuser != null)
                { 
                    var user = new ApplicationUser { UserName = model.NewEmailAddress, Email = model.NewEmailAddress };
                    var result = await AccountService.UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        await AccountService.deleteAccountAsync(oldid);

                        var account = getAccount();
                        account.UserName = model.NewEmailAddress;
                        _personRep.saveAccount(account);

                        await AccountService.SignInManager.PasswordSignInAsync(account.UserName, model.Password, true, shouldLockout: false);
                        return RedirectToAction("Index", new { Message = ManageMessageId.ChangeEmailSuccess });
                    }
                    AddErrors(result);
                }
                else
                {
                    ModelState.AddModelError("", "Sorry your password was incorrect.");
                }
            }
                
            return View(model);
        }

        #region Special

        //
        // POST: /Account/SpecialRegister
        [AllowAnonymous]
        [HttpPost]
        public ActionResult SpecialRegister(SpecialViewModel model)
        {
            if (ModelState.IsValid)
            {
                var signupModule = _contentRep.listAllSlots(true, false).Where(s => s.Spot == Slot.SpotOption.SITE_WIDE_SIGNUP).OrderBy(s => s.orderNo).FirstOrDefault();

                if (signupModule == null)
                    return Json(new { success = false, errors = "Sorry, signing up to the newsletter is not possible at this time." });

                if (signupModule.page.SystemPage != Models.Page.SystemPageOption.SPECIAL_MAILCHIMP_REGISTER_FOR_VOUCHER)
                {
                    if(MailChimpService.AddEmailToList(model.Email).success)
                        return Json(new { success = true, message = "Thank you for signing up to the e-newsletter." });
                }
                else
                {
                    if (PromotionService.specialPromotionCanInitiate(model.Email) && PromotionService.specialPromotionInitiate(model.Email))
                    {
                        return Json(new { success = true, message = "Thank you, your voucher has been sent." });
                    }
                }
                return Json(new { success = false, errors = "Sorry, you are already signed up to our e-newsletter." });
            }
            return Json(new { success = false, model = model, errors = string.Join(" ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage)) });
        }

        //
        // POST: /Account/SpecialResend
        [AllowAnonymous]
        [HttpPost]
        public ActionResult SpecialResend(string id)
        {
            if (!PromotionService.specialPromotionResend(id))
            {
                return Json(new { success = true, message = "Sorry, you are already signed up to our e-newsletter." });
            }
            return Json(new { success = false, message = "Your validation email has been re-sent." });
        }

        //
        // POST: /Account/SpecialResend
        [AllowAnonymous]
        [HttpPost]
        public ActionResult SpecialClosed(string id)
        {
            PromotionService.specialSupress();
            return Json(new { success = true });
        }

        #endregion

        //
        // GET: /Account/DeleteAccount
        public ActionResult DeleteAccount()
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "DeleteMyAccount", account = getAccount() };
            loadViewModel(vm);

            return View();
        }

        //
        // POST: /Account/DeleteAccount
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAccount(DeleteAccountViewModel model)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "DeleteMyAccount", account = getAccount() };
            loadViewModel(vm);

            if (ModelState.IsValid)
            {
                var oldid = User.Identity.GetUserId();
                // var resulttestpass = await AccountService.SignInManager.PasswordSignInAsync(getAccount().UserName, model.Password, false, shouldLockout: false);
                var passuser = await AccountService.UserManager.FindAsync(User.Identity.Name, model.Password);
                if (passuser != null)
                {
                    await AccountService.deleteAccountAsync(oldid);

                    _personRep.deleteAccount(getAccount());

                    AuthenticationManager.SignOut();

                    return Redirect("/");
                }
                else
                {
                    ModelState.AddModelError("", "Sorry your password was incorrect.");
                }
            }

            return View(model);
        }

        #region Account Area

        public ActionResult Index(int? id, ManageMessageId? message)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "Home" };
            loadViewModel(vm);

            ViewBag.StatusMessage =
                message == ManageMessageId.ChangeDetailsSuccess ? "Your details have been changed."
               : message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
               : message == ManageMessageId.ChangeEmailSuccess ? "Your email has been changed."
               : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
               : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
               : message == ManageMessageId.Error ? "An error has occurred."
               : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
               : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
               : "";

            return View();
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "Details" };
            loadViewModel(vm);

           
            vm.order = _orderRep.listAllOrdersWithTrans().Where(x => x.AccountID == getAccount().ID && 
                                                                x.SystemStatus >= Models.Order.SystemStatusOption.IN_PROGRESS).OrderByDescending(x => x.ID).FirstOrDefault();
            if (vm.order != null)
                OrderService.loadOrderWithFullDomain(vm.order);

            ViewBag.AllowedCountries = ShippingService.getAllowedCountries();
            ViewBag.AllowedStates = ShippingService.getAllowedStates();

            vm.FullRegisterUpdate = new FullRegisterUpdateViewModel()
            {
                FirstName = getAccount().firstName,
                LastName = getAccount().lastName,
                Gender = getAccount().gender,
                Channel = getAccount().channel
            };

            if (getAccount().dateOfBirth.HasValue)
            {
                DateTime d = (DateTime)getAccount().dateOfBirth;
                vm.FullRegisterUpdate.DateOfBirthDay = d.Day;
                vm.FullRegisterUpdate.DateOfBirthMonth = d.Month;
                vm.FullRegisterUpdate.DateOfBirthYear = d.Year;
            }

            if(!string.IsNullOrEmpty(getAccount().creditCardToken))
            {
                vm.savedCard = PaymentServiceStore.getService(PaymentServiceStore.PaymentTypeOption.EWAY).getSavedCreditCard(getAccount().creditCardToken);
            }


            return View(vm);
        }

        [HttpPost]
        public ActionResult Details(int? id, CustomerDetailsViewModel model)
        {
            runExpire();
            SiteViewModel vm = new SiteViewModel() { subSection = "Details" };
            loadViewModel(vm);
            
            /*
            var dbversion = _orderRep.listAllAccountDetails().SingleOrDefault(a => a.ID==model.CustomerDetails.ID);
            if (!_orderRep.listAllOrders().Any(o => o.ID==dbversion.OrderID && o.AccountID==getAccount().ID))
                return RedirectToAction("Index", new { Message = ManageMessageId.Error });
             */ 

            if (model.FullRegisterUpdate.DateOfBirthDay.HasValue || model.FullRegisterUpdate.DateOfBirthMonth.HasValue || model.FullRegisterUpdate.DateOfBirthYear.HasValue)
            {
                DateTime date;
                if (DateTime.TryParse(model.FullRegisterUpdate.DateOfBirthYear + "-" + model.FullRegisterUpdate.DateOfBirthMonth + "-" + model.FullRegisterUpdate.DateOfBirthDay, out date))
                    model.FullRegisterUpdate.DateOfBirth = date;
                else
                    ModelState.AddModelError("FullRegisterUpdate.DateOfBirth", "Date of Birth is not valid.");
            }

            if (ModelState.IsValid)
            {
                //_orderRep.saveAccountDetail(model.CustomerDetails);

                var account = getAccount();
                account.firstName = model.FullRegisterUpdate.FirstName;
                account.lastName = model.FullRegisterUpdate.LastName;
                account.gender = model.FullRegisterUpdate.Gender;
                account.channel = model.FullRegisterUpdate.Channel;
                if (model.FullRegisterUpdate.DateOfBirth.HasValue)
                    account.dateOfBirth = model.FullRegisterUpdate.DateOfBirth;

                _personRep.saveAccount(account);

                return RedirectToAction("Index", new { Message = ManageMessageId.ChangeDetailsSuccess });
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult DeleteCard(int? id)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "Details" };
            loadViewModel(vm);

            var account = getAccount();
            account.creditCardToken = null;
            _personRep.saveAccount(account);

            return RedirectToAction("Details");
        }
        
        
        
        public ActionResult Orders(int? id)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "Orders" };
            loadViewModel(vm);
            vm.orders = _orderRep.listAllOrdersWithTrans().Where(o => 
                o.AccountID == getAccount().ID && 
                o.SystemStatus != Models.Order.SystemStatusOption.IN_PROGRESS &&
                o.OrderDate > new DateTime(2015,08,20,17,53,1).LocalToUtc() //20/08/15 5:52
                ).OrderByDescending(o => o.ID).ToList();

            vm.orders.ForEach(x => OrderService.loadOrderWithFullDomain(x));

            return View(vm);
        }

        public ActionResult Order(Guid? id)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "Orders", isOrder = true };
            loadViewModel(vm);

            List<int> _quantities = new List<int>();
            for (int i = 1; i <= 100; i++) _quantities.Add(i);
            ViewBag.Quantities = _quantities;

            vm.order = OrderService.getFullOrder((Guid)id);

            vm.order.orderItems.Select(x => x.Product).ToList().ForEach(x => x.basket = new Basket() { Quantity = 1, VariantID = x.variant.ID });

            return View(vm.order);
        }

        public ActionResult Favourites(int? id)
        {
            return RedirectToAction("WishList");
        }
        public ActionResult WishList(int? id)
        {
            SiteViewModel vm = new SiteViewModel() { subSection = "Favourites" };
            loadViewModel(vm);
            vm.account = getAccount();
            vm.account.favourites = _personRep.listAllFavourites().Where(f => f.AccountID == getAccount().ID).ToList();
            foreach(Favourite f in vm.account.favourites)
            {
                f.product.basket = new Basket() { Quantity = 1, VariantID = f.product.variant.ID };
            }

            return View(vm);
        }
        public ActionResult FavouriteDelete(int? id)
        {
            var fav = _personRep.listAllFavourites().Single(f => f.ID==id);
            if (fav.AccountID != getAccount().ID)
                throw new Exception("Not authorised");

            _personRep.deleteFavourite(fav);

            return RedirectToAction("Favourites");
        }


        #endregion

        

        protected override void loadViewModel(SiteViewModel viewModel)
        {
            if(getAccount() != null)
            {
                viewModel.hasOrders = _orderRep.listAllOrders().Any(o => o.AccountID == getAccount().ID && o.SystemStatus != Models.Order.SystemStatusOption.IN_PROGRESS);
            }

            viewModel.section = "My Account";
            base.loadViewModel(viewModel);
        }

        #region Util

        [Authorize(Roles = AdminRole)]
        public async Task<ActionResult> DeleteTestAccount(string accountname)
        {
            
            // var resulttestpass = await AccountService.SignInManager.PasswordSignInAsync(getAccount().UserName, model.Password, false, shouldLockout: false);
            var user = AccountService.UserManager.FindByName(accountname);
            if (user != null)
            {
                await AccountService.deleteAccountAsync(user.Id);

                return Content("done");
            }
              

            return Content("issue");
        }

        [Authorize(Roles = AdminRole)]
        public ActionResult SecuritySeedTestAccount(string email, string pass)
        {
            var user = AccountService.UserManager.FindByName(email);
            if (user == null)
            {
                user = new ApplicationUser { UserName = email, Email = email };
                var result = AccountService.UserManager.Create(user, pass);
                return Content("Done");
            }
            else
            {
                return Content("Already done");
            }
        }

        /*
        [AllowAnonymous]
        public ActionResult SecuritySeed()
        {
            const string name = "admin@kawanaturalhealth.com.au";
            const string password = "testtest";
            //const string adminRoleName = "Admin";
            var roles = new string[] { AdminRole };

            //Create Role Admin if it does not exist
            foreach (var roletag in roles)
            {
                var role = RoleManager.FindByName(roletag);
                if (role == null)
                {
                    role = new IdentityRole(roletag);
                    var roleresult = RoleManager.Create(role);
                }
            }
            var user = UserManager.FindByName(name);
            if (user == null)
            {
                user = new ApplicationUser { UserName = name, Email = name };
                var result = UserManager.Create(user, password);
                result = UserManager.SetLockoutEnabled(user.Id, false);
            }

            // Add user admin to Role Admin if not already added
            var rolesForUser = UserManager.GetRoles(user.Id);
            var adminRole = RoleManager.FindByName(AdminRole);
            if (!rolesForUser.Contains(adminRole.Name))
            {
                var result = UserManager.AddToRole(user.Id, adminRole.Name);
            }

            return Content("Done");
        }
        */
        #endregion

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (!error.StartsWith("Name"))
                    ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_accountService != null)
                {
                    _accountService.Dispose(disposing);
                    _accountService = null;
                }
            }

            base.Dispose(disposing);
        }
        
        #endregion
    }
}