﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using System.Net.Mail;
using System.Configuration;
using PagedList;
using WebMarkupMin.AspNet4.Mvc;
using System.Text.RegularExpressions;
using Kawa.PropellaWebService;
using System.Net;
using System.Reflection;
using Newtonsoft.Json;

namespace Kawa.Controllers
{
    [RequiresSSL]
    [RequireSiteDomain]
    [CompressContent]
    public class SiteController : Controller
    {
        protected static string sep1 = "<br/>";
        protected static string sep2 = "<br/><br/>";
        public const string AdminRole = "Admin";
        public static int PAGE_SIZE = 16;

        //
        // GET: /Site/
        protected ContentRepository _contentRep;
        protected SiteResourceRepository _srRep;
        protected ProductRepository _productRep;
        protected StoreRepository _storeRep;
        protected PersonRepository _personRep;
        protected OrderRepository _orderRep;
        protected ShippingRepository _shipRep;
        protected CacheRepository _cacheRep;

        protected string sitemapdomain = System.Configuration.ConfigurationManager.AppSettings["SiteUrl"];///"https://www.meguiars.co.nz";
        protected Account account;

        protected ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                _roleManager = _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
                return _roleManager;
            }
            protected set
            {
                _roleManager = value;
            }
        }

        protected OrderService _orderService;
        public OrderService OrderService
        {
            get
            {
                _orderService = _orderService ?? new OrderService(_productRep, _orderRep, _contentRep, _srRep, this, PromotionService, ShippingService, TagService);
                return _orderService;
            }
            protected set
            {
                _orderService = value;
            }
        }

        public string findChannel()
        {
            if (Request.Cookies["reg_channel"] != null)
            {
                return Request.Cookies["reg_channel"].Value;
            }
            else
            {
                return "";
            }
        }
        public static string StripTagsHtml(string html)
        {
            if (string.IsNullOrEmpty(html)) return "";
            var txt = Regex.Replace(html, "<.*?>", string.Empty);
            if (html.Length > 300)
                return txt.Substring(0, 255) + "...";
            return txt;
        }
        protected ShippingService _shippingService;
        public ShippingService ShippingService
        {
            get
            {
                _shippingService = _shippingService ?? new ShippingService(_productRep, _shipRep, _orderRep);
                return _shippingService;
            }
            protected set
            {
                _shippingService = value;
            }
        }

        protected PromotionService _promotionService;
        public PromotionService PromotionService
        {
            get
            {
                _promotionService = _promotionService ?? new PromotionService(_productRep, _orderRep, _contentRep, _shipRep, ShippingService);
                return _promotionService;
            }
            protected set
            {
                _promotionService = value;
            }
        }

        protected PaymentServiceStore _paymentServiceStore;
        public PaymentServiceStore PaymentServiceStore
        {
            get
            {
                _paymentServiceStore = _paymentServiceStore ?? new PaymentServiceStore(_orderRep, _shipRep);
                return _paymentServiceStore;
            }
            protected set
            {
                _paymentServiceStore = value;
            }
        }

        protected TagService _tagService;
        public TagService TagService
        {
            get
            {
                _tagService = _tagService ?? new TagService(_contentRep,_productRep,_cacheRep);
                return _tagService;
            }
            protected set
            {
                _tagService = value;
            }
        }

        private SearchaniseService _searchaniseService;
        public SearchaniseService SearchaniseService
        {
            get
            {
                if (_searchaniseService == null)
                {
                    _searchaniseService = new SearchaniseService(_productRep, _contentRep);
                }

                return _searchaniseService;
            }
            private set
            {
                _searchaniseService = value;
            }
        }

        public SiteController()
        {
            _contentRep = new ContentRepository();
            _srRep = new SiteResourceRepository();
            _productRep = new ProductRepository();
            _storeRep = new StoreRepository();
            _personRep = new PersonRepository();
            _orderRep = new OrderRepository();
            _shipRep = new ShippingRepository();
            _cacheRep = new CacheRepository();
        }

        public static bool IsConfigEnabled(string name)
        {
            try
            {
                return bool.Parse(System.Configuration.ConfigurationManager.AppSettings[name].ToString());
            }
            catch
            {
                return false;
            }
        }
        protected virtual void loadViewModel(SiteViewModel viewModel)
        {
            var tagset = TagService.GetForMenu();

            viewModel.isDevice = Request.Browser.IsMobileDevice;
            viewModel.isTablet = isTablet();
            viewModel.footerPages = tagset.FooterPages;
            ViewBag.model = viewModel;
            
            var signupModule = tagset.SignupModule;
            var showSpecial = signupModule != null;
            var specialPopupSuppressed = !(showSpecial && !PromotionService.specialHasSupress());
            ViewBag.signupModule = signupModule;
            ViewBag.showSpecial = showSpecial;
            ViewBag.specialPopupSuppressed = specialPopupSuppressed;
            ViewBag.showSpecialInFooter = true;

            ViewBag.sideWideSlot = tagset.SiteWideSlot;
            ViewBag.AdhocPopupTags = tagset.AdhocPopupTags;
            ViewBag.MenuModel = new MenuViewModel()
            {
                OrderSummary = OrderService.getSummary(),
                MainProductTags = tagset.MainCategories,
                ProductCategoriesTag = tagset.CategorySuperTag,
                ShippingBanner1 = tagset.ShippingBanner1,
                ShippingBanner2 = tagset.ShippingBanner2,
                TrendingSearches = tagset.TrendingSearches,
                Brands = tagset.Brands,
                Categories = tagset.CategorySet,
                Catalogs = tagset.Catalogs
            };
        }
        

        public Account getAccount()
        {
            if (!Request.IsAuthenticated) return null;

            if (account == null)
            {
                account = _personRep.listAllAccounts().Where(a => a.UserName == User.Identity.Name).SingleOrDefault();
            }
            return account;
        }

        public Product GetDonationProduct()
        {
            Product pr = _productRep.listAllProducts(true, true).Where(p => p.sku == "PCNZ DONATION").FirstOrDefault();///inject the donation product into the siteviewmodel as required for basket display
            if (pr == null)
                pr = new Product() { isHidden = true, name = "", sku = "" };

            return pr;
        }
        public static string MaskPostCode(int? code)
        {
            string maskedpostcode = code.ToString();

            if (code.HasValue)
            { 
                if (code < 999)
                {
                    maskedpostcode = "0" + code.ToString();
                }
            }
            return maskedpostcode;
        }

        public SiteContentCollection getSiteContents()
        {
            List<Tag> tags = _contentRep.listAllTags().Where(t => !t.isHidden).ToList();
            var pages = _contentRep.listAllPages().OrderBy(p => p.ID).Where(p => !p.isHidden).ToList();
            foreach (var page in pages)
            {
                page.tags = _contentRep.listAllTagsForPage(page).Where(p => !p.isHidden).ToList();
            }
            List<Tag> mainpages = new List<Tag>();
           
            var aboutTag = new Tag() { name = "Meguiars Home", plugurl = "/"};
            aboutTag.pages = pages.Where(p => (
                p.SystemPage == Page.SystemPageOption.ABOUT_US ||
                p.SystemPage == Page.SystemPageOption.TERMS_AND_CONDITIONS ||
                p.SystemPage == Page.SystemPageOption.PRIVACY ||
                p.SystemPage == Page.SystemPageOption.CONTACT_US ||
                p.SystemPage == Page.SystemPageOption.SHIPPING ||
                p.SystemPage == Page.SystemPageOption.RETURNS ||
                p.SystemPage == Page.SystemPageOption.GIFT_VOUCHERS
                )).ToList();

            mainpages.Add(aboutTag);
            mainpages.Add(new Tag() { name = "Our Brands", plugurl = "/category/brands?mode=brands"});
            mainpages.Add(new Tag() { name = "Register", plugurl = "/Account/Register"});
            mainpages.Add(new Tag() { name = "Login", plugurl = "/Account/Login"});

            var productTags = tags.Where(t => 
                (t.Type == Tag.TypeOption.PRODUCT_SECTION ||
                t.Type == Tag.TypeOption.BRAND_SECTION ||
                t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE ||
                t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL
                ) && !t.isHidden).OrderBy(t => t.name).ToList();
            foreach (var tag in productTags)
            {
                tag.pages = pages.Where(p => p.tags.Any(t => t.ID == tag.ID)).OrderByDescending(p => p.ID).ToList();
                mainpages.Add(tag);
            }
            
            var products = _productRep.listAllProducts().Where(p => !p.isHidden && p.HasVariant).ToList();

            return new SiteContentCollection()
            {
                products = products,
                tags = mainpages
            };
        }

        protected bool hasIEProb()
        {
            if (Request.Browser.Type.ToUpper().Contains("IE"))
            {
                if (Request.Browser.MajorVersion  <= 8)
                { 
                    return true;
                }
                
            }
            return false;
        }

        protected void runExpire()
        {
            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddYears(-1);
            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            ViewBag.site = null;
        }

        protected void addBasketSetup(SiteViewModel vm)
        {
            if (vm.footerPages == null)
                vm.footerPages = _contentRep.listAllPages().Where(p => !p.isHidden && (p.SystemPage == Page.SystemPageOption.TERMS_AND_CONDITIONS )).ToList();

            ViewBag.model = vm;

            ViewBag.DonationProduct = GetDonationProduct();

        }

        protected void setupProductList(SearchResultsViewModel vm, List<Product> products, int? page, int? pageSize, PostedResultFilters postedResultFilters)
        {
            var pageNumber = page ?? 1;
            var pageSizeNumber = pageSize ?? PAGE_SIZE;
            var specialties = products.SelectMany(x => x.markertags).DistinctBy(x => x.ID).ToList();
            var brands = products.SelectMany(x => x.brandtags).DistinctBy(x => x.ID).ToList();

            vm.filter = new ResultFilterViewModel()
            {
                brands = brands.Where(y => y.Type == Tag.TypeOption.BRAND_SECTION).ToList(),
                specialties = specialties.Where(y => y.Type == Tag.TypeOption.ADHOC_PRODUCT_TAGS).ToList()
            };

            if (postedResultFilters.PriceIDs != null)
            {
                vm.filter.selectedPrices = vm.filter.prices.Where(x => postedResultFilters.PriceIDs.Contains(x.ID)).ToList();
                products = products.Where(x =>
                    (postedResultFilters.PriceIDs.Contains(1) && x.variant.price <= 25)
                    ||
                    (postedResultFilters.PriceIDs.Contains(2) && x.variant.price > 25 && x.variant.price <= 50)
                    ||
                    (postedResultFilters.PriceIDs.Contains(3) && x.variant.price > 50 && x.variant.price <= 100)
                    ||
                    (postedResultFilters.PriceIDs.Contains(4) && x.variant.price > 100)
                    ).ToList();
            }
            if (postedResultFilters.BrandIDs != null)
            {
                vm.filter.selectedBrands = vm.filter.brands.Where(x => postedResultFilters.BrandIDs.Contains(x.ID)).ToList();
                products = products.Where(x => x.brandtags.Any(y => postedResultFilters.BrandIDs.Contains(y.ID))).ToList();
            }
            if (postedResultFilters.SpecialtiesIDs != null)
            {
                vm.filter.selectedSpecialties = vm.filter.specialties.Where(x => postedResultFilters.SpecialtiesIDs.Contains(x.ID)).ToList();
                products = products.Where(x => x.markertags.Any(y => postedResultFilters.SpecialtiesIDs.Contains(y.ID))).ToList();
            }
            vm.filter.postedResultFilters = postedResultFilters;

            vm.Products = products.ToPagedList(pageNumber, pageSizeNumber);
            vm.Products.ToList().ForEach(x =>
            {
                SiteResourceService.loadUrls(x.siteResources.Where(p => p.SiteResourceType == SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE).ToArray());
                x.basket = new Basket() { Quantity = 1, VariantID = x.variant.ID };
            });

            List<int> _quantities = new List<int>();
            for (int i = 1; i <= 100; i++) _quantities.Add(i);
            ViewBag.Quantities = _quantities;
        }

        #region Email

        public MailMessage sendViewEmail(string fromemail, string toemail, string bcc, string subject, string header, string view, object model, string message, Signature signature, bool actualsend=true)
        {
            MailMessage msg = new MailMessage(fromemail, toemail)
            {
                Subject = subject,
                IsBodyHtml = true
            };
            if (bcc != null)
                msg.Bcc.Add(bcc);
            msg.Body = "<html><body>" + (!string.IsNullOrEmpty(message) ? message + "<br/><br/>" : "") + 
                        header + 
                        (signature != null ? "<br/><br/>" + signature.Html : "") + 
                        "<br/><br/><br/>" + RenderPartialViewToString(this, view, model) + "</body>";


            try
            {
                if (actualsend)
                {
                    using (var client = new SmtpClient())
                    {
                        client.Send(msg);
                    }
                }
            }
            catch (Exception e) {
                string error = string.Format("Email Send Error{0}Subject - {1}{0}To - {2}{0}{3}{0}{4}", Environment.NewLine, subject, toemail, e.Message, e.StackTrace);
                LogService.WriteError(error);
            }
            return msg;
        }

        public static string RenderPartialViewToString(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.ToString();
            }
        }

        #endregion

        #region client

        public bool IsMobileDevice()
        {
            return Request.Browser.IsMobileDevice && !isTablet();
        }

        public bool isTablet()
        {
            return Request.UserAgent.Contains("iPad") ||
                (Request.UserAgent.Contains("Android") && !Request.UserAgent.Contains("Mobile")) ||
                (Request.UserAgent.Contains("FROYO") || Request.UserAgent.Contains("Xoom") || Request.UserAgent.Contains("KFAPWI"));

        }

        public string getView(string mobsite,string action)
        {
            return (IsMobileDevice() ? ("~/Views/" + mobsite + "/" + action + ".cshtml") : action);
        }

        public static string getDomainBase()
        {
            return SiteService.getDomainBase();
        }

        #endregion
    }

    
}
