﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Reports;
using Kawa.Models.Extensions;

namespace Kawa.Controllers
{
    public class ReportsController : AdminSiteController
    {
        // GET: Reports
        public ActionResult Index()
        {
            runExpire();

            var reports = getReportsList().OrderBy(x => x.Name);

            return View(reports);
        }

        [HttpGet]
        public ActionResult Run(string id)
        {
            runExpire();

            var report = getReportsList().Single(x => x.UrlName == id);

            return View(report);
        }

        [HttpPost]
        public ActionResult Run(string id, ReportViewModel model)
        {
            runExpire();

            var report = getReportsList().Single(x => x.UrlName == id);

            report.Startdate = model.Startdate.HasValue ? ((DateTime)model.Startdate).LocalToUtc() : (DateTime?)null;
            report.Enddate = model.Enddate.HasValue ? ((DateTime)model.Enddate).AddDays(1).LocalToUtc() : (DateTime?)null;
            report.MaxRows = model.MaxRows;
            report.excludeHidden = model.excludeHidden;
            report.DownloadCsv = model.DownloadCsv;
            report.Filters.ForEach(x => x.SelectedID = model.Filters.Single(f => f.Name == x.Name).SelectedID);

            if(ModelState.IsValid)
            {
                if(model.DownloadCsv)
                {
                    var result = report.RunReportForCsv();
                    return File(result.Data, "application/csv", result.Name);
                }
                else
                {
                    ViewBag.report = report;
                    return View("Report", report.RunReportForView());
                }
            }
            return View(model);
        }

        private List<IReport> getReportsList()
        {
            return new List<IReport>(){
                new TopProductReport(),
                new TopCategoryReport(),
                new TopBrandReport(),
                new TopCustomersReport(),
                new TopBrandProductsReport(_contentRep),
                new ProductReport(_productRep),
                new TotalSalesReport(),
                new TotalSalesDailyReport(),
                new TotalSalesDailyReportAccountant(),
                new BrandReport(_contentRep),
                new CategoryReport(_contentRep),
                new TopCustomersByFilter(_contentRep,_productRep,_shipRep)
            };
        }
    }
}