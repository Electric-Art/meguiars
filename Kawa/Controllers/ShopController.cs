﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;
using AutoMapper;
using System.Net.Mail;
using System.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using System.Threading.Tasks;
using WebMarkupMin.AspNet4.Mvc;
using PaymentExpress.PxPay;
using Newtonsoft.Json;

namespace Kawa.Controllers
{
    [MinifyHtml]
    public class ShopController : SiteController
    {
        // GET: Shop
        [HttpGet]
        public ActionResult Index(bool? clear, string log)
        {
            runExpire();

            SiteViewModel vm = new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.CART };
            loadViewModel(vm);
            vm.order = OrderService.getCartOrder(true);
            vm.order.isBasket = true;
            ViewBag.DonationProduct = GetDonationProduct();
            if (clear.HasValue && (bool)clear)
            {
                OrderService.clearBasketItems(vm.order);
                vm.order = OrderService.getCartOrder(true);
                vm.order.isBasket = true;
            }
            

            return View(vm);
        }

        public int BasketCount()
        {
           ///return 21;
           
            SiteViewModel vm = new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.CART };
           
            vm.order = OrderService.getCartOrder(true);
            return vm.order.orderItems.Sum(x => x.Quantity);
            /**/
        }

        [HttpPost]
        public ActionResult Index(int? id, ShippingInputs inputs)
        {
            runExpire();

            var order = OrderService.getCartOrder(true);
            if (order.orderItems.Count == 0)
                return Redirect("Index");

            var AusID = _shipRep.listAllCountries().Single(x => x.Name == "Australia").ID;
            if(order.CountryID == AusID && !order.StateID.HasValue)
            { 
                SiteViewModel vm = new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.CART };
                loadViewModel(vm);
                vm.order = order;
                vm.order.isBasket = true;
                ModelState.AddModelError("StateID", "You must choose a state");
                return View(vm);
            }

            order = OrderService.finaliseOrder(order);
            
            return RedirectToAction("Related", new { id = order.Code });
            /* */
            if (getAccount() != null)
               return RedirectToAction("AccountDetails", new { id = order.Code });
           else
               return RedirectToAction("Account", new { id = order.Code });
           
        }

        public ActionResult Related(Guid? id)
        {
            runExpire();

            SiteViewModel vm = new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.CART };
            loadViewModel(vm);
            vm.order = OrderService.getFullOrder((Guid)id);
            var badresult = checkOrder(vm.order);
            if (badresult != null) return badresult;

            vm.order.isBasket = true;
            vm.products = _productRep.getRelatedProducts(vm.order.orderItems).ToList();
            
            vm.products.ToList().ForEach(x =>
            {
                x.basket = new Basket() { Quantity = 1, VariantID = x.variant.ID };
            });

            ViewBag.GoTo = "/Shop/Account/" + vm.order.Code;


            if (getAccount() != null)
                ViewBag.GoTo = "/Shop/AccountDetails/" + vm.order.Code;

            if (vm.products.Count == 0)
            {
                return Redirect(ViewBag.GoTo);
            }


            return View(vm);
        }

        public ActionResult LastProductRedirect(int? id)
        {
            runExpire();

            var order = OrderService.getCartOrder(true);

            if (!order.orderItems.Any())
                return Redirect("/");
            else
                return Redirect(order.orderItems.OrderByDescending(x => x.CreateDate).First().Product.getUrl());
        }

        public ActionResult AddToBasket(Basket basket, bool? partial)
        {
            runExpire();
            if (basket.Quantity == 0)
                basket.Quantity = 1;


            var result = OrderService.addBasket(basket, Request.UserHostAddress);

            //If out of stock
            if (result == null)
                return null;

            ViewBag.OrderSummary = OrderService.getSummary();

            return partial.HasValue && (bool)partial ? (ActionResult)PartialView("_BasketResult", result) : (ActionResult)RedirectToAction("Index");
        }

        public ActionResult ChangeBasket(Basket basket)
        {
            runExpire();
            var changebasket = OrderService.updateBasket(basket,false);

            SiteViewModel vm = new SiteViewModel() { };
            addBasketSetup(vm);
            var order = OrderService.getCartOrder();

            if (changebasket != null)
                order.orderItems.Where(x => x.ID == changebasket.ID).ToList().ForEach(x => x.Message = changebasket.Message);

            order.isBasket = true;
            return PartialView("_Basket", order);
        }

        public ActionResult ChangeBasketShipping(ShippingInputs inputs)
        {
            runExpire();
            SiteViewModel vm = new SiteViewModel() { };
            addBasketSetup(vm);

            //For admin only
            inputs.AAFT = null;

            var order = OrderService.getCartOrderWithNewShippingInputs(inputs);
            order.isBasket = true;

            ModelState.Clear();
            return PartialView("_Basket", order);
        }

        [HttpGet]
        public ActionResult Account(Guid? id)
        {
            runExpire();

            if (getAccount() != null)
                return RedirectToAction("AccountDetails", new { id = id });

            SiteViewModel vm = new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.ACCOUNT_DETAILS, ordercode = (Guid)id };

            AccountViewModel avm = new AccountViewModel()
            {
                Login = new LoginViewModel() { js=true },
                Register = new RegisterViewModel() { js = true, Channel = findChannel() },
                OrderCode = (Guid)id
            };

            loadViewModel(vm);

            vm.order = OrderService.getFullOrder((Guid)id);
            var badresult = checkOrder(vm.order);
            if (badresult != null) return badresult;

            return View(avm);
        }

        [HttpGet]
        public ActionResult AccountDetails(Guid? id)
        {
            runExpire();
            SiteViewModel vm = new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.ACCOUNT_DETAILS, ordercode=(Guid)id };
            
            vm.order = OrderService.getFullOrder((Guid)id);
            var badresult = checkOrder(vm.order);
            if (badresult != null) return badresult;
            
            ViewBag.AllowedCountries = ShippingService.getAllowedCountries();
            ViewBag.AllowedStates = ShippingService.getAllowedStates();

            loadViewModel(vm);
            ViewBag.showSpecialInFooter = false;
            ViewBag.BillingDetailsCC = _contentRep.listAllPages().SingleOrDefault(x => x.SystemPage == Page.SystemPageOption.BILLING_DETAILS_CC);
            
            var details = OrderService.getPrepopOrderDetails(vm.order, getAccount(), null);

            AccountDetailsViewModel avm = new AccountDetailsViewModel()
            {
                CustomerDetails = details[0],
                DeliveryDetails = (AccountDetailDelivery)details[1]
            };

            avm.DeliveryDetails.CountryID = vm.order.CountryID;
            if(avm.CustomerDetails.CountryID.HasValue)
            {
                avm.CustomerDetails._country = ShippingService.getAllowedCountries().Where(x => x.ID == (int)avm.CustomerDetails.CountryID).FirstOrDefault();
            } 
            else
            {
                avm.CustomerDetails._country = new Country();
            }    
            
            avm.DeliveryDetails._country = vm.order.country;

            if(vm.order.StateID.HasValue)
            {
                avm.DeliveryDetails.Address4 = _shipRep.listAllStates().Single(x => x.ID == vm.order.StateID).Name;
                avm.DeliveryDetails.lockState = true;
            }
            else
                avm.DeliveryDetails.lockState = false;


            if (vm.order.Postcode.HasValue)
            {
                avm.DeliveryDetails.Postcode = vm.order.Postcode.ToString();
                avm.DeliveryDetails.lockPostcode = true;
            }
            else
                avm.DeliveryDetails.lockPostcode = false;

          
                if (avm.DeliveryDetails.Postcode != null)
                {
                    if(int.TryParse(avm.DeliveryDetails.Postcode, out int n))
                    { 
                        avm.DeliveryDetails.Postcode = Kawa.Controllers.SiteController.MaskPostCode(int.Parse(avm.DeliveryDetails.Postcode)); 
                    }                    
                }

                if (avm.CustomerDetails.Postcode != null)
                {
                    if (int.TryParse(avm.CustomerDetails.Postcode, out int n))
                    {
                        avm.CustomerDetails.Postcode = Kawa.Controllers.SiteController.MaskPostCode(int.Parse(avm.CustomerDetails.Postcode));
                    }
                }
          


            avm.DeliveryDetails._AddressLookupText = _contentRep.listAllPages().SingleOrDefault(x => x.SystemPage == Page.SystemPageOption.ACCOUNTDETAIL_LOOKUP_TEXT).html2;
            avm.CustomerDetails._AddressLookupText = avm.DeliveryDetails._AddressLookupText;

            return View(avm);
        }

        [HttpPost]
        public async Task<ActionResult> AccountDetails(Guid? id, AccountDetailsViewModel model)
        {
            runExpire();
            SiteViewModel vm = new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.ACCOUNT_DETAILS, ordercode = (Guid)id };
            vm.order = OrderService.getFullOrder((Guid)id);
            var badresult = checkOrder(vm.order);
            if (badresult != null) return badresult;

            ViewBag.AllowedCountries = ShippingService.getAllowedCountries();
            ViewBag.AllowedStates = ShippingService.getAllowedStates();

            loadViewModel(vm);
            ViewBag.showSpecialInFooter = false;
            ViewBag.BillingDetailsCC = _contentRep.listAllPages().SingleOrDefault(x => x.SystemPage == Page.SystemPageOption.BILLING_DETAILS_CC);

            ViewBag.AddressLookupText = _contentRep.listAllPages().SingleOrDefault(x => x.SystemPage == Page.SystemPageOption.ACCOUNTDETAIL_LOOKUP_TEXT).html2;
            if (model.DeliveryDetails.leaveOptionSelected == AccountDetailDelivery.LEAVE_SET_NO_OPTION_CHOOSEN)
            {
                ModelState.AddModelError("", "If you grant autority to leave, you must choose an option from the dropdown.");
            }


            if (ModelState.IsValid)
            {
                if (vm.order.CustomerDetails == null)
                {
                    model.CustomerDetails.OrderID = vm.order.ID;
                    model.CustomerDetails.Type = AccountDetail.DetailTypeOption.ACCOUNT_DETAILS;
                    vm.order.Details.Add(model.CustomerDetails);
                }
                else
                {
                    if (model.CustomerDetails.ID != vm.order.CustomerDetails.ID)
                        model.CustomerDetails.ID = vm.order.CustomerDetails.ID;
                }
                model.CustomerDetails.Country = ((List<Country>)ViewBag.AllowedCountries).Single(x => x.ID == model.CustomerDetails.CountryID).Name;
                _orderRep.saveAccountDetail(model.CustomerDetails);

                if (vm.order.DeliveryDetails == null)
                {
                    model.DeliveryDetails.OrderID = vm.order.ID;
                    model.DeliveryDetails.Type = AccountDetail.DetailTypeOption.DELIVERY_DETAILS;
                    vm.order.Details.Add(model.DeliveryDetails);
                }
                else
                {
                    if (model.DeliveryDetails.ID != vm.order.DeliveryDetails.ID)
                        model.DeliveryDetails.ID = vm.order.DeliveryDetails.ID;
                }
                model.DeliveryDetails.Country = ((List<Country>)ViewBag.AllowedCountries).Single(x => x.ID == model.DeliveryDetails.CountryID).Name;
                
                if (model.DeliveryDetails.leaveOptionSelected == null)
                {
                    model.DeliveryDetails.DeliveryInstructions = null;
                    model.DeliveryDetails.LeaveAuth = false;
                }
                else
                {
                    if (model.DeliveryDetails.leaveOptionSelected != AccountDetailDelivery.LEAVE_OTHER_OPTION)
                        model.DeliveryDetails.DeliveryInstructions = AccountDetailDelivery.LEAVE_PREFIX + model.DeliveryDetails.leaveOptionSelected;
                    model.DeliveryDetails.LeaveAuth = true;
                }

                _orderRep.saveAccountDetail(model.DeliveryDetails);
               
                if (model.CustomerDetails.isMailer)
                {
                    await MailChimpService.AddEmailToListAsync(model.CustomerDetails.Email, model.CustomerDetails.FirstName, model.CustomerDetails.Surname);
                }

                if (getAccount() != null)
                {
                    var account = getAccount();
                    if(string.IsNullOrEmpty(account.firstName) && string.IsNullOrEmpty(account.lastName))
                    {
                        account.firstName = model.CustomerDetails.FirstName;
                        account.lastName = model.CustomerDetails.Surname;
                        _personRep.saveAccount(account);
                    }
                }
                /*
                if (OrderService.isCompletePromotionZeroOrder(vm.order))
                {
                    OrderService.completePromotionZeroOrder(vm.order);
                    ViewBag.basketCount = 0;
                    vm.isOrder = true;
                    return View("Complete", vm);
                }*/

                //return EPay(vm.order.Code);
                return RedirectToAction("ChoosePayment", new { id = vm.order.Code });
            }


            return View(model);
        }

        public ActionResult ChoosePayment(Guid id)
        {
            loadViewModel(new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.CHOOSE_PAYMENT, ordercode = (Guid)id });

            var order = OrderService.getFullOrder((Guid)id);

            if(order.OrderTotal == 0)
            {
                order.PaymentType = PaymentServiceStore.PaymentTypeOption.FREE;
                _orderRep.saveOrder(order);
                ///free order, just complete it.
                return Redirect("/shop/complete/" + id);
            }
            else
            { 
                var badresult = checkOrder(order);
                if (badresult != null) return badresult;

                OrderService.resetAmexSurcharge(order);
                var options = SiteService.getPaymentOptions(false);

                /*
                if(order.CustomerDetails.CountryID != _shipRep.listAllCountries().Single(x => x.Name == "Australia").ID)
                    options.Remove(PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT);

                if(ShippingService.isBlockedIP(Request.UserHostAddress))
                    options.Remove(PaymentServiceStore.PaymentTypeOption.EWAY);
                */

                var vm = new ChoosePaymentViewModel()
                {
                    orderCode = id,
                    options = options.Select(s => PaymentServiceStore.getService(s).getPaymentChoiceModel()).ToList()
                };
                vm.options.ForEach(x => x.order = order);

                return View(vm);
            }

        }

        public ActionResult PaymentIssue(Guid id)
        {
            loadViewModel(new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.PAYMENT});

            SiteViewModel vm = new SiteViewModel();

            loadViewModel(vm);
            vm.order = OrderService.getFullOrder(id);
            string ResultQs = Request.QueryString["result"];

            if (!string.IsNullOrEmpty(ResultQs))
            {
                string PxPayUserId = ConfigurationManager.AppSettings["PaymentExpress.PxPay_User"];
                string PxPayKey = ConfigurationManager.AppSettings["PaymentExpress.PxPay_Pass"];
                // Obtain the transaction result
                PxPay WS = new PxPay(PxPayUserId, PxPayKey);

                ResponseOutput output = WS.ProcessResponse(ResultQs);
                ViewBag.Message = "Sorry there was a an issue with your payment <b>" + output.ResponseText + "</b>";
                ViewBag.Message += "<div style='display:none;'> error:" + JsonConvert.SerializeObject(output) + "</div>";

            }
            return View("PaymentIssue", vm);


        }
        public ActionResult Payment(Guid id, PaymentServiceStore.PaymentTypeOption pt)
        {
            loadViewModel(new SiteViewModel() { checkoutStage = SiteViewModel.CheckoutStage.PAYMENT, ordercode = (Guid)id });

            if (pt == Models.Services.PaymentServiceStore.PaymentTypeOption.EWAY && ShippingService.isBlockedIP(Request.UserHostAddress))
                return Redirect("/");

            var order = OrderService.getFullOrder(id);
            var badresult = checkOrder(order);
            if (badresult != null) return badresult;

            OrderService.resetAmexSurcharge(order);
            var vm = PaymentServiceStore.getService(pt).getPaymentModel(getAccount(), order);
            ViewBag.Order = order;

            return vm.hasView ? View(vm) : (ActionResult)Redirect(vm.link);
        }

        public ActionResult Complete(Guid? id, FormCollection formValues, string AccessCode)
        {
            var vm = new PaymentCompleteViewModel();
            try
            {

            
                vm.order = OrderService.getFullOrder((Guid)id);

                PaymentServiceStore.PaymentTypeOption pt = (PaymentServiceStore.PaymentTypeOption)vm.order.PaymentType;

                var orderissue = OrderService.checkForBasketReset(vm.order);
                if(orderissue)
                { 
                    vm.order = OrderService.getFullOrder((Guid)id);
                    orderissue = OrderService.checkForBasketReset(vm.order);
                }

                object postObj = null;
                switch(pt)
                {
                    case Models.Services.PaymentServiceStore.PaymentTypeOption.VERIFONE:
                        postObj = Request.QueryString["checkout_id"];
                        break;
                    case Models.Services.PaymentServiceStore.PaymentTypeOption.WINDCAVE:
                        postObj = Request.QueryString["result"];
                        break;
                    case Models.Services.PaymentServiceStore.PaymentTypeOption.EWAY:
                        postObj = AccessCode;
                        break;
                    case Models.Services.PaymentServiceStore.PaymentTypeOption.PAYPAL:
                        postObj = formValues;
                        break;
                }
                if(pt == PaymentServiceStore.PaymentTypeOption.FREE)
                {
                    vm.result = new PaymentResultModel() { authcode = vm.order.ID.ToString() + vm.order.Code, fullResponse = "Zero payment order", success = true, token = "", type = PaymentServiceStore.PaymentTypeOption.FREE, responseCode = "0"};
                }
                else
                {
                    vm.result = PaymentServiceStore.getService(pt).getResult(postObj, vm.order);
                }
            

                if (vm.result.success)
                {
                    vm.order = OrderService.getFullOrder((Guid)id);///reget the order to ensure it is not been procssed while the getresult was running.
                    if (vm.order.SystemStatus == Order.SystemStatusOption.IN_PROGRESS)
                    {
                        OrderService.completeSuccessfulOrder(vm.order, vm.result);

                        if (vm.order.AccountID != null && vm.result.token != null)
                        {
                            var account = _personRep.listAllAccounts().Single(a => a.ID == vm.order.AccountID);
                            if (account.saveCreditCard && account.creditCardToken != vm.result.token)
                            {
                                account.creditCardToken = vm.result.token;
                                _personRep.saveAccount(account);
                            }
                        }

                        if (orderissue)
                        {
                            sendIssueEmail("Please enter the admin and reconcile order " + vm.order.OrderNo + ".  The items in the order where changed while the user was waiting for their transaction to process.");
                            return RedirectToAction("CompleteWithIssue", new { id = vm.order.Code });
                        }
                    }
                    ViewBag.OrderSummary = OrderService.getSummary();
                }
                else
                {
                    OrderService.saveFailedOrder(vm.order, vm.result);
                    return RedirectToAction("InComplete", new { id = vm.order.Code });
                }

                loadViewModel(new SiteViewModel()
                {
                    isOrder = true,
                    checkoutStage = SiteViewModel.CheckoutStage.COMPLETE,
                    ordercode=(Guid)id
                });

                return View(vm);


            }
            catch (Exception ex)
            {
                MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], "chris@pixelle.co.nz")
                {
                    Subject = "An order from Meguiars has had an error after payment.",
                    IsBodyHtml = true
                };
                string outputstr = ex.Message + "<BR><BR>";
                try
                {

                    string checkout_id = Request.QueryString["checkout_id"];
                    outputstr = checkout_id;

                }
                catch (Exception exr)
                {
                    outputstr += exr.Message;
                }
                msg.Body = ex.Message + "<BR><BR>" + ex.StackTrace + "<BR><BR>" + outputstr;


                EmailSendService.Send(msg);

                OrderService.saveFailedOrder(vm.order, vm.result);
                return RedirectToAction("InComplete", new { id = vm.order.Code });



            }

        }

        public ActionResult InComplete(Guid? id, FormCollection formValues, string AccessCode)
        {
            loadViewModel(new SiteViewModel()
            {
                isOrder = true,
                checkoutStage = SiteViewModel.CheckoutStage.PAYMENT,
                ordercode=(Guid)id
            });


            var vm = new PaymentFailedViewModel()
            {
                order = OrderService.getFullOrder((Guid)id),
            };

            OrderPayment p = _orderRep.listAllOrderPayments().Where(x => x.OrderID == vm.order.ID).OrderByDescending(x => x.ID).FirstOrDefault();
            ViewBag.Error = p.PaymentCode;
            ViewBag.OrderNo = vm.order.OrderNo;

            var badresult = checkOrder(vm.order,true);
            if (badresult != null) return badresult;

            OrderService.resetAmexSurcharge(vm.order);
            vm.type = (PaymentServiceStore.PaymentTypeOption)vm.order.PaymentType;

           

            return View(vm);
        }

        #region AJAX

        [HttpPost]
        public JsonResult SaveCard(bool saveCard)
        {
            if (getAccount() == null)
                return this.Json(new { success = false, error = "User no longer logged in." }, JsonRequestBehavior.AllowGet);

            var account = getAccount();
            account.saveCreditCard = saveCard;
            _personRep.saveAccount(account);

            return this.Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdatePostUrl(Guid code, bool isAmex, bool isAdmin)
        {
            var order = OrderService.getFullOrder(code);
            var badresult = checkOrder(order);
            if (badresult != null) return this.Json(new { success = false, error = "Order issue." }, JsonRequestBehavior.AllowGet);

            if((isAmex && order.AmexSurcharge.HasValue) || (!isAmex && !order.AmexSurcharge.HasValue))
                this.Json(new { success = false, error = "No Change needed." }, JsonRequestBehavior.AllowGet);

            OrderService.setAmexSurcharge(order,isAmex);

            var service = PaymentServiceStore.getService(PaymentServiceStore.PaymentTypeOption.EWAY);
            service.IsAdminPayment = isAdmin;
            var vm = (EwayViewModel)service.getPaymentModel(getAccount(), order);

            return this.Json(new { 
                success = true,
                FormActionURL = vm.ewayResponse.FormActionURL,
                AccessCode = vm.ewayResponse.AccessCode
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AmexTokenCardUsed(Guid code)
        {
            var order = OrderService.getFullOrder(code);
            var badresult = checkOrder(order);
            if (badresult != null) return this.Json(new { success = false, error = "Order issue." }, JsonRequestBehavior.AllowGet);
            OrderService.setAmexSurcharge(order, true);
            return this.Json(new
            {
                success = true
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddVoucher(Promotion voucher)
        {
            runExpire();
            SiteViewModel vm = new SiteViewModel() { };
            addBasketSetup(vm);
            Order order = OrderService.getCartOrder();
            if (string.IsNullOrEmpty(voucher.code))
            {
                order.isBasket = true;
                ViewBag.voucherMessage = "Please provide a promotion code.";
                return PartialView("_Basket", order);
            }

            var result = PromotionService.checkVoucher(voucher, order);
            ViewBag.voucherMessage = result.message;
            order = OrderService.getCartOrder();
            order.isBasket = true;
            
            return PartialView("_Basket", order);
        }


        public ActionResult GetStateChangeMessage()
        {
            runExpire();


            return PartialView("_ModalMessageContent", _contentRep.listAllPages().SingleOrDefault(x => x.SystemPage == Page.SystemPageOption.DELIVERY_POSTCODE_CHANGE_DETECTED));
        }
        public ActionResult GetAccountDetailStateChangeMessage()
        {
            runExpire();

            return PartialView("_ModalMessageContent", _contentRep.listAllPages().SingleOrDefault(x => x.SystemPage == Page.SystemPageOption.ACCOUNTDETAIL_STATEPOSTCODE_CHANGE_TEXT));
        }

        public bool IsCountryAutoSearchable(int id)
        {
            runExpire();
            return ShippingService.getAllowedCountries().Where(x => x.ID == id).FirstOrDefault().isAddressSearch;

        }

        public JsonResult GetCountry(int id)
        {
            runExpire();
                return this.Json(new
                {
                    country = ShippingService.getAllowedCountries().Where(x => x.ID == id).FirstOrDefault()
                }, JsonRequestBehavior.AllowGet); 

        }
        #endregion

        public void sendIssueEmail(string message)
        {
            MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["ShopEmail"], ConfigurationManager.AppSettings["ShopBCC"])
            {
                Subject = "An order from Kawa has had an issue",
                IsBodyHtml = true
            };
            msg.Bcc.Add(ConfigurationManager.AppSettings["ShopBCC"]);
            msg.Body = message;
            using (var client = new SmtpClient())
            {
                client.Send(msg);
            }
        }
        
        public ActionResult checkOrder(Order order, bool allowPaypal = true)
        {
            if(order.SystemStatus > Order.SystemStatusOption.IN_PROGRESS || 
                order.orderItems.Count==0 ||
                (!allowPaypal && 
                    order.SystemStatus == Order.SystemStatusOption.IN_PROGRESS && 
                    order.PaymentType == PaymentServiceStore.PaymentTypeOption.PAYPAL))
            {
                return new RedirectResult("/shop?log=orderinvalid");
            }
            return null;
        }

        protected override void loadViewModel(SiteViewModel viewModel)
        {
            viewModel.section = "Shop";
            base.loadViewModel(viewModel);
        }
    }
}