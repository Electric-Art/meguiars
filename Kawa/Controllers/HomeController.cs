﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using Kawa.Models;
using Kawa.Models.ViewModels;
using System.Net.Mail;
using System.Configuration;
using WebMarkupMin.AspNet4.Mvc;
using MailChimp.Helper;


namespace Kawa.Controllers
{
    [MinifyHtml]
    public class HomeController : SiteController
    {
        public ActionResult Index()
        {
            DeploymentConfig.RunIdentityUpdates();

            SiteViewModel svm = new SiteViewModel();
            loadViewModel(svm);

            var vm = TagService.GetForHome();
            if (vm.Metadesciption != null)
                ViewBag.MetaDescription = vm.Metadesciption.html;

            SiteResourceService.loadUrls(vm.BillboardPage.siteResources);
            SiteResourceService.loadUrls(vm.BrandPage.siteResources);
            vm.Slots.ForEach(x =>
            {
                SiteResourceService.loadUrls(x.page.siteResources);
            });

            try
            {
                var newsarticles = LMCDataService.LoadNews(DateTime.UtcNow.UtcToLocal().Year, DateTime.UtcNow.UtcToLocal().Month);
                ViewBag.RecentNews = newsarticles.RecentNews.Take(3).ToList();
            }
            catch { ViewBag.RecentNews = new List<Models.Data.New>(); }


            return View(vm);
        }
        public ActionResult Faq()
        {
            SiteViewModel svm = new SiteViewModel();
            loadViewModel(svm);
            svm.pages = _contentRep.listAllPages().Where(x => x.Type == Page.TypeOption.FAQ && !x.isHidden).OrderBy(x=>x.orderNo).ToList();
            return View(svm);
        }
        public ActionResult TestAccountCreation(string code)
        {
            string[] newcustomer = new string[] { "customercode=MEG" + code, "FirstName=Testing123","LastName=TestingLastName", "Phone=00-000-000", "recipientcode=NA", "webpassword=Password01#" };

            ///Code=[email address] , FirstName=[customer first name], LastName=[customer last name] , Phone=[customer phone number], Recipient=[Rebate Recipient Code]
            ///var result = webService.Create_Customer(newcustomer);
            var propellaRegistrationResult = PropellaService.GetGPIWebServiceData(new PropellaService.WebServiceDataRequest() { Args = newcustomer, Function = "Create_Customer" });

            return Content(propellaRegistrationResult.ReturnValue.ToString());
        }

        


        public ActionResult Galleries(GalleryTypeOption type = GalleryTypeOption.Gallery_Photo)
        {
            ViewBag.gMode = "list";
            SiteViewModel svm = new SiteViewModel();
            loadViewModel(svm);
            ViewBag.GalleryTitle = (type == GalleryTypeOption.Gallery_Photo ? "PHOTO GALLERIES" : "VIDEO CENTRE");
            using (var _db = new Models.Data.dbDataContext())
            {
                var model = new GalleryViewModel
                {
                    Galleries = LMCDataService.GetGalleries(type).Where(a => !a.IsHidden).ToList()
                };
                return View("Gallery", model);
            }
        }
        public ActionResult Gallery(int id, string name)
        {
            ViewBag.gMode = "view";
            SiteViewModel svm = new SiteViewModel();
            loadViewModel(svm);
            using (var _db = new Models.Data.dbDataContext())
            {
                var model = new GalleryViewModel
                {
                    Galleries = LMCDataService.GetGallery(id).ToList()
                    
                };
                ViewBag.GalleryTitle = (model.Galleries.FirstOrDefault().TypeE == 4 ? "Photo Gallery - " : "Video Gallery - ") + model.Galleries.FirstOrDefault().Name ;
                model.Gallery = model.Galleries.FirstOrDefault();
                return View("Gallery", model);
            }
        }
        public ActionResult Videos(string id)
        {
            DeploymentConfig.RunIdentityUpdates();

            SiteViewModel svm = new SiteViewModel();
            loadViewModel(svm);
            ViewBag.videos = _contentRep.listAllPages().Where(x => x.Type == Page.TypeOption.CONTENT_VIDEO && (x.html3 == id || x.html3=="Both")).ToList();
            ViewBag.titles = id + " Videos";
            return View(svm);
        }
        public List<Tag> BuildRecursiveStructure(Tag tag, List<Tag> tags, List<Tag> structure, int rank)
        {
            foreach(Tag t in tags.Where(x => x.ParentID == tag.ID).ToList())
            {
                rank++;
                t.level = rank;
                structure.Add(t);
                structure = BuildRecursiveStructure(t, tags, structure, rank);
            }
           
            return structure;
        }


        public List<PrescriptionTag> BuildRecursiveBreadCrumb(PrescriptionTag tag, List<PrescriptionTag> tags, List<PrescriptionTag> breadcrumb, int rank)
        {
            if (tag.ParentID != null)
            {
                PrescriptionTag parentTag = tags.Where(x => x.ID == tag.ParentID).FirstOrDefault();
                if (parentTag != null)
                {
                    rank++;
                    parentTag.level = rank;
                    breadcrumb.Add(parentTag);
                    breadcrumb = BuildRecursiveBreadCrumb(parentTag, tags, breadcrumb, rank);
                }
            }
            return breadcrumb;
        }
        public ActionResult Prescription(int? id, string name)
        {
            DeploymentConfig.RunIdentityUpdates();

            SiteViewModel svm = new SiteViewModel();
            loadViewModel(svm);
            
            if(id!=null)
            {
                svm.prescriptiontag = _contentRep.listAllPerscriptionTags(true).Where(x => x.ID == (int)id).FirstOrDefault();
            } else
            {
                svm.prescriptiontag = _contentRep.listAllPerscriptionTags(true).Where(x => x.ID == 6379).FirstOrDefault();                
            }


            List<PrescriptionTag> listTags = _contentRep.listAllPerscriptionTags(true).Where(x => x.Type == PrescriptionTag.TypeOption.CAR_PRESCRIPTION_CAT && x.isHidden == false && x.ParentID ==svm.prescriptiontag.ID).ToList();

            List<int> _quantities = new List<int>();
            for (int i = 1; i <= 100; i++) _quantities.Add(i);
            ViewBag.Quantities = _quantities;
            List<PrescriptionTag> breadcrumb = new List<PrescriptionTag>();
            breadcrumb.Add(svm.prescriptiontag);

            List<PrescriptionTag> stucturelist = _contentRep.listAllPerscriptionTags(false).Where(x => x.Type == PrescriptionTag.TypeOption.CAR_PRESCRIPTION_CAT && x.isHidden == false).ToList();

            breadcrumb = BuildRecursiveBreadCrumb(svm.prescriptiontag, stucturelist, breadcrumb, 0);

            List<PrescriptionTag> structure = new List<PrescriptionTag>();
            svm.prescriptionlistTags = listTags.ToList(); /// BuildRecursiveStructure(breadcrumb.Last(), listTags, structure, 0);
            ViewBag.breadcrumb = breadcrumb;

            svm.prescriptiontag.products = _productRep.listProductsForPrescriptionTagNotIncludingParents(svm.prescriptiontag.ID, true, true).ToList();
            int productcount = svm.prescriptiontag.products.Count();
            listTags.Add(svm.prescriptiontag);
            listTags.ForEach(x =>
            {
                SiteResourceService.loadUrls(x.siteResources);

            });

            listTags.Where(x => x.ParentID == svm.prescriptiontag.ID || x.ID == svm.prescriptiontag.ID).Where(x=>x.colour == "products").ToList().ForEach(x =>
            {
                try
                {
                    x.products = _productRep.listProductsForPrescriptionTag(x.ID, true, true).Where(xx => xx.isHidden == false).ToList();
                    x.products.ToList().ForEach(xx =>
                    {
                        xx.basket = new Basket() { Quantity = 1, VariantID = xx.variant.ID };
                    });
                    productcount = productcount + x.products.Count();
                }
                catch { x.products = new List<Product>(); }
            });
      
            ViewBag.productcount = productcount;
            return View(svm);
        }


        [HttpGet]
        public ActionResult Test()
        {
            SiteViewModel vm = new SiteViewModel()
            {
            };

            loadViewModel(vm);

            return View(vm);
        }

        [HttpGet]
        public ActionResult SiteMap()
        {
            SiteViewModel vm = new SiteViewModel()
            {
            };


            loadViewModel(vm);

            return View(vm);
        }
        [HttpGet]
        public ActionResult TestEmail()
        {
            SiteViewModel vm = new SiteViewModel()
            {
            };

            MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], "chris@pixelle.co.nz")
            {
                Subject = "An enquiry has been made from the Meguiars website",
                IsBodyHtml = true
            };


            string body = "<div style='font-size:14px'><b>Request Details</b></div><br/>";
            //body += "Subject" + sep1 + Enum.GetName(typeof(Models.Contact.SubjectOption),contact.subject)  + sep2; 
            mail.Body = body;

            using (var client = new SmtpClient())
            {
                client.Send(mail);
            }

            return View("Contact");
        }

        [HttpGet]
        public ActionResult Contact()
        {
            SiteViewModel vm = new SiteViewModel()
            {
                page = _contentRep.listAllPages().Where(p => p.SystemPage == Page.SystemPageOption.CONTACT_US).Single(),
                contact = new Models.Contact()
            };

            vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);

            loadViewModel(vm);

            ViewBag.Success = false;
            ViewBag.html2 = vm.page.html2;

            return View(vm.contact);
        }

        [HttpPost]
        public ActionResult Contact(int? id, Models.Contact contact)
        {
            SiteViewModel vm = new SiteViewModel()
            {
                page = _contentRep.listAllPages().Where(p => p.SystemPage == Page.SystemPageOption.CONTACT_US).Single(),
                contact = contact

            };

            vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);
            loadViewModel(vm);

            ViewBag.Success = false;
            ViewBag.html2 = vm.page.html2;

            var captyaResult = new CaptchaService().CheckCaptyaSuccess(Request["g-recaptcha-response"]);

            if (!captyaResult.Success)
            {
                ModelState.AddModelError("", captyaResult.Message);
            }

            if (ModelState.IsValid)
            {
                MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["FromEmail"], ConfigurationManager.AppSettings["ContactCustService"])
                {
                    Subject = "An enquiry has been made from the Meguiars website",
                    IsBodyHtml = true
                };

                if(!string.IsNullOrEmpty(contact.email))
                {
                    mail.ReplyToList.Add(contact.email);
                }

                string body = "<div style='font-size:14px'><b>Request Details</b></div><br/>";
                //body += "Subject" + sep1 + Enum.GetName(typeof(Models.Contact.SubjectOption),contact.subject)  + sep2; 
                body += contact.GetDisplayName(x => x.firstName) + sep1 + contact.firstName + sep2;
                body += contact.GetDisplayName(x => x.surname) + sep1 + contact.surname + sep2;
                body += contact.GetDisplayName(x => x.email) + sep1 + contact.email + sep2;
                body += contact.GetDisplayName(x => x.phone) + sep1 + contact.phone + sep2;
                body += contact.GetDisplayName(x => x.enquiry) + sep1 + contact.enquiry + sep2;


                mail.Body = body;

                using (var client = new SmtpClient())
                {
                    client.Send(mail);
                }
                //var client = new SmtpClient();
                //try
                //{
                //    client.Send(mail);
                //}
                //catch { }

                ViewBag.Success = true;
                //svm.isSent = true;

                return View(contact);
            }
            else
            {
                string message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));
                return View(contact);
            }
        }

        public ActionResult WishList(int? id)
        {
            if(getAccount()!=null)
            {
                return Redirect("/Account/Wishlist");
            }
            SiteViewModel vm = new SiteViewModel() { subSection = "Favourites" };
            loadViewModel(vm);

            string session = OrderService.getFavSession();
            vm.account = _personRep.listAllAccounts().Where(x => x.UserName == "guest@meguiars.co.nz").FirstOrDefault();
            vm.account.favourites = _personRep.listAllFavourites().Where(f => f.Session == session).ToList();

            vm.account.favourites.ToList().ForEach(xx =>
            {
                xx.product.basket = new Basket() { Quantity = 1, VariantID = xx.product.variant.ID };
            });


            return View("WishList", vm);
        }
        public ActionResult FavouriteDelete(int? id)
        {
            var fav = _personRep.listAllFavourites().Single(f => f.ID == id);
               
            _personRep.deleteFavourite(fav);

            return RedirectToAction("WishList");
        }
        #region Sitewide Ajax

        [HttpPost]
        public JsonResult ToggleFavourite(Favourite favourite)
        {

            Favourite existing;
            
            
         

            if (getAccount() == null)
            {
                string session = OrderService.getFavSession();
                int AccountID = _personRep.getGuest().ID;

                if (favourite.ProductID.HasValue)
                    existing = _personRep.listAllFavourites().Where(f => f.AccountID == AccountID && f.ProductID == favourite.ProductID && f.Session == session).SingleOrDefault();
                else
                    existing = _personRep.listAllFavourites().Where(f => f.AccountID == AccountID && f.PageID == favourite.PageID && f.Session == session).SingleOrDefault();

                favourite.Session = session;
                favourite.AccountID = AccountID;
            }
            else
            {
                if (favourite.ProductID.HasValue)
                    existing = _personRep.listAllFavourites().Where(f => f.AccountID == getAccount().ID && f.ProductID == favourite.ProductID).SingleOrDefault();
                else
                    existing = _personRep.listAllFavourites().Where(f => f.AccountID == getAccount().ID && f.PageID == favourite.PageID).SingleOrDefault();

                favourite.AccountID = getAccount().ID;

            }

            if (existing == null)
            {
                favourite.createDate = DateTime.Now;
                _personRep.saveFavourite(favourite);
            }
            else
            {
                _personRep.deleteFavourite(existing);
            }

            return this.Json(new { success = true, data = (existing == null?"added":"removed") }, JsonRequestBehavior.AllowGet);
        }

        /*
        [HttpPost]
        public virtual JsonResult SearchSuggest(string search)
        {
            var products = _productRep.listProductMatchesQuick(search);
            var pages = _contentRep.listPageMatchesQuick(search);

            var result = new { 
                success = true, 
                products = products.Select(p => new { label=p.name, value=p.getUrl() }),
                pages = pages.Select(p => new { label = p.name, value = p.getUrl() })
            };
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }
        */
        #endregion

        #region Util

        public ActionResult TestAnon(int? id)
        {
            var date = DateTime.UtcNow.UtcToLocal();

            return Content("Ok " + date.ToString());
        }
        
        #endregion
    }
}