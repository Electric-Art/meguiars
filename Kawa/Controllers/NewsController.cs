﻿using Kawa.Models.Data;
using Kawa.Models.Extensions;
using Kawa.Models.Services;
using Kawa.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMarkupMin.AspNet4.Mvc;

namespace Kawa.Controllers
{

    [MinifyHtml]
    public class NewsController : SiteController
    {
        // GET: News
        public ActionResult Index(int? id)
        {
            loadViewModel(new SiteViewModel());
            using (var _db = new dbDataContext())
            {
                var model = LMCDataService.LoadNews(DateTime.UtcNow.UtcToLocal().Year, DateTime.UtcNow.UtcToLocal().Month);
                ViewBag.NewsMenuViewModel = model;


                if (!model.NewsList.Any())
                    model.NewsList = model.RecentNews.Take(12).ToList();

                model.PageHeading = "Latest News";

                return View(model);
            }
        }

        public ActionResult Archive(string id)
        {
            loadViewModel(new SiteViewModel());
            string[] dates = id.Split(',');
            var model = LMCDataService.LoadNews(int.Parse(dates[0]), int.Parse(dates[1]));
            DateTime showing = new DateTime(int.Parse(dates[0]), int.Parse(dates[1]), 01);
            ViewBag.NewsMenuViewModel = model;
            model.PageHeading = "News Archive - " + showing.ToString("MMMM") + " " + dates[0];
            ViewBag.SelectedArchiveDate = showing.ToString("yyyy,MM");
            model.RecentNews = model.NewsList;
            return View("Index", model);
        }

        public ActionResult Article(int id)
        {
            loadViewModel(new SiteViewModel());
            using (var _db = new dbDataContext())
            {
                var model = LMCDataService.LoadNews(DateTime.UtcNow.UtcToLocal().Year, DateTime.UtcNow.UtcToLocal().Month);
                ViewBag.NewsMenuViewModel = model;

                model.NewsItem = LMCDataService.LoadArticle(id);
                model.NewsList = LMCDataService.ListNews().Where(x=>x.ID == id).ToList();

                ViewBag.metadescription = model.NewsItem.metadescription;
                ViewBag.metakeywords = model.NewsItem.metakeywords;

                if (model.NewsItem == null)
                    return Redirect("Index");

                return View("Index", model);
            }
        }
    }
}