﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Extensions;
using Kawa.Models.Services;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class ArticleAdminController : AdminSiteController
    {
        // GET: ArticleAdmin
        public ActionResult Index(int? id)
        {
            runExpire();

            AdminViewModel vm = new AdminViewModel()
            {
                pages = id == null ? _contentRep.listAllPages().Where(p => p.Type == Models.Page.TypeOption.CONTENT).ToList() :
                                    _contentRep.listPagesForTag((int)id).Where(p => p.Type == Models.Page.TypeOption.CONTENT).ToList(),
                tags = _contentRep.listAllTags().Where(t => t.Type == Tag.TypeOption.PRODUCT_SECTION).ToList(),
                tag = id == null ? null : _contentRep.listAllTags().Where(t => t.ID == id).SingleOrDefault()
            };

            if(vm.tag == null)
            {
                foreach (var page in vm.pages)
                    loadSrs(page, false);
            }

            

            loadViewModel(vm);

            return View(vm);
        }

        

        [HttpGet]
        public ActionResult Page(int? id, int? t, Page.TypeOption? type)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = id.HasValue ? _contentRep.listAllPages().Where(p => p.ID == id).Single() : new Page(),
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.PRODUCT_SECTION,
                pages = _contentRep.listAllPages().Where(p => p.Type == Models.Page.TypeOption.CONTENT).ToList()
                
            };
            if(vm.page.Type == Models.Page.TypeOption.NEWS_SELECTOR)
            {
                ViewBag.news = LMCDataService.LoadNews().NewsList;
            }

            if(vm.page.Type == Models.Page.TypeOption.CONTENT_VIDEO)
            {
                vm.pages = _contentRep.listAllPages().Where(p => p.Type == Models.Page.TypeOption.CONTENT_VIDEO).ToList();
            }

            vm.page.products = _productRep.listProductsForPage(vm.page.ID, false, true).ToList();
            vm.page.pages = _contentRep.listRelatedPagesForPage(vm.page.ID).ToList();
                

            return runPageAdminGet(vm, t, type, "Page");
        }

        public ActionResult AssociateProduct(int ProductID, int PageID, int Rank)
        {
            DeleteAssociatedProduct(ProductID, PageID);///to ensure no duplciates
            _productRep.savePageRelationShip(ProductID, PageID, Rank);
            return Content("added....");
        }

        public ActionResult DeleteAssociatedProduct(int ProductID, int PageID)
        {
            _productRep.deletePageRelationShip(ProductID, PageID);
            return Content("removed....");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Page(int? id, int? t, Page page)
        {

            if(page.Type == Models.Page.TypeOption.NEWS_SELECTOR)
            {
                page.html = Request.Form["page.html"];
            }
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = page,
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.PRODUCT_SECTION,
                pages = _contentRep.listAllPages().Where(p => p.Type == Models.Page.TypeOption.CONTENT).ToList()
            };

            if(page.Type == Models.Page.TypeOption.CONTENT_VIDEO)
            { 
                if (page.nameMenu != null)
                {
                    page.html4 = page.nameMenu;
                    page.nameMenu = "";
                }
            }

            if (Request["showedit"] == "true")
            {
                return runPageAdminPost(vm, page, "ArticleAdminPage", new AdminPageRouteVariables (){ 
                    id = page.ID, 
                    m = "s", 
                    t = Request["t"],
                    injectID = true
                }, "Page");
            }
            else
            {
                return runPageAdminPost(vm, page,
                    (page.Type != Models.Page.TypeOption.CLICK_THROUGH ? "ArticleAdminPage" : "ArticleAdminPage"), 
                    new AdminPageRouteVariables() {
                        id = page.ID,
                        m = "s",
                        t = Request["t"],
                        injectID = true
                    }, "Page");
            }
            
        }

        public ActionResult Featured(int? id)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                pages = _contentRep.listAllPages().ToList(),
                slots = _contentRep.listAllSlots().ToList()
            };

            if (vm.tag == null)
            {
                foreach (var page in vm.pages)
                    loadSrs(page, false);
            }


            loadViewModel(vm);

            return View(vm);
        }

        public ActionResult Modules(int? id)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                pages = _contentRep.listAllPages().Where(p => p.Type == Kawa.Models.Page.TypeOption.CLICK_THROUGH || p.Type == Kawa.Models.Page.TypeOption.CONTENT_VIDEO).ToList()
            };


            foreach (var page in vm.pages)
                loadSrs(page, false);



            loadViewModel(vm);

            return View(vm);
        }

        [HttpGet]
        public ActionResult PageDelete(int? id)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = _contentRep.listAllPages().Where(p => p.ID == id).Single()
            };

            loadViewModel(vm);

            return View(vm);
        }


        [HttpPost]
        public ActionResult PageDelete(int? id, Page inPage)
        {
            runExpire();
            Page page = _contentRep.listAllPages().Where(p => p.ID == id).Single();
            page.tags = _contentRep.listAllTagsForPage(page).ToList();


            loadSrs(page, false);
            pageSrSetrup(new AdminViewModel() { page = page });
            deleteExistingImages(page);

            if(page.tags.Any(t => t.Type == Tag.TypeOption.PAGE_SECTION))
            { 
                List<Page> children = _contentRep.listPagesForTag(page.tags.Where(t => t.Type == Tag.TypeOption.PAGE_SECTION).Single().ID).ToList();
                List<Page> savers = OrdererService.delete((IOrderedObject)page, children.Cast<IOrderedObject>().ToList()).Cast<Page>().ToList();
                foreach (var obj in savers)
                {
                    _contentRep.savePage(obj);
                }
            }
            _contentRep.deletePage(page);
            TagService.RunForMenu();
            if (page.Type == Models.Page.TypeOption.CLICK_THROUGH)
                return RedirectToRoute("ArticleAdminModules", new { id = "" });
            return RedirectToRoute("ArticleAdminIndex", new { id = page.tags.Any() ? page.tags.First().ID.ToString() : null });
        }

        [HttpGet]
        public ActionResult ShippingBanner(int? id, string msg)
        {
            ViewBag.message = msg;

            Page page = _contentRep.listAllPages().Where(p => id == 1 ? p.SystemPage == Models.Page.SystemPageOption.SHIPPING_BANNER_1 : p.SystemPage == Models.Page.SystemPageOption.SHIPPING_BANNER_2).SingleOrDefault();
            if(page == null)
            {
                page = Models.Page.ShippingBannerDefault((int)id);
            }
            return View(page);
        }

        [HttpPost]
        public ActionResult ShippingBanner(int? id, Page model)
        {
            if (!ModelState.IsValid)
                return View(model);

            Page page = _contentRep.listAllPages().Where(p => id == 1 ? p.SystemPage == Models.Page.SystemPageOption.SHIPPING_BANNER_1 : p.SystemPage == Models.Page.SystemPageOption.SHIPPING_BANNER_2).SingleOrDefault();
            if (page == null)
            {
                page = Models.Page.ShippingBannerDefault((int)id);
                page.createdDate = DateTime.Now;
            }
            page.name = model.name;
            page.html = model.html;
            page.html2 = model.html2;
            page.html3 = model.html3;
            _contentRep.savePage(page);

            TagService.RunForMenu();

            return RedirectToAction("ShippingBanner", new { msg = "Your shipping banner has been updated." });
        }

        [HttpGet]
        public ActionResult Retraction(int? id, string msg)
        {
            ViewBag.message = msg;
            ViewBag.pageTitle = "Retraction";

            Page page = _contentRep.listAllPages().Where(p => p.SystemPage == Models.Page.SystemPageOption.RETRACTION).SingleOrDefault();

            if(page == null)
            {
                var pd = new Page()
                {
                    name = "RETRACTION",
                    SystemPage = Models.Page.SystemPageOption.RETRACTION,
                    Type = Models.Page.TypeOption.TWO_BLOCK,
                    html = "An advertisement for the product Ateronon XY Pro, which we published on this website, should not have been published.<br/>In the advertisement we reproduced a video containing claims about Ateronon XY Pro that were misleading, unlawful, and unverified. These included claims that Ateronon XY Pro could \"shrink down the prostate\" and \"help prevent prostate cancer if you get onto it early\".<br/>A complaint about the advertisement was recently upheld by the Complaints Resolution Panel. We provided no evidence to support the claims we published, and the Panel found that the claims were unlawful, misleading, and unverified and breached the Therapeutic Goods Advertising Code.<br/>The Panel therefore requested that we publish this retraction.< br /> The full text of the Panel’s determination can be found at: www.tgacrp.com.au / complaints(reference 2017 / 11 / 005)",
                    createdDate = DateTime.Now,
                    isHidden = true,
                    orderNo = 1000
                };
                _contentRep.savePage(pd);
                page = pd;
            }
            return View("TwoBlockPage", page);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Retraction(int? id, Page model)
        {
            ViewBag.pageTitle = "Retraction";

            if (!ModelState.IsValid) {
                return View("TwoBlockPage", model);
            }

            Page page = _contentRep.listAllPages().Where(p => p.SystemPage == Models.Page.SystemPageOption.RETRACTION).SingleOrDefault();
            page.name = model.name;
            page.html = model.html;
            page.html2 = model.html2;
            page.html3 = model.html3;
            page.isHidden = model.isHidden;
            _contentRep.savePage(page);

            TagService.RunForHome();

            return RedirectToAction("Retraction", new { msg = "Your retraction has been updated." });
        }

        [HttpGet]
        public ActionResult ContentInsert(Page.SystemPageOption id, string msg)
        {
            ViewBag.message = msg;
            ViewBag.pageTitle = "Content Insert";

            Page page = _contentRep.listAllPages().Where(p => p.SystemPage == id).SingleOrDefault();
            if (page == null)
            {
                page = Models.Page.ContentInsertDefault(id);
            }
            return View(page);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ContentInsert(Page.SystemPageOption id, Page model)
        {
            ViewBag.pageTitle = "Content Insert";
            ModelState.Keys.Where(x => x.StartsWith("id")).ToList().ForEach(x => ModelState.Remove(x));

            if (!ModelState.IsValid)
                return View(model);

            Page page = _contentRep.listAllPages().Where(p => p.SystemPage == id).SingleOrDefault();
            if (page == null)
            {
                page = Models.Page.ContentInsertDefault(id);
                page.createdDate = DateTime.Now;
            }
            page.html = model.html;
            page.isHidden = model.isHidden;
            _contentRep.savePage(page);

            TagService.RunForMenu();

            return RedirectToAction("ContentInsert", new { msg = "Your content insert has been updated." });
        }
    }
}