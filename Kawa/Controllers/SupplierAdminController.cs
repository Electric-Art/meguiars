﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models.Data;
using System.Data.Linq;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class SupplierAdminController : AdminSiteController
    {
        // GET: SupplierAdmin
        public ActionResult Index()
        {
            using (var _db = new dbDataContext())
            {
                var svm = new AdminViewModel()
                {
                };
                loadViewModel(svm);
                return View(_db.Suppliers.OrderBy(x => x.Name).ToList());
            }
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            var svm = new AdminViewModel()
            {
            };
            loadViewModel(svm);
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Supplier>(o => o.SupplierContacts);
                options.LoadWith<Supplier>(o => o.Tag_Suppliers);
                options.LoadWith<Tag_Supplier>(o => o.Tag);
                _db.LoadOptions = options;
                ViewBag.Tags = _contentRep.listAllTags().Where(t => t.Type == Models.Tag.TypeOption.BRAND_SECTION).OrderBy(x => x.name).ToList();
                return View(id.HasValue ? _db.Suppliers.Single(x => x.ID==id) : new Supplier());
            }
        }

        [HttpPost]
        public ActionResult Edit(int? id, Supplier model)
        {
            using (var _db = new dbDataContext())
            {
                ViewBag.Tags = _contentRep.listAllTags().Where(t => t.Type == Models.Tag.TypeOption.BRAND_SECTION).OrderBy(x => x.name).ToList();
                model.tags = Request["TagIDs"] == null ? new List<Models.Tag>() : Request["TagIDs"].Split(',').Select(s => new Models.Tag() { name = ((List<Models.Tag>)ViewBag.tags).Where(tg => tg.ID == int.Parse(s)).Single().name, ID = int.Parse(s) }).ToList();
                if (ModelState.IsValid)
                {
                    if (id == 0)
                    {
                        model.CreateDate = DateTime.Now;
                        model.Contacts.ForEach(x => x.CreateDate = DateTime.Now);
                        model.SupplierContacts.AddRange(model.Contacts);
                        _db.Suppliers.InsertOnSubmit(model);
                    }
                    else
                    {
                        var existing = _db.Suppliers.Single(x => x.ID == model.ID);
                        existing.Name = model.Name;
                        existing.AccountNo = model.AccountNo;
                        existing.Phone1 = model.Phone1;
                        existing.Phone2 = model.Phone2;
                        existing.Email = model.Email;
                        existing.Website = model.Website;
                        existing.Address = model.Address;
                        existing.Discount = model.Discount;
                        existing.SpecialDeals = model.SpecialDeals;
                        existing.Brands = model.Brands;
                        existing.Terms = model.Terms;
                        existing.Notes = model.Notes;

                        existing.Contacts.Where(x => !model.Contacts.Any(y => y.ID == x.ID)).ToList().ForEach(x => _db.SupplierContacts.DeleteOnSubmit(x));
                        existing.Contacts.Where(x => model.Contacts.Any(y => y.ID == x.ID)).ToList().ForEach(x => {
                            var existingContact = model.Contacts.Single(y => y.ID == x.ID);
                            x.Name = existingContact.Name;
                            x.Phone = existingContact.Phone;
                            x.Email = existingContact.Email;
                            x.Position = existingContact.Position;
                        });
                        model.Contacts.Where(x => !existing.Contacts.Any(y => y.ID == x.ID)).ToList().ForEach(x => {
                            x.CreateDate = DateTime.Now;
                            x.SupplierID = model.ID;
                            _db.SupplierContacts.InsertOnSubmit(x);
                        });
                    }
                    _db.SubmitChanges();


                    var delTags = from o in _db.Tag_Suppliers
                                  where o.SupplierID == model.ID
                                  select o;
                    _db.Tag_Suppliers.DeleteAllOnSubmit(delTags);
                    _db.SubmitChanges();

                    if (model.tags != null && model.tags.Count() > 0)
                    {
                        List<Tag_Supplier> tags = model.tags.Select(t => new Tag_Supplier() { SupplierID = model.ID, TagID = t.ID }).ToList();
                        _db.Tag_Suppliers.InsertAllOnSubmit(tags);
                    }

                    _db.SubmitChanges();
                    return RedirectToAction("Edit", new { id=model.ID, m="s" });
                }
                string message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Delete(int? id)
        {
            using (var _db = new dbDataContext())
            {
                return View(_db.Suppliers.Single(x => x.ID==id));
            }
        }

        [HttpPost]
        public ActionResult Delete(int? id, Supplier model)
        {
            
            using (var _db = new dbDataContext())
            {
                var pocount = _db.PurchaseOrderItems.Where(x => x.SupplierID == id).Count();
                if (pocount > 5)
                    return Content("There are " + pocount + " purchase orders connected, over 5 disables deletion");

                _db.PurchaseOrderItems.Where(x => x.SupplierID == id).ToList().ForEach(x => x.SupplierID = null);
                _db.SupplierContacts.DeleteAllOnSubmit(_db.SupplierContacts.Where(x => x.SupplierID == id));
                _db.Tag_Suppliers.DeleteAllOnSubmit(_db.Tag_Suppliers.Where(x => x.SupplierID == id));
                _db.Suppliers.DeleteOnSubmit(_db.Suppliers.Single(x => x.ID == id));
                _db.SubmitChanges();
                return RedirectToAction("Index", new { id="", m="d" });
            }
        }
    }
}