﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class TagAdminController : AdminSiteController
    {
         

        // GET: Category
        public ActionResult Index(int? id, AdminViewModel.ContentFilter? filter, bool productcounts=false)
        {


            ViewBag.tags = _contentRep.listAllTags().ToList();

            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                tags = _contentRep.listAllTags().Where(t => (!id.HasValue && t.Type == Models.Tag.TypeOption.SUPER_SECTION) || (id.HasValue && t.ParentID==id)).ToList()
            };

            if (id.HasValue)
            { 
                vm.tag = _contentRep.listAllTags().Single(x => x.ID == id);
                if(vm.tag.Type == Models.Tag.TypeOption.CAR_PRESCRIPTION_CAT || vm.tag.ID == 2792 || vm.tag.ParentID == 37)///then load the products
                {
                    vm.tag.products = _productRep.listProductsForTagNotIncludingParents(vm.tag.ID, false, true).ToList();
                    vm.brands = _contentRep.listAllTags().Where(x => x.Type == Models.Tag.TypeOption.CAR_PRESCRIPTION_CAT && !x.isHidden).ToList();
                } else

                {
                    vm.tag.products = new List<Product>();
                    vm.brands = new List<Tag>();
                }
            }
            if (filter.HasValue)
            {
                vm.filter = filter;
                switch (filter)
                {
                    case AdminViewModel.ContentFilter.With_Meta:
                        vm.tags = vm.tags.Where(x => !string.IsNullOrWhiteSpace(x.metaDescription)).ToList();
                        break;
                    case AdminViewModel.ContentFilter.Without_Meta:
                        vm.tags = vm.tags.Where(x => string.IsNullOrWhiteSpace(x.metaDescription)).ToList();
                        break;
                    case AdminViewModel.ContentFilter.With_Content:
                        vm.tags = vm.tags.Where(x => !string.IsNullOrWhiteSpace(x.html)).ToList();
                        break;
                    case AdminViewModel.ContentFilter.Without_Content:
                        vm.tags = vm.tags.Where(x => string.IsNullOrWhiteSpace(x.html)).ToList();
                        break;
                }
            }

            loadViewModel(vm);
            if(productcounts)
                vm.tags.ForEach(x => x.noOfLiveProducts = _productRep.listProductsForTag(x.ID, false, true).Count(y => !y.isHidden && y.HasVariant));

            return View(vm);
        }

        public ActionResult BulkActionGroups(int? id)
        {
            runExpire();

            AdminViewModel vm = new AdminViewModel()
            {
                tags = _contentRep.listAllTags().Where(t => t.ParentID == null && !t.SystemTag.HasValue && t.Type == Models.Tag.TypeOption.ADHOC_PRODUCT_GROUP).ToList()
            };

            loadViewModel(vm);

            vm.tags.ForEach(x => x.noOfLiveProducts = _productRep.listProductsForTag(x.ID, false, true).Count(y => !y.isHidden && y.HasVariant));

            return View(vm);
        }

        // GET: Category

        public ActionResult AssociateProduct(int ProductID, int TagID, int Rank)
        {
            DeleteAssociatedProduct(ProductID, TagID);///to ensure no duplciates
            _productRep.saveTagRelationShip(ProductID, TagID, Rank);
            return Content("added....");
        }

        public ActionResult DeleteAssociatedProduct(int ProductID, int TagID)
        {
            _productRep.deleteTagRelationShip(ProductID, TagID);
            return Content("removed....");
        }

        public ActionResult ReplicateStucture(int RecipientTag, int SourceTag)
        {
            _contentRep.ReplicateStucture(RecipientTag, SourceTag);
            return Content("removed....");
        }

        public ActionResult MoveProducts(int RecipientTag, int SourceTag, int[] Productids)
        {
            foreach(int productid in Productids)
            {

                int rank = _productRep.getProductTagRank(productid, SourceTag);
                DeleteAssociatedProduct(productid, SourceTag);///remove old ref
                _productRep.saveTagRelationShip(productid, RecipientTag,rank);///create new one

            }
            return Content("transfer complete....");
        }
        public ActionResult Relations(int? id)
        {
            runExpire();
            if (!id.HasValue)
                throw new Exception("Must have a tagid");

            AdminViewModel vm = new AdminViewModel()
            {
                tag = _contentRep.listAllTags().Single(x => x.ID == id),
                tags = _contentRep.listAllTagRelations((int)id,false).ToList()
            };

            vm.brands = _contentRep.listAllTags().Where(x => x.Type == Models.Tag.TypeOption.BRAND_SECTION).ToList();
            vm.TagID = vm.tag.ID;

            loadViewModel(vm);

            return View(vm);
        }

        public ActionResult AddRelation(int tagid, int relatedtagid)
        {
            _contentRep.saveTagRelation(tagid, relatedtagid);

            return RedirectToAction("Relations", new { id = tagid });
        }

        public ActionResult DeleteRelation(int tagid, int relatedtagid)
        {
            _contentRep.deleteTagRelation(tagid, relatedtagid);

            return RedirectToAction("Relations", new { id = tagid });
        }

        [HttpGet]
        public ActionResult Tag(int? id, int? pid)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                tag = id.HasValue ? _contentRep.listAllTags().Where(p => p.ID == id).Single() : new Tag()
                {
                    ParentID = pid
                },
            };
            loadViewModel(vm);
           

            tagSrSetrup(vm);
            loadSrs(vm.srOwner, true);

            return View(vm);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Tag(int? id, Tag tag)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                tag = tag
            };
            ViewBag.tags = _contentRep.listAllTags();
            tagSrSetrup(vm);
            
            if (ModelState.IsValid)
            {
                if (tag.ID == 0)
                {
                    if (tag.ParentID.HasValue)
                    {
                        tag.Type = _contentRep.listAllTags().Single(x => x.ID == tag.ParentID).getChildType();
                    }
                    else { 
                        tag.Type = Models.Tag.TypeOption.SUPER_SECTION;
                    }
                }

                _contentRep.saveTag(tag);

                collectImagePosts(tag);
                saveExistingImages(tag);
                if (!ModelState.IsValid)
                {
                    loadSrs(vm.srOwner, true);
                    loadViewModel(vm);
                    return View(vm);
                }
                if (Request["showedit"] == "true")
                {
                    return RedirectToRoute("TagAdminTag", new { id = tag.ID, m = "s" });
                }
                else 
                {
                    if (tag.ParentID == null && !tag.SystemTag.HasValue && tag.Type == Models.Tag.TypeOption.ADHOC_PRODUCT_GROUP)
                        return RedirectToRoute("TagAdminBulkActionGroups", new { id = tag.ParentID });
                    else
                        return RedirectToRoute("TagAdminTag", new { id = tag.ID, m = "s" });
                        ///return RedirectToRoute("TagAdminIndex", new { id = tag.ParentID });
                }
            }
            else
            {
                vm.message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));

                loadViewModel(vm);
                return View(vm);
            }
        }

        [HttpGet]
        public ActionResult TagDelete(int? id)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                tag = _contentRep.listAllTags().Where(p => p.ID == id).Single()
            };

            return View(vm);
        }


        [HttpPost]
        public ActionResult TagDelete(int? id, Page inPage)
        {
            runExpire();

            var tag = _contentRep.listAllTags().Where(p => p.ID == id).Single();
            if (tag.SystemTag.HasValue)
                throw new Exception("Category is a system category and cannot be deleted");

            _contentRep.deleteTag(new Tag { ID = (int)id });
            if (tag.ParentID == null && !tag.SystemTag.HasValue && tag.Type == Models.Tag.TypeOption.ADHOC_PRODUCT_GROUP)
                return RedirectToRoute("TagAdminBulkActionGroups", new { id = "" });
            else
                return RedirectToRoute("TagAdminIndex", new { id = "" });
        }
    }
}