﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class ConfigController : AdminController
    {
        // GET: Config
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult StatusManager()
        {
            runExpire();
            loadAdminViewModel();
            return View(_orderRep.listAllStatus().ToList());
        }

        [HttpGet]
        public ActionResult Status(int? id)
        {
            runExpire();
            loadAdminViewModel();
            return View(_orderRep.listAllStatus().Single(s => s.ID == (int)id));
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Status(int? id, Status status)
        {
            runExpire();
            if (ModelState.IsValid)
            {
                _orderRep.saveStatus(status);
                return RedirectToRoute("ConfigStatusManager", new { id = "" });
            }
            else
            {
                loadAdminViewModel();
                return View(status);
            }
        }
    }
}