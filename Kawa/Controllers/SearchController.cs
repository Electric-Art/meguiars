﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using AutoMapper;
using Kawa.Models.Extensions;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Xml;
using System.Text;
using System.Xml.Linq;
using Kawa.Models.Services;
using WebMarkupMin.AspNet4.Mvc;

namespace Kawa.Controllers
{
    [MinifyHtml]
    public class SearchController : SiteController
    {

        // GET: Search
        [HttpGet]
        public ActionResult Index(string search, int? page, int? pagesize,
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] PriceIDs,
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] BrandIDs,
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] SpecialtiesIDs)
        {
            loadViewModel(new SiteViewModel());

            var postedResultFilters = new PostedResultFilters()
            {
                PriceIDs = PriceIDs,
                BrandIDs = BrandIDs,
                SpecialtiesIDs = SpecialtiesIDs,
                search = search
            };

            var vm = new SearchResultsViewModel();
            var tokens = search.Split(' ').ToList();
            //vm.pages = _contentRep.listMatches(search).OrderByDescending(p => p.rankNo).ToList();
            var products = _productRep.listMatches(search, null).Where(p => !p.isHidden && p.HasVariant).OrderBy(x => x.getRank(search, tokens)).ThenBy(x => x.name).ToList();



            ViewBag.Search = !string.IsNullOrEmpty(postedResultFilters.search) ? search : "";
            setupProductList(vm, products, page, 64, postedResultFilters);
            vm.galleries = LMCDataService.searchgalleries(search);

            

            vm.pages = _contentRep.listAllPages().Where(x => x.Type == Page.TypeOption.FAQ).Where(x => x.name.Contains(search) || x.html.Contains(search)).ToList();
            vm.news = LMCDataService.searchnews(search);

            return View(vm);
        }

        [HttpPost]
        public ActionResult Index(string search, int? page, int? pagesize, PostedResultFilters postedResultFilters)
        {
            loadViewModel(new SiteViewModel());
            var vm = new SearchResultsViewModel();

            if (!string.IsNullOrEmpty(search))
                postedResultFilters.search = search;
            else
                search = postedResultFilters.search;
            //vm.pages = _contentRep.listMatches(search).OrderByDescending(p => p.rankNo).ToList();
            var tokens = search.Split(' ').ToList();
            var products = _productRep.listMatches(search, null).Where(p => !p.isHidden && p.HasVariant).OrderBy(x => x.getRank(search, tokens)).ThenBy(x => x.name).ToList();

            ViewBag.Search = !string.IsNullOrEmpty(postedResultFilters.search) ? search : "";
            setupProductList(vm, products, page, 64, postedResultFilters);

            return View(vm);
        }

        /* Json */

        public async Task<JsonResult> SearchSuggest(string searchstr)
        {
            runExpire();

            try {

                /// return Json(await SearchaniseService.SearchSearchanise(searchstr));

            }
            catch { }

            var tokens = searchstr.Split(' ').ToList();
            var products = _productRep.listMatches(searchstr, 8).OrderBy(x => x.getRank(searchstr, tokens)).ThenBy(x => x.name).ToList();

            var tags = _contentRep.listAllTagsWithProductList().Where(x => !x.isHidden && x.name.Contains(searchstr)
                ).ToList().OrderBy(x => x.getRank(searchstr, tokens)).ThenBy(x => x.name).Take(16).ToList();

            products.ForEach(x => {
                //x.siteResources = _srRep.listAll(x).Where(y => y.SiteResourceType == SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE).ToArray();
                SiteResourceService.loadUrls(x, SiteResource.SiteResourceVersion.ITEM_SEARCH);
            });

            return Json(new SuggestionResult()
            {
                products_consumer = Mapper.Map<List<Product>, List<Models.Coms.Product>>(products.Where(x => x.brandtags.FirstOrDefault().name == "MEGUIARS CONSUMER").ToList()),
                products_pro = Mapper.Map<List<Product>, List<Models.Coms.Product>>(products.Where(x => x.brandtags.FirstOrDefault().name == "MEGUIARS PROFESSIONAL").ToList()),
                pages = _contentRep.listAllPages().Where(x => x.Type == Page.TypeOption.FAQ).Where(x => x.name.Contains(searchstr) || x.html.Contains(searchstr)).ToList(),
                news = LMCDataService.searchnews(searchstr),
                tags = Mapper.Map<List<Tag>, List<Models.Coms.Tag>>(tags),
                galleries = LMCDataService.searchgalleries(searchstr)

            });
           

     }


     public async Task<ActionResult> SearchSuggestTest(string searchstr)
     {
         return Json(await SearchaniseService.SearchSearchanise(searchstr));
     }

     public JsonResult SaveStat(Models.Data.Stat stat)
     {
         runExpire();
         if(stat.ProductID == null && stat.TagID == null)
             return Json(new { success = false });
         CacheRepository _cRep = new CacheRepository();
         stat.CreateDate = DateTime.Now;
         _cRep.saveStat(stat);
         return Json(new { success = true });
     }

     /*
     public async Task<ActionResult> SearchUpdate()
     {
         return Content(await SearchaniseService.UploadToSearchanise());
     }
     */
        }
    }