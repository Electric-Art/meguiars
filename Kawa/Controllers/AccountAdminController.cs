﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Mail;
using System.Configuration;
using System.Collections.Generic;
using PagedList;
using AutoMapper;
using System.ComponentModel;
using System.Data.Linq;

namespace Kawa.Controllers
{
    public class AccountAdminController : AdminSiteController
    {
        private AccountService _accountService;

        public AccountService AccountService
        {
            get
            {
                if (_accountService == null)
                {
                    _accountService = new AccountService();
                }
                return _accountService;
            }
            protected set
            {
                _accountService = value;
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangeDetailsSuccess,
            ChangePasswordSuccess,
            ChangeEmailSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }


        // GET: AccountAdmin
        // GET: ProductAdmin
        [HttpGet]
        public ActionResult Index(int? id, int? page, AccountFilterViewModels filterModel)
        {

            loadViewModel(new AdminViewModel());
            return View("Index", filterModel);
        }

        private List<string> SaveToCsv<T>(List<T> reportData)
        {
            var lines = new List<string>();
            List<string> exclude = new List<string>() { "aFirstName", "aLastName", "aEmail" , "isAdmin", "isTrade", "isBlocked", "creditCardToken", "saveCreditCard" };
            IEnumerable<PropertyDescriptor> props = TypeDescriptor.GetProperties(typeof(T)).OfType<PropertyDescriptor>();
            var header = string.Join(",", props.ToList().Where(x=> !exclude.Contains(x.Name)).Select(x => x.Name));
            lines.Add(header);
            var valueLines = reportData.Select(row => string.Join(",", header.Split(',').Select(a => row.GetType().GetProperty(a).GetValue(row, null))));
            lines.AddRange(valueLines);
            return lines;
        }
       


        public ActionResult Export(DateTime? Startdate, DateTime? EndDate, string channel)
        {

            runExpire();
            AdminViewModel vm = new AdminViewModel();
            var csv = new System.Text.StringBuilder();

            using (var _db = new Models.Data.dbDataContext())
            {

                ///var options = new DataLoadOptions();

                ///options.LoadWith<Order>(o => o.AccountID);

                ///options.LoadWith<AccountDetail>(o => o.AccountID);

                List<Models.Data.AccountDetail> ads = _db.AccountDetails.ToList();

                List<Models.Data.Order> ords = _db.Orders.ToList();

                List<Account> accounts = _personRep.listAllAccounts().ToList();

                if (Startdate.HasValue)
                {
                    accounts = accounts.Where(x => x.createDate > Startdate).ToList();
                }
                if (EndDate.HasValue)
                {
                    accounts = accounts.Where(x => x.createDate < EndDate).ToList();
                }

                if (channel != "")
                {
                    accounts = accounts.Where(x => x.channel == channel).ToList();
                }


                accounts.ForEach(x=> {

                    List<Models.Data.Order> orders = ords.Where(o => o.AccountID == x.ID && o.StatusID > 0).ToList();
                    Models.Data.Order lastorder = orders.OrderByDescending(o=>o.ID).FirstOrDefault();

                        if(lastorder!=null)
                        { 
                            Models.Data.AccountDetail ad = ads.Where(a => a.OrderID == lastorder.ID && a.Type == 0).FirstOrDefault();
                            if (ad != null)
                            {
                                x.Address1 = ad.Address1;
                                x.Address2 = ad.Address2;
                                x.Address3 = ad.Address3;
                                x.Address4 = ad.Address4;
                                x.PostCode = ad.Postcode;
                                x.Mobile = ad.Mobile;
                                x.isMailer = ad.isMailer;
                                x.LastOrderID = lastorder.ID;
                                x.orderCount = orders.Count();
                                x.OrdersTotal = orders.Sum(oo => oo.OrderTotal);
                            }
                        }
                });
               

                var lines = SaveToCsv(accounts);



                foreach (string s in lines)
                {
                    csv.AppendLine(s);
                }


                string fileName = String.Format("Accounts-{0}.csv", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                Response.ContentType = "text/csv";
                Response.AddHeader("content-disposition", "filename=" + fileName);

                // write string data to Response.OutputStream here
                Response.Write(csv.ToString());

                Response.End();
                return View("Accounts", vm);
            }


        }
        public JsonResult getAccountList(int? page, AccountFilterViewModels filterModel)
        {
            runExpire();
            runAccountList(page, filterModel);
            return Json(new
            {
                list = filterModel.accounts != null ? Mapper.Map<List<IAdminAccountViewModel>, List<AdminAccountViewModel>>(filterModel.accounts.ToList()) : null,
                count = filterModel.accounts != null ? filterModel.accounts.TotalItemCount : 0
            });
        }

        private void runAccountList(int? page, AccountFilterViewModels filterModel)
        {
            runExpire();
            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)

            if (!((filterModel.searchFirstName != null && filterModel.searchFirstName.Length > 3) || (filterModel.searchLastName != null && filterModel.searchLastName.Length > 3) || (filterModel.searchEmail != null && filterModel.searchEmail.Length > 3)))
            {


                filterModel.accounts = _personRep.listAllAccounts().OrderByDescending(x => x.ID).Take(50).ToPagedList(pageNumber, filterModel.pageSize);
                return;
            }

            IQueryable<Account> accounts = _personRep.listAllAccounts();
            if (!string.IsNullOrEmpty(filterModel.searchFirstName))
                accounts = accounts.Where(x => x.firstName.Contains(filterModel.searchFirstName));
            if (!string.IsNullOrEmpty(filterModel.searchLastName))
                accounts = accounts.Where(x => x.lastName.Contains(filterModel.searchLastName));
            if (!string.IsNullOrEmpty(filterModel.searchEmail))
                accounts = accounts.Where(x => x.UserName.Contains(filterModel.searchEmail));
            if (filterModel.last50)
                accounts = accounts.OrderByDescending(x => x.ID).Take(50);

            var accountResults = accounts.Select(x => (IAdminAccountViewModel)x).ToList();

            IQueryable<AccountDetail> accountDetails = _orderRep.listAllAccountDetailForAccountAdmin();
            if (!string.IsNullOrEmpty(filterModel.searchFirstName))
                accountDetails = accountDetails.Where(x => x.FirstName.Contains(filterModel.searchFirstName));
            if (!string.IsNullOrEmpty(filterModel.searchLastName))
                accountDetails = accountDetails.Where(x => x.Surname.Contains(filterModel.searchLastName));
            if (!string.IsNullOrEmpty(filterModel.searchEmail))
                accountDetails = accountDetails.Where(x => x.Email.Contains(filterModel.searchEmail));

            var accountDetailsList = accountDetails.OrderByDescending(x => x.OrderID).ToList();

            List<AccountDetail> distinctPeople = accountDetailsList
              .GroupBy(p => p.Email)
              .Select(g => g.First())
              .ToList();

            accountResults.AddRange(distinctPeople);

            filterModel.accounts = accountResults.OrderBy(x => x.getRank()).ThenBy(x => x.aLastName).ToPagedList(pageNumber, filterModel.pageSize);
        }

        public ActionResult Account(int? id, ManageMessageId? message)
        {
            runExpire();
            loadViewModel(new AdminViewModel());

            ViewBag.StatusMessage =
                message == ManageMessageId.ChangeDetailsSuccess ? "Account details have been changed."
               : message == ManageMessageId.ChangePasswordSuccess ? "Account password has been changed."
               : message == ManageMessageId.ChangeEmailSuccess ? "Account email has been changed."
               : message == ManageMessageId.SetPasswordSuccess ? "Account password has been set."
               : message == ManageMessageId.SetTwoFactorSuccess ? "Account two-factor authentication provider has been set."
               : message == ManageMessageId.Error ? "An error has occurred."
               : message == ManageMessageId.AddPhoneSuccess ? "Account phone number was added."
               : message == ManageMessageId.RemovePhoneSuccess ? "Account phone number was removed."
               : "";

            var vm = new AccountAdminViewModel()
            {
                Account = _personRep.listAllAccounts().Single(x => x.ID == id),
                Orders = _orderRep.listAllOrdersWithTrans().Where(x => x.AccountID == id && x.SystemStatus > Order.SystemStatusOption.IN_PROGRESS).OrderByDescending(x => x.ID).ToList()
            };

            vm.Orders.ForEach(x => OrderService.loadOrderWithFullDomain(x));

            return View(vm);
        }

        //
        // GET: /Manage/ChangeEmail
        public ActionResult ChangeEmail(int id)
        {
            loadViewModel(new AdminViewModel());
            ViewBag.Account = _personRep.listAllAccounts().Single(x => x.ID == id);

            return View();
        }

        //
        // POST: /Manage/ChangeEmail
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangeEmail(int id, ChangeEmailViewModel model)
        {
            loadViewModel(new AdminViewModel());
            var account = _personRep.listAllAccounts().Single(x => x.ID == id);
            ViewBag.Account = account;

            if (_personRep.listAllAccounts().Any(x => x.UserName.ToLower() == model.NewEmailAddress.ToLower()))
            {
                ModelState.AddModelError("", "Sorry, an account with this email already exists.");
            }

            if (ModelState.IsValid)
            {
                var oldid = await AccountService.UserManager.FindByNameAsync(account.UserName);
                // var resulttestpass = await SignInManager.PasswordSignInAsync(getAccount().UserName, model.Password, false, shouldLockout: false);

                var user = new ApplicationUser { UserName = model.NewEmailAddress, Email = model.NewEmailAddress };
                var result = await AccountService.UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await AccountService.deleteAccountAsync(oldid.Id);
                    account.UserName = model.NewEmailAddress;
                    _personRep.saveAccount(account);

                    return RedirectToAction("Account", new { id = account.ID, Message = ManageMessageId.ChangeEmailSuccess });
                }
                AddErrors(result);

            }

            return View(model);
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword(int id)
        {
            loadViewModel(new AdminViewModel());

            var account = _personRep.listAllAccounts().Single(x => x.ID == id);
            ViewBag.Account = account;

            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(int id, ChangePasswordAdminViewModel model)
        {
            loadViewModel(new AdminViewModel());

            var account = _personRep.listAllAccounts().Single(x => x.ID == id);
            ViewBag.Account = account;

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await AccountService.UserManager.FindByNameAsync(account.UserName);

            

            if (user == null)
            {
                var reguser = new ApplicationUser { UserName = account.UserName, Email = account.UserName };
                var regresult = await AccountService.UserManager.CreateAsync(reguser);
                if (regresult.Succeeded)
                {
                    user = reguser;
                }
                else
                {
                    ModelState.AddModelError("", "Attempted auto-porting of account failed..");
                    AddErrors(regresult);
                    return View(model);
                }
            }

            string code = await AccountService.UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var result = await AccountService.UserManager.ResetPasswordAsync(user.Id, code, model.NewPassword);
            if (result.Succeeded)
            {
                return RedirectToAction("Account", new { id = account.ID, Message = ManageMessageId.ChangePasswordSuccess });
            }
            ModelState.AddModelError("", "Hard reset of password failed..");
            AddErrors(result);
            return View(model);
        }


        #region Util

        public ActionResult TestLists(int? id, int? page)
        {
            runExpire();

            var list = MailChimpService.GetList();

            return Content("done");
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                if (!error.StartsWith("Name"))
                    ModelState.AddModelError("", error);
            }
        }


        #endregion
    }
}