﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Services;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class TradeAdminController : AdminSiteController
    {
        // GET: TradeAdmin
        [HttpGet]
        public ActionResult Home()
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = _contentRep.listAllPages().Single(p => p.SystemPage == Page.SystemPageOption.TRADE_HOME),
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.PRODUCT_SECTION,
                formRoute = "TradeAdminHome",
                pageTitle = "Edit the Trade Area Home Page"
            };
            
            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminGet(vm, null, null, "~/Views/ArticleAdmin/PageSimple.cshtml");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Home(int? id, int? t, Page page)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = page,
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.PRODUCT_SECTION,
                pages = _contentRep.listAllPages().ToList(),
                formRoute = "TradeAdminHome",
                pageTitle = "Edit the Trade Area Home Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminPost(vm, page, "TradeAdminHome", new AdminPageRouteVariables()
            {
                id = page.ID,
                m = "s",
                t = Request["t"],
                injectID = true
            }, "~/Views/ArticleAdmin/PageSimple.cshtml");
            

        }

        [HttpGet]
        public ActionResult PriceSheets()
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = _contentRep.listAllPages().Single(p => p.SystemPage == Page.SystemPageOption.TRADE_PRICE_SHEETS),
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.SECTION_GROUP,
                formRoute = "TradeAdminPriceSheets",
                pageTitle = "Edit the Price Sheets Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminGet(vm, null, null, "~/Views/ArticleAdmin/PageSimple.cshtml");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PriceSheets(int? id, int? t, Page page)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = page,
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.SECTION_GROUP,
                pages = _contentRep.listAllPages().ToList(),
                formRoute = "TradeAdminPriceSheets",
                pageTitle = "Edit the Price Sheets Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminPost(vm, page, "TradeAdminPriceSheets", new AdminPageRouteVariables()
            {
                id = page.ID,
                m = "s",
                t = Request["t"],
                injectID = true
            }, "~/Views/ArticleAdmin/PageSimple.cshtml");


        }

        [HttpGet]
        public ActionResult TrainingModules()
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = _contentRep.listAllPages().Single(p => p.SystemPage == Page.SystemPageOption.TRADE_MODULES),
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.SECTION_GROUP,
                formRoute = "TradeAdminTrainingModules",
                pageTitle = "Edit the Training Modules & Online Quizzes Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminGet(vm, null, null, "~/Views/ArticleAdmin/PageSimple.cshtml");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TrainingModules(int? id, int? t, Page page)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = page,
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.SECTION_GROUP,
                pages = _contentRep.listAllPages().ToList(),
                formRoute = "TradeAdminTrainingModules",
                pageTitle = "Edit the Training Modules & Online Quizzes Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminPost(vm, page, "TradeAdminTrainingModules", new AdminPageRouteVariables()
            {
                id = page.ID,
                m = "s",
                t = Request["t"],
                injectID = true
            }, "~/Views/ArticleAdmin/PageSimple.cshtml");


        }
        
        [HttpGet]
        public ActionResult AdministrationForms()
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = _contentRep.listAllPages().Single(p => p.SystemPage == Page.SystemPageOption.TRADE_ADMINISTRATION_FORMS),
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.SECTION_GROUP,
                formRoute = "TradeAdminAdministrationForms",
                pageTitle = "Edit the Administration Forms Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminGet(vm, null, null, "~/Views/ArticleAdmin/PageSimple.cshtml");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AdministrationForms(int? id, int? t, Page page)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = page,
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.SECTION_GROUP,
                pages = _contentRep.listAllPages().ToList(),
                formRoute = "TradeAdminAdministrationForms",
                pageTitle = "Edit the Administration Forms Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminPost(vm, page, "TradeAdminAdministrationForms", new AdminPageRouteVariables()
            {
                id = page.ID,
                m = "s",
                t = Request["t"],
                injectID = true
            }, "~/Views/ArticleAdmin/PageSimple.cshtml");


        }

        [HttpGet]
        public ActionResult BrandAssets()
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = _contentRep.listAllPages().Single(p => p.SystemPage == Page.SystemPageOption.TRADE_BRAND_ASSETS),
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.SECTION_GROUP,
                formRoute = "TradeAdminBrandAssets",
                pageTitle = "Edit the Brand Assets Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminGet(vm, null, null, "~/Views/ArticleAdmin/PageSimple.cshtml");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult BrandAssets(int? id, int? t, Page page)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = page,
                tags = _contentRep.listAllTags().ToList(),
                tagtype = Tag.TypeOption.SECTION_GROUP,
                //pages = _contentRep.listAllPages().ToList(),
                formRoute = "TradeAdminBrandAssets",
                pageTitle = "Edit the Brand Assets Page"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminPost(vm, page, "TradeAdminBrandAssets", new AdminPageRouteVariables()
            {
                id = page.ID,
                m = "s",
                t = Request["t"],
                injectID = true
            }, "~/Views/ArticleAdmin/PageSimple.cshtml");

        }
    }
}