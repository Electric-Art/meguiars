﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using WebMarkupMin.AspNet4.Mvc;

namespace Kawa.Controllers
{
    [MinifyHtml]
    public class AboutController : SiteController
    {
        // GET: About
        public ActionResult Index(string id)
        {
            loadViewModel(new SiteViewModel());

            var vm = new ArticlePageViewModel()
            {
                page = _contentRep.listAllPages(true).Single(p => p.SystemPage == Page.getSystemPageByUrl(id))
            };
            SiteResourceService.loadUrls(vm.page.siteResources);

            return View("../Articles/Article",vm);
        }
    }
}