﻿using Kawa.Models.Data;
using Kawa.Models.Extensions;
using Kawa.Models.Services;
using Kawa.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMarkupMin.AspNet4.Mvc;

namespace Kawa.Controllers
{

    [MinifyHtml]
    public class EventsController : SiteController
    {
        // GET: News
        public ActionResult Index(int? id)
        {
            loadViewModel(new SiteViewModel());
            using (var _db = new dbDataContext())
            {
                var model = LMCDataService.LoadEvents(System.DateTime.Now);

                model.PageHeading = "Events";


                return View(model);
            }
        }
        public ActionResult Event(int id)
        {
            using (var _db = new dbDataContext())
            {

                var model = LMCDataService.LoadEvents(System.DateTime.Now);

                model.Item = LMCDataService.LoadEvent(id);

                return View("Index", model);
            }
        }
    }
}