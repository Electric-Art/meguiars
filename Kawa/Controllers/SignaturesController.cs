﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;

namespace Kawa.Controllers
{
    public class SignaturesController : AdminSiteController
    {
        // GET: SignatureAdmin
        public ActionResult Index(SignatureMessage? msg)
        {
            loadViewModel(new AdminViewModel());
            ViewBag.Countries = _shipRep.listAllCountries().ToList();
            switch (msg)
            {
                case SignatureMessage.SignatureSaved:
                    ViewBag.Message = "Your signature has been saved";
                    break;
                case SignatureMessage.SignatureDeleted:
                    ViewBag.Message = "Your signature has been deleted";
                    break;
            }
            return View(_orderRep.listAllSignatures().OrderBy(x => x.ID).ToList());
        }

        public ActionResult Signature(int? id)
        {
            loadViewModel(new AdminViewModel());
            return View(id.HasValue ? _orderRep.listAllSignatures().Single(x => x.ID==id) : new Signature());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Signature(int? id, Signature model)
        {
            loadViewModel(new AdminViewModel());
            if (ModelState.IsValid)
            {
                var isNew = model.ID == 0;
                _orderRep.saveSignature(model);
                return RedirectToAction("Index", new { msg = SignatureMessage.SignatureSaved });
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult SignatureDelete(int? id)
        {
            _orderRep.deleteSignature(new Signature() { ID = (int)id });
            return RedirectToAction("Index", new { msg = SignatureMessage.SignatureDeleted });
        }

        public enum SignatureMessage
        {
            SignatureSaved,
            SignatureDeleted
        }
    }
}