﻿using Kawa.Models;
using Kawa.Models.Extensions;
using Kawa.Models.Services;
using Kawa.PropellaWebService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Kawa.Controllers
{
    public class PropellaAdminController : AdminSiteController
    {

        public ActionResult Index()
        {
            return View();
        }


        public class ProcessResult
        {
            public bool Success { get; set; }
            public int Status { get; set; }
            public string Message { get; set; }
            public string Issue { get; set; }
        }
        [HttpPost]
        public ActionResult RunProductsOutOfStockService()
        {
            var substarted = DateTime.Now;
            try
            {
                var report = PropellaService.UpdateOutOfStock();
                return Json(new ProcessResult()
                {
                    Success = true,
                    Message = string.Format("Process took {0} seconds. {1}", DateTime.Now.Subtract(substarted).Seconds, report)
                });
            }
            catch (Exception e)
            {
                return Json(new ProcessResult()
                {
                    Success = false,
                    Message = string.Format(e.Message)
                });
            }
            ///return RunErr(e); }
        }
        [HttpPost]
        public ActionResult RunProductsService(int? days)
        {
            var substarted = DateTime.Now;
            try
            {
                var report = PropellaService.UpdateProducts(days);
                return Json(new ProcessResult()
                {
                    Success = true,
                    Message = string.Format("Process took {0} seconds. {1}", DateTime.Now.Subtract(substarted).Seconds, report)
                });
            }
            catch (Exception e)
            {
                return Json(new ProcessResult()
                {
                    Success = false,
                    Message = string.Format(e.Message)
                });
            }
                ///return RunErr(e); }
        }

        [HttpPost]
        public ActionResult RunSmitsService(int days, string sku)
        {
            var substarted = DateTime.Now;
            try
            {
                var report = PropellaService.suckNEWSmitsData();/// SmitsService.UpdateSmitsData(days, sku);
                return Json(new ProcessResult()
                {
                    Success = true,
                    Message = string.Format("Process took {0} seconds. {1}", DateTime.Now.Subtract(substarted).Seconds, report)
                });
            }
            catch (Exception e) { return RunErr(e); }
        }


        [HttpPost]
        public ActionResult RunSmitsServiceSku(int days, string sku)
        {
            var substarted = DateTime.Now;
            try
            {
                var report = "";/// SmitsService.UpdateSmitsData(1000, sku);
                return Json(new ProcessResult()
                {
                    Success = true,
                    Message = string.Format("Process took {0} seconds. {1}", DateTime.Now.Subtract(substarted).Seconds, report)
                });
            }
            catch (Exception e) { return RunErr(e); }
        }

        [HttpPost]
        public ActionResult RunPromtionsService()
        {
            try
            {
                var substarted = DateTime.Now;
                var report = PropellaService.UpdatePromotions();
                return Json(new ProcessResult()
                {
                    Success = true,
                    Message = string.Format("Process took {0} seconds. {1}", DateTime.Now.Subtract(substarted).Seconds, report)
                });
            }
            catch (Exception e) { return RunErr(e); }
        }

        [HttpPost]
        public ActionResult RunCategoriesService()
        {
            try
            {
                var substarted = DateTime.Now;
                var report = PropellaService.UpdateCategories();
                return Json(new ProcessResult()
                {
                    Success = true,
                    Message = string.Format("Process took {0} seconds. {1}", DateTime.Now.Subtract(substarted).Seconds, report)
                });
            }
            catch (Exception e) { return RunErr(e); }
        }

        private ActionResult RunErr(Exception e)
        {
            return Json(new ProcessResult()
            {
                Success = false,
                Issue = e.Message
            });
        }


    }
}