﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;
using PagedList;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;
using Kawa.Models.Extensions;
using System.Reflection;
using System.ComponentModel;
using FileHelpers;
using AutoMapper;

namespace Kawa.Controllers
{
    public class ProductAdminController : AdminSiteController
    {
        //public static int PAGE_SIZE = 25;

        // GET: ProductAdmin
        [HttpGet]
        public ActionResult Index(int? id, int? page, ProductFilterViewModels filterModel)
        {
            runProductFilterSetup(filterModel);
            loadViewModel(new AdminViewModel());
            return View("Index", filterModel);
        }

        [HttpPost]
        public ActionResult Index(int? id, ProductFilterViewModels filterModel)
        {
            runProductFilterSetup(filterModel);
            loadViewModel(new AdminViewModel());
            return View("Index", filterModel);
        }

        private void runProductFilterSetup(ProductFilterViewModels filterModel)
        {
            filterModel.tags = _contentRep.listAllTags().Where(t => t.Type == Tag.TypeOption.PRODUCT_SECTION || t.Type == Tag.TypeOption.ADHOC_PRODUCT_GROUP || (t.Type == Tag.TypeOption.ADHOC_PRODUCT_TAGS && !t.isHidden) ).ToList().OrderBy(x => x.Type).ThenBy(x => x.name).ToList();
            filterModel.brands = _contentRep.listAllTags().Where(t => t.Type == Tag.TypeOption.BRAND_SECTION).ToList().OrderBy(x => x.name).ToList();
            filterModel.disclaimers = _contentRep.listDisclaimerPages().OrderBy(x => x.orderNo).ToList();
        }

        private void runProductList(int? page, ProductFilterViewModels filterModel)
        {
            runExpire();
            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)

            IQueryable<Product> products = null;

            if (filterModel.TagID.HasValue && filterModel.BrandID.HasValue)
                products = _productRep.listProductsForTagIntersection((int)filterModel.TagID, (int)filterModel.BrandID, false, true);
            else if (filterModel.TagID.HasValue)
                products = _productRep.listProductsForTag((int)filterModel.TagID, false, true);
            else if (filterModel.BrandID.HasValue)
                products = _productRep.listProductsForTag((int)filterModel.BrandID, false, true);
            else
                products = _productRep.listAllProducts(false, true);

            if (!string.IsNullOrEmpty(filterModel.search))
                products = products.Where(x => x.name.Contains(filterModel.search) || x.gtin.Contains(filterModel.search));

            if (!string.IsNullOrEmpty(filterModel.searchKeyword))
                products = products.Where(x => 
                    x.aliases.Contains(filterModel.searchKeyword) ||
                    x.titleName.Contains(filterModel.searchKeyword) ||
                    x.blurb.Contains(filterModel.searchKeyword) ||
                    x.specifications.Contains(filterModel.searchKeyword) ||
                    x.activeIngredients.Contains(filterModel.searchKeyword) ||
                    x.directions.Contains(filterModel.searchKeyword) ||
                    x.warnings.Contains(filterModel.searchKeyword) ||
                    x.freeFrom.Contains(filterModel.searchKeyword) ||
                    x.noAdded.Contains(filterModel.searchKeyword));

            if (filterModel.outOfStock.HasValue)
                products = products.Where(x => x.HasVariantOutOfStock == filterModel.outOfStock);

            if (filterModel.preOrder.HasValue)
                products = products.Where(x => x.HasVariantOutOfStock && x.HasVariantPreOrder == filterModel.preOrder);

            if (filterModel.offline.HasValue)
                products = products.Where(x => x.isHidden == filterModel.offline);

            if (filterModel.googleCatStatus.HasValue)
                products = !(bool)filterModel.googleCatStatus ? products.Where(x => x.googleCat == null || x.googleCat == "") : products.Where(x => !(x.googleCat == null || x.googleCat == ""));

            if (filterModel.bingCatStatus.HasValue)
                products = !(bool)filterModel.bingCatStatus ? products.Where(x => x.bingCat == null || x.bingCat == "") : products.Where(x => !(x.bingCat == null || x.bingCat == ""));

            if (filterModel.hideBlurb.HasValue)
                products = products.Where(x => x.hideBlurb == filterModel.hideBlurb);

            if (filterModel.austL.HasValue)
                products = ((bool)filterModel.austL) ? products.Where(x => x.austL != null && x.austL != "") : products.Where(x => !(x.austL != null && x.austL != ""));

            if (filterModel.stockNoStatus.HasValue)
            {
                switch (filterModel.stockNoStatus)
                {
                    case ProductFilterViewModels.StockNoStatus.Zero:
                        products = products.Where(x => x.VariantStockNo == 0);
                        break;
                    case ProductFilterViewModels.StockNoStatus.More_Than_Zero:
                        products = products.Where(x => x.VariantStockNo > 0);
                        break;
                    case ProductFilterViewModels.StockNoStatus.All:
                        products = products.Where(x => x.VariantStockNo != null);
                        break;
                }
            }

            if (filterModel.quanitytRestriction.HasValue)
            {
                products = filterModel.quanitytRestriction == ProductFilterViewModels.QuantityRestrictionOption.Min ? products.Where(x => x.VariantMinQuantity.HasValue) : products.Where(x => x.VariantMaxQuantity.HasValue);
            }
            List<Product> results = new List<Product>();
            foreach(Product p in products)
            {
                p.variant.priceRRP = p.getBuyPrice();
                results.Add(p);
            }
                filterModel.products = results.ToPagedList(pageNumber, filterModel.csv ? 1000 : filterModel.pageSize);

            
        }

        public ActionResult getProductList(int? page, ProductFilterViewModels filterModel)
        {
            runExpire();
            runProductList(page, filterModel);
            filterModel.products.ToList().ForEach(x => x.blurb = null);
            if (filterModel.csv)
            {
                var csvList = Mapper.Map<List<Product>, List<Models.Csv.SearchProduct>>(filterModel.products.ToList());
                Type type = typeof(Models.Csv.SearchProduct);
                FileHelperEngine engine = new FileHelperEngine(type);
                engine.HeaderText = type.GetCsvHeader();
                var FileStr = engine.WriteString(csvList);
                System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
                var fileName = "SearchProducts";
                fileName += ".csv";
                return File(encoding.GetBytes(FileStr), "application/csv", fileName);
            }
            return Json(new { list = filterModel.products, count = filterModel.products.TotalItemCount });
        }

        public JsonResult saveProductChanges(int? page, List<Product> model)
        {
            runExpire();

            if(ModelState.IsValid)
            {
                foreach(var chgprod in model)
                {
                    Variant variant = null;
                    if (chgprod.variant.ID == 0)
                        variant = new Variant()
                        {
                            ProductID = chgprod.ID,
                            name = chgprod.name,
                            createDate = DateTime.Now,
                            bulkPrices = new List<Price>()
                        };
                    else
                        variant = _productRep.listAllVariants().Single(x => x.ID == chgprod.variant.ID);
                    variant.changeBulkPricing(chgprod.variant.priceRRP, chgprod.variant.price, _productRep);
                    variant.price = chgprod.variant.price;
                    variant.priceRRP = chgprod.variant.priceRRP;
                    variant.modifiedDate = DateTime.Now;
                    _productRep.saveVariant(variant);
                }
                TagService.RunForHome();
                return Json(new { success=true });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
        }

        public JsonResult saveProductGtinUpdates(int? page, List<Product> model)
        {
            runExpire();

            if (ModelState.IsValid)
            {
                foreach (var chgprod in model)
                {
                    var product = _productRep.listAllProducts().Single(x => x.ID == chgprod.ID);
                    product.gtin = chgprod.gtin;
                    _productRep.saveProduct(product,false,false,false);
                }

                return Json(new { success = true });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
        }

        public JsonResult saveProductAustLUpdates(int? page, List<Product> model)
        {
            runExpire();

            if (ModelState.IsValid)
            {
                foreach (var chgprod in model)
                {
                    var product = _productRep.listAllProducts().Single(x => x.ID == chgprod.ID);
                    product.austL = chgprod.austL;
                    _productRep.saveProduct(product, false, false, false);
                }

                return Json(new { success = true });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
        }

        public JsonResult saveProductStockNoLUpdates(int? page, List<Product> model)
        {
            runExpire();

            if (ModelState.IsValid)
            {
                foreach (var chgprod in model)
                {
                    var variant = _productRep.listAllVariants().FirstOrDefault(x => x.ProductID == chgprod.ID);
                    var wasOOS = variant.isHidden;

                    variant.stockNo = chgprod.VariantStockNo;
                    if (wasOOS && chgprod.VariantStockNo.HasValue && chgprod.VariantStockNo > 0 && variant.isHidden)
                        variant.isHidden = false;
                    if(!wasOOS && chgprod.VariantStockNo == 0 && !variant.isHidden)
                        variant.isHidden = true;

                    if (chgprod.VariantStockNo == 0 && variant.preOrderShipDate.HasValue)
                        variant.preOrderShipDate = null;

                    var nowOOS = variant.isHidden;

                    if (wasOOS && !nowOOS)
                    {
                        sendBackInStockNotifications(variant.ProductID);
                    }

                    _productRep.saveVariant(variant);
                }

                return Json(new { success = true });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
        }

        public JsonResult saveProductOrderOverrideUpdates(int? page, List<Product> model)
        {
            runExpire();

            if (ModelState.IsValid)
            {
                foreach (var chgprod in model)
                {
                    var product = _productRep.listAllProducts().Single(x => x.ID == chgprod.ID);
                    product.orderOverride = chgprod.orderOverride;
                    _productRep.saveProduct(product, false, false, false);
                }

                return Json(new { success = true });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
        }

        public JsonResult runBulkUpdate(BulkDiscountViewModel model)
        {
            runExpire();
            if (ModelState.IsValid)
            {
                var variants = _productRep.listAllVariants().Where(x => model.variantIds.Contains(x.ID)).ToList();
                foreach(var variant in variants)
                {
                    variant.changePricing(model.single.discount, model.volumePrices, model.deleteExisting, _productRep);
                }

                if (!string.IsNullOrWhiteSpace(model.bulkProductListName))
                {
                    Tag savedList = new Tag()
                    {
                        name = model.bulkProductListName,
                        Type = Tag.TypeOption.ADHOC_PRODUCT_GROUP
                    };
                    _contentRep.saveTag(savedList);
                    _productRep.saveProductList(savedList.ID, variants.Select(x => x.ProductID).ToList());
                }

                return this.Json(new { success = true });
            }
            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,//.Replace("[", "_").Replace("]", "_").Replace(".", "_"),
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );
            return this.Json(new { success = false, errorList = errorList });
        }

        public JsonResult runBulkGoogleCatAssignment(BulkGoogleCatViewModel model)
        {
            runExpire();

            var products = _productRep.listAllProducts(false,false).Where(x => model.productIds.Contains(x.ID)).ToList();
            foreach (var product in products)
            {
                product.googleCat = model.googleCat;
                product.modifiedDate = DateTime.Now;
                _productRep.saveProduct(product, false, false, false);
            }

            return this.Json(new { success = true });
        }

        public JsonResult runBulkBingCatAssignment(BulkBingCatViewModel model)
        {
            runExpire();

            var products = _productRep.listAllProducts(false, false).Where(x => model.productIds.Contains(x.ID)).ToList();
            foreach (var product in products)
            {
                product.bingCat = model.bingCat;
                product.modifiedDate = DateTime.Now;
                _productRep.saveProduct(product, false, false, false);
            }

            return this.Json(new { success = true });
        }

        public JsonResult runBulkShipDaysAssignment(BulkShipDaysViewModel model)
        {
            runExpire();

            var products = _productRep.listAllProducts(false, true).Where(x => model.productIds.Contains(x.ID)).ToList();
            foreach (var product in products)
            {
                if (product.variant != null)
                { 
                    product.variant.shipDays = model.shipDays;
                    product.variant.shipDaysMinimum = model.shipDaysMinimum;
                    product.modifiedDate = DateTime.Now;
                    _productRep.saveVariant(product.variant);
                }
            }

            return this.Json(new { success = true });
        }

        public JsonResult runBulkHideBlurbAssignment(BulkHideBlurbViewModel model)
        {
            runExpire();

            var products = _productRep.listAllProducts(false, false).Where(x => model.productIds.Contains(x.ID)).ToList();
            foreach (var product in products)
            {
                product.hideBlurb = model.hideBlurb;
                product.modifiedDate = DateTime.Now;
                _productRep.saveProduct(product, false, false, false);
            }

            return this.Json(new { success = true });
        }

        public JsonResult runBulkDisclaimersAssignment(BulkDisclaimersViewModel model)
        {
            runExpire();

            if (model.pageIds == null) model.pageIds = new List<int>();

            var disclaimers = model.pageIds.Select(s => new Page() { SystemPage = _contentRep.listDisclaimerPages().Single(tg => tg.ID == s).SystemPage, ID = s }).ToList();
            //deal with default disclaimer type, add a suppressive association only if the "Thera" is unticked.
           
            var products = _productRep.listAllProducts(false, false).Where(x => model.productIds.Contains(x.ID)).ToList();
            foreach (var product in products)
            {
                product.modifiedDate = DateTime.Now;
                product.disclaimers = disclaimers;
                _productRep.saveProduct(product, false, false, true);
            }

            return this.Json(new { success = true });
        }

        public JsonResult runBulkOutofstockAssignment(BulkOutofstockViewModel model)
        {
            runExpire();

            var products = _productRep.listAllProducts(false, false).Where(x => model.productIds.Contains(x.ID)).ToList();
            foreach (var product in products)
            {
                product.modifiedDate = DateTime.Now;
                var variant = _productRep.listAllVariants().Single(x => x.ProductID == product.ID);
                var wasOOS = variant.isHidden;
                var nowOOS = model.outOfStock;
                if (wasOOS && !nowOOS)
                {
                    sendBackInStockNotifications(product.ID);
                }
                variant.isHidden = model.outOfStock;
                _productRep.saveVariant(variant);
            }

            TagService.RunForHome();

            return this.Json(new { success = true });
        }

        [HttpGet]
        public ActionResult CloneProduct(int? id)
        {
            runExpire();

            var product = _productRep.listAllProducts(true,true).Single(p => p.ID == id);
            product.tags = _contentRep.listAllTagsForProduct(product).ToList();
            product.products = _productRep.listRelatedProductsForProduct(product.ID).ToList();
            product.disclaimers = _contentRep.listPagesForProduct(product.ID).ToList();
            var productSrs = product.siteResources.ToList(); 

            var newproduct = product.clone(_productRep);
            var srService = new SiteResourceService();
            productSrs.ForEach(x => srService.CloneProductImage(x, newproduct.ID, _srRep));

            return RedirectToRoute("ProductAdminProduct", new { id = newproduct.ID, m = "s", t = Request["t"] });
        }

        [HttpGet]
        public ActionResult Product(int? id, int? t)
        {
            runExpire();
            var product = id.HasValue ? _productRep.listAllProducts().Single(p => p.ID == id) : new Product() { googleCat = "469", bingCat = "469", disclaimers = new List<Page>() };
            ViewBag.tags = _contentRep.listAllTags().Where(x => x.Type != Tag.TypeOption.SUPER_SECTION).OrderBy(x => x.name).ToList();
            ViewBag.tagtype = Tag.TypeOption.PRODUCT_SECTION;
            ViewBag.disclaimers = _contentRep.listDisclaimerPages().OrderBy(x => x.orderNo).ToList();

            var svm = new AdminViewModel()
            {
                product = product
            };
            loadViewModel(svm);
            productSrSetrup(svm);
            loadSrs(product, true);
            
            if (product.ID == 0 && t.HasValue)
            {
                product.tags = new List<Tag>() { ((List<Tag>)ViewBag.tags).Single(tg => tg.ID == t) };
                product.questionAndAnswers = new List<QandA>();
            }
            else
            {
                product.tags = _contentRep.listAllTagsForProduct(product).ToList();
                product.products = _productRep.listRelatedProductsForProduct(product.ID).ToList();
                product.questionAndAnswers = _productRep.listAllQandA().Where(q => q.ProductID == product.ID).ToList();
            }

            //product.questionAndAnswers.Add(new QandA() { ProductID = product.ID });

            return View(product);
        }


        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Product(int? id, int? t, Product model)
        {
            runExpire();
            var product = model;
            
            ViewBag.tags = _contentRep.listAllTags().Where(x => x.Type != Tag.TypeOption.SUPER_SECTION).OrderBy(x => x.name).ToList();
            ViewBag.tagtype = Tag.TypeOption.PRODUCT_SECTION;

            var svm = new AdminViewModel()
            {
                product = product
            };
            loadViewModel(svm);
            productSrSetrup(svm);
            if (product.siteResources == null)
                product.siteResources = new List<SiteResource>().ToArray();

            product.tags = Request["TagIDs"] == null ? new List<Tag>() : Request["TagIDs"].Split(',').Select(s => new Tag() { name = ((List<Tag>)ViewBag.tags).Where(tg => tg.ID == int.Parse(s)).Single().name, ID = int.Parse(s) }).ToList();
            product.products = Request["ProdIDs"] == null ? new List<Product>() : Request["ProdIDs"].Split(',').Select(s => new Product() { name = _productRep.listAllProducts().Single(tg => tg.ID == int.Parse(s)).name, ID = int.Parse(s) }).ToList();
            product.disclaimers = Request["DisclaimerIDs"] == null ? new List<Page>() : Request["DisclaimerIDs"].Split(',').Select(s => new Page() { SystemPage = _contentRep.listDisclaimerPages().Single(tg => tg.ID == int.Parse(s)).SystemPage, ID = int.Parse(s) }).ToList();

            //deal with default disclaimer type, add a suppressive association only if the "Thera" is unticked.
            /*
            if (product.disclaimers.Any(x => x.SystemPage == Page.SystemPageOption.PRODUCT_DISCLAIMER_THERAPEUTIC))
                product.disclaimers.Remove(product.disclaimers.Single(x => x.SystemPage == Page.SystemPageOption.PRODUCT_DISCLAIMER_THERAPEUTIC));
            else
                product.disclaimers.Add(_contentRep.listDisclaimerPages().Single(x => x.SystemPage == Page.SystemPageOption.PRODUCT_DISCLAIMER_THERAPEUTIC));
            */

            if (ModelState.IsValid)
            {
                if(product.ID == 0)
                {
                    product.createdDate = DateTime.Now;
                }

                //deal with default disclaimer type, add a suppressive association only if the "Thera" is unticked.
                //if (!product.disclaimers.Any(x => x.SystemPage == Page.SystemPageOption.PRODUCT_DISCLAIMER_THERAPEUTIC))
                //    product.disclaimers.Add(_contentRep.listDisclaimerPages().Single(x => x.SystemPage == Page.SystemPageOption.PRODUCT_DISCLAIMER_THERAPEUTIC));
                _productRep.saveProduct(product,true,true,true);

                collectImagePosts(product);

                saveExistingImages(product);

                if (!ModelState.IsValid)
                {
                    loadSrs(product, true);
                    return View(product);
                }

                var success = !product.isHidden ? await SearchaniseService.UploadSingleProductToSearchanise(product) : await SearchaniseService.DeleteSingleProductToSearchanise(product);

                TagService.RunForHome();

                if (Request["goto"] == "true")
                    return RedirectToRoute("ProductAdminVariants", new { id = product.ID, m = "s" });

                //if (Request["showedit"] == "true")
                    //return Redirect("/Admin/ProductEdit/" + product.ID + "/?m=s&t=" + Request["t"]);
                return RedirectToRoute("ProductAdminProduct", new { id = product.ID, m="s", t=Request["t"] });
                //else
                    //return RedirectToRoute("ProductAdminIndex", new { id = Request["t"] });
            }
            else
            {
                var message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));

                loadSrs(product, true);
                return View(product);
            }
        }

        [HttpGet]
        public ActionResult Notifications(int? id, string message, StockNotice.StatusOption? status)
        {
            loadViewModel(new AdminViewModel());
            var product = _productRep.listAllProducts().Single(x => x.ID == id);

            ViewBag.Message = message;
            ViewBag.Product = product;
            ViewBag.Status = status ?? StockNotice.StatusOption.PENDING;

            return View(_productRep.listAllStockNotices().Where(x => x.ProductID==id).ToList().Where(x => x.Status == (status ?? (StockNotice.StatusOption?)StockNotice.StatusOption.PENDING)));
        }
        
        [HttpPost]
        public ActionResult Notifications(int? id, string StockNoticeEmail, string infile)
        {
            loadViewModel(new AdminViewModel());

            var product = _productRep.listAllProducts().FirstOrDefault(x => x.ID == id);
            var variant = _productRep.listAllVariants().FirstOrDefault(x => x.ProductID == id);
            ViewBag.Product = product;
            ViewBag.Status = StockNotice.StatusOption.PENDING;

            if (!string.IsNullOrEmpty(StockNoticeEmail))
            {
                if (CustomValidation.IsValidEmail(StockNoticeEmail))
                {
                    if (_productRep.listAllStockNotices().Where(y => y.ProductID == id && y.email.ToLower() == StockNoticeEmail.ToLower()).ToList().Any(y => y.Status == StockNotice.StatusOption.PENDING))
                        ModelState.AddModelError("", "The email address already existed for this product");
                    else
                    {
                        _productRep.saveStockNotice(new StockNotice() { ProductID = product.ID, VariantID = variant.ID, email = StockNoticeEmail, Status = StockNotice.StatusOption.PENDING, createDate = DateTime.UtcNow });
                        ViewBag.Message = "Email added successfully";
                    }
                }
                else
                    ModelState.AddModelError("", "The email was not a valid email address");
            }
            else
            {
                if (Request.Files.Count == 0)
                    ModelState.AddModelError("", "You must upload a file");

                HttpPostedFileBase file = Request.Files[0];

                if (file == null)
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
                else if (file.ContentLength > 0)
                {
                    int MaxContentLength = 1024 * 1024 * 3; //3 MB
                    string[] AllowedFileExtensions = new string[] { ".csv" };

                    if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                    {
                        ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                    }

                    else if (file.ContentLength > MaxContentLength)
                    {
                        ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                    }
                }

                if (ModelState.IsValid)
                {
                    using (var reader = new System.IO.StreamReader(file.InputStream))
                    {
                        try
                        {
                            ViewBag.Message = "File uploaded successfully";

                            var engine = new FileHelperEngine<Models.Csv.StockNotification>();
                            var results = engine.ReadStream(reader).ToList().Skip(1).ToList();
                            var inserts = new List<StockNotice>();

                            var noOfDups = 0;
                            var errors = engine.ErrorManager.Errors;
                            if (errors.Length == 0)
                            {
                                results.ForEach(x =>
                                {
                                    if (_productRep.listAllStockNotices().Where(y => y.ProductID == id && y.email.ToLower() == x.email.ToLower()).ToList().Any(y => y.Status == StockNotice.StatusOption.PENDING ))
                                        noOfDups++;
                                    else
                                        inserts.Add(new StockNotice() { ProductID = product.ID, VariantID = variant.ID, email = x.email, Status = StockNotice.StatusOption.PENDING, createDate = DateTime.UtcNow });
                                });

                                if (ModelState.IsValid)
                                {
                                    inserts.ForEach(x =>
                                    {
                                        _productRep.saveStockNotice(x);
                                    });

                                    ViewBag.Message = string.Format("{0} emails were uploaded. {1} duplicates were skiped", (results.Count() - noOfDups), noOfDups);
                                }
                            }
                            else
                            {
                                errors.ToList().ForEach(x => ModelState.AddModelError("", x.ExceptionInfo.Message));
                            }
                        }
                        catch (Exception e)
                        {
                            ModelState.AddModelError("", "Import Error: " + e.Message);
                        }
                    }
                }
            }
            return View(_productRep.listAllStockNotices().Where(x => x.ProductID==id).ToList().Where(x => x.Status == StockNotice.StatusOption.PENDING).ToList());
        }

        [HttpGet]
        public ActionResult NotificationDelete(int? id)
        {
            var notice = _productRep.listAllStockNotices().Single(x => x.ID == id);
            _productRep.deleteStockNotice(notice);
            return RedirectToAction("Notifications", new { id = notice.ProductID, message = "Email has been deleted" });
        }

        public ActionResult Ingredients(int? id)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                ingredients = _productRep.listAllIngredients(false).Where(i => i.ProductID==(int)id).ToList(),
                product = _productRep.listAllProducts().Single(i => i.ID == (int)id)
            };

            if (OrdererService.orderingCheck(vm.ingredients.Select(i => (IOrderedObject)i).ToList()))
            {
                foreach (var ing in vm.ingredients)
                    _productRep.saveIngredient(ing);
            }

            loadViewModel(vm);

            return View(vm);
        }
       
        /*
        [HttpGet]
        public ActionResult Ingredient(int? id, int? p)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                ingredient = id.HasValue ? _productRep.listAllIngredients(false).Single(i => i.ID == id) : new Ingredient() { ProductID=(int)p }
            };

            vm.product = p.HasValue ? _productRep.listAllProducts().Single(pr => pr.ID == (int)p) :
                                        _productRep.listAllProducts().Single(pr => pr.ID == vm.ingredient.ProductID);
                
            loadViewModel(vm);
            ingredientSrSetrup(vm);
            loadSrs(vm.srOwner, true);

            return View(vm);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Ingredient(int? id, Ingredient ingredient)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                product = _productRep.listAllProducts().Where(p => p.ID == ingredient.ProductID).Single(),
                ingredient = ingredient
            };
            ingredientSrSetrup(vm);
            
            if (ModelState.IsValid)
            {
                if (ingredient.ID == 0)
                {
                    ingredient.createDate = DateTime.Now;
                    OrdererService.setNextOrderNo(ingredient, _productRep.listAllIngredients(false).Where(s => s.ProductID == ingredient.ProductID).Select(o => (IOrderedObject)o).ToList());
                }
                
                _productRep.saveIngredient(ingredient);

                collectImagePosts(ingredient);

                saveExistingImages(ingredient);

                if (!ModelState.IsValid)
                {
                    loadSrs(vm.srOwner, true);
                    loadViewModel(vm);
                    return View(vm);
                }

                if (Request["showedit"] == "true")
                    //return Redirect("/Admin/ProductEdit/" + product.ID + "/?m=s&t=" + Request["t"]);
                    return RedirectToRoute("ProductAdminIngredientAddEdit", new { id = ingredient.ID, m = "s" });
                else
                    return RedirectToRoute("ProductAdminIngredients", new { id = vm.product.ID });
            }
            else
            {
                vm.message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));

                loadSrs(vm.srOwner, true);
                loadViewModel(vm);
                return View(vm);
            }
        }

        [HttpGet]
        public ActionResult IngredientDelete(int? id)
        {
            runExpire();
            var ingredient = _productRep.listAllIngredients(false).Single(i => i.ID == id);
            loadSrs(ingredient, false);
            deleteExistingImages(ingredient);

            List<Ingredient> children = _productRep.listAllIngredients(false).Where(s => s.ProductID == ingredient.ProductID).ToList();
            List<Ingredient> savers = OrdererService.delete(ingredient, children.Cast<IOrderedObject>().ToList()).Cast<Ingredient>().ToList();
            foreach (var obj in savers)
                _productRep.saveIngredient(obj);

            _productRep.deleteIngredient(ingredient);

            return RedirectToRoute("ProductAdminIngredients", new { id = ingredient.ProductID });
        }

        public virtual ActionResult IngredientMoveDown(int? id)
        {
            runExpire();

            Ingredient ingredient = _productRep.listAllIngredients(false).Where(s => s.ID == id).SingleOrDefault();
            List<Ingredient> children = _productRep.listAllIngredients(false).Where(s => s.ProductID == ingredient.ProductID).ToList();
            List<Ingredient> savers = OrdererService.moveDown(ingredient, children.Cast<IOrderedObject>().ToList()).Cast<Ingredient>().ToList();
            foreach (var obj in savers)
            {
                _productRep.saveIngredient(obj);
            }

            return RedirectToRoute("ProductAdminIngredients", new { id = ingredient.ProductID });
        }


        public virtual ActionResult IngredientMoveUp(int? id)
        {
            runExpire();

            Ingredient ingredient = _productRep.listAllIngredients(false).Where(s => s.ID == id).SingleOrDefault();
            List<Ingredient> children = _productRep.listAllIngredients(false).Where(s => s.ProductID == ingredient.ProductID).ToList();
            List<Ingredient> savers = OrdererService.moveUp(ingredient, children.Cast<IOrderedObject>().ToList()).Cast<Ingredient>().ToList();
            foreach (var obj in savers)
                _productRep.saveIngredient(obj);

            return RedirectToRoute("ProductAdminIngredients", new { id = ingredient.ProductID });
        }
        */
        [HttpGet]
        public ActionResult Variants(int? id, string a)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                product = _productRep.listAllProducts(false,true).Single(p => p.ID == id)
            };
            loadViewModel(vm);
            ViewBag.SiteModel = vm;

            if (a != null)
            {
                vm.success = true;
                vm.message = "Your prices have been saved";
            }

            var variant = vm.product.variant ?? new Variant() { ProductID = (int)id, name = vm.product.name };
            if (variant.bulkPrices == null) variant.bulkPrices = new List<Price>();
            variant.bulkPrices.Add(new Price() { VariantID = variant.ID });

            ViewBag.ProductID = vm.product.ID;
            
            return View(variant);
        }

        [HttpPost]
        public ActionResult Variants(int? id, List<Variant> variants)
        {
            runExpire();

            var newSlot = variants.SingleOrDefault(x => x.ID == 0 && string.IsNullOrWhiteSpace(x.name));
            if(newSlot != null)
            {
                ModelState.Keys.Where(x => x.StartsWith("variants[" + variants.IndexOf(newSlot) + "]")).ToList().ForEach(x => ModelState.Remove(x));
            }

            foreach (var v in variants.Where(x => x != newSlot))
            {
                var newSlotBulks = v.bulkPrices.Where(x => x.ID == 0 && x.quantity==0 && x.amount==0).ToList();
                foreach (var b in newSlotBulks)
                {
                    ModelState.Keys.Where(x => x.StartsWith("variants[" + variants.IndexOf(v) + "].bulkPrices[" + v.bulkPrices.IndexOf(b) + "]")).ToList().ForEach(x => ModelState.Remove(x));
                }
            }

            var wasOOS = _productRep.listAllVariants().Where(x => x.ProductID == id).All(x => x.isHidden);

            if (ModelState.IsValid)
            {
                foreach (var v in variants.Where(x => x != newSlot))
                {
                    if (v.delete)
                        _productRep.deleteVariant(v);
                    else
                    { 
                        if(v.ID == 0)
                        {
                            v.createDate = DateTime.Now;
                        }
                        v.modifiedDate = DateTime.Now;
                        _productRep.saveVariant(v);

                        foreach (var b in v.bulkPrices.Where(x => !(x.ID == 0 && x.quantity==0 && x.amount==0)))
                        {
                            _productRep.savePrice(b);
                        }
                    }
                }

                var nowOOS = variants.All(x => x.isHidden);

                if (wasOOS && !nowOOS)
                {
                    sendBackInStockNotifications((int)id);
                }

                TagService.RunForHome();

                if (Request["goto"] == "true")
                    return RedirectToRoute("ProductAdminProduct", new { id = id, m = "s" });

                return RedirectToRoute("ProductAdminVariants", new { id = id, a = "s" });
            }
            else
            {
                var messages = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));


                AdminViewModel vm = new AdminViewModel()
                {
                    product = _productRep.listAllProducts().Single(p => p.ID == id)
                };
                loadViewModel(vm);
                ViewBag.SiteModel = vm;

                return View(new VariantSet() { variants = variants });
            }
        }

        private string sendBackInStockNotifications(int productID) {
            var product = _productRep.listAllProducts().Single(x => x.ID == productID);
            int notificationsSent = 0;
            string sendto = "";

            try
            {

            var notifies = _productRep.listAllStockNotices().Where(x => x.ProductID == product.ID && x.Status == StockNotice.StatusOption.PENDING).ToList();
            foreach (var notify in notifies)
            {
                if (!notifies.Any(x => x.email.ToLower() == notify.email.ToLower() && x.Status == StockNotice.StatusOption.SENT))
                {
                    var subject = string.Format("{0} is now available at Meguiars", product.name);
                    var body = string.Format("Hi!<br/><br/>Just letting you know, {0} is now available for purchase on the Meguiars website.<br/><br/>To view the product, <a href=\"https://www.meguiars.co.nz{1}\">click here</a>.<br/><br/>Tahnks,<br/><br/>Meguiars<br/><a href=\"https://www.meguiars.co.nz\" >www.meguiars.co.nz</a><br/><br/>Please be aware, this email was sent to you as you had requested to be notified when the product above was available for purchase.", product.name, product.getUrl());

                    MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["NotifyEmail"], notify.email)
                    {
                        Subject = subject,
                        IsBodyHtml = true
                    };
                    //msg.Bcc.Add(ConfigurationManager.AppSettings["ShopBCC"]);
                    msg.Body = body;
                    using (var client = new SmtpClient())
                    {
                        client.Send(msg);
                        sendto += msg.To.ToString();
                        notificationsSent++;
                    }
                    notify.sentDate = DateTime.UtcNow;
                }
                notify.Status = StockNotice.StatusOption.SENT;
                _productRep.saveStockNotice(notify);

                
            }

            return "we sent " + notificationsSent.ToString() + " notifications to " + sendto;
            }
            catch (Exception ex) {
                return "error: " + ex.Message;

            }
        }

        public JsonResult getVariant(int ProductID)
        {
            runExpire();
            var product = _productRep.listAllProducts(false, true).Single(p => p.ID == ProductID);
            var variant = product.variant ?? new Variant() { ProductID = (int)ProductID, name = product.name };
            //if (variant.bulkPrices == null) variant.bulkPrices = new List<Price>();
            //variant.bulkPrices.Add(new Price() { VariantID = variant.ID });
            return Json(variant);
        }

        public JsonResult saveVariant(Variant variant)
        {
            runExpire();
            if (ModelState.IsValid)
            {
                var wasOOS = false;
                Variant v = _productRep.listAllVariants().FirstOrDefault(x => x.ProductID == variant.ProductID);
                if(v!=null)
                {
                    wasOOS = v.isHidden;
                }
                

                if (variant.ID == 0)
                {
                    variant.createDate = DateTime.Now;
                }
                variant.modifiedDate = DateTime.Now;

                if (variant.stockNo == 0 && variant.preOrderShipDate.HasValue)
                    variant.preOrderShipDate = null;

                if (variant.stockNo == 0 && !variant.isHidden)
                    variant.isHidden = true;

                _productRep.saveVariant(variant);

                if (variant.bulkPrices == null)
                    variant.bulkPrices = new List<Price>();

                foreach (var b in _productRep.listAllVariants().Single(x => x.ID == variant.ID).bulkPrices.Where(x => !variant.bulkPrices.Select(b => b.ID).Contains(x.ID)))
                {
                    _productRep.deletePrice(b);
                }

                if (variant.bulkPrices != null && variant.bulkPrices.Any())
                { 
                    foreach (var b in variant.bulkPrices.Where(x => !(x.ID == 0 && x.quantity == 0 && x.amount == 0)))
                    {
                        _productRep.savePrice(b);
                    }
                }

                var nowOOS = variant.isHidden;
                string message = "" + wasOOS.ToString() + "/" + nowOOS.ToString();
                if (wasOOS && !nowOOS)
                {
                    message = sendBackInStockNotifications(variant.ProductID);
                }
                TagService.RunForHome();
                return this.Json(new { success = true, message = message });
            }
            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,//.Replace("[", "_").Replace("]", "_").Replace(".", "_"),
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );
            return this.Json(new { success = false, errorList = errorList });
        }
        
        [HttpGet]
        public ActionResult ProductDelete(int? id)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                product = _productRep.listAllProducts().Single(p => p.ID == id)
            };

            ViewBag.CanHide = !_productRep.listAllProductsWithOrdersOrPromotions().Any(x => x.ID == id);

            return View(vm);
        }

        [HttpPost]
        public async Task<ActionResult> ProductDelete(int? id, Product inProduct)
        {
            runExpire();
            Product product = _productRep.listAllProducts().Single(p => p.ID == id);

            /*
            var ingredients = _productRep.listAllIngredients(false).Where(i => i.ProductID == (int)id).ToList();
            foreach(var ingred in ingredients)
            {
                loadSrs(ingred, false);
                deleteExistingImages(ingred);
                _productRep.deleteIngredient(ingred);
            }
            */

            if(!_productRep.listAllProductsWithOrdersOrPromotions().Any(x => x.ID == id))
            {
                loadSrs(product, false);
                deleteExistingImages(product);
                _productRep.deleteProduct(product);
            }
            else
            {
                ModelState.AddModelError("","This product has associated order or promotions data so has been hidden but not deleted.");
                product.isHidden = true;
                _productRep.saveProduct(product, false, false, false);
            }

            var result = await SearchaniseService.DeleteSingleProductToSearchanise(product);

            return RedirectToRoute("ProductAdminIndex", new { id = Request["t"] });
        }

        public ActionResult UtilTestNoticeEmail()
        {
            //sendBackInStockNotifications(8000);
            return Content("Done");
        }

        /*
        
        public ActionResult TradeData(int? id)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                pages = _contentRep.listAllPages().Where(i => i.ProductID == (int)id).ToList(),
                product = _productRep.listAllProducts().Single(i => i.ID == (int)id)
            };

            loadViewModel(vm);

            return View(vm);
        }

        [HttpGet]
        public ActionResult DataPage(int? id, int? p)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = id.HasValue ? _contentRep.listAllPages().Single(pa => pa.ID == id) :
                                    new Page()
                                    {
                                        Type = Page.TypeOption.FILES_ONLY,
                                        ProductID = p
                                    },
                tags = _contentRep.listAllTags().ToList(),
                //tagtype = Tag.TypeOption.SECTION_GROUP,
                formRoute = "ProductAdminDataPage",
                pageTitle = (id.HasValue?"Edit":"Create") + " a Product Trade data Section"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            return runPageAdminGet(vm, null, null, "~/Views/ArticleAdmin/PageSimple.cshtml");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult DataPage(int? id, int? t, Page page)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                page = page,
                tags = _contentRep.listAllTags().ToList(),
                //tagtype = Tag.TypeOption.SECTION_GROUP,
                pages = _contentRep.listAllPages().ToList(),
                formRoute = "ProductAdminDataPage",
                pageTitle = (id.HasValue ? "Edit" : "Create") + " a Product Trade data Section"
            };

            vm.formRouteVars = new AdminPageRouteVariables() { id = vm.page.ID };

            if(page.ID==0)
            { 
                List<Page> children = _contentRep.listAllPages().Where(i => page.ProductID == (int)id).ToList();
                OrdererService.setNextOrderNo((IOrderedObject)page, children.Cast<IOrderedObject>().ToList());
            }

            if (Request["showedit"] == "true")
            {
                return runPageAdminPost(vm, page, "ProductAdminDataPage", new AdminPageRouteVariables()
                {
                    id = page.ID,
                    m = "s",
                    t = Request["t"],
                    injectID = true
                }, "~/Views/ArticleAdmin/PageSimple.cshtml");
            }
            else
            {
                return runPageAdminPost(vm, page,"ProductAdminTradeData", new AdminPageRouteVariables()
                {
                    id = page.ProductID
                }, "~/Views/ArticleAdmin/PageSimple.cshtml");
            }
        }

        [HttpGet]
        public ActionResult DataPageDelete(int? id)
        {
            runExpire();
            Page page = _contentRep.listAllPages().Where(p => p.ID == id).Single();

            if (!page.ProductID.HasValue)
                throw new Exception("Should be a product trade data page");

            page.tags = _contentRep.listAllTagsForPage(page).ToList();


            loadSrs(page, false);
            pageSrSetrup(new AdminViewModel() { page = page });
            deleteExistingImages(page);

            
            if (page.tags.Any(t => t.Type == Tag.TypeOption.SECTION))
            {
                List<Page> children = _contentRep.listPagesForTag(page.tags.Where(t => t.Type == Tag.TypeOption.SECTION).Single().ID).ToList();
                List<Page> savers = OrdererService.delete((IOrderedObject)page, children.Cast<IOrderedObject>().ToList()).Cast<Page>().ToList();
                foreach (var obj in savers)
                {
                    _contentRep.savePage(obj);
                }
            }
           

            _contentRep.deletePage(page);
            return RedirectToRoute("ProductAdminTradeData", new { id = page.ProductID });
        }
         * */
    }
}