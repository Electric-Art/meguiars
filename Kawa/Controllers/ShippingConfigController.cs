﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class ShippingConfigController : AdminController
    {
        // GET: ShippingAdmin
        public ActionResult Index()
        {
            return View();
        }

        #region ShippingMethods

        public ActionResult Carriers()
        {
            runExpire();
            loadAdminViewModel();
            return View(_shipRep.listAllCarriers());
        }

        [HttpGet]
        public ActionResult Carrier(int? id)
        {
            runExpire();
            loadAdminViewModel();
            var model = id.HasValue ? _shipRep.listAllCarriers().Single(s => s.ID == (int)id) : new Carrier();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Carrier(int? id, Carrier model)
        {
            runExpire();
            if (ModelState.IsValid)
            {
                if (model.ID == 0)
                {
                    model.createDate = DateTime.Now;
                }
                _shipRep.saveCarrier(model);
                return RedirectToRoute("ShippingConfigCarriers", new { id = "" });
            }
            else
            {
                loadAdminViewModel();
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult CarrierDelete(int? id)
        {
            runExpire();
            loadAdminViewModel();
            return View(_shipRep.listAllCarriers().Single(s => s.ID == (int)id));
        }

        [HttpPost]
        public ActionResult CarrierDelete(int? id, Carrier model)
        {
            runExpire();
            loadAdminViewModel();
            if (!_orderRep.listAllShipments().Any(x => x.CarrierID==model.ID))
            { 
                _shipRep.deleteCarrier(model);
                return RedirectToRoute("ShippingConfigCarriers");
            }
            else
            {
                ModelState.AddModelError("", "You cannot delete this carrier it has associated shipments.");
                return View(_shipRep.listAllCarriers().Single(s => s.ID == (int)id));
            }
        }

        #endregion

        #region ShippingMethods

        public ActionResult ShippingMethods()
        {
            runExpire();
            loadAdminViewModel();
            return View(_shipRep.listAllShippingMethods());
        }

        [HttpGet]
        public ActionResult ShippingMethod(int? id)
        {
            runExpire();
            loadAdminViewModel();
            var model = id.HasValue ? _shipRep.listAllShippingMethods().Single(s => s.ID == (int)id) : new ShippingMethod();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ShippingMethod(int? id, ShippingMethod model)
        {
            runExpire();
            if (ModelState.IsValid)
            {
                if (model.ID == 0)
                {
                    model.createDate = DateTime.Now;
                }
                _shipRep.saveShippingMethod(model);
                return RedirectToRoute("ShippingConfigShippingMethods", new { id = "" });
            }
            else
            {
                loadAdminViewModel();
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult ShippingMethodDelete(int? id)
        {
            runExpire();
            loadAdminViewModel();
            return View(_shipRep.listAllShippingMethods().Single(s => s.ID == (int)id));
        }

        [HttpPost]
        public ActionResult ShippingMethodDelete(int? id, ShippingMethod model)
        {
            runExpire();
            loadAdminViewModel();
            _shipRep.deleteShippingMethod(model);
            return RedirectToRoute("ShippingConfigShippingMethods");
        }

        public ActionResult ShippingMethodRates(int id)
        {
            runExpire();
            loadAdminViewModel();

            var model = new ShippingMethodRatesViewModel()
            {
                ShippingMethod = _shipRep.listAllShippingMethods().Single(s => s.ID == (int)id),
                Locations = _shipRep.listAllLocations().OrderBy(x => x.Name).ToList()
            };

            model.Locations.Insert(0, new Location() { ID = 0, Name = "Default" });

            return View(model);
        }

        public JsonResult ShippingMethodRatesForCountry(int ShippingMethodID, int? LocationID)
        {
            runExpire();

            if (LocationID == 0) LocationID = null;

            var rates = _shipRep.listAllShippingRates().Where(x => 
                x.ShippingMethodID == ShippingMethodID &&
                (LocationID.HasValue ? x.LocationID == LocationID : !x.LocationID.HasValue)
                ).ToList().OrderBy(x => x.WeightDependency);

            if (rates.Any())
                rates.Last().isLastWeight = true;

            return this.Json(rates, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ShippingMethodRateSave(ShippingRate model)
        {
            runExpire();

            if (ModelState.IsValid)
            {
                if (model.ID == 0)
                {
                    model.createDate = DateTime.Now;
                    if(model.LocationID == 0) model.LocationID = null;
                    _shipRep.saveShippingRate(model);
                }
                else
                {
                    var existing = _shipRep.listAllShippingRates().Single(x => x.ID == model.ID);
                    existing.Rate = model.Rate;
                    existing.WeightDependency = model.WeightDependency;
                    _shipRep.saveShippingRate(existing);
                }
                return this.Json(new { success = true });
            }
            var errorList = ModelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            );
            return this.Json(new { success = false, errorList = errorList });
        }

        [HttpPost]
        public JsonResult ShippingMethodRateDelete(ShippingRate model)
        {
            runExpire();

            _shipRep.deleteShippingRate(model);
            
            return this.Json(new { success = true });
        }

        #endregion

        #region Locations

        public ActionResult Locations()
        {
            runExpire();
            loadAdminViewModel();
            return View(_shipRep.listAllLocations().OrderBy(x => x.Name).ToList());
        }

        [HttpGet]
        public ActionResult Location(int? id)
        {
            runExpire();
            loadAdminViewModel();
            var model = id.HasValue ? _shipRep.listAllLocations().Single(s => s.ID == (int)id) : new Location();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Location(int? id, Location model)
        {
            runExpire();
            if (ModelState.IsValid)
            {
                if (model.ID == 0)
                {
                    model.createDate = DateTime.Now;
                }
                _shipRep.saveLocation(model);
                return RedirectToRoute("ShippingConfigLocations", new { id = "" });
            }
            else
            {
                loadAdminViewModel();
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult LocationDelete(int? id)
        {
            runExpire();
            loadAdminViewModel();

            var issues = "Warnings:";
            if (_orderRep.listAllOrders().Any(x => x.LocationID == id))
                issues += "There are orders associated with this location, this association will be lost.";
            if (_orderRep.listAllPromotions().Where(x => x.hasLocation).ToList().Any(x => x.locations.Any(y => y.ID==id)))
                issues += "There are promotions/vouchers associated with this location, their association will be lost and the promotion/voucher will be set as inactive.";

            if (issues != "Warnings:")
                ViewBag.Warnings = issues;

            return View(_shipRep.listAllLocations().Single(s => s.ID == (int)id));
        }

        [HttpPost]
        public ActionResult LocationDelete(int? id, Location model)
        {
            runExpire();
            loadAdminViewModel();

            _orderRep.listAllOrders().Where(x => x.LocationID == id).ToList().ForEach(x =>
            {
                x.LocationID = null;
                _orderRep.saveOrder(x);
            });

            _orderRep.listAllPromotions().Where(x => x.hasLocation).ToList().Where(x => x.locations.Any(y => y.ID==id)).ToList().ForEach(x =>
            {
                x.locations = new List<Location>();
                x.isHidden = true;
                _orderRep.savePromotion(x,true);
            });

            _shipRep.deleteLocation(model);
            return RedirectToRoute("ShippingConfigLocations");
        }

        public ActionResult LocationScope(int id)
        {
            runExpire();
            loadAdminViewModel();

            var model = getScopeForLocation(id);
            model.Location = _shipRep.listAllLocations().Single(s => s.ID == (int)id);
            model.CountryID = null;
            model.Countries = _shipRep.listAllCountries().ToList();
            model.StateID = null;
            model.States = _shipRep.listAllStates().ToList();
            
            return View(model);
        }

        public ActionResult LocationScopeAddCountry(int LocationID,int CountryID)
        {
            runExpire();
            _shipRep.saveCountryScope(LocationID, CountryID);
            return PartialView("_LocationScopeItems", getScopeForLocation(LocationID));
        }

        public ActionResult LocationScopeRemoveCountry(int LocationID, int ObjectID)
        {
            runExpire();
            _shipRep.deleteCountryScope(LocationID, ObjectID);
            return PartialView("_LocationScopeItems", getScopeForLocation(LocationID));
        }

        public ActionResult LocationScopeAddState(int LocationID, int StateID)
        {
            runExpire();
            _shipRep.saveStateScope(LocationID, StateID);
            return PartialView("_LocationScopeItems", getScopeForLocation(LocationID));
        }

        public ActionResult LocationScopeRemoveState(int LocationID, int ObjectID)
        {
            runExpire();
            _shipRep.deleteStateScope(LocationID, ObjectID);
            return PartialView("_LocationScopeItems", getScopeForLocation(LocationID));
        }

        public ActionResult LocationScopeAddPostcodeRange(LocationPostcodeRange model)
        {
            runExpire();
            if (!(model.FromPostcode == 0 && model.ToPostcode == 0))
            { 
                if (model.ToPostcode == 0)
                    model.ToPostcode = model.FromPostcode;
                _shipRep.saveLocationPostcodeRange(model);
            }
            return PartialView("_LocationScopeItems", getScopeForLocation(model.LocationID));
        }

        public ActionResult LocationScopeRemovePostcodeRange(int LocationPostcodeRangeID)
        {
            runExpire();
            var model = _shipRep.listAllLocationPostcodeRanges().Single(x => x.ID == LocationPostcodeRangeID);
            _shipRep.deleteLocationPostcodeRange(model);
            return PartialView("_LocationScopeItems", getScopeForLocation(model.LocationID));
        }

        private LocationScopeViewModel getScopeForLocation(int LocationID)
        {
            return new LocationScopeViewModel()
            {
                ScopeCountries = _shipRep.listAllCountriesForLocation(LocationID).ToList(),
                ScopeStates = _shipRep.listAllStatesForLocation(LocationID).ToList(),
                ScopePostcodeRanges = _shipRep.listAllLocationPostcodeRanges().Where(x => x.LocationID==LocationID).ToList()
            };
        }


        #endregion

        #region Country

        public ActionResult Countries()
        {
            runExpire();
            loadAdminViewModel();
            return View(_shipRep.listAllCountries());
        }

        [HttpGet]
        public ActionResult Country(int? id)
        {
            runExpire();
            loadAdminViewModel();
            var model = id.HasValue ? _shipRep.listAllCountries().Single(s => s.ID == (int)id) : new Country();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Country(int? id, Country model)
        {
            runExpire();
            if (ModelState.IsValid)
            {
                if (model.ID == 0)
                {
                    model.createDate = DateTime.Now;
                }
                _shipRep.saveCountry(model);
                return RedirectToRoute("ShippingConfigCountries", new { id = "" });
            }
            else
            {
                loadAdminViewModel();
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult CountryDelete(int? id)
        {
            runExpire();
            loadAdminViewModel();

            ViewBag.CanDelete = !_shipRep.listAllStates().Any(x => x.CountryID == id) && !_shipRep.listAllLocationsForCountry((int)id).Any() && !_orderRep.listAllOrders().Any(x => x.CountryID == id);

            return View(_shipRep.listAllCountries().Single(s => s.ID == (int)id));
        }

        [HttpPost]
        public ActionResult CountryDelete(int? id, Country model)
        {
            runExpire();
            loadAdminViewModel();
            _shipRep.deleteCountry(model);
            return RedirectToRoute("ShippingConfigCountries");
        }

        #endregion

        #region State

        public ActionResult States(int? id)
        {
            runExpire();
            loadAdminViewModel();
            ViewBag.Country = _shipRep.listAllCountries().Single(s => s.ID == (int)id);
            return View(_shipRep.listAllStates().Where(s => s.CountryID == id));
        }

        [HttpGet]
        public ActionResult State(int? id, int? CountryID)
        {
            runExpire();
            loadAdminViewModel();
            var model = id.HasValue ? _shipRep.listAllStates().Single(s => s.ID == (int)id) : new State() { CountryID = (int)CountryID };
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult State(int? id, State model)
        {
            runExpire();
            if (ModelState.IsValid)
            {
                if (model.ID == 0)
                {
                    model.createDate = DateTime.Now;
                }
                _shipRep.saveState(model);
                return RedirectToRoute("ShippingConfigStates", new { id = model.CountryID });
            }
            else
            {
                loadAdminViewModel();
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult StateDelete(int? id)
        {
            runExpire();
            loadAdminViewModel();
            return View(_shipRep.listAllStates().Single(s => s.ID == (int)id));
        }

        [HttpPost]
        public ActionResult StateDelete(int? id, State model)
        {
            runExpire();
            loadAdminViewModel();
            _shipRep.deleteState(model);
            return RedirectToRoute("ShippingConfigStates");
        }

        #endregion
    }
}