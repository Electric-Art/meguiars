﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Services;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class HomeAdminController : AdminSiteController
    {
        // GET: HomeAdmin
        public ActionResult Banners()
        {
            runExpire();

            var homeBanners = _contentRep.listAllPages().SingleOrDefault(p => p.Type == Page.TypeOption.BANNERS && p.SystemPage == Page.SystemPageOption.HOME_PAGE_SLIDE_SHOW);

            if (homeBanners == null)
            {
                homeBanners = new Page()
                {
                    name = "Home Page Banner",
                    Type = Page.TypeOption.BANNERS,
                    SystemPage = Page.SystemPageOption.HOME_PAGE_SLIDE_SHOW,
                    createdDate = DateTime.Now
                };

                _contentRep.savePage(homeBanners);
            }


            return RedirectToRoute("ArticleAdminPage", new { id=homeBanners.ID });
        }

        public ActionResult Brands()
        {
            runExpire();

            var homeBrands = _contentRep.listAllPages().SingleOrDefault(p => p.Type == Page.TypeOption.CONTENT && p.SystemPage == Page.SystemPageOption.POPULAR_BRANDS);

            return RedirectToRoute("ArticleAdminPage", new { id = homeBrands.ID });
        }

        [HttpGet]
        public ActionResult Modules(int? id)
        {
            runExpire();
            loadViewModel(new AdminViewModel());
            ModulesAdminViewModel vm = new ModulesAdminViewModel()
            {
                slots1 = _contentRep.listAllSlots(true,false).Where(s => s.Spot == Slot.SpotOption.HOME_PAGE_MODULE).OrderBy(s => s.orderNo).ToList(),
                slots2 = _contentRep.listAllSlots(true, false).Where(s => s.Spot == Slot.SpotOption.VIDEO_MODULE).OrderBy(s => s.orderNo).ToList(),
                slots3 = _contentRep.listAllSlots(true, false).Where(s => s.Spot == Slot.SpotOption.SITE_WIDE_SIGNUP).OrderBy(s => s.orderNo).ToList(),
                modules = _contentRep.listAllPages().Where(p => p.Type == Page.TypeOption.CLICK_THROUGH).ToList(),
                youtubemodules = _contentRep.listAllPages().Where(p => p.Type == Page.TypeOption.CONTENT_VIDEO).ToList()
            };

            foreach (var page in vm.modules)
                loadSrs(page, false);

            if (vm.slots1.Any(s => s.page == null))
            {
                var todel = vm.slots1.Single(s => s.page == null);
                return RedirectToRoute("HomeAdminModuleDelete", new { id = todel.ID });
            }
            if (vm.slots2.Any(s => s.page == null))
            {
                var todel = vm.slots2.Single(s => s.page == null);
                return RedirectToRoute("HomeAdminModuleDelete", new { id = todel.ID });
            }
            if (vm.slots3.Any(s => s.page == null))
            {
                var todel = vm.slots2.Single(s => s.page == null);
                return RedirectToRoute("HomeAdminModuleDelete", new { id = todel.ID });
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult Modules(int? id, int? PageID, Slot.SpotOption slotoption)
        {
            runExpire();

            if (!PageID.HasValue)
                return RedirectToRoute("HomeAdminModules", new { id = "" });

            Slot slot = new Slot() { Spot = slotoption, PageID = PageID };
            OrdererService.setNextOrderNo(slot, _contentRep.listAllSlots().Where(s => s.Spot == slotoption).Select(o => (IOrderedObject)o).ToList());
            _contentRep.saveSlot(slot);

            TagService.RunForMenu();

            return RedirectToRoute("HomeAdminModules", new { id="" });
        }

        public virtual ActionResult ModuleMoveDown(int? id)
        {
            runExpire();

            Slot sr = _contentRep.listAllSlots().Where(s => s.ID == id).SingleOrDefault();
            List<Slot> children = _contentRep.listAllSlots().Where(s => s.Spot == sr.Spot).ToList();
            List<Slot> savers = OrdererService.moveDown(sr, children.Cast<IOrderedObject>().ToList()).Cast<Slot>().ToList();
            foreach (var obj in savers)
            {
                _contentRep.saveSlot(obj);
            }

            TagService.RunForMenu();

            return RedirectToRoute("HomeAdminModules", new { id = "" });
        }


        public virtual ActionResult ModuleMoveUp(int? id)
        {
            runExpire();

            Slot sr = _contentRep.listAllSlots().Where(s => s.ID == id).SingleOrDefault();
            List<Slot> children = _contentRep.listAllSlots().Where(s => s.Spot == sr.Spot).ToList();
            List<Slot> savers = OrdererService.moveUp(sr, children.Cast<IOrderedObject>().ToList()).Cast<Slot>().ToList();
            foreach (var obj in savers)
                _contentRep.saveSlot(obj);

            return RedirectToRoute("HomeAdminModules", new { id = "" });
        }

        public virtual ActionResult ModuleDelete(int? id)
        {
            runExpire();

            Slot sr = _contentRep.listAllSlots().Where(s => s.ID == id).SingleOrDefault();
            List<Slot> children = _contentRep.listAllSlots().Where(s => s.Spot == sr.Spot).ToList();
            List<Slot> savers = OrdererService.delete(sr, children.Cast<IOrderedObject>().ToList()).Cast<Slot>().ToList();
            foreach (var obj in savers)
                _contentRep.saveSlot(obj);

            _contentRep.deleteSlot(sr);

            TagService.RunForMenu();

            return RedirectToRoute("HomeAdminModules", new { id = "" });
        }
    }
}