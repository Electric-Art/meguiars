﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using System.Configuration;

namespace Kawa.Controllers
{
    public class TradeController : SiteController
    {
        private static string CookieName = "kawanaturaltrade";

        // GET: Trade
        public ActionResult Index()
        {
            if (!isTradeLogged()) return RedirectToAction("LogIn");

            SiteViewModel vm = new SiteViewModel() { subSection = "Home" };
            loadViewModel(vm);

            vm.page = _contentRep.listAllPages().Single(p => p.SystemPage == Models.Page.SystemPageOption.TRADE_HOME);
            vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);

            return View(vm);
        }

        // GET: PriceSheets
        public ActionResult PriceSheets()
        {
            if (!isTradeLogged()) return RedirectToAction("LogIn");

            SiteViewModel vm = new SiteViewModel() { subSection = "PriceSheets" };
            loadViewModel(vm);

            vm.page = _contentRep.listAllPages().Single(p => p.SystemPage == Models.Page.SystemPageOption.TRADE_PRICE_SHEETS);
            vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);

            return View("FileDownloadPage",vm);
        }

        // GET: TrainingModules
        public ActionResult TrainingModules()
        {
            if (!isTradeLogged()) return RedirectToAction("LogIn");

            SiteViewModel vm = new SiteViewModel() { subSection = "TrainingModules" };
            loadViewModel(vm);

            vm.page = _contentRep.listAllPages().Single(p => p.SystemPage == Models.Page.SystemPageOption.TRADE_MODULES);
            vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);

            return View("FileDownloadPage", vm);
        }

        // GET: AdministrationForms
        public ActionResult AdministrationForms()
        {
            if (!isTradeLogged()) return RedirectToAction("LogIn");

            SiteViewModel vm = new SiteViewModel() { subSection = "AdministrationForms" };
            loadViewModel(vm);

            vm.page = _contentRep.listAllPages().Single(p => p.SystemPage == Models.Page.SystemPageOption.TRADE_ADMINISTRATION_FORMS);
            vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);

            return View("FileDownloadPage", vm);
        }

        // GET: ProductFactSheets
        public ActionResult ProductFactSheets(int? id)
        {
            if (!isTradeLogged()) return RedirectToAction("LogIn");

            SiteViewModel vm = new SiteViewModel() { subSection = "ProductFactSheets" };
            loadViewModel(vm);

            vm.products = _productRep.listAllProductsWithPages().Where(p => !p.isHidden).OrderBy(p => p.name).ToList();

            vm.product = id.HasValue? vm.products.Single(p => p.ID==id) : vm.products.First();

            vm.pages = _contentRep.listAllPages(true).Where(p => p.ProductID == vm.product.ID).ToList();

            foreach(var page in vm.pages)
                SiteResourceService.loadUrls(page.siteResources);
            /*
            vm.page = _contentRep.listAllPages().Single(p => p.ProductID == id);
            vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);
            */
            return View("ProductFactSheets", vm);
        }

        // GET: AdministrationForms
        public ActionResult BrandAssets()
        {
            if (!isTradeLogged()) return RedirectToAction("LogIn");

            SiteViewModel vm = new SiteViewModel() { subSection = "BrandAssets" };
            loadViewModel(vm);

            vm.page = _contentRep.listAllPages().Single(p => p.SystemPage == Models.Page.SystemPageOption.TRADE_BRAND_ASSETS);
            vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);

            return View("Index", vm);
        }

        [HttpGet]
        public ActionResult LogIn(string id)
        {
            SiteViewModel vm = new SiteViewModel();

            loadViewModel(vm);

            return View();
        }

        [HttpPost]
        public ActionResult LogIn(string id, TradeLoginViewModel model)
        {
            runExpire();
            SiteViewModel vm = new SiteViewModel();

            loadViewModel(vm);

            if (ModelState.IsValid)
            {
                if (ConfigurationManager.AppSettings["TradePassword"].Contains(model.Password))
                {
                    logInTrade();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Sorry your password is incorrect.");
                }

            }

            return View(model);
        }

        [HttpGet]
        public ActionResult LogOut(string id)
        {
            runExpire();
            logOffTrade();

            return Redirect("/");
        }

        private void logInTrade()
        {
            HttpCookie myCookie = Request.Cookies[CookieName] ?? new HttpCookie(CookieName);
            //myCookie.Values["UserId"] = UserId.ToString();
            myCookie.Values["LastVisit"] = DateTime.Now.ToString();
            myCookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(myCookie);
        }

        private void logOffTrade()
        {
            if (Request.Cookies[CookieName] != null)
            {
                var c = new HttpCookie(CookieName);
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }
        }

        private bool isTradeLogged()
        {
            return Request.Cookies[CookieName] != null;
        }

        
    }
}