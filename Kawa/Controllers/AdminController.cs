﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;
using System.Configuration;
using Facebook;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using RestSharp;

namespace Kawa.Controllers
{
    public class AdminController : AdminSiteController
    {
        public static string SigmaAPIKeyHeader = "dfh^6Enbjsdv&7s$fb";

        [HttpGet]
        public ActionResult Index(int? id, string message)
        {
            runExpire();
            checkSystemPages();

            ViewBag.Message = message;
            
            /*
            var service = new FacebookService();
            var vm = new AdminViewModel()
            {
                needsToken = !service.runUpdate(null, _contentRep)
            };
             */ 
            return View(new AdminViewModel());
        }

        [HttpGet]
        public ActionResult RunCache(int? id)
        {
            runExpire();
            TagService.RunCache();
            TagService.RunForMenu();
            TagService.RunForHome();

            return RedirectToAction("Index", new { message = "Cache has been updated" });
        }

        [HttpGet]
        public ActionResult MailchimpLocalCacheUpdate(int? id, int? doneno)
        {
            runExpire();

            if (!doneno.HasValue)
                MailChimpService.ClearLocalCache(_personRep);

            var result = MailChimpService.SaveNextListBatch(doneno ?? 0,_personRep);
            if (result.HasValue)
            {
                ViewBag.DoneNo = result;
                return View();
            }
            else
                return RedirectToAction("Index", new { message = "Mailchimp local cache has been updated" });
        }


        [HttpGet]
        public async Task<ActionResult> FullSearcheaniseUpdate(int? id)
        {
            var result = await SearchaniseService.ReUploadToSearchanise();

            return RedirectToAction("Index", new { message = "Searchanise Service has been updated" });
        }

        [HttpGet]
        public async Task<ActionResult> UploadBatchToSearchanise(int? id, int? doneno)
        {
            runExpire();
            
            if(!doneno.HasValue)
                await SearchaniseService.ClearSearchanise();
            
            var result = await SearchaniseService.UploadBatchToSearchanise(doneno ?? 0);
            if (result.HasValue)
            {
                ViewBag.DoneNo = result;
                return View();
            }
            else
                return RedirectToAction("Index", new { message = "Searchanise Service has been updated" });
        }

        [HttpGet]
        public async Task<ActionResult> SearcheaniseGetApiKey(int? id)
        {
            var result = await SearchaniseService.GetApiKey();

            return Content(result);
        }

        [HttpGet]
        public ActionResult ProductsWithoutBrands(int? id, int? doneno)
        {
            runExpire();

            return View(_productRep.listAllFeedProductswithoutBrandOrSku().ToList());
        }

        [HttpGet]
        public ActionResult ProductsWithoutImages(int? id, int? doneno)
        {
            runExpire();

            return View(_productRep.listAllFeedProducts(false).ToList().Where(x => x.image == null).ToList());
        }

        [HttpGet]
        public ActionResult SearchaniseAdmin(int? id, int? doneno)
        {
            runExpire();

            return View();
        }

        [HttpGet]
        public ActionResult Settings(int? id)
        {
            return View();
        }

        [HttpGet]
        public ActionResult BackupDatabase(int? id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult BackupDatabase(int? id, DbBackupViewModel model)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["SigmaBaseUrl"]);
            var req = new RestRequest("StartAdhocDbBackup/", Method.POST);
            req.RequestFormat = DataFormat.Json;
            req.AddHeader("header", "Content-Type: application/json");
            req.AddHeader("Authorization", string.Format("Bearer {0}", SigmaAPIKeyHeader));
            req.AddJsonBody(new { database = "Meguiars" });

            try
            { 
                IRestResponse<DbBackupViewModel> response = client.Execute<DbBackupViewModel>(req);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = response.Data;
                }
                else
                {
                    model = new DbBackupViewModel()
                    {
                        message = string.Format("Sigma call failed:{0}{1}", response.ErrorMessage, response.Content)
                    };
                }
               
            }
            catch (Exception e)
            {
                model = new DbBackupViewModel()
                {
                    message = string.Format("Sigma connection failed:{0}", e.Message) 
                };
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult RequestFtpAccess(int? id)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["SigmaBaseUrl"]);
            var req = new RestRequest("CheckRequestIP/", Method.POST);
            req.RequestFormat = DataFormat.Json;
            req.AddHeader("header", "Content-Type: application/json");
            req.AddHeader("Authorization", string.Format("Bearer {0}", SigmaAPIKeyHeader));

            try
            {
                IRestResponse<DbBackupViewModel> response = client.Execute<DbBackupViewModel>(req);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    ViewBag.IpCheck = response.Data;
                }
                else
                {
                    ViewBag.IpCheck = new DbBackupViewModel()
                    {
                        message = string.Format("Sigma call failed:{0}{1}", response.ErrorMessage, response.Content)
                    };
                }

            }
            catch (Exception e)
            {
                ViewBag.IpCheck = new DbBackupViewModel()
                {
                    message = string.Format("Sigma call failed:{0}", e.Message)
                };
            };
               
            return View();
        }

        [HttpPost]
        public ActionResult RequestFtpAccess(int? id, DbBackupViewModel model)
        {
            var client = new RestClient(ConfigurationManager.AppSettings["SigmaBaseUrl"]);
            var req = new RestRequest("StartAdhocWhitelist/", Method.POST);
            req.RequestFormat = DataFormat.Json;
            req.AddHeader("header", "Content-Type: application/json");
            req.AddHeader("Authorization", string.Format("Bearer {0}", SigmaAPIKeyHeader));
            req.AddJsonBody(new { ipaddress = Request.UserHostAddress });

            try
            {
                IRestResponse<DbBackupViewModel> response = client.Execute<DbBackupViewModel>(req);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    model = response.Data;
                }
                else
                {
                    model = new DbBackupViewModel()
                    {
                        message = string.Format("Sigma call failed:{0}{1}", response.ErrorMessage, response.Content)
                    };
                }

            }
            catch (Exception e)
            {
                model = new DbBackupViewModel()
                {
                    message = string.Format("Sigma connection failed:{0}", e.Message)
                };
            }
            ViewBag.IpRequestResult = model;
            return View();
        }

        [HttpGet]
        public ActionResult FixOrder(int? id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult FixOrder(int? id, OrderStatusUpdaterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var fixOrder = _orderRep.listAllOrders().SingleOrDefault(x => x.ID == model.OrderToFix);
                if (fixOrder == null)
                    ModelState.AddModelError("","Could not locate the order to fix.");
                var copyOrder = _orderRep.listAllOrders().SingleOrDefault(x => x.ID == model.OrderToCopy);
                if (copyOrder == null)
                    ModelState.AddModelError("", "Could not locate the order to copy  statuses from.");


                if (ModelState.IsValid)
                {
                    fixOrder.SystemStatus = copyOrder.SystemStatus;
                    fixOrder.StatusID = copyOrder.StatusID;
                    fixOrder.PaymentType = copyOrder.PaymentType;
                    _orderRep.saveOrder(fixOrder);

                    return RedirectToAction("FixOrderCompleted");
                }

            }
            return View(model);
        }

        [HttpGet]
        public ActionResult FixOrderCompleted(int? id)
        {
            return View();
        }

        public JsonResult GetSettings()
        {
            runExpire();

            return this.Json(SettingsService.GetSettings(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult StoreSetting(SettingsService.SettingOption setting, Object value)
        {
            runExpire();

            try
            {
                SettingsService.Store(setting, value);

                return this.Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                return this.Json(new { success = false, message = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /*
        [HttpGet]
        public async Task<ActionResult> InstagramRefresh()
        {
            runExpire();

            var instService = new InstagramService(getInstagramReturnUrl(), _contentRep);
            var success = await instService.tryForUpdate();

            if (success)
            {
                ViewBag.success = true;
                return View();
            }
            else
                return Redirect("InstagramLogin");
        }
        

        private string getInstagramReturnUrl()
        {
            string returnUri = getDomainBase();
            if (Request.Url.Host == "localhost")
                returnUri += ":3617";
            returnUri += "/admin/instagramreturn/";
            return returnUri;
        }

        [HttpGet]
        public ActionResult InstagramLogin()
        {
            var service = new InstagramService(getInstagramReturnUrl(), _contentRep);

            return Redirect(service.getLoginLink());
        }

        [HttpGet]
        public async Task<ActionResult> InstagramReturn(string code)
        {
            var service = new InstagramService(getInstagramReturnUrl(), _contentRep);
            ViewBag.success = await service.recieveLoginResult(code);
            return View("InstagramRefresh");
        }
         * */

        private void checkSystemPages()
        {
            if (!_contentRep.listAllPages().Any(p => p.SystemPage == Models.Page.SystemPageOption.PRIVACY))
            {
                var page = new Page()
                {
                    SystemPage = Models.Page.SystemPageOption.PRIVACY,
                    name = "Privacy",
                    createdDate = DateTime.Now,
                    Type = Models.Page.TypeOption.CONTENT
                };
                _contentRep.savePage(page);
            }

            if (!_contentRep.listAllPages().Any(p => p.SystemPage == Models.Page.SystemPageOption.TERMS_AND_CONDITIONS))
            {
                var page = new Page()
                {
                    SystemPage = Models.Page.SystemPageOption.TERMS_AND_CONDITIONS,
                    name = "Terms and Conditions",
                    createdDate = DateTime.Now,
                    Type = Models.Page.TypeOption.CONTENT
                };
                _contentRep.savePage(page);
            }


        }

        #region Util

        public ActionResult TestSearchaniseXml()
        {


            return Content(SearchaniseService.GetProductsXmlForUpload(_productRep.listAllProducts().Where(x => !x.isHidden).OrderByDescending(x => x.ID).Take(1).ToList()));
        }

        public ActionResult UtilAddPopSubcat()
        {
            var tag = new Tag()
            {
                name = "Popular Subcat Products",
                ParentID = 37,
                Type = Tag.TypeOption.ADHOC_PRODUCT_GROUP,
                SystemTag = Tag.SystemTagOption.PRODUCT_CAT_POPULAR_SUBCAT_PRODUCTS
            };

            _contentRep.saveTag(tag);

            return Content("Pop Subcat added");
        }

        public ActionResult UtilAddDefaultWeightToVariants()
        {
            var vars = _productRep.listAllVariants().Where(v => !v.weight.HasValue || v.weight == 0).ToList();

            vars.ForEach(x =>
            {
                x.weight = 0.3m;
                _productRep.saveVariant(x);
            });

            return Content("Variant weights added");
        }

        public ActionResult UtilPopBrandsCreate()
        {
            var module = new Page()
            {
                Type = Models.Page.TypeOption.CONTENT,
                SystemPage = Models.Page.SystemPageOption.POPULAR_BRANDS,
                createdDate = DateTime.Now,
                name = "Home Page Popular Brands Section",
                html2 = @"<h3>Meguiars</h3>
    <h4>All your Favourite Brands Discounted up to 50%</h4>
    With up to 3,000 lines in stock, we offer fast, reliable delivery Australia wide and internationally.<br />
    When you shop with us, you can expect fast, friendly, personal service every time.<br />
    Our trained staff understand what good customer service is all about.<br />
    When you call us you get to talk to a real person, not a machine.<br />
    If you have an enquiry regarding your order, you get the personal attention you need to find the answer as quickly as possible. Your health is important to us, we look forward in helping you achieve the healthy lifestyle you deserve, through functional foods and nutritional supplements.<br />
    <br />
    We sincerely hope you enjoy shopping with us and you share your experience with your family and friends.<br />
    <b>Remember</b>, ""Prevention is better than a cure"".",
            };

            _contentRep.savePage(module);

            var files = new List<string>()
            {
                "carusos.png",
                "fusion.png",
                "herbsofgold.png",
                "nutralife.png",
                "balance.png",
                "amazonia.png",
                "blooms.png",
                "eko.png",
                "thompsons.png",
                "natures-sunshine.png"
            };

            var srservice = new SiteResourceService();
            var i = 1;
            files.ForEach(x => {
                var fileInfo = new FileInfo(Path.Combine(Server.MapPath("~/content/images/brandlogos/"), x));
                srservice.AddFromDisk(new SiteResource()
                {
                    ObjectID = module.getObjectID(),
                    ObjectType = module.getObjectType(),
                    PageID = module.getObjectID(),
                    FileName = x,
                    SiteResourceType = SiteResource.SiteResourceTypeOption.BRAND_LOGO,
                    orderNo = i
                }, fileInfo, _srRep);
                i++;
            });

            return Content("Special Module created");
        }

        public ActionResult UtilSpecialModuleCreate()
        {
            var module = new Page()
            {
                Type = Models.Page.TypeOption.CLICK_THROUGH,
                SystemPage = Models.Page.SystemPageOption.SPECIAL_MAILCHIMP_REGISTER_FOR_VOUCHER,
                createdDate = DateTime.Now,
                name = "Subscribe to our e-newsletter to see new products, special sales and exclusive discounts.",
                html = "* Conditions apply. value of ordered products must be equal or higher than $30.00",
                html3 = "Subscribe to our newsletter and get a $10 Voucher."
            };

            _contentRep.savePage(module);

            return Content("Special Module created");
        }

        public ActionResult UtilAddCountries()
        {
            CountryList.getCountryNames().Where(x => x != "Australia" && x != "New Zealand").ToList().ForEach(x => _shipRep.saveCountry(new Country()
            {
                Name = x,
                CountryName = x,
                createDate = DateTime.Now
            }));

            return Content("Countries have been added");
        }

        public ActionResult UtilAddAStates()
        {
            CountryList.getStateNames().ToList().ForEach(x => _shipRep.saveState(new State()
            {
                Name = x,
                CountryID = 1,
                createDate = DateTime.Now
            }));

            return Content("States have been added");
        }

        public ActionResult UtilAddMarkerTags()
        {
            var list = new List<Tuple<Tag.SystemTagOption, string, string>>(){
                new Tuple<Tag.SystemTagOption,string,string>(Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE,"#702a85","CLEARANCE"),
                new Tuple<Tag.SystemTagOption,string,string>(Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING,"#ff0000","FREE SHIPPING"),
                new Tuple<Tag.SystemTagOption,string,string>(Tag.SystemTagOption.PRODUCT_CAT_VEGETARIAN,"#48753c","VEGETARIAN"),
                new Tuple<Tag.SystemTagOption,string,string>(Tag.SystemTagOption.PRODUCT_CAT_VEGAN,"#96662d","VEGAN"),
                new Tuple<Tag.SystemTagOption,string,string>(Tag.SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE,"#1a7599","HEAT SENSITIVE"),
            };

            var parent = new Tag()
            {
                Type = Tag.TypeOption.SUPER_SECTION,
                SystemTag = Tag.SystemTagOption.SUPER_CAT_ADHOC_TAGS,
                name = "Product Tags"
            };

            _contentRep.saveTag(parent);

            list.ForEach(x => _contentRep.saveTag(new Tag()
            {
                ParentID = parent.ID,
                Type = Tag.TypeOption.ADHOC_PRODUCT_TAGS,
                SystemTag = x.Item1,
                colour = x.Item2,
                name = x.Item3
            }));

            return Content("Done");
        }

        public ActionResult UtilAddProductTags()
        {
            var productParent = _contentRep.listAllTags().Single(x => x.SystemTag == Tag.SystemTagOption.SUPER_CAT_PRODUCTS);

            var delTags = _contentRep.listAllTags().Where(x => x.ParentID == productParent.ID).ToList();

            delTags.ForEach(x =>
            {
                var children = _contentRep.listAllTags().Where(y => y.ParentID == x.ID).ToList();
                children.ForEach(y => _contentRep.deleteTag(y));
                _contentRep.deleteTag(x);
            });

            var list = new List<string>() { 
                "Supplements", "Herbs", "Bath", "Beauty", "Grocery", "Kids", "Sports", "Clearance", "Conditions", "More" 
            };

            list.ForEach(x => _contentRep.saveTag(new Tag()
            {
                ParentID = productParent.ID,
                Type = Tag.TypeOption.PRODUCT_SECTION,
                name = x
            }));

            return Content("Done");
        }


        public ActionResult RunDbUpdates()
        {
            /*
            var tags = _contentRep.listAllTags().ToList();
            foreach(var tag in tags)
            {
                _contentRep.saveTag(tag);
            }

            var tag = _contentRep.listAllTags().Where(t => t.name=="Health and Wellness").Single();
            tag.name = "General health and wellbeing";
            tag.colour = "#fbcb01";
            _contentRep.saveTag(tag);

            tag = _contentRep.listAllTags().Where(t => t.name == "Internal Health").Single();
            tag.name = "Digestive health & detoxification";
            tag.colour = "#0db14b";
            _contentRep.saveTag(tag);

            tag = _contentRep.listAllTags().Where(t => t.name == "Arthritis & Bone Support").Single();
            tag.name = "Arthritis, joint, bone & muscle health";
            tag.colour = "#f47421";
            _contentRep.saveTag(tag);

            tag = _contentRep.listAllTags().Where(t => t.name == "Hair Care").Single();
            //tag.name = "";
            tag.colour = "#f7a773";
            _contentRep.saveTag(tag);

            tag = _contentRep.listAllTags().Where(t => t.name == "Men's Health").Single();
            //tag.name = "";
            tag.colour = "#0077be";
            _contentRep.saveTag(tag);

            tag = _contentRep.listAllTags().Where(t => t.name == "Women's Health").Single();
            //tag.name = "Children's health";
            tag.colour = "#ef4d89";
            _contentRep.saveTag(tag);
            */

            /*
            var tag = new Models.Tag(){
                name = "Fish and krill oil",
                colour = "#2fc3e5",
                Type = Models.Tag.TypeOption.PRODUCT_SECTION
            };
            _contentRep.saveTag(tag);

            tag = new Models.Tag()
            {
                name = "Heart, circulation & vein health",
                colour = "#ef3a36",
                Type = Models.Tag.TypeOption.PRODUCT_SECTION
            };
            _contentRep.saveTag(tag);

            tag = new Models.Tag()
            {
                name = "Stress, anxiety, sleep & relaxation",
                colour = "#b2d33f",
                Type = Models.Tag.TypeOption.PRODUCT_SECTION
            };
            _contentRep.saveTag(tag);

            tag = new Models.Tag()
            {
                name = "Children's Health",
                colour = "#732b90",
                Type = Models.Tag.TypeOption.PRODUCT_SECTION
            };
            _contentRep.saveTag(tag);

            tag = new Models.Tag()
            {
                name = "New Products",
                colour = "#009fc2",
                Type = Models.Tag.TypeOption.PRODUCT_SECTION,
                SystemTag = Models.Tag.SystemTagOption.SPECIALS
            };
            _contentRep.saveTag(tag);

            
            tag = new Models.Tag()
            {
                name = "Best Sellers",
                colour = "#009fc2",
                Type = Models.Tag.TypeOption.PRODUCT_SECTION,
                SystemTag = Models.Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS
            };
            _contentRep.saveTag(tag);
            */
            return Content("Done");
        }

        #endregion

        #region Tests

        public ActionResult TestChimp(string email)
        {
            runExpire();

            MailChimpService.AddEmailToList(email);


            return Content("Monkey");
        }

        // GET: AccountAdmin
        public ActionResult TestLists(int? id, int? page)
        {
            runExpire();

            var list = MailChimpService.GetLists().First();

            return Content("done");
        }


        #endregion

        #region SiteMap

        public ActionResult GenerateSiteMap()
        {
            GoogeSiteMapService service = new GoogeSiteMapService();
            service.GenerateSiteMap(getSiteContents(),sitemapdomain,Server.MapPath("~/Content/SiteResources/Sitemap.xml"));
            return RedirectToAction("Index", new { message = "Sitemap.xml saved to disk" });
        }

        public ActionResult GenerateShoppingFeed()
        {
            GoogleShoppingService service = new GoogleShoppingService(_productRep,_contentRep);
            service.BuildFullProductFeed(
                Server.MapPath("~/Content/SiteResources/ShoppingFeed.xml"), 
                "https://www.discountvitaminsexpress.com.au", 
                "/Content/SiteResources/ShoppingFeed.xml",
                false);
            return RedirectToAction("Index", new { message = "ShoppingFeed.xml saved to disk" });
        }

        public ActionResult GenerateBingShoppingFeed()
        {
            GoogleShoppingService service = new GoogleShoppingService(_productRep, _contentRep);
            service.BuildFullProductFeed(
                Server.MapPath("~/Content/SiteResources/ShoppingFeedBing.xml"),
                "https://www.discountvitaminsexpress.com.au",
                "/Content/SiteResources/ShoppingFeedBing.xml",
                true);
            return RedirectToAction("Index", new { message = "ShoppingFeedBing.xml saved to disk" });
        }

        #endregion


        #region Adminwide Ajax

        public JsonResult SaveSlot(Slot slot)
        {
            if (!slot.PageID.HasValue) throw new Exception("Slot had no page id");

            Slot existing;

            existing = _contentRep.listAllSlots().SingleOrDefault(s => s.Spot == slot.Spot);
            
            if (existing == null)
            {
                _contentRep.saveSlot(slot);
            }
            else
            {
                existing.PageID = slot.PageID;
                _contentRep.saveSlot(existing);
            }

            return this.Json(new { success = true, data = (existing == null ? slot : existing) }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SearchProducts(string search, bool offline = false)
        {
            if (search.Length <= 3)
                return this.Json(null, JsonRequestBehavior.AllowGet);
            
            var products = _productRep.listMatches(search, 25, offline).OrderBy(x => x.getRank(search, search.Split(' ').ToList())).ThenBy(x => x.name).ToList();

            var results = products.Select(x => new 
            {
                x.ID,
                name = (x.isHidden?"OFFLINE - ":x.variant.isHidden?"OUT OF STOCK - ":"") + x.name,
                stock = x.variant.stockNo
            }).ToList();

            return this.Json(results.ToList(), JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}