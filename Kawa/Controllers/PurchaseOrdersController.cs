﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Data;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;
using PagedList;
using System.Data.Linq;
using AutoMapper;
using Kawa.Models.Extensions;

namespace Kawa.Controllers
{
    public class PurchaseOrdersController : AdminSiteController
    {
        // GET: PurchaseOrders
        public ActionResult Index(PurchaseOrderItemFilterViewModel.StatusSetOption? id, int? page, PurchaseOrderItemFilterViewModel filterModel)
        {
            filterModel.statusSet = id.HasValue ? (PurchaseOrderItemFilterViewModel.StatusSetOption)id : PurchaseOrderItemFilterViewModel.StatusSetOption.IN_PROGRESS ;
            runPurchaseOrderFilterSetup(id, filterModel);
            loadViewModel(new AdminViewModel());
            return View("Index", filterModel);
        }

        #region Ajax

        public JsonResult GetPurchaseOrderItemList(int? page, PurchaseOrderItemFilterViewModel filterModel)
        {
            runExpire();
            runPurchaseOrderFilterSetup(filterModel.statusSet, filterModel);
            runPurchaseOrderList(page, filterModel, filterModel.statusSet);
            var count = filterModel.purchaseOrderItems.ToList().Count();
            var pois = Mapper.Map<List<PurchaseOrderItem>, List<Models.Coms.PurchaseOrderItem>>(filterModel.purchaseOrderItems.ToList());
            return Json(new { list = pois, count = filterModel.purchaseOrderItems.TotalItemCount });
        }

        public JsonResult GetSuppliers(PurchaseOrderItem model)
        {
            runExpire();

            if (!model.ProductID.HasValue)
                return Json(new List<Supplier>());

            var product = _productRep.listAllProducts().Single(x => x.ID == model.ProductID);
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Supplier>(o => o.SupplierContacts);
                options.LoadWith<Supplier>(o => o.Tag_Suppliers);
                options.LoadWith<Tag_Supplier>(o => o.Tag);
                _db.LoadOptions = options;
                var suppliers = _db.Suppliers.Where(x => x.Tag_Suppliers.Any(y => product.brandtags.Select(z => z.ID).Contains(y.TagID))).ToList();
                if (model.SupplierID.HasValue && !suppliers.Any(x => x.ID == model.SupplierID))
                    suppliers.Add(_db.Suppliers.Single(x => x.ID == model.SupplierID));
                return Json(suppliers.OrderBy(x => x.Name).Select(x => new { x.ID, x.Name }));
            }
        }

        public JsonResult GetBulkSuppliers()
        {
            runExpire();
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<Supplier>(o => o.SupplierContacts);
                options.LoadWith<Supplier>(o => o.Tag_Suppliers);
                options.LoadWith<Tag_Supplier>(o => o.Tag);
                _db.LoadOptions = options;
                var suppliers = _db.Suppliers.ToList();
                return Json(suppliers.OrderBy(x => x.Name).Select(x => new { x.ID, x.Name }));
            }
        }

        public JsonResult SavePurchaseOrderItem(PurchaseOrderItem model)
        {
            runExpire();

            using (var _db = new dbDataContext())
            {
                if (model.ID == 0)
                {
                    model.CreateDate = DateTime.Now;
                    _db.PurchaseOrderItems.InsertOnSubmit(model);
                }
                else
                {
                    var existing = _db.PurchaseOrderItems.Single(x => x.ID == model.ID);

                    if (existing.Status != PurchaseOrderItem.StatusOption.Complete && model.Status == PurchaseOrderItem.StatusOption.Complete)
                        model.FinalisedDate = DateTime.UtcNow.UtcToLocal().Date;

                    existing.OrderID = model.OrderID;
                    existing.ProductID = model.ProductID;
                    existing.ProductName = model.ProductName;
                    existing.SupplierID = model.SupplierID;
                    existing.QuantityRequired = model.QuantityRequired;
                    existing.QuantityRecieved = model.QuantityRecieved;
                    existing.Status = model.Status;
                    existing.Notes = model.Notes;
                    existing.OrderedDate = model.OrderedDate;
                    existing.OutOfStockUntilDate = model.OutOfStockUntilDate;
                    existing.FinalisedDate = model.FinalisedDate;
                    existing.SupplierInvNo = model.SupplierInvNo;
                }
                _db.SubmitChanges();
                return Json(new { success = true });
            }
        }

        public JsonResult DeletePurchaseOrderItem(PurchaseOrderItem model)
        {
            runExpire();

            using (var _db = new dbDataContext())
            {
                _db.PurchaseOrderItems.DeleteOnSubmit(_db.PurchaseOrderItems.Single(x => x.ID == model.ID));
                _db.SubmitChanges();
                return Json(new { success = true });
            }
        }

        [HttpPost]
        public JsonResult BulkChangeStatus(List<int> poids, PurchaseOrderItem.StatusOption status, DateTime? statusDate)
        {
            runExpire();
            using (var _db = new dbDataContext())
            {
                var pois = _db.PurchaseOrderItems.Where(o => poids.Contains(o.ID)).ToList();
                pois.ForEach(o => o.Status = status);
                switch (status)
                {
                    case PurchaseOrderItem.StatusOption.Required:
                        pois.ForEach(o => o.OrderedDate = null);
                        break;
                    case PurchaseOrderItem.StatusOption.Ordered:
                        pois.ForEach(o => o.OrderedDate = statusDate);
                        break;
                    case PurchaseOrderItem.StatusOption.OOS:
                        pois.ForEach(o => o.OutOfStockUntilDate = statusDate);
                        break;
                    case PurchaseOrderItem.StatusOption.Complete:
                        pois.ForEach(o => o.FinalisedDate = statusDate ?? DateTime.Now.ToLocalTime());
                        break;
                }
                _db.SubmitChanges();
                return this.Json(new { sucess = true }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult BulkChangeSupplier(List<int> poids, int? SupplierID, string SupplierInvNo)
        {
            runExpire();
            using (var _db = new dbDataContext())
            {
                var pois = _db.PurchaseOrderItems.Where(o => poids.Contains(o.ID)).ToList();
                if(SupplierID.HasValue)
                    pois.ForEach(o => o.SupplierID = SupplierID);
                if(!string.IsNullOrEmpty(SupplierInvNo))
                    pois.ForEach(o => o.SupplierInvNo = SupplierInvNo);
                _db.SubmitChanges();
                return this.Json(new { sucess = true }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        private void runPurchaseOrderFilterSetup(PurchaseOrderItemFilterViewModel.StatusSetOption? statusSet, PurchaseOrderItemFilterViewModel filterModel)
        {
            using (var _db = new dbDataContext())
            {
                filterModel.brands = _contentRep.listAllTags().Where(t => t.Type == Models.Tag.TypeOption.BRAND_SECTION).ToList().OrderBy(x => x.name).ToList();
                filterModel.suppliers = _db.Suppliers.OrderBy(x => x.Name).ToList();
            }
        }

        private void runPurchaseOrderList(int? page, PurchaseOrderItemFilterViewModel filterModel, PurchaseOrderItemFilterViewModel.StatusSetOption? statusSet)
        {
            runExpire();
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                options.LoadWith<PurchaseOrderItem>(o => o.Order);
                options.LoadWith<PurchaseOrderItem>(o => o.Product);
                options.LoadWith<PurchaseOrderItem>(o => o.Supplier);
                options.LoadWith<Models.Data.Product>(o => o.Variants);
                _db.LoadOptions = options;

                var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)

                IQueryable<PurchaseOrderItem> pois = statusSet == PurchaseOrderItemFilterViewModel.StatusSetOption.IN_PROGRESS ? 
                            _db.PurchaseOrderItems.Where(x => x.StatusE != (int)PurchaseOrderItem.StatusOption.Complete) :
                            _db.PurchaseOrderItems.Where(x => x.StatusE == (int)PurchaseOrderItem.StatusOption.Complete);

                if (filterModel.OrderNo.HasValue)
                    pois = pois.Where(x => x.OrderID.HasValue && x.OrderID == filterModel.OrderNo);

                if (filterModel.BrandID.HasValue)
                    pois = pois.Where(x => x.Product.Tag_Products.Any(y => y.TagID == filterModel.BrandID));

                if (filterModel.SupplierID.HasValue)
                    pois = pois.Where(x => x.SupplierID == filterModel.SupplierID);

                if (!string.IsNullOrEmpty(filterModel.ProductName))
                    pois = pois.Where(x => x.Product != null && (
                            (x.ProductName != null && x.ProductName.Contains(filterModel.ProductName)) || 
                            (x.Product.name != null && x.Product.name.Contains(filterModel.ProductName)) ||
                            (x.Product.sku != null && x.Product.sku.Contains(filterModel.ProductName)) ||
                            (x.Product.poName != null && x.Product.poName.Contains(filterModel.ProductName))
                            ));

                if (filterModel.Startdate.HasValue)
                    pois = pois.Where(o => o.OrderID.HasValue && o.Order.OrderDate >= filterModel.Startdate.LocalToUtc());
                if (filterModel.Enddate.HasValue)
                    pois = pois.Where(o => o.OrderID.HasValue && o.Order.OrderDate <= ((DateTime)filterModel.Enddate.LocalToUtc()).AddDays(1));

                if (filterModel.Status.HasValue)
                    pois = pois.Where(x => x.StatusE == (int)filterModel.Status);

                if (filterModel.SupplierInvNo != null)
                    pois = pois.Where(x => x.SupplierInvNo == filterModel.SupplierInvNo);

                filterModel.purchaseOrderItems = pois.OrderByDescending(x => x.Order != null ? x.Order.OrderDate : DateTime.Now).ToPagedList(pageNumber, filterModel.pageSize);
            }
        }
    }
}