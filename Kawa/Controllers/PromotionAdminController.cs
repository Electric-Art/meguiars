﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Services;
using Kawa.Models.ViewModels;
using PagedList;


namespace Kawa.Controllers
{
    public class PromotionAdminController : AdminSiteController
    {
        public static int PAGE_SIZE = 25;

        public ActionResult Index(int? id, int? page, PromotionFilterViewModel filter)
        {
            runExpire();

            var pageNumber = page ?? 1;
            var results = _orderRep.listAllPromotions();

            if (filter.PromotionType.HasValue)
                results = results.Where(x => x.Type == filter.PromotionType);

            if (filter.Status.HasValue)
                results = results.Where(x => x.Status == filter.Status);

            if(!string.IsNullOrEmpty(filter.search))
                results = results.Where(x => (x.code != null && x.code.Contains(filter.search)) || (x.name != null && x.name.Contains(filter.search)));

            if (!string.IsNullOrEmpty(filter.searchSpecial))
                results = results.Where(x => x.notes != null && x.notes.Contains(filter.searchSpecial));


            PromotionFilterViewModel vm = new PromotionFilterViewModel()
            {
                promotions = results.ToList().OrderBy(x => x.Status).ThenByDescending(x => x.ID).ToPagedList(pageNumber, PAGE_SIZE),
                search = filter.search,
                PromotionType = filter.PromotionType
            };

            //Depreciated - now loaded in repo
            //vm.promotions.Where(x => x.LocationID.HasValue).ToList().ForEach(x => x.location = _shipRep.listAllLocations().Single(y => y.ID == x.LocationID));

            loadViewModel(new AdminViewModel());
            return View(vm);
        }

        [HttpGet]
        public ActionResult Promotion(int? id)
        {
            runExpire();
            loadViewModel(new AdminViewModel());
            //ViewBag.Products = _productRep.listAllProducts().ToList();
            ViewBag.Tags = _contentRep.listAllTags().Where(t => t.Type == Tag.TypeOption.PRODUCT_SECTION || t.Type == Tag.TypeOption.ADHOC_PRODUCT_TAGS || t.Type == Tag.TypeOption.BRAND_SECTION).ToList();
            ViewBag.Locations = _shipRep.listAllLocations().OrderBy(x => x.Name).ToList();
            ViewBag.ShippingMethods = _shipRep.listAllShippingMethods().OrderBy(x => x.Name).ToList();

            var vm = _orderRep.listAllPromotions().SingleOrDefault(x => x.ID == id) ?? new Promotion();

            if(vm.TagID.HasValue)
            {
                var tag = ((List<Tag>)ViewBag.Tags).Single(x => x.ID == vm.TagID);
                if (tag.Type == Tag.TypeOption.BRAND_SECTION)
                    vm.BrandID = vm.TagID;
                else
                    vm.CategoryID = vm.TagID;
            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult Promotion(int? id, Promotion promotion)
        {
            runExpire();
            loadViewModel(new AdminViewModel());
            //ViewBag.Products = _productRep.listAllProducts().ToList();
            ViewBag.Tags = _contentRep.listAllTags().Where(t => t.Type == Tag.TypeOption.PRODUCT_SECTION || t.Type == Tag.TypeOption.ADHOC_PRODUCT_TAGS || t.Type == Tag.TypeOption.BRAND_SECTION).ToList();
            ViewBag.Locations = _shipRep.listAllLocations().OrderBy(x => x.Name).ToList();
            ViewBag.ShippingMethods = _shipRep.listAllShippingMethods().OrderBy(x => x.Name).ToList();

            promotion.locations = Request["TagIDs"] == null ? new List<Location>() : Request["TagIDs"].Split(',').Select(s => new Location() { Name = ((List<Location>)ViewBag.Locations).Where(tg => tg.ID == int.Parse(s)).Single().Name, ID = int.Parse(s) }).ToList();

            bool usedVoucher = false;

            if (promotion.BrandID.HasValue || promotion.CategoryID.HasValue)
            {
                var tag = ((List<Tag>)ViewBag.Tags).Single(x => x.ID == (promotion.BrandID ?? promotion.CategoryID));
                if (tag.Type == Tag.TypeOption.BRAND_SECTION)
                    promotion.TagID = promotion.BrandID;
                else
                    promotion.TagID = promotion.CategoryID;

                if (promotion.BrandID.HasValue && promotion.CategoryID.HasValue)
                    ModelState.AddModelError(string.Empty, "You can set this promotion for a brand, category or product. Not a combination.");
            }

            if (promotion.ID != 0 && (_orderRep.listAllOrders(false).Any(x => x.PromotionID == promotion.ID) || _orderRep.listAllOrders(false).Any(x => x.VoucherPromotionID == promotion.ID)))
            {
                usedVoucher = true;
                var existing = _orderRep.listAllPromotions().Single(x => x.ID == promotion.ID);
                if (
                    existing.ProductID != promotion.ProductID ||
                    existing.VariantID != promotion.VariantID ||
                    existing.TagID != promotion.TagID ||
                    existing.percentOff != promotion.percentOff ||
                    existing.amountOff != promotion.amountOff ||
                    existing.freeShipping != promotion.freeShipping ||
                    existing.ShippingMethodID != promotion.ShippingMethodID ||
                    existing.Type != promotion.Type ||
                    existing.minimumSpend != promotion.minimumSpend ||
                    existing.maximumWeight != promotion.maximumWeight ||
                    !existing.locationTheSame(promotion.locations) ||
                    existing.freeShipping != promotion.freeShipping ||
                    existing.ShippingMethodID != promotion.ShippingMethodID
                    )
                { }
                    ///ModelState.AddModelError(string.Empty, "Sorry this promotion has already been used with an order.  You can no longer change the product, category, percent off or amount off.");
            }


            if (
                    (promotion.percentOff.HasValue && promotion.amountOff.HasValue) ||
                    (promotion.percentOff.HasValue && promotion.freeShipping) ||
                    (promotion.amountOff.HasValue && promotion.freeShipping)
                )
            {
                ModelState.AddModelError(string.Empty, "Please provide an amount off OR a percentage off OR Free Shipping, you cannot combine effects.");
            }




            if (promotion.ProductID.HasValue)
            {
                ///Product pr = _productRep.listAllProducts(false, true).Where(x => x.ID == promotion.ProductID).FirstOrDefault();
                ///promotion.product = pr;
            }

            if (promotion.ProductID.HasValue && promotion.TagID.HasValue)
            {
                ModelState.AddModelError(string.Empty, "You can set this promotion for a brand, category or product. Not a combination.");
            }

            if (promotion.Type != Models.Promotion.TypeOption.PROMOTION && _orderRep.listAllPromotions().Any(v => v.ID != promotion.ID && v.code.ToLower() == promotion.code.ToLower()))
            {
                ModelState.AddModelError(string.Empty, "An existing voucher already has this code.");
            }

            if (promotion.Type == Models.Promotion.TypeOption.PROMOTION && !string.IsNullOrEmpty(promotion.code))
            {
                ModelState.AddModelError(string.Empty, "This item is a promotion, however you have added a code, do you need to change this item to a voucher?");
            }

            
            if (ModelState.IsValid)
            {
                if(promotion.ID == 0)
                    promotion.createDate = DateTime.Now;

                if (usedVoucher)
                {
                    var existing = _orderRep.listAllPromotions().Single(x => x.ID == promotion.ID);
                    existing.isHidden = promotion.isHidden;
                    existing.name = promotion.name;
                    existing.code = promotion.code;
                    existing.expiryDate = promotion.expiryDate;
                    existing.SKU = promotion.SKU;
                    existing.minimumSpend = promotion.minimumSpend;
                    promotion = existing;
                }

                _orderRep.savePromotion(promotion,true);
                return RedirectToRoute("PromotionAdminIndex", new { id="" });
            }
            else
            {
                return View(promotion);
            }
        }

        [HttpGet]
        public ActionResult PromotionDelete(int? id)
        {
            runExpire();
            var promotion = _orderRep.listAllPromotions().Single(v => v.ID == id);
            if (_orderRep.listAllOrders().Any(o => o.PromotionID == id) || _orderRep.listAllOrders().Any(o => o.VoucherPromotionID == id))
            {
                promotion.isHidden = true;
                _orderRep.savePromotion(promotion, false);
            }
            else
            { 
                _orderRep.deletePromotion(promotion);
            }
            return RedirectToRoute("PromotionAdminIndex", new { id = "" });
        }

        [HttpPost]
        public ActionResult VariantDropdown(int? id, int? ProductID, int? VariantID)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                product = ProductID.HasValue ? _productRep.listAllProducts().Single(p => p.ID==ProductID) : null
            };

            if (vm.product != null)
                vm.product.variants = _productRep.listAllVariants().Where(v => v.ProductID == ProductID).ToList();
            else
            {
                vm.product = new Product()
                {
                    variants = new List<Variant>()
                };
            }

            return PartialView(vm);
        }

        #region  Util

        [HttpGet]
        public ActionResult CreateSpecialPromotion(int? id)
        {
            var freeShippingPromo = new Promotion()
            {
                Type = Models.Promotion.TypeOption.PROMOTION,
                SystemType = Models.Promotion.SystemTypeOption.FREE_SHIPPING_PRODUCT_IN_CART,
                name = "Free shipping product in cart",
                maximumWeight = 4,
                freeShipping = true,
                createDate = DateTime.Now
            };

            _orderRep.savePromotion(freeShippingPromo,false);

            return Content("done");
        }

        #endregion

    }
}