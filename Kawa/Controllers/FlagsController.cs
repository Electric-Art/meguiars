﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;

namespace Kawa.Controllers
{
    public class FlagsController : AdminSiteController
    {
        // GET: Flag
        [HttpGet]
        public ActionResult Index(FlagMessage? msg)
        {
            loadViewModel(new AdminViewModel());
            ViewBag.Countries = _shipRep.listAllCountries().ToList();
            switch (msg)
            {
                case FlagMessage.FlagSaved:
                    ViewBag.Message = "Your flag has been saved";
                    break;
                case FlagMessage.FlagDeleted:
                    ViewBag.Message = "Your flag has been deleted";
                    break;
            }

            return View(new FlagAdminViewModel() { Flags = _orderRep.listAllFlags().OrderBy(x => x.Name).ToList(), Search = new FlagTrigger(), Flag = new Flag() });
        }

        [HttpPost]
        public ActionResult Index(FlagMessage? msg, FlagTrigger search, Flag flag)
        {
            loadViewModel(new AdminViewModel());
            ViewBag.Countries = _shipRep.listAllCountries().ToList();
            return View(new FlagAdminViewModel() {
                Flags = new FlagService(_orderRep).SearchFlags(search, flag),
                Search = search,
                Flag = flag
            });
        }

        [HttpGet]
        public ActionResult Flag(int? id)
        {
            loadViewModel(new AdminViewModel());
            var model = _orderRep.listAllFlags().SingleOrDefault(x => x.ID == id) ?? new Flag()
            {

            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Flag(int? id, Flag model)
        {
            loadViewModel(new AdminViewModel());
            model.Labels = Request.Form["Labels"];
            
            if (ModelState.IsValid)
            {
                var isNew = model.ID == 0;
                if (isNew)
                    model.createDate = DateTime.Now;
                _orderRep.saveFlag(model);

                new FlagService(_orderRep).SetFlagsCache();

                return isNew ?
                    RedirectToAction("FlagTriggers", new { id = model.ID }) :
                    RedirectToAction("Index", new { msg = FlagMessage.FlagSaved });
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult FlagDelete(int? id)
        {
            _orderRep.deleteFlag(new Flag() { ID = (int)id });
            new FlagService(_orderRep).SetFlagsCache();
            return RedirectToAction("Index", new { msg = FlagMessage.FlagDeleted });
        }

        public ActionResult FlagTriggers(int? id)
        {
            loadViewModel(new AdminViewModel());
            ViewBag.Flag = _orderRep.listAllFlags().Single(x => x.ID == id);

            return View(_orderRep.listAllFlagTriggers().Where(x => x.FlagID == (int)id).ToList());
        }

        [HttpGet]
        public ActionResult FlagTrigger(int? id, int? FlagID)
        {
            loadViewModel(new AdminViewModel());
            var model = _orderRep.listAllFlagTriggers().SingleOrDefault(x => x.ID == id) ?? new FlagTrigger()
            {
                FlagID = (int)FlagID
            };
            ViewBag.Countries = _shipRep.listAllCountries().ToList();
            return View(model);
        }

        [HttpPost]
        public ActionResult FlagTrigger(int? id, FlagTrigger model)
        {
            loadViewModel(new AdminViewModel());

            if (ModelState.IsValid)
            {
                _orderRep.saveFlagTrigger(model);
                new FlagService(_orderRep).SetFlagsCache();
                return RedirectToAction("FlagTriggers", new { id = model.FlagID });
            }

            ViewBag.Countries = _shipRep.listAllCountries().ToList();
            return View(model);
        }

        [HttpGet]
        public ActionResult FlagTriggerDelete(int? id)
        {
            var model = _orderRep.listAllFlagTriggers().SingleOrDefault(x => x.ID == id);
            _orderRep.deleteFlagTrigger(model);
            new FlagService(_orderRep).SetFlagsCache();
            return RedirectToAction("FlagTriggers", new { id = model.FlagID, msg = FlagMessage.FlagTriggerDeleted });
        }

        public enum FlagMessage {
            FlagSaved,
            FlagDeleted,
            FlagTriggerDeleted
        }
    }
}