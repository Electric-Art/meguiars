﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class RedirectController : SiteController
    {
        /* Switch this value to toggle between outputing the redirect planned vs actually doing the redirect */
        private bool debug = false;

        List<StoredRedirect> indexphpRedirects = new List<StoredRedirect>()
        {
            new StoredRedirect(){ inurl="index.php", outurl="/" },
            new StoredRedirect(){ inurl="index.php?dispatch=sitemap.view", outurl="/sitemap/" },
            new StoredRedirect(){ inurl="index.php?dispatch=profiles.add", outurl="/Account/Register" },
            new StoredRedirect(){ inurl="index.php?dispatch=categories.view&category_id=2752", outurl="/category/brands?mode=brands" },
            new StoredRedirect(){ inurl="index.php?dispatch=gift_certificates.add", outurl="/articles/92/gift-vouchers" },
            new StoredRedirect(){ inurl="index.php?dispatch=wishlist.view", outurl="/Account/Favourites" },
            new StoredRedirect(){ inurl="index.php?dispatch=product_features.compare", outurl="/Account/Favourites" },
            new StoredRedirect(){ inurl="index.php?dispatch=news.list", outurl="/about/" },
            new StoredRedirect(){ inurl="index.php?dispatch=checkout.cart", outurl="/shop" },
            new StoredRedirect(){ inurl="index.php?dispatch=checkout.checkout", outurl="/shop" },
            new StoredRedirect(){ inurl="index.php?dispatch=profiles.update", outurl="/Account/Login" },
            new StoredRedirect(){ inurl="index.php?dispatch=orders.search", outurl="/Account" },
        };

        public ActionResult Product(string id)
        {
            if (id == null)
                redirectNotFound();

            if (IsSiteHtml())
                return HtmlOut();

            var product = _productRep.listAllProducts(true, true).SingleOrDefault(p => p.nameUrl == id.ToLower() && !p.isHidden && p.HasVariant);
            if (product != null)
            {
                return redirect(product.getUrl());
            }
            else
            {
                var alias = _productRep.listAllProducts(true, true).FirstOrDefault(p => p.aliasesUrl.Contains(id.ToLower()) && !p.isHidden && p.HasVariant);
                if (alias != null)
                    return redirect(alias.getUrl());
                return redirect("/products");
            }
        }

        

        public ActionResult Html(string id)
        {
            if (IsSiteHtml())
                return HtmlOut();
            return redirectNotFound();
        }

        public ActionResult ClassicAsp(string id)
        {
            return redirectNotFound();
        }

        public ActionResult Category(string id)
        {
            if (id == null)
                redirectNotFound();

            var tag = _contentRep.listAllTagsWithProductList(true).FirstOrDefault(p => p.nameUrl == id && !p.isHidden);
            if (tag != null)
            {
                return redirect(tag.getUrl());
            }
            else
            {
                var alias = _contentRep.listAllTagsWithProductList().FirstOrDefault(p => (p.aliases.Contains(id.ToLower()) || p.aliasesUrl.Contains(id.ToLower())) && !p.isHidden);
                if (alias != null)
                    return redirect(alias.getUrl());
                return redirect("/products");
            }
        }

        public bool IsSiteHtml()
        {
            return System.IO.File.Exists(Server.MapPath("~/html/" + Request.Path));
        }

        public ActionResult HtmlOut()
        {
            return Content(System.IO.File.ReadAllText(Server.MapPath("~/html/" + Request.Path)));
        }

        public ActionResult IndexPhp(string id)
        {
            var redirectItem = indexphpRedirects.Where(x => Request.Url.ToString().ToLower().EndsWith(x.inurl)).FirstOrDefault();

            if (id == null)
                redirectNotFound();

            return redirect(redirectItem.outurl);
        }

        public ActionResult Static(string id)
        {
            if (id == null)
                redirectNotFound();

            var path = id;

            return redirect(path);
        }

        /* I put this in as the browser seemed (understandibly) to remember the perminant redirect when debug was set to false.
         * You can solve that by a browser cache dump I found, but this should help suring the test phase too I think. 
         */
        protected void runExpire()
        {
            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddYears(-1);
            Response.Expires = 0;
            Response.CacheControl = "no-cache";

            ViewBag.site = null;
        }

        private ActionResult redirect(string path)
        {
            runExpire();
            if (path == null)
                return redirectNotFound();

            if (debug)
                return View("Debug", new DebugViewModel() { redirectpath = path });
            else
                return new PermanentRedirectResult() { Url = path };
        }

        private ActionResult redirectNotFound()
        {
            runExpire();

            if (debug)
                return View("Debug", new DebugViewModel() { redirectpath = "Not Found" });
            else
                return new PermanentRedirectResult() { Url = "/" };
        }

	}
}

public class StoredRedirect
{
    public string inurl { get; set; }
    public string outurl { get; set; }
}

public class PermanentRedirectResult : ActionResult
{
    public string Url;

    public override void ExecuteResult(ControllerContext context)
    {
        context.HttpContext.Response.StatusCode = 301;
        context.HttpContext.Response.RedirectLocation = this.Url;
        context.HttpContext.Response.End();
    }
}

namespace Kawa.Models.ViewModels
{
    public class DebugViewModel
    {
        public string redirectpath { get; set; }
        public string message { get; set; }
    }

}
