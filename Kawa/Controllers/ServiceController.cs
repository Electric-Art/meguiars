﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models.Extensions;
using Kawa.Models.Services;

namespace Kawa.Controllers
{
    public class ServiceController : SiteController
    {
        // GET: Service
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NightlyMaintain(Guid id)
        {
            if (id != new Guid("913e85e6-199a-409c-aca4-7585438ff068"))
                return Content("Action denied");

            var startFull = DateTime.Now;
            var progressLog = string.Format("Meguiars daily service running at {0}. <br/>", startFull);
            
            var start = DateTime.Now;
        
            progressLog += string.Format("TagService.RunCache took {0} seconds. <br/>", (DateTime.Now - start).Seconds);


            System.Net.Mail.MailMessage mmx = new System.Net.Mail.MailMessage(ConfigurationManager.AppSettings["reportfrom_email"], ConfigurationManager.AppSettings["report_email"]);
            mmx.Subject = "Meguiars Daily Import : " + DateTime.Now.UtcToLocal().ToLongDateString() + " " + DateTime.Now.UtcToLocal().ToLongTimeString();
            mmx.IsBodyHtml = true;
            mmx.Body = "Started to run process at " + DateTime.Now.UtcToLocal().ToShortDateString() + " - " + DateTime.Now.UtcToLocal().ToLongTimeString() + GetInfo();
            //EmailSendService.Send(mmx);
            string report = "";
            DateTime started = DateTime.Now;
            DateTime finished = DateTime.Now;
            TimeSpan span = finished.Subtract(started);
            DateTime substarted = DateTime.Now;

            try
            {

                report += "Started: " + started.UtcToLocal().ToString() + "<BR><ul>";
                report += PropellaService.UpdateCategories() + "<br>";
                finished = DateTime.Now;
                span = finished.Subtract(started);
                report += "(" + span.TotalSeconds.ToString() + " secs)<BR>";
            }
            catch (Exception ex)
            {
                report += ex.Message + "<h2>MEGUIARSERROR</h2><P>" + ex.StackTrace + "</p>";
            }

            try
            {
                substarted = DateTime.Now;
                report += PropellaService.UpdateProducts(3) + "<br>";
                finished = DateTime.Now;
                span = finished.Subtract(substarted);
                report += "(" + span.TotalSeconds.ToString() + " secs)<BR>";
            }
            catch (Exception ex)
            {
                report += ex.Message + "<h2>MEGUIARSERROR</h2><P><i>" + ex.StackTrace + "</i></p>";
            }
            try
            {
                substarted = DateTime.Now;
                report += PropellaService.UpdateOutOfStock() + "<br>";
                finished = DateTime.Now;
                span = finished.Subtract(substarted);
                report += "(" + span.TotalSeconds.ToString() + " secs)<BR>";
            }
            catch (Exception ex)
            {
                report += ex.Message + "<h2>MEGUIARSERROR</h2><P><i>" + ex.StackTrace + "</i></p>";
            }

            try
            {
                substarted = DateTime.Now;
                report += PropellaService.UpdatePromotions() + "<BR>";
                finished = DateTime.Now;
                span = finished.Subtract(substarted);
                report += "(" + span.TotalSeconds.ToString() + " secs)<BR>";
            }
            catch (Exception ex)
            {
                report += ex.Message + "<h2>MEGUIARSERROR</h2><P>" + ex.StackTrace + "</p>";
            }

            try
            {
                substarted = DateTime.Now;
                report += PropellaService.suckNEWSmitsData() + "<BR>";
                report += "(" + span.TotalSeconds.ToString() + " secs)<BR>";
                span = finished.Subtract(substarted);

            }
            catch (Exception ex)
            {
                report += ex.Message + "<h2>MEGUIARSERROR</h2><P>" + ex.StackTrace + "</p>";
            }

            finished = DateTime.Now;
            report += "</ul>Finished: " + finished.UtcToLocal().ToString() + "<BR>";
            span = finished.Subtract(started);
            report += "Process time: <b>" + span.TotalSeconds.ToString() + " seconds</b><BR>";
            ViewBag.message = report;


            progressLog += report;

            GoogeSiteMapService service = new GoogeSiteMapService();
            service.GenerateSiteMap(getSiteContents(), sitemapdomain, Server.MapPath("~/Content/SiteResources/Sitemap.xml"));



            progressLog += string.Format("<BR><BR>Full service took {0} seconds. <br/>Service complete.", (DateTime.Now - startFull).Seconds);

            System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(ConfigurationManager.AppSettings["reportfrom_email"], ConfigurationManager.AppSettings["report_email"]);
            ///mm.CC.Add(ConfigurationManager.AppSettings["reportcc_email"]);

            mm.Subject = "Meguiars Daily Import : " + DateTime.Now.UtcToLocal().ToLongDateString() + " " + DateTime.Now.UtcToLocal().ToLongTimeString();
            mm.IsBodyHtml = true;
            mm.Body = progressLog + GetInfo();


            TagService.RunForMenu();
            TagService.RunCache();
            TagService.RunForHome();

            EmailSendService.Send(mm);

            return Content(progressLog);
        }

        private string GetInfo()
        {
            string returninfo = "<P><P><P>";
            foreach (string var in Request.ServerVariables)
            {
                returninfo += var + " " + Request[var] + "<br>";
            }
            return returninfo;
        }

        public ActionResult GenerateGuid()
        {
            return Content(Guid.NewGuid().ToString());
        }
    }
}