﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Services;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    [Authorize(Roles = AdminRole)]
    public class AdminSiteController : SiteController
    {
        // GET: AdminSite
        protected void loadAdminViewModel()
        {
            loadViewModel(new AdminViewModel());
        }

        protected void loadViewModel(AdminViewModel viewModel)
        {
            if (Request.HttpMethod == "GET")
            {

                switch (Request["m"])
                {
                    case "s":
                        viewModel.message = "Your changes have been saved";
                        viewModel.success = true;
                        break;
                    case "d":
                        viewModel.message = "Your item was deleted";
                        viewModel.success = true;
                        break;
                }
            }
            if (!string.IsNullOrEmpty(Request["t"]))
            {
                viewModel.tag = _contentRep.listAllTags().Where(t => t.ID == int.Parse(Request["t"])).Single();
            }
            else
            {
                /*
                if (viewModel.page != null && viewModel.tag == null)
                    viewModel.tag = _contentRep.listAllTagsForPage(viewModel.page).Where(t => t.Type == Tag.TypeOption.SECTION).SingleOrDefault();*/
            }
            ViewBag.SiteViewModel = viewModel;
        }

        #region Page Admin components

        protected ActionResult runPageAdminGet(AdminViewModel vm, int? t, Page.TypeOption? type, string viewPath)
        {
            loadViewModel(vm);
            
            if (vm.page.ID == 0 && type.HasValue)
            {
                vm.page.Type = (Page.TypeOption)type;
            }

            pageSrSetrup(vm);
            loadSrs(vm.srOwner, true);

            if (vm.page.ID == 0 && t.HasValue)
            {
                vm.page.tags = new List<Tag>() { vm.tags.Single(tg => tg.ID == t) };
            }
            else
            {
                vm.page.tags = _contentRep.listAllTagsForPage(vm.page).ToList();
            }

            return View(viewPath, vm);
        }

        protected ActionResult runPageAdminPost(AdminViewModel vm, Page page, string route, AdminPageRouteVariables routeValues, string viewPath)
        {

            pageSrSetrup(vm);
            vm.page.tags = Request["TagIDs"] == null ? new List<Tag>() : Request["TagIDs"].Split(',').Select(s => new Tag() { name = vm.tags.Where(tg => tg.ID == int.Parse(s)).Single().name, ID = int.Parse(s) }).ToList();
            vm.page.pages = Request["PageIDs"] == null ? new List<Page>() : Request["PageIDs"].Split(',').Select(s => new Page() { name = vm.pages.Where(tg => tg.ID == int.Parse(s)).Single().name, ID = int.Parse(s) }).ToList();
            

            if (ModelState.IsValid)
            {
                if (page.ID == 0)
                {
                    page.createdDate = DateTime.Now;
                }

                _contentRep.savePage(page);

                collectImagePosts(page);
                saveExistingImages(page);

                TagService.RunForMenu();

                if (!ModelState.IsValid)
                {
                    loadSrs(vm.srOwner, true);
                    loadViewModel(vm);
                    return View(viewPath,vm);
                }

                if (routeValues.injectID)
                    routeValues.id = page.ID;

                return RedirectToRoute(route, routeValues);
            }
            else
            {
                vm.message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));

                loadSrs(vm.srOwner, true);
                loadViewModel(vm);
                return View(viewPath,vm);
            }
        }


        #endregion


        #region Site Resources


        [HttpPost]
        public ActionResult SiteResources(SiteResource siteResource)
        {
            runExpire();

            string action = siteResource.action;

            if (siteResource.ID > 0)
            {
                siteResource = _srRep.listAll().Where(s => s.ID == siteResource.ID).SingleOrDefault();
            }

            var vm = new AdminViewModel
            {
                srOwner = getSrOwner(siteResource)
            };

            SiteResourceService srm = new SiteResourceService();
            SiteResource sr;
            List<SiteResource> children;
            List<SiteResource> savers;
            switch (action)
            {
                case "delete":
                    if (siteResource != null)
                    {
                        savers = OrdererService.delete(siteResource, _srRep.listAll(vm.srOwner).Cast<IOrderedObject>().ToList()).Cast<SiteResource>().ToList();
                        foreach (var obj in savers)
                        {
                            _srRep.saveSiteResource(obj);
                        }

                        srm.Delete(siteResource, _srRep);
                        _srRep.deleteSiteResource(siteResource);
                        vm.success = true;
                    }
                    else
                    {
                        vm.message = "This image has already been deleted";
                        vm.success = false;
                    }
                    break;
                case "up":
                    sr = _srRep.listAll().Where(s => s.ID == siteResource.ID).SingleOrDefault();
                    children = _srRep.listAll().Where(s => s.ObjectID == sr.ObjectID && s.ObjectType == sr.ObjectType).ToList();
                    savers = OrdererService.moveUp(sr, children.Cast<IOrderedObject>().ToList()).Cast<SiteResource>().ToList();
                    foreach (var obj in savers)
                    {
                        _srRep.saveSiteResource(obj);
                    }
                    break;
                case "down":
                    sr = _srRep.listAll().Where(s => s.ID == siteResource.ID).SingleOrDefault();
                    children = _srRep.listAll().Where(s => s.ObjectID == sr.ObjectID && s.ObjectType == sr.ObjectType).ToList();
                    savers = OrdererService.moveDown(sr, children.Cast<IOrderedObject>().ToList()).Cast<SiteResource>().ToList();
                    foreach (var obj in savers)
                    {
                        _srRep.saveSiteResource(obj);
                    }
                    break;
            }

            loadSrs(vm.srOwner, true);

            if (vm.srOwner is Page)
            {
                vm.page = (Page)vm.srOwner;
                pageSrSetrup(vm);
            }
            else if (vm.srOwner is Tag)
            {
                vm.tag = (Tag)vm.srOwner;
                tagSrSetrup(vm);
            }
            else if (vm.srOwner is PrescriptionTag)
            {
                vm.prescriptiontag = (PrescriptionTag)vm.srOwner;
                prescriptiontagSrSetrup(vm);
            }
            else if (vm.srOwner is Product)
            {
                vm.product = (Product)vm.srOwner;
                productSrSetrup(vm);
            }

            vm.isPartial = true;

            return PartialView(vm.product != null ? "_SiteResourcesEdit" : "_SiteResources", vm);
        }


        [HttpPost]
        public virtual JsonResult SiteResourceMoveDown(int SiteResourceID)
        {
            AjaxRequestResult result = new AjaxRequestResult();

            SiteResource sr = _srRep.listAll().Where(s => s.ID == SiteResourceID).SingleOrDefault();
            List<SiteResource> children = _srRep.listAll().Where(s => s.ObjectID == sr.ObjectID && s.ObjectType == sr.ObjectType).ToList();
            List<SiteResource> savers = OrdererService.moveDown(sr, children.Cast<IOrderedObject>().ToList()).Cast<SiteResource>().ToList();
            foreach (var obj in savers)
            {
                _srRep.saveSiteResource(obj);
            }

            result.success = true;
            result.message = sr.orderNo.ToString();
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public virtual JsonResult SiteResourceMoveUp(int SiteResourceID)
        {
            AjaxRequestResult result = new AjaxRequestResult();

            SiteResource sr = _srRep.listAll().Where(s => s.ID == SiteResourceID).SingleOrDefault();
            List<SiteResource> children = _srRep.listAll().Where(s => s.ObjectID == sr.ObjectID && s.ObjectType == sr.ObjectType).ToList();
            List<SiteResource> savers = OrdererService.moveUp(sr, children.Cast<IOrderedObject>().ToList()).Cast<SiteResource>().ToList();
            foreach (var obj in savers)
            {
                _srRep.saveSiteResource(obj);
            }

            result.success = true;
            result.message = sr.orderNo.ToString();
            return this.Json(result, JsonRequestBehavior.AllowGet);
        }

        protected void loadSrs(SiteResourceOwner sro, bool addNew)
        {
            List<SiteResource> srs = _srRep.listAll(sro).ToList();

            foreach (var sr in srs)
            {
                sr.url = SiteResourceService.getPath(sr);
            }

            if (addNew)
            {
                srs.Add(new SiteResource());
            }

            sro.siteResources = srs.ToArray();
        }

        protected SiteResourceOwner getSrOwner(SiteResource sr)
        {
            switch (sr.ObjectType)
            {
                case SiteResourceOwner.ObjectTypeOption.PAGE:
                    return _contentRep.listAllPages().Single(x => x.ID==sr.ObjectID);

                case SiteResourceOwner.ObjectTypeOption.PRODUCT:
                    return _productRep.listAllProducts().Single(x => x.ID==sr.ObjectID);

                case SiteResourceOwner.ObjectTypeOption.TAG:
                    return _contentRep.listAllTags().Single(x => x.ID == sr.ObjectID);
                case SiteResourceOwner.ObjectTypeOption.PRESCRIPTIONTAG:
                    return _contentRep.listAllPerscriptionTags().Single(x => x.ID == sr.ObjectID);

            }
            return null;
        }

        protected void collectImagePosts(SiteResourceOwner srOwner)
        {
            SiteResourceService srm = new SiteResourceService();
            foreach (string name in Request.Files)
            {
                var file = Request.Files[name];
                if (file.FileName != string.Empty)
                {
                    string fileName = System.IO.Path.GetFileName(file.FileName);
                    SiteResource image = srOwner.siteResources.Where(s => s.ID == 0 && s.FileName != null).FirstOrDefault();

                    if (image == null)
                    {
                        continue;
                    }
                    image.FileName = fileName;
                    //image.Caption = Request["description"];

                    //image.SiteResourceType = SiteResource.SiteResourceTypeOption.CONTENT;

                    image.ObjectID = srOwner.getObjectID();
                    image.ObjectType = srOwner.getObjectType();

                    switch(image.ObjectType)
                    {
                        case SiteResourceOwner.ObjectTypeOption.PAGE:
                            image.PageID = image.ObjectID;
                            break;
                        case SiteResourceOwner.ObjectTypeOption.PRODUCT :
                            image.ProductID = image.ObjectID;
                            break;
                        case SiteResourceOwner.ObjectTypeOption.TAG:
                            image.TagID = image.ObjectID;
                            break;
                        case SiteResourceOwner.ObjectTypeOption.PRESCRIPTIONTAG:
                            image.PrescriptionTagID = image.ObjectID;
                            break;
                    }

                    List<SiteResource> children = _srRep.listAll(srOwner).ToList();
                    OrdererService.setNextOrderNo((IOrderedObject)image, children.Cast<IOrderedObject>().ToList());
                    try
                    { 
                        //TODO
                        srm.SaveJob(file, image);
                        _srRep.saveSiteResource(image);
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError(string.Empty, e.Message);
                    }
                    /*
                    if (image.SiteResourceType == SiteResource.SiteResourceTypeOption.HEADER)
                    {
                        SiteResourceService.ResizeImageThumbnail(image, 750, 300, false);
                    }
                    */
                }
            }
        }

        protected void saveExistingImages(SiteResourceOwner srOwner)
        {
            foreach (var sr in srOwner.siteResources)
            {
                if (sr.ID != 0)
                {
                    SiteResource siteRes = _srRep.listAll().Where(s => s.ID == sr.ID).Single();
                    siteRes.Caption = sr.Caption;
                    siteRes.allowPostCard = sr.allowPostCard;
                    _srRep.saveSiteResource(siteRes);
                }
            }
        }

        protected void deleteExistingImages(SiteResourceOwner srOwner)
        {
            SiteResourceService srm = new SiteResourceService();
            foreach (var sr in srOwner.siteResources)
            {
                if (sr.ID != 0)
                {
                    SiteResource siteRes = _srRep.listAll().Where(s => s.ID == sr.ID).Single();
                    srm.Delete(siteRes, _srRep);
                }
            }
        }

        #endregion Site Resources

        #region Object Site Resource Relations

        protected void pageSrSetrup(AdminViewModel vm)
        {
            vm.srOwner = vm.page;
            if (vm.page.tags == null)
                vm.page.tags = _contentRep.listAllTagsForPage(vm.page).ToList();

            List<SiteResource.SiteResourceTypeOption> options = ((SiteResource.SiteResourceTypeOption[])Enum.GetValues(typeof(SiteResource.SiteResourceTypeOption))).ToList();
            vm.srTypes = new List<SiteResource.SiteResourceTypeOption>();

            if (vm.page.SystemPage == Page.SystemPageOption.POPULAR_BRANDS)
            {
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.BRAND_LOGO);
                return;
            }

            if (vm.page.Type == Page.TypeOption.BANNERS)
            {
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.BILLBOARD);
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.BILLBOARD_MOBILE);
                return;
            }

            if (vm.page.Type == Page.TypeOption.CLICK_THROUGH)
            {
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.ARTICLE_FEATURED_WIDE);
                return;
            }

            if (vm.page.SystemPage == Page.SystemPageOption.HOME_PAGE_SLIDE_SHOW)
            {
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.SLIDESHOW_IMAGE);
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.SLIDESHOW_MP4);
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.SLIDESHOW_MOBILE_MP4);
                return;
            } else
            { 
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.IN_CONTENT);
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.FILE_FOR_DOWNLOAD);
            }


            if (vm.page.SystemPage == Page.SystemPageOption.HOME_PAGE_SLIDE_SHOW)
            {
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.BILLBOARD);
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.BILLBOARD_MOBILE);
            }
            if (vm.page.Type == Page.TypeOption.CATALOG_CONTAINER)
            {
                vm.srTypes.Clear();
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.CONSUMER_CATALOG);
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.PROFESSIONAL_CATALOG);
                vm.srTypes.Add(SiteResource.SiteResourceTypeOption.MARINE_CATALOG);
            }


        }


        protected void productSrSetrup(AdminViewModel vm)
        {
            vm.srOwner = vm.product;
            vm.srTypes = ((SiteResource.SiteResourceTypeOption[])Enum.GetValues(typeof(SiteResource.SiteResourceTypeOption))).Where(v => 
                v == SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE ||
                v == SiteResource.SiteResourceTypeOption.XTRA_PRODUCT_IMAGE ||
                v == SiteResource.SiteResourceTypeOption.IN_CONTENT).ToList();
        }

        protected void tagSrSetrup(AdminViewModel vm)
        {
            vm.srOwner = vm.tag;
            vm.srTypes = ((SiteResource.SiteResourceTypeOption[])Enum.GetValues(typeof(SiteResource.SiteResourceTypeOption))).Where(v =>
                v == SiteResource.SiteResourceTypeOption.BILLBOARD ||
                v == SiteResource.SiteResourceTypeOption.BILLBOARD_MOBILE).ToList();
            if(vm.tag.Type == Tag.TypeOption.CAR_PRESCRIPTION_CAT)
            {
                vm.srTypes = ((SiteResource.SiteResourceTypeOption[])Enum.GetValues(typeof(SiteResource.SiteResourceTypeOption))).Where(v =>
                v == SiteResource.SiteResourceTypeOption.IMAGE ).ToList();
            }

        }

        protected void prescriptiontagSrSetrup(AdminViewModel vm)
        {
            vm.srOwner = vm.prescriptiontag;
            vm.srTypes = ((SiteResource.SiteResourceTypeOption[])Enum.GetValues(typeof(SiteResource.SiteResourceTypeOption))).Where(v =>
                v == SiteResource.SiteResourceTypeOption.BILLBOARD ||
                v == SiteResource.SiteResourceTypeOption.BILLBOARD_MOBILE).ToList();
            if (vm.prescriptiontag.Type == PrescriptionTag.TypeOption.CAR_PRESCRIPTION_CAT)
            {
                vm.srTypes = ((SiteResource.SiteResourceTypeOption[])Enum.GetValues(typeof(SiteResource.SiteResourceTypeOption))).Where(v =>
                v == SiteResource.SiteResourceTypeOption.IMAGE).ToList();
            }

        }
        /*
        protected void ingredientSrSetrup(AdminViewModel vm)
        {
            vm.srOwner = vm.ingredient;
            vm.srTypes = ((SiteResource.SiteResourceTypeOption[])Enum.GetValues(typeof(SiteResource.SiteResourceTypeOption))).Where(v => v == SiteResource.SiteResourceTypeOption.INGREDIENT_IMAGE).ToList();
        }
        */
        #endregion

        public class AjaxRequestResult
        {
            public bool success = false;
            public string message = string.Empty;
            public object data;
        }
    }
}