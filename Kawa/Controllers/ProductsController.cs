﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Extensions;
using PagedList;
using WebMarkupMin.AspNet4.Mvc;

namespace Kawa.Controllers
{
    [MinifyHtml]
    public class ProductsController : SiteController
    {
        public static int MAX_RETURNED_PRODUCTS = 500;
        
        // GET: Products
        public ActionResult Index(string id, int? page, int? pageSize,
                                PostedResultFilters postedResultFilters, 
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] PriceIDs,
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] BrandIDs,
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] SpecialtiesIDs)
        {
            loadViewModel(new SiteViewModel());
            if (postedResultFilters == null || !postedResultFilters.isInPlay())
                postedResultFilters = new PostedResultFilters()
                {
                    PriceIDs = PriceIDs,
                    BrandIDs = BrandIDs,
                    SpecialtiesIDs = SpecialtiesIDs
                };

            var vm = new SearchResultsViewModel();
            
            if (id == null)
            {
                return Redirect("/");
                //vm.Products = _productRep.listAllProducts(true, true).Where(p => !p.isHidden && p.HasVariant).Take(100).ToList();
                //vm.productTags = _contentRep.listAllTags().Where(t => t.Type == Tag.TypeOption.PRODUCT_SECTION && !t.isHidden).ToList();
            }
            
            vm.Tag = _contentRep.listAllTags(true).FirstOrDefault(p => p.nameUrl == id && !p.isHidden);////_contentRep.listAllTagsWithProductList(true).FirstOrDefault(p => p.nameUrl == id && !p.isHidden);
            if (vm.Tag == null)
            {
                var alias = _contentRep.listAllTagsWithProductList().FirstOrDefault(p => p.aliasesUrl.Contains(id.ToLower()) && !p.isHidden);
                if (alias != null) return RedirectPermanent(alias.getUrl());
                return Redirect("/");
            }
            vm.Tag.subtags = _contentRep.listAllTags().Where(x => x.ParentID == vm.Tag.ID && !x.isHidden).ToList();
            SiteResourceService.loadUrls(vm.Tag.siteResources);

            var products = _productRep.listProductsForTag(vm.Tag.ID, true, true).Where(p => !p.isHidden && p.HasVariant).Take(MAX_RETURNED_PRODUCTS).ToList();
            if(products.Count==0 && vm.Tag.aliases == "CATGRP")
            {
                products = _productRep.listProductsForChildTags(vm.Tag.ID).ToList();
            }
            if (vm.Tag.Type == Tag.TypeOption.BRAND_SECTION)
                products = products.OrderBy(x => x.orderOverride ?? 1000).ThenBy(x => x.name).ToList();

            setupProductList(vm, products, page, pageSize, postedResultFilters);
            
            return View(vm);
        }
        public ActionResult Brand(string brand, string name, int? page, int? pageSize,
                                PostedResultFilters postedResultFilters,
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] PriceIDs,
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] BrandIDs,
                                [ModelBinder(typeof(IntArrayModelBinder))] int[] SpecialtiesIDs)
        {
            loadViewModel(new SiteViewModel());




            var vm = new SearchResultsViewModel();
            vm.Brand = _contentRep.listAllTags().Where(x => x.nameUrl == brand).FirstOrDefault();


            postedResultFilters.BrandIDs = new int[] { vm.Brand.ID };

            if (name == null)
            {
                return Redirect("/");
                //vm.Products = _productRep.listAllProducts(true, true).Where(p => !p.isHidden && p.HasVariant).Take(100).ToList();
                //vm.productTags = _contentRep.listAllTags().Where(t => t.Type == Tag.TypeOption.PRODUCT_SECTION && !t.isHidden).ToList();
            }

            vm.Tag = _contentRep.listAllTags(true).FirstOrDefault(p => p.nameUrl == name && !p.isHidden);////_contentRep.listAllTagsWithProductList(true).FirstOrDefault(p => p.nameUrl == id && !p.isHidden);
            if (vm.Tag == null)
            {
                var alias = _contentRep.listAllTagsWithProductList().FirstOrDefault(p => p.aliasesUrl.Contains(name.ToLower()) && !p.isHidden);
                if (alias != null) return RedirectPermanent(alias.getUrl());
                return Redirect("/");
            }
            vm.Tag.subtags = _contentRep.listAllTags().Where(x => x.ParentID == vm.Tag.ID && !x.isHidden).ToList();
            SiteResourceService.loadUrls(vm.Tag.siteResources);

            var products = _productRep.listProductsForTag(vm.Tag.ID, true, true).Where(p => !p.isHidden && p.HasVariant).Take(MAX_RETURNED_PRODUCTS).ToList();
            if (products.Count == 0 && vm.Tag.aliases == "CATGRP")
            {
                products = _productRep.listProductsForChildTags(vm.Tag.ID).ToList();
            }
            if (vm.Tag.Type == Tag.TypeOption.BRAND_SECTION)
                products = products.OrderBy(x => x.orderOverride ?? 1000).ThenBy(x => x.name).ToList();

            setupProductList(vm, products, page, pageSize, postedResultFilters);

            return View("Index", vm);
        }
        public ActionResult Product(string id)
        {
            loadViewModel(new SiteViewModel());

            
            var product = _productRep.listAllProducts(true, true).SingleOrDefault(p => p.nameUrl == id.ToLower() && !p.isHidden && p.HasVariant);
            ViewBag.metadescription = product.metadescription;
            ViewBag.metakeywords = product.metakeywords;


            if (product == null)
            {
                var selector = id.ToLower();
                var aliasprods = _productRep.listAllProducts(true, true).Where(p => p.aliasesUrl.Contains(selector) && !p.isHidden).ToList();
                foreach(var aliasprod in aliasprods)
                {
                    if(aliasprod.aliasesUrl == selector)
                       return Redirect(aliasprod.getUrl());
                    else if(aliasprod.aliasesUrl.StartsWith(id.ToLower() + "\r\n"))
                       return Redirect(aliasprod.getUrl());
                    else if (aliasprod.aliasesUrl.EndsWith(selector))
                        return Redirect(aliasprod.getUrl());
                }

                foreach(var aliasprod in aliasprods)
                {
                    if (aliasprod.aliasesUrl.Contains(id.ToLower() + "\r\n"))
                       return Redirect(aliasprod.getUrl());
                }

                if (aliasprods.Any())
                    return Redirect(aliasprods.First().getUrl());

                return Redirect("/products");
            }

            //Load Main Cat
            product.mainCategory = _contentRep.listAllTagsForProduct(product, Tag.TagRelationType.PRIMARY).SingleOrDefault(t => !t.isHidden);
            product.tags = _contentRep.listAllTagsForProduct(product).Where(t => !t.isHidden).ToList();
            /*
            product.ingredients = _productRep.listAllIngredients(true).Where(i => i.ProductID == product.ID).ToList();
            foreach(var ingredient in product.ingredients)
            {
                ingredient.siteResources = _srRep.listAll(ingredient).ToArray();
                SiteResourceService.loadUrls(ingredient.siteResources);
            }
            product.questionAndAnswers = _productRep.listAllQandA().Where(q => q.ProductID==product.ID).ToList();
            */
            product.products = _productRep.listRelatedProductsForProduct(product.ID, true, true).Where(p => !p.isHidden && p.HasVariant).ToList();
            if (product.mainCategory == null) 
                product.mainCategory = product.tags.Where(t => t.Type == Tag.TypeOption.PRODUCT_SECTION).FirstOrDefault();


            SiteResourceService.loadUrls(product.siteResources.Where(p => p.SiteResourceType == SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE || p.SiteResourceType == SiteResource.SiteResourceTypeOption.XTRA_PRODUCT_IMAGE).ToArray());
            product.basket = new Basket() { Quantity = 1, VariantID = product.variant.ID };
            product.products.ForEach(x =>
            {
                SiteResourceService.loadUrls(x.siteResources.Where(p => p.SiteResourceType == SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE).ToArray());
                x.basket = new Basket() { Quantity = 1, VariantID = x.variant.ID };
            });
            //?? assignBasketItemsToProducts(new List<Product>() { product });

            if (getAccount() != null)
                product.favourite = _personRep.listAllFavourites().Where(f => f.AccountID==getAccount().ID && f.ProductID == product.ID).SingleOrDefault();

            ViewBag.discaimers = new List<Page>();
                
            ViewBag.RelatedProducts = new ProductSliderViewModel()
            {
                Products = product.products,
                MainTag = new Tag { name = "You may also like:" }
            };

            List<int> _quantities = new List<int>();
            for (int i = 1; i <= 100; i++) _quantities.Add(i);
            ViewBag.Quantities = _quantities;

            ViewBag.ProductVideos = _contentRep.listAllPagesForProduct(product).Where(x => x.Type == Page.TypeOption.CONTENT_VIDEO).ToList();


            SetupSchema(product);

            return View(product);
        }

        public void SetupSchema(Product product)
        {
            string brand = "Meguiar's";
            if(product.tags.Where(x=>x.Type == Tag.TypeOption.BRAND_SECTION).ToList()!=null)
            {
                brand = product.tags.Where(x => x.Type == Tag.TypeOption.BRAND_SECTION).FirstOrDefault().name.Replace("MEGUIARS", "Meguiar's");

            }
            try { 
                ProductSchema thisSchema = new ProductSchema();
                thisSchema.context = "https://schema.org/";
                thisSchema.name = product.name;
                thisSchema.sku = product.sku;
                thisSchema.description = product.metadescription;
                thisSchema.brand = new Kawa.Models.BrandSchema() 
                { 
                    name = brand, 
                    type = "Brand" 
                };
                thisSchema.offers = new Kawa.Models.OfferSchema()
                {
                    type = "Offer",
                    itemCondition = "https://schema.org/NewCondition",
                    availability = "https://schema.org/InStock",
                    price = product.getBuyPrice().ToString("#.##"),
                    priceCurrency = "NZD",
                    priceValidUntil = System.DateTime.Now.AddDays(7).ToString(),
                    url = System.Configuration.ConfigurationManager.AppSettings["SiteUrl"] + product.getUrl()
                };
                thisSchema.type = "Product";
                thisSchema.image = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(product.sHero);
                product.Schema = thisSchema;
            }
            catch
            {
                product.Schema = new ProductSchema();
            }

        }


        [HttpGet]
        public ActionResult Notify(int? id)
        {
            runExpire();

            var vm = new StockNotice() { ID = 0, ProductID = id, email = getAccount()!=null ? getAccount().UserName : null };
            ViewBag.complete = false;

            return View(vm);
        }

        [HttpPost]
        public ActionResult Notify(int? id, StockNotice model)
        {
            runExpire();

            if (ModelState.IsValid)
            {
                model.ID = 0;
                model.createDate = DateTime.Now;
                model.Status = StockNotice.StatusOption.PENDING;
                _productRep.saveStockNotice(model);
                ViewBag.complete = true;
                return View();
            }
            else
            {
                var message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));

                ViewBag.complete = false;
                return View(model);
            }
        }

       


        protected override void loadViewModel(SiteViewModel viewModel)
        {
            viewModel.section = "Products";
            base.loadViewModel(viewModel);
        }

    }
}