﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Services;
using Kawa.Models.ViewModels;
using Kawa.Models.Extensions;
using PagedList;
using AutoMapper;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using FileHelpers;
using Kawa.Models.Csv;
using System.Net.Mail;

namespace Kawa.Controllers
{
    public class ShopAdminController : AdminSiteController
    {
        public static int PAGE_SIZE = 30;

        [HttpGet]
        public ActionResult Index(Status.StatusSetOption? id, int? page, OrderFilterViewModel filterModel)
        {
            filterModel.statusSet = (Status.StatusSetOption)id;
            runOrderFilterSetup(id, filterModel);
            loadViewModel(new AdminViewModel());
            return View("Index", filterModel);
            //return runOrderList(page, filterModel, (Status.StatusSetOption)id, true);
        }

        private void runOrderFilterSetup(Status.StatusSetOption? statusSet, OrderFilterViewModel filterModel)
        {
            filterModel.setStatusList = _orderRep.listAllStatus().Where(x => x.StatusSet == statusSet).ToList();
            filterModel.countryList = _shipRep.listAllCountries().ToList();
            filterModel.shippingMethodsList = _shipRep.listAllShippingMethods().ToList();
            filterModel.carriers = _shipRep.listAllCarriers().OrderBy(x => x.Name).ToList();
            filterModel.signaturesList = _orderRep.listAllSignatures().OrderBy(x => x.Name).ToList();
            filterModel.flagList = _orderRep.listAllFlags().OrderBy(x => x.Name).ToList();
        }

        private ActionResult runOrderList(int? page, OrderFilterViewModel filterModel, Status.StatusSetOption statusSet, bool hasTranactions)
        {
            if (filterModel.PostedShippingMethods != null)
                filterModel.ShippingMethods = filterModel.shippingMethodsList.Where(x => filterModel.PostedShippingMethods.ShippingMethodIDs.Any(y => y == x.ID)).ToList();

            var pageNumber = page ?? 1; // if no page was specified in the querystring, default to the first page (1)
            System.DateTime start = System.DateTime.Now;
            var orders = _orderRep.listAllOrdersWithTrans(statusSet);


           

            if (filterModel.OrderNo != null)
                orders = orders.Where(o => o.SearchOrderNo.Contains(filterModel.OrderNo));
            if (filterModel.Customer != null)
                orders = orders.Where(o => o.SearchCustomer.Contains(filterModel.Customer));
            if (filterModel.Email != null)
                orders = orders.Where(o => o.SearchEmail.Contains(filterModel.Email));

            if (filterModel.Startdate.HasValue)
                orders = orders.Where(o => o.OrderDate >= filterModel.Startdate.LocalToUtc());
            if (filterModel.Enddate.HasValue)
                orders = orders.Where(o => o.OrderDate <= ((DateTime)filterModel.Enddate).AddDays(1).LocalToUtc());

            if (filterModel.TotalFrom.HasValue)
                orders = orders.Where(o => o.OrderTotal >= filterModel.TotalFrom);
            if (filterModel.TotalTo.HasValue)
                orders = orders.Where(o => o.OrderTotal <= filterModel.TotalTo);

            if (filterModel.StatusID.HasValue)
                orders = orders.Where(o => o.StatusID == filterModel.StatusID);

            if (filterModel.VoucherCode != null)
                orders = orders.Where(o => o.VoucherCode == filterModel.VoucherCode);

            if (filterModel.CountryID != null)
                orders = orders.Where(o => o.CountryID == filterModel.CountryID);

            if (filterModel.ShippingMethods != null && filterModel.ShippingMethods.Count>0)
                orders = orders.Where(o => o.ShippingMethodID.HasValue && filterModel.PostedShippingMethods.ShippingMethodIDs.ToList().Contains((int)o.ShippingMethodID));

            if (filterModel.isTaxExempt.HasValue)
            {
                var AusID = _shipRep.listAllCountries().Single(x => x.Name == "Australia").ID;
                if ((bool)filterModel.isTaxExempt)
                    orders = orders.Where(o => o.CountryID == AusID);
                else
                    orders = orders.Where(o => o.CountryID != AusID);
            }

            if (filterModel.isPrinted.HasValue)
                orders = orders.Where(o => o.Printed == filterModel.isPrinted);

            if (filterModel.PaymentType.HasValue)
            {
                orders = orders.Where(o => o.PaymentType == filterModel.PaymentType);
            }

            if (filterModel.Products != null)
            { 
               // var terms = filterModel.products.Split('"').Select(x => x.Trim()).Where(x => !string.IsNullOrEmpty(x)).ToList();
                orders = orders.Where(o => o.Data.Baskets.Any(b => b.Variant.Product.name.Contains(filterModel.Products) || b.Variant.Product.sku.Contains(filterModel.Products)));
            }
            
            orders = orders.OrderByDescending(o => o.ID);

            if (filterModel.FlagLabel.HasValue) {
                var fullorders = orders.Take(500).ToList();
                fullorders.ForEach(x => OrderService.loadOrderWithAccountDetails(x));
                new FlagService(_orderRep).CheckOrdersForFlags(fullorders.ToList());
                var flagorders = fullorders.Where(o => o.FlagLabels.Any(x => x.Label == filterModel.FlagLabel));
                filterModel.orders = flagorders.ToPagedList(pageNumber, filterModel.pageSize ?? PAGE_SIZE);
            }
            else { 
                filterModel.orders = orders.ToPagedList(pageNumber, filterModel.pageSize ?? PAGE_SIZE);
                filterModel.orders.ToList().ForEach(x => OrderService.loadOrderWithAccountDetails(x));
                new FlagService(_orderRep).CheckOrdersForFlags(filterModel.orders.ToList());
            }

            System.DateTime end = System.DateTime.Now;
            double msrun = (end - start).TotalSeconds;
            //vm.orders = orders.Where(o => o.Details.Any()).ToPagedList();

            loadViewModel(new AdminViewModel());

            return View("Index", filterModel);
        }

        [HttpGet]
        public ActionResult PendingPaypal(int? page, OrderFilterViewModel filterModel)
        {
            var pageNumber = page ?? 1;
            var orders = _orderRep.listAllOrdersWithTrans().OrderByDescending(x => x.ID).Take(250).ToList().Where(x => x.Data.AccountDetails.Count()==2 && x.SystemStatus == Models.Order.SystemStatusOption.IN_PROGRESS && x.PaymentType == PaymentServiceStore.PaymentTypeOption.PAYPAL && !x.IsHidden);
            filterModel.orders = orders.ToPagedList(pageNumber, PAGE_SIZE);
            foreach (var order in filterModel.orders)
            {
                OrderService.loadOrderWithFullDomain(order);
            }

            return View(filterModel);
        }

        [HttpGet]
        public ActionResult OrderHide(int? id)
        {
            var order = _orderRep.listAllOrders().Single(x => x.ID == id);
            order.IsHidden = true;
            _orderRep.saveOrder(order, false);
            return RedirectToAction("PendingPaypal");
        }

        [HttpGet]
        public ActionResult Order(int? id)
        {
            runExpire();
            
            loadViewModel(new AdminViewModel());

            var order = OrderService.getFullOrder((int)id);
            new FlagService(_orderRep).CheckOrdersForFlags(new List<Order>() { order });

            return View(order);
        }

        [HttpPost]
        public ActionResult Order(int? id, NewOrderNote NewNote)
        {
            runExpire();

            loadViewModel(new AdminViewModel());
            var order = OrderService.getFullOrder((int)id);
            new FlagService(_orderRep).CheckOrdersForFlags(new List<Order>() { order });

            if(ModelState.IsValid)
            {
                order.addNewNote(string.Format("{0} {1}",NewNote.Note,DateTime.Now), NewNote.AddToCustomer);
                _orderRep.saveOrder(order);
                return RedirectToAction("Order", new { id = id });
            }

            return View(order);
        }

        [HttpGet]
        public ActionResult OrdersForPrint(int? id, string OrderIDs)
        {
            runExpire();
            return View(OrderIDs.Split(',').Select(x => int.Parse(x)).ToList());
        }

        [HttpPost]
        public ActionResult OrderForPrint(int? id)
        {
            runExpire();
            var order = OrderService.getFullOrder((int)id);
            order.IsPrintingOrder = true;
            new FlagService(_orderRep).CheckOrdersForFlags(new List<Order>() { order });
            return PartialView("_OrderForPrint",order);
        }

        [HttpGet]
        public ActionResult OrdersForLabel(int? id, string OrderIDs)
        {

            System.DateTime start = System.DateTime.Now;
            runExpire();
            var orderIds = OrderIDs.Split(',').Select(x => int.Parse(x));

            /*
            List<Order> fullOrders = new List<Order>();
            orderIds.ToList().ForEach(x =>
            {
                var q = _orderRep.listAllOrdersWithTrans().Where(o => o.ID == x);
                var order = _orderRep.listAllOrdersWithTrans().Single(o => o.ID==x);
                order.Details = _orderRep.listAllAccountDetails().Where(d => d.OrderID == order.ID).ToList();
                fullOrders.Add(order);
            });
            */
            var fullOrders = _orderRep.quicklistAllOrdersWithTrans().Where(x => orderIds.Contains(x.ID)).ToList();
            fullOrders.ForEach(x => x.Details = _orderRep.listAllAccountDetails().Where(d => d.OrderID == x.ID).ToList());

            var orders = fullOrders.Select(x => new LabelOrder()
            {
                ///C_CHARGE_CODE = x.ShippingMethodID == 1 ? "3D35" : "3J35",                
                C_CHARGE_CODE = x.ShippingMethodID == 1 ? "3D85" : "3J85",

                C_CONSIGNEE_NAME = clearForCsv(x.DeliveryDetails.FirstName + " " + x.DeliveryDetails.Surname),
                C_CONSIGNEE_BUSINESS_NAME = clearForCsv(x.DeliveryDetails.Company.Truncate(35)),
                C_CONSIGNEE_ADDRESS_1 = clearForCsv(x.DeliveryDetails.Address1.Truncate(40)),
                C_CONSIGNEE_ADDRESS_2 = clearForCsv(x.DeliveryDetails.Address2.Truncate(40)),
                C_CONSIGNEE_SUBURB = clearForCsv(x.DeliveryDetails.Address3),
                C_CONSIGNEE_STATE_CODE = ShippingRepository.getStateCode(x.DeliveryDetails.Address4),
                C_CONSIGNEE_POSTCODE = clearForCsv(x.DeliveryDetails.Postcode),
                C_CONSIGNEE_COUNTRY_CODE = x.DeliveryDetails.Country == "Australia" ? "AU" : _shipRep.listAllCountries().Single(y => y.Name == x.DeliveryDetails.Country).Code,
                C_CONSIGNEE_PHONE_NUMBER = clearForCsv(x.DeliveryDetails.Mobile),
                C_PHONE_PRINT_REQUIRED = "YES",
                C_DELIVERY_INSTRUCTION = clearForCsv(x.DeliveryDetails.DeliveryInstructions.Truncate(128)),
                C_SIGNATURE_REQUIRED = (string.IsNullOrEmpty(x.DeliveryDetails.DeliveryInstructions)?"R":"N"),
                //C_ADD_TO_ADDRESS_BOOK = "YES",
                C_REF = x.OrderNo,
                C_REF_PRINT_REQUIRED = "YES",
                C_CONSIGNEE_EMAIL = clearForCsv(x.DeliveryDetails.Email),
                C_EMAIL_NOTIFICATION = "DESPATCH",
                A_ACTUAL_CUBIC_WEIGHT = x.CubicWeight.ToString(),
            }).ToList();

            
            System.DateTime end = System.DateTime.Now;
            double msrun = (end- start).TotalSeconds;
            orders.Insert(0, new LabelOrder());

            Type type = typeof(LabelOrder);
            var properties = type.GetProperties();
            string HeaderLine = string.Empty;
            foreach (PropertyInfo info in properties)
            {
                DisplayNameAttribute attr = (DisplayNameAttribute)Attribute.GetCustomAttribute(info, typeof(DisplayNameAttribute));
                HeaderLine += attr.DisplayName + ",";
            }
            HeaderLine = HeaderLine.TrimEnd(',');

            FileHelperEngine engine = new FileHelperEngine(type);
            engine.HeaderText = HeaderLine;
            var FileStr = engine.WriteString((IEnumerable<object>)orders);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            var fileName = "Orders for Labeling " + String.Format("{0:u}", DateTime.Now.UtcToLocal()) + msrun.ToString("0.000");
            var data = encoding.GetBytes(FileStr);
            fileName += ".csv";

            return File(data, "application/csv", fileName);
        }

        private string clearForCsv(string val)
        {
            if (string.IsNullOrEmpty(val)) return val;
            return val.Replace("\n", " ").Replace(",", ".").Replace("\"", "'").Replace("’", "'");
        }

        #region Order Change / New


        [HttpGet]
        public ActionResult NewOrder()
        {
            runExpire();

            var order = OrderService.getNewDefaultOrder(null,false);
            order.IsAdminOrder = true;
            OrderService.finaliseOrder(order);

            return RedirectToAction("OrderChange", new { id = order.ID, isadmincreated = true });
        }

        [HttpGet]
        public ActionResult OrderChange(int? id, bool isadmincreated = false)
        {
            runExpire();

            loadViewModel(new AdminViewModel());
            addBasketSetup(new SiteViewModel());

            var order = OrderService.getFullOrder((int)id, isadmincreated);

            order.isBasket = true;
            order.isAdmin = true;

            return View(order);
        }

        public ActionResult AddToBasket(int ProductID, int Quantity, int OrderID)
        {
            runExpire();

            SiteViewModel vm = new SiteViewModel() { };
            addBasketSetup(vm);

            Basket basket = new Basket()
            {
                VariantID = _productRep.listAllProducts(false, true).Single(x => x.ID == ProductID).variant.ID,
                Quantity = Quantity,
                OrderID = OrderID
            };

            var order = OrderService.getFullOrder(OrderID);
            OrderService.tagExistingStateToLog(order, "#State before item addition# ");

            var result = OrderService.addBasket(basket, null, true);

            order = OrderService.getFullOrder(OrderID, true);
            _orderRep.saveOrder(order);
            OrderService.tagExistingStateToLog(order, "######## State after item item addition ######## " + DateTime.UtcNow.UtcToLocal());

            order.isBasket = true;
            order.isAdmin = true;

            return PartialView("../shop/_Basket", order);
        }

        public ActionResult ChangeBasket(Basket basket, int OrderID)
        {
            runExpire();
            var order = OrderService.getFullOrder(OrderID,true);
            OrderService.tagExistingStateToLog(order,"#State before item line modification# ");
            
            var changebasket = OrderService.updateBasket(basket,true);

            SiteViewModel vm = new SiteViewModel() { };
            addBasketSetup(vm);

            
            if (changebasket != null)
                order.orderItems.Where(x => x.ID == changebasket.ID).ToList().ForEach(x => x.Message = changebasket.Message);

            order = OrderService.getFullOrder(OrderID, true);
            _orderRep.saveOrder(order);
            OrderService.tagExistingStateToLog(order, "######## State after item line modification ######## " + DateTime.UtcNow.UtcToLocal());

            order.isBasket = true;
            order.isAdmin = true;

            return PartialView("../shop/_Basket", order);
        }

        public ActionResult ChangeBasketPrice(int basketid, decimal price)
        {
            runExpire();

            var basket = _orderRep.listAllBasket().Single(x => x.ID == basketid);
            var order = OrderService.getFullOrder((int)basket.OrderID, true);
            OrderService.tagExistingStateToLog(order, "#State before item line modification# ");

            OrderService.adminFixBasket(basket, price);

            SiteViewModel vm = new SiteViewModel() { };
            addBasketSetup(vm);

            order = OrderService.getFullOrder(order.ID, true);
            _orderRep.saveOrder(order);
            OrderService.tagExistingStateToLog(order, "######## State after item line modification ######## " + DateTime.UtcNow.UtcToLocal());

            order.isBasket = true;
            order.isAdmin = true;

            return PartialView("../shop/_Basket", order);
        }

        [HttpPost]
        public ActionResult AddVoucher(Promotion voucher, int OrderID)
        {
            runExpire();
            var order = OrderService.getFullOrder(OrderID, true);
            OrderService.tagExistingStateToLog(order, "#State before voucher modification# ");
            if (string.IsNullOrEmpty(voucher.code))
            {
                order.isBasket = true;
                ViewBag.voucherMessage = "Please provide a promotion code.";
                return PartialView("../shop/_Basket", order);
            }

            var result = PromotionService.checkVoucher(voucher, order, true);
            ViewBag.voucherMessage = result.message;

            SiteViewModel vm = new SiteViewModel() { };
            addBasketSetup(vm);

            order = OrderService.getFullOrder(order.ID, true);
            _orderRep.saveOrder(order);
            OrderService.tagExistingStateToLog(order, "######## State after voucher modification ######## " + DateTime.UtcNow.UtcToLocal());

            order.isBasket = true;
            order.isAdmin = true;

            return PartialView("../shop/_Basket", order);
        }

        public ActionResult ChangeBasketShipping(ShippingInputs inputs, int OrderID)
        {
            runExpire();
            SiteViewModel vm = new SiteViewModel() { };
            addBasketSetup(vm);

            var order = OrderService.getFullOrder(OrderID, true);
            OrderService.tagExistingStateToLog(order, "#State before item shipping modification# ");

            order = OrderService.getFullOrderWithNewShippingInputs(OrderID, inputs);
            _orderRep.saveOrder(order);
            OrderService.tagExistingStateToLog(order, "######## State after item shipping modification ######## " + DateTime.UtcNow.UtcToLocal());

            order.isBasket = true;
            order.isAdmin = true;

            ModelState.Clear();
            return PartialView("../shop/_Basket", order);
        }

        [HttpGet]
        public ActionResult OrderChooseAccount(int? id)
        {
            runExpire();

            loadViewModel(new AdminViewModel());

            var order = OrderService.getFullOrder((int)id);

            order.isBasket = true;
            order.isAdmin = true;

            return View(new OrderChooseAccountViewModel()
            {
                OrderID = (int)id,
                Order = order,
                FilterModel = new AccountFilterViewModels()
            });
        }

        [HttpPost]
        public ActionResult OrderChooseAccount(OrderChooseAccountViewModel model)
        {
            runExpire();

            loadViewModel(new AdminViewModel());

            var order = OrderService.getFullOrder(model.OrderID);
            return RedirectToAction("OrderAccountDetails", new { 
                id = model.OrderID,
                accountID = model.AccountID,
                lastorderid = model.LastOrderID
            });
            
        }

        [HttpGet]
        public ActionResult OrderAccountDetails(int? id, int? accountID, int? lastorderid)
        {
            runExpire();

            loadViewModel(new AdminViewModel());

            var order = OrderService.getFullOrder((int)id);

            ViewBag.Order = order;
            ViewBag.AllowedCountries = ShippingService.getAllowedCountries();
            ViewBag.AllowedStates = ShippingService.getAllowedStates();
            ViewBag.showSpecial = false;

            Account account = null;
            if (accountID.HasValue)
            {
                //Fix any updates to order owership if necessary
                if (order.AccountID.HasValue && order.AccountID != accountID)
                {
                    order.AccountID = accountID;
                    _orderRep.saveOrder(order);
                }
                account = _personRep.listAllAccounts().Single(x => x.ID == accountID);
            }

            Order prepopOrder = null;
            if (lastorderid.HasValue)
            {
                //Fix any updates to order owership if necessary
                if (order.AccountID.HasValue)
                {
                    order.AccountID = null;
                    _orderRep.saveOrder(order);
                }
                prepopOrder = _orderRep.listAllOrders().Single(x => x.ID == lastorderid);
            }
            

            var details = OrderService.getPrepopOrderDetails(order, account, prepopOrder);
            AccountDetailsViewModel avm = new AccountDetailsViewModel()
            {
                CustomerDetails = details[0],
                DeliveryDetails = (AccountDetailDelivery)details[1]
            };
            
            if(order.SystemStatus == Models.Order.SystemStatusOption.IN_PROGRESS)
            {
                avm.DeliveryDetails.CountryID = order.CountryID;
                if (order.StateID.HasValue)
                {
                    avm.DeliveryDetails.Address4 = _shipRep.listAllStates().Single(x => x.ID == order.StateID).Name;
                    avm.DeliveryDetails.lockState = true;
                }
                else
                    avm.DeliveryDetails.lockState = false;
                if (order.Postcode.HasValue)
                {
                    avm.DeliveryDetails.Postcode = order.Postcode.ToString();
                    avm.DeliveryDetails.lockPostcode = true;
                }
                else
                    avm.DeliveryDetails.lockPostcode = false;
            }
            if (avm.DeliveryDetails.Postcode != null)
            {
                avm.DeliveryDetails.Postcode = Kawa.Controllers.SiteController.MaskPostCode(int.Parse(avm.DeliveryDetails.Postcode));
            }
            if (avm.CustomerDetails.Postcode != null)
            {
                avm.CustomerDetails.Postcode = Kawa.Controllers.SiteController.MaskPostCode(int.Parse(avm.CustomerDetails.Postcode));
            }
            avm.DeliveryDetails._AddressLookupText = _contentRep.listAllPages().SingleOrDefault(x => x.SystemPage == Page.SystemPageOption.ACCOUNTDETAIL_LOOKUP_TEXT).html2;
            avm.CustomerDetails._AddressLookupText = avm.DeliveryDetails._AddressLookupText;
            return View(avm);
        }

        [HttpPost]
        public async Task<ActionResult> OrderAccountDetails(int? id, AccountDetailsViewModel model)
        {
            runExpire();
            loadViewModel(new AdminViewModel());

            var order = OrderService.getFullOrder((int)id);
            
            ViewBag.AllowedCountries = ShippingService.getAllowedCountries();
            ViewBag.AllowedStates = ShippingService.getAllowedStates();
            ViewBag.showSpecial = false;
            ViewBag.Order = order;

            if (model.DeliveryDetails.leaveOptionSelected == AccountDetailDelivery.LEAVE_SET_NO_OPTION_CHOOSEN)
            {
                ModelState.AddModelError("", "If you grant autority to leave, you must choose an option from the dropdown.");
            }
            
            if (ModelState.IsValid)
            {
                if (order.CustomerDetails == null)
                {
                    model.CustomerDetails.OrderID = order.ID;
                    model.CustomerDetails.Type = AccountDetail.DetailTypeOption.ACCOUNT_DETAILS;
                    order.Details.Add(model.CustomerDetails);
                }
                else
                {
                    if (model.CustomerDetails.ID != order.CustomerDetails.ID)
                        model.CustomerDetails.ID = order.CustomerDetails.ID;
                }
                model.CustomerDetails.Country = ((List<Country>)ViewBag.AllowedCountries).Single(x => x.ID == model.CustomerDetails.CountryID).Name;
                _orderRep.saveAccountDetail(model.CustomerDetails);

                if (order.DeliveryDetails == null)
                {
                    model.DeliveryDetails.OrderID = order.ID;
                    model.DeliveryDetails.Type = AccountDetail.DetailTypeOption.DELIVERY_DETAILS;
                    order.Details.Add(model.DeliveryDetails);
                }
                else
                {
                    if (model.DeliveryDetails.ID != order.DeliveryDetails.ID)
                        model.DeliveryDetails.ID = order.DeliveryDetails.ID;
                }
                model.DeliveryDetails.Country = ((List<Country>)ViewBag.AllowedCountries).Single(x => x.ID == model.DeliveryDetails.CountryID).Name;
                _orderRep.saveAccountDetail(model.DeliveryDetails);

                if (model.DeliveryDetails.leaveOptionSelected == null)
                {
                    model.DeliveryDetails.DeliveryInstructions = null;
                    model.DeliveryDetails.LeaveAuth = false;
                }
                else
                {
                    if (model.DeliveryDetails.leaveOptionSelected != AccountDetailDelivery.LEAVE_OTHER_OPTION)
                        model.DeliveryDetails.DeliveryInstructions = AccountDetailDelivery.LEAVE_PREFIX + model.DeliveryDetails.leaveOptionSelected;
                    model.DeliveryDetails.LeaveAuth = true;
                }

                _orderRep.saveAccountDetail(model.DeliveryDetails);
                
                if (order.SystemStatus == Models.Order.SystemStatusOption.IN_PROGRESS)
                {
                    if (model.CustomerDetails.isMailer)
                    {
                        await MailChimpService.AddEmailToListAsync(model.CustomerDetails.Email, model.CustomerDetails.FirstName, model.CustomerDetails.Surname);
                    }

                    /*
                    if (OrderService.isCompletePromotionZeroOrder(order))
                    {
                        OrderService.completePromotionZeroOrder(order);
                        ViewBag.basketCount = 0;
                        return View("OrderComplete", order);
                    }*/

                    return RedirectToAction("OrderChoosePayment", new { id = id });
                }
                else
                    return RedirectToAction("Order", new { id = id });
            }
            else
            {
                var errorState = ModelState.Values.Where(x => x.Errors.Count > 0);
                var message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));
            }
            model.DeliveryDetails._AddressLookupText = _contentRep.listAllPages().SingleOrDefault(x => x.SystemPage == Page.SystemPageOption.ACCOUNTDETAIL_LOOKUP_TEXT).html2;
            model.CustomerDetails._AddressLookupText = model.DeliveryDetails._AddressLookupText;
            return View(model);
        }

        public ActionResult OrderChoosePayment(int id)
        {
            loadViewModel(new AdminViewModel());
            var order = OrderService.getFullOrder(id);
            ViewBag.Order = order;
            /*
            var badresult = checkOrder(order);
            if (badresult != null) return badresult;
            */
            OrderService.resetAmexSurcharge(order);
            var options = SiteService.getPaymentOptions(true);

            options.Remove(PaymentServiceStore.PaymentTypeOption.PAYPAL);

            if (order.CustomerDetails.CountryID != _shipRep.listAllCountries().Single(x => x.Name == "Australia").ID)
                options.Remove(PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT);

            var vm = new ChoosePaymentViewModel()
            {
                orderCode = order.Code,
                options = options.Select(s => PaymentServiceStore.getService(s).getPaymentChoiceModel()).ToList()
            };

            return View(vm);
        }

        public ActionResult OrderPayment(Guid id, PaymentServiceStore.PaymentTypeOption pt)
        {
            loadViewModel(new AdminViewModel());

            var order = OrderService.getFullOrder(id);
            ViewBag.Order = order;
            /*
            var badresult = checkOrder(order);
            if (badresult != null) return badresult;
            */
            OrderService.resetAmexSurcharge(order);
            Account account = null;
            if (order.AccountID.HasValue)
                account = _personRep.listAllAccounts().Single(x => x.ID == order.AccountID);

            var payservice = PaymentServiceStore.getService(pt);
            payservice.IsAdminPayment = true;
            var vm = payservice.getPaymentModel(account, order);
            ViewBag.Order = order;

            return vm.hasView ? View(vm) : (ActionResult)RedirectToAction("OrderComplete", new { id = id, pt = pt });
        }

        public ActionResult OrderComplete(Guid? id, FormCollection formValues, string AccessCode)
        {
            var vm = new PaymentCompleteViewModel();
            vm.order = OrderService.getFullOrder((Guid)id);

            PaymentServiceStore.PaymentTypeOption pt = (PaymentServiceStore.PaymentTypeOption)vm.order.PaymentType;

            var orderissue = OrderService.checkForBasketReset(vm.order);

            object postObj = null;
            switch (pt)
            {
                case Models.Services.PaymentServiceStore.PaymentTypeOption.EWAY:
                    postObj = AccessCode;
                    break;
                case Models.Services.PaymentServiceStore.PaymentTypeOption.PAYPAL:
                    postObj = formValues;
                    break;
            }

            vm.result = PaymentServiceStore.getService(pt).getResult(postObj, vm.order);

            if (vm.result.success)
            {
                if (vm.order.SystemStatus == Models.Order.SystemStatusOption.IN_PROGRESS)
                {
                    OrderService.completeSuccessfulOrder(vm.order, vm.result);

                    if (vm.order.AccountID != null && vm.result.token != null)
                    {
                        var account = _personRep.listAllAccounts().Single(a => a.ID == vm.order.AccountID);
                        if (account.saveCreditCard && account.creditCardToken != vm.result.token)
                        {
                            account.creditCardToken = vm.result.token;
                            _personRep.saveAccount(account);
                        }
                    }
                    /*
                    if (orderissue)
                    {
                        sendIssueEmail("Please enter the admin and reconcile order " + vm.order.OrderNo + ".  The items in the order where changed while the user was waiting for their transaction to process.");
                        return RedirectToAction("CompleteWithIssue", new { id = vm.order.Code });
                    }
                     */
                }
                ViewBag.OrderSummary = OrderService.getSummary();
            }
            else
            {
                OrderService.saveFailedOrder(vm.order, vm.result);

                return RedirectToAction("OrderInComplete", new { id = vm.order.Code });
            }

            loadViewModel(new SiteViewModel()
            {
                isOrder = true,
                checkoutStage = SiteViewModel.CheckoutStage.COMPLETE,
                ordercode = (Guid)id
            });

            return View(vm);
        }

        public ActionResult OrderInComplete(Guid? id, FormCollection formValues, string AccessCode)
        {
            loadViewModel(new SiteViewModel()
            {
                isOrder = true,
                checkoutStage = SiteViewModel.CheckoutStage.PAYMENT,
                ordercode = (Guid)id
            });


            var vm = new PaymentFailedViewModel()
            {
                order = OrderService.getFullOrder((Guid)id),
            };
            /*
            var badresult = checkOrder(vm.order);
            if (badresult != null) return badresult;
            */
            OrderService.resetAmexSurcharge(vm.order);
            vm.type = (PaymentServiceStore.PaymentTypeOption)vm.order.PaymentType;

            return View(vm);
        }

        #endregion

        [HttpGet]
        public ActionResult TestEmailLook(int? id)
        {
            runExpire();
            var order = OrderService.getFullOrder((int)id);

            ViewBag.TestHtml = RenderPartialViewToString(this, "~/Views/Shop/CompleteEmail", new PaymentCompleteViewModel() { order = order, result = null });

            return View();
        }


        #region Ajax

        public JsonResult getOrderList(int? page, OrderFilterViewModel filterModel)
        {
            runExpire();
            runOrderFilterSetup(filterModel.statusSet, filterModel);
            runOrderList(page, filterModel, filterModel.statusSet, true);
            var count = filterModel.orders.ToList().Count();
            return Json(new { list = Mapper.Map<List<Order>,List<Models.Coms.Order>>(filterModel.orders.ToList()), count = filterModel.orders.TotalItemCount });
        }

        public JsonResult StatusOptions(int? id)
        {
            runExpire();

            return this.Json( _orderRep.listAllStatus().Where(s => !s.RefactorToID.HasValue).ToList() , JsonRequestBehavior.AllowGet);
        }

        public JsonResult Signatures(int? id)
        {
            runExpire();

            return this.Json(_orderRep.listAllSignatures().ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeStatusOfOrders(List<int> orderids, int statusid, string message, int? SignatureID)
        {
            runExpire();

            var orders = _orderRep.listAllOrders().Where(o => orderids.Contains(o.ID)).ToList();
            var status = _orderRep.listAllStatus().Single(s => s.ID == statusid);
            var signature = SignatureID.HasValue ? _orderRep.listAllSignatures().Single(x => x.ID == SignatureID) : null;

            orders.ForEach(o => OrderService.loadOrderWithFullDomain(o));
            List<MailMessage> statuschangeemails = new List<MailMessage>();
            foreach(Order o in orders)
            {
                statuschangeemails.Add(OrderService.changeOrderStatusAndGetEmail(o, status, message, signature, null));
            }
            ////orders.ForEach(o => OrderService.changeOrderStatus(o, status, message, signature, null));


            Task.Run(() =>
            {
                using (var client = new SmtpClient())
                {
                    foreach (MailMessage msg in statuschangeemails)
                    {
                        try
                        {
                            client.Send(msg);
                        }
                        catch (Exception e)
                        {
                            string error = string.Format("Email Send Error{0}Subject - {1}{0}To - {2}{0}{3}{0}{4}", Environment.NewLine, msg.Subject, msg.To, e.Message, e.StackTrace);
                            LogService.WriteError(error);
                        }
                    }
                }
            });

            return this.Json(new { sucess = true }, JsonRequestBehavior.AllowGet);
        }
      
        [HttpPost]
        public JsonResult SetPrintedStatusOfOrders(List<int> orderids, bool isprinted)
        {
            runExpire();

            var orders = _orderRep.listAllOrders().Where(o => orderids.Contains(o.ID)).ToList();

            orders.ForEach(o =>
            {
                o.Printed = isprinted;
                _orderRep.saveOrder(o);
            });

            return this.Json(new { sucess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddShipment(Shipment model)
        {
            runExpire();

            model.createDate = DateTime.Now;

            _orderRep.saveShipment(model);

            return this.Json(new { sucess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteShipment(int ShipmentID)
        {
            runExpire();

            _orderRep.deleteShipment(new Shipment() { ID = ShipmentID });

            return this.Json(new { sucess = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult saveProductCubicWeightUpdates(int? page, List<Models.Coms.Order> model)
        {
            runExpire();

            if (ModelState.IsValid)
            {
                foreach (var chgord in model)
                {
                    var order = _orderRep.listAllOrders().Single(x => x.ID == chgord.ID);
                    order.CubicWeight = chgord.CubicWeight;
                    _orderRep.saveOrder(order, false);
                }

                return Json(new { success = true });
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = string.Join("; ", ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage))
                });
            }
        }


        #endregion
    }
}