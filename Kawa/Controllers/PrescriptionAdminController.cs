﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;

namespace Kawa.Controllers
{
    public class PrescriptionTagAdminController : AdminSiteController
    {


        // GET: Category
        public ActionResult Index(int? id, AdminViewModel.ContentFilter? filter, bool productcounts = false)
        {


            ViewBag.tags = _contentRep.listAllPerscriptionTags().ToList();

            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                prescriptiontags = _contentRep.listAllPerscriptionTags().Where(t => (!id.HasValue && t.Type == Models.PrescriptionTag.TypeOption.SUPER_SECTION) || (id.HasValue && t.ParentID == id)).ToList()
            };

            if (id.HasValue)
            {
                vm.prescriptiontag = _contentRep.listAllPerscriptionTags().Single(x => x.ID == id);
                if (vm.prescriptiontag.Type == Models.PrescriptionTag.TypeOption.CAR_PRESCRIPTION_CAT || vm.prescriptiontag.ID == 2792)///then load the products
                {
                    vm.prescriptiontag.products = _productRep.listProductsForPrescriptionTagNotIncludingParents(vm.prescriptiontag.ID, false, true).ToList(); ///_productRep.l.listProductsForTagNotIncludingParents(vm.tag.ID, false, true).ToList();
                    ///vm.brands = _contentRep.listAllPerscriptionTags().Where(x => x.Type == Models.PrescriptionTag.TypeOption.CAR_PRESCRIPTION_CAT && !x.isHidden).ToList();
                }
            }
            if (filter.HasValue)
            {
                vm.filter = filter;
                switch (filter)
                {
                    case AdminViewModel.ContentFilter.With_Meta:
                        vm.prescriptiontags = vm.prescriptiontags.Where(x => !string.IsNullOrWhiteSpace(x.metaDescription)).ToList();
                        break;
                    case AdminViewModel.ContentFilter.Without_Meta:
                        vm.prescriptiontags = vm.prescriptiontags.Where(x => string.IsNullOrWhiteSpace(x.metaDescription)).ToList();
                        break;
                    case AdminViewModel.ContentFilter.With_Content:
                        vm.prescriptiontags = vm.prescriptiontags.Where(x => !string.IsNullOrWhiteSpace(x.html)).ToList();
                        break;
                    case AdminViewModel.ContentFilter.Without_Content:
                        vm.prescriptiontags = vm.prescriptiontags.Where(x => string.IsNullOrWhiteSpace(x.html)).ToList();
                        break;
                }
            }

            loadViewModel(vm);
            if (productcounts)
                vm.prescriptiontags.ForEach(x => x.noOfLiveProducts = _productRep.listProductsForPrescriptionTagNotIncludingParents(x.ID, false, true).Count(y => !y.isHidden && y.HasVariant));

            return View(vm);
        }


        // GET: Category

        public ActionResult AssociateProduct(int ProductID, int TagID, int Rank)
        {
            DeleteAssociatedProduct(ProductID, TagID);///to ensure no duplciates
            _productRep.savePrescriptionTagRelationShip(ProductID, TagID, Rank);
            return Content("added....");
        }

        public ActionResult DeleteAssociatedProduct(int ProductID, int TagID)
        {
            _productRep.deletePrescriptionTagRelationShip(ProductID, TagID);
            return Content("removed....");
        }

        public ActionResult ReplicateStucture(int RecipientTag, int SourceTag)
        {
            _contentRep.ReplicateStucture(RecipientTag, SourceTag);
            return Content("removed....");
        }

        public ActionResult MoveProducts(int RecipientTag, int SourceTag, int[] Productids)
        {
            foreach (int productid in Productids)
            {

                int rank = _productRep.getProductPrescriptionTagRank(productid, SourceTag);
                DeleteAssociatedProduct(productid, SourceTag);///remove old ref
                _productRep.savePrescriptionTagRelationShip(productid, RecipientTag, rank);///create new one

            }
            return Content("transfer complete....");
        }
      
       

        [HttpGet]
        public ActionResult Tag(int? id, int? pid)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                prescriptiontag = id.HasValue ? _contentRep.listAllPerscriptionTags().Where(p => p.ID == id).Single() : new PrescriptionTag()
                {
                    ParentID = pid, Type = PrescriptionTag.TypeOption.CAR_PRESCRIPTION_CAT
                },
            };
            loadViewModel(vm);


            prescriptiontagSrSetrup(vm);
            loadSrs(vm.srOwner, true);

            return View(vm);
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Tag(int? id, PrescriptionTag prescriptiontag)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                prescriptiontag = prescriptiontag
            };
            ViewBag.prescriptiontags = _contentRep.listAllPerscriptionTags();
            prescriptiontagSrSetrup(vm);

            if (ModelState.IsValid)
            {
                if (prescriptiontag.ID == 0)
                {
                    if (prescriptiontag.ParentID.HasValue)
                    {
                        prescriptiontag.Type = _contentRep.listAllPerscriptionTags().Single(x => x.ID == prescriptiontag.ParentID).getChildType();
                    }
                    else
                    {
                        prescriptiontag.Type = Models.PrescriptionTag.TypeOption.SUPER_SECTION;
                    }
                }

                _contentRep.savePrescriptionTag(prescriptiontag);

                collectImagePosts(prescriptiontag);
                saveExistingImages(prescriptiontag);
                if (!ModelState.IsValid)
                {
                    loadSrs(vm.srOwner, true);
                    loadViewModel(vm);
                    return View(vm);
                }
                if (Request["showedit"] == "true")
                {
                    return RedirectToRoute("PrescriptionTagAdminTag", new { id = prescriptiontag.ID, m = "s" });
                }
                else
                {
                        return RedirectToRoute("PrescriptionTagAdminTag", new { id = prescriptiontag.ID, m = "s" });
                }
            }
            else
            {
                vm.message = string.Join("; ", ModelState.Values
                                       .SelectMany(x => x.Errors)
                                       .Select(x => x.ErrorMessage));

                loadViewModel(vm);
                return View(vm);
            }
        }

        [HttpGet]
        public ActionResult TagDelete(int? id)
        {
            runExpire();
            AdminViewModel vm = new AdminViewModel()
            {
                tag = _contentRep.listAllTags().Where(p => p.ID == id).Single()
            };

            return View(vm);
        }


        [HttpPost]
        public ActionResult TagDelete(int? id, Page inPage)
        {
            runExpire();

            var tag = _contentRep.listAllTags().Where(p => p.ID == id).Single();
            if (tag.SystemTag.HasValue)
                throw new Exception("Category is a system category and cannot be deleted");

            _contentRep.deleteTag(new Tag { ID = (int)id });
            if (tag.ParentID == null && !tag.SystemTag.HasValue && tag.Type == Models.Tag.TypeOption.ADHOC_PRODUCT_GROUP)
                return RedirectToRoute("TagAdminBulkActionGroups", new { id = "" });
            else
                return RedirectToRoute("TagAdminIndex", new { id = "" });
        }
    }
}