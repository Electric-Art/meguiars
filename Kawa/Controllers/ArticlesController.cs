﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using WebMarkupMin.AspNet4.Mvc;

namespace Kawa.Controllers
{
    [MinifyHtml]
    public class ArticlesController : SiteController
    {
        // GET: Articles
        public ActionResult Index()
        {
            SiteViewModel vm = new SiteViewModel();

            loadViewModel(vm);
            

            vm.pages = _contentRep.listAllPages().OrderBy(p => p.ID).Where(p => !p.isHidden).ToList();
            vm.slots = _contentRep.listAllSlots(true,true).Where(s => s.Spot != Slot.SpotOption.HOME_PAGE_MODULE).ToList().Where(s => s.page != null).ToList();

            foreach (var page in vm.pages)
            {
                page.tags = _contentRep.listAllTagsForPage(page).Where(p => !p.isHidden).ToList();
            }
            foreach (var tag in vm.productTags)
            {
                tag.pages = vm.pages.Where(p => p.tags.Any(t => t.ID==tag.ID)).OrderByDescending(p => p.ID).ToList();
            }
            foreach (var slot in vm.slots)
            {
                SiteResourceService.loadUrls(slot.page.siteResources);
            }
            
            return View(vm);
        }

        public ActionResult Article(int? id, bool? ismodal)
        {
            loadViewModel(new SiteViewModel());

            var vm = new ArticlePageViewModel();

            if (ismodal.HasValue)
                vm.isModal = (bool)ismodal;

            vm.page = _contentRep.listAllPages(true).SingleOrDefault(p => p.ID == id && !p.isHidden);
            
            //TODO
            if (vm.page == null)
            {
                return HttpNotFound();
            }

            //vm.pages = _contentRep.listAllPages().OrderBy(p => p.ID).Where(p => !p.isHidden && p.ID > 335).Take(2).ToList();
            //vm.page.mainCategory = _contentRep.listAllTagsForPage(vm.page).SingleOrDefault(p => p.Type == Tag.TypeOption.PRODUCT_SECTION && !p.isHidden);
            //vm.page.siteResources = _srRep.listAll(vm.page).ToArray();
            SiteResourceService.loadUrls(vm.page.siteResources);

            vm.page.pages = _contentRep.listRelatedPagesForPage(vm.page.ID,true).ToList();
            vm.page.pages.ForEach(x => SiteResourceService.loadUrls(x.siteResources));
            
            if (getAccount() != null)
                vm.page.favourite = _personRep.listAllFavourites().Where(f => f.AccountID == getAccount().ID && f.PageID == vm.page.ID).SingleOrDefault();
           
            return View(vm);
        }


        protected override void loadViewModel(SiteViewModel viewModel)
        {
            viewModel.section = "Articles";
            base.loadViewModel(viewModel);
        }
    }
}