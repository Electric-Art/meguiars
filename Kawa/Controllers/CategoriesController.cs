﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using WebMarkupMin.AspNet4.Mvc;

namespace Kawa.Controllers
{
    [MinifyHtml]
    public class CategoriesController : SiteController
    {
        // GET: Categories
        public ActionResult Index(string id)
        {

            return RedirectToAction("Products", new { id = id});
            /*

            loadViewModel(new SiteViewModel());

            var vm = new CategoriesViewModel();
            vm.Tag = _contentRep.listAllTags(true).SingleOrDefault(p => p.nameUrl == id && !p.isHidden);
            if (vm.Tag == null)
            {
                var alias = _contentRep.listAllTagsWithProductList().FirstOrDefault(p => p.aliasesUrl.Contains(id.ToLower()) && !p.isHidden);
                if (alias != null) return RedirectPermanent(alias.getUrl());
                return Redirect("/");
            }
            SiteResourceService.loadUrls(vm.Tag.siteResources);

            vm.NameOfSet = vm.Tag.name;
            if(vm.Tag.Type != Tag.TypeOption.SUPER_SECTION)
                vm.Tags = _contentRep.listAllTags().Where(x => x.ParentID == vm.Tag.ID && !x.isHidden).OrderBy(y => y.name).ToList();
            vm.TagBrands = _contentRep.listAllTagsForProductsWithTag(vm.Tag).Where(t => t.Type == Tag.TypeOption.BRAND_SECTION && !t.isHidden).OrderBy(y => y.name).ToList();
                    
            return View(vm);*/
        }

        public ActionResult Letter(string id, string mode)
        {
            loadViewModel(new SiteViewModel());

            var vm = new CategoriesViewModel()
            {
                NameOfSet = "\"" + id + "\"",
                TagBrands = _contentRep.listAllTags().Where(x => x.name.StartsWith(id) && (x.Type == Tag.TypeOption.BRAND_SECTION && !x.isHidden)).OrderBy(y => y.name).ToList(),
                IsLetter = true,
                IsFromBrands = mode == "brands"
            };

            vm.Tags = _contentRep.listAllTagsWithProductList().Where(x => x.name.StartsWith(id) && x.Type == Tag.TypeOption.PRODUCT_SECTION && !x.isHidden).OrderBy(y => y.name).ToList();

            return View("Index", vm);
        }

        [HttpPost]
        public ActionResult GetProductsForTag(int MainTagID, int? IntersectionTagID)
        {
            runExpire();
            List<Product> products = TagService.GetSliderProducts(_contentRep.listAllTags().Single(x => x.ID==MainTagID),null,null,IntersectionTagID);
            products.ForEach(x => {
                SiteResourceService.loadUrls(x.siteResources);
                x.basket = new Basket() { Quantity = 1, VariantID = x.variant.ID };

                });

            return PartialView("_ItemSliderData", products);
        }

        [HttpPost]
        public ActionResult GetProductsForSlider(int MainTagID)
        {
            runExpire();
            List<Product> products = TagService.GetSliderProducts(_contentRep.listAllTags().Single(x => x.ID == MainTagID), null, 5, null);
            products.ForEach(x => {
                SiteResourceService.loadUrls(x.siteResources);
                x.basket = new Basket() { Quantity = 1, VariantID = x.variant.ID };
            });

            return PartialView("_ItemSliderData", products);
        }
    }
}