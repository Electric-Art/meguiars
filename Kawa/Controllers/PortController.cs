﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.ViewModels;
using Kawa.Models.Services;
using PagedList;

namespace Kawa.Controllers
{
    [Authorize(Roles = AdminRole)]
    public class PortController : AdminSiteController
    {
        protected PortRepository _portRep;

        public PortController() : base()
        {
            _portRep = new PortRepository();
        }

        // GET: Port
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TestDate()
        {
            return Content(new DateTime(2017, 8, 1, 0, 0, 0).ToString());
        }

        public ActionResult TestPass()
        {
            string pass = "rocky006";
            string compare = "bc3fd0fcc285ffa437625d3be73a77a0";
            string salt = "Ca>Sm6@=XC";

            var service = new PortService()
            {

            };

            bool result = service.CheckPassword(pass, compare, salt);

            return Content("Done " + result);
        }

        [HttpGet]
        public ActionResult Product(int? id, int? t)
        {
            runExpire();
            var product = id.HasValue ? _productRep.listAllProducts().Single(p => p.ID == id) : _productRep.listAllProducts().OrderBy(x => x.ID).FirstOrDefault(p => p.formulation == null || p.formulation != "done");

            if (product == null)
                return Content("Congrats! You're all done");

            ViewBag.tags = _contentRep.listAllTags().Where(x => x.Type != Tag.TypeOption.SUPER_SECTION).OrderBy(x => x.name).ToList();
            ViewBag.tagtype = Tag.TypeOption.PRODUCT_SECTION;

            var svm = new AdminViewModel()
            {
                product = product
            };
            loadViewModel(svm);
            productSrSetrup(svm);
            loadSrs(product, true);
            SiteResourceService.loadUrls(product.siteResources.Where(p => p.SiteResourceType == SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE).ToArray());

            if (product.ID == 0 && t.HasValue)
            {
                product.tags = new List<Tag>() { ((List<Tag>)ViewBag.tags).Single(tg => tg.ID == t) };
            }
            else
            {
                product.tags = _contentRep.listAllTagsForProduct(product).ToList();
            }

            //product.questionAndAnswers.Add(new QandA() { ProductID = product.ID });

            return View(product);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Product(int? id, int? t, Product model)
        {
            runExpire();
            var product = _productRep.listAllProducts().Single(p => p.ID == id);

            ViewBag.tags = _contentRep.listAllTags().Where(x => x.Type != Tag.TypeOption.SUPER_SECTION).OrderBy(x => x.name).ToList();
            product.tags = Request["TagIDs"] == null ? new List<Tag>() : Request["TagIDs"].Split(',').Select(s => new Tag() { name = ((List<Tag>)ViewBag.tags).Where(tg => tg.ID == int.Parse(s)).Single().name, ID = int.Parse(s) }).ToList();

            product.formulation = "done";

            _productRep.saveProduct(product, true, false, false);

            var nextproduct = _productRep.listAllProducts().OrderBy(x => x.ID).FirstOrDefault(p => (p.formulation == null || p.formulation != "done") && p.ID > id);

            if (nextproduct == null)
                return Content("Congrats! You're all done");

            return RedirectToAction("Product", new { id = nextproduct.ID });
        }

        public ActionResult BurnFeedImages()
        {
            var portProducts = _productRep.listAllProducts(true,false).OrderBy(x => x.ID).ToList();
            var ResourceService = new SiteResourceService();
            portProducts.ForEach(x =>
            {
                x.siteResources.ToList().ForEach(y =>  {
                    ResourceService.RebuildForFeed(y,_srRep);    
                });
            });

            return Content("done");
        }


        public ActionResult FullPort()
        {
            return Content("shelved");
            /*
            TagService.RunForMenu();
            TagService.RunCache();
            BurnImages();
            BurnAlias();

            return Content("all done");
             */ 
        }


        public ActionResult BurnImages()
        {
            return Content("shelved");
            /*
            var portProducts = _productRep.listAllProducts(true,false).ToList();
            var ResourceService = new SiteResourceService();
            portProducts.ForEach(x =>
            {
                x.siteResources.ToList().ForEach(y =>  {
                    ResourceService.AddFromDisk(y,y.FileName,_srRep,false);    
                });
            });

            return Content("done");
             */
        }

        public ActionResult BurnAlias()
        {
            return Content("shelved");
            /*
            var portAliases = _portRep.listAllProductsAliases();
            portAliases.ForEach(x =>
            {
                var prod = _productRep.listAllProducts(false,false).SingleOrDefault(y => y.ID == x.productID);
                if(prod != null)
                {
                    prod.aliases += x.alias + "\n";
                    _productRep.saveProduct(prod,false,false);
                }
            });
            return Content("done");
             */ 
        }

        public ActionResult BurnUrlname()
        {
            return Content("shelved");
            /*
            var portProducts = _productRep.listAllProducts(true, false).Where(x => x.nameUrl == null).ToList();
            portProducts.ForEach(x =>
            {
                _productRep.saveProduct(x, false, false);
            });

            return Content("done");
            */
        }

        public ActionResult UpdateAliasURL()
        {
            return Content("shelved");
            /*
            var portProducts = _productRep.listAllProducts(false, false).Where(x => x.aliases.Length >= 80).ToList();
            portProducts.ForEach(x =>
            {
                _productRep.saveProduct(x, false, false);
            });

            return Content("done");
            */
        }

        public ActionResult AddProductsAndImages()
        {
            return Content("shelved");
            /*
            var portProducts = _portRep.listAllProductsWithImages().Take(2).ToList();

            var ResourceService = new SiteResourceService();

            portProducts.ForEach(x =>
            {
                var p = new Product()
                {
                    name = x.product,
                    sku = x.product_code,
                    blurb = x.full_description,
                    specifications = x.product_id.ToString(),
                    freeFrom = x.filename,
                    createdDate = DateTime.Now
                };
                _productRep.saveProduct(p,false,false);

                var sr = new SiteResource()
                {
                    SiteResourceType = SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE,
                    ProductID = p.ID,
                    ObjectID = p.getObjectID(),
                    ObjectType = p.getObjectType(),
                    orderNo = 1
                };

                ResourceService.AddFromDisk(sr,x.filename,_srRep);
            });


            return Content("done");
            */
        }
    }
}