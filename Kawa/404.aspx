﻿<% Response.StatusCode = 404 %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
        <title>404 Page Not Found</title>
<meta name="description">
<link rel="icon" type="image/x-icon" href="/favicon.ico">

    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript">
//<![CDATA[
(function(i,s,o,g,r,a,m){
i['GoogleAnalyticsObject']=r;
i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();
a=s.createElement(o), m=s.getElementsByTagName(o)[0];
a.async=1;
a.src=g;
m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-66668939-1', 'auto');
ga('send', 'pageview');
//]]>
</script>

    <link href="/Content/bootstrap-addons.css" rel="stylesheet"/>
<link href="/Content/bootstrap-five.css" rel="stylesheet"/>
<link href="/Content/site-1.css" rel="stylesheet"/>


    

    <script src="/Scripts/modernizr-2.6.2.js"></script>


    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

    
    
</head>
<body>
    

<div class="shadowbox"><div class="shadow"></div></div>


   
<div id="page-content" class="body-content">

     <div class="sectionheader" style="background-color:#2fc3e5;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12"><h1>404 Page Not Found</h1></div>
                </div>
            </div>
        </div>

        <div class="pagecontent">
            <div class="container ">
                <div class="row article">
                    <div class="col-sm-9 articleblock">
                        <div class="by">
                        </div>
                        <br /><br />
                        <p>Sorry the page you were looking for no longer exists.</p>
                        <p>Click <a href="/">here</a> to go to our home page.</p>
                     
     
                
                </div>
            </div>

        </div>
        


    
</div>
<footer>
   
</footer>



    

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.9/angular.min.js"></script>

    <script src="/Scripts/jquery.validate.js"></script>
<script src="/Scripts/jquery.validate.telepathy.js"></script>
<script src="/Scripts/jquery.validate.unobtrusive.js"></script>

    <script src="/Scripts/kawa-display-1.js"></script>
<script src="/Scripts/bootbox.min.js"></script>
<script src="/Scripts/sly.min.js"></script>

    <script src="/Scripts/ang/controllers/SiteModule.js"></script>
<script src="/Scripts/ang/controllers/SearchController.js"></script>

    <script src="/Scripts/kawa-slider.js"></script>

<script src="/Scripts/kawa-special-register.js"></script>
    
    <script src="/Scripts/jquery.cycle2.min.js"></script>
<script src="/Scripts/kawa-cycle.js"></script>

   


    
</body>
</html>

