
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[RelatedProducts]
AS
SELECT        p.sku, p2.sku AS relatedsku
FROM            Smits.dbo.ProductRelation AS pr LEFT OUTER JOIN
                         Smits.dbo.Product AS p ON p.ID = pr.ProductID LEFT OUTER JOIN
                         Smits.dbo.Product AS p2 ON p2.ID = pr.RelatedProductID
GO
