

/****** Object:  View [dbo].[LMCOutOfStockSKU]    Script Date: 10/08/2022 5:16:19 pm ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LMCOutOfStockSKU]
AS
SELECT        SKU, OutOfStock, ID, TypeE
FROM            Lovemycar.dbo.Product_OutOfStock
GO


