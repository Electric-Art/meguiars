
/****** Object:  Table [dbo].[Tag_Product]    Script Date: 26/04/2022 8:58:56 am ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Page_Product](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PageID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[Rank] [int] NOT NULL,
 CONSTRAINT [PK_Page_Product] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Page_Product] ADD  CONSTRAINT [DF_Page_Product_Rank]  DEFAULT ((0)) FOR [Rank]
GO

ALTER TABLE [dbo].[Page_Product]  WITH CHECK ADD  CONSTRAINT [FK_Page_Product_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO

ALTER TABLE [dbo].[Page_Product] CHECK CONSTRAINT [FK_Page_Product_Product]
GO

ALTER TABLE [dbo].[Page_Product]  WITH CHECK ADD  CONSTRAINT [FK_Page_Product_Page] FOREIGN KEY([PageID])
REFERENCES [dbo].[Page] ([ID])
GO

ALTER TABLE [dbo].[Page_Product] CHECK CONSTRAINT [FK_Page_Product_Page]
GO


