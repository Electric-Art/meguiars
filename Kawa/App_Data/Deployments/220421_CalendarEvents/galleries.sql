
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

CREATE VIEW [dbo].[LmcGallery]
AS
SELECT        n.ID, Lovemycar.dbo.SiteResource.ID AS sid, Lovemycar.dbo.SiteResource.FileName, Lovemycar.dbo.SiteResource.SiteResourceTypeE, Lovemycar.dbo.SiteResource.Caption, n.Name, n.Html, n.IsHidden, n.NameURL, 
                         n.metadescription, n.metakeywords, n.Intro, n.CategoryID, n.Rank, n.SystemTypeE, n.OverwriteURL, n.TypeE
FROM            Lovemycar.dbo.Page AS n INNER JOIN
                         Lovemycar.dbo.SiteResource ON n.ID = Lovemycar.dbo.SiteResource.PageID
WHERE        (n.TypeE = 4)
GO
COMMIT
