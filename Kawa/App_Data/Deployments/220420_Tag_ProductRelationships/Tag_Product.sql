/*
   Wednesday, 20 April 20221:00:33 pm
   User: 
   Server: ZERO1
   Database: Meguiars
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag_Product ADD
	Rank int NOT NULL CONSTRAINT DF_Tag_Product_Rank DEFAULT 0
GO
ALTER TABLE dbo.Tag_Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
