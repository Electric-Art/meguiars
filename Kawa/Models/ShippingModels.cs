﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models.ViewModels;

namespace Kawa.Models
{
    /// <summary>
    /// Summary description for Carrier.
    /// </summary>
    public class Carrier
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public DateTime createDate { get; set; }
    }

    /// <summary>
    /// Summary description for ShippingMethod.
	/// </summary>
    public class ShippingMethod
    {
        public int ID { get; set; }
		public string Name { get; set; }
		public string DeliveryTime { get; set; }
		public string TrackingUrl { get; set; }
		public bool isHidden { get; set; }
		public DateTime createDate { get; set; }

        public Order OrderForPrice { get; set; }
        public string BasketName {
            get {
                if (OrderForPrice == null) return Name;

                if (OrderForPrice.hasFreeShipping())
                {
                    return string.Format("<b>{0} FREE</b><br/>", Name); 
                }
                else if (OrderForPrice.hasSplitTheDifference())
                {
                    return string.Format("<b>{0} {1}</b><br/>({2})", Name, OrderForPrice.FreightTotal.ToString("c"), OrderForPrice.getSplitTheDifferenceText().Replace("selected", "selection")); 
                }
                else
                {
                    return string.Format("<b>{0} {1}</b>", Name, OrderForPrice.FreightTotal.ToString("c"));
                }
            } 
        }
    }

    /// <summary>
	/// Summary description for ShippingRate.
	/// </summary>
	public class ShippingRate
	{
		public int ID { get; set; }
		public int ShippingMethodID { get; set; }
		public int? LocationID { get; set; }
		public int Type { get; set; }
        [Required]
        public decimal Rate { get; set; }
        [Required]
        public decimal? WeightDependency { get; set; }
		public decimal? CostDependency { get; set; }
		public bool isHidden { get; set; }
		public DateTime createDate { get; set; }

        public bool isLastWeight { get; set; }
	}

    /// <summary>
	/// Summary description for Location.
	/// </summary>
	public class Location
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public string Code { get; set; }
        public bool isHidden { get; set; }
		public DateTime createDate { get; set; }
	}

    /// <summary>
	/// Summary description for Country.
	/// </summary>
	public class Country
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public int? Group { get; set; }
		public string CountryName { get; set; }
		public int? Rank { get; set; }
		public bool isHidden { get; set; }
		public string Code { get; set; }
		public string CodeA3 { get; set; }
		public string CodeN3 { get; set; }
		public string RegionCode { get; set; }
        public DateTime createDate { get; set; }
        public bool isBlocked { get; set; }

        public bool isAddressSearch { get; set; }

        [DisplayName("Numbers only postcodes")]
        public bool isIntPostCode { get; set; }

    }

    /// <summary>
    /// Summary description for LocationPostcodeRange.
    /// </summary>
    public class LocationPostcodeRange
    {
        public int ID { get; set; }
        public int LocationID { get; set; }
        public int FromPostcode { get; set; }
        public int ToPostcode { get; set; }

        public string RangeName
        {
            get
            {
                if (FromPostcode == ToPostcode) return FromPostcode.ToString();
                return string.Format("{0} to {1}", FromPostcode, ToPostcode);
            }
        }
    }

    /// <summary>
	/// Summary description for State.
	/// </summary>
	public class State
	{
		public int ID { get; set; }
		public string Name { get; set; }
		public int CountryID { get; set; }
		public string Code { get; set; }
        public bool isHidden { get; set; }
        public DateTime createDate { get; set; }
	}

    public class CountryList
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Group { get; set; }
        public string CountryName { get; set; }
        public int Rank { get; set; }

        public static string[] getCountryNames()
        {
            return new string[] {
                                "Afghanistan","Albania","Algeria","Andorra","Angola","Antigua and Barbuda","Argentina","Armenia","Australia","Austria","Azerbaijan"
                                ,"Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belize","Belgium","Benin","Bhutan","Bolivia","Botswana","Bosnia and Herzegovina","Brazil","Brunei","Bulgaria","Burkina Faso","Burundi"
                                ,"Cambodia","Cameroon","Canada","Cape Verde","Central African Republic","Chad","Chile","China","China: Hong Kong","China: Macau","China: Taiwan","Colombia","Comoros","Congo","Costa Rica","Côte d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic"
                                ,"Denmark","Djibouti","Dominica","Dominican Republic"
                                ,"East Timor","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia"
                                ,"Fijii","Finland","France"
                                ,"Gabon","The Gambia","Gaza Strip","Germany","Georgia","Ghana","Greece","Grenada","Guatemala","Guinea","Guinea-Bissau","Guyana"
                                ,"Haiti","Honduras","Hong Kong","Hungary"
                                ,"Iceland","India","Indonesia","Iran","Iraq","Ireland (Eire)","Ireland (Northern)","Israel","Italy"
                                ,"Jamaica","Japan","Jordan"
                                ,"Kazakhstan","Kenya","Korea, North","Korea, South","Kuwait","Kyrgyzstan"
                                ,"Laos -","Latvia -","Lebanon -","Lesotho -","Liberia -","Libya -","Lithuania","Luxembourg"
                                ,"Macao","Macedonia","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Micronesia","Moldova","Monaco","Mongolia","Morocco","Mexico","Mozambique","Myanmar (Burma)"
                                ,"Namibia","Nauru","Nepal","Netherlands","New Zealand","Nicaragua","Niger","Nigeria","Norway"
                                ,"Oman"
                                ,"Panama","Peru","Pakistan","Papua New Guinea","Palau","Poland","Portugal"
                                ,"Qatar"
                                ,"Romania","Russia","Rwanda"
                                ,"St. Kitts and Nevis","Saint Lucia","St. Vincent and the Grenadines","Samoa","San Marino","São Tomé and Príncipe","Saudi Arabia","Senegal","Serbia and Montenegro","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","Spain","Sri Lanka","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria"
                                ,"Taiwa","Tajikistan","Tanzania","Togo","Thailand","Trinidad and Tobago","Tunisia","Turkey","Turkmenistan","Tuvalu"
                                ,"Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan"
                                ,"Venezuela","Vietnam"
                                ,"West Bank","Western Sahara"
                                ,"Yemen","Yugoslavia now Serbia & Montenegro"
                                ,"Zaire (Congo)","Zambia","Zimbabwe"
                            };
        }

        public static string[] getStateNames()
        {
            return new string[] {
                                "New South Wales","Queensland","South Australia","Tasmania","Victoria","Western Australia","Northern Territory","ACT"
            };
        }
    }
    
}

namespace Kawa.Models.ViewModels
{
    public class LocationListModel
    {
        public string CheckBoxListName { get { return "Locations"; } }
        public List<Location> PossibleLocations { get; set; }
        public List<Location> Locations { get; set; }
    }

    public class LocationScopeViewModel
    {
        public Location Location { get; set; }
        
        public List<Country> Countries { get; set; }
        public int? CountryID { get; set; }
        public List<Country> ScopeCountries { get; set; }

        public List<State> States { get; set; }
        public int? StateID { get; set; }
        public List<State> ScopeStates { get; set; }

        public List<LocationPostcodeRange> ScopePostcodeRanges { get; set; }
        public int? FromPostcode { get; set; }
        public int? ToPostcode { get; set; }
    }


    public class ShippingMethodRatesViewModel
    {
        public ShippingMethod ShippingMethod { get; set; }
        public List<Location> Locations { get; set; }
        public Location Location { get; set; }
        public int? LocationID { get; set; }
        public List<ShippingRate> Rates { get; set; }

        public List<SelectListItem> getLocationsSelectList()
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "choose", Value = "", Selected = true } };
            items.AddRange(Locations.Select(t => new SelectListItem() { Text = t.Name, Value = t.ID.ToString() }));
            return items;
        }
    }
}