﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models;
using System.Net.Mail;
using System.Configuration;
using Kawa.Models.Extensions;

namespace Kawa.Models.Services
{
    public class AlertService
    {

        public static void SendTechMonitorAlert(string subject, string body)
        {
            MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["TechFromEmail"], ConfigurationManager.AppSettings["TechToEmail"] ?? "scott@telepathy.co.nz")
            {
                Subject = subject,
                IsBodyHtml = true,
                Body = body
            };
            EmailSendService.Send(msg,true);
        }
    }
}