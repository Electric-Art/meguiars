﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models;
using Kawa.Models.ViewModels;
using System.Runtime.Caching;
using System.Configuration;

namespace Kawa.Models.Services
{
    public class TagService
    {
        private ContentRepository _contentRep;
        private ProductRepository _productRep;
        private CacheRepository _cacheRep;
        private static MemoryCache _cache = MemoryCache.Default;

        public TagSet TagSet { get; set; }

        public List<Tag> TabCategories
        {
            get
            {
                return GetForMenu().MainCategories.Where(t => 
                    t.SystemTag != Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE &&
                    t.SystemTag != Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL &&
                    t.SystemTag != Tag.SystemTagOption.PRODUCT_MAIN_CAT_CONDITIONS &&
                    t.SystemTag != Tag.SystemTagOption.SUPER_CAT_BRANDS).ToList();
            }
        }

        public TagService(ContentRepository contentRep, ProductRepository productRep, CacheRepository cacheRep)
        {
            _contentRep = contentRep;
            _productRep = productRep;
            _cacheRep = cacheRep;
        }

        #region Menu and Footer

        public TagSet GetForMenu()
        {
            if (!_cache.Contains("TagSet"))
                RunForMenu();
            return _cache.Get("TagSet") as TagSet;
        }

        public void RunForMenu()
        {
            var supertag = _contentRep.listAllTags().Single(x => x.SystemTag == Tag.SystemTagOption.SUPER_CAT_PRODUCTS);
            var tags = _contentRep.listAllTags().Where(t =>
                (t.Type == Tag.TypeOption.PRODUCT_SECTION || t.Type == Tag.TypeOption.BRAND_SECTION || t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE || t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL)
                && !t.isHidden).ToList();
            supertag.subtags = tags.Where(x => x.ParentID == supertag.ID).OrderBy(x => x.orderNo ?? 1000).ToList();
            var productTags = new List<Tag> { _contentRep.listAllTags().Single(x => x.SystemTag == Tag.SystemTagOption.SUPER_CAT_BRANDS) };
            productTags.AddRange(supertag.subtags);
            productTags.ForEach(x =>
            {
                var isBrands = x.SystemTag.HasValue && x.SystemTag == Tag.SystemTagOption.SUPER_CAT_BRANDS;
                x.subtags = tags.Where(y => y.ParentID == x.ID).OrderBy(y => y.orderNo ?? 1000).ThenBy(y => y.name).Take(!isBrands ? 27 : 34).ToList();
                if (isBrands)
                {
                    ///x.subtags = _contentRep.listAllTagsForBrandProducts(x).ToList();
                    x.subtags2 = x.subtags;
                    if (x.subtags.Count() == 34)
                        x.subtags.Add(new Tag()
                        {
                            name = "<b>View all brands >></b>",
                            plugurl = x.getUrl() + "?mode=brands"
                        });
                    
                }
          
                else
                {
                    x.subtags2 = _contentRep.listAllTagsForProductsWithTag(x).Where(t => t.Type == Tag.TypeOption.BRAND_SECTION).OrderBy(y => y.orderNo ?? 1000).ThenBy(y => y.name).Take(6).ToList();
                    if (x.subtags.Count() >= 27)
                        x.subtags.Add(new Tag()
                        {
                            name = "<b class='vmcats'>View all categories >></b>",
                            plugurl = x.getUrl()
                        });

                    if (x.subtags2.Count() >= 6)
                        x.subtags2.Add(new Tag()
                        {
                            name = "<b>View all brands >></b>",
                            plugurl = productTags.First().getUrl()
                        });
                }
            });

            productTags.Add(tags.Single(t => t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE));
            productTags.Add(tags.Single(t => t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL));
            supertag.subtags.Add(tags.Single(t => t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE));
            supertag.subtags.Add(tags.Single(t => t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL));
            List<Tag> brands = _contentRep.listAllTags().Where(t => t.Type == Tag.TypeOption.BRAND_SECTION && !t.isHidden).ToList();
            brands.ForEach(x =>
            {
                x.subtags = _contentRep.listAllTagsForBrandProducts(x).ToList();
            });


            TagSet = new Services.TagSet()
            {
                CategorySuperTag = supertag,
                CategorySet = tags,
                MainCategories = productTags,
                AdhocPopupTags = _contentRep.listAllTags().Where(x => x.SystemTag.HasValue && x.html != null && x.html != "").ToList(),
                ShippingBanner1 = _contentRep.listAllPages().SingleOrDefault(x => x.Type == Page.TypeOption.TINY_BANNER && x.SystemPage == Page.SystemPageOption.SHIPPING_BANNER_1) ?? Page.ShippingBannerDefault(1),
                ShippingBanner2 = _contentRep.listAllPages().SingleOrDefault(x => x.Type == Page.TypeOption.TINY_BANNER && x.SystemPage == Page.SystemPageOption.SHIPPING_BANNER_2) ?? Page.ShippingBannerDefault(2),
                FooterPages = _contentRep.listAllPages().Where(p => !p.isHidden && (p.SystemPage != null)).ToList(),
                SignupModule = _contentRep.listAllSlots(true, false).Where(s => s.Spot == Slot.SpotOption.SITE_WIDE_SIGNUP).OrderBy(s => s.orderNo).FirstOrDefault(),
                SiteWideSlot = _contentRep.listAllSlots(true).OrderBy(x => x.orderNo).FirstOrDefault(x => x.Spot == Slot.SpotOption.SITE_WIDE_BANNER),
                TrendingSearches = _cacheRep.getTrendingStatEntities(Tag.SystemTagOption.SEARCH_ENTITY_TRENDING),
                Catalogs = _contentRep.listAllPages(true).Where(x => x.Type == Page.TypeOption.CATALOG_CONTAINER).FirstOrDefault(),
                Brands = brands
            };

            SiteResourceService.loadUrls(TagSet.Catalogs.siteResources);

            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(1);

            _cache.Set("TagSet", TagSet, cacheItemPolicy);
        }

        #endregion

        #region Home page 

        public HomeViewModels GetForHome()
        {
            if (!_cache.Contains("HomeSet"))
                RunForHome();
            return _cache.Get("HomeSet") as HomeViewModels;
        }

        public void RunForHome()
        {
            var vm = new HomeViewModels()
            {
                Metadesciption = _contentRep.listAllPages(true).SingleOrDefault(p =>
                                p.SystemPage == Page.SystemPageOption.HOMEPAGE_METADESCRIPTION && !p.isHidden
                                ),
                BillboardPage = _contentRep.listAllPages(true).SingleOrDefault(p =>
                                p.Type == Page.TypeOption.CONTENT &&
                                p.SystemPage == Page.SystemPageOption.HOME_PAGE_SLIDE_SHOW
                                ) ?? new Page() { siteResources = new SiteResource[0] },
                BrandPage = _contentRep.listAllPages(true).SingleOrDefault(p =>
                                p.Type == Page.TypeOption.CONTENT &&
                                p.SystemPage == Page.SystemPageOption.POPULAR_BRANDS
                                ) ?? new Page() { siteResources = new SiteResource[0] },
                RetractionPage = _contentRep.listAllPages(true).SingleOrDefault(p =>
                                p.SystemPage == Page.SystemPageOption.RETRACTION && !p.isHidden
                                ),
                Slots = _contentRep.listAllSlots(true, true).Where(s => s.Spot == Slot.SpotOption.HOME_PAGE_MODULE && !s.page.isHidden).ToList(),
                VideoSlots = _contentRep.listAllSlots(true, true).Where(s => s.Spot == Slot.SpotOption.VIDEO_MODULE && !s.page.isHidden).ToList(),
                Sliders = GetSliderModels()
            };

            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(1);
            _cache.Set("HomeSet", vm, cacheItemPolicy);
        }

        public List<ProductSliderViewModel> GetSliderModels()
        {
            var tags = _contentRep.listAllTags().Where(t =>
                (t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_POPULAR_PRODUCTS ||
                t.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS ||
                t.SystemTag == Tag.SystemTagOption.SPECIALS)
                && !t.isHidden).ToList();

            var sliders = new List<ProductSliderViewModel>(){
               
                new ProductSliderViewModel()
                {
                    MainTag = _contentRep.listAllTags().FirstOrDefault(x => x.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS),
                    Tags = TabCategories
                },
                new ProductSliderViewModel()
                {
                    MainTag = _contentRep.listAllTags().FirstOrDefault(x => x.SystemTag == Tag.SystemTagOption.SPECIALS),
                    Tags = TabCategories
                }
            };

            sliders.ForEach(x =>
            {
                x.Products = GetSliderProducts(x.MainTag,5,null).OrderBy(xa=>xa.ProductTagRelationShipRank).ToList();
                if(x.MainTag.SystemTag == Tag.SystemTagOption.SPECIALS)///also add the trending products to this one
                {
                    Tag subtag = _contentRep.listAllTags().FirstOrDefault(xx => xx.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_TRENDING_NOW);
                    x.Products.AddRange(GetSliderProducts(subtag, 24, null).OrderBy(xa => xa.ProductTagRelationShipRank).ToList());
                }
                x.Products.ForEach(y => { SiteResourceService.loadUrls(y.siteResources); y.basket = new Basket() { Quantity = 1, VariantID = y.variant.ID }; });
            });

            return sliders;
        }

        public List<Product> GetSliderProducts(Tag tag, int? take, int? skip, int? RelatedtagID = null)
        {
            List<Product> products = null;
            switch (tag.SystemTag)
            {
                case Tag.SystemTagOption.PRODUCT_CAT_POPULAR_PRODUCTS:
                    products = (RelatedtagID.HasValue ?
                        _productRep.listProductsForTagIntersection(_contentRep.listAllTags().Single(x => x.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_POPULAR_SUBCAT_PRODUCTS).ID, (int)RelatedtagID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList() :
                        _productRep.listProductsForTag(tag.ID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList()).OrderBy(x => x.name).ToList(); 
                    break;
                case Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS:
                    products = RelatedtagID.HasValue ?
                        _productRep.listProductsFromCacheIntersectionWithFilter((Tag.SystemTagOption)tag.SystemTag, tag.ID, (int)RelatedtagID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList() :
                        _productRep.listProductsFromCachWithFilter((Tag.SystemTagOption)tag.SystemTag, tag.ID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList();
                    break;
                case Tag.SystemTagOption.SPECIALS:
                    products = _productRep.listProductsForTag(tag.ID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList().OrderBy(x => x.name).ToList();
                    /*
                    products = RelatedtagID.HasValue ?
                        _productRep.listProductsForAlgorithmIntersectionWithFilter((Tag.SystemTagOption)tag.SystemTag, tag.ID, (int)RelatedtagID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList() :
                        _productRep.listProductsForAlgorithmWithFilter((Tag.SystemTagOption)tag.SystemTag, tag.ID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList();
                    */
                    break;
                case Tag.SystemTagOption.PRODUCT_CAT_TRENDING_NOW:
                    products = _productRep.listProductsForTagNotIncludingParents(tag.ID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList().OrderBy(x => x.name).ToList();
                    ///products = _productRep.listProductsFromCachWithFilter((Tag.SystemTagOption)tag.SystemTag, tag.ID, true, true).Where(y => !y.isHidden && y.HasVariant).ToList();
                    break;
            }
            if (skip.HasValue)
                products = products.Skip((int)skip).ToList();
            if (take.HasValue)
                products = products.Take((int)take).ToList();
            return products;
        }

        public void RunCache()
        {
            var tag = _contentRep.listAllTags().Single(x => x.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS);
            
            CacheRepository _cRep = new CacheRepository();

            _cRep.cacheProductsWithAlorithm(Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS, tag.ID);
            TabCategories.ForEach(y =>
            {
                _contentRep.clearCache(y.ID, null);
                _cRep.cacheProductsWithAlgorithmIntersection(Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS, tag.ID, y.ID);
            });

            tag = _contentRep.listAllTags().SingleOrDefault(x => x.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_TRENDING_NOW);
            if(tag == null)
            {
                tag = AutoCreateTag(Tag.SystemTagOption.PRODUCT_CAT_TRENDING_NOW);
            }
            _cRep.cacheProductsWithAlorithm(Tag.SystemTagOption.PRODUCT_CAT_TRENDING_NOW, tag.ID);


            tag = _contentRep.listAllTags().SingleOrDefault(x => x.SystemTag == Tag.SystemTagOption.SEARCH_ENTITY_TRENDING);
            if(tag == null)
            {
                tag = new Tag()
                {
                    name = "Search Entity Trending",
                    Type = Tag.TypeOption.SEARCH_TAGS,
                    SystemTag = Tag.SystemTagOption.SEARCH_ENTITY_TRENDING
                };
                _contentRep.saveTag(tag);
            }
            _cRep.cacheProductsWithAlorithm(Tag.SystemTagOption.SEARCH_ENTITY_TRENDING, tag.ID);
        }

        private Tag AutoCreateTag(Tag.SystemTagOption SystemTag)
        {
            switch (SystemTag)
            {
                case Tag.SystemTagOption.PRODUCT_CAT_TRENDING_NOW:
                    var tag = new Tag()
                    {
                        name = "Trending now",
                        Type = Tag.TypeOption.ADHOC_PRODUCT_GROUP,
                        ParentID = 37,
                        SystemTag = SystemTag,
                        colour = "#009fc2"
                    };
                    _contentRep.saveTag(tag);
                    return tag;
            }
            return null;
        }

        #endregion
    }

    public class TagSet
    {
        public Tag CategorySuperTag;
        public List<Tag> CategorySet;
        public List<Tag> MainCategories;
        public List<Tag> AdhocPopupTags;
        public Page ShippingBanner1;
        public Page ShippingBanner2;
        public List<Page> FooterPages;
        public Slot SignupModule;
        public Slot SiteWideSlot;
        public List<ISearchResult> TrendingSearches;
        public List<Tag> Brands;
        public Page Catalogs;
    }
}