﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using InstaSharp;
using InstaSharp.Endpoints;
using InstaSharp.Models.Responses;
using System.Threading;
using System.Threading.Tasks;

namespace Kawa.Models.Services
{
    public class InstagramService
    {
        private InstagramConfig config;
        private ContentRepository _contentRep;

        public InstagramService(string redirecturl, ContentRepository _contentRep)
        {
            var clientId = ConfigurationManager.AppSettings["InstagramClientID"];
            var clientSecret = ConfigurationManager.AppSettings["InstagramClientSecret"];
            var redirectUri = redirecturl;
            var realtimeUri = "";
            this._contentRep = _contentRep;

            config = new InstagramConfig(clientId, clientSecret, redirectUri, realtimeUri);
        }

        public async Task<bool> tryForUpdate()
        {
            Page page = _contentRep.listAllPages().SingleOrDefault(p => p.SystemPage == Page.SystemPageOption.INSTAGRAM_LAST_POST) ??
                                new Page()
                                {
                                    SystemPage = Models.Page.SystemPageOption.INSTAGRAM_LAST_POST,
                                    name = "Most Recent Instagram Post",
                                    createdDate = DateTime.Now,
                                    Type = Models.Page.TypeOption.SOCIAL_IMPORT,
                                    html4 = ""
                                };

            if (string.IsNullOrEmpty(page.html4))
                return false;

            OAuthResponse oauthResponse = new OAuthResponse();
            oauthResponse.AccessToken = page.html4.Split(',')[0];
            oauthResponse.User = new InstaSharp.Models.UserInfo { Id = long.Parse(page.html4.Split(',')[1]) };
            var users = new InstaSharp.Endpoints.Users(config, oauthResponse);
            var feed = await users.Recent(ConfigurationManager.AppSettings["InstagramUserID"]);
            // both the client secret and the token are considered sensitive data, so we won't be
            // sending them back to the browser. we'll only store them temporarily.  If a user's session times
            // out, they will have to click on the authenticate button again - sorry bout yer luck.
            //Session.Add("InstaSharp.AuthInfo", oauthResponse);
            //page.html4 = oauthResponse.

            if (feed.Data != null && feed.Data.Count() > 0)
            {
                feedPage(page, feed, oauthResponse);
                return true;
            }
            else
            {
                // all done, lets redirect to the home controller which will send some intial data to the app
                return false;
            }
        }

        private void feedPage(Page page, MediasResponse feed, OAuthResponse oauthResponse)
        {
            var img = feed.Data.First().Images.StandardResolution;
            if (feed.Data.First().Caption != null)
                page.html2 = feed.Data.First().Caption.Text;
            page.html3 = img.Url;
            page.html4 = oauthResponse.AccessToken + "," + oauthResponse.User.Id;
            _contentRep.savePage(page);
        }

        public string getLoginLink()
        {
            var scopes = new List<OAuth.Scope>();
            scopes.Add(InstaSharp.OAuth.Scope.Basic);

            return InstaSharp.OAuth.AuthLink(config.OAuthUri + "authorize", config.ClientId, config.RedirectUri, scopes, InstaSharp.OAuth.ResponseType.Code);
        }

        public async Task<bool> recieveLoginResult(string code)
        {
            Page page = _contentRep.listAllPages().SingleOrDefault(p => p.SystemPage == Page.SystemPageOption.INSTAGRAM_LAST_POST) ??
                                new Page()
                                {
                                    SystemPage = Models.Page.SystemPageOption.INSTAGRAM_LAST_POST,
                                    name = "Most Recent Instagram Post",
                                    createdDate = DateTime.Now,
                                    Type = Models.Page.TypeOption.SOCIAL_IMPORT,
                                    html4 = ""
                                };
            // add this code to the auth object
            var auth = new OAuth(config);

            // now we have to call back to instagram and include the code they gave us
            // along with our client secret
            var oauthResponse = await auth.RequestToken(code);
            var users = new InstaSharp.Endpoints.Users(config, oauthResponse);
            var feed = await users.Recent(ConfigurationManager.AppSettings["InstagramUserID"]);
            // both the client secret and the token are considered sensitive data, so we won't be
            // sending them back to the browser. we'll only store them temporarily.  If a user's session times
            // out, they will have to click on the authenticate button again - sorry bout yer luck.
            //Session.Add("InstaSharp.AuthInfo", oauthResponse);
            //page.html4 = oauthResponse.

            if (feed.Data != null && feed.Data.Count() > 0)
            {
                feedPage(page, feed, oauthResponse);
                return true;
            }
            else
            { 
            // all done, lets redirect to the home controller which will send some intial data to the app
                return false;
            }
        }
    }
}