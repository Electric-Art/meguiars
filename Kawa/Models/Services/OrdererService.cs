﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models;

namespace Kawa.Models.Services
{
    public class OrdererService
    {
        /// <summary>
        /// Check to make sure the ordered set has order numbers, if not it sets them in the order the items are provided.
        /// </summary>
        /// <param name="children">The items to be checked and set if needed.</param>
        /// <returns>Whether the children need to be saved or not.</returns>
        public static bool orderingCheck(List<IOrderedObject> children)
        {
            if(children.All(o => o.orderNo == null))
            {
                int i = 1;
                foreach(var child in children)
                {
                    child.orderNo = i;
                    i++;
                }
                return true;
            }
            return false;
        }

        public static void setNextOrderNo(IOrderedObject toSet,List<IOrderedObject> children)
        {
            IOrderedObject or = children.OrderBy(o => o.orderNo).LastOrDefault();
            toSet.orderNo = or == null ? 1 : or.orderNo + 1;
        }

        public static List<IOrderedObject> moveUp(IOrderedObject toMove, List<IOrderedObject> children)
        {
            List<IOrderedObject> savers = new List<IOrderedObject>();
            children = children.OrderBy(o => o.orderNo).ToList();
            int i = 0;
            foreach(var obj in children)
            {
                if (toMove.ID == obj.ID)
                {
                    if (i != 0)
                    {
                        int orderNo = (int)children[i - 1].orderNo;
                        children[i - 1].orderNo = toMove.orderNo;
                        toMove.orderNo = orderNo;
                        savers.Add(children[i - 1]);
                        savers.Add(toMove);
                    }
                }
                i++;
            }
            return savers;
        }

        public static List<IOrderedObject> moveDown(IOrderedObject toMove, List<IOrderedObject> children)
        {
            List<IOrderedObject> savers = new List<IOrderedObject>();
            children = children.OrderBy(o => o.orderNo).ToList();
            int i = 0;
            foreach (var obj in children)
            {
                if (toMove.ID == obj.ID)
                {
                    if (i != children.Count-1)
                    {
                        int orderNo = (int)children[i + 1].orderNo;
                        children[i + 1].orderNo = toMove.orderNo;
                        toMove.orderNo = orderNo;
                        savers.Add(children[i + 1]);
                        savers.Add(toMove);
                    }
                }
                i++;
            }
            return savers;
        }

        public static List<IOrderedObject> delete(IOrderedObject toDelete, List<IOrderedObject> children)
        {
            List<IOrderedObject> savers = new List<IOrderedObject>();
            children = children.OrderByDescending(o => o.orderNo).ToList();
            int i = 0;
            foreach (var obj in children)
            {
                if (toDelete.ID != obj.ID)
                {
                    obj.orderNo = obj.orderNo - 1;
                    savers.Add(obj);
                }
                else
                    break;
                i++;
            }
            return savers;
        }

    }
}