﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kawa.Models.Services
{
    public class SiteService
    {
        public static List<PaymentServiceStore.PaymentTypeOption> getPaymentOptions(bool fullOptions)
        {
            if(fullOptions)
                return new List<PaymentServiceStore.PaymentTypeOption>()
                {
                    PaymentServiceStore.PaymentTypeOption.EWAY,
                    PaymentServiceStore.PaymentTypeOption.PAYPAL,
                    PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT,
                };
            else
                return new List<PaymentServiceStore.PaymentTypeOption>()
                {
                    PaymentServiceStore.PaymentTypeOption.VERIFONE
                };
        }

        public static string getFullSiteName()
        {
            return "meguiars.co.nz";
        }

        public static string getDomainBase()
        {
            string returnUri = (HttpContext.Current.Request.IsSecureConnection ? "https://" : "http://") + HttpContext.Current.Request.Url.Host;
            if (HttpContext.Current.Request.Url.Host == "localhost")
                returnUri += ":" + HttpContext.Current.Request.Url.Port;
            return returnUri;
        }
    }
}