﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Kawa.Controllers;
using Kawa.Models.ViewModels;
using Kawa.Models.Extensions;
using AutoMapper;
using System.Threading.Tasks;

namespace Kawa.Models.Services
{
    public class OrderService
    {
        public static decimal AMEX_SURCHARGE_PERCENTAGE = 0.02m;

        private ProductRepository _productRep;
        private OrderRepository _orderRep;
        private SiteResourceRepository _srRep;
        private ContentRepository _contentRep;
        private SiteController _controller;
        private PromotionService PromotionService;
        private ShippingService ShippingService;
        private TagService TagService;

        public OrderService(ProductRepository productRep, OrderRepository orderRep, ContentRepository contentRep, SiteResourceRepository srRep, SiteController controller, PromotionService PromotionService, ShippingService ShippingService, TagService TagService)
        {
            _productRep = productRep;
            _orderRep = orderRep;
            _srRep = srRep;
            _contentRep = contentRep;
            _controller = controller;
            this.PromotionService = PromotionService;
            this.ShippingService = ShippingService;
            this.TagService = TagService;
        }

        public string getSession()
        {
            if (HttpContext.Current.Request.Cookies["KawaSession"] != null)
            {
                return HttpContext.Current.Request.Cookies["KawaSession"].Value;
            }
            else
            {
                return createSession();
            }
        }
        
        public string createSession(bool addCookie = true)
        {
           
            PromotionService.resetVoucher();
            string newGUID = System.Guid.NewGuid().ToString();
            if(addCookie)
            { 
                HttpContext.Current.Session["KawaSession"] = newGUID;
                HttpCookie hc = new HttpCookie("KawaSession");
                hc.Value = newGUID;
                hc.Expires = System.DateTime.Now.AddDays(7);
                HttpContext.Current.Response.Cookies.Add(hc);
            }
            return newGUID;
        }
        public string getFavSession()
        {
            if (HttpContext.Current.Request.Cookies["KawaSession_Fav"] != null)
            {
                return HttpContext.Current.Request.Cookies["KawaSession_Fav"].Value;
            }
            else
            {
                return createFavSession();
            }
        }
        public string createFavSession(bool addCookie = true)
        {

            PromotionService.resetVoucher();
            string newGUID = System.Guid.NewGuid().ToString();
            if (addCookie)
            {
                HttpContext.Current.Session["KawaSession_Fav"] = newGUID;
                HttpCookie hc = new HttpCookie("KawaSession_Fav");
                hc.Value = newGUID;
                hc.Expires = System.DateTime.Now.AddDays(7);
                HttpContext.Current.Response.Cookies.Add(hc);
            }
            return newGUID;
        }

        public List<AccountDetail> getPrepopOrderDetails(Order order, Account account, Order populateWithOrder)
        {
            AccountDetail customerDetails = null;
            AccountDetailDelivery deliveryDetails = null;
            order.Details = _orderRep.listAllAccountDetails().Where(d => d.OrderID == order.ID).ToList();
            if (order.CustomerDetails != null && order.DeliveryDetails != null)
            {
                customerDetails = order.CustomerDetails;
                deliveryDetails = Mapper.Map<AccountDetailDelivery>(order.DeliveryDetails);
            }
            else
            {
                Order populateOrder = null;

                if (populateWithOrder != null)
                    populateOrder = populateWithOrder;
                else
                {
                    if (account != null)
                    {
                        if (!order.AccountID.HasValue)
                        {
                            order.AccountID = account.ID;
                            _orderRep.saveOrder(order);
                        }
                        populateOrder = _orderRep.listAllOrders(true).OrderByDescending(o => o.ID).FirstOrDefault(o => o.AccountID == account.ID);
                    }
                }
                
                if (populateOrder != null)
                {
                    populateOrder.Details = _orderRep.listAllAccountDetails().Where(d => d.OrderID == populateOrder.ID).ToList();
                    customerDetails = populateOrder.CustomerDetails;
                    customerDetails.ID = 0;
                    customerDetails.OrderID = order.ID;
                    deliveryDetails = Mapper.Map<AccountDetailDelivery>(populateOrder.DeliveryDetails);
                    deliveryDetails.ID = 0;
                    deliveryDetails.OrderID = order.ID;
                }
                else
                {
                    if (account != null)
                    {
                        customerDetails = new AccountDetail()
                        {
                            isMailer = true,
                            Email = account.UserName,
                            CountryID = order.CountryID
                        };
                        deliveryDetails = new AccountDetailDelivery()
                        {
                            Email = account.UserName
                        };
                    }
                }
            }
            return new List<AccountDetail>()
            {
                customerDetails ?? new AccountDetail() { isMailer = true, CountryID = order.CountryID },
                deliveryDetails ?? new AccountDetailDelivery()
            };
        }

        public OrderSummary getSummary()
        {
            var order = getSummaryOrder();
            return new OrderSummary()
            {
                BasketCount = order.orderItems.Sum(x => x.Quantity),
                Total = order.orderItems.Sum(x => x.LinePrice)
            };
        }

        public Order getSessionOrder()
        {
            var sessionOrders = _orderRep.listAllOrders().Where(o => o.SessionKey == getSession()).OrderByDescending(o => o.ID).ToList();
            if (sessionOrders.Any(o => o.SystemStatus > Order.SystemStatusOption.IN_PROGRESS
                /* || (o.SystemStatus == Order.SystemStatusOption.IN_PROGRESS && o.PaymentType == PaymentServiceStore.PaymentTypeOption.PAYPAL )*/))
            {
                return getNewDefaultOrder(null);
            }
            return sessionOrders.FirstOrDefault() ?? getNewDefaultOrder(getSession());
        }

        public Order getNewDefaultOrder(string sessionKey, bool addCookie = true)
        {
            var defaultCountry = ShippingService.getDefaultCountry();
            var newOrder = new Order() { SessionKey = sessionKey ?? createSession(addCookie), CountryID = defaultCountry.ID, IsAdminOrder = false };
            newOrder.ShippingMethodID = ShippingService.getShippingMethods(newOrder).Any() ? ShippingService.getShippingMethods(newOrder).First().ID : (int?)null;
            return newOrder;
        }

        public Order getSummaryOrder()
        {
            var order = getSessionOrder();
            order.orderItems = _orderRep.listAllBasket().Where(b => b.SessionKey == order.SessionKey).ToList();
            return order;
        }

        public Order getCartOrder(bool reset = false)
        {
            var order = getSessionOrder();
            if (order.ID != 0 && reset)
            {
                resetBasketItems(order);
            }
            order.orderItems = _orderRep.listAllBasket().Where(b => b.SessionKey == order.SessionKey).ToList();
            loadItemedOrder(order, true);
            return order;
        }

        public Order getCartOrderWithNewShippingInputs(ShippingInputs inputs)
        {
            var order = getSessionOrder();
            if (order.ID != 0)
            {
                resetBasketItems(order);
            }
            else
            {
                order.OrderDate = DateTime.Now;
                order.Code = Guid.NewGuid();
            }
            order.CountryID = inputs.CountryID;
            if (order.StateID != inputs.StateID)
            {
                inputs.Postcode = null;
            }
            order.StateID = inputs.StateID;
            order.Postcode = inputs.Postcode;
            order.ShippingMethodID = inputs.ShippingMethodID;
            _orderRep.saveOrder(order);

            order.orderItems = _orderRep.listAllBasket().Where(b => b.SessionKey == order.SessionKey).ToList();
            loadItemedOrder(order,true);
            return order;
        }

        public Order getFullOrderWithNewShippingInputs(int OrderID, ShippingInputs inputs)
        {
            var order = getFullOrder(OrderID);
            order.CountryID = inputs.CountryID;
            if (order.StateID != inputs.StateID)
            {
                inputs.Postcode = null;
            }
            order.StateID = inputs.StateID;
            order.Postcode = inputs.Postcode;
            order.ShippingMethodID = inputs.ShippingMethodID;
            order.AdminAdHocFreightTotal = inputs.AAFT;
            _orderRep.saveOrder(order);

            order.orderItems = _orderRep.listAllBasket().Where(b => b.SessionKey == order.SessionKey).ToList();
            loadItemedOrder(order, true);
            return order;
        }

        public Order finaliseOrder(Order order)
        {
            if (order.ID == 0)
            {
                order.OrderDate = DateTime.Now;
                order.Code = Guid.NewGuid();
                _orderRep.saveOrder(order);
            };

            try
            {
                order.AgentData = string.Format("{0}<br/> Raw {1}", getBrowserInfo(HttpContext.Current.Request.Browser), HttpContext.Current.Request.UserAgent);
            } catch { };

            setBasketItems(order);

            loadItemedOrder(order,true);
            _orderRep.saveOrder(order);

            if (order.orderItems.Count == 0)
                return order;

            if (order.voucher != null)
                PromotionService.associateVoucher(order);
            PromotionService.associateOrRemovePromotion(order,true);
            return order;
        }

        public Order getFullOrder(Guid code, bool runNewState = false)
        {
            var order = _orderRep.listAllOrders().Single(o => o.Code == code);
            loadOrderWithFullDomain(order, runNewState);
            return order;
        }

        public Order getFullOrder(int ID, bool runNewState = false)
        {
            var order = _orderRep.listAllOrders().Single(o => o.ID == ID);
            loadOrderWithFullDomain(order, runNewState);
            return order;
        }
     
        public void loadOrderWithFullDomain(Order order, bool runNewState = false)
        {
            order.orderItems = _orderRep.listAllBasket().Where(b => b.OrderID == order.ID).ToList();
            order.Details = _orderRep.listAllAccountDetails().Where(d => d.OrderID == order.ID).ToList();

            if (order.StatusID.HasValue)
                order.Status = _orderRep.listAllStatus().Single(x => x.ID == order.StatusID);

                loadItemedOrder(order, runNewState);
        }
        public static string getCustomerCode(int? AccountID)
        {
            string prefix = "MEG";

            if (!ModelTransformations.IsLive())
                if (AccountID.HasValue)
                {
                    if (ConfigurationManager.AppSettings["IsLocalHost"] != null && ConfigurationManager.AppSettings["IsLocalHost"] == "true")

                        prefix = "MEG";

                    if (!ModelTransformations.IsLive())
                    {
                        prefix = "MEGDEVLOCAL";
                    }
                    else
                    {
                        prefix = "MEGDEV";
                        if (ConfigurationManager.AppSettings["IsLocalHost"] != null && ConfigurationManager.AppSettings["IsLocalHost"] == "true")
                        {
                            prefix = "MEGDEVLOCAL";
                        }
                        else
                        {
                            prefix = "MEGDEV";
                        }
                    }
                    return prefix + String.Format("{0:0000}", AccountID);
                }

            return prefix + String.Format("{0:0000}", AccountID);


        }
        public void loadOrderWithAccountDetails(Order order)
        {
            order.Details = _orderRep.listAllAccountDetails().Where(d => d.OrderID == order.ID).ToList();

            if (order.StatusID.HasValue)
                order.Status = _orderRep.listAllStatus().Single(x => x.ID == order.StatusID);
           
        }
        public static decimal removeGST(decimal value)
        {
            decimal gst = (value * (decimal)3);
            gst = gst / (decimal)23;
            gst = Math.Round(gst,2);
            return value - gst;
        }
        public void completeSuccessfulOrder(Order order, PaymentResultModel result)
        {
            if (order.voucher != null)
            {
                PromotionService.usePromotion(order);
            }


            OrderPayment possibledupe = _orderRep.listAllOrderPayments().Where(x => x.PaymentReference == result.authcode).FirstOrDefault();

            if(possibledupe==null)
            { 
                OrderPayment op = new OrderPayment();
                op.Type = OrderPayment.TypeOption.PAYMENT_SERVICE;
                op.Service = (PaymentServiceStore.PaymentTypeOption)order.PaymentType;
                op.OrderID = order.ID;
                op.AccountDetailID = order.CustomerDetails.ID;
                if (result.fullResponse.Length > 5000)
                {
                    op.FullResponse = result.fullResponse.Substring(0, 4990);
                }
                else
                {

                    op.FullResponse = result.fullResponse;
                }

                op.PaymentDate = System.DateTime.Now;
                op.ResponseText = result.type + " payment";
                op.PaymentStatus = 1;
                op.PaymentCode = result.responseCode;
                op.PaymentReference = result.authcode;
                _orderRep.saveOrderPayment(op);

                order.orderItems.Where(x => x.Variant.stockNo.HasValue).ToList().ForEach(x =>
                {
                    var saveVariant = _productRep.listAllVariants().Single(y => y.ID == x.Variant.ID);
                    if (x.Quantity >= saveVariant.stockNo)
                    {
                        saveVariant.isHidden = true;
                        saveVariant.stockNo = 0;
                        saveVariant.modifiedDate = DateTime.Now;
                        if (saveVariant.preOrderShipDate.HasValue)
                            saveVariant.preOrderShipDate = null;
                        TagService.RunForHome();
                    }
                    else
                    {
                        saveVariant.stockNo -= x.Quantity;
                    }
                    _productRep.saveVariant(saveVariant);
                });

                order.OrderDate = DateTime.Now;
                string CustomerCode = "PUBLIC";
                if(order.AccountID!=null)
                {
                    CustomerCode = getCustomerCode(order.AccountID);

                }

                order = getFullOrder(order.Code); 
                if (order.SystemStatus == Order.SystemStatusOption.IN_PROGRESS)
                {
                    ///send it to propella.           
                    string return_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
                    ///return_xml += "<PropellaDocument><POrder><Header><CustomerCode>PUBLIC</CustomerCode>";
                    return_xml += "<PropellaDocument><POrder><Header>";
                    return_xml += "<Customer>";
                    ////return_xml += "<CustomerCode>PUBLIC</CustomerCode>";
                    return_xml += "<CustomerCode>" + CustomerCode + "</CustomerCode>";
                    return_xml += "<DeliveryAddress>";
                    return_xml += "<DeliverySalutation></DeliverySalutation>";
                    return_xml += "<DeliveryFirstName>" + order.CustomerDetails.FirstName + "</DeliveryFirstName>";
                    return_xml += "<DeliverySecondName>" + order.CustomerDetails.Surname + "</DeliverySecondName>";
                    return_xml += "<DeliveryName>" + order.CustomerDetails.FirstName + " " + order.CustomerDetails.Surname + "</DeliveryName>";
                    return_xml += "<DeliveryAddr1>" + order.DeliveryDetails.Address1  + "</DeliveryAddr1>";
                    return_xml += "<DeliveryAddr2>" + order.DeliveryDetails.Address2 + "</DeliveryAddr2>";
                    return_xml += "<DeliveryAddr3>" + order.DeliveryDetails.Address3 + "</DeliveryAddr3>";
                    return_xml += "<DeliveryCity>" + order.DeliveryDetails.Address4 + "</DeliveryCity>";
                    return_xml += "<DeliveryCountry>" + order.DeliveryDetails.Country + "</DeliveryCountry>";
                    return_xml += "<DeliveryPostCode>" + order.DeliveryDetails.Postcode  + "</DeliveryPostCode>";
                    return_xml += "</DeliveryAddress>";
                    
                    /*
                    return_xml += "<MailingAddress>";
                    return_xml += "<MailingSalutation/>";
                    return_xml += "<MailingFirstName/>";
                    return_xml += "<MailingSecondName/>";
                    return_xml += "<MailingName>" + order.CustomerDetails.FirstName + " " + order.CustomerDetails.Surname + "</MailingName>";
                    return_xml += "<MailingAddr1>" + order.CustomerDetails.Address1 + "</MailingAddr1>";
                    return_xml += "<MailingAddr2>" + order.CustomerDetails.Address2 + "</MailingAddr2>";
                    return_xml += "<MailingAddr3>" + order.CustomerDetails.Address3 + "</MailingAddr3>";
                    return_xml += "<MailingCity>" + order.CustomerDetails.Address4 + "</MailingCity>";
                    return_xml += "<MailingCountry>" + order.CustomerDetails.Country + "</MailingCountry>";
                    return_xml += "<MailingPostCode>" + order.CustomerDetails.Postcode + "</MailingPostCode>";
                    return_xml += "</MailingAddress>";
                    */

                    return_xml += "<Phone>" + order.CustomerDetails.Phone +"</Phone>";
                    return_xml += "<Fax/>";
                    return_xml += "<Email>" + order.CustomerDetails.Email + "</Email>";
                    return_xml += "<Mobile>" + order.CustomerDetails.Mobile + "</Mobile>";
                    return_xml += "<Contact>"+ order.CustomerDetails.FirstName  +"</Contact>";
                    return_xml += "</Customer>";

                    return_xml += "<CustomerOrder>MGR-" + order.ID + "</CustomerOrder><OrderDate>" + System.DateTime.Now.UtcToLocal().ToString("dd-MMM-yyyy") + "</OrderDate><PaymentType>" + op.Type.ToString() + " </PaymentType>";
                    return_xml += "<HeaderNote></HeaderNote><LoyaltyCardNo></LoyaltyCardNo><InternalNote /></Header><Detail>";
                    int lineno = 1;
                    foreach (Basket xb in order.orderItems)
                    {
                        return_xml += "<Lines>";
                        return_xml += "<LineNo>" + lineno + "</LineNo><SupplierProductCode>" + EscapeXml(xb.Variant.sku) + "</SupplierProductCode><Description>" + EscapeXml(xb.Variant.name) + "</Description><Quantity>" + xb.Quantity + "</Quantity>";
                        return_xml += "<UnitCost>" + (xb.Variant.noTax ? xb.Price : removeGST(xb.Price)) + "</UnitCost><Discount></Discount><RebateRate></RebateRate>";
                        return_xml += "</Lines>";
                        lineno++;
                    }

                    return_xml += "</Detail><Trailer><TailNOte /></Trailer></POrder></PropellaDocument>";



                    string[] inputdata = new string[] { "orderxml=" + return_xml, "Customer=" + CustomerCode };
                    var propella_gpi_wrs = PropellaService.GetGPIWebServiceData(new PropellaService.WebServiceDataRequest() { Args = inputdata, Function = "Submit_Order" });
                    ///var propella_wsr = PropellaService.GetWebServiceData(new PropellaService.WebServiceDataRequest() { Args = inputdata, Function = "Submit_Order" });

                    ///propella_wsr = propella_ws.Submit_Order(inputdata);


                    if (order.PaymentType == PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT)
                        changeOrderStatus(order, _orderRep.listAllStatus().First(s => s.SetSystemStatus == Order.SystemStatusOption.AWAITING_PAYMENT), null, null, result);
                    else if (order.PaymentType == PaymentServiceStore.PaymentTypeOption.FREE)
                        changeOrderStatus(order, _orderRep.listAllStatus().First(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER), null, null, result);
                    else
                        changeOrderStatus(order, _orderRep.listAllStatus().First(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER), null, null, result);

                }
            }
            createSession();
        }

        public static string EscapeXml(string s)
        {
            string xml = s;
            if (!string.IsNullOrEmpty(xml))
            {
                // replace literal values with entities
                xml = xml.Replace("&", "&amp;");
                xml = xml.Replace("&lt;", "&lt;");
                xml = xml.Replace("&gt;", "&gt;");
                xml = xml.Replace("\"", "&quot;");
                xml = xml.Replace("'", "&apos;");
            }
            return xml;
        }

        public void saveFailedOrder(Order order, PaymentResultModel result)
        {
            OrderPayment op = new OrderPayment();
            op.OrderID = order.ID;
            op.AccountDetailID = order.CustomerDetails.ID;
            op.FullResponse = (result.fullResponse.Length > 4800 ? result.fullResponse.Substring(0,4800) : result.fullResponse);
            op.PaymentDate = System.DateTime.Now;
            op.ResponseText = "Failed Payment";
            op.PaymentStatus = 0;
            op.PaymentCode = result.responseCode;
            op.PaymentReference = result.authcode ?? "n/a";
            _orderRep.saveOrderPayment(op);
        }

        public bool isCompletePromotionZeroOrder(Order order)
        {
            return order.OrderTotal == 0 && order.voucher != null;
        }

        public void completePromotionZeroOrder(Order order)
        {
            var op = PromotionService.usePromotion(order);
            order.OrderDate = DateTime.Now;
            changeOrderStatus(order, _orderRep.listAllStatus().Single(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER), null, null, null);
            createSession();
        }

        public Basket updateBasket(Basket changebasket, bool isAdmin)
        {
            var existingBasketItem = _orderRep.listAllBasket().Single(b => b.ID == changebasket.ID);
            if (existingBasketItem.Product.specialType.HasValue)
                throw new Exception("Tried to update a system product");
                

            if (changebasket.Quantity > 0)
            {
                var newTotal = changebasket.Quantity;
                int? oldtotal = existingBasketItem.Quantity;
                existingBasketItem.Quantity = newTotal;

                if (existingBasketItem.Variant.minQuantity.HasValue && newTotal < existingBasketItem.Variant.minQuantity)
                {
                    existingBasketItem.Quantity = (int)existingBasketItem.Variant.minQuantity;
                    existingBasketItem.Message += "A miniumum order quantity of " + existingBasketItem.Variant.minQuantity + " item(s) can be added for this product. ";
                }

                if (existingBasketItem.Variant.maxQuantity.HasValue && newTotal > existingBasketItem.Variant.maxQuantity)
                {
                    existingBasketItem.Quantity = (int)existingBasketItem.Variant.maxQuantity;
                    existingBasketItem.Message += "A maximum order quantity of " + existingBasketItem.Variant.maxQuantity + " item(s) can be added for this product. ";
                }

                if (existingBasketItem.Variant.stockNo.HasValue && newTotal > existingBasketItem.Variant.stockNo)
                {
                    existingBasketItem.Quantity = (int)existingBasketItem.Variant.stockNo;
                    existingBasketItem.Message += "Only " + existingBasketItem.Variant.stockNo + " item(s) of stock are available for this product. ";
                }

                existingBasketItem.Product = _productRep.listAllProducts(false, true).Single(x => x.ID == existingBasketItem.Variant.ProductID);
                if (existingBasketItem.Price == 0)
                {
                    existingBasketItem.Message += "Free items quantity can not be changed.";
                    changebasket.Quantity = 1;
                    changebasket.Price = 0;
                }
                else
                { 
                    setItemPrice(existingBasketItem, true);
                }
            }
            else
            {
                _orderRep.deleteBasket(existingBasketItem);
            }

            

            evaluateForSpecialProducts(changebasket.OrderID, changebasket.SessionKey, isAdmin);
            return existingBasketItem;
        }

        public Basket addBasket(Basket basket, string ip, bool isAdmin = false)
        {
            /*
            if (ShippingService.isBlockedIP(ip))
                return null;
            */
            basket.CreateDate = DateTime.Now;
            basket.Variant = _productRep.listAllVariants().Single(x => x.ID == basket.VariantID);
            basket.Product = _productRep.listAllProducts(false,true).Single(x => x.ID == basket.Variant.ProductID);
            
            if (basket.Product.isHidden)
                return null;

            if (!isAdmin && !basket.Product.CanBeOrdered) {
                TagService.RunForHome();
                return null;
            }

            Basket existingBasketItem = null;
            if (!isAdmin)
            { 
                basket.SessionKey = getSession();
                existingBasketItem = _orderRep.listAllBasket().FirstOrDefault(b => b.SessionKey == getSession() && b.VariantID == basket.VariantID);
            }
            else
            {
                var order = _orderRep.listAllOrders().Single(x => x.ID == basket.OrderID);
                basket.SessionKey = order.SessionKey;
                existingBasketItem = _orderRep.listAllBasket().FirstOrDefault(b => b.OrderID==order.ID && b.VariantID == basket.VariantID);
            }
            
            var saveBasket = existingBasketItem ?? basket;
            saveBasket.Product = basket.Product;

            var newTotal = existingBasketItem == null ? basket.Quantity : (existingBasketItem.Quantity + basket.Quantity);
            int? oldtotal = existingBasketItem == null ? (int?)null : existingBasketItem.Quantity;
            saveBasket.Quantity = newTotal;

            if(basket.Variant.minQuantity.HasValue && newTotal < basket.Variant.minQuantity)
            {
                saveBasket.Quantity = (int)basket.Variant.minQuantity;
                basket.Message += "A miniumum order quantity of " + basket.Variant.minQuantity + " item(s) can be added for this product. ";
            }

            if(basket.Variant.maxQuantity.HasValue && newTotal > basket.Variant.maxQuantity)
            {
                saveBasket.Quantity = (int)basket.Variant.maxQuantity;
                basket.Message += "A maximum order quantity of " + basket.Variant.maxQuantity + " item(s) can be added for this product. ";
            }

            if (basket.Variant.stockNo.HasValue && newTotal > basket.Variant.stockNo)
            {
                saveBasket.Quantity = (int)basket.Variant.stockNo;
                basket.Message += "Only " + basket.Variant.stockNo + " item(s) of stock are available for this product. ";
            }

            if (oldtotal.HasValue)
                basket.Quantity = saveBasket.Quantity - (int)oldtotal;

            setItemPrice(saveBasket, true);
            string upsell = null;
            if (saveBasket.Variant.bulkPrices.Any())
            {
                getItemVolUpsell(saveBasket);
                upsell = saveBasket.VolumeSuggestion;
            }

            loadOrderItem(basket, null, true);
            if (existingBasketItem != null)
            {
                var addedq = basket.Quantity;
                basket.Quantity = saveBasket.Quantity;
                setItemPrice(basket, false);
                basket.Quantity = addedq;
            }

            basket.VolumeSuggestion = upsell;

            evaluateForSpecialProducts(basket.OrderID, basket.SessionKey, isAdmin);

            return basket;
        }

        public Basket adminFixBasket(Basket basket, decimal fixedPrice)
        {
            basket.Price = fixedPrice;
            basket.AdminFixed = true;
            _orderRep.saveBasket(basket);

            return basket;
        }

        public void loadOrderItem(Basket item, Order order, bool runNewState)
        {
            if (item.Variant == null)
                item.Variant = _productRep.listAllVariants().Single(x => x.ID == item.VariantID);
            if (item.Product == null)
                item.Product = _productRep.listAllProducts().FirstOrDefault(x => x.ID == item.Variant.ProductID);

            item.Product.siteResources = _srRep.listAll(item.Product).Where(p => p.SiteResourceType == SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE).ToArray();
            SiteResourceService.loadUrls(item.Product.siteResources);
            item.Product.tags = _contentRep.listAllTagsForProduct(item.Product).ToList();

            if(item.Variant.bulkPrices.Any())
            {
                getItemVolUpsell(item);
            }

            if (runNewState)
                setItemPrice(item, false);
                
            if (order != null)
            { 
                order.BasketTotal += item.LinePrice;
                order.Weight += item.LineWeight;
                if (!item.Variant.noTax)
                    order.taxedItemTotal += item.LinePrice;
            }
        }

        public void loadItemedOrder(Order order, bool runNewState)
        {
            order.Weight = 0;
            order.BasketTotal = 0;
            order.taxedItemTotal = 0;

            order.orderItems.ForEach(x => loadOrderItem(x, order, false));

            if (runNewState)
            {
                order.shippingComponent = ShippingService.getShippingComponent(order,this);
                if (order.shippingComponent != null)
                {
                    order.FreightTotal = order.AdminAdHocFreightTotal ?? order.shippingComponent.amount;
                    order.LocationID = order.shippingComponent.location != null ? order.shippingComponent.location.ID : (int?)null;
                    order.ShippingMethodID = order.shippingMethod.ID;
                }
                else
                {
                    order.ShippingMethodID = null;
                    order.LocationID = null;
                }
            }
            else
            { 
                ShippingService.populateLists(order);
                ShippingService.populateShippingDomain(order);
            }

            order.promotion = PromotionService.checkForPromotion(order, runNewState);
            if (runNewState) PromotionService.associateOrRemovePromotion(order, false);
            order.voucher = PromotionService.getSessionOrOrderVoucher(order);
            

            if (order.orderItems.Any() && (order.promotion != null || order.voucher != null))
            {
                PromotionService.totalOrder(order, order.shippingComponent);
            }
            else
            {
                order.totalAfterPromotion = order.BasketTotal;
                order.totalAfterVoucher = order.BasketTotal;
                order.OrderTotal = order.BasketTotal + order.FreightTotal;
            }

            if (order.AmexSurcharge.HasValue)
            {
                order.TotalBeforeSurcharge = order.OrderTotal;
                order.OrderTotal += (decimal)order.AmexSurcharge;
            }
            order.incGst = ((order.taxedItemTotal + order.FreightTotal + (order.AmexSurcharge ?? 0)) * 3) / 23;
            ///order.incGst = (order.taxedItemTotal + order.FreightTotal + (order.AmexSurcharge ?? 0)) / (decimal)11;

            order.BasketTotal = Math.Round(order.BasketTotal, 2);
            order.FreightTotal = Math.Round(order.FreightTotal, 2);
            order.TaxTotal = Math.Round(order.incGst, 2);
            order.OrderTotal = Math.Round(order.OrderTotal, 2);

            
            if (order.SystemStatus == Order.SystemStatusOption.IN_PROGRESS && !order.PaymentType.HasValue && order.voucher != null && PromotionService.checkSessionOrOrderVoucher(order))
                loadItemedOrder(order,runNewState);
        }

        public void resetAmexSurcharge(Order order)
        {
            if (order.AmexSurcharge.HasValue)
            {
                setAmexSurcharge(order, false);
            }
        }

        public void setAmexSurcharge(Order order, bool isAmex)
        {
            order.TotalBeforeSurcharge = null;
            order.AmexSurcharge = null;
            loadItemedOrder(order, false);
            if(isAmex)
            {
                order.AmexSurcharge = order.OrderTotal * AMEX_SURCHARGE_PERCENTAGE;
            }
            loadItemedOrder(order, false);
            _orderRep.saveOrder(order);
        }

        public static decimal getTotalIfAmex(Order order)
        {
            if(order.TotalBeforeSurcharge.HasValue)
                return order.OrderTotal;
            return order.OrderTotal + (order.OrderTotal * AMEX_SURCHARGE_PERCENTAGE);
        }

        public void setItemPrice(Basket item, bool save)
        {
            if (!item.AdminFixed)
            {
                var oldItemPrice = item.Price;
                if (item.Variant.bulkPrices.Any() && item.Quantity >= item.Variant.bulkPrices.OrderBy(p => p.quantity).First().quantity)
                {
                    foreach (var price in item.Variant.bulkPrices.OrderByDescending(p => p.quantity))
                    {
                        if (item.Quantity >= price.quantity)
                        {
                            item.Price = price.amount;
                            break;
                        }
                    }
                }
                else
                {
                    item.Price = item.Product.getBuyPrice();
                }
            }
            if (save)
                _orderRep.saveBasket(item);
        }

        public void evaluateForSpecialProducts(int? OrderID, string sessionKey, bool isAdmin)
        {
            var basket = _orderRep.listAllBasket().Where(b => (!isAdmin && b.SessionKey == getSession()) || (isAdmin && b.OrderID == OrderID)).ToList();
           
            if(basket.Any(x => x.Product.markertags.Any(y => y.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE)) && !basket.Any(x => x.Product.specialType  == Product.SpecialTypeOption.WEIGHT_ONLY_ICE_COLD_PACK))
            {
                //Add special ice pack product
                var specialProd = _productRep.listAllProducts(false,true).SingleOrDefault(x => x.specialType == Product.SpecialTypeOption.WEIGHT_ONLY_ICE_COLD_PACK);
                if(specialProd == null)
                {
                    specialProd = new Product()
                    {
                        name = "Cold Pack for Heat Sensitive item(s)",
                        specialType = Product.SpecialTypeOption.WEIGHT_ONLY_ICE_COLD_PACK,
                        createdDate = DateTime.Now,
                        modifiedDate = DateTime.Now,
                        isHidden = true,
                        variants = new List<Variant>()
                    };
                    _productRep.saveProduct(specialProd, false, false, false);
                    var specialProdVariant = new Variant()
                    {
                        ProductID = specialProd.ID,
                        name = "Cold Pack for Heat Sensitive item(s)",
                        price = 0,
                        priceRRP = 0,
                        weight = 0.4m,
                        createDate = DateTime.Now,
                        modifiedDate = DateTime.Now,
                        isHidden = false,
                        bulkPrices = new List<Price>()
                    };
                    _productRep.saveVariant(specialProdVariant);
                    specialProd.variants.Add(specialProdVariant);
                }
                setItemPrice(new Basket()
                {
                    VariantID = specialProd.variant.ID,
                    Variant = specialProd.variant,
                    OrderID = isAdmin ? OrderID : (int?)null,
                    SessionKey = isAdmin ? sessionKey : getSession(),
                    Quantity = 1,
                    CreateDate = DateTime.Now
                }, true);
            }
            else
            {
                if (!basket.Any(x => x.Product.markertags.Any(y => y.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE)) && basket.Any(x => x.Product.specialType == Product.SpecialTypeOption.WEIGHT_ONLY_ICE_COLD_PACK))
                {
                    //Remove special ice pack product
                    _orderRep.deleteBasket(basket.Single(x => x.Product.specialType == Product.SpecialTypeOption.WEIGHT_ONLY_ICE_COLD_PACK));
                }
            }
        }

        public void getItemVolUpsell(Basket item)
        {
            var nextPrice = item.Variant.bulkPrices.Where(x => x.quantity > item.Quantity).OrderBy(x => x.quantity).FirstOrDefault();
            if(nextPrice != null)
            {
                var nowP = 100 * (item.Variant.priceRRP - item.Price)/item.Variant.priceRRP;
                var nextP = 100 * (item.Variant.priceRRP - nextPrice.amount) / item.Variant.priceRRP;
                item.VolumeSuggestion = string.Format("Buy {0} more for an additional {1}% discount off this product.", (nextPrice.quantity - item.Quantity), (nextP - nowP).ToString("0.0"));

                switch (item.Variant.bulkPrices.OrderBy(x => x.quantity).ToList().FindIndex(x => x.ID == nextPrice.ID))
                {
                    case 0:
                        item.VolumeSuggestionColour = new string[] { "#e3edd9", "#458500" };
                        break;
                    case 1:
                        item.VolumeSuggestionColour = new string[] { "#fde8ed", "#ef446d" };
                        break;
                    default:
                        item.VolumeSuggestionColour = new string[] { "#fff8e6", "#ca9102" };
                        break;
                }
            }
        }

        private void setBasketItems(Order order)
        {
            var items = _orderRep.listAllBasket().Where(b => b.SessionKey == order.SessionKey).ToList();

            foreach (Basket b in items)
            {
                b.OrderID = order.ID;
                _orderRep.saveBasket(b);
            }

            order.orderItems = items;
        }

        private void resetBasketItems(Order order)
        {
            if (order.SystemStatus > Order.SystemStatusOption.IN_PROGRESS)
                return;
            var items = _orderRep.listAllBasket().Where(b => b.OrderID == order.ID).ToList();

            foreach (Basket b in items)
            {
                Variant pv = b.Variant;
                b.OrderID = null;
                _orderRep.saveBasket(b);
            }

            order.orderItems = items;
        }

        public void clearBasketItems(Order order)
        {
            if(order.SystemStatus > Order.SystemStatusOption.IN_PROGRESS)
                return;

            _orderRep.listAllBasket().Where(x => x.OrderID == order.ID || x.SessionKey == order.SessionKey).ToList().ForEach(x => _orderRep.deleteBasket(x));
            order.orderItems = new List<Basket>();
        }

        public bool checkForBasketReset(Order order)
        {
            var orderItems = _orderRep.listAllBasket().Where(b => b.OrderID == order.ID).ToList();
            if (orderItems.Count > 0)
                return false;
            decimal Subtotal = 0;
            var basketItems = _orderRep.listAllBasket().Where(b => b.SessionKey == order.SessionKey).ToList();
            foreach (var item in basketItems)
            {
                item.Price = item.Product.getBuyPrice() * item.Quantity;
                Subtotal += item.Price;
            }
            setBasketItems(order);
            if (Subtotal == order.BasketTotal)
            {
                return false;
            }
            return true;
        }

        public MailMessage changeOrderStatusAndGetEmail(Order order, Status status, string message, Signature signature, PaymentResultModel result)
        {
            if (status.SetSystemStatus.HasValue)
            {
                order.SystemStatus = (Order.SystemStatusOption)status.SetSystemStatus;
            }
            order.StatusID = status.ID;
            order.Status = status;

            order.addNewNote(string.Format("Status updated to {0} at {1}.{2}",
                status.Name,
                DateTime.UtcNow.UtcToLocal(),
                !string.IsNullOrEmpty(message) ? string.Format(" Message:{0}", message) : ""
                ), true);
            _orderRep.saveOrder(order);


            if (status.isNotifyCustomer || status.isNotifyStaff)
            {
                return sendStatusChangeEmail(order, status, message, signature, result, false);
            } 
            else
            {
                return null;
            }
                

        }
        public void changeOrderStatus(Order order, Status status, string message, Signature signature, PaymentResultModel result)
        {
            if(status.SetSystemStatus.HasValue)
            { 
                order.SystemStatus = (Order.SystemStatusOption)status.SetSystemStatus;
            }
            order.StatusID = status.ID;
            order.Status = status;

            order.addNewNote(string.Format("Status updated to {0} at {1}.{2}",
                status.Name,
                DateTime.UtcNow.UtcToLocal(),
                !string.IsNullOrEmpty(message) ? string.Format(" Message:{0}",message) : "" 
                ), true);
            _orderRep.saveOrder(order);

            
            if (status.isNotifyCustomer || status.isNotifyStaff)
                sendStatusChangeEmail(order, status, message, signature, result);
           
        }


        public MailMessage sendStatusChangeEmail(Order order, Status status, string message, Signature signature, PaymentResultModel result, bool actualsend=true)
        {
       
                /*
                if (!string.IsNullOrEmpty(message))
                    message = message.Replace("\n", "<br/>");
                    */
                return _controller.sendViewEmail(
                ConfigurationManager.AppSettings["ShopEmail"],
                status.isNotifyCustomer ? order.CustomerDetails.Email : ConfigurationManager.AppSettings["ShopEmail"],
                status.isNotifyStaff && status.isNotifyCustomer ? ConfigurationManager.AppSettings["ShopBCC"] : null,
                string.Format("Your Order {0} from {1} {2}", order.OrderNo, ConfigurationManager.AppSettings["SiteCompanyName"], status.EmailSubject),
                status.EmailHeader,
                "~/Views/Shop/CompleteEmail.cshtml",
                new PaymentCompleteViewModel() { order = order, result = result },
                message,
                signature, 
                actualsend
                );
        }

        public void tagExistingStateToLog(Order order, string reason)
        {
            if (order.SystemStatus == Order.SystemStatusOption.IN_PROGRESS)
                return;

            string stateFormat = "Order {0}\nItems\n{11}\nBasket Total:{2}\nWeight:{7}\nTax Total:{4}\nPromotion:{5}\nVoucher:{6}\nDelivery Country:{9}\nState:{10}\nShipping Method:{8}\nFreight Total:{3}\nTotal:{1}";

            string items = "";
            order.orderItems.ForEach(x =>
            {
                items += string.Format("{0}:{1} quanity:{2} each:{3} total:{4}\n", x.Product.name, x.Product.sku, x.Quantity, x.Price.ToString("c"), x.LinePrice.ToString("c"));
            });

            ShippingService.populateShippingDomain(order);
            string orderState = string.Format(stateFormat,
                    order.OrderNo,
                    order.OrderTotal.ToString("c"),
                    order.BasketTotal.ToString("c"),
                    order.FreightTotal.ToString("c"),
                    order.TaxTotal.ToString("c"),
                    order.promotion != null ? order.promotion.AdminDescription : "n/a",
                    order.voucher != null ? order.voucher.AdminDescription : "n/a",
                    order.Weight,
                    order.shippingMethod != null ? order.shippingMethod.Name : "n/a",
                    order.country != null ? order.country.Name : "n/a",
                    order.state != null ? order.state.Name : "n/a",
                    items
                );

            order.addNewNote(reason + "\n" + orderState,false);

            if (!order.FreightOriginalAmount.HasValue)
                order.FreightOriginalAmount = order.FreightTotal;

            _orderRep.saveOrder(order);
        }

        private string getBrowserInfo(HttpBrowserCapabilities browser)
        {
            string s = "<b>Browser Capabilities</b><br/>"
                + "Type = " + browser.Type + "<br/>"
                + "Name = " + browser.Browser + "<br/>"
                + "Version = " + browser.Version + "<br/>"
                + "Major Version = " + browser.MajorVersion + "<br/>"
                + "Minor Version = " + browser.MinorVersion + "<br/>"
                + "Platform = " + browser.Platform + "<br/>"
                + "Is Beta = " + browser.Beta + "<br/>"
                + "Is Crawler = " + browser.Crawler + "<br/>"
                + "Is AOL = " + browser.AOL + "<br/>"
                + "Is Win16 = " + browser.Win16 + "<br/>"
                + "Is Win32 = " + browser.Win32 + "<br/>"
                + "Supports Frames = " + browser.Frames + "<br/>"
                + "Supports Tables = " + browser.Tables + "<br/>"
                + "Supports Cookies = " + browser.Cookies + "<br/>"
                + "Supports VBScript = " + browser.VBScript + "<br/>"
                + "Supports JavaScript = " +
                    browser.EcmaScriptVersion.ToString() + "<br/>"
                + "Supports Java Applets = " + browser.JavaApplets + "<br/>"
                + "Supports ActiveX Controls = " + browser.ActiveXControls
                      + "<br/>"
                + "Supports JavaScript Version = " +
                    browser["JavaScriptVersion"] + "<br/>" +
                    (browser.IsMobileDevice ? "Recognised as mobile/tablet device" : "Desktop") + "<br/>" +
                    "ScreenPixelsWidth:" + browser.ScreenPixelsWidth + " ScreenPixelsHeight" + browser.ScreenPixelsHeight
                    ;

            return s;
        }
    }

    public class AddToCartResult
    {
        public Basket basket { get; set; }
        public string message { get; set; }
    }
}