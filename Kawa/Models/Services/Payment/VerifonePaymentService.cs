﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.ViewModels;
using RestSharp;
using RestSharp.Authenticators;
using System.Configuration;
using Kawa.Models.Services;
using Kawa.Models.Services.Payment;
using Kawa.Models;
using Kawa.Models.Extensions;
using PaymentExpress.PxPay;
using Newtonsoft.Json;
using PayPal.Api;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Net.Http;
using System.Web.Mvc;

namespace Kawa.Models.Services.Payment
{
    public class VerifonePaymentService : IPaymentService
    {
        private OrderRepository _orderRep;
        private ShippingRepository _shipRep;
        private RestClient _client;

        string PxPayUserId = ConfigurationManager.AppSettings["PaymentExpress.PxPay_User"];
        string PxPayKey = ConfigurationManager.AppSettings["PaymentExpress.PxPay_Pass"];

        public RestClient client
        {
            get
            {
                if (_client == null)
                {
                    _client = new RestClient(ConfigurationManager.AppSettings["eWayEndpoint"]);
                    _client.Authenticator = new HttpBasicAuthenticator(ConfigurationManager.AppSettings["eWayApiKey"], ConfigurationManager.AppSettings["eWayApiPassword"]);
                }
                return _client;
            }
        }

        public bool IsAdminPayment { get; set; }

        public VerifonePaymentService(OrderRepository orderRep, ShippingRepository shipRep)
        {
            _orderRep = orderRep;
            _shipRep = shipRep;
        }

        public PaymentChoiceViewModel getPaymentChoiceModel()
        {
            return new PaymentChoiceViewModel()
            {
                name = "Credit Card",
                description = "Use our credit card service windcave",
                type = PaymentServiceStore.PaymentTypeOption.VERIFONE
            };
        }

        public class VerifoneConfig
        {
            public int id { get; set; }
            public string name { get; set; }
            public string entity_id { get; set; }
            public string payment_contract_id { get; set; }
            public string threeds_contract_id { get; set; }
        }

        public static VerifoneConfig getVerifoneConfig()
        {
            /*
             * dev
            return new VerifoneConfig()
            {
                entity_id = "0bf7a94b-2a8c-4ad6-9bd4-2e7acf8d0894",
                payment_contract_id = "edb20640-5655-419b-a78b-79afd78a392f",
                threeds_contract_id = "feee5ab0-53da-4c1e-af39-bd120fab58f2",
                id = 1,
                name = "Smits Group Site"
            };
            */

            return new VerifoneConfig()
            {
                entity_id = "9383eced-2cdf-4061-a6d6-1004cb2d46e3",
                payment_contract_id = "6609caad-22ce-4eb6-bec7-42f363c29359",
                threeds_contract_id = "6960cdf2-7f98-4ac8-b3d3-eba07a02b96f",
                id = 1,
                name = "GPI Automotive"
            };

        }

        public static string GetVerifoneUser()
        {

            return ConfigurationManager.AppSettings["Verifone_userid"];
        }

        public static string GetVerifoneKey()
        {
            return ConfigurationManager.AppSettings["Verifone_apikey"];
        }



        public IPaymentViewModel getPaymentModel(Account account, Order order)
        {


            dynamic verifone = new JObject();

            if(account == null)
            {
                account = new Account();
                account.firstName = order.CustomerDetails.FirstName;
                account.lastName = order.CustomerDetails.Surname;
                account.UserName = order.CustomerDetails.Email;
            }

            ////test card no  = 512345000000000008/5123456789012346
            verifone.entity_id = getVerifoneConfig().entity_id;/// "b24515ee-96fd-4076-be05-80937e472664";///this is the smitsgroup site in the admin.
            verifone.return_url = ConfigurationManager.AppSettings["SiteUrl"] + "/Shop/Complete/" + order.Code;
            verifone.shop_url = ConfigurationManager.AppSettings["SiteUrl"] + "/Shop";
            ///capturecard_request.interaction_type = "HPP";
            ///capturecard_request.sales_channel = "ECOMMERCE";
            verifone.currency_code = "NZD";
            verifone.amount = (int)(order.OrderTotal * 100); ///setup.Amount;
            verifone.merchant_reference = order.ID.ToString();
            verifone.redirect_method = "HEADER_REDIRECT";

            verifone.configurations = new JObject();
            verifone.configurations.card = new JObject();
            verifone.configurations.card.mode = "3DS_PAYMENT";
            verifone.configurations.card.capture_now = true;
            verifone.configurations.card.payment_contract_id = getVerifoneConfig().payment_contract_id;//// "4848aa2f-880d-4c21-994f-7ea66d7452d4";/// "4848aa2f-880d-4c21-994f-7ea66d7452d4";

            verifone.configurations.card.threed_secure = new JObject();
            verifone.configurations.card.threed_secure.threeds_contract_id = getVerifoneConfig().threeds_contract_id;//// "cf3e46b1-2512-4d78-9b0c-05371be14cbb";
            verifone.configurations.card.threed_secure.enabled = true;

            /* save card not used with meguiars.
            if (setup.SaveCard)
            {
                verifone.configurations.card.token_preference = new JObject();
                verifone.configurations.card.token_preference.token_scope = "7388c594-b2d9-4e60-8105-8d1d90ff811e";////test one?? "02bf55f9-5afd-4cff-94dc-da8970e7aa8d";

                verifone.configurations.card.stored_credential = new JObject();
                verifone.configurations.card.stored_credential.stored_credential_type = "SIGNUP";
                verifone.configurations.card.stored_credential.processing_model_details = new JObject();
                verifone.configurations.card.stored_credential.processing_model_details.processing_model = "UNSCHEDULED_CREDENTIAL_ON_FILE";
            }
            */

            verifone.customer_details = new JObject();
            verifone.customer_details.work_phone = "0";
            verifone.customer_details.company_name = account.firstName;
            verifone.customer_details.company_registration_number = "0";
            verifone.customer_details.email_address = account.UserName;
            verifone.customer_details.entity_id = getVerifoneConfig().entity_id;/// "b24515ee-96fd-4076-be05-80937e472664"; ///this is the smitsgroup site in the admin.      

            verifone.customer_details.billing = new JObject();
            verifone.customer_details.billing.address_1 = "string";
            verifone.customer_details.billing.first_name = account.firstName;/// "string";
            verifone.customer_details.billing.last_name = account.lastName;///"string";
            verifone.customer_details.billing.country_code = "NZ";
            verifone.customer_details.billing.city = "NA";
            verifone.customer_details.shipping = new JObject();
            verifone.customer_details.shipping.address_1 = "string";
            verifone.customer_details.shipping.first_name = "string";
            verifone.customer_details.shipping.last_name = "string";

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(verifone);

            string auth = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(GetVerifoneUser() + ":" + GetVerifoneKey()));
            string getURL = ConfigurationManager.AppSettings["Verifone_API"].ToString() + "checkout-service/v2/checkout";
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, getURL); ////https://cst.test-gsc.vfims.com/oidc/
            request.Headers.Add("Authorization", auth);
            request.Content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            var client = new HttpClient();

            var response = client.SendAsync(request).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            dynamic responseObj = JObject.Parse(responseString);
            responseObj.input = verifone;
            responseObj.input_url = getURL;

            VerifoneViewModel pcvm = new VerifoneViewModel();

            order.PaymentType = PaymentServiceStore.PaymentTypeOption.VERIFONE;
            _orderRep.saveOrder(order);
            if (responseObj.url != null)
            {
                pcvm.link = responseObj.url.ToString();
                return pcvm;
            }
            else
            {
                pcvm.Error = "There was an issue starting your payment, please try again.";
                return pcvm;
            }
          


        }

        public string getSavedCreditCard(string token)
        {
            var requestcarddetails = new RestRequest("Customer/" + token, Method.GET);
            requestcarddetails.RequestFormat = DataFormat.Json;
            requestcarddetails.AddHeader("header", "Content-Type: application/json");
            IRestResponse<EWayResponseSavedCard> responsecarddetails = client.Execute<EWayResponseSavedCard>(requestcarddetails);
            return responsecarddetails.Data.Customers.First().CardDetails.Number;
        }

        public PaymentResultModel getResult(object postObject, Order order)
        {

            string auth = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(GetVerifoneUser() + ":" + GetVerifoneKey()));
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, ConfigurationManager.AppSettings["Verifone_API"].ToString() + "checkout-service/v2/checkout/" + postObject.ToString());
            request.Headers.Add("Authorization", auth);
            var client = new HttpClient();

            var response = client.SendAsync(request).Result;
            var responseString = response.Content.ReadAsStringAsync().Result;
            dynamic responseObj = JObject.Parse(responseString);

            if (responseObj != null)
            {
                try { 
                bool successfulpayment = (responseObj.events.Last.type.Value == "TRANSACTION_SUCCESS" ? true : false);
                return new PaymentResultModel()
                {
                    type = PaymentServiceStore.PaymentTypeOption.VERIFONE,
                    responseCode = responseObj.events.Last.type.Value,
                    authcode = responseObj.transaction_id,
                    success = successfulpayment,
                    fullResponse = JsonConvert.SerializeObject(responseString),
                    injectHeader = _orderRep.listAllStatus().OrderBy(x => x.ID).First(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER).EmailHeader
                };
                }
                catch(Exception ex)
                {
                    return new PaymentResultModel()
                    {
                        type = PaymentServiceStore.PaymentTypeOption.VERIFONE,
                        responseCode = "Cancelled Transaction",
                        authcode = responseObj.transaction_id + "-invalid",
                        success = false,
                        fullResponse = JsonConvert.SerializeObject(responseString),
                        injectHeader = _orderRep.listAllStatus().OrderBy(x => x.ID).First(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER).EmailHeader
                    };
                }
            }
            else
            {
                return new PaymentResultModel()
                {
                    type = PaymentServiceStore.PaymentTypeOption.VERIFONE,
                    responseCode = responseObj.events.Last.type.Value,/// "There was issue with your payment, please try again....",
                    authcode = "",
                    fullResponse = JsonConvert.SerializeObject(responseString),
                    injectHeader = _orderRep.listAllStatus().OrderBy(x => x.ID).First(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER).EmailHeader
                };
            }
        }

        private Object getwindcaveRequest(bool getToken, int ewayamount, string returnUri, string token, Order order)
        {
            return new
            {

                Customer = new
                {
                    FirstName = order.CustomerDetails.FirstName,
                    LastName = order.CustomerDetails.Surname,
                    Street1 = order.CustomerDetails.Address1,
                    Street2 = order.CustomerDetails.Address2,
                    City = order.CustomerDetails.Address3,
                    State = order.CustomerDetails.Address4,
                    PostalCode = order.CustomerDetails.Postcode,
                    Country = _shipRep.listAllCountries().Where(a => a.ID == order.CustomerDetails.CountryID).FirstOrDefault().Code.ToLower(),
                    Phone = order.CustomerDetails.Mobile,
                    Email = order.CustomerDetails.Email,
                    TokenCustomerID = token
                },

                Payment = new
                {
                    TotalAmount = ewayamount,
                    InvoiceNumber = order.OrderNoToBe,
                    InvoiceReference = order.ID,
                    InvoiceDescription = "Meguiars product(s)",
                    CurrencyCode = "AUD"
                },

                RedirectUrl = returnUri + (IsAdminPayment ? "/admin/orders/order" : "/shop/") + "complete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.EWAY,
                CancelUrl = returnUri + (IsAdminPayment ? "/admin/orders/order" : "/shop/") + "incomplete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.EWAY,

                @Method = getToken ? "TokenPayment" : "ProcessPayment",
                TransactionType = "Purchase"

            };
        }
    }


    public class VerifoneResponse
    {
        public bool valid { get; set; }
        public bool success { get; set; }

        public string TxnId { get; set; }
        public string TxnType { get; set; }
        public string AmountSettlement { get; set; }
        public string DpsTxnRef { get; set; }
        public string ResponseText { get; set; }

        public string DpsBillingId { get; set; }


    }

    public class VerifoneResponseShared
    {

        public string SharedPaymentUrl { get; set; }
        public string AccessCode { get; set; }

    }

    public class VerifoneResponseSavedCard
    {

        public List<EWayCustomer> Customers { get; set; }

    }

    public class VerifoneCustomer
    {
        public VerifoneCustomer()
        {

        }

        public long? TokenCustomerID { get; set; }
        public string Reference { get; set; }
        public string Email { get; set; }
        public EWayCardDetails CardDetails { get; set; }
    }

    public class VerifoneCardDetails
    {
        public VerifoneCardDetails()
        {

        }

        public string Number { get; set; }
        public string Name { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
    }

    public class VerifoneTransactionResult
    {
        public string AccessCode { get; set; }
        public string AuthorisationCode { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceReference { get; set; }
        public int TotalAmount { get; set; }
        public int TransactionID { get; set; }
        public bool TransactionStatus { get; set; }
        public long? TokenCustomerID { get; set; }
        public int BeagleScore { get; set; }
        /*
        "Options": [{
            "Value": "Option1"
        }, {
            "Value": "Option2"
        }],
         
        "Verification": {
            "CVN": 0,
            "Address": 0,
            "Email": 0,
            "Mobile": 0,
            "Phone": 0
        },
        "BeagleVerification": {
            "Email": 0,
            "Phone": 0
        },
        
        "Errors": null
         *  * */
    }


}

namespace Kawa.Models.ViewModels
{
    public class VerifoneViewModel : IPaymentViewModel
    {
        public bool hasView
        {
            get { return false; }
        }

        public string viewName
        {
            get { return "_WindCave"; }
        }

        public string processingText
        {
            get { return "Please do not click away from this screen while your card is being processed."; }
        }

        public string instructionText
        {
            get { return "Enter Credit Card Details"; }
        }

        public string link
        {
            get;
            set;
        }

        public string Error { get; set; }
        public EWayResponse Response { get; set; }
        public EWayResponse ResponseToken { get; set; }
        public string SavedCard { get; set; }


        public CreditCardViewModel cc { get; set; }
        public Account account { get; set; }
    }
}