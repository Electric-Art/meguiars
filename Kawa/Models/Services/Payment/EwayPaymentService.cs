﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.ViewModels;
using RestSharp;
using RestSharp.Authenticators;
using System.Configuration;
using Kawa.Models.Services;
using Kawa.Models.Services.Payment;
using Kawa.Models;
using Kawa.Models.Extensions;

namespace Kawa.Models.Services.Payment
{
    public class EwayPaymentService : IPaymentService
    {
        private OrderRepository _orderRep;
        private ShippingRepository _shipRep;
        private RestClient _client;

        public RestClient client
        {
            get
            {
                if (_client == null)
                {
                    _client = new RestClient(ConfigurationManager.AppSettings["eWayEndpoint"]);
                    _client.Authenticator = new HttpBasicAuthenticator(ConfigurationManager.AppSettings["eWayApiKey"], ConfigurationManager.AppSettings["eWayApiPassword"]);
                }
                return _client;
            }
        }

        public bool IsAdminPayment { get; set; }

        public EwayPaymentService(OrderRepository orderRep, ShippingRepository shipRep)
        {
            _orderRep = orderRep;
            _shipRep = shipRep;
        }

        public PaymentChoiceViewModel getPaymentChoiceModel()
        {
            return new PaymentChoiceViewModel()
            {
                name = "Credit Card",
                description = "Use our credit card service Eway.",
                type = PaymentServiceStore.PaymentTypeOption.EWAY
            };
        }

        public IPaymentViewModel getPaymentModel(Account account, Order order)
        {
            EwayViewModel vm = new EwayViewModel()
            {
                cc = new CreditCardViewModel(),
                account = account
            };

            int ewayamount = int.Parse(order.OrderTotal.ToString("0.00").Replace(".", ""));

            if (ConfigurationManager.AppSettings["IsStaging"] != null && ConfigurationManager.AppSettings["IsStaging"] == "true")
                ewayamount = int.Parse(decimal.Round(order.OrderTotal).ToString("0.00").Replace(".", ""));

            string token = account != null && account.creditCardToken != null ? account.creditCardToken : null;

            if (token != null)
            {
                vm.ewaySavedCard = getSavedCreditCard(token);
            }

            var request = new RestRequest("AccessCodes/", Method.POST);

            var data = getEwayRequest(account != null, ewayamount, SiteService.getDomainBase(), "", order);

            // easily add HTTP Headers
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("header", "Content-Type: application/json");
            request.AddBody(data);

            IRestResponse<EWayResponse> response2 = client.Execute<EWayResponse>(request);
            vm.ewayResponse = response2.Data;

            if (token != null)
            {
                int ewaytokenamount = !vm.ewaySavedCardIsAmex() ? ewayamount : int.Parse(OrderService.getTotalIfAmex(order).ToString("0.00").Replace(".", ""));

                if (ConfigurationManager.AppSettings["IsStaging"] != null && ConfigurationManager.AppSettings["IsStaging"] == "true")
                    ewaytokenamount = !vm.ewaySavedCardIsAmex() ? ewayamount : int.Parse(decimal.Round(OrderService.getTotalIfAmex(order)).ToString("0.00").Replace(".", ""));

                var requesttok = new RestRequest("AccessCodes/", Method.POST);
                var datatok = getEwayRequest(account != null, ewaytokenamount, SiteService.getDomainBase(), token, order);
                requesttok.RequestFormat = DataFormat.Json;
                requesttok.AddHeader("header", "Content-Type: application/json");
                requesttok.AddBody(datatok);

                IRestResponse<EWayResponse> response3 = client.Execute<EWayResponse>(requesttok);
                vm.ewayResponseToken = response3.Data;
                vm.ewayResponseToken.TokenCustomerID = long.Parse(token);
            }

            order.PaymentType = PaymentServiceStore.PaymentTypeOption.EWAY;
            _orderRep.saveOrder(order);

            return vm;

            #region Moto
            /*
             * MOTO CALL
             * 
            else
            {
                var request = new RestRequest("Transaction/", Method.POST);
                var data = new
                {

                    Customer = new
                    {
                        TokenCustomerID = token
                    },

                    Payment = new
                    {
                        TotalAmount = ewayamount,
                        InvoiceNumber = vm.order.OrderNo,
                        InvoiceDescription = "Kawa Natural Health Ltd product(s)",
                        CurrencyCode = "AUD"
                    },

                    @Method = "TokenPayment",
                    TransactionType = "MOTO"

                };

                request.RequestFormat = DataFormat.Json;
                request.AddHeader("header", "Content-Type: application/json");
                request.AddBody(data);

                IRestResponse<EWayResponse> response2 = client.Execute<EWayResponse>(request);

                return RedirectToAction("Complete", new { AccessCode = response2.Data.AccessCode });
                
            }
             */
            #endregion
        }

        public string getSavedCreditCard(string token)
        {
            var requestcarddetails = new RestRequest("Customer/" + token, Method.GET);
            requestcarddetails.RequestFormat = DataFormat.Json;
            requestcarddetails.AddHeader("header", "Content-Type: application/json");
            IRestResponse<EWayResponseSavedCard> responsecarddetails = client.Execute<EWayResponseSavedCard>(requestcarddetails);
            return responsecarddetails.Data.Customers.First().CardDetails.Number;
        }

        public PaymentResultModel getResult(object postObject, Order order)
        {
            var AccessCode = (string)postObject;
            var request = new RestRequest("AccessCode/" + AccessCode, Method.GET);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("header", "Content-Type: application/json");
            IRestResponse<EWayTransactionResult> response = client.Execute<EWayTransactionResult>(request);

            var result = new PaymentResultModel()
            {
                type = PaymentServiceStore.PaymentTypeOption.EWAY,
                responseCode = response.Data.ResponseCode,
                authcode = response.Data.AuthorisationCode,
                fullResponse = response.Content,
                injectHeader = _orderRep.listAllStatus().OrderBy(x => x.ID).First(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER).EmailHeader
            };

            result.success = result.responseCode == "00" || result.responseCode == "08" || result.responseCode == "10" || result.responseCode == "11" || result.responseCode == "16";

            if (response.Data.TokenCustomerID.HasValue)
                result.token = response.Data.TokenCustomerID.ToString();

            return result;
        }

        private Object getEwayRequest(bool getToken, int ewayamount, string returnUri, string token, Order order)
        {
            return new
            {

                Customer = new
                {
                    FirstName = order.CustomerDetails.FirstName,
                    LastName = order.CustomerDetails.Surname,
                    Street1 = order.CustomerDetails.Address1,
                    Street2 = order.CustomerDetails.Address2,
                    City = order.CustomerDetails.Address3,
                    State = order.CustomerDetails.Address4,
                    PostalCode = order.CustomerDetails.Postcode,
                    Country = _shipRep.listAllCountries().Where(a=>a.ID == order.CustomerDetails.CountryID).FirstOrDefault().Code.ToLower(),
                    Phone = order.CustomerDetails.Mobile,
                    Email = order.CustomerDetails.Email,
                    TokenCustomerID = token
                },

                Payment = new
                {
                    TotalAmount = ewayamount,
                    InvoiceNumber = order.OrderNoToBe,
                    InvoiceReference = order.ID,
                    InvoiceDescription = "Meguiars product(s)",
                    CurrencyCode = "AUD"
                },

                RedirectUrl = returnUri + (IsAdminPayment ? "/admin/orders/order" : "/shop/") + "complete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.EWAY,
                CancelUrl = returnUri + (IsAdminPayment ? "/admin/orders/order" : "/shop/") + "incomplete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.EWAY,

                @Method = getToken ? "TokenPayment" : "ProcessPayment",
                TransactionType = "Purchase"

            };
        }
    }

    public class EWayResponse
    {
        public string FormActionURL { get; set; }
        public string AccessCode { get; set; }
        public string CardNumber { get; set; }
        public string CardExpiryMonth { get; set; }
        public string CardExpiryYear { get; set; }

        public long? TokenCustomerID { get; set; }
    }

    public class EWayResponseShared
    {

        public string SharedPaymentUrl { get; set; }
        public string AccessCode { get; set; }

    }

    public class EWayResponseSavedCard
    {

        public List<EWayCustomer> Customers { get; set; }

    }

    public class EWayCustomer
    {
        public EWayCustomer()
        {

        }

        public long? TokenCustomerID { get; set; }
        public string Reference { get; set; }
        public string Email { get; set; }
        public EWayCardDetails CardDetails { get; set; }
    }

    public class EWayCardDetails
    {
        public EWayCardDetails()
        {

        }

        public string Number { get; set; }
        public string Name { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
    }

    public class EWayTransactionResult
    {
        public string AccessCode { get; set; }
        public string AuthorisationCode { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceReference { get; set; }
        public int TotalAmount { get; set; }
        public int TransactionID { get; set; }
        public bool TransactionStatus { get; set; }
        public long? TokenCustomerID { get; set; }
        public int BeagleScore { get; set; }
        /*
        "Options": [{
            "Value": "Option1"
        }, {
            "Value": "Option2"
        }],
         
        "Verification": {
            "CVN": 0,
            "Address": 0,
            "Email": 0,
            "Mobile": 0,
            "Phone": 0
        },
        "BeagleVerification": {
            "Email": 0,
            "Phone": 0
        },
        
        "Errors": null
         *  * */
    }

    
}

namespace Kawa.Models.ViewModels
{
    public class EwayViewModel : IPaymentViewModel
    {
        public bool hasView
        {
            get { return true; }
        }

        public string viewName
        {
            get { return "_Payment_Eway"; }
        }

        public string processingText
        {
            get { return "Please do not click away from this screen while your card is being processed."; }
        }

        public string instructionText
        {
            get { return "Enter Credit Card Details"; }
        }

        public string link
        {
            get;
            set;
        }

        public EWayResponse ewayResponse { get; set; }
        public EWayResponse ewayResponseToken { get; set; }
        public string ewaySavedCard { get; set; }
        public bool ewaySavedCardIsAmex()
        {
            return isAmexSurchargeOn() && ( ewaySavedCard.StartsWith("34") || ewaySavedCard.StartsWith("37") );
        }

        public bool isAmexSurchargeOn()
        {
            return !(DateTime.UtcNow.UtcToLocal() >= new DateTime(2017, 8, 1, 0, 0, 0));
        }

        public CreditCardViewModel cc { get; set; }
        public Account account { get; set; }
    }
}