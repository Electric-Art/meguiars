﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.ViewModels;
using RestSharp;
using RestSharp.Authenticators;
using System.Configuration;
using Kawa.Models.Services;
using Kawa.Models.Services.Payment;
using Kawa.Models;
using Kawa.Models.Extensions;
using PaymentExpress.PxPay;
using Newtonsoft.Json;

namespace Kawa.Models.Services.Payment
{
    public class WindCavePaymentService : IPaymentService
    {
        private OrderRepository _orderRep;
        private ShippingRepository _shipRep;
        private RestClient _client;

        string PxPayUserId = ConfigurationManager.AppSettings["PaymentExpress.PxPay_User"];
        string PxPayKey = ConfigurationManager.AppSettings["PaymentExpress.PxPay_Pass"];

        public RestClient client
        {
            get
            {
                if (_client == null)
                {
                    _client = new RestClient(ConfigurationManager.AppSettings["eWayEndpoint"]);
                    _client.Authenticator = new HttpBasicAuthenticator(ConfigurationManager.AppSettings["eWayApiKey"], ConfigurationManager.AppSettings["eWayApiPassword"]);
                }
                return _client;
            }
        }

        public bool IsAdminPayment { get; set; }

        public WindCavePaymentService(OrderRepository orderRep, ShippingRepository shipRep)
        {
            _orderRep = orderRep;
            _shipRep = shipRep;
        }

        public PaymentChoiceViewModel getPaymentChoiceModel()
        {
            return new PaymentChoiceViewModel()
            {
                name = "Credit Card",
                description = "Use our credit card service windcave",
                type = PaymentServiceStore.PaymentTypeOption.WINDCAVE
            };
        }

        public IPaymentViewModel getPaymentModel(Account account, Order order)
        {

            ///int order_total = int.Parse(order.OrderTotal.ToString("0.00"));

            PxPay WS = new PxPay(PxPayUserId, PxPayKey);

            RequestInput input = new RequestInput();

            input.AmountInput = order.OrderTotal.ToString();
            input.CurrencyInput = "NZD";
            input.MerchantReference = order.ID.ToString();
            input.EnableAddBillCard = "0";

            /*
            if (Request.Form["CARDCVN"] != null && Request.Form["CARDCVN"] != "")
            {
                if (!UserCustomer.isGuest)
                    input.DpsBillingId = UserCustomer.CreditCardToken.Split(',')[0];
            }
            if (Request.Form["SAVECARD"] != null)
                input.EnableAddBillCard = "1";
            */

            input.TxnType = "Purchase";
            input.UrlFail = ConfigurationManager.AppSettings["SiteUrl"] + "/Shop/PaymentIssue/" + order.Code;
            input.UrlSuccess = ConfigurationManager.AppSettings["SiteUrl"] + "/Shop/Complete/" + order.Code;

            Guid txnGUID = Guid.NewGuid();
            input.TxnId = txnGUID.ToString().Substring(0, 16);

            WindCaveViewModel pcvm = new WindCaveViewModel();

            order.PaymentType = PaymentServiceStore.PaymentTypeOption.WINDCAVE;
            _orderRep.saveOrder(order);


            RequestOutput output = WS.GenerateRequest(input);
            try
            {
                if (output.valid == "1")
                {
                    // Redirect user to payment page
                    pcvm.link = output.Url;
                    return pcvm;
                }
            }
            catch { }

            pcvm.windcaveError = output.URI;
            return pcvm;

        }

        public string getSavedCreditCard(string token)
        {
            var requestcarddetails = new RestRequest("Customer/" + token, Method.GET);
            requestcarddetails.RequestFormat = DataFormat.Json;
            requestcarddetails.AddHeader("header", "Content-Type: application/json");
            IRestResponse<EWayResponseSavedCard> responsecarddetails = client.Execute<EWayResponseSavedCard>(requestcarddetails);
            return responsecarddetails.Data.Customers.First().CardDetails.Number;
        }

        public PaymentResultModel getResult(object postObject, Order order)
        {

            PxPay WS = new PxPay(PxPayUserId, PxPayKey);
            ResponseOutput response = WS.ProcessResponse(postObject.ToString());

            var result = new PaymentResultModel()
            {
                type = PaymentServiceStore.PaymentTypeOption.WINDCAVE,
                responseCode = response.Success,
                authcode = response.DpsTxnRef,
                fullResponse = JsonConvert.SerializeObject(response),
                injectHeader = _orderRep.listAllStatus().OrderBy(x => x.ID).First(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER).EmailHeader
            };

            result.success = response.Success == "1" ? true : false;

            /* if (response.Data.TokenCustomerID.HasValue)
                result.token = response.Data.TokenCustomerID.ToString();
            */

            return result;
        }

        private Object getwindcaveRequest(bool getToken, int ewayamount, string returnUri, string token, Order order)
        {
            return new
            {

                Customer = new
                {
                    FirstName = order.CustomerDetails.FirstName,
                    LastName = order.CustomerDetails.Surname,
                    Street1 = order.CustomerDetails.Address1,
                    Street2 = order.CustomerDetails.Address2,
                    City = order.CustomerDetails.Address3,
                    State = order.CustomerDetails.Address4,
                    PostalCode = order.CustomerDetails.Postcode,
                    Country = _shipRep.listAllCountries().Where(a => a.ID == order.CustomerDetails.CountryID).FirstOrDefault().Code.ToLower(),
                    Phone = order.CustomerDetails.Mobile,
                    Email = order.CustomerDetails.Email,
                    TokenCustomerID = token
                },

                Payment = new
                {
                    TotalAmount = ewayamount,
                    InvoiceNumber = order.OrderNoToBe,
                    InvoiceReference = order.ID,
                    InvoiceDescription = "Meguiars product(s)",
                    CurrencyCode = "AUD"
                },

                RedirectUrl = returnUri + (IsAdminPayment ? "/admin/orders/order" : "/shop/") + "complete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.EWAY,
                CancelUrl = returnUri + (IsAdminPayment ? "/admin/orders/order" : "/shop/") + "incomplete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.EWAY,

                @Method = getToken ? "TokenPayment" : "ProcessPayment",
                TransactionType = "Purchase"

            };
        }
    }


    public class windcaveResponse
    {
        public bool valid { get; set; }
        public bool success { get; set; }

        public string TxnId { get; set; }
        public string TxnType { get; set; }
        public string AmountSettlement { get; set; }
        public string DpsTxnRef { get; set; }
        public string ResponseText { get; set; }

        public string DpsBillingId { get; set; }


    }

    public class windcaveResponseShared
    {

        public string SharedPaymentUrl { get; set; }
        public string AccessCode { get; set; }

    }

    public class windcaveResponseSavedCard
    {

        public List<EWayCustomer> Customers { get; set; }

    }

    public class windcaveCustomer
    {
        public windcaveCustomer()
        {

        }

        public long? TokenCustomerID { get; set; }
        public string Reference { get; set; }
        public string Email { get; set; }
        public EWayCardDetails CardDetails { get; set; }
    }

    public class windcaveCardDetails
    {
        public windcaveCardDetails()
        {

        }

        public string Number { get; set; }
        public string Name { get; set; }
        public string ExpiryMonth { get; set; }
        public string ExpiryYear { get; set; }
    }

    public class windcaveTransactionResult
    {
        public string AccessCode { get; set; }
        public string AuthorisationCode { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceReference { get; set; }
        public int TotalAmount { get; set; }
        public int TransactionID { get; set; }
        public bool TransactionStatus { get; set; }
        public long? TokenCustomerID { get; set; }
        public int BeagleScore { get; set; }
        /*
        "Options": [{
            "Value": "Option1"
        }, {
            "Value": "Option2"
        }],
         
        "Verification": {
            "CVN": 0,
            "Address": 0,
            "Email": 0,
            "Mobile": 0,
            "Phone": 0
        },
        "BeagleVerification": {
            "Email": 0,
            "Phone": 0
        },
        
        "Errors": null
         *  * */
    }


}

namespace Kawa.Models.ViewModels
{
    public class WindCaveViewModel : IPaymentViewModel
    {
        public bool hasView
        {
            get { return false; }
        }

        public string viewName
        {
            get { return "_WindCave"; }
        }

        public string processingText
        {
            get { return "Please do not click away from this screen while your card is being processed."; }
        }

        public string instructionText
        {
            get { return "Enter Credit Card Details"; }
        }

        public string link
        {
            get;
            set;
        }

        public string windcaveError { get; set; }
        public EWayResponse windcaveResponse { get; set; }
        public EWayResponse windcaveResponseToken { get; set; }
        public string windcaveSavedCard { get; set; }
        

        public CreditCardViewModel cc { get; set; }
        public Account account { get; set; }
    }
}