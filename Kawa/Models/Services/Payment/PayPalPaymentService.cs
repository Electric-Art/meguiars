﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.ViewModels;
using RestSharp;
using RestSharp.Authenticators;
using System.Configuration;
using Kawa.Models.Services;
using System.Web.Mvc;
using PayPal.Api;
using Kawa.Models.Services.Payment;


namespace Kawa.Models.Services.Payment
{
    public class PayPalPaymentService : IPaymentService
    {
        private OrderRepository _orderRep;
        private ShippingRepository _shipRep;
        private const string PAYPAL_REQUEST = "Paypal Payment Request";
        private const string PAYPAL_REQUEST_FAILED = "Failed Paypal Payment Request";

        public bool IsAdminPayment {get;set;}

        public PayPalPaymentService(OrderRepository orderRep, ShippingRepository shipRep)
        {
            _orderRep = orderRep;
            _shipRep = shipRep;
        }

        public IPaymentViewModel getPaymentModel(Account account, Order order)
        {
            order.PaymentType = PaymentServiceStore.PaymentTypeOption.PAYPAL;
            _orderRep.saveOrder(order);

            var apiContext = PaypalConfiguration.GetAPIContext();

            // ###Items
            // Items within a transaction.
            var itemList = new ItemList()
            {
                /*
                items = order.orderItems.Select(x =>
                            new Item()
                            {
                                name = x.Product.name,
                                currency = "AUD",
                                price = x.Price.ToString("0.00"),
                                quantity = x.Quantity.ToString(),
                                sku = x.Product.sku
                            }
                        ).ToList(),
                */
                shipping_address = new ShippingAddress
                {
                    line1 = order.DeliveryDetails.Address1 + (!string.IsNullOrEmpty(order.DeliveryDetails.Address2) ? (", " + order.DeliveryDetails.Address2) : string.Empty),
                    city = order.DeliveryDetails.Address3,
                    state = order.DeliveryDetails.Address4,
                    postal_code = order.DeliveryDetails.Postcode,
                    country_code = _shipRep.listAllCountries().Where(a => a.ID == order.DeliveryDetails.CountryID).FirstOrDefault().Code.ToUpper(),
                    recipient_name = order.DeliveryDetails.FirstName + " " + order.DeliveryDetails.Surname + (!string.IsNullOrEmpty(order.DeliveryDetails.Company) ? (" - " + order.DeliveryDetails.Company) : string.Empty),

                }
            };
            // ###Details
            // Let's you specify details of a payment amount.
            var details = new Details()
            {
                tax = "15",
                shipping = order.FreightTotal.ToString("0.00"),
                subtotal = order.BasketTotal.ToString("0.00")
            };
            details.tax = (order.OrderTotal - order.FreightTotal - order.BasketTotal).ToString("0.00");

            // ###Amount
            // Let's you specify a payment amount.
            var amount = new Amount()
            {
                currency = "AUD",
                total = order.OrderTotal.ToString("0.00"), // Total must be equal to sum of shipping, tax and subtotal.
                //details = details
            };

            // ###Payer
            // A resource representing a Payer that funds a payment
            // Payment Method
            // as `paypal`
            var payer = new Payer() { payment_method = "paypal" };

            // ###Redirect URLS
            // These URLs will determine how the user is redirected from PayPal once they have either approved or canceled the payment.
            var redirUrls = new RedirectUrls()
            {
                cancel_url = SiteService.getDomainBase() + "/shop/incomplete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.PAYPAL,
                return_url = SiteService.getDomainBase() + "/shop/complete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.PAYPAL,
            };


            // ###Transaction
            // A transaction defines the contract of a
            // payment - what is the payment for and who
            // is fulfilling it. 
            var transactionList = new List<Transaction>();

            // The Payment creation API requires a list of
            // Transaction; add the created `Transaction`
            // to a List
            transactionList.Add(new Transaction()
            {
                description = "Meguiars Shopping Cart",
                invoice_number = order.OrderNoToBe,
                amount = amount,
                item_list = itemList
            });

            // ###Payment
            // A Payment Resource; create one using
            // the above types and intent as `sale` or `authorize`
            var payment = new PayPal.Api.Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls,
                experience_profile_id = ConfigurationManager.AppSettings["PaypalExperienceProfileID"]
            };

            OrderPayment paymentRequest = null;
            // Create a payment using a valid APIContext
            try
            {
                var createdPayment = payment.Create(apiContext);
                paymentRequest = new OrderPayment()
                {
                    Type = OrderPayment.TypeOption.PAYMENT_SERVICE,
                    Service = PaymentServiceStore.PaymentTypeOption.PAYPAL,
                    OrderID = order.ID,
                    AccountDetailID = order.CustomerDetails.ID,
                    PaymentStatus = 0,
                    PaymentReference = createdPayment.id,
                    PaymentCode = createdPayment.state,
                    PaymentDate = DateTime.Now,
                    FullResponse = createdPayment.ConvertToJson(),
                    ResponseText = PAYPAL_REQUEST
                };
                _orderRep.saveOrderPayment(paymentRequest);

                var vm = new PaypalViewModel();

                // Using the `links` provided by the `createdPayment` object, we can give the user the option to redirect to PayPal to approve the payment.
                var links = createdPayment.links.GetEnumerator();
                while (links.MoveNext())
                {
                    var link = links.Current;
                    if (link.rel.ToLower().Trim().Equals("approval_url"))
                    {
                        //this.flow.RecordRedirectUrl("Redirect to PayPal to approve the payment...", link.href);
                        vm.link = link.href;
                    }
                }
                return vm;
            }
            catch (PayPal.PaymentsException e)
            {
                paymentRequest = new OrderPayment()
                {
                    Type = OrderPayment.TypeOption.PAYMENT_SERVICE,
                    Service = PaymentServiceStore.PaymentTypeOption.PAYPAL,
                    OrderID = order.ID,
                    AccountDetailID = order.CustomerDetails.ID,
                    PaymentStatus = 0,
                    PaymentReference = "",
                    PaymentCode = "",
                    PaymentDate = DateTime.Now,
                    FullResponse = e.Response,
                    ResponseText = PAYPAL_REQUEST_FAILED
                };
                _orderRep.saveOrderPayment(paymentRequest);
                return new PaypalViewModel()
                {
                    link = "/Shop/Incomplete/" + order.Code
                };
            }
        }

        public PaymentResultModel getResult(object postObject, Order order)
        {
            var apiContext = PaypalConfiguration.GetAPIContext();
            var laspaymentreq = _orderRep.listAllOrderPayments().Where(x => x.OrderID == order.ID && x.ResponseText == PAYPAL_REQUEST).OrderByDescending(x => x.ID).First();
            string payerId = HttpContext.Current.Request.Params["PayerID"];

            // Using the information from the redirect, setup the payment to execute.
            var paymentId = laspaymentreq.PaymentReference;
            var paymentExecution = new PaymentExecution() { payer_id = payerId };
            var payment = new PayPal.Api.Payment() { id = paymentId };

            PayPal.Api.Payment executedPayment = null;
            PaymentResultModel result = null;
            try { 
                executedPayment = payment.Execute(apiContext, paymentExecution);
                result = new PaymentResultModel()
                {
                    type = PaymentServiceStore.PaymentTypeOption.PAYPAL,
                    responseCode = executedPayment.state,
                    authcode = executedPayment.id,
                    fullResponse = executedPayment.ConvertToJson(),
                    injectHeader = _orderRep.listAllStatus().OrderBy(x => x.ID).First(s => s.SetSystemStatus == Order.SystemStatusOption.PAID_ORDER).EmailHeader,
                    success = executedPayment.state == "approved"
                };
                /*
                if(result.success)
                {
                    var deliveryDetails = order.DeliveryDetails;
                    var paypalDeliveryDetails = executedPayment.transactions[0].item_list.shipping_address;
                    deliveryDetails.Address1 = paypalDeliveryDetails.line1;
                    deliveryDetails.Address3 = paypalDeliveryDetails.city;
                    deliveryDetails.Postcode = paypalDeliveryDetails.postal_code;
                    
                    deliveryDetails.Address4 = paypalDeliveryDetails.state;
                    var country = _shipRep.listAllCountries().FirstOrDefault(a => a.Code == paypalDeliveryDetails.country_code);
                    if (country != null && deliveryDetails.CountryID != country.ID)
                        deliveryDetails.CountryID = country.ID;
                    
                    _orderRep.saveAccountDetail(deliveryDetails);
                }
                */
            }
            catch(Exception e)
            {
                result = new PaymentResultModel()
                {
                    responseCode = "Error from paypal",
                    authcode = "Success link clicked from Paypal",
                    fullResponse = e.InnerException.Message,
                    success = true
                };
            }
            
            return result;
        }

        public string getSavedCreditCard(string token)
        {
            return null;
        }

        public PaymentChoiceViewModel getPaymentChoiceModel()
        {
            return new PaymentChoiceViewModel()
            {
                name = "PayPal",
                description = "Pay wih Paypal.",
                type = PaymentServiceStore.PaymentTypeOption.PAYPAL,
                hasView = true,
                viewName = "_Payment_Paypal"
            };
        }
    }
}


namespace Kawa.Models.ViewModels
{
    public class PaypalViewModel : IPaymentViewModel
    {
        public bool hasView
        {
            get { return false; }
        }

        public string viewName
        {
            get { return "_Payment_Paypal"; }
        }

        public string processingText
        {
            get { return "Please wait while we redirect you to PayPal"; }
        }

        public string instructionText
        {
            get { return "Redirecting to PayPal"; }
        }

        public string link
        {
            get;
            set;
        }


        public Account account { get; set; }
    }
}


/* Notes

https://www.paypalobjects.com/en_US/vhelp/paypalmanager_help/credit_card_numbers.htm
Credit Card
Credit Card Number:
4866425750269288
Credit Card Type:
VISA
Expiration Date:
11/2019

*/