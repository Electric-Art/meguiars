﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.ViewModels;
using RestSharp;
using RestSharp.Authenticators;
using System.Configuration;
using Kawa.Models.Services;
using System.Web.Mvc;

namespace Kawa.Models.Services.Payment
{
    public class DirectDebitPaymentService : IPaymentService
    {
        private OrderRepository _orderRep;

        public bool IsAdminPayment { get; set; }

        public DirectDebitPaymentService(OrderRepository orderRep)
        {
            _orderRep = orderRep;
        }

        public PaymentChoiceViewModel getPaymentChoiceModel()
        {
            return new PaymentChoiceViewModel()
            {
                name = "Direct Deposit",
                description = "Pay wih by direct deposit.",
                type = PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT
            };
        }

        public IPaymentViewModel getPaymentModel(Account account, Order order)
        {
            order.PaymentType = PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT;
            _orderRep.saveOrder(order);

            return new DirectDebitViewModel()
            {
                link = "/Shop/Complete/" + order.Code + "?pt=" + PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT
            };
        }

        public PaymentResultModel getResult(object postObject, Order order) 
        {
            return new PaymentResultModel() { 
                type = PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT,
                success=true, 
                authcode = "",
                responseCode = "",
                fullResponse = "Paid by direct debit",
                //injectView = "_Payment_DirectDebitInject",  
                injectHeader = _orderRep.listAllStatus().First(s => s.SetSystemStatus == Order.SystemStatusOption.AWAITING_PAYMENT).EmailHeader
            }; 
        }
        public string getSavedCreditCard(string token) { return null; }
    }
}

namespace Kawa.Models.ViewModels
{
    public class DirectDebitViewModel : IPaymentViewModel
    {
        public bool hasView
        {
            get { return false; }
        }

        public string viewName
        {
            get { return null; }
        }

        public string processingText
        {
            get { return null; }
        }

        public string instructionText
        {
            get { return null; }
        }

        public string link
        {
            get;
            set;
        }

    }
}