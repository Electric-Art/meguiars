﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Linq;
using Kawa.Models.Data;

namespace Kawa.Models.Services
{
    /*
     * this thing grabs data via some views from the LMC site.
     */

    public class LMCDataService
    {


        public static List<LmcGallery> GetGallery(int id)
        {
            using (var _db = new dbDataContext())
            {
                return _db.LmcGalleries.Where(x => x.ID == id).OrderBy(x=>x.sid).ToList();
            }
        }
        public static LmcGallery GetGallery(GalleryTypeOption type, int id)
        {
            using (var _db = new dbDataContext())
            {
                return _db.LmcGalleries.SingleOrDefault(x => x.TypeE == (int)type && x.ID == id);
            }
        }

        public static List<Models.Data.LmcGallery> searchgalleries(string search)
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                _db.LoadOptions = options;
                List<Models.Data.LmcGallery> results = _db.LmcGalleries.Where(a => !a.IsHidden).Where(x => x.Name.Contains(search) || x.metakeywords.Contains(search)).ToList();
                return results.GroupBy(p => p.ID).Select(g => g.First()).ToList();
            }
        }

        public static List<LmcGallery> GetGalleries(GalleryTypeOption type)
        {
            using (var _db = new dbDataContext())
            {
                ///List<LmcGallery> data =_db.LmcGalleries.Where(x => x.TypeE == (int)type).OrderBy(x => x.ID).Select(s => new LmcGallery() { Name = s.Name, ID = s.ID }).ToList();
                List<LmcGallery> data = _db.LmcGalleries.Where(x => x.TypeE == (int)type && x.SiteResourceTypeE == 3).OrderBy(x => x.Rank).ThenBy(x=>x.SiteResourceTypeE).GroupBy(p => p.ID).Select(g => g.First()).ToList();
                return data.ToList();
            }
        }
        public static List<Models.Data.CalendarEvent> searchevents(string search)
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                _db.LoadOptions = options;
                List<Models.Data.CalendarEvent> results = _db.CalendarEvents.Where(a => !a.isHidden).Where(x => x.Name.Contains(search) || x.Intro.Contains(search) || x.Content.Contains(search)).ToList();
                return results;
            }
        }
        public static EventViewModel LoadEvents(DateTime from)
        {
            using (var _db = new dbDataContext())
            {

                return new EventViewModel()
                {
                    Header = new Page() { name = "Events" },
                    List = _db.CalendarEvents.Where(a => !a.isHidden && a.StartDate.HasValue)
                                        .Where(x => (DateTime)x.StartDate > from).OrderByDescending(x=>x.StartDate).ToList()
                };
            }
        }

        public static CalendarEvent LoadEvent(int id)
        {
            using (var _db = new dbDataContext())
            {
                return _db.CalendarEvents.SingleOrDefault(a => a.ID == id && !a.isHidden);
            }
        }

        public static List<Models.Data.New> searchnews(string search)
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                _db.LoadOptions = options;
                List<Models.Data.New> results = _db.News.Where(x=>x.SiteResourceTypeE == 3).Where(a => !a.IsHidden && a.NewsDate.HasValue).Where(x => x.Title.Contains(search) || x.Html.Contains(search) || x.metadescription.Contains(search) || x.metakeywords.Contains(search)).ToList();
                Data.Page px = _db.Pages.Where(x => x.Type == 9).FirstOrDefault();
                List<int> split = px.html.Split(',').ToList().Select(int.Parse).ToList();


                List<Models.Data.New> valid = results.Where(x => split.Contains(x.ID)).ToList();

                return valid;
            }
        }

        public static List<Models.Data.New> ListNews()
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                _db.LoadOptions = options;
                return _db.News.Where(a => !a.IsHidden && a.NewsDate.HasValue).ToList();
            }
        }
        public static NewsViewModel LoadNews()
        {
            using (var _db = new dbDataContext())
            {
                var options = new DataLoadOptions();
                _db.LoadOptions = options;

                return new NewsViewModel()
                {
                    Header = new Page(),                   
                    NewsList = _db.News.Where(a => !a.IsHidden && a.NewsDate.HasValue).ToList()
                };
            }
        }

        public static NewsViewModel LoadNews(int year, int month)
            {
                using (var _db = new dbDataContext())
                {
                    var options = new DataLoadOptions();
                    _db.LoadOptions = options;


                    Data.Page px = _db.Pages.Where(x => x.Type == 9).FirstOrDefault();
                    List<int> split = px.html.Split(',').ToList().Select(int.Parse).ToList();


                    List<Models.Data.New> valid = _db.News.Where(x => split.Contains(x.ID)).ToList();

                     NewsViewModel nvm = new NewsViewModel()
                    {
                        Header = new Page(),
                        RecentNews = valid.Where(a => !a.IsHidden && a.NewsDate.HasValue).Where(x=>x.SiteResourceTypeE ==3).OrderByDescending(x => (DateTime)x.NewsDate).Take(10).ToList(),
                        ArchiveOptions = valid.Where(a => !a.IsHidden && a.NewsDate.HasValue).ToList()
                                                    .Select(x => new DateTime(((DateTime)x.NewsDate).Year, ((DateTime)x.NewsDate).Month, 1))
                                                    .Distinct()
                                                    /*.Where(x => !(x.Month == month && x.Year == year))*/
                                                    .OrderByDescending(a => a).ToList(),
                        NewsList = valid.Where(a => !a.IsHidden && a.NewsDate.HasValue)
                                            .Where(x => (((DateTime)x.NewsDate).Month == month && ((DateTime)x.NewsDate).Year == year)).ToList()
                    };


                    return nvm;
                }
            }

            public static New LoadArticle(int id)
            {
                using (var _db = new dbDataContext())
                {
                    var options = new DataLoadOptions();
                    _db.LoadOptions = options;
                    return _db.News.FirstOrDefault(a => a.ID == id && !a.IsHidden);
                }
            }
        }
    }