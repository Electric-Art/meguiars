﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.ViewModels;
using RestSharp;
using RestSharp.Authenticators;
using System.Configuration;
using Kawa.Models.Services.Payment;

namespace Kawa.Models.Services
{
    public class PaymentServiceStore
    {
        public enum PaymentTypeOption
        {
            EWAY,
            PAYPAL,
            DIRECT_DEBIT,
            WINDCAVE,
            FREE,
            VERIFONE
        }
        private OrderRepository _orderRep;
        private ShippingRepository _shipRep;
        private Dictionary<PaymentTypeOption, IPaymentService> _store;

        public PaymentServiceStore(OrderRepository orderRep, ShippingRepository shipRep)
        {
            _orderRep = orderRep;
            _shipRep = shipRep;
        }
        
        public IPaymentService getService(PaymentTypeOption type)
        {
            if (_store == null) _store = new Dictionary<PaymentTypeOption,IPaymentService>();

            if (!_store.Keys.Any(k => k==type))
            {
                switch (type)
                {
                    case PaymentTypeOption.VERIFONE:
                        _store[type] = new VerifonePaymentService(_orderRep, _shipRep);
                        break;
                    case PaymentTypeOption.WINDCAVE:
                        _store[type] = new WindCavePaymentService(_orderRep, _shipRep);
                        break;
                    case PaymentTypeOption.EWAY :
                        _store[type] = new EwayPaymentService(_orderRep, _shipRep);
                        break;
                    case PaymentTypeOption.PAYPAL:
                        _store[type] = new PayPalPaymentService(_orderRep, _shipRep);
                        break;
                    case PaymentTypeOption.DIRECT_DEBIT:
                        _store[type] = new DirectDebitPaymentService(_orderRep);
                        break;
                }
            }
            return _store[type];
        }
    }
    
    public interface IPaymentService
    {
        PaymentChoiceViewModel getPaymentChoiceModel();
        IPaymentViewModel getPaymentModel(Account account, Order order);
        PaymentResultModel getResult(object postObject, Order order);
        string getSavedCreditCard(string token);

        bool IsAdminPayment {get;set;}
    }

    
}