﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.ViewModels;

namespace Kawa.Models.Services
{
    public class GoogeSiteMapService
    {
        private string sitemapdomain;

        public bool GenerateSiteMap(SiteContentCollection scc, string sitemapdomain, string savePath)
        {
            this.sitemapdomain = sitemapdomain;
            string mapheader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">\n";
            string node = "";

            foreach (Tag t in scc.tags)
            {
                node += siteMapAddTag(t);
            }

            foreach (Product p in scc.products)
            {
                string cnalt = null;
                node += buildnode(p.getUrl(), "0.5", cnalt);
            }

            string mapfooter = "</urlset>";
            string xmloutput = mapheader + node + mapfooter;

            System.IO.File.WriteAllText(savePath, xmloutput);

            return true;
        }

        public string siteMapAddTag(Tag tag)
        {
            string loc = tag.getUrl();
            string node = buildnode(loc, "0.5", tag.CnAlternative);

            if (tag.pages != null)
            {
                foreach (Page p in tag.pages)
                {
                    node += buildnode(p.getUrl(), "0.5");
                }
            }

            if (tag.subtags != null)
            {
                foreach (Tag t in tag.subtags)
                {
                    node += buildnode(t.getUrl(), "0.5", tag.CnAlternative);
                    node += siteMapAddTag(t);
                }
            }

            return node;
        }

        public string buildnode(string loc, string priority, string cnAlternative = null)
        {
            string node = string.Empty;
            string siteURL = sitemapdomain;

            if (loc == null)
                return node;

            node += "<url>\n<loc>" + siteURL + loc.Replace("&", "&amp;") + "</loc>\n";
            node += "<changefreq>daily</changefreq><priority>" + priority + "</priority>\n";
            if (cnAlternative != null) node += " <xhtml:link rel=\"alternate\" hreflang=\"zh-Hans\" href=\"" + siteURL + cnAlternative.Replace("&", "&amp;") + "\"/>";
            node += "<lastmod>" + System.DateTime.Now.ToString("yyyy-MM-dd") + "</lastmod>\n</url>\n";
            if (cnAlternative != null)
            {
                node += "<url>\n<loc>" + siteURL + cnAlternative.Replace("&", "&amp;") + "</loc>\n";
                node += "<changefreq>daily</changefreq><priority>" + priority + "</priority>\n";
                if (cnAlternative != null) node += " <xhtml:link rel=\"alternate\" hreflang=\"en\" href=\"" + siteURL + loc.Replace("&", "&amp;") + "\"/>";
                node += "<lastmod>" + System.DateTime.Now.ToString("yyyy-MM-dd") + "</lastmod>\n</url>\n";
            }
            return node;
        }
    }


}