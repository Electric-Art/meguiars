﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Configuration;
using Kawa.Models.Extensions;

namespace Kawa.Models.Services
{
    public class PromotionService
    {
        private static string SESSION_VOUCHER_NAME = "MeguiarsVoucher";

        private ProductRepository _productRep;
        private OrderRepository _orderRep;
        private ContentRepository _contentRep;
        private ShippingRepository _shippingRep;
        private ShippingService ShippingService;

        public PromotionService(ProductRepository productRep, OrderRepository orderRep, ContentRepository contentRep, ShippingRepository shippingRep, ShippingService shippingService)
        {
            _productRep = productRep;
            _orderRep = orderRep;
            _contentRep = contentRep;
            _shippingRep = shippingRep;
            ShippingService = shippingService;
        }

        public static decimal getBuyPrice(Variant v, Promotion p)
        {


                decimal rate = v.price;

                if (p != null)
                {
                        ///means a actual special price being applied.
                        if (p.percentOff != null)
                        {
                            decimal discountedpercent = ((decimal)p.percentOff / 100);
                            rate = rate - (rate * discountedpercent);
                            
                        }
                        else if (p.amountOff != null)
                        {
                            decimal discountedrate = rate - (decimal)p.amountOff;
                            rate = discountedrate * decimal.Parse("1.15");
                        }
                }

                return decimal.Round(rate, 2, MidpointRounding.AwayFromZero);
         
        }

        public void resetVoucher()
        {
            HttpContext.Current.Session[SESSION_VOUCHER_NAME] = null;
        }

        public int? getSessionVoucherID()
        {
            return (int?)HttpContext.Current.Session[SESSION_VOUCHER_NAME];
        }

        public Promotion getSessionOrOrderVoucher(Order order)
        {
            Promotion voucher = null;
            if (getSessionVoucherID().HasValue && order.SystemStatus == Order.SystemStatusOption.IN_PROGRESS)
                voucher = _orderRep.listAllPromotions().SingleOrDefault(v => v.ID == getSessionVoucherID());

            if (order.voucher == null && order.VoucherPromotionID.HasValue)
                voucher = _orderRep.listAllPromotions().SingleOrDefault(v => v.ID == order.VoucherPromotionID);

            if (voucher != null)
                checkPromotionForSpecialCircumstances(voucher, order);

            return voucher;
        }
        //return if needs retotal.
        public bool checkSessionOrOrderVoucher(Order order)
        {
            if (order.voucher != null && !checkPromotionAgainstOrder(order.voucher, order).success)
            {
                resetVoucher();
                if (order.VoucherPromotionID.HasValue)
                { 
                    order.VoucherPromotionID = null;
                    _orderRep.saveOrder(order);
                }
                return true;
            }
            return false;
        }

        public void loadVoucherCodeForOrder(Order order)
        {
            order.VoucherCode = _orderRep.listAllPromotions().FirstOrDefault(v => v.ID == order.VoucherPromotionID).code;
        }

        public Promotion checkForPromotion(Order order, bool runNewState)
        {
            if (order.SystemStatus == Order.SystemStatusOption.IN_PROGRESS || runNewState)
            { 
                var promotions = _orderRep.listAllPromotions().Where(x => x.Type == Promotion.TypeOption.PROMOTION && x.SystemType != Promotion.SystemTypeOption.PROPELLAPROMO && !x.isHidden && (!x.expiryDate.HasValue || x.expiryDate >= DateTime.Now)).OrderByDescending(x => x.priority ?? -1).ToList();
                foreach(var promotion in promotions)
                {
                    promotion.successfulTestAgainstOrder = checkPromotionAgainstOrder(promotion, order).success;
                    if (promotion.successfulTestAgainstOrder)
                        checkPromotionForSpecialCircumstances(promotion, order);
                }
                if (promotions.Any(x => x.successfulTestAgainstOrder && !x.splitTheDifference))
                    return promotions.First(x => x.successfulTestAgainstOrder && !x.splitTheDifference);
                if (promotions.Any(x => x.successfulTestAgainstOrder && x.splitTheDifference))
                    return promotions.First(x => x.successfulTestAgainstOrder && x.splitTheDifference);
            }
            else if(order.PromotionID.HasValue)
            {
                var promotion = _orderRep.listAllPromotions().Single(x => x.ID == order.PromotionID);
                checkPromotionForSpecialCircumstances(promotion, order);
                return promotion;
            }
            return null;
        }

        public void checkPromotionForSpecialCircumstances(Promotion promotion, Order order)
        {
            if (!isSplitDiffCapable(order))
                return;

            if (ShippingService.getFreeShippingEquivalentCost(order.CountryID,order.StateID,order.Postcode) == null)
                return;

            //#SpDiff# SPLIT THE DIFFERENCE TAG AS SPECIAL SITUATION
            if (promotion.maximumWeight.HasValue && order.Weight > promotion.maximumWeight &&
                //ALL FREE SHIPPING CART HAS NO LIMIT IN AUS
                !(promotion.SystemType == Promotion.SystemTypeOption.FREE_SHIPPING_PRODUCT_IN_CART &&
                    order.orderItems.All(x => x.Product.tags.Any(y => y.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING))
                ) &&
                //FREE SHIPPING PROMOTIONS AND VOUCHERS NOW SPLIT THE DIFFERENCE - RATHER THAN REJECTING TAG AS SPECIAL SITUATION
                (promotion.freeShipping)
                )
            {
                promotion.splitTheDifference = true;
                promotion.splitTheDifferenceExplanation = "Promotion weight limit exceeded";
            }

            if (promotion.ShippingMethodID.HasValue && order.ShippingMethodID != promotion.ShippingMethodID &&
                //#SpDiff# FREE SHIPPING PROMOTIONS AND VOUCHERS NOW SPLIT THE DIFFERENCE - RATHER THAN REJECTING TAG AS SPECIAL SITUATION
                (promotion.freeShipping && order.ShippingMethodID == 2))
            {
                promotion.splitTheDifference = true;
                promotion.splitTheDifferenceExplanation = "Express Post selected";
            }
        }

        public PromotionCheckResult checkPromotionAgainstOrder(Promotion promotion, Order order)
        {
            if (promotion.OrderID.HasValue || promotion.Status != Promotion.StatusOption.AVAILABLE_FOR_USE)
            {
                return new PromotionCheckResult()
                {
                    success = false,
                    message = "This promotion has already been used."
                };
            }

            if (promotion.expiryDate.HasValue && promotion.expiryDate < DateTime.Now)
            {
                return new PromotionCheckResult()
                {
                    success = false,
                    message = "Promotion has expired."
                };
            }

            if (promotion.minimumSpend.HasValue && 
                    (
                        (promotion.Type == Promotion.TypeOption.PROMOTION && (order.BasketTotal < promotion.minimumSpend))
                        ||
                        (promotion.Type != Promotion.TypeOption.PROMOTION && (order.totalAfterPromotion < promotion.minimumSpend))
                    )
                )
            {
                return new PromotionCheckResult()
                {
                    success = false,
                    message = string.Format("You need an item total more than or equal to {0} to use this promotion.",
                                        ((decimal)promotion.minimumSpend).ToString("c"))
                };
            }

            if (promotion.maximumWeight.HasValue && order.Weight > promotion.maximumWeight &&
                //ALL FREE SHIPPING CART HAS NO LIMIT IN AUS
                !(promotion.SystemType == Promotion.SystemTypeOption.FREE_SHIPPING_PRODUCT_IN_CART &&
                    order.orderItems.All(x => x.Product.tags.Any(y => y.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING))
                ) &&
                //#SpDiff# FREE SHIPPING PROMOTIONS AND VOUCHERS NOW SPLIT THE DIFFERENCE - RATHER THAN REJECTING TAG AS SPECIAL SITUATION 
                !(promotion.freeShipping && isSplitDiffCapable(order))
                ) 
            {
                return new PromotionCheckResult()
                {
                    success = false,
                    message = string.Format("You need a weight less than or equal to {0}kg to use this promotion.",
                                        ((decimal)promotion.maximumWeight).ToString("0.0"))
                };
            }

            if (promotion.locations.Any() && !ShippingService.getLocationsForOrder(order).Any(x => promotion.locationAllowed(x.ID)))
            {
                return new PromotionCheckResult()
                {
                    success = false,
                    message = string.Format("This voucher is not available for your current delivery location.")
                };
            }
         
            if (promotion.SKU !=null && promotion.minimumSpend.HasValue)
            {
                var freeproduct = _productRep.listAllProducts(false,true).Single(x => x.sku == promotion.SKU);
                if(freeproduct!=null && freeproduct.isHidden == false)
                { 
                    if(order.BasketTotal >= promotion.minimumSpend)
                    {
                        Basket freebie = new Basket()
                        {
                            VariantID = freeproduct.variant.ID,
                            Variant = freeproduct.variant,
                            OrderID = order.ID == 0 ? (int?)null : order.ID,
                            Price = 0,
                            SessionKey = order.SessionKey,
                            Quantity = 1,
                            CreateDate = DateTime.Now
                        };
                        if(_orderRep.listAllBasket().Where(x=>x.SessionKey == order.SessionKey && x.VariantID == freebie.VariantID).ToList().Count == 0)
                        { 
                            _orderRep.saveBasket(freebie);///only add it once.
                            
                        }

                    } else {
                        ///remove item if it exists
                        List<Basket> freeitems = _orderRep.listAllBasket().Where(x => x.SessionKey == order.SessionKey && x.VariantID == freeproduct.variant.ID).ToList();
                        foreach(Basket b in freeitems)
                        {
                            _orderRep.deleteBasket(b);
                        }
                   
                    }
                }
            }

           
            if (promotion.ProductID.HasValue && !order.orderItems.Any(x => x.Product.ID == promotion.ProductID))
            {
                var product = _productRep.listAllProducts().Single(x => x.ID == promotion.ProductID);
                return new PromotionCheckResult()
                {
                    success = false,
                    message = string.Format("This voucher requires the product {0} in your cart.", product.name)
                };
            }
            /* old producct relationship system.
             */

            if (promotion.TagID.HasValue && !_productRep.listProductsForTag((int)promotion.TagID).ToList().Any(x => order.orderItems.Any(y => y.Product.ID == x.ID)))
            {
                var tag = _contentRep.listAllTags().Single(x => x.ID == promotion.TagID);
                return new PromotionCheckResult()
                {
                    success = false,
                    message = string.Format("This voucher requires a {0} product in your cart.", tag.name)
                };
            }

            if (promotion.ShippingMethodID.HasValue && order.ShippingMethodID != promotion.ShippingMethodID &&
                //#SpDiff# FREE SHIPPING PROMOTIONS AND VOUCHERS NOW SPLIT THE DIFFERENCE - RATHER THAN REJECTING TAG AS SPECIAL SITUATION
                !(promotion.freeShipping && order.ShippingMethodID == 2 && isSplitDiffCapable(order)))
            {
                var shippingMedthod = _shippingRep.listAllShippingMethods().Single(x => x.ID==promotion.ShippingMethodID);
                return new PromotionCheckResult()
                {
                    success = false,
                    message = string.Format("This voucher requires the shipping method {0} in your cart.", shippingMedthod.Name)
                };
            }

            

            if (promotion.SystemType.HasValue)
            {
                switch (promotion.SystemType)
                {
                    case Promotion.SystemTypeOption.MAILER_SPECIAL:
                      
                        break;
                    case Promotion.SystemTypeOption.FREE_SHIPPING_PRODUCT_IN_CART:
                        if (!order.orderItems.Any(x => x.Product.tags.Any(y => y.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING)))
                        {
                            return new PromotionCheckResult()
                            {
                                success = false,
                                message = string.Format("You need a product with free shipping for this promotion", promotion.name)
                            };
                        }
                        break;
                }
            }

            
            return new PromotionCheckResult()
            {
                success = true,
            };
        }

        public PromotionCheckResult checkVoucher(Promotion voucher, Order order, bool forceSaveToOrder = false)
        {
            var foundVoucher = _orderRep.listAllPromotions().FirstOrDefault(v => v.code == voucher.code.Trim() && !v.isHidden);

            if (foundVoucher == null)
            {
                return new PromotionCheckResult()
                {
                    success = false, 
                    message = "Voucher was not found."
                };
            }

            var checkResult = checkPromotionAgainstOrder(foundVoucher, order);
            
            if (voucher.clearThisPromotion)
            {
                resetVoucher();
                if (order.VoucherPromotionID.HasValue)
                {
                    order.VoucherPromotionID = null;
                    if (order.ID > 0)
                        _orderRep.saveOrder(order);
                }
            }
            else
            {
                if (checkResult.success)
                {
                    if (order.SystemStatus == Order.SystemStatusOption.IN_PROGRESS && !forceSaveToOrder)
                    { 
                        HttpContext.Current.Session[SESSION_VOUCHER_NAME] = foundVoucher.ID;
                    }
                    else
                    {
                        order.VoucherPromotionID = foundVoucher.ID;
                        _orderRep.saveOrder(order);
                    }
                }
            }

            return checkResult;
        }

        public void associateOrRemovePromotion(Order order, bool saveOrder)
        {
            order.PromotionID = order.promotion != null ? order.promotion.ID : (int?)null;
            if (saveOrder)
                _orderRep.saveOrder(order);
        }

        public void associateVoucher(Order order)
        {
            if (!checkVoucher(order.voucher, order).success)
                return;
            order.VoucherPromotionID = order.voucher.ID;
            _orderRep.saveOrder(order);
        }

        public OrderPayment usePromotion(Order order)
        {
            var promotion = order.voucher;
            if (promotion == null)
                return null;

            OrderPayment op = new OrderPayment() { Type = OrderPayment.TypeOption.VOUCHER };
            op.OrderID = order.ID;
            op.AccountDetailID = order.CustomerDetails.ID;
            op.ResponseText = "Promotion";
            op.FullResponse = "Promotion Used " + promotion.code + " for Order " + order.ID + " - " + order.CustomerDetails.FirstName + " " + order.CustomerDetails.Surname;
            op.PaymentDate = System.DateTime.Now;
            op.PaymentStatus = 1;
            op.PaymentCode = promotion.code;
            op.PaymentReference = "Promotion";
            _orderRep.saveOrderPayment(op);

            if(order.voucher.Type == Promotion.TypeOption.ONE_USE_VOUCHER)
            {
                promotion.OrderID = order.ID;
                promotion.Status = Promotion.StatusOption.USED;
                promotion.usedDate = DateTime.Now;
                _orderRep.savePromotion(promotion,false);
            }
            return op;
        }

        public void totalOrder(Order order, ShippingComponent shippingComponent)
        {
            if (order.promotion != null)
            {
                var promotionEffect = order.promotion.getDiscount(order);

                switch (promotionEffect.EffectedTotalType)
                {
                    case PromotionEffect.EffectedTotalOption.BASKET_TOTAL:
                        order.promotionDiscount = promotionEffect.amountOff;
                        order.totalAfterPromotion = order.BasketTotal - promotionEffect.amountOff;
                        order.taxedItemTotal = order.taxedItemTotal - promotionEffect.amountOffTaxed;
                        break;
                    case PromotionEffect.EffectedTotalOption.FREE_SHIPPING:
                        order.totalAfterPromotion = order.BasketTotal;
                        if(shippingComponent != null)
                        {
                            order.FreightWouldHaveBeenTotal = shippingComponent.amount;
                        }
                        if (order.promotion.splitTheDifference)
                        {
                            order.promotionDiscount = (decimal)ShippingService.getFreeShippingEquivalentCost(order.CountryID, order.StateID, order.Postcode);
                            order.totalAfterPromotion = order.BasketTotal;
                            order.FreightTotal = order.AdminAdHocFreightTotal ?? ((decimal)order.FreightWouldHaveBeenTotal - order.promotionDiscount);
                        }
                        else
                        {
                            order.promotionDiscount = order.FreightWouldHaveBeenTotal ?? order.FreightTotal;
                            //order.taxedItemTotal uneffected
                            order.FreightTotal = order.AdminAdHocFreightTotal ?? 0;
                        }
                        break;
                    case PromotionEffect.EffectedTotalOption.ORDER_TOTAL:
                        throw new Exception("Promotion effects on full total including shipping has not been implemented.");
                }

                order.promotionText = promotionEffect.message;
            }
            else
                order.totalAfterPromotion = order.BasketTotal;

            if (order.voucher != null)
            {
                var voucherEffect = order.voucher.getDiscount(order);

                switch (voucherEffect.EffectedTotalType)
                {
                    case PromotionEffect.EffectedTotalOption.BASKET_TOTAL:
                        order.voucherDiscount = voucherEffect.amountOff;
                        order.totalAfterVoucher = (order.totalAfterPromotion - voucherEffect.amountOff);
                        order.taxedItemTotal = order.taxedItemTotal - voucherEffect.amountOffTaxed;
                        break;
                    case PromotionEffect.EffectedTotalOption.FREE_SHIPPING:
                        order.totalAfterVoucher = order.totalAfterPromotion;
                        if (shippingComponent != null)
                        {
                            order.FreightWouldHaveBeenTotal = shippingComponent.amount;
                        }
                        if (order.voucher.splitTheDifference)
                        {
                            order.voucherDiscount = (decimal)ShippingService.getFreeShippingEquivalentCost(order.CountryID, order.StateID, order.Postcode);
                            order.FreightTotal = order.AdminAdHocFreightTotal ?? ((decimal)order.FreightWouldHaveBeenTotal - order.promotionDiscount);
                        }
                        else
                        {
                            order.voucherDiscount = order.FreightWouldHaveBeenTotal ?? order.FreightTotal;
                            order.FreightTotal = order.AdminAdHocFreightTotal ?? 0;
                            //order.taxedItemTotal uneffected
                        }
                        break;
                    case PromotionEffect.EffectedTotalOption.ORDER_TOTAL:
                        throw new Exception("Promotion effects on full total including shipping has not been implemented.");
                }

                order.voucherText = order.voucher.name;
            }
            else
                order.totalAfterVoucher = order.totalAfterPromotion;


            if (order.totalAfterVoucher < 0)
                order.OrderTotal = order.FreightTotal;
            else
                order.OrderTotal = order.totalAfterVoucher + order.FreightTotal;

        }
        
        #region Special
        
        public string specialPromotionGetTag(string email)
        {
            return email + "|" + Promotion.SystemTypeOption.MAILER_SPECIAL.ToString();
        }

        public bool specialPromotionCanInitiate(string email)
        {
            return !_orderRep.listAllPromotions().Any(x => x.notes == specialPromotionGetTag(email));
        }

        public bool specialPromotionInitiate(string email)
        {
            var result = MailChimpService.AddEmailToList(email);

            if (!result.success)
            {
                return false;
            }

            Promotion promotion = new Promotion()
            {
                Type = Promotion.TypeOption.ONE_USE_VOUCHER,
                SystemType = Promotion.SystemTypeOption.MAILER_SPECIAL,
                notes = specialPromotionGetTag(email),
                minimumSpend = 50,
                amountOff = 10,
                code = generateRandomString(6),
                createDate = DateTime.Now,
            };

            _orderRep.savePromotion(promotion,false);

            var mail = new MailMessage(ConfigurationManager.AppSettings["FromEmailSpecial"], email)
            {
                Subject = "Your E-newsletter Promotion",
                Body = string.Format("Your $10 off voucher code is {0}. Please confirm your subscription before using this voucher. (Please check your spam box if you can't find the email from mailchimp.com)", promotion.code),
                IsBodyHtml = true
            };
            using (var client = new SmtpClient())
            {
                client.Send(mail);
            }
            return true;
        }

        public bool specialPromotionResend(string code)
        {
            Promotion promotion = _orderRep.listAllPromotions().SingleOrDefault(x => x.code.ToLower()==code.ToLower() && x.SystemType == Promotion.SystemTypeOption.MAILER_SPECIAL);

            var result = MailChimpService.AddEmailToList(promotion.notes.Split('|')[0]);
            return result.success;
        }

        public void specialSupress()
        {
            HttpCookie hc = new HttpCookie("SpecialSupressed");
            hc.Value = "true";
            hc.Expires = System.DateTime.Now.AddDays(3 * 30);
            HttpContext.Current.Response.Cookies.Add(hc);
        }

        public bool specialHasSupress()
        {
            return HttpContext.Current.Request.Cookies.AllKeys.Any(x => x == "SpecialSupressed");
        }

        #endregion

        public static string generateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static bool isSplitDiffCapable(Order order)
        {
            return (order.ID == 0) || order.ID >= (ModelTransformations.IsLive() ? 100109 : 99000);
        }
    }

    public class PromotionCheckResult
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string email { get; set; }
    }

    public class PromotionEffect
    {
        public enum EffectedTotalOption
        {
            BASKET_TOTAL,
            FREE_SHIPPING,
            ORDER_TOTAL
        }

        public decimal amountOff { get; set; }
        public decimal amountOffTaxed { get; set; }
        public EffectedTotalOption EffectedTotalType { get; set; }
        public string message { get; set; } 
    }
}