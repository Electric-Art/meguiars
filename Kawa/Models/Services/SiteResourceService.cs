﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;
using Simple.ImageResizer;
using Kawa.Models.Extensions;

namespace Kawa.Models
{
    public class SiteResourceService
    {
        public enum ForceOption
        {
            WIDTH,
            HEIGHT
        }

        public static void loadUrls(SiteResource[] srs)
        {
            foreach (var sr in srs)
            {
                sr.urlLessFile = getPathWithoutFile(sr);
                sr.url = getPath(sr);
            }
        }

        public static void loadUrls(SiteResourceOwner owner, SiteResource.SiteResourceVersion version)
        {
            foreach (var sr in owner.siteResources)
            {
                sr.urlLessFile = getPathWithoutFile(sr);
                sr.url = owner.getSrUrl(sr.SiteResourceType, version);
            }
        }

        public void SaveJob(HttpPostedFileBase file, ISavedFile job)
        {
            var dir = new DirectoryInfo(HttpContext.Current.Server.MapPath(job.SavePath));
            if (!dir.Exists) dir.Create();
            job.FileName = PrepareName(dir.FullName, file.FileName, job.SiteResourceType);
            switch (job.SiteResourceType)
            {
                case SiteResource.SiteResourceTypeOption.FILE_FOR_DOWNLOAD:
                case SiteResource.SiteResourceTypeOption.IN_CONTENT:
                case SiteResource.SiteResourceTypeOption.BILLBOARD:
                case SiteResource.SiteResourceTypeOption.BILLBOARD_MOBILE:
                case SiteResource.SiteResourceTypeOption.SLIDESHOW_MP4:
                case SiteResource.SiteResourceTypeOption.SLIDESHOW_MOBILE_MP4:
                case SiteResource.SiteResourceTypeOption.BRAND_LOGO:
                    file.SaveAs(Path.Combine(dir.FullName, job.FileName));
                    break;
                default :
                    file.SaveAs(Path.Combine(dir.FullName, job.FileName));
                    ///PrepareImage(file, job);
                    break;
            }
        }

        private string PrepareName(string path, string name, SiteResource.SiteResourceTypeOption type, bool overwrite = false)
        {
            string extension = null;

            switch (type)
            {
                case SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE:
                    extension = ".png";
                    break;
                case SiteResource.SiteResourceTypeOption.ARTICLE_FEATURED_WIDE:
                    extension = ".jpg";
                    break;
                case SiteResource.SiteResourceTypeOption.XTRA_PRODUCT_IMAGE:
                    extension = ".png";
                    break;
                default:
                    extension = Path.GetExtension(name);
                    break;
            }

            var savename = Path.GetFileNameWithoutExtension(name).ToSafeString(false, true) + extension;
            if (!overwrite && System.IO.File.Exists(Path.Combine(path, savename)))
            {
                return System.DateTime.Now.Ticks + "_" + savename;
            }
            return savename;
        }

        private void PrepareImage(HttpPostedFileBase file, ISavedFile job)
        {
            byte[] data;
            Image img = null;
            using (Stream inputStream = file.InputStream)
            {
                MemoryStream memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                data = memoryStream.ToArray();
                img = Image.FromStream(inputStream);
            }
            var imageResizer = new ImageResizer(data);
            try
            {
                switch (job.SiteResourceType)
                {
                    case SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE:
                        int minBoxWidth = 256;
                        Size inSize = new Size(img.Width, img.Height);
                        var aspectRatio = (double)inSize.Width / (double)inSize.Height;
                        Size toSize = (aspectRatio >= 1) ? new Size((int)(minBoxWidth * aspectRatio), minBoxWidth) : new Size(minBoxWidth, (int)(minBoxWidth / aspectRatio));
                        imageResizer.Resize(toSize.Width, toSize.Height, false, ImageEncoding.Png);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_FEED);

                        imageResizer = new ImageResizer(data);
                        imageResizer.Resize(448, 448, false, ImageEncoding.Png);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_LARGE);

                        imageResizer = new ImageResizer(data);
                        imageResizer.Resize(300, 300, false, ImageEncoding.Png);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_PAGE);

                        imageResizer = new ImageResizer(data);
                        imageResizer.Resize(213, 213, false, ImageEncoding.Png);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_LIST);

                        imageResizer = new ImageResizer(data);
                        imageResizer.Resize(100, 100, false, ImageEncoding.Png);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_SEARCH);
                        break;
                    case SiteResource.SiteResourceTypeOption.ARTICLE_FEATURED_WIDE:
                        imageResizer.Resize(660, 660, false, ImageEncoding.Jpg);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_PAGE);
                        break;
                    case SiteResource.SiteResourceTypeOption.SLIDESHOW_IMAGE:
                        minBoxWidth = 700;
                        inSize = new Size(img.Width, img.Height);
                        aspectRatio = (double)inSize.Width / (double)inSize.Height;
                        toSize = (aspectRatio >= 1) ? new Size((int)(minBoxWidth * aspectRatio), minBoxWidth) : new Size(minBoxWidth, (int)(minBoxWidth / aspectRatio));
                        imageResizer.Resize(toSize.Width, toSize.Height, false, ImageEncoding.Png);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_PAGE);
                        break;
                    case SiteResource.SiteResourceTypeOption.XTRA_PRODUCT_IMAGE:
                        imageResizer.Resize(448, 448, false, ImageEncoding.Png);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_LARGE);

                        imageResizer = new ImageResizer(data);
                        imageResizer.Resize(300, 300, false, ImageEncoding.Png);
                        saveImageResize(imageResizer, job, SiteResource.SiteResourceVersion.ITEM_PAGE);
                        break;
                }
            }
            catch (Exception e)
            {
                /*
                if (System.IO.File.Exists(fullPath))
                {
                    new System.IO.FileInfo(fullPath).Delete();
                }
                 * */
                throw e;
            }

        }

        private void saveImageResize(ImageResizer resizer, ISavedFile job, SiteResource.SiteResourceVersion version)
        {
            var dir = new DirectoryInfo(HttpContext.Current.Server.MapPath(job.SavePath));
            if (!dir.Exists) dir.Create();
            resizer.SaveToFile(Path.Combine(dir.FullName, SiteResource.getFileNameVersion(job.FileName,version,false)));
        }

        private bool saveImageResizeExists(ISavedFile job, SiteResource.SiteResourceVersion version)
        {
            var dir = new DirectoryInfo(HttpContext.Current.Server.MapPath(job.SavePath));
            return new FileInfo(Path.Combine(dir.FullName, SiteResource.getFileNameVersion(job.FileName, version, false))).Exists;
        }

        private static string getFileDirectory(SiteResource sR)
        {
            return HttpContext.Current.Server.MapPath("~/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + sR.ObjectID.ToString() + "/");
        }
        private static string getFileDirectoryDb(Models.Data.SiteResource sR)
        {
            return HttpContext.Current.Server.MapPath("~/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + sR.ObjectID.ToString() + "/");
        }
        public void AddFromDisk(SiteResource sR, FileInfo file, SiteResourceRepository _res)
        {
            string imagesDir = getFileDirectory(sR);
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(imagesDir);
            if (!dir.Exists)
                dir.Create();

            ///sR.FileName = ea.Controllers.SiteController.cleanFileName(sR.FileName);

            if (System.IO.File.Exists(imagesDir + sR.FileName))
            {
                //if the file is already there, then create a unique name.
                sR.FileName = System.DateTime.Now.Ticks + "_" + sR.FileName;
            }

            file.CopyTo(imagesDir + sR.FileName);

            _res.saveSiteResource(sR);
        }

        public void MoveFromTagtoPrescription(Models.Data.SiteResource sR, FileInfo file, SiteResourceRepository _res)
        {
            string from = getFileDirectoryDb(sR).Replace("PRESCRIPTIONTAG","TAG");
            string to = getFileDirectoryDb(sR);
            System.IO.File.Move(from + sR.FileName, to + sR.FileName);
        }
        public void RebuildForFeed(SiteResource sR, SiteResourceRepository _res)
        {
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(getFileDirectory(sR));
            FileInfo file = new FileInfo(Path.Combine(dir.FullName, sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LARGE)));

            if (!file.Exists)
            {
                using (System.IO.StreamWriter txtfile = new System.IO.StreamWriter(HttpContext.Current.Server.MapPath("~/Content/SiteResources/ProductsMissingLargeImages.txt"), true))
                {
                    txtfile.WriteLine(sR.ProductID + "," + file.Name);
                }
                return;
            }

            var imageResizer = new ImageResizer(file.FullName);
            var img = Image.FromFile(file.FullName);
            int minBoxWidth = 256;
            Size inSize = new Size(img.Width, img.Height);
            var aspectRatio = (float)inSize.Width / (float)inSize.Height;
            Size toSize = (aspectRatio >= 1) ? new Size((int)(minBoxWidth * aspectRatio), minBoxWidth) : new Size(minBoxWidth, (int)(minBoxWidth / aspectRatio));
            imageResizer.Resize(toSize.Width, toSize.Height, false, ImageEncoding.Png);
            saveImageResize(imageResizer, sR, SiteResource.SiteResourceVersion.ITEM_FEED);

            FileInfo fileToCopy = new FileInfo(Path.Combine(dir.FullName, sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_FEED)));
            System.IO.DirectoryInfo dirToCopyTo = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~/Content/SiteResources/FeedImagesToUpload/PRODUCT/" + sR.ProductID + "/"));
            if(!dirToCopyTo.Exists)
                dirToCopyTo.Create();
            fileToCopy.CopyTo(Path.Combine(dirToCopyTo.FullName,fileToCopy.Name));
        }

        public void AddFromDisk(SiteResource sR, string fileName, SiteResourceRepository _res, bool overwrite)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                return;

            string imagesDir = getFileDirectory(sR);
            System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(imagesDir);
            if (!dir.Exists)
                dir.Create();

            var dirpool = new DirectoryInfo("\\\\Mac\\Dropbox\\Telepathy Clients\\ElectricArt\\DVE\\DVE sql\\DVE Product Images");
            if (!dirpool.Exists) throw new Exception("pool dir doesn't exist fool");

            var file = new FileInfo(Path.Combine(dirpool.FullName, fileName.Replace(":", "_").Replace("Nature's", "Nature’s")));

            if (!file.Exists)
                return;

            sR.FileName = PrepareName(dir.FullName, fileName, sR.SiteResourceType, true);
            switch (sR.SiteResourceType)
            {
                case SiteResource.SiteResourceTypeOption.FILE_FOR_DOWNLOAD:
                case SiteResource.SiteResourceTypeOption.IN_CONTENT:
                case SiteResource.SiteResourceTypeOption.BILLBOARD:
                case SiteResource.SiteResourceTypeOption.BILLBOARD_MOBILE:
                case SiteResource.SiteResourceTypeOption.BRAND_LOGO:
                    file.CopyTo(imagesDir + sR.FileName);
                    break;
                default:
                    var imageResizer = new ImageResizer(file.FullName);
                    switch (sR.SiteResourceType)
                    {
                        case SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE:
                            if (overwrite || !saveImageResizeExists(sR, SiteResource.SiteResourceVersion.ITEM_LARGE))
                            { 
                                imageResizer.Resize(448, 448, false, ImageEncoding.Png);
                                saveImageResize(imageResizer, sR, SiteResource.SiteResourceVersion.ITEM_LARGE);
                            }
                            if (overwrite || !saveImageResizeExists(sR, SiteResource.SiteResourceVersion.ITEM_PAGE))
                            { 
                                imageResizer.Resize(300, 300, false, ImageEncoding.Png);
                                saveImageResize(imageResizer, sR, SiteResource.SiteResourceVersion.ITEM_PAGE);
                            }
                            if (overwrite || !saveImageResizeExists(sR, SiteResource.SiteResourceVersion.ITEM_LIST))
                            {
                                imageResizer.Resize(213, 213, false, ImageEncoding.Png);
                                saveImageResize(imageResizer, sR, SiteResource.SiteResourceVersion.ITEM_LIST);
                            }
                            if (overwrite || !saveImageResizeExists(sR, SiteResource.SiteResourceVersion.ITEM_SEARCH))
                            {
                                imageResizer.Resize(100, 100, false, ImageEncoding.Png);
                                saveImageResize(imageResizer, sR, SiteResource.SiteResourceVersion.ITEM_SEARCH);
                            }
                            break;
                        case SiteResource.SiteResourceTypeOption.ARTICLE_FEATURED_WIDE:
                            imageResizer.Resize(660, 319, false, ImageEncoding.Jpg);
                            saveImageResize(imageResizer, sR, SiteResource.SiteResourceVersion.ITEM_PAGE);
                            break;
                    }
                    break;
            }


            _res.saveSiteResource(sR);
        }

        public void ClearFiles()
        {
            var dirpool = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Port/DVE Product Images/"));
            if (!dirpool.Exists) throw new Exception("pool dir doesn't exist fool");

            var files = dirpool.GetFiles().Where(x => x.Name.Contains(":")).ToList();

        }

        public void Delete(SiteResource sR, SiteResourceRepository _res)
        {
            string imagesDir = getFileDirectory(sR);
            System.IO.FileInfo file = new System.IO.FileInfo(imagesDir + sR.FileName);

            if (file.Exists)
                file.Delete();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE));
            if (file.Exists) file.Delete();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST));
            if (file.Exists) file.Delete();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_SEARCH));
            if (file.Exists) file.Delete();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LARGE));
            if (file.Exists) file.Delete();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_FEED));
            if (file.Exists) file.Delete();
            /*
            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE,false));
            if (file.Exists) file.Delete();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST, false));
            if (file.Exists) file.Delete();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA, false));
            if (file.Exists) file.Delete();
            */
            _res.deleteSiteResource(sR);
        }

        public SiteResource CloneProductImage(SiteResource sR, int NewProductID, SiteResourceRepository _res)
        {
            var newSr = sR.clone(NewProductID,SiteResourceOwner.ObjectTypeOption.PRODUCT,_res);
            newSr.ProductID = NewProductID;
            _res.saveSiteResource(newSr);

            string imagesDir = getFileDirectory(sR);
            System.IO.FileInfo file = new System.IO.FileInfo(imagesDir + sR.FileName);

            string newImagesDir = getFileDirectory(newSr);
            var newDir = new DirectoryInfo(newImagesDir);
            if (!newDir.Exists)
                newDir.Create();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE),true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_SEARCH, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_SEARCH, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LARGE, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LARGE, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_FEED, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_FEED, false), true);

            return newSr;
        }


        public SiteResource CloneTagSiteResource(SiteResource sR, int newTagID, SiteResourceRepository _res)
        {
            var newSr = sR.clone(newTagID, SiteResourceOwner.ObjectTypeOption.TAG, _res);
            newSr.TagID = newTagID;
            _res.saveSiteResource(newSr);

            string imagesDir = getFileDirectory(sR);
            System.IO.FileInfo file = new System.IO.FileInfo(imagesDir + sR.FileName);

            string newImagesDir = getFileDirectory(newSr);
            var newDir = new DirectoryInfo(newImagesDir);
            if (!newDir.Exists)
                newDir.Create();

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_PAGE, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LIST_RETINA, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_SEARCH, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_SEARCH, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LARGE, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_LARGE, false), true);

            file = new System.IO.FileInfo(imagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_FEED, false));
            if (file.Exists) file.CopyTo(newImagesDir + sR.getFileNameVersion(SiteResource.SiteResourceVersion.ITEM_FEED, false), true);

            return newSr;
        }

        public static string getPathWithoutFile(SiteResource sR)
        {
            return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/";
        }

        public static string getPath(SiteResource sR)
        {
            return getPathWithoutFile(sR) + sR.FileName;
        }

        //TODO Remove when you have images
        public static string getPathDemo(SiteResource sR)
        {
            /*
            switch (sR.SiteResourceType)
            {
                case SiteResource.SiteResourceTypeOption.HEADER :
                    return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/thumb_" + sR.FileName;
            }
            */
            return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/2/" + "th_ANXIETY_AID_30s_RGB.jpg";
            
        }

        public static string getThumbPath(SiteResource sR)
        {
            /*
            switch (sR.SiteResourceType)
            {
                case SiteResource.SiteResourceTypeOption.CONTENT:
                    return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/thumb_" + sR.FileName;
                    
                case SiteResource.ResourceType.EXTERNAL_IMAGE:
                    return sR.FileName;
                    
            }
             * */
            return "/Content/SiteResources/" + sR.ObjectType.ToString() + "/" + (int)sR.ObjectID + "/thumb_" + sR.FileName;
        }

        public string getFileName(SiteResource sR)
        {
            return sR.FileName;
        }

    }


}
