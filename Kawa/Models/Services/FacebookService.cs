﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Facebook;

namespace Kawa.Models.Services
{
    public class FacebookService
    {
        public bool runUpdate(string usertoken, ContentRepository _contentRep)
        {
            try
            {
                if (usertoken != null)
                    usertoken = GetExtendedAccessToken(usertoken);

                Page page = _contentRep.listAllPages().SingleOrDefault(p => p.SystemPage == Page.SystemPageOption.FACEBOOK_PAGE_LAST_POST) ??
                                new Page()
                                {
                                    SystemPage = Models.Page.SystemPageOption.FACEBOOK_PAGE_LAST_POST,
                                    name = "Most Recent Facebook Page Post",
                                    createdDate = DateTime.Now,
                                    Type = Models.Page.TypeOption.SOCIAL_IMPORT,
                                    html4 = "CAANwRgP1xqMBAOYgGX4h8DZCmzi7kq3h1bRlunrZBsoGINxUScW0wnu7ZB9OilaiCtmsgZAOIWXemGXRWZAjcm2cYCt3vZBxXLsO7RKQXGhILPZATBp4N0ZCrWL3tTvIqnmJbj5zTYOHMwWNKXxJJeGSZCKJaYNeP80fSywMlsFGIs9SiTMzD0wX1yFRZAZAXF6Cr0ZD"
                                };


                var client = new FacebookClient(usertoken ?? page.html4);
                dynamic postResult = client.Get("/kawahealth/feed", new { fields = "id, message, picture, object_id, created_time", limit = "1" });
                dynamic imageResult = client.Get("/" + postResult.data[0].object_id, new { fields = "name, images" });


                page.html2 = postResult.data[0].message;
                page.articleDate = DateTime.Parse(postResult.data[0].created_time);
                if (imageResult.images.Count>0)
                {
                    page.html3 = imageResult.images[0].source;
                }
                else
                {
                    page.html3 = null;
                }
                if (usertoken != null)
                    page.html4 = usertoken;
                _contentRep.savePage(page);
                return true;
            }
            catch (FacebookOAuthException)
            {
                // The access token expired or the user 
                // has not granted your app 'email' permission.
                // Handle this by redirecting the user to the
                // Facebook authenticate and ask for email permission.
                return false;
            }
        }

        private string GetExtendedAccessToken(string ShortLivedToken)
        {
            FacebookClient client = new FacebookClient();
            string extendedToken = "";
            try
            {
                dynamic result = client.Get("/oauth/access_token", new
                {
                    grant_type = "fb_exchange_token",
                    client_id = ConfigurationManager.AppSettings["FacebookAppID"],
                    client_secret = ConfigurationManager.AppSettings["FacebookAppSecret"],
                    fb_exchange_token = ShortLivedToken
                });
                extendedToken = result.access_token;
            }
            catch
            {
                extendedToken = ShortLivedToken;
            }
            return extendedToken;
        }
    }
}