﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MailChimp;
using MailChimp.Lists;
using MailChimp.Helper;
using System.Configuration;
using System.Threading.Tasks;

namespace Kawa.Models.Services
{
    public class MailChimpService
    {
        public static void ClearLocalCache(PersonRepository _personRep)
        {
            _personRep.clearAllMailChimpCaches();
        }

        public static int? SaveNextListBatch(int pageNo, PersonRepository _personRep)
        {
            // Pass the API key on the constructor:
            MailChimpManager mc = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);

            //var list = mc.GetLists().Data.First();

            var results = mc.GetAllMembersForList(ConfigurationManager.AppSettings["MailChimpListID"], "subscribed", pageNo, 100).Data;
            var caches = results.Select(x => new Models.Data.MailChimpCache()
            {
                Email = x.Email,
                EmailType = x.EmailType,
                Timestamp = x.Timestamp,
                TimestampOptIn = x.TimestampOptIn,
                createDate = DateTime.Now
            }).ToList();
            _personRep.saveMailChimpCaches(caches);
            
            // Next, make any API call you'd like:
            return (results.Count == 0) ? (int?)null : pageNo + 1;
        }

        public static List<ListInfo> GetLists()
        {
            // Pass the API key on the constructor:
            MailChimpManager mc = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);

            //var list = mc.GetLists().Data.First();

            var lists = mc.GetLists();
            // Next, make any API call you'd like:
            return lists.Data;
        }

        public static List<MemberInfo> GetList()
        {
            // Pass the API key on the constructor:
            MailChimpManager mc = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);

            //var list = mc.GetLists().Data.First();

            var fullMembers = new List<MemberInfo>();
            bool zero = false;
            int startIndex = 0;
            while (!zero)
            {
                var results = mc.GetAllMembersForList(ConfigurationManager.AppSettings["MailChimpListID"], "subscribed", startIndex, 1000).Data;
                startIndex++;
                if (results.Count == 0)
                    zero = true;
                else
                    fullMembers.AddRange(results);
            }
            // Next, make any API call you'd like:
            return fullMembers;
        }

        public static bool IsEmailPastPending(string email)
        {
            MailChimpManager mc = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);
            var memberinfo = mc.GetMemberInfo(ConfigurationManager.AppSettings["MailChimpListID"], new List<EmailParameter>() { new EmailParameter() { Email = email } }).Data;
            return memberinfo.Any() && memberinfo.First().Status != "pending";
        }

        public static async Task<string> AddEmailToListAsync(string email, string FNAME, string LNAME)
        {
            MailChimpManager mc = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);
            EmailParameter emailToSend = new EmailParameter()
            {
                Email = email
            };
            MergeVar otherdata = new MergeVar();
            otherdata.Add("FNAME", FNAME);
            otherdata.Add("LNAME", LNAME);
            
            try
            {
                EmailParameter result = await Task.Run(() => mc.Subscribe(ConfigurationManager.AppSettings["MailChimpListID"], emailToSend, otherdata));
            }
            catch (Exception e)
            {

            }
            return email;
        }

        public static MailChimpResponse AddEmailToList(string email)
        {
            
            MailChimpManager mc = new MailChimpManager(ConfigurationManager.AppSettings["MailChimpApiKey"]);
            
            EmailParameter emailToSend = new EmailParameter()
            {
                Email = email
            };
            try 
            { 
                EmailParameter results = mc.Subscribe(ConfigurationManager.AppSettings["MailChimpListID"], emailToSend);
                return new MailChimpResponse() { success = true, email = results.Email };
            }
            catch(Exception e)
            {
                return new MailChimpResponse() { success = false, message = e.Message };
            }
        }
    }

    public class MailChimpResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string email { get; set; }
    }

}