﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using Kawa.Models.Extensions;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Kawa.Models.Data;

namespace Kawa.Models.Services
{
    public class DeploymentService
    {
        public static void RunDeploymentInit()
        {
            using (var _db = new dbDataContext())
            {
                try
                {
                    _db.Deployments.Any();
                }
                catch { 
                    var deploymentDirectory = new DirectoryInfo(Path.Combine(HttpRuntime.AppDomainAppPath, "App_Data\\Deployments", "DeploymentInit"));
                    if (!deploymentDirectory.Exists || !deploymentDirectory.GetFiles().Where(x => Path.GetExtension(x.FullName).ToLower() == ".sql").Any())
                    {
                        AlertService.SendTechMonitorAlert(string.Format("Deployment {0} has NOT run on {1}", "RunDeploymentInit", ModelTransformations.GetSiteUrl()), "No scripts were found.");
                        return;
                    }
                    var sqlFiles = deploymentDirectory.GetFiles().Where(x => Path.GetExtension(x.FullName).ToLower() == ".sql").ToList();
                    var deploy = new Deployment()
                    {
                        Name = "RunDeploymentInit",
                        Branch = "RunDeploymentInit",
                        UpdateLog = string.Format("Running deploy {0} batch {1}\n", "RunDeploymentInit", "RunDeploymentInit"),
                        StartDate = DateTime.Now
                    };

                    sqlFiles.ForEach(sqlFile =>
                    {
                        var sql = File.ReadAllText(sqlFile.FullName);
                        var start = DateTime.Now;
                        var result = "";
                        SplitSqlStatements(sql).ToList().ForEach(x =>
                        {
                            var runSql = x.Replace("BEGIN TRANSACTION", "--BEGIN TRANSACTION").Replace("COMMIT", "--COMMIT");
                            result += _db.ExecuteCommand(runSql);
                        });
                        deploy.UpdateLog += string.Format("Running {0} took {1} ms\n{2}\n", sqlFile.Name, (DateTime.Now - start).Milliseconds, result);
                    });

                    AlertService.SendTechMonitorAlert(string.Format("Deployment {0} has run on {1}", "RunDeploymentInit", ModelTransformations.GetSiteUrl()), deploy.UpdateLog.Replace("\n", "<br/><br/>"));

                    //deploymentDirectory.Delete(true);
                }
            }
        }

        public static void RunSqlDeployment(string name, string branch)
        {
            using(var _db = new dbDataContext())
            {
                if (_db.Deployments.Any(x => x.Name.ToLower() == name.ToLower()))
                    return;

                var deploymentDirectory = new DirectoryInfo(Path.Combine(HttpRuntime.AppDomainAppPath, "App_Data\\Deployments", name));
                if(!deploymentDirectory.Exists || !deploymentDirectory.GetFiles().Where(x => Path.GetExtension(x.FullName).ToLower() == ".sql").Any())
                {
                    AlertService.SendTechMonitorAlert(string.Format("Deployment {0} has NOT run on {1}", name, ModelTransformations.GetSiteUrl()), "No scripts were found.");
                    return;
                }
                var sqlFiles = deploymentDirectory.GetFiles().Where(x => Path.GetExtension(x.FullName).ToLower() == ".sql").OrderBy(x => x.Name).ToList();
                var deploy = new Deployment()
                {
                    Name = name,
                    Branch = branch,
                    UpdateLog = string.Format("Running deploy {0} batch {1}\n", name, branch),
                    StartDate = DateTime.Now
                };

                sqlFiles.ForEach(sqlFile =>
                {
                    var sql = File.ReadAllText(sqlFile.FullName);
                    var start = DateTime.Now;
                    var result = "";
                    SplitSqlStatements(sql).ToList().ForEach(x =>
                    {
                        var runSql = x.Replace("BEGIN TRANSACTION", "--BEGIN TRANSACTION").Replace("COMMIT", "--COMMIT");
                        result += _db.ExecuteCommand(runSql);
                    });
                    deploy.UpdateLog += string.Format("Running {0} took {1} ms\n{2}\n", sqlFile.Name, (DateTime.Now - start).Milliseconds, result);
                });

                deploy.FinishDate = DateTime.Now;

                _db.Deployments.InsertOnSubmit(deploy);
                _db.SubmitChanges();

                AlertService.SendTechMonitorAlert(string.Format("Deployment {0} has run on {1}", name, ModelTransformations.GetSiteUrl()), deploy.UpdateLog.Replace("\n","<br/><br/>"));

                //deploymentDirectory.Delete(true);
            }
        }

        public static void SuccessfulAdhocDeployment(string name, string branch, string description)
        {
            using (var _db = new dbDataContext())
            {
                var deploy = new Deployment()
                {
                    Name = name,
                    Branch = branch,
                    UpdateLog = description,
                    StartDate = DateTime.Now
                };
                _db.Deployments.InsertOnSubmit(deploy);
                _db.SubmitChanges();
            }
        }

        public static bool ShouldRunAdhocDeployment(string name)
        {
            using (var _db = new dbDataContext())
            {
                return !_db.Deployments.Any(x => x.Name.ToLower() == name.ToLower());
            }
        }

        private static IEnumerable<string> SplitSqlStatements(string sqlScript)
        {
            //split the script on "GO" commands
            string[] splitter = new string[] { "\r\nGO\r\n" };
            return sqlScript.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
        }

        public static Deployment StartRunningDeployment(string name, string branch, string action)
        {
            using (var _db = new dbDataContext())
            {
                if (_db.Deployments.Any(x => x.Name.ToLower() == name.ToLower()))
                    return null;

                var deploy = new Deployment()
                {
                    Name = name,
                    Branch = branch,
                    UpdateLog = action,
                    StartDate = DateTime.UtcNow
                };

                return deploy;
            }
        }

        public static void FinishRunningDeployment(Deployment deployment)
        {
            using (var _db = new dbDataContext())
            {
                deployment.FinishDate = DateTime.UtcNow;
                _db.Deployments.InsertOnSubmit(deployment);
                _db.SubmitChanges();

                AlertService.SendTechMonitorAlert(string.Format("Deployment {0} has run on {1}", deployment.Name, ModelTransformations.GetSiteUrl()), deployment.UpdateLog.Replace("\n", "<br/><br/>"));
            }
        }

        /*
        public static void AddAccount(string deployment, string email, string password, Account.TypeOption accountType, string role, string name)
        {

            using (var _db = new dbDataContext())
            {
                if (!_db.Accounts.Any(x => x.Email.ToLower() == email))
                {
                    var UserManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var RoleManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationRoleManager>();

                    var orwinRole = RoleManager.FindByName(role);
                    if (orwinRole == null)
                    {
                        orwinRole = new IdentityRole(role);
                        var roleresult = RoleManager.Create(orwinRole);
                    }
                    var user = new ApplicationUser { UserName = email.ToLower(), Email = email.ToLower() };
                    var result = UserManager.Create(user, password);
                    if (result.Succeeded)
                    {
                        UserManager.AddToRole(user.Id, role);

                        Account account = new Account()
                        {
                            Name = name,
                            Email = user.UserName,
                            Type = accountType,
                            CreateDate = DateTime.Now,
                            IsDisabled = false
                        };

                        _db.Accounts.InsertOnSubmit(account);
                        _db.SubmitChanges();
                        AlertService.SendTechMonitorAlert(string.Format("Deployment {0} added account to {1}", deployment, email, ModelTransformations.GetSiteUrl()),string.Format("{0} added as {1} {2}", email, accountType, role));
                    }
                    else
                    {
                        AlertService.SendTechMonitorAlert(string.Format("Deployment {0} has NOT added account to {1}", deployment, email, ModelTransformations.GetSiteUrl()), string.Format("{0} failed as {1} {2} - {3}", email, accountType, role, string.Join(",",result.Errors.ToArray())));
                    }
                }
            }
            
        }
        */
    }
}