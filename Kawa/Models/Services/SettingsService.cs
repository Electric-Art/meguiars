﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using Kawa.Models.Data;

namespace Kawa.Models.Services
{
    public class SettingsService
    {
        public enum SettingOption
        {
            International_Default_Days_To_Ship = 1,
            International_Default_Days_To_Ship_Minimum = 2,
            Local_Default_Days_To_Ship = 3,
            Local_Default_Days_To_Ship_Minimum = 4
        }

        public static object GetSettings()
        {
            return new
            {
                Local_Default_Days_To_Ship = Pull<int>(SettingOption.Local_Default_Days_To_Ship),
                Local_Default_Days_To_Ship_Minimum = Pull<int>(SettingOption.Local_Default_Days_To_Ship_Minimum),
                International_Default_Days_To_Ship = Pull<int>(SettingOption.International_Default_Days_To_Ship),
                International_Default_Days_To_Ship_Minimum = Pull<int>(SettingOption.International_Default_Days_To_Ship_Minimum)
            };
        }

        public static void Store(SettingOption setting, Object val)
        {
            var setup = new SettingSetup(setting);
            try {
                int val_int;
                string val_string;
                decimal val_decimal;

                var @switch = new Dictionary<Type, Action> {
                        { typeof(int), () => val_int = (int)Convert.ChangeType(val, typeof(int)) },
                        { typeof(string), () => val_string = (string)Convert.ChangeType(val, typeof(string)) },
                        { typeof(decimal), () => val_decimal = (decimal)Convert.ChangeType(val, typeof(decimal))  },
                    };
                @switch[setup.getMedium()]();
            }
            catch
            {
                throw new Exception("Wrong type for this setting");
            }
            using (var _db = new dbDataContext())
            {
                var settingData = _db.Settings.SingleOrDefault(x => x.ID == (int)setting);
                var @switch = new Dictionary<Type, Action> {
                        { typeof(int), () => settingData.val_int = (int)Convert.ChangeType(val, typeof(int)) },
                        { typeof(string), () => settingData.val_string = (string)Convert.ChangeType(val, typeof(string)) },
                        { typeof(decimal), () => settingData.val_decimal = (decimal)Convert.ChangeType(val, typeof(decimal))  },
                    };

                if (settingData == null)
                {
                    settingData = new Setting()
                    {
                        ID = (int)setting,
                        CreateDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        Name = setting.ToString()
                    };
                    @switch[setup.getMedium()]();
                    _db.Settings.InsertOnSubmit(settingData);
                    _db.SubmitChanges();
                }
                else
                {
                    @switch[setup.getMedium()]();
                    _db.SubmitChanges();
                }
            }
        }

        public static void Store<T>(SettingOption setting, T val)
        {
            var setup = new SettingSetup(setting);
            if (setup.getMedium() != typeof(T))
                throw new Exception("Wrong type for this setting");

            using (var _db = new dbDataContext())
            {
                var settingData = _db.Settings.SingleOrDefault(x => x.ID == (int)setting);
                if(settingData == null)
                {
                    settingData = new Setting()
                    {
                        ID = (int)setting,
                        CreateDate = DateTime.Now,
                        ModifiedDate = DateTime.Now
                    };
                    var @switch = new Dictionary<Type, Action> {
                        { typeof(int), () => settingData.val_int = (int)Convert.ChangeType(val, typeof(int)) },
                        { typeof(string), () => settingData.val_string = (string)Convert.ChangeType(val, typeof(string)) },
                        { typeof(decimal), () => settingData.val_decimal = (decimal)Convert.ChangeType(val, typeof(decimal))  },
                    };
                    @switch[typeof(T)]();
                    _db.Settings.InsertOnSubmit(settingData);
                    _db.SubmitChanges();
                }
            }
        }

        public static T Pull<T>(SettingOption setting)
        {
            var setup = new SettingSetup(setting);
            if (setup.getMedium() != typeof(T))
                throw new Exception("Wrong type for this setting");

            using (var _db = new dbDataContext())
            {
                var settingData = _db.Settings.SingleOrDefault(x => x.ID == (int)setting);
                if (settingData == null)
                {
                    return (T)Convert.ChangeType(setup.getDefault(), typeof(T));
                }
                else
                {
                    T val = default(T);
                    var @switch = new Dictionary<Type, Action> {
                        { typeof(int), () => val = (T)Convert.ChangeType(settingData.val_int, typeof(T)) },
                        { typeof(string), () => val = (T)Convert.ChangeType(settingData.val_string, typeof(T)) },
                        { typeof(decimal), () => val = (T)Convert.ChangeType(settingData.val_decimal, typeof(T))  },
                    };
                    @switch[typeof(T)]();
                    return val;
                }
            }
        }
    }

    public class SettingSetup
    {
        private SettingsService.SettingOption setting;

        public SettingSetup(SettingsService.SettingOption setting)
        {
            this.setting = setting;
        }

        public Type getMedium()
        {
            switch(setting)
            {
                case SettingsService.SettingOption.Local_Default_Days_To_Ship:
                case SettingsService.SettingOption.International_Default_Days_To_Ship:
                case SettingsService.SettingOption.Local_Default_Days_To_Ship_Minimum:
                case SettingsService.SettingOption.International_Default_Days_To_Ship_Minimum:
                    return typeof(int);
            }
            return typeof(int);
        }

        public Object getDefault()
        {
            switch (setting)
            {
                case SettingsService.SettingOption.Local_Default_Days_To_Ship_Minimum:
                    return 1;
                case SettingsService.SettingOption.Local_Default_Days_To_Ship:
                    return 3;
                case SettingsService.SettingOption.International_Default_Days_To_Ship_Minimum:
                    return 3;
                case SettingsService.SettingOption.International_Default_Days_To_Ship:
                    return 5;
            }
            return null;
        }
    }
}