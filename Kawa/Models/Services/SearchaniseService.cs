﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models;
using Kawa.Models.ViewModels;
using AutoMapper;
using Kawa.Models.Extensions;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Web.Script.Serialization;
using System.Configuration;

namespace Kawa.Models.Services
{
    public class SearchaniseService
    {
        private ProductRepository _productRep;
        private ContentRepository _contentRep;
        private const string API_KEY = "6P3P8R2b8g";
        private const string SECRET = "9J0J4y3K6b4s1N1n8M5P";
        public const int BATCHNO = 200;

        public SearchaniseService(ProductRepository prodRepo, ContentRepository contentRepo)
        {
            _productRep = prodRepo;
            _contentRep = contentRepo;
        }

        public async Task<string> GetApiKey()
        {
            //await ClearSearchanise();
            var xmlstr = "";
            using (var client = new HttpClient())
            {
               
                client.BaseAddress = new Uri("http://searchanise.com");

                var result = await client.PostAsync("/api/signup?email=scott@telepathy.co.nz&version=1.2&url=https://www.meguiars.co.nz", null);
                xmlstr = await result.Content.ReadAsStringAsync();
                
            }
            return string.Format("Successful Api response {0}", xmlstr);
        }

        public async Task<SuggestionResult> SearchSearchanise(string searchstr)
        {
            var querystring = string.Format("apiKey={0}&maxResults=8&suggestions=true&suggestionsMaxResults=4&q={1}", API_KEY, searchstr);
            string responseBody = null;
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync("http://searchanise.com/search?" + querystring);
                response.EnsureSuccessStatusCode();
                responseBody= await response.Content.ReadAsStringAsync();
                
            }

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            var result = json_serializer.Deserialize<SearchaniseSuggestionResponse>(responseBody);
            //return null;
            
            var prods = _productRep.listAllProducts(true,true).Where(x => result.items.Select(y => y.product_id).Contains(x.ID)).ToList();
            prods.ForEach(x =>
            {
                SiteResourceService.loadUrls(x, SiteResource.SiteResourceVersion.ITEM_SEARCH);
            });
            var tokens = searchstr.Split(' ').ToList();
            var tags = _contentRep.listAllTagsWithProductList().Where(x => !x.isHidden && x.name.Contains(searchstr)
                ).ToList().OrderBy(x => x.getRank(searchstr, tokens)).ThenBy(x => x.name).Take(16).ToList();
            return new SuggestionResult()
            {
                products_consumer = Mapper.Map<List<Product>, List<Models.Coms.Product>>(prods),
                tags = Mapper.Map<List<Tag>, List<Models.Coms.Tag>>(tags),
                suggestions = result.suggestions,
                isSearchanise = true
            };
            
        }

        public async Task<string> ReUploadToSearchanise()
        {
            //await ClearSearchanise();
            if (checkIfUpdatesInActive())
                return "Searchanise updates are disabled for the development site";

            var googleProducts = _productRep.listAllProducts(false, true).Where(x => !x.isHidden && x.HasVariant).ToList();
            var batchNo = 200;
            var doneNo = batchNo;
            var nextProducts = googleProducts.Take(doneNo);

            while (nextProducts.Count() > 0)
            {
                using (var client = new HttpClient())
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        client.BaseAddress = new Uri("http://searchanise.com");
                        var values = new[] 
                        {
                            new KeyValuePair<string, string>("private_key", SECRET),
                            new KeyValuePair<string, string>("data", GetProductsXmlForUpload(googleProducts))
                        };
                        foreach (var keyValuePair in values)
                        {
                            content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                        }

                        var result = await client.PostAsync("/api/items/update", content);
                        nextProducts = googleProducts.Skip(doneNo).Take(batchNo);
                        doneNo += batchNo;
                    }
                }
            }
            return string.Format("Successful Update with Searchanise - {0} products updated", googleProducts.Count());
        }

        public async Task<bool> ClearSearchanise()
        {
            if (checkIfUpdatesInActive())
                return false;

            using (var client = new HttpClient())
            {
                client.Timeout.Add(new TimeSpan(0, 1, 0));
                using (var content = new MultipartFormDataContent())
                {
                    client.BaseAddress = new Uri("http://searchanise.com");
                    var values = new[] 
                        {
                            new KeyValuePair<string, string>("private_key", SECRET),
                            new KeyValuePair<string, string>("all", "true")
                        };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }

                    var result = await client.PostAsync("/api/items/delete", content);
                }
            }
            return true;
        }

        public async Task<int?> UploadBatchToSearchanise(int startNo)
        {
            if (checkIfUpdatesInActive())
                return null;

            var nextProducts = _productRep.listAllProducts(false, true).Where(x => !x.isHidden && x.HasVariant).Skip(startNo).Take(BATCHNO).ToList();
            //var doneNo = batchNo;
            //var nextProducts = googleProducts;

            if (nextProducts.Count()>0)
            {
                using (var client = new HttpClient())
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        client.BaseAddress = new Uri("http://searchanise.com");
                        var values = new[] 
                        {
                            new KeyValuePair<string, string>("private_key", SECRET),
                            new KeyValuePair<string, string>("data", GetProductsXmlForUpload(nextProducts))
                        };
                        foreach (var keyValuePair in values)
                        {
                            content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                        }

                        var result = await client.PostAsync("/api/items/update", content);
                        return startNo + BATCHNO;
                    }
                }
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> UploadSingleProductToSearchanise(Product product)
        {
            if (checkIfUpdatesInActive())
                return false;

            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    client.BaseAddress = new Uri("http://searchanise.com");
                    var values = new[] 
                        {
                            new KeyValuePair<string, string>("private_key", SECRET),
                            new KeyValuePair<string, string>("data", GetProductsXmlForUpload(new List<Product>{product}))
                        };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }

                    var result = await client.PostAsync("/api/items/update", content);
                }
            }
            return true;
        }

        
        public async Task<bool> DeleteSingleProductToSearchanise(Product product)
        {
            if (checkIfUpdatesInActive())
                return false;

            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    client.BaseAddress = new Uri("http://searchanise.com");
                    var values = new[] 
                        {
                            new KeyValuePair<string, string>("private_key", SECRET),
                            new KeyValuePair<string, string>("id", product.ID.ToString())
                        };
                    foreach (var keyValuePair in values)
                    {
                        content.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                    }

                    var result = await client.PostAsync("/api/items/delete", content);
                }
            }
            return true;
        }



        public string GetProductsXmlForUpload(List<Product> products)
        {
            XNamespace atm = "http://www.w3.org/2005/Atom";
            XNamespace cns = "http://searchanise.com/ns/1.0";

            

            XDocument document = new XDocument(
                new XElement("feed",
                // new XAttribute(XNamespace.Xml, atm),
                     new XAttribute(XNamespace.Xmlns + "cs", cns),
                     new XElement("title", "Searchanise data feed"),
                     new XElement("updated", DateTime.Now.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz")),
                     new XElement("id", "http://meguiars.electricart.co.nz"),

                    from product in products
                    select new XElement("entry",
                        new XElement("id", product.ID),
                        new XElement("title", new XCData(product.name)),
                        //new XElement("summary", new XCData(RemoveInvalidXMLChars(product.blurb))),
                        new XElement("link", "https://www.meguiars.co.nz" + product.getUrl())
                        )));

            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + document.ToString();
        }

        // filters control characters but allows only properly-formed surrogate sequences
        private static Regex _invalidXMLChars = new Regex(
            @"(?<![\uD800-\uDBFF])[\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F\uFEFF\uFFFE\uFFFF]",
            RegexOptions.Compiled);

        /// <summary>
        /// removes any unusual unicode characters that can't be encoded into XML
        /// </summary>
        public static string RemoveInvalidXMLChars(string text)
        {
            if (string.IsNullOrEmpty(text)) return "";
            return _invalidXMLChars.Replace(text, "");
        }

        public static string CleanDescription(string instr)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(instr);
            return System.Convert.ToBase64String(toEncodeAsBytes);
        }

        private static bool checkIfUpdatesInActive()
        {
            return (ConfigurationManager.AppSettings["RunSearchaniseUpdates"] != null && ConfigurationManager.AppSettings["RunSearchaniseUpdates"].ToLower() == "false");
        }

    }

    public class SearchaniseSuggestionResponse
    {
        public int totalItems { get; set; }
        public List<string> suggestions { get; set; }
        public List<SearchaniseSuggestionProduct> items {get;set;}
    }

    public class SearchaniseSuggestionProduct
    {
        public int product_id {get;set;}
    }
}