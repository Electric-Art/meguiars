﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.IO;
using System.Configuration;

namespace Kawa.Models.Services
{
    public class EmailSendService
    {
        public static void Send(MailMessage msg, bool forcePropperRecipient = false)
        {
            try
            {
                if (ConfigurationManager.AppSettings["Environment"] != "Production" && !forcePropperRecipient)
                {
                    var origionalTos = msg.To.Select(x => x.Address).ToList();
                    var origionalCCs = msg.CC.Select(x => x.Address).ToList();
                    var origionalBCCs = msg.Bcc.Select(x => x.Address).ToList();
                    msg.To.Clear();
                    msg.CC.Clear();
                    msg.Bcc.Clear();
                    msg.To.Add(ConfigurationManager.AppSettings["TestingToEmail"]);
                    msg.Body += string.Format("#TOs({0})#CCs({1})#BCCs({2})", string.Join(",", origionalTos), string.Join(",", origionalCCs), string.Join(",", origionalBCCs));
                }
                if(msg.Body.Contains("MEGUIARSERROR"))
                {
                    msg.Subject = "MEGUIARSERROR IN UPDATE - " + msg.Subject;
                }
                using (var client = new SmtpClient())
                {
                    client.Send(msg);
                }
            }
            catch (Exception e) {
                string error = string.Format("Email Send Error{0}Subject - {1}{0}To - {2}{0}From - {3}{0}{4}{0}{5}", Environment.NewLine, msg.Subject, msg.To, msg.From, e.Message, e.StackTrace);
                LogService.WriteError(error);
            }
        }
    }

    public class LogService
    {
        static string separator = "=================================================================";
        static string log = string.Empty;

        public static void WriteError(string error)
        {
            System.IO.File.AppendAllText(getLogFullPath(), separator + Environment.NewLine + DateTime.UtcNow.ToLocalTime() + Environment.NewLine + error + Environment.NewLine + separator + Environment.NewLine);
        }

        private static string getLogFullPath()
        {
            var logDir = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/Content/SiteResources/Log"));
            if (!logDir.Exists)
                logDir.Create();

            var time = DateTime.UtcNow.ToLocalTime();
            var filename = string.Format("Log {0}-{1}-{2}.log", time.Year, time.Month.ToString("00"), time.Day.ToString("00"));
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/SiteResources/Log"), filename);
        }

    }
}