﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using System.Runtime.Caching;

namespace Kawa.Models.Services
{
    public class ShippingService
    {
        private ProductRepository _productRep;
        private ShippingRepository _shipRep;
        private OrderRepository _orderRep;
        private static MemoryCache _cache = MemoryCache.Default;

        private Location location;

        public ShippingService(ProductRepository productRep, ShippingRepository shipRep, OrderRepository orderRep)
        {
            _productRep = productRep;
            _shipRep = shipRep;
            _orderRep = orderRep;
        }

        public bool isBlockedIP(string strIPAddress)
        {
            if (strIPAddress == null) return false;
            var country = getCountryByIp(strIPAddress);
            if (country == null) return false;
            return country.isBlocked;
        }

        public Country getCountryByIp(string strIPAddress)
        {
            if (strIPAddress == "::1" || strIPAddress == "127.0.0.1")///localhost settings
            {
                strIPAddress = "125.236.238.143";
            }
            string[] ip = strIPAddress.Split('.');
            try
            {
                decimal a = int.Parse(ip[0]) * 256 * 256 * 256;
                decimal b = int.Parse(ip[1]) * 256 * 256;
                decimal c = int.Parse(ip[2]) * 256;
                decimal d = int.Parse(ip[3]);
                decimal ipnumber = a + b + c + d;

                int ipx = int.Parse(ipnumber.ToString());
                var result = _shipRep.lookupIP(ipx).ToList();
                if (result.Count() != 0 && !string.IsNullOrEmpty(result.First()))
                {
                    return _shipRep.listAllCountries().SingleOrDefault(x => x.Code == result.First());
                }
            }
            catch
            {
                return null;
            }

            return null;
        }

        public static Country getDefaultCountry()
        {
            if (!_cache.Contains("DefaultCountry"))
            {
                CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(1);
                _cache.Set("DefaultCountry", new ShippingRepository().listAllCountries().Single(x => x.Name == "New Zealand"), cacheItemPolicy);
            }
            return _cache.Get("DefaultCountry") as Country;
        }

        public List<Country> getAllowedCountries()
        {
            return _shipRep.listAllCountries().Where(x => !x.isHidden).OrderBy(x => x.Name).ToList();
        }

        public List<State> getAllowedStates()
        {
            return _shipRep.listAllStates().Where(x => !x.isHidden).OrderBy(x => x.Name).ToList();
        }

        public List<ShippingMethod> getShippingMethods(Order order)
        {
            var rates = getRatesForCountryOrDefault(order.CountryID, order.StateID, order.Postcode);
            if (rates.Any())
                return _shipRep.listAllShippingMethods().Where(x => rates.Select(y => y.ShippingMethodID).Contains(x.ID)).ToList();
            else
                return new List<ShippingMethod>();
        }

        public void populateLists(Order order)
        {
            order.allowedCountries = _shipRep.listAllCountries().ToList();
            order.allowedStates = _shipRep.listAllStates().ToList();
            order.allowedShippingMethods = _shipRep.listAllShippingMethods().ToList();
        }

        public void populateShippingDomain(Order order)
        {
            if (order.CountryID.HasValue)
                order.country = _shipRep.listAllCountries().SingleOrDefault(x => x.ID == order.CountryID);
            if (order.StateID.HasValue)
                order.state = _shipRep.listAllStates().SingleOrDefault(x => x.ID == order.StateID);
            if (order.ShippingMethodID.HasValue)
                order.shippingMethod = _shipRep.listAllShippingMethods().SingleOrDefault(x => x.ID == order.ShippingMethodID);
        }

        public ShippingComponent getShippingComponent(Order order, OrderService orderService)
        {
            order.allowedCountries = getAllowedCountries();
            order.allowedStates = getAllowedStates();

            if (order.StateID.HasValue && order.allowedStates.Single(x => x.ID == order.StateID).CountryID != order.CountryID)
            { 
                order.StateID = null;
                if (order.ID != 0)
                    _orderRep.saveOrder(order);
            }
            var possibleRates = getRatesForCountryOrDefault(order.CountryID, order.StateID, order.Postcode);
            order.allowedShippingMethods = getShippingMethods(order);
            if (!order.allowedShippingMethods.Any())
            {
                return null;
            }
            if(order.ShippingMethodID.HasValue)
                order.shippingMethod = order.allowedShippingMethods.SingleOrDefault(x => x.ID==order.ShippingMethodID) ?? order.allowedShippingMethods.First();
            else
                order.shippingMethod = order.allowedShippingMethods.First();

            var rates = possibleRates.Where(x => x.ShippingMethodID == order.shippingMethod.ID);
            if (location != null && rates.Any(x => x.LocationID == location.ID))
                rates = rates.Where(x => x.LocationID == location.ID);

            var orderWeight = order.orderItems.Sum(x => (x.Variant.weight ?? 0) * (decimal)x.Quantity);
            var rate = rates.OrderBy(x => x.WeightDependency).FirstOrDefault(x => x.WeightDependency >= orderWeight) ?? rates.OrderBy(x => x.WeightDependency).Last();

            if(!order.IsCalculationOrder)
            {
                calcShippingOptionPrices(order, orderService);
            }

            return new ShippingComponent()
            {
                rate = rate,
                location = location,
                amount = rate.Rate
            };
        }

        public void calcShippingOptionPrices(Order order, OrderService orderService)
        {
            order.allowedShippingMethods.ForEach(x =>
            {
                x.OrderForPrice = order.CloneForCalc();
                x.OrderForPrice.ShippingMethodID = x.ID;
                orderService.loadItemedOrder(x.OrderForPrice, true);
            });
        }

        public decimal? getFreeShippingEquivalentCost(int? CountryID, int? StateID, int? Postcode)
        {
            if (!getRatesForCountryOrDefault(CountryID, StateID, Postcode).Where(x => x.ShippingMethodID == 1).Any())
                return null;
            return getRatesForCountryOrDefault(CountryID, StateID, Postcode).Where(x => x.ShippingMethodID == 1).OrderBy(x => x.Rate).First().Rate;
        }

        private List<ShippingRate> getRatesForCountryOrDefault(int? CountryID, int? StateID, int? Postcode)
        {
            if(CountryID.HasValue && getDefaultCountry() != null && CountryID == getDefaultCountry().ID && !StateID.HasValue && !Postcode.HasValue)
            {
                if (!_cache.Contains("DefaultCountryLocation"))
                {
                    CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
                    cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(1);
                    _cache.Set("DefaultCountryLocation", _shipRep.listAllLocationsForCountry((int)CountryID).FirstOrDefault(x => !x.isHidden), cacheItemPolicy);
                }
                location = _cache.Get("DefaultCountryLocation") as Location;
            }
            else
                location = CountryID.HasValue ? _shipRep.listAllLocationsForCountry((int)CountryID).FirstOrDefault(x => !x.isHidden) : null;
            if (StateID.HasValue)
                location = _shipRep.listAllLocationsForState((int)StateID).FirstOrDefault(x => !x.isHidden) ?? location;
            if (StateID.HasValue && Postcode.HasValue)
                location = _shipRep.listAllLocationsForPostCode((int)StateID, (int)Postcode).FirstOrDefault(x => !x.isHidden) ?? location;
            List<ShippingRate> rates = new List<ShippingRate>();
            if (location != null)
            {
                rates.AddRange(_shipRep.listAllShippingRates().Where(x => x.LocationID == location.ID).ToList());
            }
            rates.AddRange(_shipRep.listAllShippingRates().Where(x => !x.LocationID.HasValue).ToList());
            return rates;
        }

        public List<Location> getLocationsForOrder(Order order)
        {
            List<Location> locs = new List<Location>();
            if (order.CountryID.HasValue)
                locs.AddRange(_shipRep.listAllLocationsForCountry((int)order.CountryID).Where(x => !x.isHidden));
            if (order.StateID.HasValue)
                locs.AddRange(_shipRep.listAllLocationsForState((int)order.StateID).Where(x => !x.isHidden));
            if (order.StateID.HasValue && order.Postcode.HasValue)
                locs.AddRange(_shipRep.listAllLocationsForPostCode((int)order.StateID, (int)order.Postcode).Where(x => !x.isHidden));
            return locs;
        }
    }

    public class ShippingComponent
    {
        public ShippingRate rate { get; set; }
        public Location location { get; set; }
        public decimal amount { get; set; }
    }
}

namespace Kawa.Models.ViewModels
{
    public class ShippingInputs
    {
        public int? ShippingMethodID { get; set; }
        public int? CountryID { get; set; }
        public int? StateID { get; set; }
        public int? Postcode { get; set; }
        public decimal? AAFT { get; set; }
    }

}