﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace Kawa.Models.Services
{
    public class CaptchaService
    {
        public CaptchaResponse CheckCaptyaSuccess(string response)
        {
            //secret that was generated in key value pair
            const string secret = "6Le8bdIhAAAAAOtnlpGdMIzqvuug07lmI7Lo1fH5";

            var client = new WebClient();
            var reply =
                client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0)
                {
                    captchaResponse.Message = "There was a problem with the CAPTYA";
                    return captchaResponse;
                }

                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        captchaResponse.Message = "The secret parameter is missing.";
                        break;
                    case ("invalid-input-secret"):
                        captchaResponse.Message = "The secret parameter is invalid or malformed.";
                        break;

                    case ("missing-input-response"):
                        captchaResponse.Message = "Please fill in the reCAPTCHA.";
                        break;
                    case ("invalid-input-response"):
                        captchaResponse.Message = "The response parameter is invalid or malformed.";
                        break;

                    default:
                        captchaResponse.Message = "Error occured. Please try again";
                        break;
                }
            }
            return captchaResponse;
        }

        public class CaptchaResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("error-codes")]
            public List<string> ErrorCodes { get; set; }

            public string Message;
        }
    }
}