﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using Kawa.Models;
using Kawa.Models.ViewModels;

namespace Kawa.Models.Services
{
    public class PortService
    {
        private PortRepository _portRepo;

        private PortRepository repo
        {
            get
            {
                if (_portRepo == null)
                    _portRepo = new PortRepository();
                return _portRepo;
            }
        }

        public bool CheckForPortAccountAvailable(string email)
        {
            var portUsers = repo.listAllPortAccounts().Where(x => x.email.ToLower() == email.ToLower() && !x.hasPorted).OrderByDescending(x => x.ID).ToList();
            if (portUsers.Any())
            { 
                portUsers.ForEach(x =>
                {
                    x.hasPorted = true;
                    x.portDate = DateTime.Now;
                });
                repo.savePortAccount();
            }
            return portUsers.Any();
        }

        public bool CheckForPortAccount(LoginViewModel model)
        {

            var portUsers = repo.listAllPortAccounts().Where(x => x.email.ToLower()==model.Email.ToLower() && !x.hasPorted).OrderByDescending(x => x.ID).ToList();
            if (!portUsers.Any())
                return false;
            bool hit = false;
            portUsers.ForEach(x => {
                if(CheckPassword(model.Password, x.pass, x.salt)) 
                {
                    hit = true;
                }
            });
            if(hit)
            {
                portUsers.ForEach(x =>
                {
                    x.hasPorted = true;
                    x.portDate = DateTime.Now;
                });
                repo.savePortAccount();
            }
            return hit;
        }

        public bool CheckPassword(string inPassword, string inHash, string inSalt)
        {
            var compareHash = "";
            //http://forum.cs-cart.com/topic/27668-how-to-verify-cs-cart-passwords/
            if(!string.IsNullOrEmpty(inSalt))
              compareHash = md5(md5(inPassword) + md5(inSalt));
            else
              compareHash = md5(inPassword);
           
            return inHash == compareHash;
        }

        public static string md5(string input)
        {
            byte[] asciiBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(input);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
        }

    }

   
}