﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Caching;

namespace Kawa.Models.Services
{
    public class FlagService
    {
        private OrderRepository _orderRep;
        private static MemoryCache _cache = MemoryCache.Default;

        public FlagService(OrderRepository orderRep)
        {
            _orderRep = orderRep;
        }

        public void CheckOrdersForFlags(List<Order> orders)
        {
            var flags = GetFlags();
            orders.ForEach(x => x.FlagLabels = flags.Where(y => y.triggers.Any(z =>
                checkVal(z.FirstName,x.CustomerDetails.FirstName) &&
                checkVal(z.Surname, x.CustomerDetails.Surname) &&
                checkVal(z.Email, x.CustomerDetails.Email) &&
                checkVal(z.Address1, x.DeliveryDetails.Address1) &&
                checkVal(z.Address2, x.DeliveryDetails.Address2) &&
                checkVal(z.Address3, x.DeliveryDetails.Address3) &&
                checkVal(z.Address4, x.DeliveryDetails.Address4) &&
                checkVal(z.Postcode, x.DeliveryDetails.Postcode) &&
               (!z.CountryID.HasValue || z.CountryID == x.DeliveryDetails.CountryID)
             )).SelectMany(y => y.GetLabels()).ToList());
        }

        public List<Flag> SearchFlags(FlagTrigger trigger, Flag flag)
        {
            var flags = GetFlags();
            if(!string.IsNullOrEmpty(trigger.FirstName))
                flags = flags.Where(y => y.triggers.Any(z => checkValFuzzy(z.FirstName, trigger.FirstName))).ToList();
            if (!string.IsNullOrEmpty(trigger.Surname))
                flags = flags.Where(y => y.triggers.Any(z => checkValFuzzy(z.Surname, trigger.Surname))).ToList();
            if (!string.IsNullOrEmpty(trigger.Email))
                flags = flags.Where(y => y.triggers.Any(z => checkValFuzzy(z.Email, trigger.Email))).ToList();
            if (!string.IsNullOrEmpty(trigger.Address1))
                flags = flags.Where(y => y.triggers.Any(z => checkValFuzzy(z.Address1, trigger.Address1))).ToList();
            if (!string.IsNullOrEmpty(trigger.Address2))
                flags = flags.Where(y => y.triggers.Any(z => checkValFuzzy(z.Address2, trigger.Address2))).ToList();
            if (!string.IsNullOrEmpty(trigger.Address3))
                flags = flags.Where(y => y.triggers.Any(z => checkValFuzzy(z.Address3, trigger.Address3))).ToList();
            if (!string.IsNullOrEmpty(trigger.Address4))
                flags = flags.Where(y => y.triggers.Any(z => checkValFuzzy(z.Address4, trigger.Address4))).ToList();
            if (!string.IsNullOrEmpty(trigger.Postcode))
                flags = flags.Where(y => y.triggers.Any(z => checkValFuzzy(z.Postcode, trigger.Postcode))).ToList();
            if (trigger.CountryID.HasValue)
                flags = flags.Where(y => y.triggers.Any(z => z.CountryID == trigger.CountryID)).ToList();

            if(!string.IsNullOrEmpty(flag.Labels))
            {
                flags = flags.Any() ? flags : GetFlags();
                return flags.Where(x => flag.GetLabels().Any(y => x.GetLabels().Any(z => z.Name == y.Name))).ToList();
            }

            return flags;
        }

        private bool checkVal(string triggerval, string orderval)
        {
            if(string.IsNullOrEmpty(triggerval))return true;
            return triggerval.Equals(orderval.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }

        private bool checkValFuzzy(string triggerval, string orderval)
        {
            if (string.IsNullOrEmpty(triggerval)) return false;
            return triggerval.Equals(orderval.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }

        public List<Flag> GetFlags()
        {
            if (!_cache.Contains("FlagSet"))
            {
                SetFlagsCache();
            }
            return _cache.Get("FlagSet") as List<Flag>;
        }

        public void SetFlagsCache()
        {
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddDays(1);
            _cache.Set("FlagSet", _orderRep.listAllFlags().ToList(), cacheItemPolicy);
        }
    }
}