﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.Services;
using Kawa.Models;
using Kawa.PropellaWebService;
using System.Reflection;
using Newtonsoft.Json;
using System.Net;
using Kawa.Models.Data;
using System.Data;
using Kawa.Models.Extensions;
using AutoMapper;
using Kawa.Models.Data.SmitsGroup;
using System.Net.Mail;
using System.Configuration;
using System.Data.Linq;

namespace Kawa.Models.Services
{
    public class PropellaService
    {
        public static PropellaWebService.PropellaWebServicesSoapClient webService;
        public static GPIService.PropellaWebServicesSoapClient gpi_webService;
        public enum InvoiceTypeOption
        {
            Invoices,
            CurrentOrders,
            PendingOrders
        }


        public class WebServiceDataRequest
        {
            public string Function { get; set; }
            public string[] Args { get; set; }
        }


        public static WebServiceResult GetWebServiceData(WebServiceDataRequest data)
        {
            webService = SetupPropellaService();
            object[] o = new object[] { data.Args };
            Type thistype = webService.GetType();
            MethodInfo runmethod = thistype.GetMethod(data.Function);
            WebServiceResult result = (WebServiceResult)runmethod.Invoke(webService, o);
            webService.Close();
            string extrainfo = webService.Endpoint.ListenUri.ToString() + "<br><br>" + Newtonsoft.Json.JsonConvert.SerializeObject(webService.ClientCredentials.UserName.UserName) + "<BR><br>" + Newtonsoft.Json.JsonConvert.SerializeObject(webService.Endpoint.Binding);
            EmailWSResult(data.Function, JsonConvert.SerializeObject(data.Args), JsonConvert.SerializeObject(result), extrainfo);
            return result;
        }

        public static void EmailWSResult(string call, string input, string json, string serviceurl = null)
        {
            /*
                MailMessage msg = new MailMessage(ConfigurationManager.AppSettings["NotifyEmail"], "chris@pixelle.co.nz")
                {
                    Subject = "meguiars:DATARES - " + ConfigurationManager.AppSettings["SiteUrl"] + " --- " + call,
                    IsBodyHtml = true,
                };
                msg.Body = "<pre>INPUT:" + input + "<BR><BR>RETURN:<BR>" + json + "<BR><BR></pre>URL: " + serviceurl + "<BR><BR><hr size=1>";

                EmailSendService.Send(msg);
             */

        }
        public static Kawa.GPIService.WebServiceResult GetGPIWebServiceData(WebServiceDataRequest data)
        {
            gpi_webService = SetupPropellaGPIService();
            object[] o = new object[] { data.Args };
            Type thistype = gpi_webService.GetType();
            MethodInfo runmethod = thistype.GetMethod(data.Function);
            Kawa.GPIService.WebServiceResult result = (Kawa.GPIService.WebServiceResult)runmethod.Invoke(gpi_webService, o);
            gpi_webService.Close();
            string extrainfo = gpi_webService.Endpoint.ListenUri.ToString() + "<br><br>" + Newtonsoft.Json.JsonConvert.SerializeObject(gpi_webService.ClientCredentials.UserName.UserName) + "<BR><br>" + Newtonsoft.Json.JsonConvert.SerializeObject(gpi_webService.Endpoint.Binding);


            EmailWSResult(data.Function, JsonConvert.SerializeObject(data.Args), JsonConvert.SerializeObject(result), extrainfo);
            return result;
        }


        public static PropellaWebService.PropellaWebServicesSoapClient SetupPropellaService()
        {

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };

            PropellaWebService.PropellaWebServicesSoapClient webService = new PropellaWebService.PropellaWebServicesSoapClient();
            webService.ClientCredentials.UserName.UserName = "electricart";
            webService.ClientCredentials.UserName.Password = "hKF8Mc9NLUURnVGCN2PoSRoy2Ug3Zfvm8OohTcCM2e";
            webService.Open();
            return webService;
        }

        public static GPIService.PropellaWebServicesSoapClient SetupPropellaGPIService()
        {

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            System.Net.ServicePointManager.ServerCertificateValidationCallback += delegate { return true; };

            GPIService.PropellaWebServicesSoapClient gpi_webService = new GPIService.PropellaWebServicesSoapClient();
            gpi_webService.ClientCredentials.UserName.UserName = "electricart";
            gpi_webService.ClientCredentials.UserName.Password = "hKF8Mc9NLUURnVGCN2PoSRoy2Ug3Zfvm8OohTcCM2e";
            gpi_webService.Open();
            return gpi_webService;
        }

        public bool IsPropellaOnline()
        {
            return false;
        }


        public static void saveAttempt(string function, string[] args)
        {
            using (var _db = new dbDataContext())
            {
                WebServiceData wsd = new WebServiceData();
                wsd.Function = function;
                wsd.Args = Newtonsoft.Json.JsonConvert.SerializeObject(args);
                ///string[] deserialised = JsonConvert.DeserializeObject<string[]>(args);
                wsd.Status = 0;
                wsd.CreateDate = System.DateTime.Now;
                _db.WebServiceDatas.InsertOnSubmit(wsd);
                _db.SubmitChanges();
            }
        }

        public static string SendWebServiceData(WebServiceData wsd)
        {

            try
            {
                string[] args = JsonConvert.DeserializeObject<string[]>(wsd.Args);
                ///object[] o = new object[] { args };

                ///Type thistype = webService.GetType();
                ////MethodInfo runmethod = thistype.GetMethod(wsd.Function);

                var result = GetWebServiceData(new WebServiceDataRequest() { Args = args, Function = wsd.Function });

                /*
                 * WebServiceResult result = (WebServiceResult)runmethod.Invoke(webService, o);
                wsd.Status = 1;
                wsd.Result = result.ResultMessage;
                wsd.SentDate = System.DateTime.Now; */

                return "ID:" + wsd.ID + " sent";
            }
            catch
            {
                wsd.Status = 0;
                wsd.Result = "Error: Unable to send at this time " + System.DateTime.Now.ToString();

                return "ID:" + wsd.ID + " - " + wsd.Result;
            }


        }
        
        public static string suckNEWSmitsData()
        {


            
            string smitsNEWURL = ConfigurationManager.AppSettings["SmitsBaseUrl"] + "/SiteResources/";
            string smitsNEWFiles = "PRODUCT/";

            using (var _db = new dbDataContext())
            {

                List<Models.Data.Product> products = _db.Products.ToList();

                using (var _dbs = new SmitsDataContext())
                {

                    DateTime started = System.DateTime.Now;
                    List<Models.Data.SmitsGroup.Product> smitsProducts = _dbs.Products.ToList();
                    List<Models.Data.SmitsGroup.SiteResource> smitsFiles = _dbs.SiteResources.ToList();
                    TimeSpan datacollection = DateTime.Now.Subtract(started);
                    int changeCount = 0;
                    string extrainfo = "";
                    products.ForEach(p =>
                    {

                        bool changes = false;
                        string tds = "";
                        string msds = "";

                        Models.Data.SmitsGroup.Product sp = smitsProducts.Where(s => s.sku == p.sku).FirstOrDefault();
                        if (sp != null)
                        {
                            List<Models.Data.SmitsGroup.SiteResource> resources = smitsFiles.Where(s => s.ProductID == sp.ID).OrderBy(sr => sr.orderNo).ToList();

                            if (sp != null)
                            {
                                ///sync the data
                                
                                if(sp.description!=null)
                                    p.directions = sp.description.Replace("src=\"../../../","src=\"https://smitsgroup.co.nz/");

                                ///p.StockMessage = Smits_rep.GetStockMessages(sp.ID);
                                changes = true;
                                changeCount++;
                            }

                            if (resources.Count != 0)
                            {
                                string productFileLocation = smitsNEWURL + smitsNEWFiles + sp.ID + "/";
                                List<string> productimages = new List<string>();

                                Models.Data.SmitsGroup.SiteResource mainimage = resources.Where(file => file.SiteResourceType == 0).OrderBy(file => file.orderNo).FirstOrDefault();
                                if (mainimage != null)
                                {

                                    p.sHero = productFileLocation + mainimage.FileName;
                                    string newFileName = mainimage.FileName.Replace(".jpg", "_ext.jpg");
                                    if (mainimage.FileName.Contains("@2x"))
                                    {
                                        newFileName = mainimage.FileName.Replace("@2x.jpg", "_ext@2x.jpg");
                                    }

                                    p.sThumb = productFileLocation + newFileName;
                                }

                                foreach (var f in resources)
                                {
                                    ///sort attachments 0=Product Image, 15=MSDS,16=TDS,
                                    if (f.SiteResourceType == 0)
                                    {

                                        productimages.Add(productFileLocation + f.FileName);
                                    }
                                    if (f.SiteResourceType == 16)
                                    {
                                        tds += "<div class='msds'><a target='_new' class=tiny href='" + productFileLocation + f.FileName + "'><img src='/Content/images/download-icon.png' align=left><div class='msdslabel'> Technical Data Sheet </div></a></div>";
                                    }

                                    if (f.SiteResourceType == 15)
                                    {
                                        msds += "<div class='msds'><a target='_new' class=tiny href='" + productFileLocation + f.FileName + "'><img src='/Content/images/download-icon.png' align=left><div class='msdslabel'> Material Safety Data Sheet </div></a></div>";
                      
                                    }
                                    changeCount++;

                                }
                                p.metadescription = sp.metadescription;
                                p.metakeywords = sp.metakeywords;
                                p.sFiles =  tds + msds;
                                p.sHero = Newtonsoft.Json.JsonConvert.SerializeObject(productimages);
                                changes = true;
                                _db.SubmitChanges();
                            }
                        }
                        else
                        {
                            //extrainfo = extrainfo + "<br> - unable to find " + p.SKU;
                        }

                    });
                    TimeSpan span = DateTime.Now.Subtract(started);
                    double perproduct = span.TotalMilliseconds / products.Count();

                    

                    return "processed " + products.Count() + " products made " + changeCount + " changes - it took " + span.TotalSeconds.ToString("f") + " seconds total | " + perproduct.ToString("f") + " ms per item | " + datacollection.TotalSeconds.ToString("f") + " seconds to collect the data<p>" + extrainfo;
                   
                }

            }
        }


        public static string UpdateCategories()
        {
            using (var _db = new dbDataContext())
            {


                string[] args = new string[] { "ProdGrouping=CAT", "ProdGrouping=catgrp", "ProdGrouping=disccat" };
                var result = GetWebServiceData(new WebServiceDataRequest() { Args = args, Function = "Get_Product_Groupings" });
                ////webService.Get_Product_Groupings();

                var updateCats = new List<Models.Data.Tag>();
                foreach (DataRow dr in result.ResultData.Tables[0].Rows)
                {

                    int type = 0;
                    if (dr[0].ToString() == "DISCCAT")
                        type = 4;
                    else
                        type = 1;

                    updateCats.Add(new Models.Data.Tag()
                    {
                        oldID = int.Parse(dr[1].ToString()),
                        name = dr[3].ToString(),
                        ///Code = dr[2].ToString(),
                        aliases = dr[0].ToString(),
                        PropParentID = int.Parse(dr[4].ToString()),
                        nameUrl = dr[3].ToString().ToSafeString(),
                        html = dr[2].ToString(),
                        Type = type
                        
                    });

                }

                var toDel = _db.Tags.ToList().Where(x => x.SystemTag == null).Where(x => !updateCats.Any(y => y.oldID == x.oldID && y.aliases == x.aliases) && x.Type!= 7);

                //any tags being delted need relationships cleared.
                foreach(Data.Tag t in toDel)
                {
                    _db.Tag_Products.DeleteAllOnSubmit(_db.Tag_Products.Where(x => x.TagID == t.ID));
                }

                _db.SubmitChanges();

                toDel = toDel.Where(x => x.ParentID != 37);
                _db.Tags.DeleteAllOnSubmit(toDel);

                _db.SubmitChanges();
                ContentRepository _contentRep = new ContentRepository();
                var countNew = 0;
                var countUpdates = 0;
                updateCats.ForEach(x =>
                {
                    var existing = _db.Tags.SingleOrDefault(y => y.oldID == x.oldID && y.aliases == x.aliases);
                    if (existing == null)
                    {
                        _db.Tags.InsertOnSubmit(x);
                        countNew++;
                    }
                    else
                    {
                        x.ID = existing.ID;
                        existing.name = x.name;
                        existing.oldID = x.oldID;
                        existing.aliases = x.aliases;
                        existing.nameUrl = x.nameUrl;
                        existing.html = x.html;
                        existing.Type = x.Type;
                        existing.PropParentID = x.PropParentID;

                        ///_contentRep.saveTag(existing);
                        ////Mapper.Map(x, existing);
                        countUpdates++;
                    }
                });
                _db.SubmitChanges();
                ///now we need to sort the parents.

                List<Data.Tag> alltags = _db.Tags.ToList();
                alltags.Where(x => x.PropParentID != null).ToList().ForEach(x =>
                {
                    if (x.aliases == "CATGRP")
                        x.ParentID = 34;///set departments to be the parent.

                    if (x.PropParentID > 1)
                        x.ParentID = alltags.Where(t => t.oldID == x.PropParentID).FirstOrDefault().ID;

                });

                _db.SubmitChanges();
                alltags.Where(x => x.Type ==4).ToList().ForEach(x =>
                {
                    if(x.name.StartsWith("MEGUIARS"))
                    {
                        x.ParentID = 55;
                    } else
                    {
                        x.isHidden = 1;
                    }
                    
                });
                alltags.Where(x => x.Type == 1).ToList().ForEach(x =>
                {
                    x.isHidden = 1;
                });

                _db.SubmitChanges();
                List<int> tagids = _contentRep.listAllTagsIDSForBrandsProducts(_db.Tags.Where(x => x.Type != 7).ToList()).Distinct().ToList();
                List<Models.Data.Tag> activetags = new List<Data.Tag>();

                foreach (int i in tagids)
                {
                    _db.Tags.Where(x => x.ID == i).FirstOrDefault().isHidden = 0;
                    _db.SubmitChanges();
                }

                /*
                var otherBrands = _db.Categories.SingleOrDefault(x => x.PropID == Category.OtherBrandsPropID && x.GroupingType == "DISCCAT");
                if (otherBrands == null)
                {
                    otherBrands = new Category();
                    otherBrands.Name = "Other Brands";
                    otherBrands.Code = "zzzzzz101010";
                    otherBrands.PropID = Category.OtherBrandsPropID;
                    otherBrands.GroupingType = "DISCCAT";
                    _db.Categories.InsertOnSubmit(otherBrands);
                    countNew++;
                }*/

                _db.SubmitChanges();



                return string.Format("Categories -> {0} inserted, {1} updated, {2} deleted", countNew, countUpdates, toDel.Count());
            }
        }


        public static bool isMeguiars(string code)
        {
            List<string> validcodes = new List<string>() { "1428", "1429", "1446" };
            return validcodes.Contains(code);
        }
        public static string UpdateOutOfStock()
        {
            string error = "";
            int countUpdates = 0;
            using (var _db = new dbDataContext())
            {
                _db.Variants.Where(xx => xx.isHidden == 1).ToList().ForEach(x =>
                 {
                     x.isHidden = 0;
                 });
                _db.SubmitChanges();

                foreach (LMCOutOfStockSKU outofstocksku in _db.LMCOutOfStockSKUs)
                {
                    Data.Product p = _db.Products.Where(x => x.sku == outofstocksku.SKU).FirstOrDefault();
                    if(p!=null)
                    { 
                    Data.Variant v = _db.Variants.Where(x => x.ProductID == p.ID).FirstOrDefault();
                        if(v!=null)
                        { 
                            if(outofstocksku.OutOfStock.HasValue)
                                if(outofstocksku.OutOfStock == 1)
                                    v.isHidden = 1;
                        }

                        countUpdates++;
                    }
                }
                _db.SubmitChanges();
            }
            
            return string.Format("Products Set Out of Stock -> {0} set out of stock", countUpdates);

        }
        public static string UpdateProducts(int? days)
        {
            using (var _db = new dbDataContext())
            {

                string error = "";


                List<Models.Data.Variant> variants = _db.Variants.ToList();

                string[] args;
                if (days.HasValue)
                {
                    DateTime lastUpdate = DateTime.UtcNow.UtcToLocal().AddDays(-1);
                    lastUpdate = DateTime.Now.UtcToLocal().AddDays(-(int)days);
                    args = new string[] { "LastModified=" + (lastUpdate.Day + "/" + lastUpdate.Month + "/" + lastUpdate.Year) };
                }
                else
                    args = new string[] { };

                var result = GetWebServiceData(new WebServiceDataRequest() { Args = args, Function = "Get_Products" });
                ///webService.Get_Products(args);

                var updateProds = new List<Models.Data.Product>();
                DataRowCollection validrows = result.ResultData.Tables[0].Rows;
                foreach (DataRow dr in validrows)
                {
                    if (dr[3] is DBNull || dr[4] is DBNull)
                        continue;

                    ///if (!isMeguiars(dr[8].ToString()))
                        ////continue;
                    
                    ///dr["Obsolete"] = 0;

                    Models.Data.Product newprod = new Models.Data.Product()
                    {
                        oldID = int.Parse(dr[0].ToString()),
                        sku = dr[1].ToString().Trim(),
                        name = dr[2].ToString(),
                        nameUrl = dr[2].ToString().ToSafeString(),
                        createDate = DateTime.Now,
                        modifiedDate = DateTime.Now,
                        price = decimal.Parse(dr["Price1"].ToString()),
                        ///Description = dr[2].ToString(),
                        ///Rate1 = decimal.Parse(dr[3].ToString()),
                        ///Rate2 = decimal.Parse(dr[4].ToString()),
                        ///PropCategoryID = int.Parse(dr[5].ToString()),
                        ///PropCatGrpID = int.Parse(dr[6].ToString()),
                        ///DivisionID = int.Parse(dr[7].ToString()),
                        ///PropDiscCatID = ValidateBrand(int.Parse(dr[8].ToString()), _db),
                        ///DiscCatID = int.Parse(dr[8].ToString()),
                        ///SClassID = int.Parse(dr[9].ToString()),
                        ///SourceID = int.Parse(dr[10].ToString()),
                        isHidden = (int.Parse(dr["Obsolete"].ToString()) == 0 ? 0 : 1),
                        ////DisabledPrice = (int)dr["DisablePrice"]
                    };

                    if (newprod.sku != "PCNZ DONATION")             
                    { 
                        double incgst = double.Parse(newprod.price.ToString()) * 1.15;
                        newprod.price = decimal.Round(decimal.Parse(incgst.ToString()), 2, MidpointRounding.AwayFromZero);
                    }

                    updateProds.Add(newprod);
                }
                int delcount = 0;
                var delPropProductIDs = updateProds.Where(x => x.isHidden==1).Select(x => x.oldID);
                delcount = delPropProductIDs.Count();
                if (delPropProductIDs.Count() > 1000)
                {
                    var toDel = _db.Products.Where(p => delPropProductIDs.Take(1000).Contains(p.ID));
                    _db.Products.DeleteAllOnSubmit(toDel);

                    _db.SubmitChanges();
                    if (delPropProductIDs.Count() > 2000)
                    {
                        toDel = _db.Products.Where(p => delPropProductIDs.Skip(1000).Take(1000).Contains(p.ID));
                        _db.Products.DeleteAllOnSubmit(toDel);

                        _db.SubmitChanges();
                    }
                    if (delPropProductIDs.Count() > 3000)
                    {
                        toDel = _db.Products.Where(p => delPropProductIDs.Skip(2000).Take(1000).Contains(p.ID));
                        _db.Products.DeleteAllOnSubmit(toDel);
                        _db.SubmitChanges();
                    }
                    if (delPropProductIDs.Count() > 4000)
                    {
                        toDel = _db.Products.Where(p => delPropProductIDs.Skip(3000).Take(1000).Contains(p.ID));
                        _db.Products.DeleteAllOnSubmit(toDel);
                        _db.SubmitChanges();
                    }
                    if (delPropProductIDs.Count() > 5000)
                    {
                        toDel = _db.Products.Where(p => delPropProductIDs.Skip(4000).Take(1000).Contains(p.ID));
                        _db.Products.DeleteAllOnSubmit(toDel);
                        _db.SubmitChanges();
                    }
                    if (delPropProductIDs.Count() > 6000)
                    {
                        toDel = _db.Products.Where(p => delPropProductIDs.Skip(5000).Take(1000).Contains(p.ID));
                        _db.Products.DeleteAllOnSubmit(toDel);
                        _db.SubmitChanges();
                    }
                    if (delPropProductIDs.Count() > 7000)
                    {
                        toDel = _db.Products.Where(p => delPropProductIDs.Skip(6000).Take(1000).Contains(p.ID));
                        _db.Products.DeleteAllOnSubmit(toDel);
                        _db.SubmitChanges();
                    }
                    if (delPropProductIDs.Count() > 8000)
                    {
                        toDel = _db.Products.Where(p => delPropProductIDs.Skip(7000).Take(1000).Contains(p.ID));
                        _db.Products.DeleteAllOnSubmit(toDel);
                        _db.SubmitChanges();
                    }
                }
                else
                {
                    var toDel = _db.Products.Where(p => delPropProductIDs.Contains(p.ID));
                    _db.Products.DeleteAllOnSubmit(toDel);
                }
                

                var countNew = 0;
                var countUpdates = 0;
                updateProds.ForEach(x =>
                {
                    var existing = _db.Products.SingleOrDefault(y => y.oldID == x.oldID);
                    if (existing == null)
                    {
                        _db.Products.InsertOnSubmit(x);
                        countNew++;
                    }
                    else
                    {

                        existing.sku = x.sku;
                        existing.name = x.name;
                        existing.nameUrl = x.nameUrl;
                        existing.isHidden = int.Parse(x.isHidden.ToString());
                        existing.price = x.price;
                        countUpdates++;
                    }


                });

                _db.SubmitChanges();
                _db.Products.ToList().ForEach(x =>
                {
                    Models.Data.Variant v = variants.Where(var => var.ProductID == x.ID).FirstOrDefault();
                    try
                    {
                        if (v == null)
                        {
                            ///need to sort variants here....
                            v = new Data.Variant();
                            v.ProductID = x.ID;
                            v.name = x.name;
                            v.sku = x.sku;
                            v.priceRRP = 0;
                            v.isHidden = x.isHidden;
                            v.price = (decimal)x.price;
                            v.modifiedDate = System.DateTime.Now;
                            v.createDate = System.DateTime.Now;
                            if (x.sku == "PCNZ DONATION")
                            {
                                v.noTax = true;
                            }
                            _db.Variants.InsertOnSubmit(v);
                        }
                        else
                        {
                            if (x.price == null)
                            { x.price = 0; x.isHidden = 1; }
                                

                            if(x.sku == "PCNZ DONATION")
                            {
                                v.noTax = true;
                            }
                            v.name = x.name;
                            v.sku = x.sku;
                            v.isHidden = x.isHidden;
                            v.price = (decimal)x.price;
                            v.priceRRP = 0;
                            v.modifiedDate = System.DateTime.Now;
                            v.price = (decimal)x.price;
                        }
                    }
                    catch (Exception ex)
                    {
                        error += ex.Message + x.name + v.ID;
                    }
                 });


                _db.SubmitChanges();

                foreach (DataRow dr in result.ResultData.Tables[0].Rows)
                {

                    if (dr[3] is DBNull || dr[4] is DBNull)
                        continue;

                    if (!isMeguiars(dr[8].ToString()))
                        continue;

                    int oldid = int.Parse(dr[0].ToString());
                    var existing = _db.Products.SingleOrDefault(y => y.oldID == oldid);
                    List<Tag_Product> mytags = _db.Tag_Products.Where(x => x.ProductID == existing.ID).ToList();

                    int brandoldID = int.Parse(dr[8].ToString());
                    Models.Data.Tag brand = _db.Tags.Where(x => x.oldID == brandoldID && x.aliases == "DISCCAT").FirstOrDefault();

                    if (brand != null && brand.isHidden ==0)///only process products which are meguiars brands, other brands are hidden
                    {
                        if (mytags.Where(x => x.TagID == brand.ID).Count() == 0)
                        {
                            _db.Tag_Products.DeleteAllOnSubmit(mytags.Where(x => x.Tag.aliases == "DISCCAT"));
                            _db.SubmitChanges();
                            _db.Tag_Products.InsertOnSubmit(new Tag_Product() { TagID = brand.ID, ProductID = existing.ID });
                        }

                        int oldcatid = int.Parse(dr[5].ToString());
                        Models.Data.Tag category = _db.Tags.Where(x => x.oldID == oldcatid && x.aliases == "CAT").FirstOrDefault();
                        if (category != null)
                        {
                            if (mytags.Where(x => x.TagID == category.ID).Count() == 0)
                            {
                                _db.Tag_Products.DeleteAllOnSubmit(mytags.Where(x => x.Tag.aliases == "CAT"));
                                _db.SubmitChanges();
                                _db.Tag_Products.InsertOnSubmit(new Tag_Product() { TagID = category.ID, ProductID = existing.ID });
                            }
                        }

                        int oldcatgrpid = int.Parse(dr[6].ToString());
                        Models.Data.Tag categorygrp = _db.Tags.Where(x => x.oldID == oldcatgrpid && x.aliases == "CATGRP").FirstOrDefault();
                        if (categorygrp != null)
                        {
                            if (mytags.Where(x => x.TagID == categorygrp.ID).Count() == 0)
                            {
                                _db.Tag_Products.DeleteAllOnSubmit(mytags.Where(x => x.Tag.aliases == "CATGRP"));
                                _db.SubmitChanges();
                                _db.Tag_Products.InsertOnSubmit(new Tag_Product() { TagID = categorygrp.ID, ProductID = existing.ID });
                            }
                        }

                    }

                    
                }

                _db.SubmitChanges();
                List<Models.Data.Tag> validbrands = _db.Tags.Where(x => x.isHidden == 0 && x.Type == 4).ToList();
                foreach (Models.Data.Product p in _db.Products)
                {
                    List<Models.Data.Tag_Product> t = _db.Tag_Products.Where(x => validbrands.Select(b => b.ID).Contains(x.TagID) && x.ProductID == p.ID).ToList();
                    if(t.Count==0)
                    {
                        p.isHidden = 1;
                    }
                }
                _db.SubmitChanges();

                ////suckNEWSmitsData();

                return string.Format("Products -> {0} inserted, {1} updated, {2} deleted - {3}", countNew, countUpdates, delcount, error);
            }

        }

        public static string UpdatePromotions()
        {
            using (var _db = new dbDataContext())
            {

                var options = new DataLoadOptions();
                options.LoadWith<Models.Data.Product>(o => o.Variants);
                _db.LoadOptions = options;

                string[] args = new string[] { "ClassCode=Public" };

                var result = GetWebServiceData(new WebServiceDataRequest() { Args = args, Function = "Get_Promotions" });
                List<Models.Data.Product> allproducts = _db.Products.ToList();
                ///var result = webService.Get_Promotions(data);

                var updatePromos = new List<Models.Data.Promotion>();
                int specialsID = 29;/// _db.Tags.Where(x => x.name == "Trending Products").FirstOrDefault().ID;
                int clearanceID = _db.Tags.Where(x => x.name == "Clearance").FirstOrDefault().ID;

               /// _db.Tag_Products.DeleteAllOnSubmit(_db.Tag_Products.Where(x => x.TagID == 29));///clear out the old special tags.
                _db.Tag_Products.DeleteAllOnSubmit(_db.Tag_Products.Where(x => x.TagID == specialsID));///clear out the old special tags.
                _db.Tag_Products.DeleteAllOnSubmit(_db.Tag_Products.Where(x => x.TagID == clearanceID));///clear out the old clearance tags.
                _db.SubmitChanges();

                _db.Promotions.DeleteAllOnSubmit(_db.Promotions.Where(x=>x.SystemType == 2));///remove all the propella promotions.
               
                _db.SubmitChanges();


                
                List<Tag_Product> taggedproducts = new List<Tag_Product>();
                foreach (DataRow dr in result.ResultData.Tables[0].Rows)
                {
                    Models.Data.Product thisproduct = allproducts.Where(x => x.oldID == (int)dr["ProductID"]).FirstOrDefault();
                    if(thisproduct!=null)
                    {
                        string promoType = dr["PromoType"].ToString();
                        ///taggedproducts.Add(new Tag_Product() { TagID = 29, ProductID = thisproduct.ID });
                        taggedproducts.Add(new Tag_Product() { TagID = (promoType != "BARGAIN BIN" ? specialsID : clearanceID), ProductID = thisproduct.ID });

                        Models.Data.Promotion thispromo = new Models.Data.Promotion()
                        {
                            name = promoType,
                            createDate = DateTime.Now,
                            ///StartDate = DateTime.Parse(dr["startdate"].ToString()),
                            expiryDate = DateTime.Parse(dr["enddate"].ToString()),
                            //Reference = dr[3].ToString(),
                            ProductID = thisproduct.ID,
                            ///Qty = dr["qty"].ToString(),
                            isHidden=0,
                            percentOff = double.Parse(dr["discount"].ToString()),
                            ///rebatemult = decimal.Parse(dr["rebatemult"].ToString()),
                            ///rebaterate = decimal.Parse(dr["rebaterate"].ToString()),
                            ///Margin = decimal.Parse(dr["margin"].ToString()),
                            SystemType = 2///Promotion.SystemTypeOption.PROPELLAPROMO,
                        };
                        if(thispromo.percentOff== null || thispromo.percentOff ==0)
                        {
                            thispromo.amountOff = thisproduct.Variants.FirstOrDefault().price - decimal.Parse(dr["tender"].ToString());
                            thispromo.percentOff = null;
                        }
                        updatePromos.Add(thispromo);
                    }
                }
                _db.Tag_Products.InsertAllOnSubmit(taggedproducts);
                _db.Promotions.InsertAllOnSubmit(updatePromos);
                _db.SubmitChanges();


                return string.Format("Promotions -> {0} inserted.", updatePromos.Count());

            }
        }
        /*
                public static void SendCustomerSkus(string CustomerCode, Customer_SKU.TypeOption type)
                {
                    using (var _db = new dbDataContext())
                    {

                        var qlist = _db.Customer_SKUs.Where(x => x.CustomerCode == CustomerCode && x.TypeE == (int)type);

                        string xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><PropellaDocument><CustomerMask><CustomerCode>" + CustomerCode + "</CustomerCode><Reference>" + type.ToString() + "</Reference><Products>";
                        foreach (var p in qlist)
                        {
                            xml += "<Product>" + p.SKU + "</Product>";
                        }
                        xml += "</Products></CustomerMask></PropellaDocument>";


                        string[] args = new string[] { "customer=" + CustomerCode, "listxml=" + xml };
                        var result = GetWebServiceData(new WebServiceDataRequest() { Args = args, Function = "Submit_StandardOrderList" });


                    }
                }

                public static void UpdateCustomerSkus(string CustomerCode)
                {
                    using (var _db = new dbDataContext())
                    {

                        PullCustomerSkus(CustomerCode, Customer_SKU.TypeOption.Paint, true);
                        PullCustomerSkus(CustomerCode, Customer_SKU.TypeOption.Quick);
                    }
                }

                public static ProcessResult UpdateCustomerInvoices(string CustomerCode, string session)
                {
                    using (var _db = new dbDataContext())
                    {

                        try
                        {
                            string[] args = new string[] { "customer=" + CustomerCode, "CardNumber=" + HttpContext.Current.Session["CardNumber"] };
                            ///var result = webService.Get_InvoiceHistory(args);

                            var result = GetWebServiceData(new WebServiceDataRequest() { Args = args, Function = "Get_InvoiceHistory" });


                            _db.ExecuteCommand("delete from Invoice WHERE CustomerCode = {0}", CustomerCode);

                            ///_db.Invoices.DeleteAllOnSubmit(_db.Invoices.Where(x => x.CustomerCode == CustomerCode));
                            ///_db.SubmitChanges();


                            var log = "";
                            DataSet ds = result.ResultData;
                            if (ds.Tables.Count != 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    var inv = ModelInvoice(InvoiceTypeOption.Invoices, CustomerCode, session, dr, log);
                                    if (inv != null)
                                        _db.Invoices.InsertOnSubmit(inv);
                                }

                                foreach (DataRow dr in ds.Tables[1].Rows)
                                {
                                    var inv = ModelInvoice(InvoiceTypeOption.CurrentOrders, CustomerCode, session, dr, log);
                                    if (inv != null)
                                        _db.Invoices.InsertOnSubmit(inv);
                                }

                                foreach (DataRow dr in ds.Tables[2].Rows)
                                {
                                    var inv = ModelInvoice(InvoiceTypeOption.PendingOrders, CustomerCode, session, dr, log);
                                    if (inv != null)
                                        _db.Invoices.InsertOnSubmit(inv);
                                }
                            }
                            _db.SubmitChanges();
                            var invs = _db.Invoices.Where(x => x.CustomerCode == CustomerCode && x.Session == session).OrderByDescending(a => a.OrderDate).ToList().GroupBy(p => p.OrderCode).Select(g => g.First()).ToList();

                            invs.ForEach(x =>
                            {
                                x.GlobalOrderStatus = GetAllStatusForThisInvoice(x, invs.Where(a => a.OrderCode == x.OrderCode).OrderBy(a => a.Backorder).ToList());
                            });
                            _db.SubmitChanges();

                            if (!string.IsNullOrEmpty(log))
                            {
                                AlertService.SendWebServiceAlert("Trade Saver Webservice: Invoices Report for customer " + CustomerCode, log);
                                return new ProcessResult { Success = false, Issue = log };
                            }
                            else
                            {
                                //means the update went ok. so update the last update for this customer.

                                //might as well clear them as any old records are invalid anyway.
                                _db.Customer_InvoiceUpdates.DeleteAllOnSubmit(_db.Customer_InvoiceUpdates.Where(x => x.CustomerCode == CustomerCode));

                                Customer_InvoiceUpdate update = new Customer_InvoiceUpdate();
                                update.CustomerCode = CustomerCode;
                                update.Updated = System.DateTime.Now;
                                _db.Customer_InvoiceUpdates.InsertOnSubmit(update);
                                _db.SubmitChanges();
                                return new ProcessResult { Success = true };
                            }

                        }
                        catch (Exception ex)
                        {
                            return new ProcessResult { Success = false, Message = ex.Message };
                        }


                    }

                }

                public static OrderResult CompleteOrder(Order order, List<Basket> basketItems, string extra, string orderNo, Customer customer)
                {
                    //this compiles the order and sends it to propella.
                    string return_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
                    return_xml += "<PropellaDocument><POrder><Header><CustomerCode>" + customer.CustomerCode + "</CustomerCode>";
                    return_xml += "<CustomerOrder>" + EscapeXml(orderNo) + "</CustomerOrder><OrderDate>" + DateTime.Now.UtcToLocal().ToString("dd-MMM-yyyy") + "</OrderDate><PaymentType>" + order.Type + " </PaymentType>";
                    //TODO track CardHolder
                    return_xml += "<HeaderNote>" + EscapeXml(extra) + "</HeaderNote><LoyaltyCardNo>+ " + HttpContext.Current.Session["CardNumber"] + "</LoyaltyCardNo><InternalNote /></Header><Detail>";
                    int lineno = 1;
                    foreach (Basket xb in basketItems)
                    {
                        xb.Product.PrepareRates(customer);
                        return_xml += "<Lines><LineNo>" + lineno + "</LineNo><SupplierProductCode>" + EscapeXml(xb.SKU) + "</SupplierProductCode><Description>" + EscapeXml(xb.Product.Name) + "</Description><Quantity>" + xb.Quantity + "</Quantity>";
                        return_xml += "<UnitCost>" + xb.Product.GetCustomerRate(customer) + "</UnitCost><Discount></Discount><RebateRate></RebateRate>";
                        if (xb.PaintMix != null)
                        {
                            string detail = xb.PaintMix.Car + "," + xb.PaintMix.Model + "," + xb.PaintMix.ColourCode + "," + xb.PaintMix.ColourName + "," + xb.PaintMix.BrandPaintMix?.Name + "," + xb.PaintMix.CategoryPaintMix?.Name + "," + xb.PaintMix.Size;
                            return_xml += "<LineNote>" + EscapeXml(detail) + "</LineNote>";
                        }

                        return_xml += "</Lines>";
                        lineno++;
                    }

                    return_xml += "</Detail><Trailer><TailNOte /></Trailer></POrder></PropellaDocument>";
                    order.XML = return_xml;
                    //saved by the OrderService calling this function.


                    string[] inputdata = new string[] { "orderxml=" + return_xml, "Customer=" + customer.CustomerCode };
                    try
                    {

                        var result = GetWebServiceData(new WebServiceDataRequest() { Args = inputdata, Function = "Submit_Order" });

                        ///var result = webService.Submit_Order(inputdata);
                        //save XML to order object, for audit
                        //send info about paintmixes.
                        ///notify_paintmixes(basketItems, order);
                        ///return propella_wsr.ResultMessage;
                        if (result.ReturnValue == 0)
                        {
                            //sent
                            //TODO SEND CONFIRMATION EMAILS to c.Email
                            return new OrderResult()
                            {
                                Success = true,
                                Message = "Thank you - Your order has been now been submitted.<P>You will receive a confirmation email summarising your order once it has been confirmed by our customer services team.<p> Please note that our customer services team work between the hours of 8am - 5pm week days.<p>Please print a copy of the order below for your records."
                            };

                        }
                        else
                        {
                            //not sent- internal propella issue.
                            return new OrderResult()
                            {
                                Success = false,
                                Message = "Your order was not able to sent, please try again later." + result.ResultMessage + result.ReturnValue
                            };
                        }
                    }
                    catch
                    {
                        //not sent- no propella connection.
                        PropellaService.saveAttempt("Submit_Order", inputdata);
                        return new OrderResult()
                        {
                            Success = true,
                            Message = "Thank you - Your order has been now been submitted.<P>You will receive a confirmation email summarising your order once it has been confirmed by our customer services team.<p> Please note that our customer services team work between the hours of 8am - 5pm week days.<p>Please print a copy of the order below for your records."

                        };
                    }





                }

                public static OrderResult ResendOrder(int OrderID)
                {
                    using (var _db = new dbDataContext())
                    {
                        var order = _db.Orders.Single(x => x.ID == OrderID);

                        string[] inputdata = new string[] { "orderxml=" + order.XML, "Customer=" + order.CustomerCode };
                        var result = GetWebServiceData(new WebServiceDataRequest() { Args = inputdata, Function = "Submit_Order" });
                        ////var result = webService.Submit_Order(inputdata);
                        if (result.ReturnValue == 0)
                        {
                            return new OrderResult()
                            {
                                Success = true,
                                Message = "Order resent to Propella."
                            };
                        }
                        else
                        {
                            //not sent
                            return new OrderResult()
                            {
                                Success = false,
                                Message = "Order NOT resent to Propella, please try again later." + result.ResultMessage + result.ReturnValue
                            };
                        }
                    }
                }

                private static string GetAllStatusForThisInvoice(Invoice i, List<Invoice> invoices)
                {
                    string status = "";
                    invoices.Where(ix => ix.Status != null && !status.Contains(ix.Status)).ToList().ForEach(ix =>
                    {
                        if (status != "")
                        {
                            status += "/" + ix.Status;
                        }
                        else
                        {
                            status += ix.Status;
                        }
                    });
                    return status;
                }

                private static Invoice ModelInvoice(InvoiceTypeOption type, string CustomerCode, string session, DataRow dr, string log)
                {
                    try
                    {
                        Invoice p = new Invoice();
                        p.CustomerCode = CustomerCode;
                        DateTime od = DateTime.Parse(dr["OrderDate"].ToString());

                        switch (type)
                        {
                            case InvoiceTypeOption.Invoices:
                            case InvoiceTypeOption.CurrentOrders:
                                p.Status = type == InvoiceTypeOption.Invoices ? "History" : dr["status"].ToString();
                                p.OrderDate = od;
                                if (type == InvoiceTypeOption.Invoices && od > DateTime.Now.AddMonths(-1))
                                {
                                    p.Status = "Dispatched";
                                }
                                p.InvoiceDate = DateTime.Parse(dr["InvoiceDate"].ToString());
                                //p.Extension = dr["extprice"].ToString();
                                p.DespatchNumber = dr["DespatchNumber"].ToString();
                                p.DespatchLineValue = ValidateNullToZero(dr["DespatchLineValue"].ToString());
                                p.DespatchFreightValue = dr["DespatchFreightValue"].ToString();
                                p.DespatchGSTValue = dr["DespatchGSTValue"].ToString();
                                p.DespatchValue = dr["DespatchValue"].ToString();
                                p.FreightVia = dr["FreightVia"].ToString();
                                p.Shipped = dr["Shipped"].ToString();
                                p.Backorder = dr["Backorder"].ToString();
                                p.OrderCode = dr["OrderNumber"].ToString();
                                p.ConsignmentNo = dr["ConsignmentNo"].ToString();
                                p.PaymentRef = dr["PaymentRef"].ToString();
                                break;
                            case InvoiceTypeOption.PendingOrders:
                                p.OrderDate = od;
                                p.Status = "Pending";
                                p.Shipped = "0";
                                p.Backorder = "0";
                                p.OrderCode = "PENDING_" + dr["CustomerOrder"].ToString() + "_" + CustomerCode;
                                p.PaymentRef = dr["PaymentType"].ToString();
                                break;
                        }


                        p.CardHolder = dr["Cardholder"].ToString();
                        p.OrderValue = dr["OrderValue"].ToString();
                        p.CustomerOrder = dr["CustomerOrder"].ToString();
                        p.PointsTotal = dr["PointsTotal"].ToString();

                        p.LineNumber = dr["LineNumber"].ToString();
                        /// p.ProductID = int.Parse(dr["productid"].ToString());
                        p.Quantity = dr["quantity"].ToString();
                        p.UnitPrice = dr["unitprice"].ToString();
                        p.Discount = dr["Discount"].ToString();
                        //p.PointRate = dr["PointRate"].ToString(); //WAS NOT USED
                        p.PointValue = dr["PointValue"].ToString();
                        p.Extension = dr["extprice"].ToString();
                        p.LineNote = dr["LineNote"].ToString();
                        p.SKU = dr["prodno"].ToString();


                        p.Session = session;
                        return (p);
                    }
                    catch (Exception ex)
                    {
                        log += "<p>Get_CurrentOrders" + ex.Message + " (" + dr[0].ToString() + ")";
                        string msg = ex.Message;
                        foreach (DataColumn d in dr.Table.Columns)
                        {
                            msg += d.ColumnName + ":" + dr[d.ColumnName].ToString() + "<BR>";
                        }
                        log += "<p>";
                        return null;
                    }
                }
        */
        private static string ValidateNullToZero(string val)
        {
            if (val == "")
            {
                return "0";
            }
            if (val == null)
            {
                return "0";
            }

            return val;

        }
/*
        private static void PullCustomerSkus(string CustomerCode, Customer_SKU.TypeOption type, bool clearonstartup = false)
        {
            using (var _db = new dbDataContext())
            {


                string[] inputdata = new string[] { "customer=" + CustomerCode, "listname=" + type };
                var result = GetWebServiceData(new WebServiceDataRequest() { Args = inputdata, Function = "Get_StandardOrderList" });
                ///var result = webService.Get_StandardOrderList(new string[] { "customer=" + CustomerCode, "listname=" + type });
                ///
                if (clearonstartup)
                {
                    _db.Customer_SKUs.DeleteAllOnSubmit(_db.Customer_SKUs.Where(x => x.CustomerCode == CustomerCode));
                    _db.SubmitChanges();
                }
                List<string> skus = new List<string>();
                foreach (DataRow dr in result.ResultData.Tables[0].Rows)
                {
                    Customer_SKU s = new Customer_SKU();
                    s.CustomerCode = CustomerCode;
                    ///s.SKU = type.ToString();
                    s.Type = type;
                    s.SKU = dr["prodno"].ToString();
                    s.PropProductID = null;


                    ///
                    if (!skus.Contains(s.SKU))
                    {
                        _db.Customer_SKUs.InsertOnSubmit(s);
                        skus.Add(s.SKU);
                    }
                }
                _db.SubmitChanges();
            }

        }

        private static int ValidateBrand(int DISCCATID, dbDataContext _db)
        {
            ///101010 is bascially a brand, which is called NoBRAND
            if (DISCCATID == 0)
            {
                return Category.OtherBrandsPropID;
            }
            else if (DISCCATID == -1)
            {
                return Category.OtherBrandsPropID;
            }
            else
            {
                return _db.Categories.Any(x => x.PropID == DISCCATID && x.GroupingType == "DISCCAT") ? DISCCATID : Category.OtherBrandsPropID;
            }
        }
*/
        private static string EscapeXml(string s)
        {
            string xml = s;
            if (!string.IsNullOrEmpty(xml))
            {
                // replace literal values with entities
                xml = xml.Replace("&", "&amp;");
                xml = xml.Replace("&lt;", "&lt;");
                xml = xml.Replace("&gt;", "&gt;");
                xml = xml.Replace("\"", "&quot;");
                xml = xml.Replace("'", "&apos;");
            }
            return xml;
        }
    }
}