﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using Kawa.Models.Extensions;
using Kawa.Models;
using System.Xml;


namespace Kawa.Models.Services
{
    public class GoogleShoppingService
    {
        private ProductRepository _productRep;
        private ContentRepository _contentRep;

        public GoogleShoppingService(ProductRepository productRep, ContentRepository contentRep)
        {
            _productRep = productRep;
            _contentRep = contentRep;
        }
        private string ShowOffset(DateTime time, TimeZoneInfo timeZone)
        {
            DateTime convertedTime = time;
            TimeSpan offset;

            offset = timeZone.GetUtcOffset(time);
            string output = "+" + string.Format("{0:hh\\:mm}", offset);/// offset.ToString("hh:mm");/// = "+" + offset.Hours + ":" + offset.Minutes;
            return output;
          
        }
        public void BuildFullProductFeed(string savePath, string siteDomain, string feedPath, bool isBing)
        {
            XNamespace gns = "http://base.google.com/ns/1.0";

            var googleProducts = _productRep.listAllFeedProducts(isBing).ToList();
            //googleProducts = googleProducts.Where(x => x.cats.Count() > 1).Take(20).ToList();
            var tags = _contentRep.listAllTags().ToList();
            /*
            var googleProducts = googleProductsFull.Where(x => x.markertags.Any(y => y.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING)).Take(20).ToList();
            googleProducts.AddRange(googleProductsFull.Where(x => !x.markertags.Any(y => y.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING)).Take(20));
            */
            googleProducts.ForEach(x =>
            {
                if(x.image != null)
                    SiteResourceService.loadUrls(new SiteResource[]{x.image});
                x.localCatPaths = new List<string>();
                x.cats.ForEach(z =>
                {
                    string localCatPath = z.name;
                    var parentCat = tags.FirstOrDefault(y => y.ID == z.ParentID);
                    if (parentCat != null)
                        localCatPath = parentCat.name + " > " + localCatPath;
                    x.localCatPaths.Add(localCatPath);
                });
            });

            TimeZoneInfo aest = TimeZoneInfo.FindSystemTimeZoneById("AUS Eastern Standard Time");


            XDocument document = new XDocument(
                new XElement("rss",
                    new XAttribute("version", "2.0"),
                    new XAttribute(XNamespace.Xmlns + "g", gns),
                    new XElement("channel",
                        new XElement("title", "Meguiars Company Feed"),
                        new XElement("description", "Meguiars product list"),
                        new XElement("link", siteDomain + feedPath),
                        from googleProduct in googleProducts.Where(x => x.image != null)
                        select new XElement("item",
                            new XElement(gns + "id", googleProduct.ID),
                            new XElement(gns + "title", new XCData(googleProduct.name ?? "")),
                            new XElement(gns + "description", new XCData(GetValidXmlString(googleProduct.specifications ?? ""))),
                            new XElement(gns + "link", siteDomain + googleProduct.getUrl()),
                            new XElement(gns + "image_link", siteDomain + GetValidXmlString(googleProduct.image.getUrl(SiteResource.SiteResourceVersion.ITEM_FEED))),
                            new XElement(gns + "condition", "new"),
                            new XElement(gns + "availability", googleProduct.preorder ? "preorder" : "in stock"),
                            (googleProduct.preorderDate.HasValue ?
                                new XElement(gns + "availability_date", DateTime.Parse(googleProduct.preorderDate.ToString()).ToString("yyyy-MM-ddT") + "12:00" + ShowOffset(DateTime.Parse(googleProduct.preorderDate.ToString()), aest))
                             : null),
                            new XElement(gns + "price", googleProduct.price.ToString("0.00")),
                            (!string.IsNullOrEmpty(googleProduct.gtin) ?
                                new XElement(gns + "gtin", googleProduct.gtin) : null
                            ),
                            new XElement(gns + "mpn", googleProduct.sku),
                            new XElement(gns + "brand", googleProduct.brand != null ? googleProduct.brand.name : "unknown"),
                            new XElement(gns + "google_product_category", isBing ? googleProduct.bingCat : googleProduct.googleCat),
                            (googleProduct.freeShipping ?
                                new XElement(gns + "shipping",
                                    new XElement(gns + "country", "AU"),
                                    new XElement(gns + "price", "0 AUD"))
                                :
                                null
                            ),
                            from localCatPath in googleProduct.localCatPaths
                            select new XElement(gns + "product_type", localCatPath.Replace("&", "&amp;").Replace(">", "&gt;"))
                            /*
                            new XElement("item",
                                new XElement(gns + "country", ))
                             */
                            /*Brand),
                            new XElement(gns + "manufacturer", googleProduct.ProductRecommendedAttributes.Manufacturer),
                            new XElement(gns + "product_type", googleProduct.ProductRecommendedAttributes.ProductType),
                            from pmt in googleProduct.ProductOptionalAttributes.PaymentAccepteds
                            select new XElement(gns + "payment_accepted", pmt)
                            */

                            ))));

            //return new XmlActionResult(document);

            System.IO.File.WriteAllText(savePath, document.ToString(SaveOptions.None));
        }

        static string GetValidXmlString(string text)
        {
            try
            {
                XmlConvert.VerifyXmlChars(text);
                if (text.IndexOf("+ú+å") > -1)
                    text = text.Replace("+ú+å", "--");

                //WATCH OUT! There is a weird hidden character between the IndexOf("") & text.Replace("[HIDDEN CHARACTER HERE]", "");
                if (text.IndexOf("") > -1)
                    text = text.Replace("", "");
                return text;
            }
            catch
            {
                return XmlConvert.EncodeName(text);
            }
        }
    }
}