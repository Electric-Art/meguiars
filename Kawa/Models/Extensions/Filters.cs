﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Script.Serialization;
using Kawa.Controllers;
using System.Xml.Linq;
using System.Xml;
using System.Text;

namespace Kawa.Models.Extensions
{
    /*
    public class OrderIsValid : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var siteController = filterContext.Controller as ShopController;
            if (siteController == null)
            {
                throw new InvalidOperationException("It is not SiteController !!!");
            }

            var filterContext.ActionParameters["id"] = 852;

            HttpRequestBase req = filterContext.HttpContext.Request;
            HttpResponseBase res = filterContext.HttpContext.Response;

            SiteViewModel vm = new SiteViewModel();
            vm.order = siteController.getSessionOrder();
            siteController.addOrderItems(vm);

            if (vm.orderItems == null || vm.orderItems.Count == 0)
            {
                ///we have a problem
                res.Redirect("/Shop/Index?msg=Order Problem");
            }
        }
    }
     * */

    public class RequiresSSL : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase req = filterContext.HttpContext.Request;
            HttpResponseBase res = filterContext.HttpContext.Response;
            var configOn = ConfigurationManager.AppSettings["EnforceSSL"] != null && ConfigurationManager.AppSettings["EnforceSSL"].ToLower()=="true";

            //Check if we're secure or not and if we're on the local box
            if (!req.IsSecureConnection && !req.IsLocal && configOn)
            {
                var builder = new UriBuilder(req.Url)
                {
                    Scheme = Uri.UriSchemeHttps,
                    Port = 443
                };

                filterContext.Result = new RedirectResult(builder.Uri.ToString(), false);
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class RequireSiteDomain : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // The base only redirects GET, but we added HEAD as well. This avoids exceptions for bots crawling using HEAD.
            // The other requests will throw an exception to ensure the correct verbs are used. 
            // We fall back to the base method as the mvc exceptions are marked as internal. 

            var configOn = ConfigurationManager.AppSettings["EnforceDomain"] != null && ConfigurationManager.AppSettings["EnforceDomain"].ToLower() == "true";
            
            if (configOn && filterContext.HttpContext.Request.Url.Host.ToLower() != ConfigurationManager.AppSettings["ForceDomain"].ToLower())
            {
                var httpsOn = ConfigurationManager.AppSettings["EnforceSSL"] != null && ConfigurationManager.AppSettings["EnforceSSL"].ToLower() == "true";
                string url = httpsOn ? "https://" : "http://" + ConfigurationManager.AppSettings["ForceDomain"] + filterContext.HttpContext.Request.RawUrl;
                filterContext.Result = new RedirectResult(url, false);
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }

    public class JsonDateTimeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if(bindingContext.ValueProvider.GetValue(bindingContext.ModelName) == null  || bindingContext.ValueProvider.GetValue(bindingContext.ModelName).RawValue == null)
                return base.BindModel(controllerContext, bindingContext);
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).RawValue.ToString();
            if(value.StartsWith("/Date"))
            {
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                var indatestr = value.Substring(6).Replace(")/", String.Empty);
                string json = "\"\\/Date(" + indatestr + ")\\/\""; ;
                DateTime date = serializer.Deserialize<DateTime>(json);
                return date;
            }
            else
                return base.BindModel(controllerContext, bindingContext);
        }
    }

    public class IntArrayModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null || string.IsNullOrEmpty(value.AttemptedValue))
            {
                return null;
            }

            return value
                .AttemptedValue
                .Split(',')
                .Select(int.Parse)
                .ToArray();
        }
    }

    public sealed class XmlActionResult : ActionResult
    {
        private readonly XDocument _document;

        public Formatting Formatting { get; set; }
        public string MimeType { get; set; }

        public XmlActionResult(XDocument document)
        {
            if (document == null)
                throw new ArgumentNullException("document");

            _document = document;

            // Default values
            MimeType = "text/xml";
            Formatting = Formatting.None;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.Clear();
            context.HttpContext.Response.ContentType = MimeType;

            using (var writer = new XmlTextWriter(context.HttpContext.Response.OutputStream, Encoding.UTF8) { Formatting = Formatting })
                _document.WriteTo(writer);
        }
    }
}