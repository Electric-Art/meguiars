﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Text;
using System.Globalization;
using System.Threading;

namespace Kawa.Models.Extensions
{
    public static class HtmlExtensions
    {

        

        public static List<SelectListItem> ToSelectList<T>(
                                        this IEnumerable<T> enumerable,
                                        Func<T, string> text,
                                        Func<T, string> value,
                                        string defaultOption)
        {
            var items = enumerable.Select(f => new SelectListItem()
            {
                Text = text(f),
                Value = value(f)
            }).ToList();
            items.Insert(0, new SelectListItem()
            {
                Text = defaultOption,
                Value = ""
            });
            return items;
        }

        public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = e, Name = textInfo.ToTitleCase(e.ToString().ToLower().Replace("_", " ")) };

            return new SelectList(values, "Id", "Name", enumObj);
        }

        public static SelectList ToSelectListChoose<TEnum>(this TEnum enumObj, string chooseText = "choose one")
        {
            var chooseVal = new List<Object>();
            chooseVal.Add(new { Value = "", Text = chooseText });

            var type = typeof(TEnum);
            var list = EnumHelper.GetSelectList(type);
            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                list.Remove(list.First());

            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
            TextInfo textInfo = cultureInfo.TextInfo;

            /*
            var test = typeof(Nullable<>).MakeGenericType(typeof(TEnum));
            var ty = GetNullableType(typeof(TEnum));
            var values = from TEnum e in Enum.GetValues(ty)
                         select new { Id = e, Name = textInfo.ToTitleCase(e.ToString().ToLower().Replace("_", " ")) };
            */
            foreach (var item in list)
                item.Text = textInfo.ToTitleCase(item.Text.ToString().ToLower().Replace("_", " "));



            return new SelectList(chooseVal.Union(list), "Value", "Text", enumObj);
        }

        public static string ToJS<T>(this T enumObj)
        {
            Type enumType = typeof(T);

            // Can't use type constraints on value types, so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            Array enumValArray = Enum.GetValues(enumType);

            List<T> enumValList = new List<T>(enumValArray.Length);

            var jsobj = "[";
            foreach (int val in enumValArray)
            {
                jsobj += "{Name:'";
                jsobj += Enum.GetName(enumType, val) + "',ID:";
                jsobj += val;
                jsobj += "},";
            }
            jsobj = jsobj.TrimEnd(',');
            jsobj += "]";
            return jsobj;
        }



        public static Type GetNullableType(Type t)
        {
            if (t.IsValueType)
            {
                return typeof(Nullable<>).MakeGenericType(t);
            }
            else
            {
                return (t);
            }
        }
    }
}