﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Configuration;

namespace Kawa.Models.Extensions
{
    public class ModelTransformations
    {

        public static string EncodeStringToUrl(string title, bool allowUnderscores = false, bool replaceUnderscores = false, int maxlen = 80)
        {

            // make it all lower case
            title = title.ToLower();
            // remove entities
            title = Regex.Replace(title, @"&\w+;", "");

            if (replaceUnderscores)
                title = title.Replace('_', '-');
            // remove anything that is not letters, numbers, dash, or space
            if (allowUnderscores)
                title = Regex.Replace(title, @"[^a-z0-9_\-\s]", "");
            else
                title = Regex.Replace(title, @"[^a-z0-9\-\s]", "");
            // replace spaces
            title = title.Replace(' ', '-');
            // collapse dashes
            title = Regex.Replace(title, @"-{2,}", "-");
            // trim excessive dashes at the beginning
            title = title.TrimStart(new[] { '-' });
            // if it's too long, clip it
            if (title.Length > maxlen)
                title = title.Substring(0, maxlen-1);
            // remove trailing dashes
            title = title.TrimEnd(new[] { '-' });
            return title;
        }

        public static string GetSiteUrl()
        {
            if (HttpContext.Current == null)
            {
                return ConfigurationManager.AppSettings["SiteUrl"] ?? "https://www.discountvitaminsexpress.com.au/";
            }
            try
            {
                var domain = (HttpContext.Current.Request.IsSecureConnection ? "https" : "http") + "://" + HttpContext.Current.Request.Url.Host.ToLower();
                if (HttpContext.Current.Request.Url.Host == "localhost")
                    domain += ":" + HttpContext.Current.Request.Url.Port;
                return domain;
            }
            catch
            {
                return ConfigurationManager.AppSettings["SiteUrl"] ?? "https://www.discountvitaminsexpress.com.au/";
            }
        }

        public static bool IsLive()
        {
            return !(ConfigurationManager.AppSettings["IsStaging"] != null && ConfigurationManager.AppSettings["IsStaging"] == "true");
        }
    }

    public static class DateTimeExtensions
    {
        public static DateTime UtcToLocal(this DateTime source)
        {
            TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
            source = DateTime.SpecifyKind(source, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeFromUtc(source, localTimeZone);
        }

        public static DateTime? UtcToLocal(this DateTime? source)
        {
            if (!source.HasValue) return null;
            return ((DateTime)source).UtcToLocal();
        }

        public static DateTime LocalToUtc(this DateTime source)
        {
            TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById("New Zealand Standard Time");
            source = DateTime.SpecifyKind(source, DateTimeKind.Unspecified);
            return TimeZoneInfo.ConvertTimeToUtc(source, localTimeZone);
        }

        public static DateTime? LocalToUtc(this DateTime? source)
        {
            if (!source.HasValue) return null;
            return ((DateTime)source).LocalToUtc();
        }
    }

    public static class Extensions
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        public static string ToSafeString(this string str, bool allowUnderscores = false, bool replaceUnderscores = false, int maxsize=80)
        {
            return ModelTransformations.EncodeStringToUrl(str, allowUnderscores, replaceUnderscores, maxsize);
        }

        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string GetDisplayName<TModel, TProperty>(this TModel model, Expression<Func<TModel, TProperty>> expression)
        {
            return ModelMetadata.FromLambdaExpression<TModel, TProperty>(
                expression,
                new ViewDataDictionary<TModel>(model)
                ).DisplayName;
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }


        /*
        public static string GetDisplayNameOld<T, U>(this T src, Expression<Func<T, U>> exp)
        {
            var me = exp.Body as MemberExpression;
            if (me == null)
                throw new ArgumentException("Must be a MemberExpression.", "exp");

            var attr = me.Member
                         .GetCustomAttributes(typeof(DisplayAttribute), true)
                         .Cast<DisplayAttribute>()
                         .SingleOrDefault();

            return "<b>" + ((attr != null) ? attr.Name : me.Member.Name) + "</b>";
        }
         * */
    }
}