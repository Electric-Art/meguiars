﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kawa.Models
{
    public interface ISearchResult
    {
        int ID { get; }
        string name {get; }
        string url { get; }
        SiteResourceOwner.ObjectTypeOption objectType { get; }
    }
}
