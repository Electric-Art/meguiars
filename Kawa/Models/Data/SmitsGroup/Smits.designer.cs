﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kawa.Models.Data.SmitsGroup
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Smits")]
	public partial class SmitsDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertProduct(Product instance);
    partial void UpdateProduct(Product instance);
    partial void DeleteProduct(Product instance);
    partial void InsertSiteResource(SiteResource instance);
    partial void UpdateSiteResource(SiteResource instance);
    partial void DeleteSiteResource(SiteResource instance);
    #endregion
		
		public SmitsDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["SmitsConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public SmitsDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public SmitsDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public SmitsDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public SmitsDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Product> Products
		{
			get
			{
				return this.GetTable<Product>();
			}
		}
		
		public System.Data.Linq.Table<SiteResource> SiteResources
		{
			get
			{
				return this.GetTable<SiteResource>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Product")]
	public partial class Product : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private string _name;
		
		private string _menuName;
		
		private string _intro;
		
		private string _description;
		
		private string _sku;
		
		private System.Nullable<decimal> _price;
		
		private System.Nullable<decimal> _specialPrice;
		
		private System.DateTime _createDate;
		
		private System.Nullable<System.DateTime> _modifiedDate;
		
		private int _isHidden;
		
		private int _isNotForSale;
		
		private System.Nullable<int> _weight;
		
		private System.Nullable<int> _propellaID;
		
		private string _propellaName;
		
		private System.Nullable<decimal> _propellaRate;
		
		private System.Nullable<int> _propellaDivision;
		
		private System.Nullable<int> _propellaCatGrp;
		
		private System.Nullable<int> _propellaCat;
		
		private string _propellaSKU;
		
		private System.Nullable<int> _propellaObsolete;
		
		private System.Nullable<int> _propellaSource;
		
		private System.Nullable<int> _propellaSClassID;
		
		private System.Nullable<int> _propellaDiscCat;
		
		private string _propellaDisablePrice;
		
		private int _masterProduct;
		
		private int _specialRank;
		
		private string _metadescription;
		
		private string _metakeywords;
		
		private EntitySet<SiteResource> _SiteResources;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnnameChanging(string value);
    partial void OnnameChanged();
    partial void OnmenuNameChanging(string value);
    partial void OnmenuNameChanged();
    partial void OnintroChanging(string value);
    partial void OnintroChanged();
    partial void OndescriptionChanging(string value);
    partial void OndescriptionChanged();
    partial void OnskuChanging(string value);
    partial void OnskuChanged();
    partial void OnpriceChanging(System.Nullable<decimal> value);
    partial void OnpriceChanged();
    partial void OnspecialPriceChanging(System.Nullable<decimal> value);
    partial void OnspecialPriceChanged();
    partial void OncreateDateChanging(System.DateTime value);
    partial void OncreateDateChanged();
    partial void OnmodifiedDateChanging(System.Nullable<System.DateTime> value);
    partial void OnmodifiedDateChanged();
    partial void OnisHiddenChanging(int value);
    partial void OnisHiddenChanged();
    partial void OnisNotForSaleChanging(int value);
    partial void OnisNotForSaleChanged();
    partial void OnweightChanging(System.Nullable<int> value);
    partial void OnweightChanged();
    partial void OnpropellaIDChanging(System.Nullable<int> value);
    partial void OnpropellaIDChanged();
    partial void OnpropellaNameChanging(string value);
    partial void OnpropellaNameChanged();
    partial void OnpropellaRateChanging(System.Nullable<decimal> value);
    partial void OnpropellaRateChanged();
    partial void OnpropellaDivisionChanging(System.Nullable<int> value);
    partial void OnpropellaDivisionChanged();
    partial void OnpropellaCatGrpChanging(System.Nullable<int> value);
    partial void OnpropellaCatGrpChanged();
    partial void OnpropellaCatChanging(System.Nullable<int> value);
    partial void OnpropellaCatChanged();
    partial void OnpropellaSKUChanging(string value);
    partial void OnpropellaSKUChanged();
    partial void OnpropellaObsoleteChanging(System.Nullable<int> value);
    partial void OnpropellaObsoleteChanged();
    partial void OnpropellaSourceChanging(System.Nullable<int> value);
    partial void OnpropellaSourceChanged();
    partial void OnpropellaSClassIDChanging(System.Nullable<int> value);
    partial void OnpropellaSClassIDChanged();
    partial void OnpropellaDiscCatChanging(System.Nullable<int> value);
    partial void OnpropellaDiscCatChanged();
    partial void OnpropellaDisablePriceChanging(string value);
    partial void OnpropellaDisablePriceChanged();
    partial void OnmasterProductChanging(int value);
    partial void OnmasterProductChanged();
    partial void OnspecialRankChanging(int value);
    partial void OnspecialRankChanged();
    partial void OnmetadescriptionChanging(string value);
    partial void OnmetadescriptionChanged();
    partial void OnmetakeywordsChanging(string value);
    partial void OnmetakeywordsChanged();
    #endregion
		
		public Product()
		{
			this._SiteResources = new EntitySet<SiteResource>(new Action<SiteResource>(this.attach_SiteResources), new Action<SiteResource>(this.detach_SiteResources));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_name", DbType="VarChar(500) NOT NULL", CanBeNull=false)]
		public string name
		{
			get
			{
				return this._name;
			}
			set
			{
				if ((this._name != value))
				{
					this.OnnameChanging(value);
					this.SendPropertyChanging();
					this._name = value;
					this.SendPropertyChanged("name");
					this.OnnameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_menuName", DbType="VarChar(500)")]
		public string menuName
		{
			get
			{
				return this._menuName;
			}
			set
			{
				if ((this._menuName != value))
				{
					this.OnmenuNameChanging(value);
					this.SendPropertyChanging();
					this._menuName = value;
					this.SendPropertyChanged("menuName");
					this.OnmenuNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_intro", DbType="VarChar(MAX)")]
		public string intro
		{
			get
			{
				return this._intro;
			}
			set
			{
				if ((this._intro != value))
				{
					this.OnintroChanging(value);
					this.SendPropertyChanging();
					this._intro = value;
					this.SendPropertyChanged("intro");
					this.OnintroChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_description", DbType="Text", UpdateCheck=UpdateCheck.Never)]
		public string description
		{
			get
			{
				return this._description;
			}
			set
			{
				if ((this._description != value))
				{
					this.OndescriptionChanging(value);
					this.SendPropertyChanging();
					this._description = value;
					this.SendPropertyChanged("description");
					this.OndescriptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_sku", DbType="VarChar(50)")]
		public string sku
		{
			get
			{
				return this._sku;
			}
			set
			{
				if ((this._sku != value))
				{
					this.OnskuChanging(value);
					this.SendPropertyChanging();
					this._sku = value;
					this.SendPropertyChanged("sku");
					this.OnskuChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_price", DbType="Money")]
		public System.Nullable<decimal> price
		{
			get
			{
				return this._price;
			}
			set
			{
				if ((this._price != value))
				{
					this.OnpriceChanging(value);
					this.SendPropertyChanging();
					this._price = value;
					this.SendPropertyChanged("price");
					this.OnpriceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_specialPrice", DbType="Money")]
		public System.Nullable<decimal> specialPrice
		{
			get
			{
				return this._specialPrice;
			}
			set
			{
				if ((this._specialPrice != value))
				{
					this.OnspecialPriceChanging(value);
					this.SendPropertyChanging();
					this._specialPrice = value;
					this.SendPropertyChanged("specialPrice");
					this.OnspecialPriceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_createDate", DbType="DateTime NOT NULL")]
		public System.DateTime createDate
		{
			get
			{
				return this._createDate;
			}
			set
			{
				if ((this._createDate != value))
				{
					this.OncreateDateChanging(value);
					this.SendPropertyChanging();
					this._createDate = value;
					this.SendPropertyChanged("createDate");
					this.OncreateDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_modifiedDate", DbType="DateTime")]
		public System.Nullable<System.DateTime> modifiedDate
		{
			get
			{
				return this._modifiedDate;
			}
			set
			{
				if ((this._modifiedDate != value))
				{
					this.OnmodifiedDateChanging(value);
					this.SendPropertyChanging();
					this._modifiedDate = value;
					this.SendPropertyChanged("modifiedDate");
					this.OnmodifiedDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isHidden", DbType="Int NOT NULL")]
		public int isHidden
		{
			get
			{
				return this._isHidden;
			}
			set
			{
				if ((this._isHidden != value))
				{
					this.OnisHiddenChanging(value);
					this.SendPropertyChanging();
					this._isHidden = value;
					this.SendPropertyChanged("isHidden");
					this.OnisHiddenChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_isNotForSale", DbType="Int NOT NULL")]
		public int isNotForSale
		{
			get
			{
				return this._isNotForSale;
			}
			set
			{
				if ((this._isNotForSale != value))
				{
					this.OnisNotForSaleChanging(value);
					this.SendPropertyChanging();
					this._isNotForSale = value;
					this.SendPropertyChanged("isNotForSale");
					this.OnisNotForSaleChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_weight", DbType="Int")]
		public System.Nullable<int> weight
		{
			get
			{
				return this._weight;
			}
			set
			{
				if ((this._weight != value))
				{
					this.OnweightChanging(value);
					this.SendPropertyChanging();
					this._weight = value;
					this.SendPropertyChanged("weight");
					this.OnweightChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaID", DbType="Int")]
		public System.Nullable<int> propellaID
		{
			get
			{
				return this._propellaID;
			}
			set
			{
				if ((this._propellaID != value))
				{
					this.OnpropellaIDChanging(value);
					this.SendPropertyChanging();
					this._propellaID = value;
					this.SendPropertyChanged("propellaID");
					this.OnpropellaIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaName", DbType="VarChar(255)")]
		public string propellaName
		{
			get
			{
				return this._propellaName;
			}
			set
			{
				if ((this._propellaName != value))
				{
					this.OnpropellaNameChanging(value);
					this.SendPropertyChanging();
					this._propellaName = value;
					this.SendPropertyChanged("propellaName");
					this.OnpropellaNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaRate", DbType="Money")]
		public System.Nullable<decimal> propellaRate
		{
			get
			{
				return this._propellaRate;
			}
			set
			{
				if ((this._propellaRate != value))
				{
					this.OnpropellaRateChanging(value);
					this.SendPropertyChanging();
					this._propellaRate = value;
					this.SendPropertyChanged("propellaRate");
					this.OnpropellaRateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaDivision", DbType="Int")]
		public System.Nullable<int> propellaDivision
		{
			get
			{
				return this._propellaDivision;
			}
			set
			{
				if ((this._propellaDivision != value))
				{
					this.OnpropellaDivisionChanging(value);
					this.SendPropertyChanging();
					this._propellaDivision = value;
					this.SendPropertyChanged("propellaDivision");
					this.OnpropellaDivisionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaCatGrp", DbType="Int")]
		public System.Nullable<int> propellaCatGrp
		{
			get
			{
				return this._propellaCatGrp;
			}
			set
			{
				if ((this._propellaCatGrp != value))
				{
					this.OnpropellaCatGrpChanging(value);
					this.SendPropertyChanging();
					this._propellaCatGrp = value;
					this.SendPropertyChanged("propellaCatGrp");
					this.OnpropellaCatGrpChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaCat", DbType="Int")]
		public System.Nullable<int> propellaCat
		{
			get
			{
				return this._propellaCat;
			}
			set
			{
				if ((this._propellaCat != value))
				{
					this.OnpropellaCatChanging(value);
					this.SendPropertyChanging();
					this._propellaCat = value;
					this.SendPropertyChanged("propellaCat");
					this.OnpropellaCatChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaSKU", DbType="VarChar(255)")]
		public string propellaSKU
		{
			get
			{
				return this._propellaSKU;
			}
			set
			{
				if ((this._propellaSKU != value))
				{
					this.OnpropellaSKUChanging(value);
					this.SendPropertyChanging();
					this._propellaSKU = value;
					this.SendPropertyChanged("propellaSKU");
					this.OnpropellaSKUChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaObsolete", DbType="Int")]
		public System.Nullable<int> propellaObsolete
		{
			get
			{
				return this._propellaObsolete;
			}
			set
			{
				if ((this._propellaObsolete != value))
				{
					this.OnpropellaObsoleteChanging(value);
					this.SendPropertyChanging();
					this._propellaObsolete = value;
					this.SendPropertyChanged("propellaObsolete");
					this.OnpropellaObsoleteChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaSource", DbType="Int")]
		public System.Nullable<int> propellaSource
		{
			get
			{
				return this._propellaSource;
			}
			set
			{
				if ((this._propellaSource != value))
				{
					this.OnpropellaSourceChanging(value);
					this.SendPropertyChanging();
					this._propellaSource = value;
					this.SendPropertyChanged("propellaSource");
					this.OnpropellaSourceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaSClassID", DbType="Int")]
		public System.Nullable<int> propellaSClassID
		{
			get
			{
				return this._propellaSClassID;
			}
			set
			{
				if ((this._propellaSClassID != value))
				{
					this.OnpropellaSClassIDChanging(value);
					this.SendPropertyChanging();
					this._propellaSClassID = value;
					this.SendPropertyChanged("propellaSClassID");
					this.OnpropellaSClassIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaDiscCat", DbType="Int")]
		public System.Nullable<int> propellaDiscCat
		{
			get
			{
				return this._propellaDiscCat;
			}
			set
			{
				if ((this._propellaDiscCat != value))
				{
					this.OnpropellaDiscCatChanging(value);
					this.SendPropertyChanging();
					this._propellaDiscCat = value;
					this.SendPropertyChanged("propellaDiscCat");
					this.OnpropellaDiscCatChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_propellaDisablePrice", DbType="VarChar(50)")]
		public string propellaDisablePrice
		{
			get
			{
				return this._propellaDisablePrice;
			}
			set
			{
				if ((this._propellaDisablePrice != value))
				{
					this.OnpropellaDisablePriceChanging(value);
					this.SendPropertyChanging();
					this._propellaDisablePrice = value;
					this.SendPropertyChanged("propellaDisablePrice");
					this.OnpropellaDisablePriceChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_masterProduct", DbType="Int NOT NULL")]
		public int masterProduct
		{
			get
			{
				return this._masterProduct;
			}
			set
			{
				if ((this._masterProduct != value))
				{
					this.OnmasterProductChanging(value);
					this.SendPropertyChanging();
					this._masterProduct = value;
					this.SendPropertyChanged("masterProduct");
					this.OnmasterProductChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_specialRank", DbType="Int NOT NULL")]
		public int specialRank
		{
			get
			{
				return this._specialRank;
			}
			set
			{
				if ((this._specialRank != value))
				{
					this.OnspecialRankChanging(value);
					this.SendPropertyChanging();
					this._specialRank = value;
					this.SendPropertyChanged("specialRank");
					this.OnspecialRankChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_metadescription", DbType="VarChar(255)")]
		public string metadescription
		{
			get
			{
				return this._metadescription;
			}
			set
			{
				if ((this._metadescription != value))
				{
					this.OnmetadescriptionChanging(value);
					this.SendPropertyChanging();
					this._metadescription = value;
					this.SendPropertyChanged("metadescription");
					this.OnmetadescriptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_metakeywords", DbType="VarChar(255)")]
		public string metakeywords
		{
			get
			{
				return this._metakeywords;
			}
			set
			{
				if ((this._metakeywords != value))
				{
					this.OnmetakeywordsChanging(value);
					this.SendPropertyChanging();
					this._metakeywords = value;
					this.SendPropertyChanged("metakeywords");
					this.OnmetakeywordsChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Product_SiteResource", Storage="_SiteResources", ThisKey="ID", OtherKey="ProductID")]
		public EntitySet<SiteResource> SiteResources
		{
			get
			{
				return this._SiteResources;
			}
			set
			{
				this._SiteResources.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_SiteResources(SiteResource entity)
		{
			this.SendPropertyChanging();
			entity.Product = this;
		}
		
		private void detach_SiteResources(SiteResource entity)
		{
			this.SendPropertyChanging();
			entity.Product = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.SiteResource")]
	public partial class SiteResource : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private string _FileName;
		
		private int _SiteResourceType;
		
		private string _Caption;
		
		private string _Caption2;
		
		private string _embedCode;
		
		private System.Nullable<int> _width;
		
		private System.Nullable<int> _height;
		
		private System.Nullable<int> _ParentID;
		
		private int _ObjectID;
		
		private int _ObjectType;
		
		private System.Nullable<int> _orderNo;
		
		private System.Nullable<int> _PageID;
		
		private System.Nullable<int> _SiteID;
		
		private int _allowPostCard;
		
		private System.Nullable<int> _IngredientID;
		
		private System.Nullable<int> _ProductID;
		
		private System.Nullable<int> _TagID;
		
		private System.DateTime _createDate;
		
		private System.DateTime _modifiedDate;
		
		private EntitySet<SiteResource> _SiteResources;
		
		private EntityRef<Product> _Product;
		
		private EntityRef<SiteResource> _SiteResource1;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnFileNameChanging(string value);
    partial void OnFileNameChanged();
    partial void OnSiteResourceTypeChanging(int value);
    partial void OnSiteResourceTypeChanged();
    partial void OnCaptionChanging(string value);
    partial void OnCaptionChanged();
    partial void OnCaption2Changing(string value);
    partial void OnCaption2Changed();
    partial void OnembedCodeChanging(string value);
    partial void OnembedCodeChanged();
    partial void OnwidthChanging(System.Nullable<int> value);
    partial void OnwidthChanged();
    partial void OnheightChanging(System.Nullable<int> value);
    partial void OnheightChanged();
    partial void OnParentIDChanging(System.Nullable<int> value);
    partial void OnParentIDChanged();
    partial void OnObjectIDChanging(int value);
    partial void OnObjectIDChanged();
    partial void OnObjectTypeChanging(int value);
    partial void OnObjectTypeChanged();
    partial void OnorderNoChanging(System.Nullable<int> value);
    partial void OnorderNoChanged();
    partial void OnPageIDChanging(System.Nullable<int> value);
    partial void OnPageIDChanged();
    partial void OnSiteIDChanging(System.Nullable<int> value);
    partial void OnSiteIDChanged();
    partial void OnallowPostCardChanging(int value);
    partial void OnallowPostCardChanged();
    partial void OnIngredientIDChanging(System.Nullable<int> value);
    partial void OnIngredientIDChanged();
    partial void OnProductIDChanging(System.Nullable<int> value);
    partial void OnProductIDChanged();
    partial void OnTagIDChanging(System.Nullable<int> value);
    partial void OnTagIDChanged();
    partial void OncreateDateChanging(System.DateTime value);
    partial void OncreateDateChanged();
    partial void OnmodifiedDateChanging(System.DateTime value);
    partial void OnmodifiedDateChanged();
    #endregion
		
		public SiteResource()
		{
			this._SiteResources = new EntitySet<SiteResource>(new Action<SiteResource>(this.attach_SiteResources), new Action<SiteResource>(this.detach_SiteResources));
			this._Product = default(EntityRef<Product>);
			this._SiteResource1 = default(EntityRef<SiteResource>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FileName", DbType="VarChar(MAX) NOT NULL", CanBeNull=false)]
		public string FileName
		{
			get
			{
				return this._FileName;
			}
			set
			{
				if ((this._FileName != value))
				{
					this.OnFileNameChanging(value);
					this.SendPropertyChanging();
					this._FileName = value;
					this.SendPropertyChanged("FileName");
					this.OnFileNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SiteResourceType", DbType="Int NOT NULL")]
		public int SiteResourceType
		{
			get
			{
				return this._SiteResourceType;
			}
			set
			{
				if ((this._SiteResourceType != value))
				{
					this.OnSiteResourceTypeChanging(value);
					this.SendPropertyChanging();
					this._SiteResourceType = value;
					this.SendPropertyChanged("SiteResourceType");
					this.OnSiteResourceTypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Caption", DbType="VarChar(MAX)")]
		public string Caption
		{
			get
			{
				return this._Caption;
			}
			set
			{
				if ((this._Caption != value))
				{
					this.OnCaptionChanging(value);
					this.SendPropertyChanging();
					this._Caption = value;
					this.SendPropertyChanged("Caption");
					this.OnCaptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Caption2", DbType="VarChar(MAX)")]
		public string Caption2
		{
			get
			{
				return this._Caption2;
			}
			set
			{
				if ((this._Caption2 != value))
				{
					this.OnCaption2Changing(value);
					this.SendPropertyChanging();
					this._Caption2 = value;
					this.SendPropertyChanged("Caption2");
					this.OnCaption2Changed();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_embedCode", DbType="VarChar(MAX)")]
		public string embedCode
		{
			get
			{
				return this._embedCode;
			}
			set
			{
				if ((this._embedCode != value))
				{
					this.OnembedCodeChanging(value);
					this.SendPropertyChanging();
					this._embedCode = value;
					this.SendPropertyChanged("embedCode");
					this.OnembedCodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_width", DbType="Int")]
		public System.Nullable<int> width
		{
			get
			{
				return this._width;
			}
			set
			{
				if ((this._width != value))
				{
					this.OnwidthChanging(value);
					this.SendPropertyChanging();
					this._width = value;
					this.SendPropertyChanged("width");
					this.OnwidthChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_height", DbType="Int")]
		public System.Nullable<int> height
		{
			get
			{
				return this._height;
			}
			set
			{
				if ((this._height != value))
				{
					this.OnheightChanging(value);
					this.SendPropertyChanging();
					this._height = value;
					this.SendPropertyChanged("height");
					this.OnheightChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ParentID", DbType="Int")]
		public System.Nullable<int> ParentID
		{
			get
			{
				return this._ParentID;
			}
			set
			{
				if ((this._ParentID != value))
				{
					if (this._SiteResource1.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnParentIDChanging(value);
					this.SendPropertyChanging();
					this._ParentID = value;
					this.SendPropertyChanged("ParentID");
					this.OnParentIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ObjectID", DbType="Int NOT NULL")]
		public int ObjectID
		{
			get
			{
				return this._ObjectID;
			}
			set
			{
				if ((this._ObjectID != value))
				{
					this.OnObjectIDChanging(value);
					this.SendPropertyChanging();
					this._ObjectID = value;
					this.SendPropertyChanged("ObjectID");
					this.OnObjectIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ObjectType", DbType="Int NOT NULL")]
		public int ObjectType
		{
			get
			{
				return this._ObjectType;
			}
			set
			{
				if ((this._ObjectType != value))
				{
					this.OnObjectTypeChanging(value);
					this.SendPropertyChanging();
					this._ObjectType = value;
					this.SendPropertyChanged("ObjectType");
					this.OnObjectTypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_orderNo", DbType="Int")]
		public System.Nullable<int> orderNo
		{
			get
			{
				return this._orderNo;
			}
			set
			{
				if ((this._orderNo != value))
				{
					this.OnorderNoChanging(value);
					this.SendPropertyChanging();
					this._orderNo = value;
					this.SendPropertyChanged("orderNo");
					this.OnorderNoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PageID", DbType="Int")]
		public System.Nullable<int> PageID
		{
			get
			{
				return this._PageID;
			}
			set
			{
				if ((this._PageID != value))
				{
					this.OnPageIDChanging(value);
					this.SendPropertyChanging();
					this._PageID = value;
					this.SendPropertyChanged("PageID");
					this.OnPageIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SiteID", DbType="Int")]
		public System.Nullable<int> SiteID
		{
			get
			{
				return this._SiteID;
			}
			set
			{
				if ((this._SiteID != value))
				{
					this.OnSiteIDChanging(value);
					this.SendPropertyChanging();
					this._SiteID = value;
					this.SendPropertyChanged("SiteID");
					this.OnSiteIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_allowPostCard", DbType="Int NOT NULL")]
		public int allowPostCard
		{
			get
			{
				return this._allowPostCard;
			}
			set
			{
				if ((this._allowPostCard != value))
				{
					this.OnallowPostCardChanging(value);
					this.SendPropertyChanging();
					this._allowPostCard = value;
					this.SendPropertyChanged("allowPostCard");
					this.OnallowPostCardChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IngredientID", DbType="Int")]
		public System.Nullable<int> IngredientID
		{
			get
			{
				return this._IngredientID;
			}
			set
			{
				if ((this._IngredientID != value))
				{
					this.OnIngredientIDChanging(value);
					this.SendPropertyChanging();
					this._IngredientID = value;
					this.SendPropertyChanged("IngredientID");
					this.OnIngredientIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ProductID", DbType="Int")]
		public System.Nullable<int> ProductID
		{
			get
			{
				return this._ProductID;
			}
			set
			{
				if ((this._ProductID != value))
				{
					if (this._Product.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnProductIDChanging(value);
					this.SendPropertyChanging();
					this._ProductID = value;
					this.SendPropertyChanged("ProductID");
					this.OnProductIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_TagID", DbType="Int")]
		public System.Nullable<int> TagID
		{
			get
			{
				return this._TagID;
			}
			set
			{
				if ((this._TagID != value))
				{
					this.OnTagIDChanging(value);
					this.SendPropertyChanging();
					this._TagID = value;
					this.SendPropertyChanged("TagID");
					this.OnTagIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_createDate", DbType="DateTime NOT NULL")]
		public System.DateTime createDate
		{
			get
			{
				return this._createDate;
			}
			set
			{
				if ((this._createDate != value))
				{
					this.OncreateDateChanging(value);
					this.SendPropertyChanging();
					this._createDate = value;
					this.SendPropertyChanged("createDate");
					this.OncreateDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_modifiedDate", DbType="DateTime NOT NULL")]
		public System.DateTime modifiedDate
		{
			get
			{
				return this._modifiedDate;
			}
			set
			{
				if ((this._modifiedDate != value))
				{
					this.OnmodifiedDateChanging(value);
					this.SendPropertyChanging();
					this._modifiedDate = value;
					this.SendPropertyChanged("modifiedDate");
					this.OnmodifiedDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="SiteResource_SiteResource", Storage="_SiteResources", ThisKey="ID", OtherKey="ParentID")]
		public EntitySet<SiteResource> SiteResources
		{
			get
			{
				return this._SiteResources;
			}
			set
			{
				this._SiteResources.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Product_SiteResource", Storage="_Product", ThisKey="ProductID", OtherKey="ID", IsForeignKey=true)]
		public Product Product
		{
			get
			{
				return this._Product.Entity;
			}
			set
			{
				Product previousValue = this._Product.Entity;
				if (((previousValue != value) 
							|| (this._Product.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Product.Entity = null;
						previousValue.SiteResources.Remove(this);
					}
					this._Product.Entity = value;
					if ((value != null))
					{
						value.SiteResources.Add(this);
						this._ProductID = value.ID;
					}
					else
					{
						this._ProductID = default(Nullable<int>);
					}
					this.SendPropertyChanged("Product");
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="SiteResource_SiteResource", Storage="_SiteResource1", ThisKey="ParentID", OtherKey="ID", IsForeignKey=true)]
		public SiteResource SiteResource1
		{
			get
			{
				return this._SiteResource1.Entity;
			}
			set
			{
				SiteResource previousValue = this._SiteResource1.Entity;
				if (((previousValue != value) 
							|| (this._SiteResource1.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._SiteResource1.Entity = null;
						previousValue.SiteResources.Remove(this);
					}
					this._SiteResource1.Entity = value;
					if ((value != null))
					{
						value.SiteResources.Add(this);
						this._ParentID = value.ID;
					}
					else
					{
						this._ParentID = default(Nullable<int>);
					}
					this.SendPropertyChanged("SiteResource1");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_SiteResources(SiteResource entity)
		{
			this.SendPropertyChanging();
			entity.SiteResource1 = this;
		}
		
		private void detach_SiteResources(SiteResource entity)
		{
			this.SendPropertyChanging();
			entity.SiteResource1 = null;
		}
	}
}
#pragma warning restore 1591
