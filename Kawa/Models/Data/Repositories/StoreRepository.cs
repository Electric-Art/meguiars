﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Kawa.Models.Data;

namespace Kawa.Models
{
    /// <summary>
    /// Summary description for Store.
    /// </summary>



    public class StoreRepository
    {
        dbDataContext _db;

        public StoreRepository()
        {
            _db = new dbDataContext();
        }

        public IQueryable<Store> listAll()
        {

            return from i in _db.Stores
                   select new Store
                   {
                       ID = i.ID,
                       name = i.name,
                       shopPosition = i.shopPosition,
                       streetAddress = i.streetAddress,
                       suburb = i.suburb,
                       state = i.state,
                       phone = i.phone,
                       website = i.website,
                       storeWebsite = i.storeWebsite,
                       postCode = i.postCode,
                       description = i.description,
                       contactDetails = i.contactDetails,
                       Type = i.Type,
                       geocodeRun = i.geocodeRun,
                       lat = i.lat,
                       lng = i.lng
                   };

        }

        public void saveStore(Store store)
        {
            if (store != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Store add;

                if (store.ID > 0)
                {
                    add = _db.Stores.Where(x => x.ID == store.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Store();
                }

                //synch it
                add.name = store.name;
                add.shopPosition = store.shopPosition;
                add.streetAddress = store.streetAddress;
                add.suburb = store.suburb;
                add.state = store.state;
                add.phone = store.phone;
                add.website = store.website;
                add.storeWebsite = store.storeWebsite;
                add.postCode = store.postCode;
                add.description = store.description;
                add.contactDetails = store.contactDetails;
                add.Type = store.Type;
                add.geocodeRun = store.geocodeRun;
                add.lat = store.lat;
                add.lng = store.lng;

                //save it
                if (add.ID == 0)
                    _db.Stores.InsertOnSubmit(add);

                _db.SubmitChanges();

                store.ID = add.ID;
            }
        }

        public bool deleteStore(Store store)
        {
            var delStore = from o in _db.Stores
                           where o.ID == store.ID
                           select o;


            //delete the order
            _db.Stores.DeleteAllOnSubmit(delStore);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

    }
}
