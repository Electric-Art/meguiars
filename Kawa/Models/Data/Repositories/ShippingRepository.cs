﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Kawa.Models.Data;

namespace Kawa.Models
{
    public class ShippingRepository
    {
        dbDataContext _db;

        public ShippingRepository()
		{
			_db = new dbDataContext();
		}

        #region IP2Location
        public IQueryable<string> lookupIP(int ipnumber)
        {
            return from i in _db.IP2Countries
                   where i.Start < ipnumber && i.End > ipnumber
                   select i.Code;
        }
        #endregion

        public IQueryable<Location> listAllLocations()
        {

            return from i in _db.Locations
                   select new Location
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Code = i.Code,
                       isHidden = i.isHidden,
                       createDate = i.createDate,

                   };

        }

        public IQueryable<Location> listAllLocationsForCountry(int CountryID)
        {
            return from i in _db.Locations
                   where i.Location_Countries.Any(x => x.CountryID == CountryID) 
                   && !i.Location_States.Any()
                   && !i.LocationPostcodeRanges.Any()
                   select new Location
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Code = i.Code,
                       isHidden = i.isHidden,
                       createDate = i.createDate
                   };

        }

        public IQueryable<Location> listAllLocationsForState(int StateID)
        {
            return from i in _db.Locations
                   where i.Location_States.Any(x => x.StateID == StateID) && !i.LocationPostcodeRanges.Any()
                   select new Location
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Code = i.Code,
                       isHidden = i.isHidden,
                       createDate = i.createDate
                   };

        }

        public IQueryable<Location> listAllLocationsForPostCode(int StateID, int Postcode)
        {
            return from i in _db.Locations
                   where i.Location_States.Any(x => x.StateID == StateID)
                   && i.LocationPostcodeRanges.Any(x => Postcode >= x.FromPostcode && Postcode <= x.ToPostcode)
                   select new Location
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Code = i.Code,
                       isHidden = i.isHidden,
                       createDate = i.createDate
                   };

        }

        public void saveLocation(Location location)
        {
            if (location != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Location add;

                if (location.ID > 0)
                {
                    add = _db.Locations.Where(x => x.ID == location.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Location();
                }

                //synch it
                add.Name = location.Name;
                add.Code = location.Code;
                add.isHidden = location.isHidden;
                add.createDate = location.createDate;


                //save it
                if (add.ID == 0)
                    _db.Locations.InsertOnSubmit(add);

                _db.SubmitChanges();

                location.ID = add.ID;
            }
        }

        public bool deleteLocation(Location location)
        {
            var delLocation_Countries = from o in _db.Location_Countries
                              where o.LocationID == location.ID
                              select o;
            _db.Location_Countries.DeleteAllOnSubmit(delLocation_Countries);

            var delLocation_States = from o in _db.Location_States
                                        where o.LocationID == location.ID
                                        select o;
            _db.Location_States.DeleteAllOnSubmit(delLocation_States);

            var delShippingRates = from o in _db.ShippingRates
                                     where o.LocationID == location.ID
                                     select o;
            _db.ShippingRates.DeleteAllOnSubmit(delShippingRates);


            var delLocation = from o in _db.Locations
                              where o.ID == location.ID
                              select o;


            //delete the order
            _db.Locations.DeleteAllOnSubmit(delLocation);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<ShippingRate> listAllShippingRates()
        {

            return from i in _db.ShippingRates
                   select new ShippingRate
                   {
                       ID = i.ID,
                       ShippingMethodID = i.ShippingMethodID,
                       LocationID = i.LocationID,
                       Type = i.Type,
                       Rate = i.Rate,
                       WeightDependency = i.WeightDependency,
                       CostDependency = i.CostDependency,
                       isHidden = i.isHidden,
                       createDate = i.createDate,

                   };

        }

        public void saveShippingRate(ShippingRate shippingRate)
        {
            if (shippingRate != null)
            {

                //see if it's in the db
                Kawa.Models.Data.ShippingRate add;

                if (shippingRate.ID > 0)
                {
                    add = _db.ShippingRates.Where(x => x.ID == shippingRate.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.ShippingRate();
                }

                //synch it
                add.ShippingMethodID = shippingRate.ShippingMethodID;
                add.LocationID = shippingRate.LocationID;
                add.Type = shippingRate.Type;
                add.Rate = shippingRate.Rate;
                add.WeightDependency = shippingRate.WeightDependency;
                add.CostDependency = shippingRate.CostDependency;
                add.isHidden = shippingRate.isHidden;
                add.createDate = shippingRate.createDate;


                //save it
                if (add.ID == 0)
                    _db.ShippingRates.InsertOnSubmit(add);

                _db.SubmitChanges();

                shippingRate.ID = add.ID;
            }
        }

        public bool deleteShippingRate(ShippingRate shippingRate)
        {
            var delShippingRate = from o in _db.ShippingRates
                                  where o.ID == shippingRate.ID
                                  select o;


            //delete the order
            _db.ShippingRates.DeleteAllOnSubmit(delShippingRate);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<Country> listAllCountries()
        {

            return from i in _db.Countries
                   select new Country
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Group = i.Group,
                       CountryName = i.CountryName,
                       Rank = i.Rank,
                       isHidden = i.isHidden,
                       Code = i.Code,
                       CodeA3 = i.CodeA3,
                       CodeN3 = i.CodeN3,
                       RegionCode = i.RegionCode,
                       createDate = i.createDate,
                       isBlocked = i.isBlocked,
                       isAddressSearch = i.isAddressSearch,
                       isIntPostCode = i.isIntPostCode
                   };

        }

        public IQueryable<Country> listAllCountriesForLocation(int LocationID)
        {

            return from i in _db.Countries
                   where i.Location_Countries.Any(lc => lc.LocationID == LocationID)
                   select new Country
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Group = i.Group,
                       CountryName = i.CountryName,
                       Rank = i.Rank,
                       isHidden = i.isHidden,
                       Code = i.Code,
                       CodeA3 = i.CodeA3,
                       CodeN3 = i.CodeN3,
                       RegionCode = i.RegionCode,
                       createDate = i.createDate,
                       isBlocked = i.isBlocked,
                       isAddressSearch = i.isAddressSearch,
                       isIntPostCode = i.isIntPostCode
                   };

        }

        public void saveCountry(Country country)
        {
            if (country != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Country add;

                if (country.ID > 0)
                {
                    add = _db.Countries.Where(x => x.ID == country.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Country();
                }

                //synch it
                add.Name = country.Name;
                add.Group = country.Group;
                add.CountryName = country.CountryName;
                add.Rank = country.Rank;
                add.isHidden = country.isHidden;
                add.Code = country.Code;
                add.CodeA3 = country.CodeA3;
                add.CodeN3 = country.CodeN3;
                add.RegionCode = country.RegionCode;
                add.createDate = country.createDate;
                add.isBlocked = country.isBlocked;
                add.isAddressSearch = country.isAddressSearch;
                add.isIntPostCode = country.isIntPostCode;

                //save it
                if (add.ID == 0)
                    _db.Countries.InsertOnSubmit(add);

                _db.SubmitChanges();

                country.ID = add.ID;
            }
        }

        public bool deleteCountry(Country country)
        {
            var delCountry = from o in _db.Countries
                             where o.ID == country.ID
                             select o;


            //delete the order
            _db.Countries.DeleteAllOnSubmit(delCountry);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public void saveCountryScope(int LocationID, int CountryID)
        {
            if (_db.Location_Countries.Where(x => x.LocationID == LocationID && x.CountryID == CountryID).Any())
                return;
               //see if it's in the db
            Kawa.Models.Data.Location_Country add = new Kawa.Models.Data.Location_Country();

            //synch it
            add.LocationID = LocationID;
            add.CountryID = CountryID;
            _db.Location_Countries.InsertOnSubmit(add);

            _db.SubmitChanges();
        }

        public void deleteCountryScope(int LocationID, int CountryID)
        {
            var delScope = from o in _db.Location_Countries
                             where o.LocationID == LocationID && o.CountryID == CountryID
                             select o;

            //delete the order
            _db.Location_Countries.DeleteAllOnSubmit(delScope);
            _db.SubmitChanges();
        }

        public IQueryable<State> listAllStates()
        {

            return from i in _db.States
                   select new State
                   {
                       ID = i.ID,
                       Name = i.Name,
                       CountryID = i.CountryID,
                       Code = i.Code,
                       isHidden = i.isHidden,
                       createDate = i.createDate
                   };

        }

        public static string getStateCode(string state)
        {
            switch(state)
            {
                case "New South Wales" :
                    return "NSW";
                case "Queensland":
                    return "QLD";
                case "South Australia":
                    return "SA";
                case "Tasmania":
                    return "TAS";
                case "Victoria":
                    return "VIC";
                case "Western Australia":
                    return "WA";
                case "Northern Territory":
                    return "NT";
                case "Australian Capital Territory":
                    return "ACT";

            }
            return "N/A";
        }

        public IQueryable<State> listAllStatesForLocation(int LocationID)
        {

            return from i in _db.States
                   where i.Location_States.Any(lc => lc.LocationID == LocationID)
                   select new State
                   {
                       ID = i.ID,
                       Name = i.Name,
                       CountryID = i.CountryID,
                       Code = i.Code,
                       isHidden = i.isHidden,
                       createDate = i.createDate
                   };

        }

        public void saveState(State state)
        {
            if (state != null)
            {

                //see if it's in the db
                Kawa.Models.Data.State add;

                if (state.ID > 0)
                {
                    add = _db.States.Where(x => x.ID == state.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.State();
                }

                //synch it
                add.Name = state.Name;
                add.CountryID = state.CountryID;
                add.Code = state.Code;
                add.isHidden = state.isHidden;
                add.createDate = state.createDate;

                //save it
                if (add.ID == 0)
                    _db.States.InsertOnSubmit(add);

                _db.SubmitChanges();

                state.ID = add.ID;
            }
        }

        public bool deleteState(State state)
        {
            var delState = from o in _db.States
                           where o.ID == state.ID
                           select o;


            //delete the order
            _db.States.DeleteAllOnSubmit(delState);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public void saveStateScope(int LocationID, int StateID)
        {
            if (_db.Location_States.Where(x => x.LocationID == LocationID && x.StateID == StateID).Any())
                return;
            //see if it's in the db
            Kawa.Models.Data.Location_State add = new Kawa.Models.Data.Location_State();

            //synch it
            add.LocationID = LocationID;
            add.StateID = StateID;
            _db.Location_States.InsertOnSubmit(add);

            _db.SubmitChanges();
        }

        public void deleteStateScope(int LocationID, int StateID)
        {
            var delScope = from o in _db.Location_States
                           where o.LocationID == LocationID && o.StateID == StateID
                           select o;

            //delete the order
            _db.Location_States.DeleteAllOnSubmit(delScope);
            _db.SubmitChanges();
        }

        public IQueryable<LocationPostcodeRange> listAllLocationPostcodeRanges()
        {
            return from i in _db.LocationPostcodeRanges
                   select new LocationPostcodeRange
                   {
                       ID = i.ID,
                       LocationID = i.LocationID,
                       FromPostcode = i.FromPostcode,
                       ToPostcode = i.ToPostcode,
                   };
        }

        public void saveLocationPostcodeRange(LocationPostcodeRange locationPostcodeRange)
        {
            //see if it's in the db
            Kawa.Models.Data.LocationPostcodeRange add;

            if (locationPostcodeRange.ID > 0)
            {
                add = _db.LocationPostcodeRanges.Where(x => x.ID == locationPostcodeRange.ID).SingleOrDefault();
            }
            else
            {
                add = new Kawa.Models.Data.LocationPostcodeRange();
            }

            //synch it
            add.LocationID = locationPostcodeRange.LocationID;
            add.FromPostcode = locationPostcodeRange.FromPostcode;
            add.ToPostcode = locationPostcodeRange.ToPostcode;


            //save it
            if (add.ID == 0)
                _db.LocationPostcodeRanges.InsertOnSubmit(add);

            _db.SubmitChanges();

            locationPostcodeRange.ID = add.ID;
        }

        public bool deleteLocationPostcodeRange(LocationPostcodeRange locationPostcodeRange)
        {
            var delLocationPostcodeRange = from o in _db.LocationPostcodeRanges
                                           where o.ID == locationPostcodeRange.ID
                                           select o;
            //delete the order
            _db.LocationPostcodeRanges.DeleteAllOnSubmit(delLocationPostcodeRange);

            _db.SubmitChanges();
            bool result = true;
            return result;
        }

        public IQueryable<ShippingMethod> listAllShippingMethods()
        {
            return from i in _db.ShippingMethods
                   select new ShippingMethod
                   {
                       ID = i.ID,
                       Name = i.Name,
                       DeliveryTime = i.DeliveryTime,
                       TrackingUrl = i.TrackingUrl,
                       isHidden = i.isHidden,
                       createDate = i.createDate,

                   };
        }

        public void saveShippingMethod(ShippingMethod shippingMethod)
        {
            if (shippingMethod != null)
            {

                //see if it's in the db
                Kawa.Models.Data.ShippingMethod add;

                if (shippingMethod.ID > 0)
                {
                    add = _db.ShippingMethods.Where(x => x.ID == shippingMethod.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.ShippingMethod();
                }

                //synch it
                add.Name = shippingMethod.Name;
                add.DeliveryTime = shippingMethod.DeliveryTime;
                add.TrackingUrl = shippingMethod.TrackingUrl;
                add.isHidden = shippingMethod.isHidden;
                add.createDate = shippingMethod.createDate;


                //save it
                if (add.ID == 0)
                    _db.ShippingMethods.InsertOnSubmit(add);

                _db.SubmitChanges();

                shippingMethod.ID = add.ID;
            }
        }

        public bool deleteShippingMethod(ShippingMethod shippingMethod)
        {
            var delShippingMethod = from o in _db.ShippingMethods
                                    where o.ID == shippingMethod.ID
                                    select o;


            //delete the order
            _db.ShippingMethods.DeleteAllOnSubmit(delShippingMethod);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<Carrier> listAllCarriers()
        {

            return from i in _db.Carriers
                   select new Carrier
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Url = i.Url,
                       createDate = i.createDate,

                   };

        }

        public void saveCarrier(Carrier carrier)
        {
            if (carrier != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Carrier add;

                if (carrier.ID > 0)
                {
                    add = _db.Carriers.Where(x => x.ID == carrier.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Carrier();
                }

                //synch it
                add.Name = carrier.Name;
                add.Url = carrier.Url;
                add.createDate = carrier.createDate;


                //save it
                if (add.ID == 0)
                    _db.Carriers.InsertOnSubmit(add);

                _db.SubmitChanges();

                carrier.ID = add.ID;
            }
        }

        public bool deleteCarrier(Carrier carrier)
        {
            var delCarrier = from o in _db.Carriers
                             where o.ID == carrier.ID
                             select o;


            //delete the order
            _db.Carriers.DeleteAllOnSubmit(delCarrier);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

    }

    public static class ShippingRepositoryExtensions
    {
        public static Location createAggregate(this Data.Location i)
        {
            return new Location()
            {
                ID = i.ID,
                Name = i.Name,
                Code = i.Code,
                isHidden = i.isHidden,
                createDate = i.createDate,
            };
        }

    }
}

