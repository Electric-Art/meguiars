﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.Data;

namespace Kawa.Models
{
    public class SiteResourceRepository
    {
        dbDataContext _db;
		
		public SiteResourceRepository()
		{
			_db = new dbDataContext();
		}

        public IQueryable<SiteResource> listAll(SiteResourceOwner sro)
        {
            return listAll().Where(sr => sr.ObjectID == sro.getObjectID() && sr.ObjectType == sro.getObjectType());
        }

		public IQueryable<SiteResource> listAll()
		{
			
			return (from i in _db.SiteResources
					select new SiteResource {
                       			ID = i.ID,
						        FileName = i.FileName,
			                    SiteResourceType = (SiteResource.SiteResourceTypeOption) i.SiteResourceType,
			                    Caption = i.Caption,
			                    Caption2 = i.Caption2,
			                    embedCode = i.embedCode,
			                    width = i.width,
			                    height = i.height,
			                    ParentID = i.ParentID,
                                ObjectID = i.ObjectID,
			                    ObjectType = (SiteResourceOwner.ObjectTypeOption) i.ObjectType,
			                    orderNo = i.orderNo.HasValue ? (int)i.orderNo : 1000,
			                    PageID = i.PageID,
			                    SiteID = i.SiteID,
                                ProductID = i.ProductID,
                                TagID = i.TagID,
                                IngredientID = i.IngredientID,
                                allowPostCard = i.allowPostCard==1,
								PrescriptionTagID = i.PrescriptionTagID
							}).OrderBy(s => s.orderNo);
			
		}

		public void saveSiteResource(SiteResource siteResource)
		{
			if (siteResource != null)
			{

				//see if it's in the db
				Kawa.Models.Data.SiteResource add;

				if (siteResource.ID > 0)
				{
					add = _db.SiteResources.Where(x => x.ID == siteResource.ID).SingleOrDefault();
				}
				else
				{
					add = new Kawa.Models.Data.SiteResource();
				}

				//synch it
				add.FileName = siteResource.FileName;
				add.SiteResourceType = (int) siteResource.SiteResourceType;
				add.Caption = siteResource.Caption;
				add.Caption2 = siteResource.Caption2;
				add.embedCode = siteResource.embedCode;
				add.width = siteResource.width;
				add.height = siteResource.height;
				add.ParentID = siteResource.ParentID;
                add.ObjectID = siteResource.ObjectID;
				add.ObjectType = (int)siteResource.ObjectType;
				add.orderNo = siteResource.orderNo;
				add.PageID = siteResource.PageID;
				add.SiteID = siteResource.SiteID;
                add.ProductID = siteResource.ProductID;
                add.TagID = siteResource.TagID;
				add.PrescriptionTagID = siteResource.PrescriptionTagID;
				add.IngredientID = siteResource.IngredientID;
                add.allowPostCard = siteResource.allowPostCard ? 1 : 0;
		        
				//save it
				if (add.ID == 0)
					_db.SiteResources.InsertOnSubmit(add);

				_db.SubmitChanges();

				siteResource.ID = add.ID;
			}
		}	
		
		public bool deleteSiteResource(SiteResource siteResource)
        {
             var delSiteResource = from o in _db.SiteResources
                            where o.ID == siteResource.ID
                           select o;


            //delete the order
            _db.SiteResources.DeleteAllOnSubmit(delSiteResource);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

	
    }

    public static class SiteResourceExtensions
    {
        public static SiteResource createAggregate(this Data.SiteResource i)
        {
            return new SiteResource()
            {
                ID = i.ID,
                FileName = i.FileName,
                SiteResourceType = (SiteResource.SiteResourceTypeOption)i.SiteResourceType,
                Caption = i.Caption,
                Caption2 = i.Caption2,
                embedCode = i.embedCode,
                width = i.width,
                height = i.height,
                ParentID = i.ParentID,
                ObjectID = i.ObjectID,
                ObjectType = (SiteResourceOwner.ObjectTypeOption)i.ObjectType,
                orderNo = i.orderNo.HasValue ? (int)i.orderNo : 1000,
                PageID = i.PageID,
                SiteID = i.SiteID,
                ProductID = i.ProductID,
                IngredientID = i.IngredientID,
                TagID = i.TagID,
				PrescriptionTagID = i.PrescriptionTagID,
				allowPostCard = i.allowPostCard == 1
            };
        }
    }
}