﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Kawa.Models.Data;
using Kawa.Models.Extensions;

namespace Kawa.Models
{
    public class PortRepository
    {
        dbDataContext _db;

        public PortRepository()
		{
			_db = new dbDataContext();
		}
        
        public List<kawa_Port_AliasesResult> listAllProductsAliases()
        {
            return _db.kawa_Port_Aliases().ToList();
        }

        public IQueryable<Port_ProductAndImage> listAllProductsWithImages()
        {
            return from i in _db.Port_ProductAndImages select i;
        }

        public IQueryable<Port_User> listAllPortAccounts()
        {
            return from i in _db.Port_Users select i;
        }

        public void savePortAccount()
        {
            _db.SubmitChanges();
        }
    }
}