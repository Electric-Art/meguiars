﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Kawa.Models.Data;
using Kawa.Models.Extensions;

namespace Kawa.Models
{
    public class CacheRepository
    {
        dbDataContext _db;
		
		public CacheRepository()
		{
			_db = new dbDataContext();
            _db.CommandTimeout = 60;
		}

        public void cacheProductsWithAlorithm(Tag.SystemTagOption SystemTag, int TagID)
        {
            switch(SystemTag)
            {
                case Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS:
                    _db.kawa_Cache_Best_Sellers(TagID, null);
                    break;
                case Tag.SystemTagOption.PRODUCT_CAT_TRENDING_NOW:
                    _db.kawa_Cache_Trending_Now(TagID, null);
                    break;
                case Tag.SystemTagOption.SEARCH_ENTITY_TRENDING:
                    _db.kawa_Cache_Stat_Trending(TagID, 0);
                    break;
            }
        }

        public void cacheProductsWithAlgorithmIntersection(Tag.SystemTagOption SystemTag, int Tag1ID, int Tag2ID)
        {
            switch (SystemTag)
            {
                case Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS:
                    _db.kawa_Cache_Best_Sellers(Tag1ID, Tag2ID);
                    break;
            }
        }

        public List<ISearchResult> getTrendingStatEntities(Tag.SystemTagOption SystemTag)
        {
            var tag = _db.Tags.SingleOrDefault(x => x.SystemTag == (int)SystemTag);
            if (tag == null)
                return new List<ISearchResult>();
            var results = _db.ListCaches.Where(x => x.Tag1ID == tag.ID && (x.Tag2ID.HasValue || x.ProductID.HasValue)).OrderBy(x => x.ID).ToList();
            return results.Select(x => x.ProductID.HasValue
                ?
                   (ISearchResult)(_db.Products.Where(y => y.ID == x.ProductID).Select(i => new Product()
                    {
                        ID = i.ID,
                        name = i.name,
                        nameUrl = i.nameUrl,
                    }).Single())
                :
                    (ISearchResult)(_db.Tags.Where(y => y.ID == x.Tag2ID).Select(i => new Tag()
                    {
                        ID = i.ID,
                        name = i.name,
                        nameUrl = i.nameUrl,
                        Type = (Tag.TypeOption)i.Type
                    }).Single())
                ).ToList();
        }

        public void saveStat(Models.Data.Stat stat)
        {
            _db.Stats.InsertOnSubmit(stat);
            _db.SubmitChanges();
        }
    }
    
}