﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Kawa.Models.Data;

namespace Kawa.Models
{
    public class PersonRepository
    {
        dbDataContext _db;

        public PersonRepository()
        {
            _db = new dbDataContext();
        }

        public IQueryable<Account> listAllAccounts()
		{
			
			return from i in _db.Accounts
					select new Account {
                       			ID = i.ID,
						        UserName = i.UserName,
			                    createDate = i.createDate,
			                    isAdmin = i.isAdmin,
			                    isTrade = i.isTrade,
			                    isBlocked = i.isBlocked,
                                creditCardToken = i.creditCardToken,
                                saveCreditCard = i.saveCreditCard,
                                firstName = i.firstName,
                                lastName = i.lastName,
                                gender = (Account.GenderOption?)i.gender,
                                dateOfBirth = i.dateOfBirth,
                                channel = i.channel
							};
			
		}

		public void saveAccount(Account account)
		{
			if (account != null)
			{

				//see if it's in the db
				Kawa.Models.Data.Account add;

				if (account.ID > 0)
				{
					add = _db.Accounts.Where(x => x.ID == account.ID).SingleOrDefault();
				}
				else
				{
					add = new Kawa.Models.Data.Account();
				}

				//synch it
				add.UserName = account.UserName;
				add.createDate = account.createDate;
				add.isAdmin = account.isAdmin;
				add.isTrade = account.isTrade;
				add.isBlocked = account.isBlocked;
                add.creditCardToken = account.creditCardToken;
                add.saveCreditCard = account.saveCreditCard;
                add.firstName = account.firstName;
                add.lastName = account.lastName;
                add.gender = (int?)account.gender;
                add.dateOfBirth = account.dateOfBirth;
                add.channel = account.channel;
		        
				//save it
				if (add.ID == 0)
					_db.Accounts.InsertOnSubmit(add);

				_db.SubmitChanges();

				account.ID = add.ID;
			}
		}	
		
		public bool deleteAccount(Account account)
        {
            var delOrders = from o in _db.Orders
                             where o.AccountID == account.ID
                             select o;

            foreach (var order in delOrders)
                order.AccountID = null;

            _db.SubmitChanges();

            var delFavs = from o in _db.Favourites
                          where o.AccountID == account.ID
                            select o;

            _db.Favourites.DeleteAllOnSubmit(delFavs);
            _db.SubmitChanges();

             var delAccount = from o in _db.Accounts
                            where o.ID == account.ID
                           select o;


            //delete the order
            _db.Accounts.DeleteAllOnSubmit(delAccount);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }
        public Account getGuest()
        {
            return listAllAccounts().Where(x => x.UserName == "guest@meguiars.co.nz").FirstOrDefault();
        }

        public void ResetSessionFavs(string session)
        {
            var delFavs = from o in _db.Favourites
                             where o.Session == session
                             select o;

            _db.Favourites.DeleteAllOnSubmit(delFavs);
            _db.SubmitChanges();
        }
        public IQueryable<Favourite> listAllFavourites()
        {

            return from i in _db.Favourites
                   select new Favourite
                   {
                       ID = i.ID,
                       AccountID = i.AccountID,
                       ProductID = i.ProductID,
                       PageID = i.PageID,
                       createDate = i.createDate,
                       Session = i.Session,
                       Type = i.Type,
                       product = i.Product != null ? i.Product.createAggregate() : null,
                       page = i.Page != null ? i.Page.createAggregate() : null
                   };

        }

        public void saveFavourite(Favourite favourite)
        {
            if (favourite != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Favourite add;

                if (favourite.ID > 0)
                {
                    add = _db.Favourites.Where(x => x.ID == favourite.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Favourite();
                }

                //synch it
                add.AccountID = favourite.AccountID;
                add.ProductID = favourite.ProductID;
                add.PageID = favourite.PageID;
                add.createDate = favourite.createDate;
                add.Type = favourite.Type;
                add.Session = favourite.Session;


                //save it
                if (add.ID == 0)
                    _db.Favourites.InsertOnSubmit(add);

                _db.SubmitChanges();

                favourite.ID = add.ID;
            }
        }

        public bool deleteFavourite(Favourite favourite)
        {
            var delFavourite = from o in _db.Favourites
                               where o.ID == favourite.ID
                               select o;


            //delete the order
            _db.Favourites.DeleteAllOnSubmit(delFavourite);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<MailChimpCache> listAllMailChimpCaches()
		{
			return from i in _db.MailChimpCaches select i;
        }

        public void saveMailChimpCaches(List<MailChimpCache> caches)
        {
            _db.MailChimpCaches.InsertAllOnSubmit(caches);
            _db.SubmitChanges();
        }

        public bool clearAllMailChimpCaches()
        {
            var list =  from i in _db.MailChimpCaches select i;
            _db.MailChimpCaches.DeleteAllOnSubmit(list);
            _db.SubmitChanges();
            return true;
        }

        #region Depreciated

        public IQueryable<Person> listAll()
        {

            return from i in _db.Persons
                   select new Person
                   {
                       ID = i.ID,
                       UserID = i.UserID,
                       firstName = i.firstName,
                       surname = i.surname,
                       title = i.title,
                       email = i.email,
                       address = i.address,
                       suburb = i.suburb,
                       state = i.state,
                       pcode = i.pcode,
                       phone = i.phone,
                       mobile = i.mobile,
                       isSubscribed = i.isSubscribed==1,
                       createDate = i.createDate,
                       FacebookID = i.FacebookID,
                       GoogleID = i.GoogleID,
                       GoogleUsername = i.GoogleUsername,
                       TwitterID = i.TwitterID,
                       TwitterUsername = i.TwitterUsername,
                       Type = (Person.TypeOption)i.Type
                   };

        }

        public void savePerson(Person person)
        {
            if (person != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Person add;

                if (person.ID > 0)
                {
                    add = _db.Persons.Where(x => x.ID == person.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Person();
                }

                //synch it
                add.UserID = person.UserID;
                add.firstName = person.firstName;
                add.surname = person.surname;
                add.title = person.title;
                add.email = person.email;
                add.address = person.address;
                add.suburb = person.suburb;
                add.state = person.state;
                add.pcode = person.pcode;
                add.phone = person.phone;
                add.mobile = person.mobile;
                add.isSubscribed = person.isSubscribed?1:0;
                add.createDate = person.createDate;
                add.FacebookID = person.FacebookID;
                add.GoogleID = person.GoogleID;
                add.GoogleUsername = person.GoogleUsername;
                add.TwitterID = person.TwitterID;
                add.TwitterUsername = person.TwitterUsername;
                add.Type = (int)person.Type;

                //save it
                if (add.ID == 0)
                    _db.Persons.InsertOnSubmit(add);

                _db.SubmitChanges();

                person.ID = add.ID;
            }
        }

        public bool deletePerson(Person person)
        {
            var delPerson = from o in _db.Persons
                            where o.ID == person.ID
                            select o;


            //delete the order
            _db.Persons.DeleteAllOnSubmit(delPerson);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<Signup> listAllSignups()
        {

            return from i in _db.Signups
                   select new Signup
                   {
                       ID = i.ID,
                       FirstName = i.FirstName,
                       Surname = i.Surname,
                       Email = i.Email,
                       StoreName = i.StoreName,
                       Suburb = i.Suburb,
                       createDate = i.createDate,
                       Type = i.Type,

                   };

        }

        public void saveSignup(Signup signup)
        {
            if (signup != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Signup add;

                if (signup.ID > 0)
                {
                    add = _db.Signups.Where(x => x.ID == signup.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Signup();
                }

                //synch it
                add.FirstName = signup.FirstName;
                add.Surname = signup.Surname;
                add.Email = signup.Email;
                add.StoreName = signup.StoreName;
                add.Suburb = signup.Suburb;
                add.createDate = signup.createDate;
                add.Type = signup.Type;


                //save it
                if (add.ID == 0)
                    _db.Signups.InsertOnSubmit(add);

                _db.SubmitChanges();

                signup.ID = add.ID;
            }
        }

        public bool deleteSignup(Signup signup)
        {
            var delSignup = from o in _db.Signups
                            where o.ID == signup.ID
                            select o;


            //delete the order
            _db.Signups.DeleteAllOnSubmit(delSignup);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        #endregion
    }
}