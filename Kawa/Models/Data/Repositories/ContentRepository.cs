﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Kawa.Models.Data;
using Kawa.Models.Extensions;

namespace Kawa.Models
{
    public class ContentRepository
    {
        dbDataContext _db;

        public ContentRepository()
		{
			_db = new dbDataContext();
		}

        private IQueryable<Page> selectPage(IQueryable<Data.Page> list, bool loadSr)
        {
            return list.Select(i => new Page
                    {
                        ID = i.ID,
                        name = i.name,
                        Type = (Page.TypeOption)i.Type,
                        nameMenu = i.nameMenu,
                        html = i.html,
                        html2 = i.html2,
                        html3 = i.html3,
                        html4 = i.html4,
                        createdDate = i.createdDate,
                        modifiedDate = i.modifiedDate,
                        SiteID = i.SiteID,
                        ParentID = i.ParentID,
                        orderNo = i.orderNo.HasValue ? (int)i.orderNo : 1000,
                        isHidden = i.isHidden == 1,
                        articleDate = i.articleDate,
                        SystemPage = (Page.SystemPageOption)i.SystemPage,
                        ProductID = i.ProductID,
                        siteResources = loadSr ? i.SiteResources.OrderBy(x => x.orderNo).Select(s => s.createAggregate()).ToArray() : null
                    }).OrderBy(p => p.orderNo);
        }

        public IQueryable<Page> listAllPages(bool loadSr = false)
        {

            return selectPage(from i in _db.Pages
                              select i, loadSr);

        }

        public IQueryable<Page> listDisclaimerPages(bool loadSr = false)
        {
            return selectPage(from i in _db.Pages
                              
                              select i, loadSr);

        }

        public IQueryable<Page> listPagesForTag(int TagID, bool loadSr = false)
        {
            return selectPage(from i in _db.Pages
                              where i.Tag_Pages.Any(t => t.TagID==TagID)
                              select i, loadSr);
        }

        public IQueryable<Page> listPagesForProduct(int ProductID, bool loadSr = false)
        {
            return selectPage(from i in _db.Pages
                              where i.Product_Pages.Any(t => t.ProductID == ProductID)
                              select i, loadSr);
        }

        public List<Page> listMatches(string name)
        {
            var sql = @"select ID, name, SystemPage, Type, 
                            rankNo1 = case when (name = {0} or html = {0} or html2 = {0}) then 20 
                                            when (name like '%' + {0} + '%' or html2 like '%' + {0} + '%') then 10 
                                            else 0 end
                        from page 
                            where (name like '%' + {0} + '%' or html like '%' + {0} + '%' or html2 like '%' + {0} + '%')
                             order by rankNo1 desc";

            //var query = _db.ExecuteQuery<Kawa.Models.ModelVariants.Page>(sql, name);

            List<Page> pages = _db.ExecuteQuery<Kawa.Models.ModelVariants.Page>(sql, name).Select(i =>
                                new Page
                                {
                                    ID = i.ID,
                                    name = i.name,
                                    SiteID = i.SiteID,
                                    rankNo = i.rankNo1 + i.rankNo2 + i.rankNo3,
                                    rankNo1 = i.rankNo1,
                                    rankNo2 = i.rankNo2,
                                    rankNo3 = i.rankNo3,
                                    Type = i.Type,
                                    SystemPage = i.SystemPage
                                }).ToList().Where(
                                
                                p => p.fitForSearch()
                                    
                                    
                                
                                ).ToList();
            return pages;
        }

        public List<Page> listPageMatchesQuick(string search)
        {
            return selectPage(from i in _db.Pages
                              where i.name.Contains(search)
                              select i, false).ToList().Where(p => p.fitForSearch()).ToList();

        }

        public IQueryable<Page> listRelatedPagesForPage(int PageID, bool loadSr = false)
        {
            return selectPage(from i in _db.Pages
                              where i.PageRelations1.Any(t => t.PageID == PageID)
                                 select i,
                                 loadSr);
        }

        public void savePage(Page page)
        {
            if (page != null)
            {
                //see if it's in the db
                Kawa.Models.Data.Page add;

                if (page.ID > 0)
                {
                    add = _db.Pages.Where(x => x.ID == page.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Page();
                }

                //synch it
                add.name = page.name;
                add.Type = (int)page.Type;
                add.nameMenu = page.nameMenu;
                add.html = page.html;
                add.html2 = page.html2;
                add.html3 = page.html3;
                add.html4 = page.html4;
                add.createdDate = page.createdDate;
                add.modifiedDate = page.modifiedDate;
                add.SiteID = page.SiteID;
                add.ParentID = page.ParentID;
                add.orderNo = page.orderNo;
                add.isHidden = page.isHidden ? 1 : 0;
                add.articleDate = page.articleDate;
                add.SystemPage = (int?)page.SystemPage;
                add.ProductID = page.ProductID;

                //save it
                if (add.ID == 0)
                    _db.Pages.InsertOnSubmit(add);

                _db.SubmitChanges();

                page.ID = add.ID;

                if (page.tags == null)
                    return;

                var delTags = from o in _db.Tag_Pages
                              where o.PageID == page.ID
                              select o;
                _db.Tag_Pages.DeleteAllOnSubmit(delTags);
                _db.SubmitChanges();

                if (page.tags != null && page.tags.Count() > 0)
                {
                    List<Kawa.Models.Data.Tag_Page> tags = page.tags.Select(t => new Kawa.Models.Data.Tag_Page() { PageID = page.ID, TagID = t.ID }).ToList();
                    _db.Tag_Pages.InsertAllOnSubmit(tags);
                    _db.SubmitChanges();
                }

                var delPages = from o in _db.PageRelations
                               where o.PageID == page.ID
                               select o;
                _db.PageRelations.DeleteAllOnSubmit(delPages);
                _db.SubmitChanges();

                if (page.pages != null && page.pages.Count() > 0)
                {
                    List<Kawa.Models.Data.PageRelation> pages = page.pages.Select(t => new Kawa.Models.Data.PageRelation() { PageID = page.ID, RelatedPageID = t.ID, Type = 0 }).ToList();
                    _db.PageRelations.InsertAllOnSubmit(pages);
                    _db.SubmitChanges();
                }
            }
        }

        public bool deletePage(Page page)
        {
            var delTags = from o in _db.Tag_Pages
                          where o.PageID == page.ID
                          select o;
            _db.Tag_Pages.DeleteAllOnSubmit(delTags);
            _db.SubmitChanges();

            var delSlots = from o in _db.Slots
                          where o.PageID == page.ID
                          select o;
            _db.Slots.DeleteAllOnSubmit(delSlots);
            _db.SubmitChanges();

            var delPageRelations = from o in _db.PageRelations
                           where o.PageID == page.ID || o.RelatedPageID == page.ID
                           select o;
            _db.PageRelations.DeleteAllOnSubmit(delPageRelations);
            _db.SubmitChanges();

            var delPage = from o in _db.Pages
                          where o.ID == page.ID
                          select o;


            //delete the order
            _db.Pages.DeleteAllOnSubmit(delPage);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        private IQueryable<Tag> selectTag(IQueryable<Data.Tag> list, bool loadSr = false)
        {
            return list.Select(i => new Tag
            {
                ID = i.ID,
                name = i.name,
                Type = (Tag.TypeOption)i.Type,
                SystemTag = (Tag.SystemTagOption)i.SystemTag,
                ParentID = i.ParentID,
                orderNo = i.orderNo,
                isHidden = i.isHidden == 1,
                OrderByType = (Tag.OrderByTypeOption)i.OrderByType,
                nameUrl = i.nameUrl,
                colour = i.colour,
                oldID = i.oldID.HasValue ? (int)i.oldID : 0,
                isInverseTextColour = i.isInverseTextColour,
                aliases = i.aliases,
                aliasesUrl = i.aliasesUrl,
                html = i.html,
                metaDescription = i.metaDescription,
                hasChildren = i.Tags.Any(),
                siteResources = loadSr ? i.SiteResources.Select(s => s.createAggregate()).ToArray() : null,
                noOfProducts = i.Tag_Products.Count(),
                parent = i.Tag1 != null ? new Tag()
                {
                    ID = i.Tag1.ID,
                    name = i.Tag1.name,
                    Type = (Tag.TypeOption)i.Tag1.Type,
                    SystemTag = (Tag.SystemTagOption)i.Tag1.SystemTag
                } : null
            });

        }

        private IQueryable<PrescriptionTag> selectPrescriptionTag(IQueryable<Data.PrescriptionTag> list, bool loadSr = false)
        {
            return list.Select(i => new PrescriptionTag
            {
                        ID = i.ID,
						name = i.name,
			            Type = (PrescriptionTag.TypeOption)i.Type,
                        SystemTag = (PrescriptionTag.SystemTagOption)i.SystemTag,
			            ParentID = i.ParentID,
                        orderNo = i.orderNo,
                        isHidden = i.isHidden==1,
                        OrderByType = (PrescriptionTag.OrderByTypeOption)i.OrderByType,
                        nameUrl = i.nameUrl,
                        colour = i.colour,
                        isInverseTextColour = i.isInverseTextColour,
                        aliases = i.aliases,
                        aliasesUrl = i.aliasesUrl,
                        html = i.html,
                        metaDescription = i.metaDescription,
                        hasChildren = i.PrescriptionTags.Any(),
                        siteResources = loadSr ? i.SiteResources.Select(s => s.createAggregate()).ToArray() : null,
                        noOfProducts = i.PrescriptionTag_Products.Count(),
                       

            });

        }

        public IQueryable<Tag> listAllTagsWithProductList(bool loadSr = false)
        {

            return selectTag(from i in _db.Tags
                             where (i.Type == (int)Tag.TypeOption.PRODUCT_SECTION || 
                                    i.Type == (int)Tag.TypeOption.BRAND_SECTION ||
                                    i.Type == (int)Tag.TypeOption.ADHOC_PRODUCT_TAGS)
                                    && !(i.Type == (int)Tag.TypeOption.PRODUCT_SECTION && i.Tag1 != null && i.Tag1.Type == (int)Tag.TypeOption.SUPER_SECTION)
                             select i, loadSr);

        }

        public IQueryable<Tag> listAllTags(bool loadSr = false)
		{
			
			return selectTag(from i in _db.Tags.Where(x=>x.Type!=7)
                             select i, loadSr);
			
		}

        public IQueryable<PrescriptionTag> listAllPerscriptionTags(bool loadSr = false)
        {

            return selectPrescriptionTag(from i in _db.PrescriptionTags.Where(x => x.Type == 7 || x.ID == 6379)
                             select i, loadSr);

        }

        public IQueryable<Tag> listAllTagsForProductsWithTag(Tag tag)
        {

            return selectTag(from i in _db.Tags
                             where i.Tag_Products.Any(tp => 
                                 tp.Product.Tag_Products.Any(tp2 => tp2.TagID == tag.ID) ||
                                 tp.Product.Tag_Products.Any(tp2 => tp2.Tag.ParentID == tag.ID)
                                 )
                             select i);

        }

        public IQueryable<Tag> listAllTagsForPage(Page page)
        {
            return selectTag(from i in _db.Tags
                   where i.Tag_Pages.Any(tp => tp.PageID == page.ID)
                   select i);
        }


        public IQueryable<Tag> listAllTagsForProduct(Product product)
        {
            return selectTag(from i in _db.Tags
                             where i.Tag_Products.Any(tp => tp.ProductID == product.ID)
                             select i);
        }
        public IQueryable<Page> listAllPagesForProduct(Product product)
        {
            return selectPage(from i in _db.Pages
                             where i.Page_Products.Any(tp => tp.ProductID == product.ID)
                             select i,false);
        }

        public List<Tag> listAllTagsForBrandProducts(Tag brand)
        {
            Models.Data.Tag tag = new Data.Tag() { ID = brand.ID, name = brand.name };
            List<int> tagids = listAllTagsIDSForBrandsProducts(new List<Models.Data.Tag>(){ tag });

            List<Tag> tagsdata = selectTag(from i in _db.Tags
                                           where tagids.Take(2000).Contains(i.ID) && i.Type == 1 && i.isHidden==0
                   select i).ToList();

            return tagsdata;
        }

        public List<int> listAllTagsIDSForBrandsProducts(List<Models.Data.Tag> brands)
        {


            /*
             * 
      select * from Tag
      where ID in   (
      select TagID from Tag_Product where productID in 	(select ProductID from Tag_product where TagID in(4908,4907)
      ))*/

            List<int> products = new List<int>();
            foreach (Models.Data.Tag b in brands)
            {
                List<int> bproducts = _db.Tag_Products.Where(x => x.Tag.Type != 7 && x.TagID == b.ID && x.Product.isHidden==0).Select(x => x.ProductID).ToList();
                products.AddRange(bproducts);

               
            }

            ///List<int> checkforhidden = _db.Products.Where(x => products.Contains(x.ID) && x.isHidden==0).Select(x => x.ID).ToList();
            List<Tag_Product> tag_prods = _db.Tag_Products.Where(x => products.Contains(x.ProductID)).ToList();

            IQueryable<int> tps = _db.Tag_Products.Where(x => products.Contains(x.ProductID)).Select(x => x.TagID);

            return tps.ToList();
        }

        public IQueryable<Tag> listAllTagsForProduct(Product product, Tag.TagRelationType relation)
        {
            return selectTag(from i in _db.Tags
                             where i.Tag_Products.Any(tp => tp.ProductID == product.ID && tp.Type == (int)relation)
                             select i);
        }

        public IQueryable<Tag> listAllTagsForProductsWithTag(Tag tag, Tag.TypeOption tagType)
        {
            return selectTag(from i in _db.Tags
                             where i.Tag_Products.Any(tp => tp.Product.Tag_Products.Any(x => x.TagID==tag.ID))
                             && i.Type == (int)tagType
                             select i);
        }

        public IQueryable<Tag> listAllTagRelations(int TagID, bool loadSr = false)
        {

            return selectTag(from i in _db.Tags
                             where _db.Tag_Tags.Any(x => x.RelatedTagID == i.ID && x.TagID == TagID)
                             select i, loadSr);

        }

        public void ResetStructure(int TagID)
        {
            var delete =  selectTag(from i in _db.Tags
                             where i.ID == TagID
                             select i);
            deleteTag(delete.FirstOrDefault());
        }


        public void ReplicateStucture(int RecipientTag, int SourceTag)
        {
            Models.Data.Tag recipient = _db.Tags.Where(x => x.ID == RecipientTag).FirstOrDefault();
            foreach(Models.Data.Tag t in _db.Tags.Where(x => x.ParentID == SourceTag))
            {
                Models.Data.Tag newtag = new Models.Data.Tag(); ///_db.Tags.Where(x => x.name == t.name && x.ParentID == RecipientTag).FirstOrDefault();/// new Models.Data.Tag();
                newtag.ParentID = RecipientTag;

                foreach(Data.Tag tx in _db.Tags.Where(x => x.name == t.name && x.ParentID == RecipientTag))
                {
                    tx.isHidden = 1;
                }

                newtag.name = t.name;
                newtag.nameUrl = (recipient.name +"-"+ t.name).ToSafeString();                
                newtag.html = t.html;
                newtag.orderNo = t.orderNo;                
                newtag.aliases = t.aliases;
                newtag.Type = t.Type;
                newtag.SystemTag = t.SystemTag;
                newtag.colour = t.colour;
                
     
                    _db.Tags.InsertOnSubmit(newtag);
                    ///newtag = _db.Tags.Where(x => x.nameUrl == newtag.nameUrl).FirstOrDefault();
           
                    

                _db.SubmitChanges();

                ProductRepository _productRep = new ProductRepository();

                ///also copy product rels..
                foreach (Tag_Product tp in t.Tag_Products)
                {
                    _productRep.deleteTagRelationShip(tp.ProductID, newtag.ID);
                    _productRep.saveTagRelationShip(tp.ProductID, newtag.ID, tp.Rank);

                    ///newtag.Tag_Products.Add(new Tag_Product() { TagID = newtag.ID, ProductID = tp.ProductID, Type = tp.Type });
                }
                
                SiteResourceRepository _srRep = new SiteResourceRepository();
                var srService = new SiteResourceService();

                List<SiteResource> sourceSRS = _srRep.listAll().Where(x => x.ObjectID == t.ID && x.ObjectType == SiteResourceOwner.ObjectTypeOption.TAG).ToList();
                foreach (SiteResource sr in sourceSRS)
                {
                   
                    srService.CloneTagSiteResource(sr, newtag.ID, _srRep);                   
                }

                ReplicateStucture(newtag.ID, t.ID);

                _db.SubmitChanges();

            }
        }

        public void saveTag(Tag tag)
		{
			if (tag != null)
			{

				//see if it's in the db
				Kawa.Models.Data.Tag add;

				if (tag.ID > 0)
				{
					add = _db.Tags.Where(x => x.ID == tag.ID).SingleOrDefault();
				}
				else
				{
					add = new Kawa.Models.Data.Tag();
                    add.nameUrl = ModelTransformations.EncodeStringToUrl(tag.name);
                }

				//synch it
				add.name = tag.name;
				add.Type = (int)tag.Type;
				add.ParentID = tag.ParentID;
                add.SystemTag = (int?)tag.SystemTag;
                add.orderNo = tag.orderNo;
                add.isHidden = tag.isHidden ? 1 : 0;
                add.OrderByType = (int)tag.OrderByType;
                if(tag.Type != Tag.TypeOption.CAR_PRESCRIPTION_CAT)///do not change this stick with the scripted version
                {
                    add.nameUrl = ModelTransformations.EncodeStringToUrl(tag.name);
                }
                
                add.colour = tag.colour;
                add.isInverseTextColour = tag.isInverseTextColour;
                add.aliases = tag.aliases;
                add.aliasesUrl = !string.IsNullOrWhiteSpace(tag.aliases) ? tag.aliases.ToSafeString() : null;
                add.html = tag.html;
                add.metaDescription = tag.metaDescription;

                //save it
                if (add.ID == 0)
					_db.Tags.InsertOnSubmit(add);

				_db.SubmitChanges();

				tag.ID = add.ID;
			}
		}

        public void savePrescriptionTag(PrescriptionTag tag)
        {
            if (tag != null)
            {

                //see if it's in the db
                Kawa.Models.Data.PrescriptionTag add;

                if (tag.ID > 0)
                {
                    add = _db.PrescriptionTags.Where(x => x.ID == tag.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.PrescriptionTag();
                    add.nameUrl = ModelTransformations.EncodeStringToUrl(tag.name);
                }

                //synch it
                add.name = tag.name;
                add.Type = (int)tag.Type;
                add.ParentID = tag.ParentID;
                add.SystemTag = (int?)tag.SystemTag;
                add.orderNo = tag.orderNo;
                add.isHidden = tag.isHidden ? 1 : 0;
                add.OrderByType = (int)tag.OrderByType;
                if (tag.Type != PrescriptionTag.TypeOption.CAR_PRESCRIPTION_CAT)///do not change this stick with the scripted version
                {
                    add.nameUrl = ModelTransformations.EncodeStringToUrl(tag.name);
                }

                add.colour = tag.colour;
                add.isInverseTextColour = tag.isInverseTextColour;
                add.aliases = tag.aliases;
                add.aliasesUrl = !string.IsNullOrWhiteSpace(tag.aliases) ? tag.aliases.ToSafeString() : null;
                add.html = tag.html;
                add.metaDescription = tag.metaDescription;

                //save it
                if (add.ID == 0)
                    _db.PrescriptionTags.InsertOnSubmit(add);

                _db.SubmitChanges();

                tag.ID = add.ID;
            }
        }
        public bool deleteTag(Tag tag)
        {
            var delTagPages = from o in _db.Tag_Pages
                         where o.TagID == tag.ID
                         select o;
            _db.Tag_Pages.DeleteAllOnSubmit(delTagPages);

            var delTagProduct = from o in _db.Tag_Products
                              where o.TagID == tag.ID
                              select o;
            _db.Tag_Products.DeleteAllOnSubmit(delTagProduct);

            var delStatTag = from o in _db.Stats
                             where o.TagID == tag.ID
                             select o;
            _db.Stats.DeleteAllOnSubmit(delStatTag);

            var delTag = from o in _db.Tags
                            where o.ID == tag.ID
                           select o;
            _db.Tags.DeleteAllOnSubmit(delTag);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public void saveTagRelation(int TagID, int RelatedTagID)
        {
            if (_db.Tag_Tags.Any(x => x.TagID == TagID && x.RelatedTagID == RelatedTagID))
                return;

            _db.Tag_Tags.InsertOnSubmit(new Kawa.Models.Data.Tag_Tag()
            {
                TagID = TagID,
                RelatedTagID = RelatedTagID
            });
            _db.SubmitChanges();
        }

        public bool deleteTagRelation(int TagID, int RelatedTagID)
        {
            var delTagRel = from o in _db.Tag_Tags
                         where o.TagID == TagID && o.RelatedTagID == RelatedTagID
                         select o;
            _db.Tag_Tags.DeleteAllOnSubmit(delTagRel);
            _db.SubmitChanges();
            return true;
        }

        public IQueryable<Slot> listAllSlots(bool loadPage = false, bool loadSr = false)
        {

            return from i in _db.Slots
                   select new Slot
                   {
                       ID = i.ID,
                       Type = i.Type,
                       Spot = (Slot.SpotOption)i.Spot,
                       PageID = i.PageID,
                       orderNo = i.orderNo,
                       page = (loadPage && i.Page != null) ? new Page()
                            {
                                ID = i.Page.ID,
                                name = i.Page.name,
                                Type = (Page.TypeOption)i.Page.Type,
                                nameMenu = i.Page.nameMenu,
                                html = i.Page.html,
                                html2 = i.Page.html2,
                                html3 = i.Page.html3,
                                html4 = i.Page.html4,
                                isHidden = i.Page.isHidden == 1,
                                articleDate = i.Page.articleDate,
                                SystemPage = (Page.SystemPageOption)i.Page.SystemPage,
                                siteResources = loadSr ? i.Page.SiteResources.Select(s => s.createAggregate()).ToArray() : null
                            } : null
                   };

        }

        public void saveSlot(Slot slot)
        {
            if (slot != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Slot add;

                if (slot.ID > 0)
                {
                    add = _db.Slots.Where(x => x.ID == slot.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Slot();
                }

                //synch it
                add.Type = slot.Type;
                add.Spot = (int)slot.Spot;
                add.PageID = slot.PageID;
                add.orderNo = slot.orderNo;

                //save it
                if (add.ID == 0)
                    _db.Slots.InsertOnSubmit(add);

                _db.SubmitChanges();

                slot.ID = add.ID;
            }
        }

        public bool deleteSlot(Slot slot)
        {
            var delSlot = from o in _db.Slots
                          where o.ID == slot.ID
                          select o;


            //delete the order
            _db.Slots.DeleteAllOnSubmit(delSlot);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public void clearCache(int Tag1ID, int? Tag2ID)
        {
            var del = from o in _db.ListCaches
                          where o.Tag1ID == Tag1ID && (!Tag2ID.HasValue || o.Tag2ID == Tag2ID)
                          select o;
            _db.ListCaches.DeleteAllOnSubmit(del);
            _db.SubmitChanges();
        }

        public void saveCache(Data.ListCache record)
        {
            _db.ListCaches.InsertOnSubmit(record);
            _db.SubmitChanges();
        }
	}

    public static class ContentRepositoryExtensions
    {
        public static Page createAggregate(this Data.Page i)
        {
            return new Page()
            {
                ID = i.ID,
                name = i.name,
                Type = (Page.TypeOption)i.Type,
                nameMenu = i.nameMenu,
                html = i.html,
                html2 = i.html2,
                html3 = i.html3,
                html4 = i.html4,
                createdDate = i.createdDate,
                modifiedDate = i.modifiedDate,
                SiteID = i.SiteID,
                ParentID = i.ParentID,
                orderNo = i.orderNo.HasValue ? (int)i.orderNo : 1000,
                isHidden = i.isHidden == 1,
                articleDate = i.articleDate,
                SystemPage = (Page.SystemPageOption?)i.SystemPage,
                ProductID = i.ProductID
            };
        }

        public static Tag createAggregate(this Data.Tag t)
        {
            return new Tag()
            {
                ID = t.ID,
                name = t.name,
                nameUrl = t.nameUrl,
                Type = (Models.Tag.TypeOption)t.Type,
                colour = t.colour,
                SystemTag = (Tag.SystemTagOption?)t.SystemTag,
                ParentID = t.ParentID,
                orderNo = t.orderNo,
                oldID = t.oldID.HasValue ? (int)t.oldID : 0,
                isHidden = (t.isHidden == 0 ? false :true)
            };
        }
    }
}