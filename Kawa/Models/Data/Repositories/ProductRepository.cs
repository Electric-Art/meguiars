﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Kawa.Models.Data;
using Kawa.Models.Extensions;

namespace Kawa.Models
{
    public class ProductRepository
    {
        dbDataContext _db;
		
		public ProductRepository()
		{
			_db = new dbDataContext();
		}

        private IQueryable<Product> selectProduct(IQueryable<Data.Product> list,bool loadSr,bool loadVariants,bool orderByName = true, int? tagrelationshiprank = null, int? pagerelationshiprank = null, int ? prescriptiontagrelationshiprank = null)
        {
            var results = list.Select(i => new Product
            {
                ID = i.ID,
                name = i.name,
                nameUrl = i.nameUrl,
                titleName = i.titleName,
                menuName = i.menuName,
                intro = i.intro,
                blurb = i.blurb,
                activeIngredients = i.activeIngredients,
                formulation = i.formulation,
                dosage = i.dosage,
                warnings = i.warnings,
                specifications = i.specifications,
                freeFrom = i.freeFrom,
                interactions = i.interactions,
                availability = i.availability,
                sku = i.sku,
                price = i.price,
                specialPrice = i.specialPrice,
                aliases = i.aliases,
                aliasesUrl = i.aliasesUrl,
                createdDate = i.createDate,
                modifiedDate = i.modifiedDate,
                isHidden = i.isHidden == 1,
                googleCat = i.googleCat,
                gtin = i.gtin,
                nationalOnly = i.nationalOnly,
                specialType = (Product.SpecialTypeOption?)i.specialType,
                orderOverride = i.orderOverride,
                bingCat = i.bingCat,
                directions = i.directions,
                noAdded = i.noAdded,
                hideBlurb = i.hideBlurb == 1,
                packNotes = i.packNotes,
                austL = i.austL,
                poName = i.poName,
                HasVariant = i.Variants.Any(),
                HasVariantOutOfStock = i.Variants.Any() && i.Variants.First().isHidden==1,
                HasVariantPreOrder = i.Variants.Any() && i.Variants.First().preOrderShipDate.HasValue,
                VariantStockNo = i.Variants.Any() ? i.Variants.First().stockNo  : null,
                VariantMaxQuantity = i.Variants.Any() ? i.Variants.First().maxQuantity : null,
                VariantMinQuantity = i.Variants.Any() ? i.Variants.First().minQuantity : null,
                variants = loadVariants ? i.Variants.Select(v => v.createAggregate()).ToList() : null,
                promotion = i.Promotions.Where(x=>x.SystemType==2).Select(v => v.createAggregate()).FirstOrDefault(),
                siteResources = loadSr ? i.SiteResources.Select(s => s.createAggregate()).ToArray() : null,
                markertags = i.Tag_Products.Where(tp => tp.Tag.Type == (int)Tag.TypeOption.ADHOC_PRODUCT_GROUP).Select(tp => tp.Tag.createAggregate()).ToList(),
                brandtags = i.Tag_Products.Where(tp => tp.Tag.Type == (int)Tag.TypeOption.BRAND_SECTION).Select(tp => tp.Tag.createAggregate()).ToList(),
                disclaimers = i.Product_Pages.Select(x => new Page() { SystemPage = (Page.SystemPageOption) x.Page.SystemPage, isHidden = x.Action == (int)Product_Page.ActionOption.Suppress }).ToList(),
                ProductTagRelationShipRank = tagrelationshiprank != null ? i.Tag_Products.Where(tp => tp.TagID == tagrelationshiprank).Select(tp => tp.Rank).FirstOrDefault() : 0,
                ProductPageRelationShipRank = pagerelationshiprank != null ? i.Page_Products.Where(tp => tp.PageID == pagerelationshiprank).Select(tp => tp.Rank).FirstOrDefault() : 0,
                ProductPrescriptionTagRelationShipRank = prescriptiontagrelationshiprank != null ? i.PrescriptionTag_Products.Where(tp => tp.TagID == prescriptiontagrelationshiprank).Select(tp => tp.Rank).FirstOrDefault() : 1,
                metakeywords = i.metakeywords,
                metadescription = i.metadescription,
                sHero = i.sHero,
                sFiles = i.sFiles,
                sThumb = i.sThumb
            });

            if(orderByName)
                results = results.OrderBy(p => p.name);

            return results;
        }

        public IQueryable<Product> listAllProducts(bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                              select i, 
                              loadSr, loadVariants);
        }

        public IQueryable<Product> listAllProductsWithCat(bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.Tag_Products.Any(t => t.Tag.Type == 1)
                                 select i,
                                 loadSr, loadVariants);
        }

        public IQueryable<Product> listAllProductsWithPages(bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.Pages.Any()
                                 select i,
                                 loadSr, loadVariants);
        }

        public IQueryable<Product> listAllProductsWithPage(int PageID, bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.Product_Pages.Any(x => x.PageID == PageID)
                                 select i,
                                 loadSr, loadVariants);
        }


        

        public IQueryable<Product> listProductsForChildTags(int TagID, bool loadSr = false, bool loadVariants = false)
        {

            List<int> tagids = _db.Tags.Where(x=>x.ParentID == TagID).ToList().Select(x=>x.ID).ToList();


            return selectProduct(from i in _db.Products
                                           where i.Tag_Products.Any(tp => tagids.ToList().Contains(tp.TagID) && i.isHidden == 0)
                                           select i,true,true);
            
        }


        public int getProductTagRank(int id, int tagID)
        {
            return _db.Tag_Products.Where(x => x.ProductID == id && x.TagID == tagID).FirstOrDefault().Rank;
        }
        public int getProductPrescriptionTagRank(int id, int tagID)
        {
            return _db.PrescriptionTag_Products.Where(x => x.ProductID == id && x.TagID == tagID).FirstOrDefault().Rank;
        }
        public IQueryable<Product> getRelatedProducts(List<Basket> basket)
        {
            List<Product> prods = new List<Product>();
            foreach(Basket b in basket)
            {
                prods.Add(b.Product);
            }
            return getRelatedProducts(prods);
        }

        public IQueryable<Product> getRelatedProducts(List<Product> products)
        {

            List<string> relatedskus = new List<string>();
            foreach(Product px in products)
            {
                relatedskus.AddRange(from i in _db.RelatedProducts.Where(x => x.sku == px.sku).Select(x => x.relatedsku) select i);
            }
            

            return selectProduct(from i in _db.Products
                                 where relatedskus.Contains(i.sku) && i.isHidden == 0
                                 select i,
                              true, true);
        }
        public IQueryable<Product> listProductsForPage(int PageID, bool loadSr = false, bool loadVariants = false)
        {
            IQueryable<Product> results = selectProduct(from i in _db.Products
                                                        where i.Page_Products.Any(t => t.PageID == PageID)
                                                        select i,
                            loadSr, loadVariants, false,null,PageID).OrderBy(x => x.ProductPageRelationShipRank);

            return results;
        }
        public IQueryable<Product> listProductsForTag(int TagID, bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.Tag_Products.Any(t => t.TagID == TagID ||  t.Tag.ParentID == TagID)
                              select i,
                              loadSr, loadVariants);
        }
        public IQueryable<Product> listProductsForPrescriptionTag(int TagID, bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.PrescriptionTag_Products.Any(t => t.TagID == TagID || t.PrescriptionTag.ParentID == TagID)
                                 select i,
                              loadSr, loadVariants,false,null,null,TagID);
        }
        public IQueryable<Product> listProductsForPrescriptionTagNotIncludingParents(int TagID, bool loadSr = false, bool loadVariants = false)
        {
            IQueryable<Product> results = selectProduct(from i in _db.Products
                                                        where i.PrescriptionTag_Products.Any(t => t.TagID == TagID)
                                                        select i,
                              loadSr, loadVariants, false, null, null, TagID).OrderBy(x => x.ProductPrescriptionTagRelationShipRank);

            return results;
        }

        public IQueryable<Product> listProductsForTagNotIncludingParents(int TagID, bool loadSr = false, bool loadVariants = false)
        {
            IQueryable<Product> results = selectProduct(from i in _db.Products
                                 where i.Tag_Products.Any(t => t.TagID == TagID)
                                 select i,
                              loadSr, loadVariants, false,TagID).OrderBy(x=>x.ProductTagRelationShipRank);
           
            return results;
        }
        public IQueryable<Product> listProductsForTagIntersection(int Tag1ID, int Tag2ID, bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.Tag_Products.Any(t => t.TagID == Tag1ID || t.Tag.ParentID == Tag1ID) && i.Tag_Products.Any(t => t.TagID == Tag2ID || t.Tag.ParentID == Tag2ID)
                                 select i,
                              loadSr, loadVariants);
        }

        public IQueryable<Product> listProductsForTagWithFilter(int TagID, bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.Tag_Products.Any(t => t.TagID == TagID || t.Tag.ParentID == TagID)
                                 &&
                                 i.Tag_Products.Any(t => _db.Tag_Tags.Any(y => y.TagID == TagID && y.RelatedTagID == t.TagID))
                                 select i,
                              loadSr, loadVariants);
        }

        public IQueryable<Product> listProductsForTagIntersectionWithFilter(int Tag1ID, int Tag2ID, bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.Tag_Products.Any(t => t.TagID == Tag1ID) && i.Tag_Products.Any(t => t.TagID == Tag2ID || t.Tag.ParentID == Tag2ID)
                                 &&
                                 i.Tag_Products.Any(t => _db.Tag_Tags.Any(y => y.TagID == Tag1ID && y.RelatedTagID == t.TagID))

                                 select i,
                              loadSr, loadVariants);
        }

        public IQueryable<Product> listProductsForAlgorithmWithFilter(Tag.SystemTagOption SystemTag, int TagID, bool loadSr = false, bool loadVariants = false)
        {
            var filtered = (from i in _db.Products
                                 where !i.Tag_Products.Any(t => _db.Tag_Tags.Any(y => y.TagID == TagID && y.RelatedTagID == t.TagID))
                                 select i);

            filtered = addAlgo(SystemTag, filtered);

            return selectProduct(filtered,
                              loadSr, loadVariants, false);
        }

        public IQueryable<Product> listProductsForAlgorithmIntersectionWithFilter(Tag.SystemTagOption SystemTag, int Tag1ID, int Tag2ID, bool loadSr = false, bool loadVariants = false)
        {
            var filtered = (from i in _db.Products
                                 where i.Tag_Products.Any(t => t.TagID == Tag2ID || t.Tag.ParentID == Tag2ID)
                                 &&
                                 !i.Tag_Products.Any(t => _db.Tag_Tags.Any(y => y.TagID == Tag1ID && y.RelatedTagID == t.TagID))
                                 select i);

            filtered = addAlgo(SystemTag, filtered);

            return selectProduct(filtered,
                              loadSr, loadVariants, false);
        }

        private IQueryable<Data.Product> addAlgo(Tag.SystemTagOption SystemTag, IQueryable<Data.Product> products)
        {
            switch (SystemTag)
            {
                case Tag.SystemTagOption.PRODUCT_CAT_BEST_SELLERS:
                    products = products.Where(x => x.Variants.Any() &&
                                                    x.Variants.First().Baskets.Any(y => (y.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER ||
                                                                                        y.Order.SystemStatus == (int)Order.SystemStatusOption.SENT) &&
                                                                                        y.Order.OrderDate >= DateTime.Now.AddYears(-1)));
                    products = products.OrderByDescending(x => x.Variants.First().Baskets.Sum(y => y.Price * y.Quantity)).Take(25);
                    break;
                case Tag.SystemTagOption.SPECIALS:
                    products = products.Where(x => x.Variants.Any());
                    products = products.OrderByDescending(x => x.createDate).Take(25);
                    break;
                case Tag.SystemTagOption.PRODUCT_CAT_TRENDING_NOW:
                    products = products.Where(x => x.Variants.Any() &&
                                                    x.Variants.First().Baskets.Any(y => (y.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER ||
                                                                                        y.Order.SystemStatus == (int)Order.SystemStatusOption.SENT) &&
                                                                                        y.Order.OrderDate >= DateTime.Now.AddDays(-7)));
                    products = products.OrderByDescending(x => x.Variants.First().Baskets.Sum(y => y.Price * y.Quantity)).Take(25);
                    break;
            }
            return products;
        }

        public IQueryable<Product> listProductsFromCachWithFilter(Tag.SystemTagOption SystemTag, int TagID, bool loadSr = false, bool loadVariants = false)
        {
            var cached = (from i in _db.ListCaches
                            where i.Tag1ID == TagID && !i.Tag2ID.HasValue
                          orderby i.ID
                            select i.Product);

            return selectProduct(cached,
                              loadSr, loadVariants, false);
        }

        public IQueryable<Product> listProductsFromCacheIntersectionWithFilter(Tag.SystemTagOption SystemTag, int Tag1ID, int Tag2ID, bool loadSr = false, bool loadVariants = false)
        {
            var filtered = (from i in _db.ListCaches
                            where i.Tag1ID == Tag1ID && i.Tag2ID == Tag2ID
                            orderby i.ID
                            select i.Product);

            return selectProduct(filtered,
                              loadSr, loadVariants, false);
        }


        public IQueryable<Product> listProductsForSymptom(int SymptomID, bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.Symptom_Products.Any(t => t.SymptomID == SymptomID)
                                 select i,
                                 loadSr, loadVariants);
        }

        public IQueryable<Product> listRelatedProductsForProduct(int ProductID, bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where i.ProductRelations1.Any(t => t.ProductID == ProductID)
                                 select i,
                                 loadSr, loadVariants);
        }

        public List<Product> listMatches(string search, int? maxReturned, bool isHiddden = false)
        {
            search = search.Replace("'s", "s").Replace("'S", "S");
            if (string.IsNullOrEmpty(search) || search.Length < 2)
                return new List<Product>();

            List<Product> products = listProductMatchesQuickStartsWith(search).Where(x => x.isHidden==isHiddden && x.HasVariant).ToList();
            if (maxReturned.HasValue && products.Count > maxReturned)
                return products.Take((int)maxReturned).ToList();

            products.AddRange(listProductMatchesQuickContains(search).Where(x => !products.Select(y => y.ID).Contains(x.ID) && x.isHidden==isHiddden && x.HasVariant));
            if(maxReturned.HasValue && products.Count > maxReturned)
                return products.Take((int)maxReturned).ToList();

            products.AddRange(listAliasMatchesQuick(search).Where(x => !products.Select(y => y.ID).Contains(x.ID) && x.isHidden==isHiddden && x.HasVariant));
            if (maxReturned.HasValue && products.Count > maxReturned)
                return products.Take((int)maxReturned).ToList();

            var keywords = search.Split(' ').ToList();
            var fuzzyProductPool = from i in _db.Products where i.isHidden==(isHiddden?1:0) && i.Variants.Count()>0 select i;
            var fuzzyReturned = containsAllOfTheseKeywords(fuzzyProductPool, keywords);
            products.AddRange(fuzzyReturned.Where(x => !products.Select(y => y.ID).Contains(x.ID)));

            if (maxReturned.HasValue && products.Count > maxReturned)
                return products.Take((int)maxReturned).ToList();

            return products;
            /*

            return selectProduct(from i in _db.Products
                          where i.name.Contains(search) ||
                            searchstrings.Any(word => i.name.ToLower().Contains(word.ToLower()))
                          select i,
                                 true, true).ToList();
            
            var sql = @"select ID, name, 
                            rankNo1 = case when (name = {0} or blurb = {0}) then 20 
                                            when (name like '%' + {0} + '%' or blurb like '%' + {0} + '%') then 10 
                                            else 0 end
                        from product 
                            where (name like '%' + {0} + '%' or blurb like '%' + {0} + '%')
                            or exists(select * from Symptom_Product where ProductID=product.ID and SymptomID in (select ID from Symptom where name like '%' + {0} + '%'))
                             order by rankNo1 desc";

            //var query = _db.ExecuteQuery<Kawa.Models.ModelVariants.Page>(sql, new Object[] { name });

            List<Product> products = _db.ExecuteQuery<Kawa.Models.Data.Product>(sql, new Object[] { name }).Select(i =>
                                new Product
                                {
                                    ID = i.ID,
                                    name = i.name
                                }).ToList();
            return products;
             * */
        }

        public List<Product> containsAllOfTheseKeywords(IQueryable<Data.Product> qry, List<string> keywords)
        {
            foreach (var keyw in keywords)
                qry = qry.Where(obj => obj.name == keyw ||
                                      obj.name.IndexOf(keyw) != -1 ||
                                      obj.aliases == keyw ||
                                      obj.aliases.IndexOf(keyw) != -1);

            return selectProduct(qry,true,true,false).ToList();
        }

        public List<Product> containsOneOfTheseKeywords(IQueryable<Data.Product> qry, List<string> keywords)
        {
            List<List<Product>> parts = new List<List<Product>>();

            foreach (string keyw in keywords)
                parts.Add(
                    selectProduct(
                                from obj in qry
                                where obj.name == keyw ||
                                      obj.name.IndexOf(keyw) != -1 ||
                                      obj.aliases == keyw ||
                                      obj.aliases.IndexOf(keyw) != -1
                                select obj,true,true,false).ToList());
                    

            IEnumerable<Product> union = null;
            bool first = true;
            foreach (List<Product> part in parts)
            {
                if (first)
                {
                    union = part;
                    first = false;
                }
                else
                    union = union.Union(part);
            }

            return union.ToList();
        }

        public List<Product> listProductMatchesQuickStartsWith(string search)
        {
            return selectProduct(from i in _db.Products
                                 where i.name.Contains(search) || i.gtin.Contains(search) || i.sku.Contains(search)
                                 select i,
                                 true, true).ToList();
        }

        public List<Product> listProductMatchesQuickContains(string search)
        {
            return selectProduct(from i in _db.Products
                                 where i.name.Contains(search) || i.sku.Contains(search) || i.metakeywords.Contains(search) || i.metadescription.Contains(search)
                                 select i,
                                 true, true).ToList();
        }

        public List<Product> listAliasMatchesQuick(string search)
        {
            return selectProduct(from i in _db.Products
                                 where i.aliases.Contains(search)
                                 select i,
                                 true, true).ToList();
        }

        public IQueryable<Product> listAllProductsWithOrdersOrPromotions(bool loadSr = false, bool loadVariants = false)
        {
            return selectProduct(from i in _db.Products
                                 where  _db.Baskets.Any(x => x.Variant.ProductID==i.ID && x.OrderID.HasValue) ||
                                        _db.Promotions.Any(x => x.ProductID == i.ID)
                                 select i,
                              loadSr, loadVariants);
        }

        public IQueryable<FeedProduct> listAllFeedProducts(bool isBing)
        {
            return from i in _db.Products
                   where i.isHidden == 0 && i.Variants.Any() && (i.Variants.First().isHidden == 0 || (i.Variants.First().isHidden == 1 && i.Variants.First().preOrderShipDate.HasValue)) && ((!isBing && i.googleCat != "") || (isBing && i.bingCat != ""))
                   select new FeedProduct()
                   {
                       ID = i.ID,
                       name = i.name,
                       nameUrl = i.nameUrl,
                       sku = i.sku,
                       specifications = i.specifications,
                       price = i.Variants.First().price,
                       googleCat = i.googleCat,
                       bingCat = i.bingCat,
                       gtin = i.gtin,
                       freeShipping = i.Tag_Products.Any(tp => tp.Tag.SystemTag == (int)Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING),
                       preorder = i.Variants.First().isHidden == 1 && i.Variants.First().preOrderShipDate.HasValue,
                       preorderDate = i.Variants.First().preOrderShipDate,

                       image = i.SiteResources.Any(x => x.SiteResourceType == (int)Models.SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE) ?
                                    i.SiteResources.Where(x => x.SiteResourceType == (int)Models.SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE).Select(
                                       s => s.createAggregate()
                                    ).First() : null,
                       brand = i.Tag_Products.Where(tp => tp.Tag.Type == (int)Tag.TypeOption.BRAND_SECTION).Select(tp => tp.Tag.createAggregate()).FirstOrDefault(),
                       cats = i.Tag_Products.Where(tp => tp.Tag.Type == (int)Tag.TypeOption.PRODUCT_SECTION).Select(tp => tp.Tag.createAggregate()).ToList()
                   };
        }

        public IQueryable<FeedProduct> listAllFeedProductswithoutBrandOrSku()
        {
            return from i in _db.Products
                   where ((!i.Tag_Products.Any(tp => tp.Tag.Type == (int)Tag.TypeOption.BRAND_SECTION)) || 
                            (i.sku == null || i.sku == "") || 
                            i.googleCat == "784" ||
                            !i.Tag_Products.Any(tp => tp.Tag.Type == (int)Tag.TypeOption.PRODUCT_SECTION))
                            && i.Variants.Any()
                   select new FeedProduct()
                   {
                       ID = i.ID,
                       name = i.name,
                       nameUrl = i.nameUrl,
                       sku = i.sku,
                       specifications = i.specifications,
                       price = i.Variants.First().price,
                       googleCat = i.googleCat,
                       freeShipping = i.Tag_Products.Any(tp => tp.Tag.SystemTag == (int)Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING),
                       image = i.SiteResources.Any(x => x.SiteResourceType == (int)Models.SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE) ?
                                    i.SiteResources.Where(x => x.SiteResourceType == (int)Models.SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE).Select(
                                       s => s.createAggregate()
                                    ).First() : null,
                       brand = i.Tag_Products.Where(tp => tp.Tag.Type == (int)Tag.TypeOption.BRAND_SECTION).Select(tp => tp.Tag.createAggregate()).FirstOrDefault(),
                       cats = i.Tag_Products.Where(tp => tp.Tag.Type == (int)Tag.TypeOption.PRODUCT_SECTION).Select(tp => tp.Tag.createAggregate()).ToList()
                   };
        }

		public void saveProduct(Product product, bool saveTags, bool saveRelatedProducts, bool saveDisclaimers)
		{
			if (product != null)
			{

				//see if it's in the db
				Kawa.Models.Data.Product add;

				if (product.ID > 0)
				{
					add = _db.Products.Where(x => x.ID == product.ID).SingleOrDefault();
                    if (add.specialType.HasValue)
                        product.specialType = (Product.SpecialTypeOption?)add.specialType;
                }
				else
				{
					add = new Kawa.Models.Data.Product();
				}

				//synch it
				add.name = product.name;
                add.nameUrl = product.name.ToSafeString();
				add.titleName = product.titleName;
				add.menuName = product.menuName;
                add.intro = product.intro;
				add.blurb = product.blurb;
				add.activeIngredients = product.activeIngredients;
                add.formulation = product.formulation;
				add.dosage = product.dosage;
				add.warnings = product.warnings;
                add.specifications = product.specifications;
                add.freeFrom = product.freeFrom;
				add.interactions = product.interactions;
				add.availability = product.availability;
				add.sku = product.sku;
				add.price = product.price;
				add.specialPrice = product.specialPrice;
                add.aliases = product.aliases;
                add.aliasesUrl = !string.IsNullOrWhiteSpace(product.aliases) ? product.aliases.ToSafeString(false, false, 2083) : null;
                add.createDate = product.createdDate;
                add.modifiedDate = product.modifiedDate;
                add.isHidden = product.isHidden?1:0;
                add.googleCat = product.googleCat;
                add.gtin = product.gtin;
                add.directions = product.directions;
                add.noAdded = product.noAdded;
                add.packNotes = product.packNotes;
                add.hideBlurb = product.hideBlurb ? 1 : 0;
                add.nationalOnly = product.nationalOnly;
                add.specialType = (int?)product.specialType;
                add.orderOverride = product.orderOverride;
                add.bingCat = product.bingCat;
                add.austL = product.austL;
                add.poName = product.poName;
                add.metadescription = product.metadescription;
                add.metakeywords = product.metakeywords;
                //save it
                if (add.ID == 0)
					_db.Products.InsertOnSubmit(add);

				_db.SubmitChanges();

				product.ID = add.ID;

                if(saveTags)
                { 
                    var delTags = from o in _db.Tag_Products
                                  where o.ProductID == product.ID
                                  select o;
                    _db.Tag_Products.DeleteAllOnSubmit(delTags);
                    _db.SubmitChanges();

                    if (product.tags != null && product.tags.Count() > 0)
                    {
                        List<Kawa.Models.Data.Tag_Product> tags = product.tags.Select(t => new Kawa.Models.Data.Tag_Product() { ProductID = product.ID, TagID = t.ID }).ToList();
                        _db.Tag_Products.InsertAllOnSubmit(tags);
                        _db.SubmitChanges();
                    }
                }

                if(saveRelatedProducts)
                {
                    var delProds = from o in _db.ProductRelations
                                  where o.ProductID == product.ID
                                  select o;
                    _db.ProductRelations.DeleteAllOnSubmit(delProds);
                    _db.SubmitChanges();

                    if (product.products != null && product.products.Count() > 0)
                    {
                        List<Kawa.Models.Data.ProductRelation> products = product.products.Select(t => new Kawa.Models.Data.ProductRelation() { ProductID = product.ID, RelatedProductID = t.ID, Type = 0 }).ToList();
                        _db.ProductRelations.InsertAllOnSubmit(products);
                        _db.SubmitChanges();
                    }
                }

                if (saveDisclaimers)
                {
                    var delPages = from o in _db.Product_Pages
                                  where o.ProductID == product.ID
                                  select o;
                    _db.Product_Pages.DeleteAllOnSubmit(delPages);
                    _db.SubmitChanges();

                    if (product.disclaimers != null && product.disclaimers.Count() > 0)
                    {
                        List<Product_Page> disclaimers = product.disclaimers.Select(t => new Product_Page()
                        {
                            ProductID = product.ID,
                            PageID = t.ID,
                            ActionSetting = Product_Page.ActionOption.Engage
                        }).ToList();
                        _db.Product_Pages.InsertAllOnSubmit(disclaimers);
                        _db.SubmitChanges();
                    }
                }
            }
		}


        public void savePageRelationShip(int ProductID, int PageID, int Rank)
        {
            _db.Page_Products.InsertOnSubmit(new Kawa.Models.Data.Page_Product() { ProductID = ProductID, PageID = PageID, Rank = Rank });
            _db.SubmitChanges();
        }

        public void deletePageRelationShip(int ProductID, int PageID)
        {
            _db.Page_Products.DeleteAllOnSubmit(_db.Page_Products.Where(x => x.ProductID == ProductID && x.PageID == PageID));
            _db.SubmitChanges();
        }

        public void saveTagRelationShip(int ProductID, int TagID, int Rank)
        {
            _db.Tag_Products.InsertOnSubmit(new Kawa.Models.Data.Tag_Product() { ProductID = ProductID, TagID = TagID, Rank =Rank});
            _db.SubmitChanges();
        }
        public void deleteTagRelationShip(int ProductID, int TagID)
        {
            _db.Tag_Products.DeleteAllOnSubmit(_db.Tag_Products.Where(x=> x.ProductID == ProductID && x.TagID == TagID));
            _db.SubmitChanges();
        }

        public void savePrescriptionTagRelationShip(int ProductID, int TagID, int Rank)
        {
            _db.PrescriptionTag_Products.InsertOnSubmit(new Kawa.Models.Data.PrescriptionTag_Product() { ProductID = ProductID, TagID = TagID, Rank = Rank });
            _db.SubmitChanges();
        }
        public void deletePrescriptionTagRelationShip(int ProductID, int TagID)
        {
            _db.PrescriptionTag_Products.DeleteAllOnSubmit(_db.PrescriptionTag_Products.Where(x => x.ProductID == ProductID && x.TagID == TagID));
            _db.SubmitChanges();
        }

        public void saveProductList(int TagID, List<int> ProductIDs)
        {
            List<Kawa.Models.Data.Tag_Product> tags = ProductIDs.Select(x => new Kawa.Models.Data.Tag_Product() { 
                ProductID = x,
                TagID = TagID 
            }).ToList();
            _db.Tag_Products.InsertAllOnSubmit(tags);
            _db.SubmitChanges();
        }
        /*
        public bool canDeleteProduct(Product product)
        {
            return from 
        }
		*/
		public bool deleteProduct(Product product)
        {
            /*
            var delProdSymptom = from o in _db.Symptom_Products
                                 where o.ProductID == product.ID
                                 select o;
            _db.Symptom_Products.DeleteAllOnSubmit(delProdSymptom);
            _db.SubmitChanges();

            var delQaA = from o in _db.QandAs
                         where o.ProductID == product.ID
                         select o;
            _db.QandAs.DeleteAllOnSubmit(delQaA);
            _db.SubmitChanges();
            */

            var delProdTags = from o in _db.Tag_Products
                                 where o.ProductID == product.ID
                                 select o;
            _db.Tag_Products.DeleteAllOnSubmit(delProdTags);
            _db.SubmitChanges();

            var delProdPages = from o in _db.Product_Pages
                              where o.ProductID == product.ID
                              select o;
            _db.Product_Pages.DeleteAllOnSubmit(delProdPages);
            _db.SubmitChanges();

            var delRelProd = from o in _db.ProductRelations
                                 where o.ProductID == product.ID || o.RelatedProductID == product.ID
                                 select o;
            _db.ProductRelations.DeleteAllOnSubmit(delRelProd);
            _db.SubmitChanges();


            var baskets = from o in _db.Baskets
                           where o.Variant.ProductID == product.ID && !o.OrderID.HasValue
                           select o;
            _db.Baskets.DeleteAllOnSubmit(baskets);
            _db.SubmitChanges();

            var prices = from o in _db.Prices
                           where o.Variant.ProductID == product.ID
                           select o;
            _db.Prices.DeleteAllOnSubmit(prices);
            _db.SubmitChanges();

            var variants = from o in _db.Variants
                         where o.ProductID == product.ID
                         select o;
            _db.Variants.DeleteAllOnSubmit(variants);
            _db.SubmitChanges();


             var delProduct = from o in _db.Products
                            where o.ID == product.ID
                           select o;


            //delete the order
            _db.Products.DeleteAllOnSubmit(delProduct);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<Symptom> listAllSymptoms()
        {

            return from i in _db.Symptoms
                   select new Symptom
                   {
                       ID = i.ID,
                       name = i.name,

                   };

        }

        public void saveSymptom(Symptom symptom)
        {
            if (symptom != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Symptom add;

                if (symptom.ID > 0)
                {
                    add = _db.Symptoms.Where(x => x.ID == symptom.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Symptom();
                }

                //synch it
                add.name = symptom.name;


                //save it
                if (add.ID == 0)
                    _db.Symptoms.InsertOnSubmit(add);

                _db.SubmitChanges();

                symptom.ID = add.ID;

                var delProducts = from o in _db.Symptom_Products
                                  where o.SymptomID == symptom.ID
                              select o;
                _db.Symptom_Products.DeleteAllOnSubmit(delProducts);
                _db.SubmitChanges();

                if (symptom.products != null && symptom.products.Count() > 0)
                {
                    List<Kawa.Models.Data.Symptom_Product> products = symptom.products.Select(t => new Kawa.Models.Data.Symptom_Product() { SymptomID = symptom.ID, ProductID = t.ID }).ToList();
                    _db.Symptom_Products.InsertAllOnSubmit(products);
                    _db.SubmitChanges();
                }
            }
        }

        public bool deleteSymptom(Symptom symptom)
        {
            var delProdSymptom = from o in _db.Symptom_Products
                             where o.SymptomID == symptom.ID
                             select o;

            _db.Symptom_Products.DeleteAllOnSubmit(delProdSymptom);

            _db.SubmitChanges();

            var delSymptom = from o in _db.Symptoms
                             where o.ID == symptom.ID
                             select o;


            //delete the order
            _db.Symptoms.DeleteAllOnSubmit(delSymptom);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        private IQueryable<Variant> selectVariant(IQueryable<Data.Variant> list)
        {
            return list.Select(v => new Variant
            {
                ID = v.ID,
                ProductID = v.ProductID,
                name = v.name,
                sku = v.sku,
                price = v.price,
                priceRRP = (decimal)v.priceRRP,
                //priceMargin = v.priceMargin,
                weight = v.weight,
                minQuantity = v.minQuantity,
                maxQuantity = v.maxQuantity,
                noTax = v.noTax,
                stockNo = v.stockNo,
                shipDays = v.shipDays,
                createDate = v.createDate,
                modifiedDate = v.modifiedDate,
                isHidden = v.isHidden == 1,
                preOrderShipDate = v.preOrderShipDate,
                shipDaysMinimum = v.shipDaysMinimum,

                bulkPrices = v.Prices.Select(p => new Price()
                {
                    ID = p.ID,
                    VariantID = p.VariantID,
                    amount = p.amount,
                    quantity = p.quantity,
                    priceMargin = (((decimal)v.priceRRP - p.amount) / (decimal)v.priceRRP) * 100
                }).OrderBy(x => x.quantity).ToList()
            });
        }

        public IQueryable<Variant> listAllVariants()
        {

            return selectVariant(from i in _db.Variants select i);

        }

        public void saveVariant(Variant variant)
        {
            if (variant != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Variant add;

                if (variant.ID > 0)
                {
                    add = _db.Variants.Where(x => x.ID == variant.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Variant();
                }

                //synch it
                add.ProductID = variant.ProductID;
                add.name = variant.name;
                add.sku = variant.sku;
                add.price = decimal.Round(variant.price,2);
                add.priceRRP = decimal.Round(variant.priceRRP,2);
                //add.priceMargin = variant.priceMargin;
                add.weight = variant.weight;
                add.minQuantity = variant.minQuantity;
                add.maxQuantity = variant.maxQuantity;
                add.noTax = variant.noTax;
                add.stockNo = variant.stockNo;
                add.shipDays = variant.shipDays;
                add.createDate = variant.createDate;
                add.modifiedDate = variant.modifiedDate;
                add.isHidden = variant.isHidden?1:0;
                add.preOrderShipDate = variant.preOrderShipDate;
                add.shipDaysMinimum = variant.shipDaysMinimum;

                //save it
                if (add.ID == 0)
                    _db.Variants.InsertOnSubmit(add);

                _db.SubmitChanges();

                variant.ID = add.ID;
            }
        }

        public bool deleteVariant(Variant variant)
        {
            var delVariant = from o in _db.Variants
                             where o.ID == variant.ID
                             select o;


            //delete the order
            _db.Variants.DeleteAllOnSubmit(delVariant);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public void savePrice(Price price)
        {
            if (price != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Price add;

                if (price.ID > 0)
                {
                    add = _db.Prices.Where(x => x.ID == price.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Price();
                }

                //synch it
                add.VariantID = price.VariantID;
                add.amount = decimal.Round(price.amount,2);
                add.quantity = price.quantity;


                //save it
                if (add.ID == 0)
                    _db.Prices.InsertOnSubmit(add);

                _db.SubmitChanges();

                price.ID = add.ID;
            }
        }

        public bool deletePrice(Price price)
        {
            var delPrice = from o in _db.Prices
                           where o.ID == price.ID
                           select o;


            //delete the order
            _db.Prices.DeleteAllOnSubmit(delPrice);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        
        #region q and a

        public IQueryable<QandA> listAllQandA()
        {

            return from i in _db.QandAs
                   select new QandA
                   {
                       ID = i.ID,
                       question = i.question,
                       answer = i.answer,
                       ProductID = i.ProductID,
                       OrderNo = i.OrderNo,
                       createDate = i.createDate,

                   };

        }

        public void saveQandA(QandA qandA)
        {
            if (qandA != null)
            {

                //see if it's in the db
                Kawa.Models.Data.QandA add;

                if (qandA.ID > 0)
                {
                    add = _db.QandAs.Where(x => x.ID == qandA.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.QandA();
                }

                //synch it
                add.question = qandA.question;
                add.answer = qandA.answer;
                add.ProductID = qandA.ProductID;
                add.OrderNo = qandA.OrderNo;
                add.createDate = qandA.createDate;


                //save it
                if (add.ID == 0)
                    _db.QandAs.InsertOnSubmit(add);

                _db.SubmitChanges();

                qandA.ID = add.ID;
            }
        }

        public bool deleteQandA(QandA qandA)
        {
            var delQandA = from o in _db.QandAs
                           where o.ID == qandA.ID
                           select o;


            //delete the order
            _db.QandAs.DeleteAllOnSubmit(delQandA);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<Ingredient> listAllIngredients(bool loadSr)
        {

            return from i in _db.Ingredients
                   select new Ingredient
                   {
                       ID = i.ID,
                       ProductID = i.ProductID,
                       name = i.name,
                       description = i.description,
                       createDate = i.createDate,
                       orderNo =i.orderNo,
                       siteResources = loadSr ? i.SiteResources.Select(s => s.createAggregate()).ToArray() : null,
                   };

        }

        public void saveIngredient(Ingredient ingredient)
        {
            if (ingredient != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Ingredient add;

                if (ingredient.ID > 0)
                {
                    add = _db.Ingredients.Where(x => x.ID == ingredient.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Ingredient();
                }

                //synch it
                add.ProductID = ingredient.ProductID;
                add.name = ingredient.name;
                add.description = ingredient.description;
                add.createDate = ingredient.createDate;
                add.orderNo = ingredient.orderNo;

                //save it
                if (add.ID == 0)
                    _db.Ingredients.InsertOnSubmit(add);

                _db.SubmitChanges();

                ingredient.ID = add.ID;
            }
        }

        public bool deleteIngredient(Ingredient ingredient)
        {
            var delIngredient = from o in _db.Ingredients
                                where o.ID == ingredient.ID
                                select o;


            //delete the order
            _db.Ingredients.DeleteAllOnSubmit(delIngredient);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        #endregion

        #region StockNotices

        public IQueryable<StockNotice> listAllStockNotices()
        {

            return from i in _db.StockNotices
                   select new StockNotice
                   {
                       ID = i.ID,
                       email = i.email,
                       ProductID = i.ProductID,
                       VariantID = i.VariantID,
                       Status = (StockNotice.StatusOption)i.Status,
                       notes = i.notes,
                       createDate = i.createDate,
                       sentDate = i.sentDate,

                   };

        }

        public void saveStockNotice(StockNotice stockNotice)
        {
            if (stockNotice != null)
            {

                //see if it's in the db
                Kawa.Models.Data.StockNotice add;

                if (stockNotice.ID > 0)
                {
                    add = _db.StockNotices.Where(x => x.ID == stockNotice.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.StockNotice();
                }

                //synch it
                add.email = stockNotice.email;
                add.ProductID = stockNotice.ProductID;
                add.VariantID = stockNotice.VariantID;
                add.Status = (int)stockNotice.Status;
                add.notes = stockNotice.notes;
                add.createDate = stockNotice.createDate;
                add.sentDate = stockNotice.sentDate;


                //save it
                if (add.ID == 0)
                    _db.StockNotices.InsertOnSubmit(add);

                _db.SubmitChanges();

                stockNotice.ID = add.ID;
            }
        }

        public bool deleteStockNotice(StockNotice stockNotice)
        {
            var delStockNotice = from o in _db.StockNotices
                                 where o.ID == stockNotice.ID
                                 select o;


            //delete the order
            _db.StockNotices.DeleteAllOnSubmit(delStockNotice);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        #endregion

    }

    public static class ProductRepositoryExtensions
    {
        public static Promotion createAggregate(this Data.Promotion v)
        {
           
                return new Promotion()
                {
                    ID = v.ID,
                    ProductID = v.ProductID,
                    name = v.name,
                    VariantID = v.VariantID,
                    Type = (Promotion.TypeOption)v.Type,
                    percentOff = v.percentOff,
                    amountOff = v.amountOff,
                    Status = (Promotion.StatusOption)v.Status,
                    createDate = v.createDate,
                    usedDate = v.usedDate,
                    isHidden = v.isHidden == 1,
                    SessionKey = v.SessionKey,
                    TagID = v.TagID,
                    notes = v.notes,
                    SystemType = (Promotion.SystemTypeOption)v.SystemType,
                    minimumSpend = v.minimumSpend,
                    maximumWeight = v.maximumWeight,

                    priority = v.priority,
                    ShippingMethodID = v.ShippingMethodID
                };
          
        }
        public static Product createAggregate(this Data.Product i)
        {
            return new Product()
            {
                ID = i.ID,
                name = i.name,
                nameUrl = i.nameUrl,
                titleName = i.titleName,
                menuName = i.menuName,
                intro = i.intro,
                blurb = i.blurb,
                activeIngredients = i.activeIngredients,
                formulation = i.formulation,
                dosage = i.dosage,
                warnings = i.warnings,
                specifications = i.specifications,
                freeFrom = i.freeFrom,
                interactions = i.interactions,
                availability = i.availability,
                sku = i.sku,
                price = i.price,
                specialPrice = i.specialPrice,
                aliases = i.aliases,
                aliasesUrl = i.aliasesUrl,
                createdDate = i.createDate,
                modifiedDate = i.modifiedDate,
                isHidden = i.isHidden == 1,
                googleCat = i.googleCat,
                gtin = i.gtin,
                nationalOnly = i.nationalOnly,
                specialType = (Product.SpecialTypeOption?)i.specialType,
                orderOverride = i.orderOverride,
                bingCat = i.bingCat,
                directions = i.directions,
                noAdded = i.noAdded,
                hideBlurb = i.hideBlurb==1,
                packNotes = i.packNotes,
                austL = i.austL,
                poName = i.poName,
                HasVariant = i.Variants.Any(),
                HasVariantOutOfStock = i.Variants.Any() && i.Variants.First().isHidden == 1,
                HasVariantPreOrder = i.Variants.Any() && i.Variants.First().preOrderShipDate.HasValue,
                variants = i.Variants.Select(v => v.createAggregate()).ToList(),

                markertags = i.Tag_Products.Where(tp => tp.Tag.Type == (int)Tag.TypeOption.ADHOC_PRODUCT_TAGS).Select(tp => tp.Tag.createAggregate()).ToList(),
                sThumb = i.sThumb,
                sHero = i.sHero
            };
        }

        public static Variant createAggregate(this Data.Variant v)
        {



            return new Variant()
            {
                ID = v.ID,
                ProductID = v.ProductID,
                name = v.name,
                sku = v.sku,
                price = v.price,
                priceRRP = (decimal)v.priceRRP,
                weight = v.weight,
                minQuantity = v.minQuantity,
                maxQuantity = v.maxQuantity,
                noTax = v.noTax,
                stockNo = v.stockNo,
                shipDays = v.shipDays,
                createDate = v.createDate,
                modifiedDate = v.modifiedDate,
                isHidden = v.isHidden == 1,
                preOrderShipDate = v.preOrderShipDate,
                shipDaysMinimum = v.shipDaysMinimum,
                bulkPrices = v.Prices.Select(p => new Price()
                {
                    ID = p.ID,
                    VariantID = p.VariantID,
                    quantity = p.quantity,
                    amount = p.amount,
                    priceMargin = (((decimal)v.priceRRP - p.amount) / (decimal)v.priceRRP) * 100
                }).OrderBy(x => x.quantity).ToList()
            };
        }
    }
}