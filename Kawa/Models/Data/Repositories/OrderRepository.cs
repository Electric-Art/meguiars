﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Kawa.Models.Data;


namespace Kawa.Models
{
    public class OrderRepository
    {
        dbDataContext _db;

        public OrderRepository()
		{
			_db = new dbDataContext();
		}

        public IQueryable<Basket> listAllBasket()
		{

            return from i in _db.Baskets
                   select new Basket
                   {
                       ID = i.ID,
                       VariantID = i.VariantID,
                       Price = i.Price,
                       Weight = i.Weight,
                       SessionKey = i.SessionKey,
                       OrderID = i.OrderID,
                       Quantity = i.Quantity,
                       ShipPrice = i.ShipPrice,
                       CountryID = i.CountryID,
                       Notes = i.Notes,
                       AdminFixed = i.AdminFixed,
                       CreateDate = i.CreateDate,
                       Product = i.Variant.Product.createAggregate(),
                       Variant = i.Variant.createAggregate()
                   };
			
		}

		public void saveBasket(Basket basket)
		{
            if (basket.IsCalculationBasket)
                throw new Exception("Should not be saving a caclulation basket");

			//see if it's in the db
			Kawa.Models.Data.Basket add;

			if (basket.ID > 0)
			{
				add = _db.Baskets.Where(x => x.ID == basket.ID).SingleOrDefault();
			}
			else
			{
				add = new Kawa.Models.Data.Basket();
			}

			//synch it
			add.VariantID = basket.VariantID;
            add.Price = basket.Price;
            add.Weight = basket.Weight;
            add.SessionKey = basket.SessionKey;
			add.OrderID = basket.OrderID;
			add.Quantity = basket.Quantity;
			add.ShipPrice = basket.ShipPrice;
			add.CountryID = basket.CountryID;
			add.Notes = basket.Notes;
            add.AdminFixed = basket.AdminFixed;
			add.CreateDate = basket.CreateDate;
	
		        
			//save it
			if (add.ID == 0)
				_db.Baskets.InsertOnSubmit(add);

			_db.SubmitChanges();

			basket.ID = add.ID;
		}	
		
		public bool deleteBasket(Basket basket)
        {
             var delBasket = from o in _db.Baskets
                            where o.ID == basket.ID
                           select o;


            //delete the order
            _db.Baskets.DeleteAllOnSubmit(delBasket);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<Order> listAllOrders(bool onlyWithDetails = false)
        {

            return from i in _db.Orders
                   where (!onlyWithDetails || i.AccountDetails.Any())
                   select new Order
                   {
                       ID = i.ID,
                       OrderDate = i.OrderDate,
                       OrderTotal = i.OrderTotal,
                       SystemStatus = (Order.SystemStatusOption)i.SystemStatus,
                       StatusID = i.StatusID,
                       ProcessDate = i.ProcessDate,
                       CompleteDate = i.CompleteDate,
                       BasketTotal = i.BasketTotal,
                       FreightTotal = i.FreightTotal,
                       TaxTotal = i.TaxTotal,
                       Invoice = i.Invoice,
                       InvoiceTotal = i.InvoiceTotal,
                       idx = i.idx,
                       AcknowledgeDate = i.AcknowledgeDate,
                       FeedbackRequestDate = i.FeedbackRequestDate,
                       FeedbackResponseDate = i.FeedbackResponseDate,
                       AdminNotes = i.AdminNotes,
                       Code = i.Code,
                       SessionKey = i.SessionKey,
                       AgentData = i.AgentData,
                       PromotionID = i.PromotionID,
                       AccountID = i.AccountID,
                       ShippingMethodID = i.ShippingMethodID,
                       LocationID = i.LocationID,
                       CountryID = i.CountryID,
                       StateID = i.StateID,
                       Postcode = i.Postcode,
                       SystemNotes = i.SystemNotes,
                       PaymentType = (Kawa.Models.Services.PaymentServiceStore.PaymentTypeOption)i.PaymentType,
                       CustomerNotes = i.CustomerNotes,
                       VoucherPromotionID = i.VoucherPromotionID,
                       Weight = i.Weight,
                       Printed = i.Printed,
                       TotalBeforeSurcharge = i.TotalBeforeSurcharge,
                       AmexSurcharge = i.AmexSurcharge,
                       AdminAdHocFreightTotal = i.AdminAdHocFreightTotal,
                       IsHidden = i.IsHidden,
                       CubicWeight = i.CubicWeight,
                       FreightWouldHaveBeenTotal = i.FreightWouldHaveBeenTotal,
                       FreightOriginalAmount = i.FreightOriginalAmount,
                       IsAdminOrder = i.IsAdminOrder,

                       Details = i.AccountDetails.Select(a => new AccountDetail()
                       {
                           ID = a.ID,
                           Email = a.Email
                       }).ToList(),
                       taxes = i.OrderTaxes.Select(t => new OrderTax()
                       {
                           ID = t.ID,
                           OrderID = t.OrderID,
                           TaxType = (OrderTax.TaxTypeOption)t.TaxType,
                           Amount = t.Amount
                       }).ToList(),
                       shipments = i.Shipments.Select(t => new Shipment()
                       {
                           ID = t.ID,
                           OrderID = t.OrderID,
                           ShippingMethodID = t.ShippingMethodID,
                           CarrierID = t.CarrierID,
                           TrackingNo = t.TrackingNo,
                           Comment = t.Comment,
                           createDate = t.createDate,
                           carrier = new Carrier()
                           {
                               Name = t.Carrier.Name,
                               Url = t.Carrier.Url
                           },
                           shippingMethod = new ShippingMethod()
                           {
                               Name = t.ShippingMethod.Name
                           }
                       }).ToList()
                   };

        }

        public IQueryable<Order> quicklistAllOrdersWithTrans(Status.StatusSetOption? statusSet = null)
        {
            return from i in _db.Orders
                   where i.AccountDetails.Count > 0 && (!statusSet.HasValue || (statusSet.HasValue && i.Status.StatusSet == (int?)statusSet))
                   select new Order
                   {
                       ID = i.ID,
                       OrderDate = i.OrderDate,
                       OrderTotal = i.OrderTotal,
                       SystemStatus = (Order.SystemStatusOption)i.SystemStatus,
                       StatusID = i.StatusID,
                       ProcessDate = i.ProcessDate,
                       CompleteDate = i.CompleteDate,
                       BasketTotal = i.BasketTotal,
                       FreightTotal = i.FreightTotal,
                       TaxTotal = i.TaxTotal,
                       Invoice = i.Invoice,
                       InvoiceTotal = i.InvoiceTotal,
                       idx = i.idx,
                       AcknowledgeDate = i.AcknowledgeDate,
                       FeedbackRequestDate = i.FeedbackRequestDate,
                       FeedbackResponseDate = i.FeedbackResponseDate,
                       AdminNotes = i.AdminNotes,
                       Code = i.Code,
                       SessionKey = i.SessionKey,
                       AgentData = i.AgentData,
                       PromotionID = i.PromotionID,
                       AccountID = i.AccountID,
                       ShippingMethodID = i.ShippingMethodID,
                       LocationID = i.LocationID,
                       CountryID = i.CountryID,
                       StateID = i.StateID,
                       Postcode = i.Postcode,
                       SystemNotes = i.SystemNotes,
                       PaymentType = (Kawa.Models.Services.PaymentServiceStore.PaymentTypeOption)i.PaymentType,
                       CustomerNotes = i.CustomerNotes,
                       VoucherPromotionID = i.VoucherPromotionID,
                       Weight = i.Weight,
                       Printed = i.Printed,
                       TotalBeforeSurcharge = i.TotalBeforeSurcharge,
                       AmexSurcharge = i.AmexSurcharge,
                       AdminAdHocFreightTotal = i.AdminAdHocFreightTotal,
                       IsHidden = i.IsHidden,
                       CubicWeight = i.CubicWeight,
                       FreightWouldHaveBeenTotal = i.FreightWouldHaveBeenTotal,
                       FreightOriginalAmount = i.FreightOriginalAmount,
                       IsAdminOrder = i.IsAdminOrder,

                       ///LastPayment = i.OrderPayments.OrderByDescending(p => p.ID).Select(p => p.createAggregate()).FirstOrDefault(),
                       ///VoucherCode = i.Promotion != null ? i.Promotion.code : (i.Promotions.Any() ? i.Promotions.First().code : null),
                       /*taxes = i.OrderTaxes.Select(t => new OrderTax()
                       {
                           ID = t.ID,
                           OrderID = t.OrderID,
                           TaxType = (OrderTax.TaxTypeOption)t.TaxType,
                           Amount = t.Amount
                       }).ToList(),
                       shipments = i.Shipments.Select(t => new Shipment()
                       {
                           ID = t.ID,
                           OrderID = t.OrderID,
                           ShippingMethodID = t.ShippingMethodID,
                           CarrierID = t.CarrierID,
                           TrackingNo = t.TrackingNo,
                           Comment = t.Comment,
                           createDate = t.createDate,
                           carrier = new Carrier()
                           {
                               Name = t.Carrier.Name,
                               Url = t.Carrier.Url
                           },
                           shippingMethod = new ShippingMethod()
                           {
                               Name = t.ShippingMethod.Name
                           }
                       }).ToList(),*/
                       Status = i.Status != null ? i.Status.createAggregate() : null,
                       StatusSet = i.Status != null ? (Status.StatusSetOption?)i.Status.StatusSet : null,
                       ///SearchOrderNo = i.SystemStatus > (int)Order.SystemStatusOption.IN_PROGRESS ? "MGR-" + i.ID : "PRG-" + i.ID,
                       ///SearchCustomer = i.AccountDetails.Any(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS) ? (i.AccountDetails.Single(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).FirstName + " " + i.AccountDetails.Single(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).Surname) : null,
                       ///SearchEmail = i.AccountDetails.Any(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS) ? (i.AccountDetails.Single(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).Email) : null,
                       Data = i
                   };

        }



        public IQueryable<Order> listAllOrdersWithTrans(Status.StatusSetOption? statusSet = null)
        {
            return from i in _db.Orders
                   where i.AccountDetails.Count > 0 && (!statusSet.HasValue || (statusSet.HasValue && i.Status.StatusSet == (int?)statusSet))
                   select new Order
                   {
                       ID = i.ID,
                       OrderDate = i.OrderDate,
                       OrderTotal = i.OrderTotal,
                       SystemStatus = (Order.SystemStatusOption)i.SystemStatus,
                       StatusID = i.StatusID,
                       ProcessDate = i.ProcessDate,
                       CompleteDate = i.CompleteDate,
                       BasketTotal = i.BasketTotal,
                       FreightTotal = i.FreightTotal,
                       TaxTotal = i.TaxTotal,
                       Invoice = i.Invoice,
                       InvoiceTotal = i.InvoiceTotal,
                       idx = i.idx,
                       AcknowledgeDate = i.AcknowledgeDate,
                       FeedbackRequestDate = i.FeedbackRequestDate,
                       FeedbackResponseDate = i.FeedbackResponseDate,
                       AdminNotes = i.AdminNotes,
                       Code = i.Code,
                       SessionKey = i.SessionKey,
                       AgentData = i.AgentData,
                       PromotionID = i.PromotionID,
                       AccountID = i.AccountID,
                       ShippingMethodID = i.ShippingMethodID,
                       LocationID = i.LocationID,
                       CountryID = i.CountryID,
                       StateID = i.StateID,
                       Postcode = i.Postcode,
                       SystemNotes = i.SystemNotes,
                       PaymentType = (Kawa.Models.Services.PaymentServiceStore.PaymentTypeOption)i.PaymentType,
                       CustomerNotes = i.CustomerNotes,
                       VoucherPromotionID = i.VoucherPromotionID,
                       Weight = i.Weight,
                       Printed = i.Printed,
                       TotalBeforeSurcharge = i.TotalBeforeSurcharge,
                       AmexSurcharge = i.AmexSurcharge,
                       AdminAdHocFreightTotal = i.AdminAdHocFreightTotal,
                       IsHidden = i.IsHidden,
                       CubicWeight = i.CubicWeight,
                       FreightWouldHaveBeenTotal = i.FreightWouldHaveBeenTotal,
                       FreightOriginalAmount = i.FreightOriginalAmount,
                       IsAdminOrder = i.IsAdminOrder,

                       LastPayment = i.OrderPayments.OrderByDescending(p => p.ID).Select(p => p.createAggregate()).FirstOrDefault(),
                       VoucherCode = i.Promotion != null ? i.Promotion.code : (i.Promotions.Any() ? i.Promotions.First().code : null),
                       taxes = i.OrderTaxes.Select(t => new OrderTax()
                       {
                           ID = t.ID,
                           OrderID = t.OrderID,
                           TaxType = (OrderTax.TaxTypeOption)t.TaxType,
                           Amount = t.Amount
                       }).ToList(),
                       shipments = i.Shipments.Select(t => new Shipment()
                       {
                           ID = t.ID,
                           OrderID = t.OrderID,
                           ShippingMethodID = t.ShippingMethodID,
                           CarrierID = t.CarrierID,
                           TrackingNo = t.TrackingNo,
                           Comment = t.Comment,
                           createDate = t.createDate,
                           carrier = new Carrier()
                           {
                               Name = t.Carrier.Name,
                               Url = t.Carrier.Url
                           },
                           shippingMethod = new ShippingMethod()
                           {
                               Name = t.ShippingMethod.Name
                           }
                       }).ToList(),
                       Status = i.Status != null ? i.Status.createAggregate() : null,
                       StatusSet = i.Status != null ? (Status.StatusSetOption?)i.Status.StatusSet : null,
                       SearchOrderNo = i.SystemStatus > (int)Order.SystemStatusOption.IN_PROGRESS ? "MGR-" + i.ID : "PRG-" + i.ID,
                       SearchCustomer = i.AccountDetails.Any(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS) ? (i.AccountDetails.Single(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).FirstName + " " + i.AccountDetails.Single(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).Surname) : null,
                       SearchEmail = i.AccountDetails.Any(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS) ? (i.AccountDetails.Single(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).Email) : null,
                       Data = i
                   };

        }

        public void saveOrder(Order order, bool saveTax = false)
        {
            if (order.IsCalculationOrder)
                throw new Exception("Should not be saving a caclulation order");

            //see if it's in the db
            Kawa.Models.Data.Order add;

            if (order.ID > 0)
            {
                add = _db.Orders.Where(x => x.ID == order.ID).SingleOrDefault();
            }
            else
            {
                add = new Kawa.Models.Data.Order();
            }

            //synch it
            add.OrderDate = order.OrderDate;
            add.OrderTotal = order.OrderTotal;
            add.SystemStatus = (int)order.SystemStatus;
            add.StatusID = order.StatusID;
            add.ProcessDate = order.ProcessDate;
            add.CompleteDate = order.CompleteDate;
            add.BasketTotal = order.BasketTotal;
            add.FreightTotal = order.FreightTotal;
            add.TaxTotal = order.TaxTotal;
            add.Invoice = order.Invoice;
            add.InvoiceTotal = order.InvoiceTotal;
            add.idx = order.idx;
            add.AcknowledgeDate = order.AcknowledgeDate;
            add.FeedbackRequestDate = order.FeedbackRequestDate;
            add.FeedbackResponseDate = order.FeedbackResponseDate;
            add.AdminNotes = order.AdminNotes;
            add.Code = order.Code;
            add.SessionKey = order.SessionKey;
            add.AgentData = order.AgentData;
            add.PromotionID = order.PromotionID;
            add.AccountID = order.AccountID;
            add.ShippingMethodID = order.ShippingMethodID;
            add.LocationID = order.LocationID;
            add.CountryID = order.CountryID;
            add.StateID = order.StateID;
            add.Postcode = order.Postcode;
            add.SystemNotes = order.SystemNotes;
            add.PaymentType = (int?)order.PaymentType;
            add.CustomerNotes = order.CustomerNotes;
            add.VoucherPromotionID = order.VoucherPromotionID;
            add.Weight = order.Weight;
            add.Printed = order.Printed;
            add.TotalBeforeSurcharge = order.TotalBeforeSurcharge;
            add.AmexSurcharge = order.AmexSurcharge;
            add.AdminAdHocFreightTotal = order.AdminAdHocFreightTotal;
            add.IsHidden = order.IsHidden;
            add.CubicWeight = order.CubicWeight;
            add.FreightWouldHaveBeenTotal = order.FreightWouldHaveBeenTotal;
            add.FreightOriginalAmount = order.FreightOriginalAmount;
            add.IsAdminOrder = order.IsAdminOrder;

            //save it
            if (add.ID == 0)
                _db.Orders.InsertOnSubmit(add);

            _db.SubmitChanges();

            order.ID = add.ID;

            if (saveTax)
            {
                var del = from o in _db.OrderTaxes
                              where o.OrderID == order.ID
                              select o;
                _db.OrderTaxes.DeleteAllOnSubmit(del);
                _db.SubmitChanges();

                if (order.taxes != null && order.taxes.Count() > 0)
                {
                    List<Kawa.Models.Data.OrderTax> tags = order.taxes.Select(t => new Kawa.Models.Data.OrderTax()
                    {
                        OrderID = order.ID,
                        TaxType = (int)t.TaxType,
                        Amount = t.Amount
                    }).ToList();
                    _db.OrderTaxes.InsertAllOnSubmit(tags);
                    _db.SubmitChanges();
                }
            }
           
        }

        public bool deleteOrder(Order order)
        {
            var delOrder = from o in _db.Orders
                           where o.ID == order.ID
                           select o;


            //delete the order
            _db.Orders.DeleteAllOnSubmit(delOrder);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<AccountDetail> listAllAccountDetails()
        {
            return from i in _db.AccountDetails
                   select new AccountDetail
                   {
                       ID = i.ID,
                       OrderID = i.OrderID,
                       FirstName = i.FirstName,
                       Surname = i.Surname,
                       Phone = i.Phone,
                       Email = i.Email,
                       Address1 = i.Address1,
                       Address2 = i.Address2,
                       Address3 = i.Address3,
                       Address4 = i.Address4,
                       Postcode = i.Postcode,
                       CountryID = i.CountryID,
                       Type = (AccountDetail.DetailTypeOption)i.Type,
                       isHidden = i.isHidden,
                       GUID = i.GUID,
                       Company = i.Company,
                       Country = i.Country,
                       isMailer = i.isMailer,
                       Mobile = i.Mobile,
                       DeliveryInstructions = i.DeliveryInstructions,
                       LeaveAuth = i.LeaveAuth.HasValue ? (i.LeaveAuth == 1) : (bool?)null
                   };

        }

        public IQueryable<AccountDetail> listAllAccountDetailForAccountAdmin()
        {
            return from i in _db.AccountDetails
                   where i.Order.SystemStatus > (int)Order.SystemStatusOption.IN_PROGRESS
                   && i.Type.HasValue && (int)i.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS
                   select new AccountDetail
                   {
                       ID = i.ID,
                       OrderID = i.OrderID,
                       FirstName = i.FirstName,
                       Surname = i.Surname,
                       Email = i.Email,
                   };

        }

        public void saveAccountDetail(AccountDetail accountDetail)
        {
            if (accountDetail != null)
            {

                //see if it's in the db
                Kawa.Models.Data.AccountDetail add;

                if (accountDetail.ID > 0)
                {
                    add = _db.AccountDetails.Where(x => x.ID == accountDetail.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.AccountDetail();
                }

                //Clean data
                if (accountDetail.Mobile != null)
                    accountDetail.Mobile = accountDetail.Mobile.Replace("O", "0").Replace("o", "0");

                //synch it
                add.OrderID = accountDetail.OrderID;
                add.FirstName = accountDetail.FirstName;
                add.Surname = accountDetail.Surname;
                add.Phone = accountDetail.Phone;
                add.Email = accountDetail.Email;
                add.Address1 = accountDetail.Address1;
                add.Address2 = accountDetail.Address2;
                add.Address3 = accountDetail.Address3;
                add.Address4 = accountDetail.Address4;
                add.Postcode = accountDetail.Postcode;
                add.CountryID = accountDetail.CountryID;
                add.Type = (int)accountDetail.Type;
                add.isHidden = accountDetail.isHidden;
                add.GUID = accountDetail.GUID;
                add.Company = accountDetail.Company;
                add.Country = accountDetail.Country;
                add.isMailer = accountDetail.isMailer;
                add.Mobile = accountDetail.Mobile;
                add.DeliveryInstructions = accountDetail.DeliveryInstructions;
                add.LeaveAuth = accountDetail.LeaveAuth.HasValue ? ((bool)accountDetail.LeaveAuth ? 1 : 0) : (int?)null;

                //save it
                if (add.ID == 0)
                    _db.AccountDetails.InsertOnSubmit(add);

                _db.SubmitChanges();

                accountDetail.ID = add.ID;
            }
        }

        public bool deleteAccountDetail(AccountDetail accountDetail)
        {
            var delAccountDetail = from o in _db.AccountDetails
                                   where o.ID == accountDetail.ID
                                   select o;


            //delete the order
            _db.AccountDetails.DeleteAllOnSubmit(delAccountDetail);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<OrderPayment> listAllOrderPayments()
        {

            return from i in _db.OrderPayments
                   select new OrderPayment
                   {
                       ID = i.ID,
                       OrderID = i.OrderID,
                       AccountDetailID = i.AccountDetailID,
                       PaymentStatus = i.PaymentStatus,
                       PaymentReference = i.PaymentReference,
                       PaymentCode = i.PaymentCode,
                       PaymentDate = i.PaymentDate,
                       FullResponse = i.FullResponse,
                       ResponseText = i.ResponseText,
                       Type = (OrderPayment.TypeOption)i.Type,
                       Service = (Services.PaymentServiceStore.PaymentTypeOption)i.Service
                   };

        }

        public void saveOrderPayment(OrderPayment orderPayment)
        {
            if (orderPayment != null)
            {

                //see if it's in the db
                Kawa.Models.Data.OrderPayment add;

                if (orderPayment.ID > 0)
                {
                    add = _db.OrderPayments.Where(x => x.ID == orderPayment.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.OrderPayment();
                }

                //synch it
                add.OrderID = orderPayment.OrderID;
                add.AccountDetailID = orderPayment.AccountDetailID;
                add.PaymentStatus = orderPayment.PaymentStatus;
                add.PaymentReference = orderPayment.PaymentReference;
                add.PaymentCode = orderPayment.PaymentCode;
                add.PaymentDate = orderPayment.PaymentDate;
                add.FullResponse = orderPayment.FullResponse;
                add.ResponseText = orderPayment.ResponseText;
                add.Type = (int)orderPayment.Type;
                add.Service = (int)orderPayment.Service;

                //save it
                if (add.ID == 0)
                    _db.OrderPayments.InsertOnSubmit(add);

                _db.SubmitChanges();

                orderPayment.ID = add.ID;
            }
        }

        #region Status

        public IQueryable<Status> listAllStatus()
        {

            return from i in _db.Status
                   select new Status
                   {
                       ID = i.ID,
                       Name = i.Name,
                       SetSystemStatus = (Order.SystemStatusOption?)i.SetSystemStatus,
                       EmailSubject = i.EmailSubject,
                       EmailHeader = i.EmailHeader,
                       Color = i.Color,
                       isNotifyCustomer = i.isNotifyCustomer,
                       isNotifyStaff = i.isNotifyStaff,
                       InventoryEffect = i.InventoryEffect,
                       isPayOrderAgain = i.isPayOrderAgain,
                       OrderNo = i.OrderNo,
                       CreateDate = i.CreateDate,
                       Notes = i.Notes,
                       StatusSet = (Status.StatusSetOption?)i.StatusSet,
                       RefactorToID = i.RefactorToID
                   };

        }

        public void saveStatus(Status status)
        {
            if (status != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Status add;

                if (status.ID > 0)
                {
                    add = _db.Status.Where(x => x.ID == status.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Status();
                }

                //synch it
                add.Name = status.Name;
                add.SetSystemStatus = (int?)status.SetSystemStatus;
                add.EmailSubject = status.EmailSubject;
                add.EmailHeader = status.EmailHeader;
                add.Color = status.Color;
                add.isNotifyCustomer = status.isNotifyCustomer;
                add.isNotifyStaff = status.isNotifyStaff;
                add.InventoryEffect = status.InventoryEffect;
                add.isPayOrderAgain = status.isPayOrderAgain;
                add.OrderNo = status.OrderNo;
                add.CreateDate = status.CreateDate;
                add.Notes = status.Notes;
                add.StatusSet = (int?)status.StatusSet;
                add.RefactorToID = status.RefactorToID;

                //save it
                if (add.ID == 0)
                    _db.Status.InsertOnSubmit(add);

                _db.SubmitChanges();

                status.ID = add.ID;
            }
        }

        public bool deleteStatus(Status status)
        {
            var delStatus = from o in _db.Status
                            where o.ID == status.ID
                            select o;


            //delete the order
            _db.Status.DeleteAllOnSubmit(delStatus);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        #endregion

        #region Promotions

        public IQueryable<Promotion> listAllPromotions()
        {

            return from i in _db.Promotions
                   select new Promotion
                   {
                       ID = i.ID,
                       name = i.name,
                       code = i.code,
                       ProductID = i.ProductID,
                       VariantID = i.VariantID,
                       Type = (Promotion.TypeOption)i.Type,
                       percentOff = i.percentOff,
                       amountOff = i.amountOff,
                       Status = (Promotion.StatusOption)i.Status,
                       createDate = i.createDate,
                       usedDate = i.usedDate,
                       OrderID = i.OrderID,
                       isHidden = i.isHidden == 1,
                       SessionKey = i.SessionKey,
                       expiryDate = i.expiryDate,
                       TagID = i.TagID,
                       notes = i.notes,
                       SystemType = (Promotion.SystemTypeOption)i.SystemType,
                       minimumSpend = i.minimumSpend,
                       maximumWeight = i.maximumWeight,
                       //Depreciated
                       //LocationID = i.LocationID,
                       freeShipping = i.freeShipping ?? false,
                       priority = i.priority,
                       ShippingMethodID = i.ShippingMethodID,
                       product = i.Product == null ? null : new Product() { ID = i.Product.ID, name = i.Product.name },
                       tag = i.Tag == null ? null : new Tag() { ID = i.Tag.ID, name = i.Tag.name },
                       hasLocation = i.Promotion_Locations.Any(),
                       locations = i.Promotion_Locations.Select(x =>  x.Location.createAggregate()).ToList(),
                       SKU = i.SKU
                   };

        }

        public void savePromotion(Promotion promotion, bool saveLocations)
        {
            if (promotion != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Promotion add;

                if (promotion.ID > 0)
                {
                    add = _db.Promotions.Where(x => x.ID == promotion.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Promotion();
                }

                //synch it
                add.name = promotion.name;
                add.code = promotion.code;
                add.ProductID = promotion.ProductID;
                add.VariantID = promotion.VariantID;
                add.Type = (int)promotion.Type;
                add.percentOff = promotion.percentOff;
                add.amountOff = promotion.amountOff;
                add.Status = (int)promotion.Status;
                add.createDate = promotion.createDate;
                add.usedDate = promotion.usedDate;
                add.OrderID = promotion.OrderID;
                add.isHidden = promotion.isHidden ? 1 : 0;
                add.SessionKey = promotion.SessionKey;
                add.expiryDate = promotion.expiryDate;
                add.TagID = promotion.TagID;
                add.notes = promotion.notes;
                add.SystemType = (int?)promotion.SystemType;
                add.minimumSpend = promotion.minimumSpend;
                add.maximumWeight = promotion.maximumWeight;
                add.SKU = promotion.SKU;
                //DEPRECIATED
                //add.LocationID = promotion.LocationID;
                add.freeShipping = promotion.freeShipping ? true : (bool?)null;
                add.priority = promotion.priority;
                add.ShippingMethodID = promotion.ShippingMethodID;

                //save it
                if (add.ID == 0)
                    _db.Promotions.InsertOnSubmit(add);

                _db.SubmitChanges();

                promotion.ID = add.ID;

                if (saveLocations)
                {
                    var delTags = from o in _db.Promotion_Locations
                                  where o.PromotionID == promotion.ID
                                  select o;
                    _db.Promotion_Locations.DeleteAllOnSubmit(delTags);
                    _db.SubmitChanges();

                    if (promotion.locations != null && promotion.locations.Count() > 0)
                    {
                        List<Kawa.Models.Data.Promotion_Location> locs = promotion.locations.Select(t => new Kawa.Models.Data.Promotion_Location() { PromotionID = promotion.ID, LocationID = t.ID }).ToList();
                        _db.Promotion_Locations.InsertAllOnSubmit(locs);
                        _db.SubmitChanges();
                    }
                }
            }
        }

        public bool deletePromotion(Promotion promotion)
        {
            var delPromotion = from o in _db.Promotions
                               where o.ID == promotion.ID
                               select o;


            //delete the order
            _db.Promotions.DeleteAllOnSubmit(delPromotion);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }


        #endregion

        #region Shipments

        public IQueryable<Shipment> listAllShipments()
        {

            return from i in _db.Shipments
                   select new Shipment
                   {
                       ID = i.ID,
                       OrderID = i.OrderID,
                       ShippingMethodID = i.ShippingMethodID,
                       CarrierID = i.CarrierID,
                       TrackingNo = i.TrackingNo,
                       Comment = i.Comment,
                       createDate = i.createDate,

                   };

        }

        public void saveShipment(Shipment shipment)
        {
            if (shipment != null)
            {

                //see if it's in the db
                Kawa.Models.Data.Shipment add;

                if (shipment.ID > 0)
                {
                    add = _db.Shipments.Where(x => x.ID == shipment.ID).SingleOrDefault();
                }
                else
                {
                    add = new Kawa.Models.Data.Shipment();
                }

                //synch it
                add.OrderID = shipment.OrderID;
                add.ShippingMethodID = shipment.ShippingMethodID;
                add.CarrierID = shipment.CarrierID;
                add.TrackingNo = shipment.TrackingNo;
                add.Comment = shipment.Comment;
                add.createDate = shipment.createDate;


                //save it
                if (add.ID == 0)
                    _db.Shipments.InsertOnSubmit(add);

                _db.SubmitChanges();

                shipment.ID = add.ID;
            }
        }

        public bool deleteShipment(Shipment shipment)
        {
            var delShipment = from o in _db.Shipments
                              where o.ID == shipment.ID
                              select o;


            //delete the order
            _db.Shipments.DeleteAllOnSubmit(delShipment);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        #endregion

        #region Flags

        public IQueryable<Flag> listAllFlags()
        {
            return from i in _db.Flags
                   select new Flag
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Type = i.Type,
                       Labels = i.Labels,
                       Notes = i.Notes,
                       isHidden = i.isHidden==1,
                       createDate = i.createDate,
                       triggers = i.FlagTriggers.Select(x => new FlagTrigger {
                       			ID = x.ID,
						        FlagID = x.FlagID,
			                    FirstName = x.FirstName,
			                    Surname = x.Surname,
			                    Email = x.Email,
			                    Address1 = x.Address1,
			                    Address2 = x.Address2,
			                    Address3 = x.Address3,
			                    Address4 = x.Address4,
			                    Postcode = x.Postcode,
			                    CountryID = x.CountryID,
                                CountryName = x.Country.Name
							}).ToList()
                   };
        }

        public void saveFlag(Flag flag)
        {
            //see if it's in the db
            Kawa.Models.Data.Flag add;

            if (flag.ID > 0)
            {
                add = _db.Flags.Where(x => x.ID == flag.ID).SingleOrDefault();
            }
            else
            {
                add = new Kawa.Models.Data.Flag();
            }

            //synch it
            add.Name = flag.Name;
            add.Type = flag.Type;
            add.Labels = flag.Labels;
            add.Notes = flag.Notes;
            add.isHidden = flag.isHidden ? 1 : 0;
            add.createDate = flag.createDate;

            //save it
            if (add.ID == 0)
                _db.Flags.InsertOnSubmit(add);

            _db.SubmitChanges();

            flag.ID = add.ID;
        }

        public bool deleteFlag(Flag flag)
        {
            var delFlagTriggers = from o in _db.FlagTriggers
                          where o.FlagID == flag.ID
                          select o;
            _db.FlagTriggers.DeleteAllOnSubmit(delFlagTriggers);

            var delFlag = from o in _db.Flags
                          where o.ID == flag.ID
                          select o;

            //delete the order
            _db.Flags.DeleteAllOnSubmit(delFlag);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        public IQueryable<FlagTrigger> listAllFlagTriggers()
        {
			return from i in _db.FlagTriggers
					select new FlagTrigger {
                       			ID = i.ID,
						        FlagID = i.FlagID,
			                    FirstName = i.FirstName,
			                    Surname = i.Surname,
			                    Email = i.Email,
			                    Address1 = i.Address1,
			                    Address2 = i.Address2,
			                    Address3 = i.Address3,
			                    Address4 = i.Address4,
			                    Postcode = i.Postcode,
			                    CountryID = i.CountryID,
                                CountryName = i.Country.Name
							};
		}

		public void saveFlagTrigger(FlagTrigger flagTrigger)
		{
			//see if it's in the db
			Kawa.Models.Data.FlagTrigger add;

			if (flagTrigger.ID > 0)
			{
				add = _db.FlagTriggers.Where(x => x.ID == flagTrigger.ID).SingleOrDefault();
			}
			else
			{
                add = new Kawa.Models.Data.FlagTrigger();
			}

			//synch it
			add.FlagID = flagTrigger.FlagID;
			add.FirstName = flagTrigger.FirstName;
			add.Surname = flagTrigger.Surname;
			add.Email = flagTrigger.Email;
			add.Address1 = flagTrigger.Address1;
			add.Address2 = flagTrigger.Address2;
			add.Address3 = flagTrigger.Address3;
			add.Address4 = flagTrigger.Address4;
			add.Postcode = flagTrigger.Postcode;
			add.CountryID = flagTrigger.CountryID;
	
			
			//save it
			if (add.ID == 0)
				_db.FlagTriggers.InsertOnSubmit(add);

			_db.SubmitChanges();

			flagTrigger.ID = add.ID;
		}	
		
		public bool deleteFlagTrigger(FlagTrigger flagTrigger)
        {
             var delFlagTrigger = from o in _db.FlagTriggers
                            where o.ID == flagTrigger.ID
                           select o;


            //delete the order
            _db.FlagTriggers.DeleteAllOnSubmit(delFlagTrigger);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        #endregion

        #region Signature

        public IQueryable<Signature> listAllSignatures()
        {
            return from i in _db.Signatures
                   select new Signature
                   {
                       ID = i.ID,
                       Name = i.Name,
                       Html = i.Html,
                       Colour = i.Colour,
                       OrderNo = i.OrderNo,

                   };

        }

        public void saveSignature(Signature signature)
        {
            //see if it's in the db
            Kawa.Models.Data.Signature add;

            if (signature.ID > 0)
            {
                add = _db.Signatures.Where(x => x.ID == signature.ID).SingleOrDefault();
            }
            else
            {
                add = new Kawa.Models.Data.Signature();
            }

            //synch it
            add.Name = signature.Name;
            add.Html = signature.Html;
            add.Colour = signature.Colour;
            add.OrderNo = signature.OrderNo;


            //save it
            if (add.ID == 0)
                _db.Signatures.InsertOnSubmit(add);

            _db.SubmitChanges();

            signature.ID = add.ID;
        }

        public bool deleteSignature(Signature signature)
        {
            var delSignature = from o in _db.Signatures
                               where o.ID == signature.ID
                               select o;


            //delete the order
            _db.Signatures.DeleteAllOnSubmit(delSignature);

            _db.SubmitChanges();
            bool result = true;


            return result;
        }

        #endregion
    }

    public static class OrderRepositoryExtensions
    {
        public static OrderPayment createAggregate(this Data.OrderPayment i)
        {
            return new OrderPayment()
            {
                ID = i.ID,
                OrderID = i.OrderID,
                AccountDetailID = i.AccountDetailID,
                PaymentStatus = i.PaymentStatus,
                PaymentReference = i.PaymentReference,
                PaymentCode = i.PaymentCode,
                PaymentDate = i.PaymentDate,
                FullResponse = i.FullResponse,
                ResponseText = i.ResponseText,
                Type = (OrderPayment.TypeOption)i.Type,
                Service = (Services.PaymentServiceStore.PaymentTypeOption)i.Service
            };
        }

        public static Status createAggregate(this Data.Status i)
        {
            return new Status()
            {
                ID = i.ID,
                Name = i.Name,
                SetSystemStatus = (Order.SystemStatusOption?)i.SetSystemStatus,
                EmailSubject = i.EmailSubject,
                EmailHeader = i.EmailHeader,
                Color = i.Color,
                isNotifyCustomer = i.isNotifyCustomer,
                isNotifyStaff = i.isNotifyStaff,
                InventoryEffect = i.InventoryEffect,
                isPayOrderAgain = i.isPayOrderAgain,
                OrderNo = i.OrderNo,
                CreateDate = i.CreateDate,
                Notes = i.Notes,
                StatusSet = (Status.StatusSetOption?)i.StatusSet,
                RefactorToID = i.RefactorToID
            };
        }
    }
}