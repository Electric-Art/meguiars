﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using Kawa.Models.Services;

namespace Kawa.Models
{
    public class Variant
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        [Required]
        public string name { get; set; }

        
        public string sku { get; set; }
        [DisplayFormat(DataFormatString = "{0:#.##}", ApplyFormatInEditMode = true)]
        [Required]
        public decimal price { get; set; }
        [DisplayFormat(DataFormatString = "{0:#.##}", ApplyFormatInEditMode = true)]
        [DisplayName("rrp")]
        public decimal priceRRP { get; set; }
        [DisplayName("weight")]
        [Required]
        public decimal? weight { get; set; }
        [DisplayName("min qty")]
        public int? minQuantity { get; set; }
        [DisplayName("max qty")]
        public int? maxQuantity { get; set; }
        [DisplayName("no gst")]
		public bool noTax { get; set; }
        [DisplayName("stock till out")]
		public int? stockNo { get; set; }
        [DisplayName("max days to ship")]
        public int? shipDays { get; set; }
        public DateTime createDate { get; set; }
        public DateTime? modifiedDate { get; set; }
        [DisplayName("Out of Stock")]
        public bool isHidden { get; set; }
        [DisplayName("Pre-Order Ship Date")]
        public DateTime? preOrderShipDate { get; set; }
        [DisplayName("min days to ship")]
        public int? shipDaysMinimum { get; set; }
        public List<Price> bulkPrices { get; set; }


        [DisplayName("Delete this variant")]
        public bool delete { get; set; }

        [DisplayName("discount")]
        public decimal priceMargin { 
            get{
                if (priceRRP == 0) return 0;
                return ((priceRRP - price) / priceRRP) * 100;
            }
        }

        public string shipDaysOrDefault
        {
            get
            {
                var min = shipDaysMinimum ?? SettingsService.Pull<int>(SettingsService.SettingOption.Local_Default_Days_To_Ship_Minimum);
                var max = shipDays ?? SettingsService.Pull<int>(SettingsService.SettingOption.Local_Default_Days_To_Ship);
                return min == max ? max.ToString() : string.Format("{0}-{1}", min, max) ;
            }
        }

        public string preOrderShipDateStr
        {
            get
            {
                return preOrderShipDate.HasValue ? ((DateTime)preOrderShipDate).ToShortDateString() : null;
            }
        }

        public int getShipDaysOrDefault(Order order)
        {
            if(order.CountryID.HasValue && order.CountryID != ShippingService.getDefaultCountry().ID)
            {
                var defaultIntDays = SettingsService.Pull<int>(SettingsService.SettingOption.International_Default_Days_To_Ship);
                return (shipDays.HasValue  && shipDays > defaultIntDays) ? (int)shipDays : defaultIntDays;
            }
            else
            {
                return shipDays ?? SettingsService.Pull<int>(SettingsService.SettingOption.Local_Default_Days_To_Ship);
            }
        }

        public int getShipDaysOrDefaultMiniumum(Order order)
        {
            if (order.CountryID.HasValue && order.CountryID != ShippingService.getDefaultCountry().ID)
            {
                var defaultIntDays = SettingsService.Pull<int>(SettingsService.SettingOption.International_Default_Days_To_Ship_Minimum);
                return (shipDaysMinimum.HasValue && shipDaysMinimum > defaultIntDays) ? (int)shipDaysMinimum : defaultIntDays;
            }
            else
            {
                return shipDaysMinimum ?? SettingsService.Pull<int>(SettingsService.SettingOption.Local_Default_Days_To_Ship_Minimum);
            }
        }

        public string getShipDaysText(Order order, bool isShort)
        {
            if (isHidden && preOrderShipDate.HasValue)
                return string.Format("<b>PRE-ORDER</b> Expected to ship by <b>{0}</b>", ((DateTime)preOrderShipDate).ToShortDateString());

            var min = getShipDaysOrDefaultMiniumum(order);
            var max = getShipDaysOrDefault(order);
            return isShort ? 
                (min == max? string.Format("{0} business day(s)", max) : string.Format("{0}-{1} business day(s)", min, max))
                :
                (min == max ? string.Format("Usually ships within <b>{0} business day(s)</b>", max) : string.Format("Usually ships within <b>{0}-{1} business day(s)</b>", min, max));
        }

        public decimal getBulkPriceDiscount(Price bulkPrice)
        {
            if (priceRRP == 0) return 0;
            return ((priceRRP - bulkPrice.amount) / priceRRP) * 100;
        }

        public void changePricing(decimal discount, List<Kawa.Models.ViewModels.BulkDiscount> volumePrices, bool deleteExisting, ProductRepository repo)
        {
            price = priceRRP - (priceRRP * discount / 100);
            modifiedDate = DateTime.Now;
            repo.saveVariant(this);

            if(deleteExisting || (volumePrices != null && volumePrices.Count()>1))
            {
                bulkPrices.ForEach(x => repo.deletePrice(x));
            }

            if (volumePrices != null && volumePrices.Count() >= 1)
            { 
                volumePrices.ForEach(x => repo.savePrice(new Price()
                {
                    VariantID = ID,
                    amount = priceRRP - (priceRRP * x.discount / 100),
                    quantity = x.quantity
                }));
            }
        }
        
        public void changeBulkPricing(decimal newRrp, decimal newPrice, ProductRepository repo)
        {
            if (priceRRP != newRrp)
            { 
                foreach(var bulkPrice in bulkPrices)
                {
                    var oldDiscount = (decimal)getBulkPriceDiscount(bulkPrice);
                    bulkPrice.amount = newRrp - (newRrp * oldDiscount/100);
                    repo.savePrice(bulkPrice);
                }
            }
        }

        public Variant clone(int ProductID, ProductRepository repo)
        {
            var variant = (Variant)this.MemberwiseClone();
            variant.ID = 0;
            variant.createDate = DateTime.Now;
            variant.modifiedDate = DateTime.Now;
            variant.ProductID = ProductID;
            variant.name = name + "[Clone]";
            repo.saveVariant(variant);
            if (bulkPrices != null)
                bulkPrices.ForEach(x => x.clone(variant.ID, repo));
            return variant;
        }
    }

    /// <summary>
    /// Summary description for Price.
    /// </summary>
    public class Price
    {
        public int ID { get; set; }
        public int VariantID { get; set; }
        [DisplayFormat(DataFormatString = "{0:#}", ApplyFormatInEditMode = true)]
        [Required]
        public decimal amount { get; set; }
        [DisplayFormat(DataFormatString = "{0:#.##}", ApplyFormatInEditMode = true)]
        [Required]
        public int quantity { get; set; }
        [DisplayName("discount")]
        public decimal priceMargin { get; set; }

        public Price clone(int VariantID, ProductRepository repo)
        {
            var price = (Price)this.MemberwiseClone();
            price.ID = 0;
            price.VariantID = VariantID;
            repo.savePrice(price);
            return price;
        }
    }

}

namespace Kawa.Models.ViewModels
{
    public class VariantSet
    {
        public List<Variant> variants { get; set; }
    }
}