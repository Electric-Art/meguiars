﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using System.Globalization;

namespace Kawa.Models
{

    public class PrescriptionTag : SiteResourceOwner, ISearchResult
    {
        public enum TypeOption
        {
            SUPER_SECTION, //0
            PRODUCT_SECTION, //1
            ADHOC_PRODUCT_GROUP, //2
            PAGE_SECTION, //3
            BRAND_SECTION, //4
            ADHOC_PRODUCT_TAGS, //5
            SEARCH_TAGS, //6
            CAR_PRESCRIPTION_CAT

        }

        public enum SystemTagOption
        {
            SPECIALS, //0
            PRODUCT_CAT_BEST_SELLERS, //1
            PRODUCT_CAT_POPULAR_PRODUCTS, //2
            SUPER_CAT_PRODUCTS, //3
            SUPER_CAT_BRANDS, //4
            SUPER_CAT_ADHOC, //5
            SUPER_CAT_ADHOC_TAGS, //6
            PRODUCT_CAT_CLEARANCE, //7
            PRODUCT_CAT_FREE_SHIPPING, //8
            PRODUCT_CAT_VEGETARIAN, //9
            PRODUCT_CAT_VEGAN, //10
            PRODUCT_CAT_HEAT_SENSITIVE, //11
            PRODUCT_MAIN_CAT_CONDITIONS, //12
            PRODUCT_CAT_POPULAR_SUBCAT_PRODUCTS, //13
            PRODUCT_CAT_TRENDING_NOW, //14
            SEARCH_ENTITY_TRENDING, //15
            PRODUCT_CAT_HOT_SPECIAL, //16
            CAR_PRESCRIPTION
        }

        public enum OrderByTypeOption
        {
            ORDER_NO,
            ARTICLE_DATE
        }

        public enum TagRelationType
        {
            NORMAL,
            PRIMARY
        }

        public int ID { get; set; }
        [Required]
        public string name { get; set; }
        public TypeOption Type { get; set; }
        public SystemTagOption? SystemTag { get; set; }
        [DisplayName("Parent Section")]
        public int? ParentID { get; set; }
        [DisplayName("rank")]
        public int? orderNo { get; set; }
        [DisplayName("hidden")]
        public bool isHidden { get; set; }
        public OrderByTypeOption OrderByType { get; set; }
        public string nameUrl { get; set; }
        public string colour { get; set; }
        public int? isInverseTextColour { get; set; }

        [DisplayName("introduction")]
        public string aliases { get; set; }
        public string aliasesUrl { get; set; }
        [DisplayName("front end description")]
        public string html { get; set; }
        [DisplayName("meta description")]
        public string metaDescription { get; set; }

        public bool hasChildren { get; set; }
        public PrescriptionTag parent { get; set; }
        public List<Product> products { get; set; }
        public List<Page> pages { get; set; }
        public List<Tag> subtags { get; set; }
        public List<Tag> subtags2 { get; set; }

        public int oldID { get; set; }
        public int PropParentID { get; set; }

        public string plugurl { get; set; }

        public int level { get; set; }
        public int noOfProducts { get; set; }

        public string nameWithProductNo
        {
            get
            {
                return (isHidden ? "HIDDEN - " : "") + name + " (" + noOfProducts + ")";
            }
        }

        public string nameSpecialtag
        {
            get
            {
                return SystemTag == SystemTagOption.PRODUCT_CAT_HOT_SPECIAL ? name.TrimEnd('S') : name;
            }
        }

        public int noOfLiveProducts { get; set; }

        //SiteResource
        public override int getObjectID()
        {
            return ID;
        }

        public override ObjectTypeOption getObjectType()
        {
            return ObjectTypeOption.PRESCRIPTIONTAG;
        }

        public ObjectTypeOption objectType
        {
            get { return getObjectType(); }
        }

        //SiteMap
        public string CnAlternative { get; set; }

        public string getTagImage()
        {
            switch (SystemTag)
            {
                case PrescriptionTag.SystemTagOption.PRODUCT_CAT_CLEARANCE:
                    return "/content/images/systemcats/clearance.png";
                case PrescriptionTag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING:
                    return "/content/images/systemcats/delivery.png";
                case PrescriptionTag.SystemTagOption.PRODUCT_CAT_VEGETARIAN:
                    return "/content/images/systemcats/vegetarian.png";
                case PrescriptionTag.SystemTagOption.PRODUCT_CAT_VEGAN:
                    return "/content/images/systemcats/vegan.png";
                case PrescriptionTag.SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE:
                    return "/content/images/systemcats/heat-sensitive.png";
                case PrescriptionTag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL:
                    return "/content/images/systemcats/hotspecial.png";
            }
            return null;
        }

        public string toTitleCase()
        {
            return new CultureInfo("en-US", false).TextInfo.ToTitleCase(name.ToLower());
        }

        public string getTagColour()
        {
            /*
            switch (SystemTag)
            {
                case Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE:
                    return "#702a85";
                case Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING:
                    return "/content/images/systemcats/delivery.png";\
                case Tag.SystemTagOption.PRODUCT_CAT_VEGETARIAN:
                    return "#48753c";
                case Tag.SystemTagOption.PRODUCT_CAT_VEGAN:
                    return "#96662d";
                case Tag.SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE:
                    return "/content/images/systemcats/heat-sensitive.png";
                case Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL:
                    return "/content/images/systemcats/hotspecial.png";
            }
            */
            return colour;
        }

        public string getTagTextColour()
        {
            switch (ID)
            {
                case 1:
                    return "#000";
                case 2:
                    return "#fff";
                case 3:
                    return "#fff";
                case 4:
                    return "#000";
                case 5:
                    return "#fff";
                case 6:
                    return "#fff";
            }
            return null;
        }

        public string getUrl()
        {
            if (plugurl != null) return plugurl;

            if (SystemTag.HasValue && SystemTag == PrescriptionTag.SystemTagOption.SUPER_CAT_BRANDS)
            {
                return "/category/brands?mode=brands";
            }

            if (SystemTag.HasValue && SystemTag == PrescriptionTag.SystemTagOption.SUPER_CAT_PRODUCTS)
            {
                return "#";
            }
            if (Type == TypeOption.CAR_PRESCRIPTION_CAT || SystemTag == SystemTagOption.CAR_PRESCRIPTION)
            {
                if (ID == 0)
                    return "/home/prescription";

                return "/home/prescription/" + ID + "/" + nameUrl;
            }


            if ((parent != null && parent.Type == TypeOption.SUPER_SECTION && Type == TypeOption.PRODUCT_SECTION)
                ||
                Type == TypeOption.SUPER_SECTION
                )
                return "/products/" + nameUrl;

            if (Type == TypeOption.PRODUCT_SECTION ||
                Type == TypeOption.BRAND_SECTION ||
                Type == TypeOption.ADHOC_PRODUCT_TAGS ||
                SystemTag == SystemTagOption.SUPER_CAT_BRANDS)
                return "/products/" + nameUrl;
            return null;
        }

        public string url
        {
            get
            {
                return getUrl();
            }
        }

        public TypeOption getChildType()
        {
            if (Type == TypeOption.SUPER_SECTION)
            {
                switch (SystemTag)
                {
                    case SystemTagOption.SUPER_CAT_PRODUCTS:
                        return TypeOption.PRODUCT_SECTION;
                    case SystemTagOption.SUPER_CAT_BRANDS:
                        return TypeOption.BRAND_SECTION;
                    case SystemTagOption.SUPER_CAT_ADHOC:
                        return TypeOption.ADHOC_PRODUCT_GROUP;
                    case SystemTagOption.SUPER_CAT_ADHOC_TAGS:
                        return TypeOption.ADHOC_PRODUCT_TAGS;
                    case SystemTagOption.CAR_PRESCRIPTION:
                        return TypeOption.CAR_PRESCRIPTION_CAT;
                }
                throw new Exception("Supersection not recognised");
            }
            return Type;
        }

        public int getRank(string search, List<string> searchTokens)
        {
            if (name.ToLower() == search.ToLower()) return 1;
            if (name.Contains(search, StringComparison.OrdinalIgnoreCase)) return 2;
            if (aliases.Contains(search, StringComparison.OrdinalIgnoreCase)) return 3;
            var nameTokens = name.Split(' ').ToList();
            if (searchTokens.All(x => nameTokens.Any(y => y.Equals(x, StringComparison.OrdinalIgnoreCase)))) return 4;
            return 5;
        }

        public int getSpecialOrderNo()
        {
            switch (SystemTag)
            {
                case SystemTagOption.PRODUCT_CAT_FREE_SHIPPING:
                    return 1;
                case SystemTagOption.PRODUCT_CAT_HOT_SPECIAL:
                    return 2;
                case SystemTagOption.PRODUCT_CAT_CLEARANCE:
                    return 3;
                case SystemTagOption.PRODUCT_CAT_VEGETARIAN:
                    return 4;
                case SystemTagOption.PRODUCT_CAT_VEGAN:
                    return 5;
                case SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE:
                    return 6;
            }
            return 100;
        }
    }
    public class Tag : SiteResourceOwner, ISearchResult
    {
        public enum TypeOption
        {
            SUPER_SECTION, //0
            PRODUCT_SECTION, //1
            ADHOC_PRODUCT_GROUP, //2
            PAGE_SECTION, //3
            BRAND_SECTION, //4
            ADHOC_PRODUCT_TAGS, //5
            SEARCH_TAGS, //6
            CAR_PRESCRIPTION_CAT

        }

        public enum SystemTagOption
        {
            SPECIALS, //0
            PRODUCT_CAT_BEST_SELLERS, //1
            PRODUCT_CAT_POPULAR_PRODUCTS, //2
            SUPER_CAT_PRODUCTS, //3
            SUPER_CAT_BRANDS, //4
            SUPER_CAT_ADHOC, //5
            SUPER_CAT_ADHOC_TAGS, //6
            PRODUCT_CAT_CLEARANCE, //7
            PRODUCT_CAT_FREE_SHIPPING, //8
            PRODUCT_CAT_VEGETARIAN, //9
            PRODUCT_CAT_VEGAN, //10
            PRODUCT_CAT_HEAT_SENSITIVE, //11
            PRODUCT_MAIN_CAT_CONDITIONS, //12
            PRODUCT_CAT_POPULAR_SUBCAT_PRODUCTS, //13
            PRODUCT_CAT_TRENDING_NOW, //14
            SEARCH_ENTITY_TRENDING, //15
            PRODUCT_CAT_HOT_SPECIAL, //16
            CAR_PRESCRIPTION
        }

        public enum OrderByTypeOption
        {
            ORDER_NO,
            ARTICLE_DATE
        }

        public enum TagRelationType
        {
            NORMAL,
            PRIMARY
        }

        public int ID { get; set; }
        [Required]
        public string name { get; set; }
        public TypeOption Type { get; set; }
        public SystemTagOption? SystemTag { get; set; }
        [DisplayName("Parent Section")]
        public int? ParentID { get; set; }
        [DisplayName("rank")]
        public int? orderNo { get; set; }
        [DisplayName("hidden")]
        public bool isHidden { get; set; }
        public OrderByTypeOption OrderByType { get; set; }
        public string nameUrl { get; set; }
        public string colour { get; set; }
        public int? isInverseTextColour { get; set; }

        [DisplayName("introduction")]
        public string aliases { get; set; }
        public string aliasesUrl { get; set; }
        [DisplayName("front end description")]
        public string html { get; set; }
        [DisplayName("meta description")]
        public string metaDescription { get; set; }

        public bool hasChildren { get; set; }
        public Tag parent { get; set; }
        public List<Product> products { get; set; }
        public List<Page> pages { get; set; }
        public List<Tag> subtags { get; set; }
        public List<Tag> subtags2 { get; set; }

        public int oldID { get; set; }
        public int PropParentID { get; set; }

        public string plugurl { get; set; }

        public int level { get; set; }
        public int noOfProducts { get; set; }
        
        public string nameWithProductNo
        {
            get {
                return (isHidden?"HIDDEN - ":"") + name + " (" + noOfProducts + ")";
            }
        }

        public string nameSpecialtag
        {
            get
            {
                return SystemTag == SystemTagOption.PRODUCT_CAT_HOT_SPECIAL ? name.TrimEnd('S') : name;
            }
        }

        public int noOfLiveProducts { get; set; }

        //SiteResource
        public override int getObjectID()
        {
            return ID;
        }

        public override ObjectTypeOption getObjectType()
        {
            return ObjectTypeOption.TAG;
        }

        public ObjectTypeOption objectType
        {
            get { return getObjectType(); }
        }

        //SiteMap
        public string CnAlternative { get; set; }

        public string getTagImage()
        {
            switch (SystemTag)
            {
                case Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE:
                    return "/content/images/systemcats/clearance.png";
                case Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING:
                    return "/content/images/systemcats/delivery.png";
                case Tag.SystemTagOption.PRODUCT_CAT_VEGETARIAN:
                    return "/content/images/systemcats/vegetarian.png";
                case Tag.SystemTagOption.PRODUCT_CAT_VEGAN:
                    return "/content/images/systemcats/vegan.png";
                case Tag.SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE:
                    return "/content/images/systemcats/heat-sensitive.png";
                case Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL:
                    return "/content/images/systemcats/hotspecial.png";
            }
            return null;
        }

        public string toTitleCase()
        {
            return new CultureInfo("en-US", false).TextInfo.ToTitleCase(name.ToLower());
        }

        public string getTagColour()
        { 
            /*
            switch (SystemTag)
            {
                case Tag.SystemTagOption.PRODUCT_CAT_CLEARANCE:
                    return "#702a85";
                case Tag.SystemTagOption.PRODUCT_CAT_FREE_SHIPPING:
                    return "/content/images/systemcats/delivery.png";\
                case Tag.SystemTagOption.PRODUCT_CAT_VEGETARIAN:
                    return "#48753c";
                case Tag.SystemTagOption.PRODUCT_CAT_VEGAN:
                    return "#96662d";
                case Tag.SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE:
                    return "/content/images/systemcats/heat-sensitive.png";
                case Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL:
                    return "/content/images/systemcats/hotspecial.png";
            }
            */
            return colour;
        }

        public string getTagTextColour()
        {
            switch (ID)
            {
                case 1:
                    return "#000";
                case 2:
                    return "#fff";
                case 3:
                    return "#fff";
                case 4:
                    return "#000";
                case 5:
                    return "#fff";
                case 6:
                    return "#fff";
            }
            return null;
        }

        public string getUrl()
        {
            if (plugurl != null) return plugurl;
            
            if (SystemTag.HasValue && SystemTag == Tag.SystemTagOption.SUPER_CAT_BRANDS)
            {
                return "/category/brands?mode=brands";
            }

            if (SystemTag.HasValue && SystemTag == Tag.SystemTagOption.SUPER_CAT_PRODUCTS)
            {
                return "#";
            }
            if (Type == TypeOption.CAR_PRESCRIPTION_CAT || SystemTag == SystemTagOption.CAR_PRESCRIPTION)
            {
                if (ID == 0)
                    return "/home/prescription";

                return "/home/prescription/" + ID + "/" + nameUrl;
            }


            if ((parent != null && parent.Type == TypeOption.SUPER_SECTION && Type == TypeOption.PRODUCT_SECTION)
                ||
                Type == TypeOption.SUPER_SECTION
                )
                return "/products/" + nameUrl;

            if (Type == TypeOption.PRODUCT_SECTION ||
                Type == TypeOption.BRAND_SECTION ||
                Type == TypeOption.ADHOC_PRODUCT_GROUP ||
                Type == TypeOption.ADHOC_PRODUCT_TAGS || 
                SystemTag == SystemTagOption.SUPER_CAT_BRANDS)
                return "/products/" + nameUrl;
            return null;
        }

        public string url
        {
            get
            {
                return getUrl();
            }
        }

        public TypeOption getChildType()
        {
            if (Type == TypeOption.SUPER_SECTION)
            {
                switch (SystemTag)
                {
                    case SystemTagOption.SUPER_CAT_PRODUCTS:
                        return TypeOption.PRODUCT_SECTION;
                    case SystemTagOption.SUPER_CAT_BRANDS:
                        return TypeOption.BRAND_SECTION;
                    case SystemTagOption.SUPER_CAT_ADHOC:
                        return TypeOption.ADHOC_PRODUCT_GROUP;
                    case SystemTagOption.SUPER_CAT_ADHOC_TAGS:
                        return TypeOption.ADHOC_PRODUCT_TAGS;
                    case SystemTagOption.CAR_PRESCRIPTION:
                        return TypeOption.CAR_PRESCRIPTION_CAT;
                }
                throw new Exception("Supersection not recognised");
            }
            return Type;
        }

        public int getRank(string search, List<string> searchTokens)
        {
            if (name.ToLower() == search.ToLower()) return 1;
            if (name.Contains(search, StringComparison.OrdinalIgnoreCase)) return 2;
            if (aliases.Contains(search, StringComparison.OrdinalIgnoreCase)) return 3;
            var nameTokens = name.Split(' ').ToList();
            if (searchTokens.All(x => nameTokens.Any(y => y.Equals(x, StringComparison.OrdinalIgnoreCase)))) return 4;
            return 5;
        }

        public int getSpecialOrderNo()
        {
            switch(SystemTag)
            {
                case SystemTagOption.PRODUCT_CAT_FREE_SHIPPING:
                    return 1;
                case SystemTagOption.PRODUCT_CAT_HOT_SPECIAL:
                    return 2;
                case SystemTagOption.PRODUCT_CAT_CLEARANCE:
                    return 3;
                case SystemTagOption.PRODUCT_CAT_VEGETARIAN:
                    return 4;
                case SystemTagOption.PRODUCT_CAT_VEGAN:
                    return 5;
                case SystemTagOption.PRODUCT_CAT_HEAT_SENSITIVE:
                    return 6;
            }
            return 100;
        }
    }

    /// <summary>
    /// Summary description for Page.
    /// </summary>
    public class Page : SiteResourceOwner, IOrderedObject
    {
        public enum TypeOption
        {
            CONTENT,
            CONTENT_VIDEO,
            CLICK_THROUGH,
            BANNERS,
            FILES_ONLY,
            SOCIAL_IMPORT,
            TINY_BANNER,
            TWO_BLOCK,
            CONTENT_INSERT,
            NEWS_SELECTOR,
            FAQ,
            CATALOG_CONTAINER
        }

        public enum SystemPageOption
        {
            ABOUT_US, // 0
            TERMS_AND_CONDITIONS, // 1
            PRIVACY, // 2
            OUR_BRANDS, // 3
            CONTACT_US, // 4
            SHIPPING, // 5
            RETURNS, // 6
            GIFT_VOUCHERS, // 7
            SPECIAL_MAILCHIMP_REGISTER_FOR_VOUCHER, // 8
            POPULAR_BRANDS, // 9
  

            SHIPPING_BANNER_1 = 50,
            SHIPPING_BANNER_2 = 51,

            HOME_PAGE_SLIDE_SHOW = 100,
            FACEBOOK_PAGE_LAST_POST = 101,
            INSTAGRAM_LAST_POST = 102,
            
            RETRACTION = 200,
            BILLING_DETAILS_CC = 201,
            HOMEPAGE_METADESCRIPTION = 202,



            DELIVERY_POSTCODE_CHANGE_DETECTED =300,
            ACCOUNTDETAIL_LOOKUP_TEXT = 400,
            ACCOUNTDETAIL_STATEPOSTCODE_CHANGE_TEXT
        }

        public int ID { get; set; }
        [DisplayName("Name")]
        [Required]
        public string name { get; set; }
        [DisplayName("Page Type")]
        public TypeOption Type { get; set; }
        [DisplayName("Name in the menu")]
        public string nameMenu { get; set; }
        [DisplayName("Story Brief")]
        public string html { get; set; }
        [DisplayName("Content")]
        public string html2 { get; set; }
        [DisplayName("Author")]
        public string html3 { get; set; }
        public string html4 { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime? modifiedDate { get; set; }
        public int? SiteID { get; set; }
        public int? ParentID { get; set; }
        public int? orderNo { get; set; }
        [DisplayName("Offline")]
        public bool isHidden { get; set; }
        [DisplayName("Article date")]
        public DateTime? articleDate { get; set; }
        public SystemPageOption? SystemPage { get; set; }
        public int? ProductID { get; set; }

        public Tag mainCategory { get; set; }
        public List<Page> pages { get; set; }
        public List<Tag> tags { get; set; }


        public List<Product> products { get; set; }

        public int? rankNo { get; set; }
        public int? rankNo1 { get; set; }
        public int? rankNo2 { get; set; }
        public int? rankNo3 { get; set; }

        public string html_first { get; set; }
        public string html_rest { get; set; }

        public Favourite favourite { get; set; }

        public override int getObjectID()
        {
            return ID;
        }
        public override ObjectTypeOption getObjectType()
        {
            return ObjectTypeOption.PAGE;
        }

        public string getUrl()
        {
            if (Type == TypeOption.CLICK_THROUGH)
            {
                return html2;
            }

            if (SystemPage.HasValue)
            {
                var url = getUrlBySystemPage((Page.SystemPageOption)SystemPage);
                if (url != null)
                    return url;
            }

            return "/articles/" + ID + "/" + ModelTransformations.EncodeStringToUrl(getShortName());
        }

        
        public static string getUrlBySystemPage(Page.SystemPageOption SystemPage)
        {
            switch (SystemPage)
            {
                case Page.SystemPageOption.ABOUT_US:
                    return "/about/";
                case Page.SystemPageOption.CONTACT_US:
                    return "/contact/";
                default:
                    return null;
            }
        }

        public static SystemPageOption getSystemPageByUrl(string url)
        {
            Page.SystemPageOption systemPage = SystemPageOption.ABOUT_US;
            /*
            switch (url)
            {
                case null:
                    systemPage = Page.SystemPageOption.ABOUT_FRANK_READ;
                    break;
                case "premium-formulae":
                    systemPage = Page.SystemPageOption.ABOUT_HOW_WE;
                    break;
                case "100-percent-australian-made":
                    systemPage = Page.SystemPageOption.ABOUT_AUSTRALIAN_MADE;
                    break;
                case "australian-family-owned-business":
                    systemPage = Page.SystemPageOption.PERCENT_100;
                    break;
                case "100-percent-money-back-guarantee":
                    systemPage = Page.SystemPageOption.MONEY_BACK;
                    break;
            }
             * */
            return systemPage;
        }

        public static string getClassBySystemPage(Page.SystemPageOption SystemPage)
        {
            var @class = "";
            /*
            switch (SystemPage)
            {
                case Page.SystemPageOption.ABOUT_FRANK_WATCH_VIDEO:
                    break;
                case Page.SystemPageOption.ABOUT_HOW_WE:
                    @class = "about-pre-bord";
                    break;
                case Page.SystemPageOption.ABOUT_AUSTRALIAN_MADE:
                    @class = "about-made-bord";
                    break;
                case Page.SystemPageOption.PERCENT_100:
                    @class = "about-family-bord";
                    break;
                case Page.SystemPageOption.MONEY_BACK:
                    @class = "about-money-bord";
                    break;
            }
                             * */
            return @class;
        }

        public string disclaimerAbreviation
        {
            get
            {
              
                return null;
            }
        }

        public string getDisclaimerAbreviation()
        {


            return "No";
        }

        public string getShortName()
        {
            return (string.IsNullOrEmpty(nameMenu) ? name : nameMenu);
        }

        public string getUrlAdmin()
        {
            return "/admin/pageedit/" + ID;
        }

        public bool fitForSearch()
        {
            return !isHidden &&
                    (Type == Page.TypeOption.CONTENT || Type == Page.TypeOption.CONTENT_VIDEO);
        }

        public int getRank(string search, List<string> searchTokens)
        {
            if (name.ToLower() == search.ToLower()) return 1;
            if (name.Contains(search, StringComparison.OrdinalIgnoreCase)) return 2;
            return 3;
        }

        public static Page ShippingBannerDefault(int id)
        {
            return id == 1 ? new Page()
            {
                Type = Models.Page.TypeOption.TINY_BANNER,
                SystemPage = Models.Page.SystemPageOption.SHIPPING_BANNER_1,
                name = "FREE SHIPPING",
                html = "sc-truck.png",
                html2 = "*conditions apply",
                html3 = "orders within AUS Over $199"
            } :
            new Page()
            {
                Type = Models.Page.TypeOption.TINY_BANNER,
                SystemPage = Models.Page.SystemPageOption.SHIPPING_BANNER_2,
                name = "INTERNATIONAL SHIPPING",
                html = "sc-plane.png",
                html2 = "*conditions apply",
                html3 = "available to selected countries."
            };
        }

        public static Page ContentInsertDefault(Page.SystemPageOption id)
        {
            switch(id)
            {
                case Page.SystemPageOption.BILLING_DETAILS_CC:
                    return new Page()
                    {
                        Type = Models.Page.TypeOption.CONTENT_INSERT,
                        SystemPage = id,
                        name = "Billing Details Credit Card Warning",
                        html = "<b>IMPORTANT: IF PAYING WITH A CREDIT CARD</b><br/>If paying with a credit card, \"Billing Details\" should be the residential address of the card holder. You can provide a PO Box or Business address in the \"Delivery Details\" section if needed."
                    };
                case Page.SystemPageOption.HOMEPAGE_METADESCRIPTION:
                    return new Page()
                    {
                        Type = Models.Page.TypeOption.CONTENT_INSERT,
                        SystemPage = id,
                        name = "Homepage Metadescription",
                        html = ""
                    };
            }
            return null;
        }

        public bool isHtml()
        {
            return SystemPage != Kawa.Models.Page.SystemPageOption.HOMEPAGE_METADESCRIPTION;
        }
    }

    public class Slot : IOrderedObject
    {
        public enum SpotOption
        {
            HOME_PAGE_MODULE,
            SITE_WIDE_BANNER,
            SITE_WIDE_SIGNUP,
            VIDEO_MODULE
        }

        public int ID { get; set; }
        public int Type { get; set; }
        public SpotOption Spot { get; set; }
        public int? PageID { get; set; }
        public int? orderNo { get; set; }

        public Page page { get; set; }
    }

    public class AdminPageRouteVariables
    {
        public int? id { get; set; }
        public string t  { get; set; }
        public string m { get; set; }

        public bool injectID;
    }

    public class ResultFilterViewModel
    {
        public List<PriceFilter> prices
        {
            get
            {
                return new List<PriceFilter>(){
                    new PriceFilter(){ ID=1, name="$25 and below" },
                    new PriceFilter(){ ID=2, name="$25-50" },
                    new PriceFilter(){ ID=3, name="$50-100" },
                    new PriceFilter(){ ID=4, name="$100 and above" }
                };
            }
        }
        public List<Tag> brands { get; set; }
        public List<Tag> specialties { get; set; }

        public List<PriceFilter> selectedPrices { get; set; }
        public List<Tag> selectedBrands { get; set; }
        public List<Tag> selectedSpecialties { get; set; }

        public PostedResultFilters postedResultFilters { get; set; }
    }
    public enum GalleryTypeOption
    {
        Content,
        Promotion_Stack,
        More_Info,
        Icon_Holder,
        Gallery_Photo,
        Gallery_Video,
        Manual_Content
    }
    public class GalleryViewModel
    {
        public List<Models.Data.LmcGallery> Galleries { get; set; }
        public Models.Data.LmcGallery Gallery { get; set; }
    }

    public class EventViewModel
    {
        public string PageHeading { get; set; }
        public Page Header { get; set; }
        public List<Kawa.Models.Data.CalendarEvent> List { get; set; }
        public List<DateTime> ArchiveOptions { get; set; }
        public Kawa.Models.Data.CalendarEvent Item { get; set; }

        public string Current
        {
            get
            {
                return DateTime.Now.UtcToLocal().Year.ToString() + "," + DateTime.Now.UtcToLocal().Month.ToString();
            }
        }
    }
    public class NewsViewModel
    {
        public string PageHeading { get; set; }
        public Page Header { get; set; }
        public List<Kawa.Models.Data.New> NewsList { get; set; }
        public List<Kawa.Models.Data.New> RecentNews { get; set; }
        public List<DateTime> ArchiveOptions { get; set; }
        public Kawa.Models.Data.New NewsItem { get; set; }

        public string Current
        {
            get
            {
                return DateTime.Now.UtcToLocal().Year.ToString() + "," + DateTime.Now.UtcToLocal().Month.ToString();
            }
        }
    }
    public class PriceFilter
    {
        public int ID { get; set; }
        public string name { get; set; }
    }

    public class PostedResultFilters
    {
        public int[] PriceIDs { get; set; }
        public int[] BrandIDs { get; set; }
        public int[] SpecialtiesIDs { get; set; }

        public string search { get; set; }

        public bool isInPlay()
        {
            return PriceIDs != null ||  BrandIDs != null || SpecialtiesIDs != null;
        }
    }
}

namespace Kawa.Models.ModelVariants
{
    public class Page
    {
        public int ID { get; set; }
        [Required]
        public string name { get; set; }
        public int? SiteID { get; set; }
        public int? rankNo1 { get; set; }
        public int? rankNo2 { get; set; }
        public int? rankNo3 { get; set; }

        public Kawa.Models.Page.SystemPageOption? SystemPage { get; set; }
        public Kawa.Models.Page.TypeOption Type { get; set; }
    }
}

namespace Kawa.Models.Data
{
    public partial class Product_Page
    {
        public enum ActionOption
        {
            Engage,
            Suppress
        }

        public ActionOption? ActionSetting
        {
            get { return (ActionOption?)Action; }
            set { Action = (int?)value; }
        }
    }


}

namespace Kawa.Models.Coms
{
    public class Tag
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public Models.Tag.TypeOption Type { get; set; }
    }



}
