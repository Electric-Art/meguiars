﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kawa.Models
{
    public interface IOrderedObject
    {
        int ID  { get; set; }
        int? orderNo  { get; set; }
    }
}