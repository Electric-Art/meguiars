﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;

namespace Kawa.Models
{


    public class ProductSchema
    {
        public string  @context { get; set; }
        public string @type { get; set; }
        public string name { get; set; }
        public List<string> image { get; set; }
        public string description { get; set; }
        public string sku { get; set; }
        public BrandSchema brand { get; set; }
        public OfferSchema offers { get; set; }

    }
    public class BrandSchema
    {
        public string @type { get; set; }
        public string name { get; set; }
    }

    public class OfferSchema
    {
        /* "@@type": "Offer",
                "url": "@ConfigurationManager.AppSettings["SiteUrl"]@Model.getUrl()",
                "priceCurrency": "NZD",
                "price": "@Model.getBuyPrice().ToString("#.##")",
                "priceValidUntil": "@System.DateTime.Now.AddDays(7)",
                "itemCondition": "https://schema.org/NewCondition",
                "availability": "https://schema.org/InStock"*/
        public string @type { get; set; }
        public string url { get; set; }
        public string priceCurrency { get; set; }
        public string price { get; set; }
        public string priceValidUntil { get; set; }
        public string itemCondition { get; set; }
        public string availability { get; set; }
    }

    public class Product : SiteResourceOwner, ISearchResult
    {

        public ProductSchema Schema {get;set;}
        public enum SpecialTypeOption
        {
            WEIGHT_ONLY_ICE_COLD_PACK
        }
        public decimal getBuyPrice()
        {
            if(promotion != null)
            {
                return PromotionService.getBuyPrice(variant, promotion);
            }
            else
                return variant.price;
        }
        public int ID { get; set; }
        [DisplayName("name")]
        public string name { get; set; }
        public string nameUrl { get; set; }
        [DisplayName("description title")]
        public string titleName { get; set; }
        [DisplayName("menu name")]
        public string menuName { get; set; }
        public string intro { get; set; }
        [DisplayName("long description")]
        public string blurb { get; set; }
        [DisplayName("ingredients")]
        public string activeIngredients { get; set; }
        public string formulation { get; set; }
        public string dosage { get; set; }
        [DisplayName("warnings")]
        public string warnings { get; set; }
        [DisplayName("meta description")]
        public string specifications { get; set; }
        [DisplayName("free from")]
        public string freeFrom { get; set; }
        public string interactions { get; set; }
        public string availability { get; set; }
        public string sku { get; set; }
        public decimal? price { get; set; }
        [DisplayName("special price")]
        public decimal? specialPrice { get; set; }
        public string aliases { get; set; }
        public string aliasesUrl { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime? modifiedDate { get; set; }
        [DisplayName("hide product")]
        public bool isHidden { get; set; }
        [DisplayName("google category ID")]
        public string googleCat { get; set; }
        [DisplayName("bing category ID")]
        public string bingCat { get; set; }
        [DisplayName("barcode")]
        public string gtin { get; set; }
        [DisplayName("Australia only")]
        public bool nationalOnly { get; set; }
        public SpecialTypeOption? specialType { get; set; }
        public int? orderOverride { get; set; }
        [DisplayName("directions")]
        public string directions { get; set; }
        [DisplayName("no added")]
        public string noAdded { get; set; }
        [DisplayName("hide long description")]
        public bool hideBlurb { get; set; }
        [DisplayName("pick/pack notes")]
        public string packNotes { get; set; }
        [DisplayName("AUST L")]
        public string austL { get; set; }
        [DisplayName("purchase order name")]
        public string poName { get; set; }

        public int oldID { get; set; }
        public string sHero { get; set; }
        public string sThumb { get; set; }
        public string sFiles { get; set; }
        public string metadescription { get; set; }
        public string metakeywords { get; set; }

        public int ProductTagRelationShipRank { get; set; }
        public int ProductPageRelationShipRank { get; set; }
        public int ProductPrescriptionTagRelationShipRank { get; set; }
        public Promotion promotion { get; set; }
        public Variant variant
        {
             get
             {
                 if (variants == null) return null;
                 return variants.FirstOrDefault();
             }
        }
        public bool HasVariant { get; set; }
        public bool HasVariantOutOfStock { get; set; }
        public bool HasVariantPreOrder { get; set; }
        public int? VariantStockNo { get; set; }
        public int? VariantMinQuantity { get; set; }
        public int? VariantMaxQuantity { get; set; }
        public DateTime? VariantPreOrder
        {
            get
            {
                if (variants == null || variants.Count()==0) return null;
                return variants.First().preOrderShipDate;
            }
        }
        public string VariantPreOrderNice
        {
            get
            {
                if (VariantPreOrder.HasValue)
                    return ((DateTime)VariantPreOrder).ToShortDateString();
                return null;
            }
        }

        public bool CanBeOrdered {
            get
            {
                return !(HasVariantOutOfStock && !HasVariantPreOrder);
            }
        }

        public Variant saveVar { get; set; }

        public Tag mainCategory { get; set; }
        public List<Tag> tags { get; set; }
        public List<Tag> markertags { get; set; }
        public List<Tag> brandtags { get; set; }
        [DisplayName("warning type(s)")]
        public List<Page> disclaimers { get; set; }
        public List<Product> products { get; set; }
        public List<Variant> variants { get; set; }
        public List<QandA> questionAndAnswers { get; set; }
        public List<Ingredient> ingredients { get; set; }

        public List<Page> disclaimersDisplay {
            get
            {
               
                    return disclaimers.Union(new List<Page>() { new Page() { SystemPage = Page.SystemPageOption.ABOUT_US } }).OrderBy(x => x.SystemPage).ToList();
               
            }
        }

        public Basket basketItem { get; set; }
        public Favourite favourite { get; set; }

        public Basket basket { get; set; }
        public string disclaimer { get; set; }

        public override int getObjectID()
        {
            return ID;
        }

        public override ObjectTypeOption getObjectType()
        {
            return ObjectTypeOption.PRODUCT;
        }
        
        public string getUrl()
        {
            return "/product/" + nameUrl;
        }

        public static string getUrl(string nameUrl)
        {
            return "/product/" + nameUrl;
        }

        public string url
        {
            get
            {
                return getUrl();
            }
        }


        public ObjectTypeOption objectType
        {
            get { return getObjectType(); }
        }

        public string getUrlName()
        {
            return ModelTransformations.EncodeStringToUrl(getShortName());
        }

        public string getAdminUrl()
        {
            return "/admin/productedit/" + ID + "/";
        }

        public string getShortName()
        {
            return (string.IsNullOrEmpty(menuName) ? name : menuName);
        }

        public string getUrl(int TagID)
        {
            return getUrl() + "?t=" + TagID;
        }

        public int getRank(string search, List<string> searchTokens)
        {
            if (name.ToLower() == search.ToLower()) return 1;
            if (name.Contains(search, StringComparison.OrdinalIgnoreCase)) return 2;
            if (!string.IsNullOrEmpty(aliases) && aliases.Contains(search, StringComparison.OrdinalIgnoreCase)) return 3;

            var nameTokens = name.Split(' ').ToList();
            if(searchTokens.All(x => nameTokens.Any(y => y.Equals(x, StringComparison.OrdinalIgnoreCase)))) return 4;

            return 5;
        }

        public bool shouldShowContent(Page.SystemPageOption? systemPage)
        {
          
                return disclaimers.Any(x => x.SystemPage == systemPage);
        }

        public Product clone(ProductRepository repo)
        {
            var product = (Product)this.MemberwiseClone();
            product.ID = 0;
            product.name = name + " [Clone]";
            product.createdDate = DateTime.Now;
            product.modifiedDate = DateTime.Now;
            product.isHidden = true;
            product.aliases = null;
            repo.saveProduct(product, true, true, true);
            variant.clone(product.ID, repo);
            return product;
        }

    }

    public class FeedProduct
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string nameUrl { get; set; }
        public string sku { get; set; }
        public string specifications { get; set; }
        public decimal price { get; set; }
        public string googleCat { get; set; }
        public string bingCat { get; set; }
        public string gtin { get; set; }
        public bool freeShipping { get; set; }
        public bool preorder { get; set; }
        public SiteResource image { get; set; }
        public Tag brand { get; set; }
        public List<Tag> cats { get; set; }
        public List<string> localCatPaths { get; set; }
        public DateTime? preorderDate { get; set; }
        public string getUrl()
        {
            return "/product/" + nameUrl;
        }

    }

    public class Ingredient : SiteResourceOwner, IOrderedObject
    {
        public int ID { get; set; }
        public int ProductID { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime createDate { get; set; }
        public int? orderNo { get; set; }

        public override int getObjectID()
        {
            return ID;
        }

        public override ObjectTypeOption getObjectType()
        {
            return ObjectTypeOption.INGREDIENT;
        }
    }

    public class QandA
    {
        public int ID { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
        public int? ProductID { get; set; }
        public int? OrderNo { get; set; }
        public DateTime createDate { get; set; }
    }

    public class StockNotice
    {
        public enum StatusOption
        {
            PENDING,
            SENT,
            CANCELLED
        }

        public int ID { get; set; }
        [EmailAddress]
        [Required]
        public string email { get; set; }
        public int? ProductID { get; set; }
        public int? VariantID { get; set; }
        public StatusOption Status { get; set; }
        public string notes { get; set; }
        public DateTime createDate { get; set; }
        public DateTime? sentDate { get; set; }
    }

    public class Symptom
    {
        public int ID { get; set; }
        public string name { get; set; }

        public List<Product> products { get; set; }
    }
}



namespace Kawa.Models.Coms
{
    public class Product
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string imageUrl { get; set; }
        public decimal price { get; set; }
        public string sku { get; set; }

        public List<Tag> brandtags { get; set; }
    }

    public class ProductTypeahead
    {
        public int ID { get; set; }
        public string name { get; set; }
    }
}

namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class SearchProduct
    {
        [FieldTitle("Name")]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string name;
        [FieldTitle("Code")]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string code; //sku
        [FieldTitle("Warn")]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string warn;//disclaimersDisplay
        [FieldTitle("GoogleCat")]
        public string googleCat;
        [FieldTitle("BingCat")]
        public string bingCat;
        [FieldTitle("ShipDays")]
        public string shipDays;//{{item.variant.shipDays}} {{item.VariantPreOrderNice}}
        [FieldTitle("StockCount")]
        public string stockCount;// VariantStockNo
        [FieldTitle("Gtin")]
        public string gtin;
        [FieldTitle("AustL")]
        public string austL;
        [FieldTitle("RRP")]
        public string rrp;//item.variant.priceRRP
        [FieldTitle("%")]
        public string discountPercent;//item.variant.priceMargin
        [FieldTitle("Sale Price")]
        public string price;//item.variant.price
        [FieldTitle("Status")]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string status;//{{item.isHidden ? 'offline' : 'online'}}
    }

    [DelimitedRecord(",")]
    public class StockNotification
    {
        [FieldTitle("Email")]
        [FieldQuoted('"', QuoteMode.OptionalForBoth)]
        public string email;
    }
}