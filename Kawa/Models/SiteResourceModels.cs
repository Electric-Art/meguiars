﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;

namespace Kawa.Models
{
    /// <summary>
    /// Summary description for SiteResource.
    /// </summary>
    public class SiteResource : IOrderedObject, ISavedFile
    {

        public enum SiteResourceTypeOption
        {
            PRODUCT_IMAGE = 0,
            IN_CONTENT = 1,
            BILLBOARD = 2,
            BILLBOARD_MOBILE = 3,
            FILE_FOR_DOWNLOAD = 4,
            ARTICLE_HEADER = 5,
            ARTICLE_FEATURED_WIDE = 6,
            ARTICLE_SQUARE = 7,
            BRAND_LOGO = 8,
            XTRA_PRODUCT_IMAGE = 9,
            ////
            SLIDESHOW_MP4,
            SLIDESHOW_MOBILE_MP4,
            SLIDESHOW_IMAGE,
            IMAGE,
            CONSUMER_CATALOG,
            PROFESSIONAL_CATALOG,
            MARINE_CATALOG
        }

        public enum SiteResourceVersion
        {
            ITEM_PAGE,
            ITEM_PAGE_RETINA,
            ITEM_LIST,
            ITEM_LIST_RETINA,
            ITEM_SEARCH,
            ITEM_SEARCH_RETINA,
            ITEM_LARGE,
            ITEM_LARGE_RETINA,
            ITEM_FEED,
            ITEM_FEED_RETINA
        }


        public int ID { get; set; }
        public string FileName { get; set; }
        public SiteResourceTypeOption SiteResourceType { get; set; }
        public string Caption { get; set; }
        public string Caption2 { get; set; }
        public string embedCode { get; set; }
        public int? width { get; set; }
        public int? height { get; set; }
        public int? ParentID { get; set; }
        public int ObjectID { get; set; }
        public SiteResourceOwner.ObjectTypeOption ObjectType { get; set; }
        public int? orderNo { get; set; }
        public int? PageID { get; set; }
        public int? SiteID { get; set; }
        public bool allowPostCard { get; set; }
        public int? ProductID { get; set; }
        public int? IngredientID { get; set; }
        public int? TagID { get; set; }
        public int? PrescriptionTagID { get; set; }

        public string action { get; set; }
        public string url { get; set; }
        public string urlLessFile { get; set; }

        public string SavePath
        {
            get {
                return "~/Content/SiteResources/" + ObjectType.ToString() + "/" + ObjectID.ToString() + "/";
            }
        }

        public string getFileImage()
        {
            var img = "text-x-generic-icon.png";
            if (FileName.EndsWith(".pdf")) { img = "adobe_pdf.png"; }
            if (FileName.EndsWith(".zip")) { img = "mac_zip_icon.jpg"; }
            return img;
        }

        public string getUrl(SiteResourceVersion version, bool isRetina = false)
        {
            return urlLessFile + getFileNameVersion(version, isRetina);
        }

        public string getUrlOrFile(SiteResourceVersion version, bool isRetina = true)
        {
            var ext = Path.GetExtension(FileName).ToLower();
            if (ext == (".png") || ext == (".gif") || ext == (".jpeg") || ext == (".jpg"))
                return urlLessFile + getFileNameVersion(version, isRetina);
            else
                return "/content/images/" + getFileImage();
        }

        public string getFileNameVersion(SiteResourceVersion version, bool isRetina = false)
        {
            return getFileNameVersion(FileName,version,isRetina);
        }

        public static string getFileNameVersion(string fileName, SiteResourceVersion version, bool isRetina = false)
        {
            if (isRetina)
            {
                switch (version)
                {
                    case SiteResourceVersion.ITEM_PAGE_RETINA: return fileName;
                    case SiteResourceVersion.ITEM_LIST_RETINA: return fileName.Replace("@2x", "_list@2x");
                }
                return "version not found";
            }
            else
            {
                switch (version)
                {
                    case SiteResourceVersion.ITEM_PAGE: return fileName;
                    case SiteResourceVersion.ITEM_LIST: return fileName.Replace(".", "_list.");
                    case SiteResourceVersion.ITEM_SEARCH: return fileName.Replace(".", "_srh.");
                    case SiteResourceVersion.ITEM_LARGE: return fileName.Replace(".", "_lrg.");
                    case SiteResourceVersion.ITEM_FEED: return fileName.Replace(".", "_feed.");
                }
                return "version not found";
            }
        }

        public bool isPortrait()
        {
            if (width.HasValue)
            {
                return width < height;
            }
            return false;
        }

        public SiteResource clone(int ObjectID, SiteResourceOwner.ObjectTypeOption type, SiteResourceRepository _repo)
        {
            var sR = (SiteResource)this.MemberwiseClone();
            sR.ID = 0;
            sR.ObjectID = ObjectID;
            sR.ObjectType = type;
            _repo.saveSiteResource(sR);
            return sR;
        }
    }

    public class SiteResourceTicket : ISavedFile
    {
        public Guid Code { get; set; }
        public SiteResource.SiteResourceTypeOption SiteResourceType { get; set; }
        public string FileName { get; set; }

        public string SavePath
        {
            get
            {
                return "~/Content/SiteResources/Scratch/" + Code + "/";
            }
        }
    }

    public interface ISavedFile
    {
        string SavePath { get; }
        string FileName { get; set; }
        SiteResource.SiteResourceTypeOption SiteResourceType { get; set; }
    }

    public abstract class SiteResourceOwner
    {
        public enum ObjectTypeOption
        {
            PAGE, //0
            PRODUCT, //1
            TAG, //2
            INGREDIENT, //3
            PRESCRIPTIONTAG

        }

        public SiteResource[] siteResources { get; set; }

        public abstract int getObjectID();
        public abstract ObjectTypeOption getObjectType();

        public string getPostObjectName()
        {
            return this.GetType().Name.ToLower();
        }

        public string getSrUrl(SiteResource.SiteResourceTypeOption option)
        {
            if (siteResources.Any(p => p.SiteResourceType == option))
            {
                return siteResources.Where(p => p.SiteResourceType == option).FirstOrDefault().url;
            }
            return "not:found";
        }

        public bool hasSrUrl(SiteResource.SiteResourceTypeOption option, SiteResource.SiteResourceVersion version)
        {
            if (siteResources.Any(p => p.SiteResourceType == option))
            {
                var file = siteResources.Where(p => p.SiteResourceType == option).First();
                return true;
            }
            return false;
        }

        public string getSrUrl(SiteResource.SiteResourceTypeOption option, SiteResource.SiteResourceVersion version)
        {
            if (siteResources.Any(p => p.SiteResourceType == option))
            {
                var file = siteResources.Where(p => p.SiteResourceType == option).First();
                return file.urlLessFile + file.getFileNameVersion(version, false);
            }
            return "/content/images/" + (version != SiteResource.SiteResourceVersion.ITEM_SEARCH ? "no-image-large.png" : "no-image-small.png");
        }

        public bool getSrIsPortrait(SiteResource.SiteResourceTypeOption option, SiteResource.SiteResourceVersion version)
        {
            if (siteResources.Any(p => p.SiteResourceType == option))
            {
                var file = siteResources.Where(p => p.SiteResourceType == option).First();
                return file.isPortrait();
            }
            return true;
        }

        /*
        public static SiteResourceOwner getSimpleObject(SiteResource sr)
        {
            switch (sr.ObjectType)
            {
                case ObjectTypeOption.PAGE :
                    return new Page() { ID = sr.ObjectID };
                    
                case ObjectTypeOption.PRODUCT:
                    return new Product() { ID = sr.ObjectID };

                case ObjectTypeOption.INGREDIENT:
                    return new Ingredient() { ID = sr.ObjectID };
                    
            }
            return null;
        }
        */
    }
}
