﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kawa.Models
{
    public class Favourite
    {
        public int ID { get; set; }
        public int AccountID { get; set; }
        public int? ProductID { get; set; }
        public int? PageID { get; set; }
        public DateTime createDate { get; set; }
        public int Type { get; set; }

        public string Session { get; set; }

        public Product product { get; set; }
        public Page page { get; set; }
    }
}