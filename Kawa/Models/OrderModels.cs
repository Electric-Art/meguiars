﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;

namespace Kawa.Models
{
    public class Order
    {
        public enum SystemStatusOption
        {
            IN_PROGRESS = 0,
            AWAITING_PAYMENT = 1,
            PAID_ORDER = 2,
            CANCELLED = 3,
            SENT = 4,
            REFUNDED = 5
        }

        public int ID { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal OrderTotal { get; set; }
        public int? StatusID { get; set; }
        public SystemStatusOption SystemStatus { get; set; }
        public DateTime? ProcessDate { get; set; }
        public DateTime? CompleteDate { get; set; }
        public decimal BasketTotal { get; set; }
        public decimal FreightTotal { get; set; }
        public decimal TaxTotal { get; set; }
        public string Invoice { get; set; }
        public decimal? InvoiceTotal { get; set; }
        public int? idx { get; set; }
        public DateTime? AcknowledgeDate { get; set; }
        public DateTime? FeedbackRequestDate { get; set; }
        public DateTime? FeedbackResponseDate { get; set; }
        public string AdminNotes { get; set; }
        public Guid Code { get; set; }
        public string SessionKey { get; set; }
        public string AgentData { get; set; }
        public int? PromotionID { get; set; }
        public int? AccountID { get; set; }
        public int? ShippingMethodID { get; set; }
        public int? LocationID { get; set; }
        public int? CountryID { get; set; }
        public int? StateID { get; set; }
        public int? Postcode { get; set; }
        public string SystemNotes { get; set; }
        public PaymentServiceStore.PaymentTypeOption? PaymentType { get; set; }
        public string CustomerNotes { get; set; }
        public int? VoucherPromotionID { get; set; }
        public decimal? Weight { get; set; }
        public bool Printed { get; set; }
        public decimal? TotalBeforeSurcharge { get; set; }
        public decimal? AmexSurcharge { get; set; }
        public decimal? AdminAdHocFreightTotal { get; set; }
        public bool IsHidden { get; set; }
        public decimal? CubicWeight { get; set; }
        public decimal? FreightWouldHaveBeenTotal { get; set; }
        public decimal? FreightOriginalAmount { get; set; }
        public bool? IsAdminOrder { get; set; }

        public List<AccountDetail> Details { get; set; }
        public OrderPayment LastPayment { get; set; }
        public string VoucherCode { get; set; }
        public List<Basket> orderItems { get; set; }
        public List<OrderTax> taxes { get; set; }
        public List<Shipment> shipments { get; set; }

        public Country country { get; set; }
        public State state { get; set; }
        public ShippingMethod shippingMethod { get; set; }
        public List<ShippingMethod> allowedShippingMethods { get; set; }
        public List<Country> allowedCountries { get; set; }
        public List<State> allowedStates { get; set; }
        public ShippingComponent shippingComponent { get; set; }

        public Promotion promotion { get; set; }
        public string promotionText { get; set; }
        public decimal promotionDiscount { get; set; }
        public decimal totalAfterPromotion { get; set; }

        public Promotion voucher { get; set; }
        public string voucherText { get; set; }
        public decimal voucherDiscount { get; set; }
        public decimal totalAfterVoucher { get; set; }

        public decimal incGst { get; set; }
        public decimal taxedItemTotal { get; set; }

        public bool isBasket { get; set; }
        public bool isAdmin { get; set; }
        
        public Status Status { get; set; }
        public Status.StatusSetOption? StatusSet { get; set; }

        public string SearchOrderNo { get; set; }
        public string SearchCustomer { get; set; }
        public string SearchEmail { get; set; }

        public Data.Order Data { get; set; }

        public NewOrderNote NewNote { get; set; }
        public List<FlagLabel> FlagLabels { get; set; }
        
        public bool IsCalculationOrder { get; set; }
        public bool IsPrintingOrder { get; set; }

        public Order CloneForCalc()
        {
            var order = (Order)this.MemberwiseClone();
            order.IsCalculationOrder = true;
            order.orderItems = order.orderItems.Select(x => x.CloneForCalc()).ToList();
            return order;
        }

        public AccountDetail CustomerDetails
        {
            get
            {
                return Details.Where(d => d.Type == AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).SingleOrDefault();
            }
        }

        public AccountDetail DeliveryDetails
        {
            get
            {
                return Details.Where(d => d.Type == AccountDetail.DetailTypeOption.DELIVERY_DETAILS).SingleOrDefault();
            }
        }

        public string OrderNo
        {
            get
            {
                if (SearchOrderNo != null) return SearchOrderNo;
                if (ConfigurationManager.AppSettings["IsStaging"] != null && ConfigurationManager.AppSettings["IsStaging"] == "true")
                    return SystemStatus > SystemStatusOption.IN_PROGRESS ? "MGR-DEV-" + ID : "PRG-DEV-" + ID;
                return SystemStatus > SystemStatusOption.IN_PROGRESS ? "MGR-" + ID : "PRG-" + ID;
            }
        }

        public string OrderNoToBe
        {
            get
            {
                if (ConfigurationManager.AppSettings["IsStaging"] != null && ConfigurationManager.AppSettings["IsStaging"] == "true")
                    return "MGR-DEV-" + ID;
                return "MGR-" + ID;
            }
        }

        public string FinalOrderDate
        {
            get
            {
                return LastPayment != null ? LastPayment.PaymentDate.ToLocalTime().ToShortDateString() : OrderDate.ToLocalTime().ToShortDateString();
            }
        }

        public string OrderDateLocal
        {
            get
            {
                return OrderDate.UtcToLocal().ToShortDateString() + " " + OrderDate.UtcToLocal().ToShortTimeString();
            }
        }

        public void addNewNote(string note, bool saveToCustomer)
        {
            SystemNotes = note + "\n\n" + SystemNotes;
            if(saveToCustomer)
                CustomerNotes += note + "\n\n" + CustomerNotes;
        }

        public bool hasFreeShipping()
        {
            if (AdminAdHocFreightTotal.HasValue) return AdminAdHocFreightTotal == 0;
            return FreightTotal == 0;
            //return (promotion != null && promotion.freeShipping) || (voucher != null && voucher.freeShipping);
        }
        
        public string getFreeShippingText()
        {
            if (voucher != null && voucher.freeShipping && !voucher.splitTheDifference)
                return voucher.Description;
            if (promotion != null && promotion.freeShipping && !promotion.splitTheDifference)
                return promotion.Description;
            if (AdminAdHocFreightTotal.HasValue)
                return "Admin set $0 shipping";
            if (voucher != null && voucher.freeShipping)
                return voucher.Description;
            if (promotion != null && promotion.freeShipping)
                return promotion.Description;
            return null;
        }

        public bool hasSplitTheDifference()
        {
            if (AdminAdHocFreightTotal.HasValue) return false;

            return (promotion != null && promotion.splitTheDifference) || (voucher != null && voucher.splitTheDifference);
        }

        public decimal getSplitTheDifferenceCost()
        {
            /*
            if (promotion != null && promotion.freeShipping)
                return FreightTotal - promotionDiscount;
            if (voucher != null && voucher.freeShipping)
                return FreightTotal - voucherDiscount;
             * */
            return FreightTotal;
        }

        public string getSplitTheDifferenceText()
        {
            if (voucher != null && voucher.freeShipping)
                return getSplitTheDiffText(voucher.Description, voucher.splitTheDifferenceExplanation, voucherDiscount, (decimal)FreightWouldHaveBeenTotal);
            if (promotion != null && promotion.freeShipping)
                return getSplitTheDiffText(promotion.Description, promotion.splitTheDifferenceExplanation, promotionDiscount, (decimal)FreightWouldHaveBeenTotal);
            return null;
        }

        private string getSplitTheDiffText(string promoName, string splitExplanantion, decimal amountOff, decimal normalFreight)
        {
            return string.Format("{0} - {1} - Discounted shipping: {2} off the full shipping cost {3}.", 
                                promoName,
                                splitExplanantion,
                                amountOff.ToString("c"), 
                                normalFreight.ToString("c"));
        }

        public decimal getRealShippingAmount()
        {
            if (AdminAdHocFreightTotal.HasValue) return (decimal)AdminAdHocFreightTotal;
            if (hasSplitTheDifference()) return getSplitTheDifferenceCost();
            return FreightTotal;
        }

        public bool isReadyToSubmit()
        {
            return orderItems.Any() && 
                ShippingMethodID.HasValue && 
                !hasNationalOnlyViolation() &&
                !hasPreOrderViolation() &&
                !isAdmin;
        }

        public bool isReadyToAdminProceed()
        {
            return orderItems.Any() && 
                ShippingMethodID.HasValue && 
                !hasNationalOnlyViolation() &&
                !hasPreOrderViolation() &&
                isAdmin && 
                SystemStatus == SystemStatusOption.IN_PROGRESS;
        }

        public string getPaymentType()
        {
            if (!PaymentType.HasValue) return "n/a";
            switch(PaymentType)
            {
                case PaymentServiceStore.PaymentTypeOption.EWAY:
                    return "Credit Card";
                case PaymentServiceStore.PaymentTypeOption.PAYPAL:
                    return "Paypal";
                case PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT:
                    return "Direct Deposit";
            }
            return "n/a";
        }

        public ShippingStatus getBasketShippingStatus()
        {
            if(allowedShippingMethods.Any())
            {
                if (hasNationalOnlyViolation())
                {
                    return new ShippingStatus()
                    {
                        Allowed = false,
                        HasCartViolation = true,
                        Message = "You have products that only ship only in Australia in your cart.",
                        LongMessage = "You have products that only ship in Australia in your cart. Please remove them to ship overseas."
                    };
                }
                if (hasPreOrderViolation())
                {
                    return new ShippingStatus()
                    {
                        Allowed = false,
                        HasCartViolation = true,
                        Message = orderItems.All(x => x.Product.HasVariantPreOrder) ? "Only 1 pre-order item can be added to an order." :  "Orders with pre-order items cannot contain any other item.",
                        LongMessage = orderItems.All(x => x.Product.HasVariantPreOrder) ? "Only 1 pre-order item can be added to an order, please remove 1 or more pre-order items." : "Orders with pre-order items cannot contain any other item. Please remove either the pre-order item OR the other item(s)."
                    };
                }
                return new ShippingStatus() { Allowed=true };
            }
            else
            {
                if(CountryID.HasValue && CountryID == 1)
                {
                    if(StateID.HasValue)
                    {
                        if(Postcode.HasValue)
                            return new ShippingStatus()
                            {
                                Allowed = false,
                                Message = "Postcode not found in State, please try again.",
                                LongMessage = "Postcode not found in State, please try again."
                            };
                        else
                            return new ShippingStatus()
                            {
                                Allowed = false,
                                Message = "Please provide a postcode for shipping.",
                                LongMessage = "Please provide a delivery postcode in the Postcode text box."
                            };
                    }
                    else
                        return new ShippingStatus() { Allowed = false, 
                            Message = "Please select state for shipping.",
                            LongMessage = "Please select your delivery state from the dropdown menu."
                        };
                }
                else
                    return new ShippingStatus() { 
                        Allowed = false, 
                        Message = "Sorry we cannot ship to this location at the moment..",
                        LongMessage = "Sorry we cannot ship to this location at the moment..",
                    };
            }
        }

        public bool hasNationalOnlyViolation()
        {
            return orderItems.Any(x => x.Product.nationalOnly) && CountryID.HasValue && CountryID != 1;
        }

        public bool hasPreOrderViolation()
        {
            return (orderItems.Any(x => x.Product.HasVariantPreOrder) && orderItems.Count()>1);
        }
    }

    public class ShippingStatus
    {
        public bool Allowed {get;set;}
        public bool HasCartViolation { get; set; }
        public string Message {get;set;}
        public string LongMessage { get; set; }
    }

    public class Status
    {
        public enum StatusSetOption
        {
            IN_PROGRESS = 0,
            AWAITING_PAYMENT = 1,
            ACTIVE_ORDER = 2,
            SENT = 3
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public Order.SystemStatusOption? SetSystemStatus { get; set; }
        public string EmailSubject { get; set; }
        public string EmailHeader { get; set; }
        public string Color { get; set; }
        public bool isNotifyCustomer { get; set; }
        public bool isNotifyStaff { get; set; }
        public int InventoryEffect { get; set; }
        public bool isPayOrderAgain { get; set; }
        public int OrderNo { get; set; }
        public DateTime CreateDate { get; set; }
        public string Notes { get; set; }
        public StatusSetOption? StatusSet { get; set; }
        public int? RefactorToID { get; set; }
    }

    public class NewOrderNote
    {
        [DisplayName("Add a note")]
        [Required]
        public string Note { get; set; }
        [DisplayName("Customer can see")]
        public bool AddToCustomer { get; set; }
    }

    public class OrderPayment
    {
        public enum TypeOption
        {
            PAYMENT_SERVICE,
            VOUCHER
        }

        public int ID { get; set; }
        public int OrderID { get; set; }
        public int AccountDetailID { get; set; }
        public int PaymentStatus { get; set; }
        public string PaymentReference { get; set; }
        public string PaymentCode { get; set; }
        public DateTime PaymentDate { get; set; }
        public string FullResponse { get; set; }
        public string ResponseText { get; set; }
        public TypeOption Type { get; set; }
        public Kawa.Models.Services.PaymentServiceStore.PaymentTypeOption Service { get; set; }
    }

    public class OrderTax
    {
        public enum TaxTypeOption
        {
            GST,
            AMEX
        }

        public int ID { get; set; }
        public int OrderID { get; set; }
        public TaxTypeOption TaxType { get; set; }
        public decimal Amount { get; set; }

        public static decimal getRate(TaxTypeOption type)
        {
            switch (type)
            {
                case TaxTypeOption.GST: return 0.15m;
                case TaxTypeOption.AMEX: return 0.25m;
            }
            return 0;
        }
    }

    public class OrderSummary
    {
        public int BasketCount { get; set; }
        public decimal Total { get; set; }
    }

    public class AccountDetail : IAdminAccountViewModel
    {
        public enum DetailTypeOption
        {
            ACCOUNT_DETAILS,
            DELIVERY_DETAILS
        }

        public int ID { get; set; }
        public int OrderID { get; set; }
        [DisplayName("First Name")]
        [Required]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        [Required]
        public string Surname { get; set; }
        [DisplayName("Telephone")]
        public string Phone { get; set; }
        [DisplayName("Email Address")]
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        
        [DisplayName("Address")]
        [Required]
        public string Address1 { get; set; }
        [DisplayName("Address 2")]
        public string Address2 { get; set; }
        [DisplayName("Suburb/City")]
        [Required]
        public string Address3 { get; set; }
        [DisplayName("State")]
        public virtual string Address4 { get; set; }
        [DisplayName("Postcode")]
        [Required]
        public string Postcode { get; set; }
        [DisplayName("Country")]
        public int? CountryID { get; set; }

        public DetailTypeOption Type { get; set; }
        public bool isHidden { get; set; }
        public string GUID { get; set; }

        [DisplayName("Company name")]
        public string Company { get; set; }
        public string Country { get; set; }

        public Country _country { get; set; }

        [DisplayName("Subscribe to our newsletter.")]
        public bool isMailer { get; set; }
        [DisplayName("Phone")]
        [Required]
        [MinLength(8, ErrorMessage = "Phone must be at least 8 digits (include your area code)")]
        public string Mobile { get; set; }
        [DisplayName("Authority to leave parcel (optional – you can skip this step)")]
        public string DeliveryInstructions { get; set; }
        public bool? LeaveAuth { get; set; }

        public string Address4_Options { get; set; }

        public string aFirstName { get { return FirstName; } }
        public string aLastName { get { return Surname; } }
        public string aEmail { get { return Email; } }
        public int? AccountID { get; set; }
        public int? LastOrderID { get { return OrderID; } }


        public string _AddressLookupText { get; set; }

        public int getRank()
        {
            return 2;
        }
    }

    public class AccountDetailDelivery : AccountDetail
    {
        public const string LEAVE_NOT_SET = "Accept terms above to make selection";
        public const string LEAVE_SET_NO_OPTION_CHOOSEN = "Please select";
        public const string LEAVE_OTHER_OPTION = "Enter your own";
        public const string LEAVE_PREFIX = "If not home, leave parcel ";
        
        
        [DisplayName("State")]
        public override string Address4 { get; set; }

        public bool lockState { get; set; }
        public bool lockPostcode { get; set; }
        public string leaveOptionSelected;


        public string LeaveOption
        {
            get
            {
                if (string.IsNullOrEmpty(DeliveryInstructions)) return null;
                return getLeaveOptions().Any(x => x == DeliveryInstructions.Replace(LEAVE_PREFIX, "")) ? DeliveryInstructions.Replace(LEAVE_PREFIX, "") : "Enter your own";
            }
            set
            {
                leaveOptionSelected = value;
            }
        }

        public bool showInstructionsTextbox()
        {
            if (DeliveryInstructions == null) return false;
            return !getLeaveOptions().Any(x => x == DeliveryInstructions.Replace(LEAVE_PREFIX, ""));
        }

        public bool showSystemChangeMessage()
        {
            return !(string.IsNullOrEmpty(DeliveryInstructions) || LeaveAuth.HasValue);
        }

        public List<string> getLeaveOptions()
        {
            return new List<string>()
            {
                (string.IsNullOrEmpty(DeliveryInstructions) ? LEAVE_NOT_SET : LEAVE_SET_NO_OPTION_CHOOSEN),
                "At the front door",
                "On the front porch",
                "Any safe place on my premises",
                "Under carport / veranda",
                "In my letterbox (if size allows)",
                "By the side gate",
                LEAVE_OTHER_OPTION
            };
        }

        public bool isLeaveOptionOther()
        {
            return !string.IsNullOrEmpty(DeliveryInstructions) && !getLeaveOptions().Any(x => x == DeliveryInstructions);
        }
    }

    public class Basket
    {
        public int ID { get; set; }
        public int VariantID { get; set; }
        public decimal Price { get; set; }
        public decimal? Weight { get; set; }
        public string SessionKey { get; set; }
        public int? OrderID { get; set; }
        public int Quantity { get; set; }
        public decimal? ShipPrice { get; set; }
        public int? CountryID { get; set; }
        public string Notes { get; set; }
        public bool AdminFixed { get; set; }
        public DateTime CreateDate { get; set; }

        public bool IsCalculationBasket { get; set; }
        public Basket CloneForCalc()
        {
            var basket = (Basket)this.MemberwiseClone();
            basket.IsCalculationBasket = true;
            return basket;
        }

        public decimal LinePrice
        {
            get
            {
                return Price * Quantity;
            }
        }

        public decimal LineWeight
        {
            get
            {
                if (Variant == null) return 0;
                return (Variant.weight ?? 0) * Quantity;
            }
        }

        public decimal LineMaxQuantity
        {
            get
            {
                if (Variant == null) return 100;
                return Variant.stockNo.HasValue ? (int)Variant.stockNo : 100;
            }
        }

        public Product Product { get; set; }
        public Variant Variant { get; set; }

        public string Message { get; set; }
        public string VolumeSuggestion { get; set; }
        public string[] VolumeSuggestionColour { get; set; }
    }


    /// <summary>
    /// Summary description for Shipment.
    /// </summary>
    public class Shipment
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public int ShippingMethodID { get; set; }
        public int CarrierID { get; set; }
        public string TrackingNo { get; set; }
        public string Comment { get; set; }
        public DateTime createDate { get; set; }

        public ShippingMethod shippingMethod { get; set; }
        public Carrier carrier { get; set; }
    }

    #region Flags

    public class FlagAdminViewModel
    {
        public List<Flag> Flags { get; set; }
        public Flag Flag { get; set; }
        public FlagTrigger Search { get; set; }
    }

    /// <summary>
    /// Summary description for Flag.
    /// </summary>
    public class Flag
    {
        public enum LabelOption
        {
            VIP, 
            urgent_dispatch, 
            problem_customer, 
            fraud,
            ok_to_send,
            other,
            regular_customer
        }

        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        public int Type { get; set; }
        public string Labels { get; set; }
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }
        [DisplayName("Is Deactivated")]
        public bool isHidden { get; set; }
        public DateTime createDate { get; set; }

        public List<FlagLabel> GetLabels()
        {
            if (string.IsNullOrEmpty(Labels))
                return new List<FlagLabel>();
            return Labels.Split(',').ToList().Select(x => (Flag.LabelOption)Enum.Parse(typeof(Flag.LabelOption), x.Trim())).Select(x => new FlagLabel(ID,x)).ToList();
        }

        public bool HasLabel(LabelOption label)
        {
            if (string.IsNullOrEmpty(Labels))
                return false;
            return Labels.Split(',').ToList().Select(x => (Flag.LabelOption)Enum.Parse(typeof(Flag.LabelOption), x.Trim())).Any(x => x == label);
        }

        public List<FlagTrigger> triggers { get; set; }
    }

    public class FlagLabel
    {
        public FlagLabel(int FlagID, Flag.LabelOption label)
        {
            this.Link = "/admin/flags/flag/" + FlagID;
            this.Label = label;
            switch (label)
            {
                case Flag.LabelOption.VIP:
                    Name = "VIP";
                    ClassName = "label-vip";
                    break;
                case Flag.LabelOption.urgent_dispatch:
                    Name = "URGENT";
                    ClassName = "label-urgent_dispatch";
                    break;
                case Flag.LabelOption.problem_customer:
                    Name = "PROBLEM";
                    ClassName = "label-problem_customer";
                    break;
                case Flag.LabelOption.fraud:
                    Name = "FRAUD";
                    ClassName = "label-fraud";
                    break;
                case Flag.LabelOption.ok_to_send:
                    Name = "OK TO SEND";
                    ClassName = "label-ok_to_send";
                    break;
                case Flag.LabelOption.other:
                    Name = "OTHER";
                    ClassName = "label-other";
                    break;
                case Flag.LabelOption.regular_customer:
                    Name = "REGULAR";
                    ClassName = "label-regular_customer";
                    break;
            }
        }
        public string Link { get; set; }
        public string Name { get; set; }
        public string ClassName { get; set; }
        public Flag.LabelOption Label { get; set; }
    }

    /// <summary>
    /// Summary description for FlagTrigger.
    /// </summary>
    public class FlagTrigger
    {
        public int ID { get; set; }
        public int FlagID { get; set; }
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string Surname { get; set; }
        [DisplayName("Email Address")]
        public string Email { get; set; }
        [DisplayName("Address")]
        public string Address1 { get; set; }
        [DisplayName("Address 2")]
        public string Address2 { get; set; }
        [DisplayName("Suburb/City")]
        public string Address3 { get; set; }
        [DisplayName("State")]
        public string Address4 { get; set; }
        [DisplayName("Postcode")]
        public string Postcode { get; set; }
        [DisplayName("Country")]
        public int? CountryID { get; set; }

        public string CountryName { get; set; }
    }

    #endregion

    /// <summary>
	/// Summary description for Signature.
	/// </summary>
	public class Signature
    {
        public int ID { get; set; }
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Html { get; set; }
        public string Colour { get; set; }
        public int? OrderNo { get; set; }
    }
}

namespace Kawa.Models.Coms
{
    /// <summary>
    /// Summary description for Shipment.
    /// </summary>
    public class Order
    {
        public int ID { get; set; }
        public string OrderNo { get; set; }
        public Status Status { get; set; }
        public string PaymentType { get; set; }
        public int? PromotionID { get; set; }
        public int? VoucherPromotionID { get; set; }
        public string OrderDateLocal { get; set; }
        public decimal OrderTotal { get; set; }
        public string CustomerDetails_FirstName { get; set; }
        public string CustomerDetails_Surname { get; set; }
        public bool Printed { get; set; }
        public decimal? CubicWeight { get; set; }
        public List<FlagLabel> FlagLabels { get; set; }
    }

}


namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class LabelOrder
    {
        [DisplayName("C_CHARGE_CODE")]
        public string C_CHARGE_CODE { get ; set; }
        //[DisplayName("C_MERCHANT_CONSIGNEE_CODE")]
        //public string C_MERCHANT_CONSIGNEE_CODE { get ; set; }
        [DisplayName("C_CONSIGNEE_NAME")]
        public string C_CONSIGNEE_NAME { get ; set; }
        [DisplayName("C_CONSIGNEE_BUSINESS_NAME")]
        public string C_CONSIGNEE_BUSINESS_NAME { get ; set; }
        [DisplayName("C_CONSIGNEE_ADDRESS_1")]
        public string C_CONSIGNEE_ADDRESS_1 { get ; set; }
        [DisplayName("C_CONSIGNEE_ADDRESS_2")]
        public string C_CONSIGNEE_ADDRESS_2 { get ; set; }
        /*
        [DisplayName("C_CONSIGNEE_ADDRESS_3")]
        public string C_CONSIGNEE_ADDRESS_3 { get ; set; }
        [DisplayName("C_CONSIGNEE_ADDRESS_4")]
        public string C_CONSIGNEE_ADDRESS_4 { get ; set; }
         */
        [DisplayName("C_CONSIGNEE_SUBURB")]
        public string C_CONSIGNEE_SUBURB { get ; set; }
        [DisplayName("C_CONSIGNEE_STATE_CODE")]
        public string C_CONSIGNEE_STATE_CODE { get ; set; }
        [DisplayName("C_CONSIGNEE_POSTCODE")]
        public string C_CONSIGNEE_POSTCODE { get ; set; }
        [DisplayName("C_CONSIGNEE_COUNTRY_CODE")]
        public string C_CONSIGNEE_COUNTRY_CODE { get ; set; }
        [DisplayName("C_CONSIGNEE_PHONE_NUMBER")]
        public string C_CONSIGNEE_PHONE_NUMBER { get ; set; }
        [DisplayName("C_PHONE_PRINT_REQUIRED")]
        public string C_PHONE_PRINT_REQUIRED { get ; set; }
        //[DisplayName("C_CONSIGNEE_FAX_NUMBER")]
        //public string C_CONSIGNEE_FAX_NUMBER { get ; set; }
        [DisplayName("C_DELIVERY_INSTRUCTION")]
        public string C_DELIVERY_INSTRUCTION { get ; set; }
        [DisplayName("C_SIGNATURE_REQUIRED")]
        public string C_SIGNATURE_REQUIRED { get ; set; }
        /*
        [DisplayName("C_PART_DELIVERY")]
        public string C_PART_DELIVERY { get ; set; }
        [DisplayName("C_COMMENTS")]
        public string C_COMMENTS { get ; set; }
         */
        //[DisplayName("C_ADD_TO_ADDRESS_BOOK")]
        //public string C_ADD_TO_ADDRESS_BOOK { get ; set; }
        //[DisplayName("C_CTC_AMOUNT")]
        //public string C_CTC_AMOUNT { get ; set; }
        [DisplayName("C_REF")]
        public string C_REF { get ; set; }
        [DisplayName("C_REF_PRINT_REQUIRED")]
        public string C_REF_PRINT_REQUIRED { get ; set; }
        /*
        [DisplayName("C_REF2")]
        public string C_REF2 { get ; set; }
        [DisplayName("C_REF2_PRINT_REQUIRED")]
        public string C_REF2_PRINT_REQUIRED { get ; set; }
        [DisplayName("C_CHARGEBACK_ACCOUNT")]
        public string C_CHARGEBACK_ACCOUNT { get ; set; }
        [DisplayName("C_RECURRING_CONSIGNMENT")]
        public string C_RECURRING_CONSIGNMENT { get ; set; }
        [DisplayName("C_RETURN_NAME")]
        public string C_RETURN_NAME { get ; set; }
        [DisplayName("C_RETURN_ADDRESS_1")]
        public string C_RETURN_ADDRESS_1 { get ; set; }
        [DisplayName("C_RETURN_ADDRESS_2")]
        public string C_RETURN_ADDRESS_2 { get ; set; }
        [DisplayName("C_RETURN_ADDRESS_3")]
        public string C_RETURN_ADDRESS_3 { get ; set; }
        [DisplayName("C_RETURN_ADDRESS_4")]
        public string C_RETURN_ADDRESS_4 { get ; set; }
        [DisplayName("C_RETURN_SUBURB")]
        public string C_RETURN_SUBURB { get ; set; }
        [DisplayName("C_RETURN_STATE_CODE")]
        public string C_RETURN_STATE_CODE { get ; set; }
        [DisplayName("C_RETURN_POSTCODE")]
        public string C_RETURN_POSTCODE { get ; set; }
        [DisplayName("C_RETURN_COUNTRY_CODE")]
        public string C_RETURN_COUNTRY_CODE { get ; set; }
        [DisplayName("C_REDIR_COMPANY_NAME")]
        public string C_REDIR_COMPANY_NAME { get ; set; }
        [DisplayName("C_REDIR_NAME")]
        public string C_REDIR_NAME { get ; set; }
        [DisplayName("C_REDIR_ADDRESS_1")]
        public string C_REDIR_ADDRESS_1 { get ; set; }
        [DisplayName("C_REDIR_ADDRESS_2")]
        public string C_REDIR_ADDRESS_2 { get ; set; }
        [DisplayName("C_REDIR_ADDRESS_3")]
        public string C_REDIR_ADDRESS_3 { get ; set; }
        [DisplayName("C_REDIR_ADDRESS_4")]
        public string C_REDIR_ADDRESS_4 { get ; set; }
        [DisplayName("C_REDIR_SUBURB")]
        public string C_REDIR_SUBURB { get ; set; }
        [DisplayName("C_REDIR_STATE_CODE")]
        public string C_REDIR_STATE_CODE { get ; set; }
        [DisplayName("C_REDIR_POSTCODE")]
        public string C_REDIR_POSTCODE { get ; set; }
        [DisplayName("C_REDIR_COUNTRY_CODE")]
        public string C_REDIR_COUNTRY_CODE { get ; set; }
        */
        [DisplayName("C_CONSIGNEE_EMAIL")]
        public string C_CONSIGNEE_EMAIL { get ; set; }
        
        [DisplayName("C_EMAIL_NOTIFICATION")]
        public string C_EMAIL_NOTIFICATION { get ; set; }
        
        [DisplayName("A_ACTUAL_CUBIC_WEIGHT")]
        public string A_ACTUAL_CUBIC_WEIGHT { get ; set; }
        /*
        [DisplayName("A_LENGTH")]
        public string A_LENGTH { get ; set; }
        [DisplayName("A_WIDTH")]
        public string A_WIDTH { get ; set; }
        [DisplayName("A_HEIGHT")]
        public string A_HEIGHT { get ; set; }
        [DisplayName("A_NUMBER_IDENTICAL_ARTS")]
        public string A_NUMBER_IDENTICAL_ARTS { get ; set; }
        */
    }

    
}