﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Data
{
    public partial class PurchaseOrderItem
    {
        public enum StatusOption
        {
            Required, //0 
            Ordered, //1
            PreOrder, //2
            OOS, //3
            Discontinued, //4
            Received, //5
            Complete, //6
        }

        public StatusOption Status
        {
            get { return (StatusOption)StatusE; }
            set { StatusE = (int)value; }
        }

    }

    [MetadataType(typeof(Supplier_Validation))]
    public partial class Supplier
    {
        private List<Models.Tag> _Tags;
        public List<Models.Tag> tags
        {
            get
            {
                if (_Tags == null)
                    _Tags = Tag_Suppliers.Select(x => new Models.Tag() { ID = x.Tag.ID, name = x.Tag.name }).ToList();
                return _Tags;
            }
            set
            {
                _Tags = value;
            }
        }

        private List<SupplierContact> _Contacts;
        public List<SupplierContact> Contacts
        {
            get
            {
                if (_Contacts == null)
                    _Contacts = SupplierContacts.ToList();
                return _Contacts;
            }
            set
            {
                _Contacts = value;
            }
        }

    }

    public partial class Supplier_Validation
    {
        [Required]
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Terms { get; set; }
        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        [DataType(DataType.MultilineText)]
        [DisplayName("Special Deals")]
        public string SpecialDeals { get; set; }
    }

    public partial class SupplierContact
    {
        public bool IsTemplate { get; set; }
    }
}


namespace Kawa.Models.Coms
{
    public partial class PurchaseOrderItem
    {
        public int ID { get; set; }
        public int OrderID { get; set; }
        public string OrderIDDate { get; set; }

        public int? ProductID { get; set; }
        public string ProductIDName { get; set; }
        public string ProductIDSku { get; set; }
        public string ProductIDDaysToShip { get; set; }
        public string ProductIDUrl { get; set; }
        public string ProductName { get; set; }
        
        public int? SupplierID { get; set; }
        public string SupplierName { get; set; }

        public int? QuantityRequired { get; set; }
        public int? QuantityRecieved { get; set; }

        public Data.PurchaseOrderItem.StatusOption Status { get; set; }
        public string StatusName { get; set; }
        public string Notes { get; set; }

        public string OrderedDate { get; set; }
        public string OutOfStockUntilDate { get; set; }
        public string FinalisedDate { get; set; }

        public string SupplierInvNo { get; set; }
    }
}