﻿using System;
using System.Reflection;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FileHelpers;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Kawa.Models
{
    public interface IReport
    {
        bool HasDateRange { get; set; }

        bool HasProductHiddenFilter { get; set; }
        DateTime? Startdate { get; set; }
        DateTime? Enddate { get; set; }
        int? MaxRows { get; set; }

        [DisplayName("Exclude Hidden Products")]
        bool excludeHidden { get; set; }

        bool DownloadCsv { get; set; }
        string Name { get; }
        string UrlName { get; }
        string FullName { get; }
        List<FilterSet> Filters { get; set; }

        DataTable RunReportForView();
        ReportFile RunReportForCsv();
    }

    public abstract class ReportBase<T> : IReport
    {
        public bool HasDateRange { get; set; }
        public bool HasProductHiddenFilter { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public bool excludeHidden { get; set; }
        public int? MaxRows { get; set; }

        public bool DownloadCsv { get; set; }

        public abstract string Name { get; }
        
        protected dbDataContext _db;
        private List<FilterSet> _filters;
        
        public string UrlName 
        {
            get { return Name.ToSafeString(); } 
        }

        public string FullName
        {
            get { return Name + getDatePart(); }
        }

        public List<FilterSet> Filters
        {
            get
            {
                if (_filters == null)
                {
                    _filters = GetFilterSet();
                    _filters.ForEach(x => {
                        if(x.Options != null) 
                            x.Options.Insert(0, new FilterOption() { Name = x.Name });
                    });
                }
                return _filters;
            }
            set { _filters = value; }
        }

        public ReportBase()
        {
            _db = new dbDataContext();
            _db.CommandTimeout = 75;
            HasDateRange = true;
            
        }

        /*
        public abstract DataTable RunReportForView();
        public abstract ReportFile RunReportForCsv();
        */

        public abstract List<T> RunReport();
        public abstract List<FilterSet> GetFilterSet();

       

        public DataTable RunReportForView()
        {
            return CreateDataTable(RunReport());
        }
        public ReportFile RunReportForCsv()
        {
            return CreateCsv(RunReport());
        }

        public ReportFile CreateCsv(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();
            string HeaderLine = string.Empty;
            foreach (PropertyInfo info in properties)
            {
                DisplayNameAttribute attr = (DisplayNameAttribute)Attribute.GetCustomAttribute(info, typeof(DisplayNameAttribute));
                HeaderLine += attr.DisplayName + ",";
            }
            HeaderLine = HeaderLine.TrimEnd(',');

            FileHelperEngine engine = new FileHelperEngine(type);
            engine.HeaderText = HeaderLine;
            var FileStr = engine.WriteString((IEnumerable<object>)list);
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            var fileName = Name + getDatePart();
            
            fileName += ".csv";
            return new ReportFile(){
                Name = fileName,
                Data = encoding.GetBytes(FileStr)
            };
        }

        public DataTable CreateDataTable(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                DisplayNameAttribute attr = (DisplayNameAttribute)Attribute.GetCustomAttribute(info, typeof(DisplayNameAttribute));
                dataTable.Columns.Add(new DataColumn(attr.DisplayName, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

        private string getDatePart()
        {
            var fileName = string.Empty;
            if (Startdate == null && Enddate == null)
            {
                fileName += "all records";
            }
            else
            {
                fileName += (Startdate != null ? ((DateTime)Startdate).UtcToLocal().ToShortDateString() : "Beginning") + " till ";
                fileName += (Enddate != null ? ((DateTime)Enddate).UtcToLocal().AddDays(-1).ToShortDateString() : "End");
            }
            return " " + fileName;
        }

        public string clearForCsv(string val)
        {
            if (string.IsNullOrEmpty(val)) return val;
            return val.Replace("\n", " ").Replace(",", ".").Replace("\"", "'");
        }


    }

    public class FilterSet
    {
        public enum FilterSetUIOption
        {
            DropDown,
            Lookup
        }

        public string Name { get; set; }
        public List<FilterOption> Options { get; set; }
        public int? SelectedID { get; set; }

        public FilterSetUIOption FilterSetUI { get; set; }
        public string ObjectName { get; set; }
    }

    public class ReportFile
    {
        public string Name { get; set; }
        public byte[] Data { get; set; }
    }

    public class FilterOption
    {
        public int? ID { get; set; }
        public string Name { get; set; }
    }
}


namespace Kawa.Models.ViewModels
{
    public class ReportViewModel
    {
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public int? MaxRows { get; set; }
        public bool DownloadCsv { get; set; }
        public List<FilterSet> Filters { get; set; }

        public bool excludeHidden { get; set; }
    }

}
