﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Services;
using System.ComponentModel;
using PagedList;

namespace Kawa.Models.ViewModels
{
    public class AdminViewModel
    {
        public enum  ContentFilter {
            With_Meta,
            Without_Meta,
            With_Content,
            Without_Content
        }

        public List<Page> pages { get; set; }
        public Page page { get; set; }
        public List<Slot> slots { get; set; }
        public Slot slot { get; set; }

        public List<Product> products { get; set; }
        public Product product { get; set; }

        public List<Promotion> promotions { get; set; }
        public Promotion promotion { get; set; }

        public List<Symptom> symptoms { get; set; }
        public Symptom symptom { get; set; }

        public IPagedList<Order> orders { get; set; }
        public Order order { get; set; }
        public SiteViewModel svm { get; set; }

        public List<Person> persons { get; set; }
        public Person person { get; set; }

        public List<Tag> tags { get; set; }
        public Tag tag { get; set; }

        public List<PrescriptionTag> prescriptiontags { get; set; }
        public PrescriptionTag prescriptiontag { get; set; }

        public ContentFilter? filter { get; set; }
        public List<Tag> subtags { get; set; }
        public List<Tag> brands { get; set; }
        public Tag parenttag { get; set; }
        public Tag.TypeOption? tagtype { get; set; }
        public int? TagID { get; set; }
        
        public SiteResourceOwner srOwner { get; set; }
        public List<SiteResource.SiteResourceTypeOption> srTypes { get; set; }

        public bool success { get; set; }
        public string message { get; set; }

        public bool isPartial { get; set; }

        public List<Store> stores { get; set; }
        public Store store { get; set; }

        public string search;

        public List<Signup> signups { get; set; }
        public int PartialIndex { get; set; }

        public List<Ingredient> ingredients { get; set; }
        public Ingredient ingredient { get; set; }

        public int? skipTo { get; set; }
        public int? skipNo { get; set; }

        public Account account { get; set; }
        public IPagedList<Account> accounts { get; set; }

        public string formRoute { get; set; }
        public AdminPageRouteVariables formRouteVars { get; set; }
        public string pageTitle { get; set; }

        public bool? needsToken { get; set; }

        public List<SelectListItem> tagItems()
        {
            return tagItems(false);
        }

        public List<SelectListItem> tagItems(bool onlyParents)
        {
            var items = new List<SelectListItem>(){new SelectListItem(){Text="choose",Value=""}};
            items.AddRange(tags.Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
            return items;
        }


        public List<SelectListItem> productItems()
        {
            return productItems(false,"choose");
        }

        public List<SelectListItem> productItems(bool onlyParents, string choose)
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = choose, Value = "" } };
            items.AddRange(products.Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
            return items;
        }

        public List<SelectListItem> tagItems(string choose)
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = choose, Value = "" } };
            items.AddRange(tags.Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
            return items;
        }

        public List<SelectListItem> tagItems(string choose, Tag.TypeOption Type)
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = choose, Value = "" } };
            items.AddRange(tags.Where(x => x.Type == Type).Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
            return items;
        }

        public List<SelectListItem> variantItems()
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "choose", Value = "" } };
            if (product != null)
                items.AddRange(product.variants.Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
            return items;
        }

        public List<SelectListItem> relProductItems()
        {
            return relProductItems(false);
        }

        public List<SelectListItem> relProductItems(bool onlyParents)
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "choose", Value = "" } };
            items.AddRange(products.Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
            return items;
        }

        public List<SelectListItem> relPageItems()
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "choose", Value = "" } };
            items.AddRange(pages.Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
            return items;
        }

        public bool isFileOnlyPage()
        {
            return false;  //tag.SystemTag == Tag.SystemTagOption.FACT_SHEET;
        }

        public bool isNoMainTextOnlyPage()
        {
            return false; //tag.SystemTag == Tag.SystemTagOption.HEALTH_ELETTER;
        }

        public string getFiletypeDescription(SiteResource.SiteResourceTypeOption type)
        {
            string str = "";
            switch (type)
            {
                case SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE:
                    str = "Product Image";
                    break;
                case SiteResource.SiteResourceTypeOption.IN_CONTENT:
                    str = "Content Image";
                    break;
                case SiteResource.SiteResourceTypeOption.BILLBOARD:
                    str = "Billboard - Desktop";
                    break;
                case SiteResource.SiteResourceTypeOption.BILLBOARD_MOBILE:
                    str = "Billboard - Mobile Size";
                    break;
                case SiteResource.SiteResourceTypeOption.FILE_FOR_DOWNLOAD:
                    str = "File For Download";
                    break;
                case SiteResource.SiteResourceTypeOption.ARTICLE_FEATURED_WIDE:
                    str = "Home Page Slot";
                    break;
                case SiteResource.SiteResourceTypeOption.BRAND_LOGO:
                    str = "Brand Logo";
                    break;
                case SiteResource.SiteResourceTypeOption.XTRA_PRODUCT_IMAGE:
                    str = "Extra Product Image";
                    break;

            }
            if(str=="")
            {
                str = type.ToString().Replace("_"," " );
            }
            return str;
        }

        public List<SelectListItem> getSlotPageOptions(Slot.SpotOption slotOption)
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "choose", Value = "" } };

            switch(slotOption)
            {
                case Slot.SpotOption.HOME_PAGE_MODULE:
                    items.AddRange(pages.Where(p => p.siteResources.Any(s => s.SiteResourceType == SiteResource.SiteResourceTypeOption.ARTICLE_FEATURED_WIDE)).Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString(), Selected = slots.Any(s => s.Spot == slotOption && t.ID==s.PageID) }));
                    break;
                case Slot.SpotOption.SITE_WIDE_BANNER:
                    items.AddRange(pages.Where(p => !p.siteResources.Any()).Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString(), Selected = slots.Any(s => s.Spot == slotOption && t.ID == s.PageID) }));
                    break;
            }

            
            return items;
        }
    }

    public class OrderStatusUpdaterViewModel
    {
        [DisplayName("Order No to be Adjusted")]
        public int OrderToFix { get; set; }
        [DisplayName("Order No to use Status & Purchase method from")]
        public int OrderToCopy { get; set; }
    }
}
