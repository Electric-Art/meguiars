﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kawa.Models.Services;

namespace Kawa.Models.ViewModels
{
    public class OrderFilterViewModel
    {
        

        public string OrderNo { get; set; }
        public string Customer { get; set; }
        public string Email { get; set; }
        
        [DisplayName("Start Date")]
        public DateTime? Startdate { get; set; }
        [DisplayName("End Date")]
        public DateTime? Enddate { get; set; }
        [DisplayName("Total From")]
        public decimal? TotalFrom { get; set; }
        [DisplayName("Total To")]
        public decimal? TotalTo { get; set; }
        [DisplayName("Status")]
        public int? StatusID { get; set; }
        [DisplayName("Promotion")]
        public string VoucherCode { get; set; }
        [DisplayName("Country")]
        public int? CountryID { get; set; }
        [DisplayName("Gst")]
        public bool? isTaxExempt { get; set; }
        [DisplayName("Is Printed")]
        public bool? isPrinted { get; set; }
        [DisplayName("Payment Method")]
        public PaymentServiceStore.PaymentTypeOption? PaymentType { get; set; }
        [DisplayName("shipping methods")]
        public List<ShippingMethod> ShippingMethods { get; set; }
        public PostedShippingMethods PostedShippingMethods { get; set; }
        [DisplayName("products")]
        public string Products { get; set; }
        [DisplayName("Flag")]
        public Flag.LabelOption? FlagLabel { get; set; }

        public int? page { get; set; }
        public int? pageSize { get; set; }
        public Status.StatusSetOption statusSet { get; set; }

        public IPagedList<Order> orders { get; set; }
        public List<Status> setStatusList { get; set; }
        public List<Country> countryList { get; set; }
        public List<ShippingMethod> shippingMethodsList { get; set; }
        public List<Signature> signaturesList { get; set; }
        public List<Flag> flagList { get; set; }
        public List<int> pageSizes { get; set; }

        public List<Carrier> carriers { get; set; }
        public Shipment Shipment { get; set; }

        public int? SignatureID { get; set; }

        public Object getRouteVal(int page)
        {
            return new {
                Page = page,
                Startdate = Startdate, 
                Enddate = Enddate,
                TotalFrom = TotalFrom,
                TotalTo = TotalTo,
                StatusID = StatusID,
                VoucherCode = VoucherCode,
                CountryID = CountryID,
                isTaxExempt = isTaxExempt,
                isPrinted = isPrinted,
                PaymentType = PaymentType,
                PostedShippingMethods = PostedShippingMethods,
                Products = Products,
                FlagLabel = FlagLabel,
                PageSize = pageSize
            };
        }

        public List<SelectListItem> getStatusItems()
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "status", Value = "" } };
            items.AddRange(setStatusList.Select(t => new SelectListItem() { Text = t.Name, Value = t.ID.ToString() }));
            return items;
        }

        public List<SelectListItem> getTaxExemptItems()
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "tax status", Value = "" } };
            items.Add(new SelectListItem() { Text = "Has Gst", Value = "true" });
            items.Add(new SelectListItem() { Text = "No Gst", Value = "false" });
            return items;
        }

        public List<SelectListItem> getPrintedOptionItems()
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "print status", Value = "" } };
            items.Add(new SelectListItem() { Text = "Printed", Value = "true" });
            items.Add(new SelectListItem() { Text = "Not Printed", Value = "false" });
            return items;
        }
    }

    public class PostedShippingMethods
    {
        public int[] ShippingMethodIDs { get; set; }
    }

    public class PromotionFilterViewModel
    {
        public Promotion.TypeOption? PromotionType { get; set; }
        public Promotion.StatusOption? Status { get; set; }
        public string search { get; set; }
        public string searchSpecial { get; set; }

        public IPagedList<Promotion> promotions { get; set; }

        public Object getRouteVal(int page)
        {
            return new { page = page, PromotionType = PromotionType, search = search };
        }
    }
}