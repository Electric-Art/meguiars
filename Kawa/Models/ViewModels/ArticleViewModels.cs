﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kawa.Models.ViewModels
{
    public class ArticlePageViewModel
    {
        public Page page { get; set; }
        public bool isModal { get; set; }
    }
}