﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using 

namespace Kawa.Models.ViewModels
{
    public class ChoosePaymentViewModel
    {
        public Guid orderCode { get; set; }
        public List<PaymentChoiceViewModel> options { get; set; }
    }
}