﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models.Services;
using Kawa.Models.Extensions;

namespace Kawa.Models.ViewModels
{
    public class SiteViewModel
    {
        public enum CheckoutStage
        {
            CART,
            ACCOUNT_DETAILS,
            CHOOSE_PAYMENT,
            PAYMENT,
            COMPLETE
        }

        public bool isDevice { get; set; }
        public bool isTablet { get; set; }

        public List<Page> pages { get; set; }
        public Page page { get; set; }
        public Tag tag { get; set; }

        public PrescriptionTag prescriptiontag { get; set; }
        public List<PrescriptionTag> prescriptiontags { get; set; }

        public List<PrescriptionTag> prescriptionlistTags { get; set; }

        public Product product { get; set; }

        public Product DonationProduct { get; set; }
        public List<Slot> slots { get; set; }

        public Tag supertag { get; set; }
        
        public Page blogPage { get; set; }

        public Person person { get; set; }
        public SignupPerson signupPerson { get; set; }

        public bool signupPersonFinished { get; set; }

        public Contact contact { get; set; }
        public Signup signup { get; set; }

        public List<Page> homePages { get; set; }

        public List<Tag> productTags { get; set; }
        public List<Tag> listTags { get; set; }

        public List<Product> products { get; set; }

        //public LogOnModel LogOnModel { get; set; }

        public string search { get; set; }

        public List<Page> footerPages { get; set; }

        public SiteContentCollection sitedump { get; set; }

        public bool fullproduct { get; set; }

        public string getBillboardLinkAttribs(string link)
        {
            if (link.StartsWith("#"))
            {
                int top = 0;
                bool isProd = false;
                switch (link.TrimStart('#').ToLower())
                {
                    case "about" :
                        top = 819;
                        break;
                    case "products":
                        top = 411;
                        isProd = true;
                        break;
                    case "store":
                        top = 1285;
                        break;
                    case "ebook":
                        top = 2484;
                        break;
                    case "ask":
                        top = 1700;
                        break;
                    case "club":
                        top = 2083;
                        break;
                    case "quality":
                        top = 2884;
                        break;
                }
                return "href=\"javascript:void(0)\" onclick=\"jumpToHomePosition(" + top + "," + (isProd?"true":"false") + ");\"";
            }
            return "href=\"" + link + "\"";
        }

        public Basket newItem { get; set; }
        /*
        public List<Basket> basketItems { get; set; }
        public List<Basket> orderItems { get; set; }
        public decimal Subtotal { get; set; }
        public decimal PromotionDiscount { get; set; }
        public decimal Total { get; set; }
        public decimal IncGst { get; set; }
         */

        public Order order { get; set; }
        public bool isOrder { get; set; }
        public CheckoutStage checkoutStage { get; set; }
        public Guid ordercode { get; set; }
        public string promotionMessage { get; set; }
        /*
        public Promotion promotion { get; set; }
        public string promotiontext { get; set; }
        */
        public string section { get; set; }
        public string subSection { get; set; }
        public bool formSuccess { get; set; }
        public bool hasOrders { get; set; }

        public List<Order> orders { get; set; }

        public string savedCard { get; set; }
        public IPaymentViewModel pvm { get; set; }

        public Account account { get; set; }
        public CreditCardViewModel cc { get; set; }
        public AccountDetail CustomerDetails { get; set; }
        public AccountDetail DeliveryDetails { get; set; }
        public FullRegisterUpdateViewModel FullRegisterUpdate { get; set; }

        public string postCode { get; set; }
        public bool isModal { get; set; }

        public string getStageImg(CheckoutStage stage)
        {
            if (stage > checkoutStage)
                return "a";
            else if (stage == checkoutStage)
                return "b";
            else
                return "c";
        }

        public string sectionClass(string section)
        {
            return this.section == section ? "section-selected" : "";
        }

        public string subSectionClass(string subSection)
        {
            if (subSection == "Subscriptions")
                return "hidden";

            if (!hasOrders &&
                (subSection == "Orders" || subSection == "Subscriptions")
                )
                return "hidden";

            return this.subSection == subSection ? "active" : "";
        }

        public string getFooterUrl(Page.SystemPageOption SystemPage)
        {
            var page = footerPages.SingleOrDefault(p => p.SystemPage == SystemPage);
            if (page != null)
                return page.getUrl();
            else
                return "#";
        }

        public string getSystemUrl(string option)
        {
            Page.SystemPageOption e = (Page.SystemPageOption) Enum.Parse(typeof(Page.SystemPageOption), option);
            return Kawa.Models.Page.getUrlBySystemPage(e);
        }

        public bool isAboutHome()
        {
            return page.SystemPage == Page.SystemPageOption.ABOUT_US;
        }

        public bool isAboutSubSection(string option)
        {
            return page.SystemPage == (Page.SystemPageOption)Enum.Parse(typeof(Page.SystemPageOption), option);
        }

        public string isAboutSubSectionClass()
        {
            return Kawa.Models.Page.getClassBySystemPage((Page.SystemPageOption)page.SystemPage);
        }

        public SelectList getSubjects()
        {
            return contact.subject.ToSelectList();
        }

        public Page getSlotPage(Models.Slot.SpotOption Spot)
        {
            var slot = slots.SingleOrDefault(s => s.Spot == Spot);
            if (slot == null) return null;
            //return pages.SingleOrDefault(p => p.ID == slot.PageID);
            return slot.page;
        }

        public List<Tag> getArticleTags(int colIndex)
        {
            var nonSys = productTags.Where(t => !t.SystemTag.HasValue && t.pages.Count>0).ToList();
            var startNo = nonSys.Count() / 2;
            var endNo = nonSys.Count() - nonSys.Count() / 2;
            if (colIndex == 0)
                return nonSys.Take(startNo).ToList();
            else
                return nonSys.GetRange(startNo, endNo).ToList();
        }
    }

    public class SiteContentCollection
    {
        public List<Tag> tags { get; set; }
        public List<Product> products { get; set; }
    }
}