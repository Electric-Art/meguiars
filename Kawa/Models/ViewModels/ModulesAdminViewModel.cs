﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kawa.Models;
using Kawa.Models.Services;
using PagedList;


namespace Kawa.Models.ViewModels
{
    public class ModulesAdminViewModel
    {
        public List<Page> modules { get; set; }
        public List<Page> youtubemodules { get; set; }
        public List<Slot> slots1 { get; set; }
        public List<Slot> slots2 { get; set; }
        public List<Slot> slots3 { get; set; }
        public Slot slot { get; set; }

        public List<SelectListItem> getSlotPageOptions(Slot.SpotOption slotOption)
        {
            var items = new List<SelectListItem>() { new SelectListItem() { Text = "choose", Value = "" } };

            switch (slotOption)
            {
                case Slot.SpotOption.VIDEO_MODULE:
                    items.AddRange(youtubemodules.Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
                    break;
                case Slot.SpotOption.HOME_PAGE_MODULE:
                    items.AddRange(modules.Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
                    break;
                case Slot.SpotOption.SITE_WIDE_BANNER:
                case Slot.SpotOption.SITE_WIDE_SIGNUP:
                    items.AddRange(modules.Where(p => !p.siteResources.Any()).Select(t => new SelectListItem() { Text = t.name, Value = t.ID.ToString() }));
                    break;
            }


            return items;
        }
    }
}