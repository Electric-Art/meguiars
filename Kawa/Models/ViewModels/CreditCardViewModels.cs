﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.ViewModels
{
    public class CreditCardViewModel
    {
        [Required]
        [CreditCard]
        [DisplayName("Card Number")]
        public long? EWAY_CARDNUMBER { get; set; }

        [Required]
        [DisplayName("Name on the Card")]
        public string EWAY_CARDNAME { get; set; }

        [Required]
        [CreditCardMonth]
        [CreditCardDate]
        [DisplayName("Expiry Month")]
        public string EWAY_CARDEXPIRYMONTH { get; set; }

        [Required]
        [CreditCardYear]
        [CreditCardDate]
        [DisplayName("Expiry Year")]
        public string EWAY_CARDEXPIRYYEAR { get; set; }

        [Required]
        [DisplayName("CVN")]
        public string EWAY_CARDCVN { get; set; }

    }

    public class CreditCardMonthAttribute : ValidationAttribute, IClientValidatable
    {
	    //http://www.codeproject.com/Articles/613330/Building-Client-JavaScript-Custom-Validation-in-AS
        
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int month;
            bool res = int.TryParse(value.ToString(), out month);
            if (!res)
            {
                return new ValidationResult(validationContext.DisplayName + "must be a number representing a month");
            }
            else
            {
                return month > 0 && month < 13?
                    ValidationResult.Success :
                    new ValidationResult(validationContext.DisplayName + "must be a number between 1 and 12");
            }
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ValidationType = "creditcardmonth",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName)
            };
            //modelClientValidationRule.ValidationParameters.Add("boolprop", BooleanPropertyName);
            yield return modelClientValidationRule;
        }
    }

    public class CreditCardYearAttribute : ValidationAttribute, IClientValidatable
    {
        //http://www.codeproject.com/Articles/613330/Building-Client-JavaScript-Custom-Validation-in-AS

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int year;
            bool res = int.TryParse(value.ToString(), out year);
            if (!res)
            {
                return new ValidationResult(validationContext.DisplayName + " must be a number representing a month");
            }
            else
            {
                //TODO Match current year
                return year >= 15 ?
                    ValidationResult.Success :
                    new ValidationResult(validationContext.DisplayName + " must be this year or later");
            }
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ValidationType = "creditcardyear",
                ErrorMessage = FormatErrorMessage(metadata.DisplayName)
            };
            //modelClientValidationRule.ValidationParameters.Add("boolprop", BooleanPropertyName);
            yield return modelClientValidationRule;
        }
    }

    public class CreditCardDateAttribute : ValidationAttribute, IClientValidatable
    {
        //http://www.codeproject.com/Articles/613330/Building-Client-JavaScript-Custom-Validation-in-AS

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int year = GetValue<int>(validationContext.ObjectInstance, "EWAY_CARDEXPIRYYEAR");
            int month = GetValue<int>(validationContext.ObjectInstance, "EWAY_CARDEXPIRYMONTH");
            DateTime date;

            if (DateTime.TryParse("20" + year + "-" + month + "-01",out date))
            {
                return date >= DateTime.Now.ToLocalTime().AddMonths(-1) ?
                    ValidationResult.Success :
                    new ValidationResult("This card has expired");
            }
            else
            {
                //TODO Match current year
                return year >= 15 ?
                    ValidationResult.Success :
                    new ValidationResult("This expiry date is invalid");
            }
        }

        private static T GetValue<T>(object objectInstance, string propertyName)
        {
            if (objectInstance == null) throw new ArgumentNullException("objectInstance");
            if (string.IsNullOrWhiteSpace(propertyName))
                throw new ArgumentNullException("propertyName");

            var propertyInfo = objectInstance.GetType().GetProperty(propertyName);
            return (T)propertyInfo.GetValue(objectInstance);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var modelClientValidationRule = new ModelClientValidationRule
            {
                ValidationType = "creditcarddate",
                ErrorMessage = "Expiry date is invalid"
            };
            //modelClientValidationRule.ValidationParameters.Add("boolprop", BooleanPropertyName);
            yield return modelClientValidationRule;
        }
    }
}