﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kawa.Models.ViewModels
{
    public class AccountDetailsViewModel
    {
        public AccountDetail CustomerDetails { get; set; }
        public AccountDetailDelivery DeliveryDetails { get; set; }
    }

    public class CustomerDetailsViewModel
    {
        public AccountDetail CustomerDetails { get; set; }
        public FullRegisterUpdateViewModel FullRegisterUpdate { get; set; }
    }

    public class AccountViewModel
    {
        public LoginViewModel Login { get; set; }
        public RegisterViewModel Register { get; set; }

        public Guid OrderCode { get; set; }
        public Favourite favourite { get; set; }
    }

    public class OrderChooseAccountViewModel
    {
        public int OrderID { get; set; }
        public int? AccountID { get; set; }
        public int? LastOrderID { get; set; }
        public Order Order { get; set; }
        public AccountFilterViewModels FilterModel { get; set; }
    }
}