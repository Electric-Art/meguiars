﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;

namespace Kawa.Models.ViewModels
{
    public class ProductFilterViewModels
    {
        public enum StockNoStatus
        {
            Zero = 0,
            More_Than_Zero = 1,
            All = 2
        }

        public enum QuantityRestrictionOption
        {
            Min = 0,
            Max = 1
        }

        public int pageSize { get; set; }
        public bool csv { get; set; }
        public int? TagID { get; set; }
        public int? BrandID { get; set; }
        public string search { get; set; }
        public string searchKeyword { get; set; }
        public bool? outOfStock { get; set; }
        public bool? preOrder { get; set; }
        public bool? offline { get; set; }
        public bool? googleCatStatus { get; set; }
        public bool? bingCatStatus { get; set; }
        public bool? hideBlurb { get; set; }
        public bool? austL { get; set; }
        public int? DisclaimerID { get; set; }
        public StockNoStatus? stockNoStatus { get; set; }
        public QuantityRestrictionOption? quanitytRestriction { get; set; }

        public List<Tag> tags { get; set; }
        public List<Tag> brands { get; set; }
        public IPagedList<Product> products { get; set; }
        public List<Page> disclaimers { get; set; }

        public Object getRouteVal(int page)
        {
            return new { TagID = TagID, Page = page };
        }
    }

    public class PurchaseOrderItemFilterViewModel
    {
        public enum StatusSetOption
        {
            IN_PROGRESS = 0,
            COMPLETE = 1
        }

        public int pageSize { get; set; }
        public int? OrderNo { get; set; }
        public int? BrandID { get; set; }
        public int? SupplierID { get; set; }
        public int? TagID { get; set; }
        public string ProductName { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public Data.PurchaseOrderItem.StatusOption? Status { get; set; }
        public StatusSetOption statusSet { get; set; }
        public string SupplierInvNo { get; set; }

        public List<Tag> tags { get; set; }
        public List<Tag> brands { get; set; }
        public List<Data.Supplier> suppliers { get; set; }
        public IPagedList<Data.PurchaseOrderItem> purchaseOrderItems { get; set; }

        public object getRouteVal(int page)
        {
            return new { TagID, OrderNo, ProductName, Startdate, Enddate, Page = page };
        }
    }

    public class BulkDiscountViewModel
    {
        public List<int> variantIds { get; set; }
        public BulkDiscount single { get; set; }
        public List<BulkDiscount> volumePrices { get; set; }
        public bool deleteExisting { get; set; }
        public string bulkProductListName { get; set; }
    }

    public class BulkDiscount
    {
        public int quantity { get; set; }
        public decimal discount { get; set; }
    }

    public class BulkGoogleCatViewModel
    {
        public List<int> productIds { get; set; }
        public string googleCat { get; set; }
    }

    public class BulkBingCatViewModel
    {
        public List<int> productIds { get; set; }
        public string bingCat { get; set; }
    }

    public class BulkShipDaysViewModel
    {
        public List<int> productIds { get; set; }
        public int? shipDays { get; set; }
        public int? shipDaysMinimum { get; set; }
    }

    public class BulkHideBlurbViewModel
    {
        public List<int> productIds { get; set; }
        public bool hideBlurb { get; set; }
    }

    public class BulkDisclaimersViewModel
    {
        public List<int> productIds { get; set; }
        public List<int> pageIds { get; set; }
    }

    public class BulkOutofstockViewModel
    {
        public List<int> productIds { get; set; }
        public bool outOfStock { get; set; }
    }
}