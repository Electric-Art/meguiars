﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kawa.Models
{
    public class MenuViewModel
    {
        public OrderSummary OrderSummary {get; set;}
        public List<Tag> MainProductTags { get; set; }
        public List<Tag> Brands { get; set; }
        public List<Tag> Categories { get; set; }
        public Tag ProductCategoriesTag { get; set; }
        public Page ShippingBanner1 { get; set; }
        public Page Catalogs { get; set; }
        public Page ShippingBanner2 { get; set; }
        public List<ISearchResult> TrendingSearches;
    }
}