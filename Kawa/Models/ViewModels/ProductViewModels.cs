﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Mvc;

namespace Kawa.Models.ViewModels
{
    public class SearchResultsViewModel
    {
        public IPagedList<Product> Products { get; set; }
        public Tag Tag { get; set; }
        public Tag Brand { get; set; }
        public List<Models.Page> pages { get; set; }
        public List<Models.Data.New> news { get; set; }
        public List<Models.Data.LmcGallery> galleries { get; set; }
        public ResultFilterViewModel filter { get; set; }

        public string getPagesUrl(int page, UrlHelper Url)
        {
            return Url.Action("Index", new {Page=page}) + getRouteVal();
        }

        public string getRouteVal()
        {
            return (Products.PageSize > 25 ? "&pagesize=" + Products.PageSize : null) + getRouteValOfSearch();
        }

        public string getRouteValOfSearch()
        {
            if (filter.postedResultFilters == null)
                return null;
            return "&" +
                (filter.postedResultFilters.PriceIDs != null ? "PriceIDs=" + string.Join(",", filter.postedResultFilters.PriceIDs.Select(n => n.ToString()).ToArray()) + "&" : "")
                +
                (filter.postedResultFilters.BrandIDs != null ? "BrandIDs=" + string.Join(",", filter.postedResultFilters.BrandIDs.Select(n => n.ToString()).ToArray()) + "&" : "")
                +
                (filter.postedResultFilters.SpecialtiesIDs != null ? "SpecialtiesIDs=" + string.Join(",", filter.postedResultFilters.SpecialtiesIDs.Select(n => n.ToString()).ToArray()) + "&" : "")
                + 
                (!string.IsNullOrEmpty(filter.postedResultFilters.search)?"search=" + HttpContext.Current.Server.UrlEncode(filter.postedResultFilters.search):"");
            
        }
    }

    public class ProductImageViewModel
    {
        public ProductImageViewModel(Product Product, SiteResource.SiteResourceVersion Version)
        {
            this.Product = Product;
            this.Version = Version;
        }

        public Product Product { get; set; }
        public SiteResource.SiteResourceVersion Version { get; set; }
    }

    public class CategoriesViewModel
    {
        public Tag Tag { get; set; }
        public List<Tag> TagBrands { get; set; }
        public List<Tag> Tags { get; set; }
        public string NameOfSet { get; set; }

        public bool IsLetter { get; set; }
        public bool IsFromBrands { get; set; }
    }

    
}