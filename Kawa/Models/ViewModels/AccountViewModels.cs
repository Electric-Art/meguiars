﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Linq;
using PagedList;
using System.ComponentModel;

namespace Kawa.Models.ViewModels
{
    public class AccountFilterViewModels
    {
        public int pageSize { get; set; }
        public string searchFirstName { get; set; }
        public string searchLastName { get; set; }
        public string searchEmail { get; set; }

        public bool last50 { get; set; }

        public IPagedList<IAdminAccountViewModel> accounts { get; set; }

        public Object getRouteVal(int page)
        {
            return new { searchFirstName = searchFirstName, searchLastName = searchLastName, searchEmail = searchEmail };
        }
    }

    public class AccountAdminViewModel
    {
        public Account Account { get; set; }
        public List<Order> Orders { get; set; }
    }

    public class SpecialViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public bool js { get; set; }
        public int? ProductID { get; set; }
        public int? PageID { get; set; }
    }

    public class RegisterViewModel : FullRegisterBaseViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Gender")]
        public Account.GenderOption? Gender { get; set; }

        public int? DateOfBirthDay { get; set; }
        public int? DateOfBirthMonth { get; set; }
        public int? DateOfBirthYear { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "How did you hear about us?")]
        public string Channel { get; set; }

        [Display(Name = "Invite Code (optional)")]
        public string VoucherCode { get; set; }


        public bool js { get; set; }
        public bool fullregister { get; set; }
        public int? ProductID { get; set; }
        public int? PageID { get; set; }

        [DisplayName("Subscribe to The Enthusiast newsletter, to receive info on product launches, tips, competitions and specials!")]
        public bool isMailer { get; set; }


    }

    public class FullRegisterUpdateViewModel : FullRegisterBaseViewModel
    {
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Gender")]
        public Account.GenderOption? Gender { get; set; }

        public int? DateOfBirthDay { get; set; }
        public int? DateOfBirthMonth { get; set; }
        public int? DateOfBirthYear { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime? DateOfBirth { get; set; }

        [Display(Name = "How did you hear about Meguiars?")]
        public string Channel { get; set; }
    }

    public class FullRegisterBaseViewModel
    {
        public System.Web.Mvc.SelectList getChannelSelectItems(string modelValue)
        {
            var chooseVal = new List<Object>();
            chooseVal.Add(new { Value = "", Text = "choose one" });
            var list = getChannelOptions().Select(i => new System.Web.Mvc.SelectListItem() { Text = i, Value = i });

            return new System.Web.Mvc.SelectList(chooseVal.Union(list), "Value", "Text", modelValue);
        }

        public List<string> getChannelOptions()
        {
            return new List<string>()
            {
                "Online",
                "Friends",
                "Family",
                "Advertising - Radio",
                "Advertising - Print",
                "Advertising - Other",
                "Other"
            };
        }
    }

    public class TradeLoginViewModel
    {
        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
