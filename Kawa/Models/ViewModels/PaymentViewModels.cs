﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models.Services;

namespace Kawa.Models.ViewModels
{
    #region Services

    public class PaymentChoiceViewModel
    {
        public string name { get; set; }
        public string description { get; set; }
        public PaymentServiceStore.PaymentTypeOption type { get; set; }
        public bool hasView { get; set; }
        public string viewName { get; set; }
        public Order order { get; set; }
    }

    public interface IPaymentViewModel
    {
        bool hasView { get; }
        
        string viewName { get; }
        string instructionText { get; }
        string processingText { get; }

        string link { get; set; }
    }

    public class PaymentResultModel
    {
        public PaymentServiceStore.PaymentTypeOption type { get; set; }
        public string responseCode { get; set; }
        public string authcode { get; set; }
        public string fullResponse { get; set; }
        public bool success { get; set; }
        public string token { get; set; }
        public string injectView { get; set; }
        public string injectHeader { get; set; }
    }

    #endregion

    #region Pages

    public class PaymentCompleteViewModel
    {
        public Order order { get; set; }
        public PaymentServiceStore.PaymentTypeOption type { get; set; }
        public PaymentResultModel result { get; set; }
    }

    public class PaymentFailedViewModel
    {
        public Order order { get; set; }
        public PaymentServiceStore.PaymentTypeOption type { get; set; }
    }

    #endregion
}