﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Kawa.Models.ViewModels
{
    public class DbBackupViewModel
    {
        public bool success { get; set; }
        public string link { get; set; }
        public string message { get; set; }

        public string FileName
        {
            get
            {
                return Path.GetFileName(new Uri(link).LocalPath);
            }
        }
    }
}