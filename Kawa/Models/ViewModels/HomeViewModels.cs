﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kawa.Models.ViewModels
{
    public class HomeViewModels
    {
        public Page Metadesciption { get; set; }
        public Page BillboardPage { get; set; }
        public Page BrandPage { get; set; }
        public Page RetractionPage { get; set; }
        public List<Slot> Slots { get; set; }

        public List<Slot> VideoSlots { get; set; }
        public List<Tag> CatMenuTags { get; set; }
        public List<ProductSliderViewModel> Sliders { get; set; }
    }

    public class ProductSliderViewModel
    {
        public Tag MainTag { get; set; }
        public List<Tag> Tags { get; set; }
        public List<Product> Products { get; set; }
        public bool HideSubcats { get; set; }
    }
}