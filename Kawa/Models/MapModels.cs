﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models
{
    public class MapModel
    {
        public double nelat { get; set; }
        public double nelng { get; set; }
        public double swlat { get; set; }
        public double swlng { get; set; }
        public string state { get; set; }
    }

    /// <summary>
    /// Summary description for Store.
    /// </summary>
    public class Store
    {
        public int ID { get; set; }
        [DisplayName("Store Name")]
        [Required]
        public string name { get; set; }
        [DisplayName("Shop Position")]
        public string shopPosition { get; set; }
        [DisplayName("Street Address")]
        public string streetAddress { get; set; }
        [DisplayName("Suburb")]
        public string suburb { get; set; }
        [DisplayName("State")]
        public string state { get; set; }
        [DisplayName("Phone")]
        public string phone { get; set; }
        [DisplayName("Website")]
        public string website { get; set; }
        [DisplayName("Store Website")]
        public string storeWebsite { get; set; }
        [DisplayName("Post Code")]
        public int? postCode { get; set; }
        [DisplayName("Description")]
        public string description { get; set; }
        [DisplayName("Contact Details")]
        public string contactDetails { get; set; }
        public int Type { get; set; }
        public int? geocodeRun { get; set; }
        [DisplayName("Latitude")]
        public double? lat { get; set; }
        [DisplayName("Longitude")]
        public double? lng { get; set; }
    }
}