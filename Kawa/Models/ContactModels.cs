﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kawa.Models.Services;
//using System.Web.Mvc;

namespace Kawa.Models
{
    public class Contact
    {
        public enum SubjectOption
        {
            General_enquiry, 
            Online_order_enquiry, 
            Product_information, 
            Feedback, 
            Accounts,
            Find_a_stockist 
        }

        [DisplayName("First Name")]
        [Required]
        public string firstName { get; set; }
        [DisplayName("Surname")]
        [Required]
        public string surname { get; set; }
        [DisplayName("Title")]
        public string title { get; set; }
        [DisplayName("Email Address")]
        [EmailAddress]
        [Required]
        public string email { get; set; }

      
        [DisplayName("Phone")]
        [Required]
        [MinLength(10, ErrorMessage = "Phone must be at least 10 digits (include your area code)")]
        public string phone { get; set; }
        [DisplayName("Mobile")]
        public string mobile { get; set; }
        [DisplayName("Subject")]
        public SubjectOption subject { get; set; }
        [DisplayName("Enquiry Details")]
        public string enquiry { get; set; }
 
    }
}