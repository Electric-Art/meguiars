﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Kawa.Models.Services;

namespace Kawa.Models
{
    public class Promotion
    {
        public enum TypeOption
        {
            PROMOTION,
            ONE_USE_VOUCHER,
            REUSABLE_VOUCHER
        }

        public enum StatusOption
        {
            AVAILABLE_FOR_USE,
            USED,
            INVALIDATED
        }

        public enum SystemTypeOption
        {
            MAILER_SPECIAL,
            FREE_SHIPPING_PRODUCT_IN_CART,
            PROPELLAPROMO
        }

        public string Description
        {
            get
            {
                return (name ?? code);
            }
        }

        public string AdminDescription
        {
            get
            {
                return Description + "(" + ID + ")";
            }
        }

        public string Effect
        {
            get
            {
                if (percentOff.HasValue)
                    return percentOff + "% Off";
                else
                    if (amountOff.HasValue)
                        return "$" + amountOff + " Off";
                    else
                        if (freeShipping)
                            return "Free Shipping";
                        else
                            return "No Effect Set";
            }
        }

        public int ID { get; set; }
        [DisplayName("Name")]
        public string name { get; set; }
        [DisplayName("Code")]
        public string code { get; set; }
        [DisplayName("Product")]
		public int? ProductID { get; set; }
        [DisplayName("Variant")]
		public int? VariantID { get; set; }
        public TypeOption Type { get; set; }
        [DisplayName("Percent Off")]
		public double? percentOff { get; set; }
        [DisplayName("Amount Off")]
		public decimal? amountOff { get; set; }
        public StatusOption Status { get; set; }
		public DateTime createDate { get; set; }
		public DateTime? usedDate { get; set; }
		public int? OrderID { get; set; }
        [DisplayName("Is Deactivated")]
		public bool isHidden { get; set; }
        public string SessionKey { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DisplayName("Expiry Date")]
        public DateTime? expiryDate { get; set; }
        [DisplayName("Category")]
        public int? TagID { get; set; }
        public string notes { get; set; }
        public SystemTypeOption? SystemType { get; set; }
        [DisplayName("Minimum Spend")]
        public decimal? minimumSpend { get; set; }
        [DisplayName("Maximum Weight")]
        public decimal? maximumWeight { get; set; }

        /*
        [DisplayName("Only in Location")]
        public int? LocationID { get; set; }
        */

        [DisplayName("Free Shipping")]
        public bool freeShipping { get; set; }
        [DisplayName("Priority")]
        public int? priority { get; set; }
        [DisplayName("Shipping Method")]
        public int? ShippingMethodID { get; set; }

        public bool clearThisPromotion { get; set; }
        public bool splitTheDifference { get; set; }
        public string splitTheDifferenceExplanation { get; set; }
        public bool successfulTestAgainstOrder { get; set; }
        
        [DisplayName("Category")]
        public int? CategoryID { get; set; }
        [DisplayName("Brand")]
        public int? BrandID { get; set; }

        public Product product { get; set; }
        public Tag tag { get; set; }
        public bool hasLocation { get; set; }
        public List<Location> locations { get; set; }
        [DisplayName("Free product SKU")]
        public string SKU { get; set; }

        public bool locationAllowed(int LocationID)
        {
            return locations.Any(x => x.ID == LocationID);
        }

        public bool locationTheSame(List<Location> compareLocations)
        {
            return locations.Count() == compareLocations.Count() && locations.All(x => compareLocations.Select(y => y.ID).Contains(x.ID));
        }

        public PromotionEffect getDiscount(Order order)
        {
            List<Basket> items = order.orderItems;
            decimal inPrice = order.BasketTotal;

            if(freeShipping)
                return new PromotionEffect()
                {
                    EffectedTotalType = PromotionEffect.EffectedTotalOption.FREE_SHIPPING,
                    amountOff = order.FreightTotal,
                    amountOffTaxed = order.FreightTotal,
                    message = Description
                };


            PromotionEffect effect = new PromotionEffect()
            {
                EffectedTotalType = PromotionEffect.EffectedTotalOption.BASKET_TOTAL
            };

            if (VariantID.HasValue && items.Any(i => i.Variant.ID == VariantID))
            {
                decimal runningDiscount = 0;
                decimal runningDiscountTaxed = 0;
                foreach (var itemofprod in items.Where(i => i.Variant.ID == VariantID))
                {
                    if (percentOff.HasValue)
                    { 
                        var discount = (itemofprod.LinePrice * (decimal)percentOff / (decimal)100);
                        runningDiscount += discount;
                        if(!itemofprod.Variant.noTax)
                            runningDiscountTaxed += discount;
                    }
                    if (amountOff.HasValue)
                    {
                        var discount = (decimal)amountOff;
                        runningDiscount += discount;
                        if(!itemofprod.Variant.noTax)
                            runningDiscountTaxed += discount;
                    }
                }
                if (percentOff.HasValue)
                    effect.message = (int)percentOff + "% off " + product.name;
                if (amountOff.HasValue)
                    effect.message = ((decimal)amountOff).ToString("c") + " off " + product.name;

                effect.amountOff = runningDiscount;
                effect.amountOffTaxed = runningDiscountTaxed;
                return effect;
            }

            if (ProductID.HasValue && items.Any(i => i.Product.ID == ProductID))
            {
                decimal runningDiscount = 0;
                decimal runningDiscountTaxed = 0;
                foreach(var itemofprod in items.Where(i => i.Product.ID == ProductID))
                {
                    if (percentOff.HasValue)
                    {
                        var discount = (itemofprod.LinePrice * (decimal)percentOff / (decimal)100);
                        runningDiscount += discount;
                        if(!itemofprod.Variant.noTax)
                            runningDiscountTaxed += discount;
                    }
                    if (amountOff.HasValue)
                    {
                        var discount = (decimal)amountOff;
                        runningDiscount += discount;
                        if(!itemofprod.Variant.noTax)
                            runningDiscountTaxed += discount;
                    }
                }
                if (percentOff.HasValue)
                    effect.message = (int)percentOff + "% off " + product.name;
                if (amountOff.HasValue)
                    effect.message = ((decimal)amountOff).ToString("c") + " off " + product.name;

                effect.amountOff = runningDiscount;
                effect.amountOffTaxed = runningDiscountTaxed;
                return effect;
            }

            if (TagID.HasValue)
            {
                if(!items.Any(i => i.Product.tags.Any()))
                {
                    throw new Exception("item products have no categories");
                }

                decimal runningDiscount = 0;
                decimal runningDiscountTaxed = 0;
                foreach (var itemoftag in items.Where(i => i.Product.tags.Any(t => t.ID == TagID)))
                {
                    if (percentOff.HasValue)
                    {
                        var discount = (itemoftag.LinePrice * (decimal)percentOff / (decimal)100);
                        runningDiscount += discount;
                        if (!itemoftag.Variant.noTax)
                            runningDiscountTaxed += discount;
                    }
                    if (amountOff.HasValue)
                    {
                        var discount = (decimal)amountOff;
                        runningDiscount += discount;
                        if (!itemoftag.Variant.noTax)
                            runningDiscountTaxed += discount;
                    }
                }
                if (percentOff.HasValue)
                    effect.message = (int)percentOff + "% off " + tag.name;
                if (amountOff.HasValue)
                    effect.message = ((decimal)amountOff).ToString("c") + " off " + tag.name;

                effect.amountOff = runningDiscount;
                effect.amountOffTaxed = runningDiscountTaxed;
                return effect;
            }

            if (percentOff.HasValue)
            {
                effect.message = (int)percentOff + "% off";
                effect.amountOff = (inPrice * (decimal)percentOff / (decimal)100);

                var ratioOfTaxed = items.Where(x => !x.Variant.noTax).Sum(x => x.LinePrice) / items.Sum(x => x.LinePrice);
                effect.amountOffTaxed = effect.amountOff * ratioOfTaxed;

                return effect;
            }

            if (amountOff.HasValue)
            {
                effect.message = ((decimal)amountOff).ToString("c") + " off";
                effect.amountOff = (decimal)amountOff;

                var ratioOfTaxed = items.Where(x => !x.Variant.noTax).Sum(x => x.LinePrice) / items.Sum(x => x.LinePrice);
                effect.amountOffTaxed = effect.amountOff * ratioOfTaxed;

                return effect;
            }

            effect.amountOff = 0;
            return effect;
        }


    }
}