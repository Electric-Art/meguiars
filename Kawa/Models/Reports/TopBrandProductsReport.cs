﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TopBrandProductsReport : TopProductReport
    {
        protected ContentRepository _contentRep;

        public TopBrandProductsReport(ContentRepository contentRep)
            : base()
        {
            _contentRep = contentRep;
            //Setup
            MaxRows = 50;
            HasProductHiddenFilter = true;
        }

        public override string Name { 
            get {
                if (Filters.Single(x => x.Name == "Brand").SelectedID.HasValue)
                    return string.Format("Top {0} Products", Filters.Single(y => y.Name == "Brand").Options.Single(y => y.ID == Filters.Single(z => z.Name == "Brand").SelectedID).Name); 
                else
                    return "Top Brand Products"; 
            } 
        }


        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
            {
                new FilterSet(){
                            Name = "Brand",
                            Options =_contentRep.listAllTags().Where(x => x.Type == Tag.TypeOption.BRAND_SECTION).Select(x => new FilterOption(){ID=x.ID,Name=x.name}).ToList()
                        }
            };
        }

        protected override IQueryable<Data.Variant> getApplicableProducts(int ishidden)
        {
            if (Filters.Single(x => x.Name == "Brand").SelectedID.HasValue)
                return from i in _db.Variants where  (ishidden == 1 ? i.Product.isHidden == 0 : 1 == 1) && i.Product.Tag_Products.Any(x => x.TagID == Filters.Single(y => y.Name == "Brand").SelectedID) select i;
            else
                return base.getApplicableProducts(ishidden);
        }
    }
}
