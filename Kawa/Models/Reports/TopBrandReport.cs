﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TopBrandReport : TopCategoryReport
    {
        public TopBrandReport()
            : base()
        {
            //Setup
            MaxRows = 50;
            HasProductHiddenFilter = true;
        }

        public override string Name { get { return "Top Brands"; } }


        protected override IQueryable<Data.Tag> getApplicableTags()
        {
            return from i in _db.Tags where i.Type == (int)Tag.TypeOption.BRAND_SECTION select i;
        }
    }
}
