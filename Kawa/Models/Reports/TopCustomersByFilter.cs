﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TopCustomersByFilter : ReportBase<Models.Csv.TopCustomerByFilter>
    {
        protected ContentRepository _contentRep;
        protected ProductRepository _productRep;
        protected ShippingRepository _shipRep;

        public TopCustomersByFilter(ContentRepository contentRep, ProductRepository productRep, ShippingRepository shipRep)  : base()
        {
            //Setup
            MaxRows = 50;
            _contentRep = contentRep;
            _productRep = productRep;
            _shipRep = shipRep;
            HasProductHiddenFilter = false;
        }

        public override string Name { get { return "Top Customers by Purchase"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
            {
                new FilterSet(){
                            Name = "Categories",
                             Options =_contentRep.listAllTags().Where(x => x.Type == Tag.TypeOption.PRODUCT_SECTION).Select(x => new FilterOption(){ID=x.ID,Name=x.name}).OrderBy(x => x.Name).ToList()
                        },
                new FilterSet(){
                            Name = "Brands",
                             Options =_contentRep.listAllTags().Where(x => x.Type == Tag.TypeOption.BRAND_SECTION).Select(x => new FilterOption(){ID=x.ID,Name=x.name}).OrderBy(x => x.Name).ToList()
                        },
                new FilterSet(){
                            Name = "Product",
                            FilterSetUI = FilterSet.FilterSetUIOption.Lookup,
                            ObjectName = "Product"
                        },
                new FilterSet(){
                            Name = "Mailchimp",
                            Options = new List<FilterOption> { 
                                new FilterOption(){ID=1,Name="In Mailchimp"} ,
                                new FilterOption(){ID=2,Name="Not In Mailchimp"} 
                            }
                        },
            };
        }

        public override List<Models.Csv.TopCustomerByFilter> RunReport()
        {
            
            //var mailerUsers = MailChimpService.GetList();
            //var mailerUsers = new List<MailChimp.Lists.MemberInfo>();

            Product productFilter = null;
            if(Filters.Single(x => x.Name == "Product").SelectedID.HasValue)
                productFilter = _productRep.listAllProducts(false,true).Single(x => x.ID == Filters.Single(y => y.Name == "Product").SelectedID);

            Tag categoryFilter = null;
            if(Filters.Single(x => x.Name == "Categories").SelectedID.HasValue)
                categoryFilter = _contentRep.listAllTags().Single(x => x.ID == Filters.Single(y => y.Name == "Categories").SelectedID);
            
            Tag brandFilter = null;
            if(Filters.Single(x => x.Name == "Brands").SelectedID.HasValue)
                brandFilter = _contentRep.listAllTags().Single(x => x.ID == Filters.Single(y => y.Name == "Brands").SelectedID);

            var mailFilter = Filters.Single(x => x.Name == "Mailchimp").SelectedID;
            
            var rows = from o in _db.Orders
                       where (!Startdate.HasValue || (o.OrderDate >= Startdate)) && (!Enddate.HasValue || (o.OrderDate <= Enddate))
                       && (o.SystemStatus == (int)Order.SystemStatusOption.SENT || o.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER)
                       select o;

            if(productFilter != null)
                rows = rows.Where(o => o.Baskets.Any(x => x.VariantID == productFilter.variant.ID));

            if(categoryFilter != null)
                rows = rows.Where(o => o.Baskets.Any(x => x.Variant.Product.Tag_Products.Any(y => y.TagID == categoryFilter.ID) || x.Variant.Product.Tag_Products.Any(y => y.Tag.ParentID == categoryFilter.ID)));

            if(brandFilter != null)
                rows = rows.Where(o => o.Baskets.Any(x => x.Variant.Product.Tag_Products.Any(y => y.TagID == brandFilter.ID)));
            /*
            if(mailFilter.HasValue)
            {
                if(mailFilter == 1)
                    rows = rows.Where(o => _db.MailChimpCaches.Any(x => x.Email.ToLower() == o.AccountDetails.Single(y => y.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).Email.ToLower()));
                else
                    rows = rows.Where(o => !_db.MailChimpCaches.Any(x => x.Email.ToLower() == o.AccountDetails.Single(y => y.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).Email.ToLower()));
            }
            */
                       
            var grouprows = from o in rows         
                            group o by o.AccountDetails.Single(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).Email.ToLower() into g
                            select g;

            IQueryable<Models.Csv.TopCustomerByFilter> csvrows = null;

            if(productFilter != null)
                csvrows = from g in grouprows
                            select new Models.Csv.TopCustomerByFilter()
                            {
                                email = g.Key,
                                units = g.Sum(o => o.Baskets.Where(x => x.VariantID == productFilter.variant.ID).Sum(x => x.Quantity)),
                                noOrders = g.Count()
                            };
            if(categoryFilter != null)
                csvrows = from g in grouprows
                          select new Models.Csv.TopCustomerByFilter()
                          {
                              email = g.Key,
                              units = g.Sum(o => o.Baskets.Where(x => x.Variant.Product.Tag_Products.Any(y => y.TagID == categoryFilter.ID)).Sum(x => x.Quantity)),
                              noOrders = g.Count()
                          };
            if (brandFilter != null)
                csvrows = from g in grouprows
                          select new Models.Csv.TopCustomerByFilter()
                          {
                              email = g.Key,
                              units = g.Sum(o => o.Baskets.Where(x => x.Variant.Product.Tag_Products.Any(y => y.TagID == brandFilter.ID)).Sum(x => x.Quantity)),
                              noOrders = g.Count()
                          };



            var rowsFinal = MaxRows.HasValue ? csvrows.OrderByDescending(x => x.units).Take((int)MaxRows).ToList() : csvrows.OrderByDescending(x => x.units).ToList();
            /*
            if (mailFilter.SelectedID.HasValue)
            {
                if (mailFilter.SelectedID == 1)
                    rowsFinal = rowsFinal.Where(a => mailerUsers.Any(y => y.Email.ToLower() == a.email.ToLower())).ToList();
                else
                    rowsFinal = rowsFinal.Where(a => !mailerUsers.Any(y => y.Email.ToLower() == a.email.ToLower())).ToList();
            }
            */
            rowsFinal.ForEach(x => {
                var lastAccount = (from a in _db.AccountDetails 
                                   where a.Email.ToLower() == x.email.ToLower() && 
                                   a.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS && 
                                   (a.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || a.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER)
                                   orderby a.ID descending
                                   select a).First();
                x.firstName = clearForCsv(lastAccount.FirstName);
                x.surname = clearForCsv(lastAccount.Surname);
                x.phone = clearForCsv(lastAccount.Mobile);
                x.company = clearForCsv(lastAccount.Company);
                x.address = clearForCsv(lastAccount.Address1);
                x.address2 = clearForCsv(lastAccount.Address2);
                x.suburb = clearForCsv(lastAccount.Address3);
                x.state = clearForCsv(lastAccount.Address4);
                x.country = _shipRep.listAllCountries().Single(y => y.ID == lastAccount.CountryID).Name;
                x.subscribed = (from e in _db.MailChimpCaches where e.Email.ToLower() == x.email.ToLower() select e).Any()?"1":"0";
            });

            if (mailFilter.HasValue)
            {
                if (mailFilter == 1)
                    rowsFinal = rowsFinal.Where(o => o.subscribed == "1").ToList();
                else
                    rowsFinal = rowsFinal.Where(o => o.subscribed == "0").ToList();
            }

            return rowsFinal;
        }
    }
}

namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class TopCustomerByFilter
    {
        [DisplayName("First Name")]
        public string firstName { get; set; }

        [DisplayName("Surname")]
        public string surname { get; set; }

        [DisplayName("Email address")]
        public string email { get; set; }

        [DisplayName("Phone")]
        public string phone { get; set; }

        [DisplayName("Company")]
        public string company { get; set; }

        [DisplayName("Address")]
        public string address { get; set; }

        [DisplayName("Address 2")]
        public string address2 { get; set; }

        [DisplayName("Suburb")]
        public string suburb { get; set; }

        [DisplayName("State")]
        public string state { get; set; }

        [DisplayName("Country")]
        public string country { get; set; }

        [DisplayName("Subscribed")]
        public string subscribed { get; set; }

        [DisplayName("Units")]
        public int? units { get; set; }

        [DisplayName("Orders")]
        public int? noOrders { get; set; }
    }
}