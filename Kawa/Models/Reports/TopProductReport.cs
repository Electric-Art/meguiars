﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TopProductReport : ReportBase<Models.Csv.TopProduct>
    {
        public TopProductReport()
            : base()
        {
            //Setup
            MaxRows = 50;
            HasProductHiddenFilter = true;
        }

        public override string Name { get { return "Top Products"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
            {
                
            };
        }

        public override List<Models.Csv.TopProduct> RunReport()
        {
            var results = getApplicableProducts((excludeHidden ? 1 : 0));

            if(MaxRows.HasValue)
                //itemsTotal
                results = results.OrderByDescending(i => 
                    i.Baskets.Where(x => 
                        
                        (x.Order != null && (x.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || x.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER))
                        &&
                        (!Startdate.HasValue || (x.Order.OrderDate >= Startdate))
                        &&
                        (!Enddate.HasValue || (x.Order.OrderDate <= Enddate))

                        ).Sum(x => (decimal?)(x.Price * x.Quantity)) ?? 0
                 );

            var rows = results.Select(i => new Models.Csv.TopProduct()
            {
                name = i.Product.name,
                
                sku = i.sku,

                noSold = i.Baskets.Where(x =>

                        (x.Order != null && (x.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || x.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER))
                        &&
                        (!Startdate.HasValue || (x.Order.OrderDate >= Startdate))
                        &&
                        (!Enddate.HasValue || (x.Order.OrderDate <= Enddate))


                    ).Sum(x => (int?)x.Quantity) ?? 0,

                itemsTotal = i.Baskets.Where(x =>

                        (x.Order != null && (x.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || x.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER))
                        &&
                        (!Startdate.HasValue || (x.Order.OrderDate >= Startdate))
                        &&
                        (!Enddate.HasValue || (x.Order.OrderDate <= Enddate))

                    ).Sum(x => (decimal?)(x.Price * x.Quantity)) ?? 0,
                
                noOrders = i.Baskets.Where(x =>

                        (x.Order != null && (x.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || x.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER))
                        &&
                        (!Startdate.HasValue || (x.Order.OrderDate >= Startdate))
                        &&
                        (!Enddate.HasValue || (x.Order.OrderDate <= Enddate))
                    
                    ).GroupBy(b => b.OrderID).Select(group => new
                {
                    Metric = group.Key
                }).Count(),

            });

            return MaxRows.HasValue ? rows.Take((int)MaxRows).ToList() : rows.ToList();
        }

        protected virtual IQueryable<Data.Variant> getApplicableProducts(int ishidden)
        {
            if(ishidden==1)
            {
                return from i in _db.Variants join p in _db.Products on i.ProductID equals p.ID where p.isHidden==0 select i;

            } else
            {
                return from i in _db.Variants select i;
            }
            
        }
    }
}


namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class TopProduct
    {
        [DisplayName("Product Name")]
        public string name { get; set; }

        [DisplayName("Product Sku")]
        public string sku { get; set; }

        [DisplayName("No. Sold")]
        public int? noSold { get; set; }

        [DisplayName("Items Total")]
        public decimal? itemsTotal { get; set; }

        [DisplayName("No of Orders")]
        public int? noOrders { get; set; }
    }
}