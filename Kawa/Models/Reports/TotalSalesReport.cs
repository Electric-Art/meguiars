﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TotalSalesReport : ReportBase<Models.Csv.TotalSales>
    {
        public TotalSalesReport()
            : base()
        {
            //Setup
            HasProductHiddenFilter = false;
        }

        public override string Name { get { return "Total Sales"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
            {

            };
        }

        public override List<Models.Csv.TotalSales> RunReport()
        {
            var results = getApplicableOrders();
            var AusID = (from i in _db.Countries where i.Name == "Australia" select i).Single().ID;

            var rows = new List<Models.Csv.TotalSales>()
            { 
                new Models.Csv.TotalSales()
                {
                    name = "Total Sales",
                    creditCardTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    payPalTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    directDepositTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    total = results.Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    noOfOrders = results.Count()
                },
                
                new Models.Csv.TotalSales()
                {
                    name = "Total GST",
                    creditCardTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    payPalTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    directDepositTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    total = results.Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    noOfOrders = results.Count()
                },
                
                new Models.Csv.TotalSales()
                {
                    name = "Total GST minus international orders",
                    creditCardTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY && x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    payPalTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL && x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    directDepositTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT && x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    total = results.Where(x => x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    noOfOrders = results.Count(x => x.CountryID==AusID)
                }
               
            };
            return rows;
        }

        protected virtual IQueryable<Data.Order> getApplicableOrders()
        {
            return from i in _db.Orders
                   where
                   (i.SystemStatus == (int)Order.SystemStatusOption.SENT || i.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER)
                   &&
                   (!Startdate.HasValue || (i.OrderDate >= Startdate))
                   &&
                   (!Enddate.HasValue || (i.OrderDate <= Enddate))
                   select i;
        }
    }
}


namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class TotalSales
    {
        [DisplayName("Sales Group")]
        public string name { get; set; }

        [DisplayName("Credit Card total $$")]
        public decimal creditCardTotal { get; set; }

        [DisplayName("PayPal total $$")]
        public decimal payPalTotal { get; set; }

        [DisplayName("Direct Deposit total $$")]
        public decimal directDepositTotal { get; set; }

        [DisplayName("Total $$")]
        public decimal total { get; set; }

        [DisplayName("Orders")]
        public decimal noOfOrders { get; set; }
    }
}