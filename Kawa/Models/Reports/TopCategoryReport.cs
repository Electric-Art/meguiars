﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TopCategoryReport : ReportBase<Models.Csv.TopCategory>
    {
        public TopCategoryReport()
            : base()
        {
            //Setup
            MaxRows = 50;
            HasProductHiddenFilter = true;
        }

        public override string Name { get { return "Top Categories"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
            {
                
            };
        }

        public override List<Models.Csv.TopCategory> RunReport()
        {
            var results = getApplicableTags();

            if (MaxRows.HasValue)
                //itemsTotal
                
                results = results.OrderByDescending(i =>
                    _db.Baskets.Where(x =>

                        (x.Order != null && (x.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || x.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER))
                        &&
                        (!Startdate.HasValue || (x.Order.OrderDate >= Startdate))
                        &&
                        (!Enddate.HasValue || (x.Order.OrderDate <= Enddate))

                        && x.Variant.Product.Tag_Products.Any(tp => tp.TagID == i.ID)

                        && (excludeHidden ? x.Variant.Product.isHidden == 0 : 1 == 1)

                    ).Sum(x => (decimal?)(x.Price * x.Quantity)) ?? 0
                 );
              

            var rows = results.Select(i => new Models.Csv.TopCategory()
            {
                category = i.name,

                noSold = _db.Baskets.Where(x =>

                        (x.Order != null && (x.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || x.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER))
                        &&
                        (!Startdate.HasValue || (x.Order.OrderDate >= Startdate))
                        &&
                        (!Enddate.HasValue || (x.Order.OrderDate <= Enddate))

                        && x.Variant.Product.Tag_Products.Any(tp => tp.TagID==i.ID)

                        && (excludeHidden ? x.Variant.Product.isHidden ==0 : 1==1)


                    ).Sum(x => (int?)x.Quantity) ?? 0,

                itemsTotal = _db.Baskets.Where(x =>

                        (x.Order != null && (x.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || x.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER))
                        &&
                        (!Startdate.HasValue || (x.Order.OrderDate >= Startdate))
                        &&
                        (!Enddate.HasValue || (x.Order.OrderDate <= Enddate))

                        && x.Variant.Product.Tag_Products.Any(tp => tp.TagID == i.ID)
                        && (excludeHidden ? x.Variant.Product.isHidden == 0 : 1 == 1)

                    ).Sum(x => (decimal?)x.Price * x.Quantity) ?? 0,

                noOrders = _db.Baskets.Where(x =>

                        (x.Order != null && (x.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || x.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER))
                        &&
                        (!Startdate.HasValue || (x.Order.OrderDate >= Startdate))
                        &&
                        (!Enddate.HasValue || (x.Order.OrderDate <= Enddate))

                        && x.Variant.Product.Tag_Products.Any(tp => tp.TagID == i.ID)
                        && (excludeHidden ? x.Variant.Product.isHidden == 0 : 1 == 1)

                    ).GroupBy(b => b.OrderID).Select(group => new
                    {
                        Metric = group.Key
                    }).Count(),

            });

            return MaxRows.HasValue ? rows.Take((int)MaxRows).ToList() : rows.ToList();
        }

        protected virtual IQueryable<Data.Tag> getApplicableTags()
        {
            return from i in _db.Tags where i.Type == (int)Tag.TypeOption.PRODUCT_SECTION select i;
        }
    }
}


namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class TopCategory
    {
        [DisplayName("Category")]
        public string category { get; set; }

        [DisplayName("No. Sold")]
        public int? noSold { get; set; }

        [DisplayName("Items Total")]
        public decimal? itemsTotal { get; set; }

        [DisplayName("No of Orders")]
        public int? noOrders { get; set; }
    }
}