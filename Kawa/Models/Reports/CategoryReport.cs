﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class CategoryReport : TopProductReport
    {
        protected ContentRepository _contentRep;

        public CategoryReport(ContentRepository contentRep) : base()
        {
            _contentRep = contentRep;
        }

        public override string Name { get { return "Category Product Sales"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
                    {
                        new FilterSet(){
                            Name = "Categories",
                             Options =_contentRep.listAllTags().Where(x => x.Type == Tag.TypeOption.PRODUCT_SECTION).Select(x => new FilterOption(){ID=x.ID,Name=x.name}).OrderBy(x => x.Name).ToList()
                        }
                    };
        }

        protected override IQueryable<Data.Variant> getApplicableProducts(int ishidden)
        {
            if (Filters.Single(x => x.Name == "Categories").SelectedID.HasValue)
                return from i in _db.Variants where (ishidden == 1 ? i.Product.isHidden == 0 : 1 == 1) && i.Product.Tag_Products.Any(t => t.TagID == Filters.Single(x => x.Name == "Categories").SelectedID) select i;
            else
                return base.getApplicableProducts(ishidden);
        }
    }
}