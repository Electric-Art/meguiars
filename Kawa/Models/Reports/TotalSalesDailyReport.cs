﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TotalSalesDailyReport : ReportBase<Models.Csv.TotalSalesDaily>
    {
        public TotalSalesDailyReport()
            : base()
        {
            //Setup
            HasProductHiddenFilter = false;
        }

        public override string Name { get { return "Total Sales Daily"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
            {

            };
        }

        public override List<Models.Csv.TotalSalesDaily> RunReport()
        {
            if (Startdate == null || Enddate == null || ((DateTime)Enddate - (DateTime)Startdate).Days > 35)
            {
                var rowsErr = new List<Models.Csv.TotalSalesDaily>();
                rowsErr.Add(new Models.Csv.TotalSalesDaily(){
                    date = "Reports - Sorry you cannot run this report over a period more than a month, as this will compromose server performance. Please run each month separately"
                });
                return rowsErr;
            }

            var AusID = (from i in _db.Countries where i.Name == "Australia" select i).Single().ID;

            var rows = new List<Models.Csv.TotalSalesDaily>();
            for (DateTime date = (DateTime)Startdate.UtcToLocal(); date.Date < (DateTime)Enddate.UtcToLocal(); date = date.AddDays(1))
            {
                var currentDate = date.LocalToUtc();
                var results = getApplicableOrders((DateTime)currentDate, date.AddDays(1).LocalToUtc());
                rows.Add(new Models.Csv.TotalSalesDaily()
                {
                    date = currentDate.UtcToLocal().ToShortDateString(),
                    tot_creditCardTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    tot_payPalTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    tot_directDepositTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    tot_total = results.Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    gst_creditCardTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gst_payPalTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gst_directDepositTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gst_total = results.Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gstminus_creditCardTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY && x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gstminus_payPalTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL && x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gstminus_directDepositTotal = results.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT && x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gstminus_total = results.Where(x => x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    noOfOrders = results.Count()
                });
            };
            return rows;
        }

        protected virtual IQueryable<Data.Order> getApplicableOrders(DateTime startTime, DateTime endTime)
        {
            return from i in _db.Orders
                   where
                   (i.SystemStatus == (int)Order.SystemStatusOption.SENT || i.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER)
                   &&
                   (i.OrderDate >= startTime)
                   &&
                   (i.OrderDate < endTime)
                   select i;
        }
    }
}


namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class TotalSalesDaily
    {
        [DisplayName("Date")]
        public string date { get; set; }

        [DisplayName("Total Sales - Credit Card total $$")]
        public decimal tot_creditCardTotal { get; set; }

        [DisplayName("Total Sales - PayPal total $$")]
        public decimal tot_payPalTotal { get; set; }

        [DisplayName("Total Sales - Direct Deposit total $$")]
        public decimal tot_directDepositTotal { get; set; }

        [DisplayName("Total Sales - Total $$")]
        public decimal tot_total { get; set; }

        [DisplayName("Total GST - Credit Card total $$")]
        public decimal gst_creditCardTotal { get; set; }

        [DisplayName("Total GST - PayPal total $$")]
        public decimal gst_payPalTotal { get; set; }

        [DisplayName("Total GST - Direct Deposit total $$")]
        public decimal gst_directDepositTotal { get; set; }

        [DisplayName("Total GST - Total $$")]
        public decimal gst_total { get; set; }

        [DisplayName("Total GST minus int orders - Credit Card total $$")]
        public decimal gstminus_creditCardTotal { get; set; }

        [DisplayName("Total GST minus int orders - PayPal total $$")]
        public decimal gstminus_payPalTotal { get; set; }

        [DisplayName("Total GST minus int orders - Direct Deposit total $$")]
        public decimal gstminus_directDepositTotal { get; set; }

        [DisplayName("Total GST minus int orders - Total $$")]
        public decimal gstminus_total { get; set; }

        [DisplayName("Orders")]
        public decimal noOfOrders { get; set; }
    }
}