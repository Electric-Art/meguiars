﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class ProductReport : TopProductReport
    {
        //protected OrderRepository _orderRep;
        protected ProductRepository _productRep;
        //protected ContentRepository _contentRep;
        
        public ProductReport(ProductRepository productRep) : base()
        {
            _productRep = productRep;
        }
        
        public override string Name { get { return "Product Sales"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
                    {
                        new FilterSet(){
                            Name = "Product",
                            FilterSetUI = FilterSet.FilterSetUIOption.Lookup,
                            ObjectName = "Product"
                        }
                    };
        }

        protected override IQueryable<Data.Variant> getApplicableProducts(int ishidden)
        {
            if (Filters.Single(x => x.Name == "Product").SelectedID.HasValue)
                return from i in _db.Variants where (ishidden == 1 ? i.Product.isHidden == 0 : 1 == 1) && i.ProductID == Filters.Single(x => x.Name == "Product").SelectedID select i;
            else
                return base.getApplicableProducts(ishidden);
        }
    }
}

namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class Product
    {
        [DisplayName("Product Name")]
        public string name { get; set; }

        [DisplayName("Product Sku")]
        public string sku { get; set; }

        [DisplayName("No. Sold")]
        public int? noSold { get; set; }

        [DisplayName("Items Total")]
        public decimal? itemsTotal { get; set; }

        [DisplayName("No of Orders")]
        public int? noOrders { get; set; }
    }
}