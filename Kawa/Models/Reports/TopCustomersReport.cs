﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TopCustomersReport : ReportBase<Models.Csv.TopCustomer>
    {
        public TopCustomersReport()
            : base()
        {
            //Setup
            MaxRows = 50;
            HasProductHiddenFilter = false;
        }

        public override string Name { get { return "Top Customers"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
            {
                
            };
        }

        public override List<Models.Csv.TopCustomer> RunReport()
        {
            var rows = from o in _db.Orders
                       where (!Startdate.HasValue || (o.OrderDate >= Startdate)) && (!Enddate.HasValue || (o.OrderDate <= Enddate))
                       && (o.SystemStatus == (int)Order.SystemStatusOption.SENT || o.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER)
                       group o by o.AccountDetails.Single(x => x.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS).Email.ToLower() into g
                       select new Models.Csv.TopCustomer(){
                           email = g.Key,
                           orderTotal = g.Sum(o => o.OrderTotal),
                           noOrders = g.Count()
                       };

            var rowsFinal = MaxRows.HasValue ? rows.OrderByDescending(x => x.orderTotal).Take((int)MaxRows).ToList() : rows.OrderByDescending(x => x.orderTotal).ToList();

            rowsFinal.ForEach(x => {
                var lastAccount = (from a in _db.AccountDetails 
                                   where a.Email.ToLower() == x.email.ToLower() && 
                                   a.Type == (int)AccountDetail.DetailTypeOption.ACCOUNT_DETAILS && 
                                   (a.Order.SystemStatus == (int)Order.SystemStatusOption.SENT || a.Order.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER)
                                   orderby a.ID descending
                                   select a).First();
                x.customer = lastAccount.FirstName + " " + lastAccount.Surname;
            });

            return rowsFinal;
        }

    }
}


namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class TopCustomer
    {
        [DisplayName("Customer Name")]
        public string customer { get; set; }

        [DisplayName("Customer Email")]
        public string email { get; set; }

        [DisplayName("Orders Total")]
        public decimal? orderTotal { get; set; }

        [DisplayName("No of Orders")]
        public int? noOrders { get; set; }
    }
}