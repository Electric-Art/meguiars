﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Kawa.Models.Data;
using Kawa.Models.Services;
using Kawa.Models.Extensions;
using FileHelpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Kawa.Models.Reports
{
    public class TotalSalesDailyReportAccountant : ReportBase<Models.Csv.TotalSalesDaily>
    {
        public TotalSalesDailyReportAccountant()
            : base()
        {
            //Setup
            HasProductHiddenFilter = false;
        }

        public override string Name { get { return "Total Sales Daily - Accountants"; } }

        public override List<FilterSet> GetFilterSet()
        {
            return new List<FilterSet>()
            {

            };
        }

        public override List<Models.Csv.TotalSalesDaily> RunReport()
        {
            if (Startdate == null || Enddate == null || ((DateTime)Enddate - (DateTime)Startdate).Days > 35)
            {
                var rowsErr = new List<Models.Csv.TotalSalesDaily>();
                rowsErr.Add(new Models.Csv.TotalSalesDaily(){
                    date = "Reports - Sorry you cannot run this report over a period more than a month, as this will compromose server performance. Please run each month separately"
                });
                return rowsErr;
            }

            var AusID = (from i in _db.Countries where i.Name == "Australia" select i).Single().ID;

            var rows = new List<Models.Csv.TotalSalesDaily>();
            for (DateTime date = (DateTime)Startdate.UtcToLocal(); date.Date < (DateTime)Enddate.UtcToLocal(); date = date.AddDays(1))
            {
                var currentDate = date.LocalToUtc();
                var resultsFor6pm = getApplicableOrders((DateTime)currentDate.AddHours(18), currentDate.AddHours(18 + 24));
                var resultsFor4pm = getApplicableOrders((DateTime)currentDate.AddHours(16), currentDate.AddHours(16 + 24));
                var row = new Models.Csv.TotalSalesDaily()
                {
                    date = currentDate.UtcToLocal().ToShortDateString() + " - " + currentDate.UtcToLocal().AddDays(1).ToShortDateString(),
                    tot_creditCardTotal = resultsFor6pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    tot_payPalTotal = resultsFor4pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    tot_directDepositTotal = resultsFor6pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT).Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    //tot_total = results.Sum(x => (decimal?)x.OrderTotal) ?? 0,
                    gst_creditCardTotal = resultsFor6pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gst_payPalTotal = resultsFor4pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gst_directDepositTotal = resultsFor6pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    //gst_total = results.Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gstminus_creditCardTotal = resultsFor6pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.EWAY && x.CountryID == AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gstminus_payPalTotal = resultsFor4pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.PAYPAL && x.CountryID == AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    gstminus_directDepositTotal = resultsFor6pm.Where(x => x.PaymentType == (int)PaymentServiceStore.PaymentTypeOption.DIRECT_DEBIT && x.CountryID == AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0,
                    //gstminus_total = results.Where(x => x.CountryID==AusID).Sum(x => (decimal?)x.TaxTotal) ?? 0
                };

                row.tot_total = row.tot_creditCardTotal + row.tot_directDepositTotal + row.tot_payPalTotal;
                row.gst_total = row.gst_creditCardTotal + row.gst_directDepositTotal + row.gst_payPalTotal;
                row.gstminus_total = row.gstminus_creditCardTotal + row.gstminus_directDepositTotal + row.gstminus_payPalTotal;

                rows.Add(row);
            };
            return rows;
        }

        protected virtual IQueryable<Data.Order> getApplicableOrders(DateTime startTime, DateTime endTime)
        {
            return from i in _db.Orders
                   where
                   (i.SystemStatus == (int)Order.SystemStatusOption.SENT || i.SystemStatus == (int)Order.SystemStatusOption.PAID_ORDER)
                   &&
                   (i.OrderDate >= startTime)
                   &&
                   (i.OrderDate < endTime)
                   select i;
        }
    }
}

