﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FileHelpers;

namespace Kawa.Models
{
    public interface IAdminAccountViewModel
    {
        string aFirstName { get;  }
        string aLastName { get;  }
        string aEmail { get;  }
        int? AccountID { get; }
        int? LastOrderID { get; }

        int getRank();
    }

    public class AdminAccountViewModel
    {
        public string aFirstName { get; set; }
        public string aLastName { get; set; }
        public string aEmail { get; set; }
        public int? AccountID { get; set; }
        public int? LastOrderID { get; set; }


        /// <summary>
        /// export fields.
        /// </summary>
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string Mobile { get; set; }
        public bool isMailer { get; set; }
        public int OrderCount { get; set; }
        public decimal OrdersTotal { get; set; }
    }

    public class Account : IAdminAccountViewModel
    {
        public enum GenderOption
        {
            FEMALE,
            MALE
        }

        public int ID { get; set; }
		public string UserName { get; set; }
		public DateTime createDate { get; set; }
		public bool isAdmin { get; set; }
		public bool isTrade { get; set; }
		public bool isBlocked { get; set; }
        public string creditCardToken { get; set; }
        public bool saveCreditCard { get; set; }
		public string firstName { get; set; }
		public string lastName { get; set; }
        public GenderOption? gender { get; set; }
		public DateTime? dateOfBirth { get; set; }
		public string channel { get; set; }

        public List<Favourite> favourites { get; set; }

        public int favouritesCount { get; set; }
        public int orderCount { get; set; }

        public string aFirstName { get { return firstName; } }
        public string aLastName { get { return lastName; } }
        public string aEmail { get { return UserName; } }
        public int? AccountID { get { return ID; } }
        public int? LastOrderID { get; set; }
        /// <summary>
        /// export fields.
        /// </summary>
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string Mobile { get; set; }
        public bool isMailer { get; set; }
        public decimal OrdersTotal { get; set; }


        public int getRank()
        {
            return 1;
        }
    }

    public class Person
    {
        public enum TypeOption
        {
            HEALTH_CLUB,
            EBOOK,
            SUSPENDED_CLUB
        }

        public int ID { get; set; }
        public Guid? UserID { get; set; }
        [DisplayName("First Name")]
        [Required]
        public string firstName { get; set; }
        [DisplayName("Surname")]
        [Required]
        public string surname { get; set; }
        [DisplayName("Title")]
        public string title { get; set; }
        [DisplayName("Email Address")]
        [EmailAddress]
        [Required]
        public string email { get; set; }

        [DisplayName("Postal Address")]
        public string address { get; set; }
        [DisplayName("Suburb")]
        public string suburb { get; set; }
        [DisplayName("State")]
        public string state { get; set; }
        [DisplayName("PostCode")]
        public string pcode { get; set; }
        [DisplayName("Phone")]
        public string phone { get; set; }
        [DisplayName("Mobile")]
        public string mobile { get; set; }
        [DisplayName("Subscribe to newsletter")]
        public bool isSubscribed { get; set; }
        public DateTime createDate { get; set; }
        public long? FacebookID { get; set; }
        public string GoogleID { get; set; }
        public string GoogleUsername { get; set; }
        public string TwitterID { get; set; }
        public string TwitterUsername { get; set; }

        public TypeOption Type { get; set; }
        public string FacebookToken { get; set; }
    }

    public class SignupPerson : Person
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class FacebookAccountResponse
    {
        public string accessToken { get; set; }
        public int expiresIn { get; set; }
        public string signedRequest { get; set; }
        public string userID { get; set; }
    }

    /// <summary>
    /// Summary description for Signup.
    /// </summary>
    /// 
    [DelimitedRecord(",")]
    public class Signup
    {
        public int ID { get; set; }
        [DisplayName("First Name")]
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string Email { get; set; }
        [DisplayName("Store Name")]
        [Required]
        public string StoreName { get; set; }
        [Required]
        public string Suburb { get; set; }
        public DateTime createDate { get; set; }
        public int Type { get; set; }




    }
}

namespace Kawa.Models.Csv
{
    [DelimitedRecord(",")]
    public class Person
    {
        public int ID { get; set; }
        [DisplayName("First Name")]
        [Required]
        public string firstName { get; set; }
        [DisplayName("Surname")]
        [Required]
        public string surname { get; set; }
        [DisplayName("Title")]
        public string title { get; set; }
        [DisplayName("Email Address")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        [Required]
        public string email { get; set; }

        [DisplayName("Postal Address")]
        public string address { get; set; }
        [DisplayName("Suburb")]
        public string suburb { get; set; }
        [DisplayName("State")]
        [Required]
        public string state { get; set; }
        [DisplayName("PostCode")]
        [Required]
        public string pcode { get; set; }
        [DisplayName("Phone")]
        public string phone { get; set; }
        [DisplayName("Mobile")]
        public string mobile { get; set; }
        [DisplayName("Subscribe to newsletter")]
        public bool isSubscribed { get; set; }
        public DateTime createDate { get; set; }

        public Models.Person.TypeOption Type { get; set; }
    }
}