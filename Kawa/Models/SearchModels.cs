﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kawa.Models
{
    public class SuggestionResult
    {
        public List<Models.Coms.Product> products_consumer { get; set; }
        public List<Models.Coms.Product> products_pro { get; set; }

        public List<Models.Coms.Tag> tags { get; set; }

        public List<Models.Data.New> news { get; set; }
        public List<Models.Data.CalendarEvent> events { get; set; }
        public List<Models.Data.LmcGallery> galleries { get; set; }
        public List<Models.Page> pages { get; set; }

        public List<string> suggestions { get; set; }
        public bool isSearchanise  { get; set; }
    }
}