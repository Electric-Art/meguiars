﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kawa.Startup))]
namespace Kawa
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
