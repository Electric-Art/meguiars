﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Kawa.Models.Extensions;

namespace Kawa
{
    public class FactoryConfig
    {
        public static void RegisterFactories()
        {
            Mapper.CreateMap<Models.Person, Models.Csv.Person>();
            Mapper.CreateMap<Models.AccountDetail, Models.AccountDetailDelivery>();
            Mapper.CreateMap<Models.IAdminAccountViewModel, Models.AdminAccountViewModel>();

            Mapper.CreateMap<Models.Tag, Models.Coms.Tag>().ForMember(
                dest => dest.url,
                opt => opt.MapFrom(src => src.getUrl())
            );

            Mapper.CreateMap<Models.Product, Models.Coms.Product>().ForMember(
                   dest => dest.url,
                   opt => opt.MapFrom(src => src.getUrl())
                ).ForMember(
                   dest => dest.imageUrl,
                   opt => opt.MapFrom(src => src.getSrUrl(Models.SiteResource.SiteResourceTypeOption.PRODUCT_IMAGE,Models.SiteResource.SiteResourceVersion.ITEM_SEARCH))
               ).ForMember(
                   dest => dest.price,
                   opt => opt.MapFrom(src => src.variant.price));

            Mapper.CreateMap<Models.Order, Models.Coms.Order>().ForMember(
                  dest => dest.CustomerDetails_FirstName,
                  opt => opt.MapFrom(src => src.CustomerDetails.FirstName)
              ).ForMember(
                  dest => dest.CustomerDetails_Surname,
                  opt => opt.MapFrom(src => src.CustomerDetails.Surname)
              );

            Mapper.CreateMap<Models.Data.PurchaseOrderItem, Models.Coms.PurchaseOrderItem>()
                .ForMember(
                   dest => dest.OrderIDDate,
                   opt => opt.MapFrom(src => src.OrderID.HasValue ? src.Order.OrderDate.UtcToLocal().ToString("dd/MM/yyyy hh:mm tt") : null)
                )
                .ForMember(
                   dest => dest.ProductIDName,
                   opt => opt.MapFrom(src => src.ProductID.HasValue ? src.Product.poName != null ? src.Product.poName : src.Product.name : null)
                ).ForMember(
                   dest => dest.ProductIDSku,
                   opt => opt.MapFrom(src => src.ProductID.HasValue ? src.Product.sku : null)
                ).ForMember(
                   dest => dest.ProductIDDaysToShip,
                   opt => opt.MapFrom(src => src.ProductID.HasValue ? src.Product.Variants.First().shipDays ?? (int?)3 : null)
                ).ForMember(
                   dest => dest.ProductIDUrl,
                   opt => opt.MapFrom(src => src.ProductID.HasValue ? Models.Product.getUrl(src.Product.nameUrl) : null)
                ).ForMember(
                   dest => dest.SupplierName,
                   opt => opt.MapFrom(src => src.SupplierID.HasValue ? src.Supplier.Name : null)
                ).ForMember(
                   dest => dest.OrderedDate,
                   opt => opt.MapFrom(src => src.OrderedDate.HasValue ? ((DateTime)src.OrderedDate).ToString("dd/MM/yyyy") : null)
               ).ForMember(
                   dest => dest.OutOfStockUntilDate,
                   opt => opt.MapFrom(src => src.OutOfStockUntilDate.HasValue ? ((DateTime)src.OutOfStockUntilDate).ToString("dd/MM/yyyy") : null)
               ).ForMember(
                   dest => dest.FinalisedDate,
                   opt => opt.MapFrom(src => src.FinalisedDate.HasValue ? ((DateTime)src.FinalisedDate).ToString("dd/MM/yyyy") : null)
               ).ForMember(
                   dest => dest.StatusName,
                   opt => opt.MapFrom(src => src.Status.ToString())
               );


            Mapper.CreateMap<Models.Product, Models.Csv.SearchProduct>()
                .ForMember(
                   dest => dest.code,
                   opt => opt.MapFrom(src => src.sku)
                ).ForMember(
                   dest => dest.warn,
                   opt => opt.MapFrom(src => string.Join(",", src.disclaimersDisplay.Select(x => x.disclaimerAbreviation)))
                ).ForMember(
                   dest => dest.shipDays,
                   opt => opt.MapFrom(src => src.variant.shipDays + " " + src.VariantPreOrderNice)
               ).ForMember(
                   dest => dest.stockCount,
                   opt => opt.MapFrom(src => src.VariantStockNo)
               ).ForMember(
                   dest => dest.rrp,
                   opt => opt.MapFrom(src => src.variant.priceRRP)
                ).ForMember(
                   dest => dest.discountPercent,
                   opt => opt.MapFrom(src => src.variant.priceMargin)
               ).ForMember(
                   dest => dest.price,
                   opt => opt.MapFrom(src => src.variant.price)
               ).ForMember(
                   dest => dest.status,
                   opt => opt.MapFrom(src => src.isHidden ? "offline" : "online"));
        }
    }
}