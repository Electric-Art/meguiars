﻿using System.Web;
using System.Web.Mvc;
using Kawa.Models.Extensions;

namespace Kawa
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            ModelBinders.Binders.Add(typeof(System.DateTime), new JsonDateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(System.DateTime?), new JsonDateTimeModelBinder());
        }
    }
}
