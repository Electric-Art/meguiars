﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Reflection;
using Kawa.Controllers;

namespace Kawa
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            RouteConfigRedirects.RegisterVersionRedirectRoutes(routes);

            RegisterAdminControllerRoutes(routes, typeof(AdminController), "Admin");
            RegisterAdminControllerRoutes(routes, typeof(ConfigController), "Config");
            RegisterAdminControllerRoutes(routes, typeof(ShippingConfigController), "ShippingConfig");
            RegisterAdminControllerRoutes(routes, typeof(HomeAdminController), "Home");
            RegisterAdminControllerRoutes(routes, typeof(TagAdminController), "Categories");
            RegisterAdminControllerRoutes(routes, typeof(PrescriptionTagAdminController), "Prescription");
            RegisterAdminControllerRoutes(routes, typeof(ArticleAdminController), "Pages");
            RegisterAdminControllerRoutes(routes, typeof(ProductAdminController), "Products");
            RegisterAdminControllerRoutes(routes, typeof(ShopAdminController), "Orders");
            RegisterAdminControllerRoutes(routes, typeof(AccountAdminController), "Accounts");
            RegisterAdminControllerRoutes(routes, typeof(PromotionAdminController), "Promotions");
            RegisterAdminControllerRoutes(routes, typeof(PropellaAdminController), "Propella");
            RegisterAdminControllerRoutes(routes, typeof(ReportsController), "Reports");
            RegisterAdminControllerRoutes(routes, typeof(FlagsController), "Flags");
            RegisterAdminControllerRoutes(routes, typeof(SignaturesController), "Signatures");
            RegisterAdminControllerRoutes(routes, typeof(PurchaseOrdersController), "PurchaseOrders");
            RegisterAdminControllerRoutes(routes, typeof(SupplierAdminController), "Suppliers");
            RegisterAdminControllerRoutes(routes, typeof(FaqAdminController), "Faq");
            routes.MapRoute(
               name: "Megacann",
               url: "megacann",
                defaults: new { controller = "Articles", action = "Article", id = 548 }
           );
            routes.MapRoute(
               name: "nzpc300",
               url: "NZPC300",
                defaults: new { controller = "Articles", action = "Article", id = 549 }
           );
            routes.MapRoute(
               name: "Signup",
               url: "signup/{code}",
               defaults: new { controller = "Account", action = "Signup", code = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "About",
                url: "about/{id}",
                defaults: new { controller = "About", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Marine",
                url: "marine",
                defaults: new { controller = "Articles", action = "Article", id = 92 }
            );
            routes.MapRoute(
                name: "Contact",
                url: "contact/{id}",
                defaults: new { controller = "Home", action = "Contact", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "SiteMap",
                url: "SiteMap/{id}",
                defaults: new { controller = "Home", action = "SiteMap", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Articles",
                url: "articles/{id}",
                defaults: new { controller = "Articles", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Article",
                url: "articles/{id}/{.}",
                defaults: new { controller = "Articles", action = "Article", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "StockNotify",
                url: "products/notify/{id}",
                defaults: new { controller = "Products", action = "Notify", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Category",
                url: "Category/{id}",
                defaults: new { controller = "Categories", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Products",
                url: "products/{id}",
                defaults: new { controller = "Products", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "BrandProducts",
                url: "products/{brand}/{name}",
                defaults: new { controller = "Products", action = "Brand", brand = UrlParameter.Optional, name = UrlParameter.Optional}
            );
            routes.MapRoute(
                name: "Product",
                url: "product/{id}",
                defaults: new { controller = "Products", action = "Product", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Defaultx",
                url: "{controller}/{action}/{id}/{name}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional, name = UrlParameter.Optional }
            );
           

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }

        public static void RegisterAdminControllerRoutes(RouteCollection routes, Type t, string shortControllerName, bool useShortForRouteName = false)
        {
            var controllerName = useShortForRouteName ? shortControllerName + "Admin" : t.Name.Replace("Controller", string.Empty);
            foreach (var action in GetMvcActionMethods(t))
            {
                if (action == "Index")
                {
                    continue;
                }
                routes.MapRoute(
                    name: controllerName + action,
                    url: "admin/" + shortControllerName.ToLower() + "/" + action.ToLower() + "/{id}",
                    defaults: new { controller = controllerName, action = action, id = UrlParameter.Optional }
                    );
            }

            routes.MapRoute(
                        name: controllerName + "Index",
                        url: "admin/" + shortControllerName.ToLower() + "/{id}",
                        defaults: new { controller = controllerName, action = "Index", id = UrlParameter.Optional }
                    );
        }

        public static List<string> GetMvcActionMethods(Type t)
        {
            var methods = t.GetMethods(BindingFlags.Public | BindingFlags.Instance).Where(m => typeof(ActionResult).IsAssignableFrom(m.ReturnType));
            return methods.Select(m => m.Name).Distinct().ToList();
        }
    }
}
