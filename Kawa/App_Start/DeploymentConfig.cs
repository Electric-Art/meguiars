﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models;
using Kawa.Models.Services;
using System.Configuration;
using Kawa.Models.Data;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Kawa.Models.Extensions;
using System.IO;

namespace Kawa
{
    public class DeploymentConfig
    {
        private static bool HasRunRunIdentityUpdates;

        public static void RunUpdates()
        {
            RunUpdates(true);
        }

        public static void RunIdentityUpdates()
        {
            if (HasRunRunIdentityUpdates) return;
            RunUpdates(false);
            HasRunRunIdentityUpdates = true;
        }

        public static void RunUpdates(bool startup)
        {
            RunUpdates01(startup);
        }


        public static void RunUpdates01(bool startup)
        {
            if(startup)
            {


                
                DeploymentService.RunSqlDeployment("220905_productmeta", "220905_productmeta");

                if (DeploymentService.ShouldRunAdhocDeployment("220905_productmeta_import"))
                {
                    var deploy = DeploymentService.StartRunningDeployment("220905_productmeta_import", "220905_productmeta_import", "220905_productmeta_import");
                    PropellaService.suckNEWSmitsData();
                    DeploymentService.FinishRunningDeployment(deploy);
                }


                ///DeploymentService.RunSqlDeployment("220728_PrescriptionTag", "220728_PrescriptionTag");

                DeploymentService.RunSqlDeployment("220810_OutOfStockLMC", "develop");

                DeploymentService.RunSqlDeployment("220726_GuestFavs", "220726_GuestFavs");
                
                if (DeploymentService.ShouldRunAdhocDeployment("220728_PrescriptionTag_Migration"))
                {
                    var deploy = DeploymentService.StartRunningDeployment("220728_PrescriptionTag_Migration", "220728_PrescriptionTag_Migration", "220728_PrescriptionTag_Migration");

                    using (var _db = new Models.Data.dbDataContext())
                    {
                        SiteResourceService srs = new SiteResourceService();
                        DirectoryInfo startDir = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~/") + "Content/SiteResources/TAG/");

                        foreach (DirectoryInfo d in startDir.GetDirectories())
                        {
                            int dirID = int.Parse(d.Name);

                            if(dirID > 6379)
                            {
                                d.MoveTo(HttpContext.Current.Server.MapPath("~/") + "Content/SiteResources/PRESCRIPTIONTAG/" + d.Name);
                            }

                        }

                    }
                    DeploymentService.FinishRunningDeployment(deploy);
                }


                if (DeploymentService.ShouldRunAdhocDeployment("220726_GuestFavs_account"))
                {
                    var deploy = DeploymentService.StartRunningDeployment("220726_GuestFavs_account", "220726_GuestFavs_account", "220726_GuestFavs_account");

                    using (var _db = new Models.Data.dbDataContext())
                    {
                        
                        _db.Accounts.InsertOnSubmit(new Models.Data.Account() { firstName = "Guest", UserName = "guest@meguiars.co.nz", createDate = System.DateTime.Now });
                        _db.SubmitChanges();
                    }
                    DeploymentService.FinishRunningDeployment(deploy);
                }

                


                DeploymentService.RunSqlDeployment("220815_PromoSKU", "220815_PromoSKU");
                DeploymentService.RunSqlDeployment("220420_Tag_ProductRelationships", "220420_Tag_ProductRelationships");
                DeploymentService.RunSqlDeployment("220420_RelatedProducts", "220420_RelatedProducts");
                DeploymentService.RunSqlDeployment("220421_CalendarEvents", "220421_CalendarEvents");

                DeploymentService.RunSqlDeployment("220426_PageProductRelationships", "220426_PageProductRelationships");
                if (DeploymentService.ShouldRunAdhocDeployment("220503_CatalogContainer"))
                {
                    var deploy = DeploymentService.StartRunningDeployment("220503_CatalogContainer", "220503_CatalogContainer", "220503_CatalogContainer");

                    using (var _db = new Models.Data.dbDataContext())
                    {
                        Models.Data.Page p = new Models.Data.Page();
                        p.Type = 11;
                        p.name = "Product Catalogs";
                        p.createdDate = System.DateTime.Now;
                        _db.Pages.InsertOnSubmit(p);
                        _db.SubmitChanges();
                    }
                    DeploymentService.FinishRunningDeployment(deploy);
                }
                if (DeploymentService.ShouldRunAdhocDeployment("220503_NewSelector"))
                {
                    var deploy = DeploymentService.StartRunningDeployment("220503_NewSelector", "220503_NewSelector", "220503_NewSelector");

                    using (var _db = new Models.Data.dbDataContext())
                    {
                        Models.Data.Page p = new Models.Data.Page();
                        p.Type = 9;
                        p.name = "News Article Selector";
                        p.createdDate = System.DateTime.Now;
                        _db.Pages.InsertOnSubmit(p);
                        _db.SubmitChanges();
                    }
                    DeploymentService.FinishRunningDeployment(deploy);
                }

                if (DeploymentService.ShouldRunAdhocDeployment("220412_SetupPrescriptions"))
                {
                    var deploy = DeploymentService.StartRunningDeployment("220412_SetupPrescriptions", "220412_SetupPrescriptions", "220412_SetupPrescriptions");
                    List<string> buildme = new List<string>() { "Exotic","Sports", "Muscle", "Vintage", "Hotrod and Custom", "Classic", "Daily Driver", "Lowrider", "Motorcycle", "Truck & 4x4", "BOAT" };

                    using (var _db = new Models.Data.dbDataContext())
                    {
                        foreach(string s in buildme)
                        {

                            Models.Data.Tag p = new Models.Data.Tag();
                            p.name = s;
                            p.ParentID = 6379;
                            p.Type = 7;                            
                            p.nameUrl = p.name.ToSafeString();
                            _db.Tags.InsertOnSubmit(p);
                            _db.SubmitChanges();

                            Models.Data.Tag parent = _db.Tags.Where(x => x.name == p.name).FirstOrDefault();

                            List<string> subtags = new List<string>() { "Other","Paint","Wheels","Tires","Headlights","Interior"};
                            foreach(string sx in subtags)
                            { 
                                p = new Models.Data.Tag();
                                p.name = sx;
                                p.ParentID = parent.ID;
                                p.Type = 7;
                                string uniquename = parent.name +"-"+ p.name;
                                p.nameUrl = uniquename.ToSafeString();
                                _db.Tags.InsertOnSubmit(p);
                                _db.SubmitChanges();
                            }


                        }
                    }
                    DeploymentService.FinishRunningDeployment(deploy);
                }
            }
            else
            {
                AddAccount("Add Admin User", "admin@meguiars.co.nz", "2fRBBwdUABNR2Xuq", "Admin", "admin account");
            }
        }


        public static bool AddAccount(string deployment, string email, string password, string role, string name)
        {
            using (var _db = new dbDataContext())
            {

                var UserManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var RoleManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationRoleManager>();

                if (UserManager.FindByName(email) != null)
                    return false;

                var orwinRole = RoleManager.FindByName(role);
                if (orwinRole == null)
                {
                    orwinRole = new IdentityRole(role);
                    var roleresult = RoleManager.Create(orwinRole);
                }
                var user = new ApplicationUser { UserName = email.ToLower(), Email = email.ToLower() };
                var result = UserManager.Create(user, password);
                if (result.Succeeded)
                {
                    UserManager.AddToRole(user.Id, role);

                    AlertService.SendTechMonitorAlert(string.Format("Deployment {0} added account to {1}", deployment, email, ModelTransformations.GetSiteUrl()), string.Format("{0} added as {1} {2}", email, role, role));
                    return true;
                }
                else
                {
                    AlertService.SendTechMonitorAlert(string.Format("Deployment {0} has NOT added account to {1}", deployment, email, ModelTransformations.GetSiteUrl()), string.Format("{0} failed as {1} {2} - {3}", email, role, role, string.Join(",", result.Errors.ToArray())));
                    return false;
                }
            }
        }



    }
}