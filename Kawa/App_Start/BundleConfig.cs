﻿using System.Web;
using System.Web.Optimization;

namespace Kawa
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/sitewidejs").Include(
                        //"~/Scripts/jquery.validate*",
                        "~/Scripts/kawa-display.js", 
                        //"~/Scripts/retina.js",
                        "~/Scripts/kawa-products.js",
                        "~/Scripts/bootbox.min.js",
                        "~/Scripts/sly.min.js",
                        "~/Scripts/ang/controllers/SiteModule.js",
                        "~/Scripts/ang/controllers/SearchController.js",
                        "~/Scripts/kawa-takeover.js",
                        "~/Scripts/kawa-slider.js"));

            bundles.Add(new ScriptBundle("~/bundles/cycle").Include(
                        "~/Scripts/jquery.cycle2.min.js", "~/Scripts/kawa-cycle.js"));

            bundles.Add(new ScriptBundle("~/bundles/slider").Include(
                        "~/Scripts/kawa-slider.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            
            bundles.Add(new ScriptBundle("~/bundles/stores").Include(
                        "~/Scripts/kawa-mapcontroller.js"));

            bundles.Add(new ScriptBundle("~/bundles/shop").Include(
                        "~/Scripts/kawa-shop.js"));

            bundles.Add(new ScriptBundle("~/bundles/accountdetails").Include(
                       "~/Scripts/kawa-accountdetails.js"));

            bundles.Add(new ScriptBundle("~/bundles/account").Include(
                        "~/Scripts/kawa-account.js"));

            bundles.Add(new ScriptBundle("~/bundles/payment").Include(
                        "~/Scripts/kawa-payment.js"));

            bundles.Add(new ScriptBundle("~/bundles/favourites").Include(
                        "~/Scripts/kawa-favourites.js"));

            bundles.Add(new ScriptBundle("~/bundles/tracking").Include(
                        "~/Scripts/kawa-tracking.js"));

            bundles.Add(new ScriptBundle("~/bundles/admin").Include(
                        "~/Scripts/bootbox.min.js",
                        "~/Scripts/kawa-admin.js"));

            bundles.Add(new ScriptBundle("~/bundles/slotmanager").Include(
                        "~/Scripts/kawa-slotmanager.js"));

            bundles.Add(new ScriptBundle("~/bundles/shopadmin").Include(
                        "~/Scripts/ang/directives/pagination/dirPagination.js",
                        "~/Scripts/ang/directives/checklist-model.js",
                        "~/Scripts/ang/controllers/KawaApp.js",
                        "~/Scripts/ang/controllers/OrderController.js",
                        "~/Scripts/ang/controllers/PurchaseOrderController.js"));

            bundles.Add(new ScriptBundle("~/bundles/hud").Include(
                        "~/Scripts/ang/directives/hud.js"));

            bundles.Add(new ScriptBundle("~/bundles/shippingadmin").Include(
                        "~/Scripts/kawa-shipping-admin.js"));

            bundles.Add(new ScriptBundle("~/bundles/orderupdate").Include(
                        "~/Scripts/kawa-admin-orderupdate.js"));

            bundles.Add(new ScriptBundle("~/bundles/shippingrates").Include(
                        "~/Scripts/ang/directives/checklist-model.js",
                        "~/Scripts/ang/directives/pagination/dirPagination.js",
                        "~/Scripts/ang/controllers/KawaApp.js",
                        "~/Scripts/ang/controllers/ShippingRatesController.js"));

            bundles.Add(new ScriptBundle("~/bundles/productlist").Include(
                        "~/Scripts/ang/directives/checklist-model.js",
                        "~/Scripts/ang/directives/pagination/dirPagination.js",
                       "~/Scripts/ang/controllers/KawaApp.js",
                       "~/Scripts/ang/controllers/ProductListController.js"));

            bundles.Add(new ScriptBundle("~/bundles/accountlist").Include(
                        "~/Scripts/ang/directives/pagination/dirPagination.js",
                       "~/Scripts/ang/directives/checklist-model.js",
                        "~/Scripts/ang/controllers/KawaApp.js",
                       "~/Scripts/ang/controllers/AccountListController.js"));

            bundles.Add(new ScriptBundle("~/bundles/variants").Include(
                        "~/Scripts/ang/directives/pagination/dirPagination.js",
                      "~/Scripts/ang/directives/checklist-model.js",
                        "~/Scripts/ang/controllers/KawaApp.js",
                      "~/Scripts/ang/controllers/VariantController.js"));

            bundles.Add(new ScriptBundle("~/bundles/search").Include(
                      "~/Scripts/ang/controllers/SiteModule.js",
                      "~/Scripts/ang/controllers/SearchController.js"));

            bundles.Add(new ScriptBundle("~/bundles/settings").Include(
                        "~/Scripts/ang/directives/pagination/dirPagination.js",
                      "~/Scripts/ang/directives/checklist-model.js",
                        "~/Scripts/ang/controllers/KawaApp.js",
                      "~/Scripts/ang/controllers/SettingsController.js"));

            bundles.Add(new ScriptBundle("~/bundles/special").Include(
                      "~/Scripts/kawa-special-register.js"));

            bundles.Add(new ScriptBundle("~/bundles/typeahead").Include(
                        "~/Scripts/typeahead.bundle.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminlookups").Include(
                        "~/Scripts/kawa-adminlookups.js"));

            bundles.Add(new ScriptBundle("~/bundles/prod").Include(
                        "~/Scripts/bootstrap-image-gallery.js"));

            bundles.Add(new ScriptBundle("~/bundles/repeatedform").Include(
                        "~/Scripts/kawa-repeatedform.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      //"~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap-addons.css",
                      "~/Content/bootstrap-five.css",
                      "~/Content/menu.css",
                      "~/Content/product.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/accountcss").Include(
                      "~/Content/simple-sidebar.css"));

            bundles.Add(new StyleBundle("~/Content/csssitewideslot").Include(
                      "~/Content/sitewideslot.css"));

            bundles.Add(new StyleBundle("~/Content/admincss").Include(
                      "~/Content/simple-sidebar.css",
                      "~/Content/site.css",
                      "~/Content/admin.css"));

            bundles.Add(new StyleBundle("~/Content/prodimages").Include(
                     "~/Content/bootstrap-image-gallery.css"));
        }
    }
}
