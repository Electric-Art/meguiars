﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kawa.Models;

namespace Kawa
{
    public class ContentConfig
    {
        public static void RunUpdates()
        {
            //RunUpdates01();


        }

        private static void RunUpdates01()
        {
            var _contentRep = new ContentRepository();

            if (!_contentRep.listAllTags().Any(x => x.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL))
            {
                _contentRep.saveTag(new Tag()
                {
                    ParentID = _contentRep.listAllTags().Single(x => x.SystemTag == Tag.SystemTagOption.SUPER_CAT_ADHOC_TAGS).ID,
                    Type = Tag.TypeOption.ADHOC_PRODUCT_TAGS,
                    SystemTag = Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL,
                    colour = "#ff0000",
                    name = "HOT SPECIALS"
                });
            }
            else {
                var tag = _contentRep.listAllTags().Single(x => x.SystemTag == Tag.SystemTagOption.PRODUCT_CAT_HOT_SPECIAL);
                if(tag.colour != "#ff0000")
                {
                    tag.colour = "#ff0000";
                    _contentRep.saveTag(tag);
                }
            }
        }
    }
}