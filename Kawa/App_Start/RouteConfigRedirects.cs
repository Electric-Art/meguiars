﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Kawa
{
    public class RouteConfigRedirects
    {
        
        public static void RegisterVersionRedirectRoutes(RouteCollection routes)
        {
            //routes.RouteExistingFiles = true;

            List<StoredRedirect> redirects = new List<StoredRedirect>()
            {
                new StoredRedirect(){ inurl="shipping.html", outurl="/articles/89/shipping" },
                new StoredRedirect(){ inurl="about-us.html", outurl="" },
                new StoredRedirect(){ inurl="privacy.html", outurl="/articles/404/privacy" },
                new StoredRedirect(){ inurl="returns.html", outurl="/articles/91/returns" },
                new StoredRedirect(){ inurl="terms-conditions.html", outurl="/articles/405/terms-and-conditions" },
                new StoredRedirect(){ inurl="contact.html", outurl="/contact/" },
                new StoredRedirect(){ inurl="shop-by-brand", outurl="/category/brands?mode=brands" },
                new StoredRedirect(){ inurl="shop-by-use", outurl="/category/conditions?mode=brands" },
                new StoredRedirect(){ inurl="clearance", outurl="/products/clearance" },
                new StoredRedirect(){ inurl="shop-by-category", outurl="/category/supplements" },
            };

            foreach (var redirect in redirects)
            {
                routes.MapRoute(
                    name: "Redirect-" + redirect.inurl.Trim(),
                    url: redirect.inurl.Trim(),
                    defaults: new { controller = "Redirect", action = "Static", id = redirect.outurl.Trim() }
                );
            }

            routes.MapRoute(
                name: "Redirect-Index-Php",
                url: "index.php",
                defaults: new { controller = "Redirect", action = "IndexPhp", id = UrlParameter.Optional }
            );
            
            routes.MapRoute(
                name: "Redirect-Other-Html1",
                url: "{dir1}/{id}.html",
                defaults: new { controller = "Redirect", action = "Html", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Redirect-Other-Html2",
                url: "{dir1}/{dir2}/{id}.html",
                defaults: new { controller = "Redirect", action = "Html", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Redirect-Other-Html3",
                url: "{dir1}/{dir2}/{dir3}/{id}.html",
                defaults: new { controller = "Redirect", action = "Html", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Redirect-Other-Html4",
                url: "{dir1}/{dir2}/{dir3}/{dir4}/{id}.html",
                defaults: new { controller = "Redirect", action = "Html", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Redirect-Other-Html5",
                url: "{dir1}/{dir2}/{dir3}/{dir4}/{dir5}/{id}.html",
                defaults: new { controller = "Redirect", action = "Html", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Redirect-Other-Html6",
                url: "{dir1}/{dir2}/{dir3}/{dir4}/{dir5}/{dir6}/{id}.html",
                defaults: new { controller = "Redirect", action = "Html", id = UrlParameter.Optional }
            );
           
            routes.MapRoute(
                name: "Redirect-Products",
                url: "{id}.html",
                defaults: new { controller = "Redirect", action = "Product", id = UrlParameter.Optional }
            );
            /*
            routes.MapRoute(
                name: "Redirect-Classic-Asp",
                url: "{id}.asp",
                defaults: new { controller = "Redirect", action = "ClassicAsp", id = UrlParameter.Optional }
            );
            */
            routes.MapRoute(
                name: "Redirect-Categories-1",
                url: "shop-by-category/{id}",
                defaults: new { controller = "Redirect", action = "Category", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Redirect-Categories-2",
               url: "shop-by-category/{supercat}/{id}",
               defaults: new { controller = "Redirect", action = "Category", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Redirect-Use-1",
                url: "shop-by-use/{id}",
                defaults: new { controller = "Redirect", action = "Category", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Redirect-Use-2",
               url: "shop-by-use/{supercat}/{id}",
               defaults: new { controller = "Redirect", action = "Category", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Redirect-Use-3",
               url: "shop-by-use/{supercat}/{cat}/{id}",
               defaults: new { controller = "Redirect", action = "Category", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Redirect-Brand-1",
                url: "shop-by-brand/{id}",
                defaults: new { controller = "Redirect", action = "Category", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Redirect-Brand-2",
               url: "shop-by-brand/{supercat}/{id}",
               defaults: new { controller = "Redirect", action = "Category", id = UrlParameter.Optional }
           );




            

            
        }
    }
}