﻿$(function () {

    tinymce.init({
        selector: "textarea.editor",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "emoticons template textcolor colorpicker textpattern imagetools"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons | sizeselect | bold italic | fontselect |  fontsizeselect",
        toolbar2: '',
        content_css: '/content/site.css',
        body_class: 'tinymce-body-override'
    });

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    
    $(".add-picker").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    });

    $(".checksave").click(function () {
        var destination = $(this).attr("href");
        var formclass = $(this).attr("data-formtosubmit");
        bootbox.dialog({
            message: "Would you like to save first?",
            buttons: {
                success: {
                    label: "save",
                    className: "btn-success",
                    callback: function () {
                        if ($("form." + formclass).length)
                        {
                            $("." + formclass).append($("<input type='hidden' name='goto' value='true' />"));
                            $("." + formclass).submit();
                        }
                        else
                        {
                            $("." + formclass).click();
                            setTimeout(function () {
                                window.location = destination;
                            },2000);
                        }
                    }
                },
                danger: {
                    label: "don't save",
                    className: "btn-danger",
                    callback: function () {
                        window.location = destination;
                    }
                },
                main: {
                    label: "cancel",
                    className: "btn-primary",
                    callback: function () {
                        console.log("nothing doing");
                    }
                }
            }
        });

        return false;
    });

    $(".tagsdiv option").each(function () {
        if ($(this).html().indexOf("HIDDEN") == 0)
            $(this).addClass("hidden-tag");
    })

    $(".adminsearch option").each(function () {
        if ($(this).html().indexOf("HIDDEN") == 0)
            $(this).addClass("hidden-tag");
    })

    $(".tagsel.port").change(function () {
        if ($(this).val() == "") return;
        var container = $(this).parents(".tagsdiv");
        var sel = container.find(".tagsel");

        var tag = {
            name: sel.find("option:selected").text(),
            ID: sel.find("option:selected").val()
        };

        if (tag.ID == "")
            return;

        $(container).find(".taglist").append($("<span class='tag' id='taglabel_" + tag.ID + "'>" + tag.name + " <a href='javascript:void(0)' class='removetag' data-tagid='" + tag.ID + "'>x</a></span>"));
        $(container).find(".taglist").append($("<input type='hidden' id='tag_" + tag.ID + "' name='TagIDs' value='" + tag.ID + "' />"));
    });

    $(".addtag").click(function(){
        var container = $(this).parents(".tagsdiv");
        var sel = container.find(".tagsel");

        var tag = {
            name: sel.find("option:selected").text(),
            ID: sel.find("option:selected").val()
        };

        if (tag.ID == "")
            return;

        $(container).find(".taglist").append($("<span class='tag' id='taglabel_" + tag.ID + "'>" + tag.name + " <a href='javascript:void(0)' class='removetag' data-tagid='" + tag.ID + "'>x</a></span>"));
        $(container).find(".taglist").append($("<input type='hidden' id='tag_" + tag.ID + "' name='TagIDs' value='" + tag.ID + "' />"));
    });

    $(".taglist").on("click", ".removetag", function () {
        var tagid = $(this).attr("data-tagid");
        $("#taglabel_" + tagid).remove();
        $("#tag_" + tagid).remove();
    });

    var addProductToList = function(container, product) {
        $(container).append($("<span class='tag' id='productlabel_" + product.ID + "'>" + product.name + " <a href='javascript:void(0)' data-productid='" + product.ID + "' class='removeproduct'>x</a></span>"));
        $(container).append($("<input type='hidden' id='product_" + product.ID + "' name='ProdIDs' value='" + product.ID + "' />"));
    }

    $(".addproduct").click(function () {
        var container = $(".productsdiv");

        var product = {
            name: $("#NewRelatedProductID_Lookup").val(),
            ID: $("#NewRelatedProductID").val()
        };

        if (product.ID == "")
            return;

        addProductToList(container, product);
       
        $("#NewRelatedProductID_Lookup").val("");
        $("#NewRelatedProductID").val("")
    });

    $(".productsdiv").on("click", ".removeproduct", function () {
        var productid = $(this).attr("data-productid");
        $("#productlabel_" + productid).remove();
        $("#product_" + productid).remove();
    });
    
    $(".chk-master").change(function () {
        $(".chk-slave").prop("checked", this.checked);
    });

    $(".product-multiselector").each(function () {
        var lookupsrc = $(this).attr("data-lookup");
        var container = $(".productsdiv");
        var list = $('<ul class="product-multiselector-result"></ul>');
        $(this).after(list);
        rxjs.fromEvent(this, 'keyup')
            .pipe(
                rxjs.operators.map(
                    e => e.target.value // Project the text from the input
                ),
                rxjs.operators.filter(txt => txt.length > 2),   // search only more than 2 text
                rxjs.operators.debounceTime(750 /* Pause for 750ms */),
                rxjs.operators.distinctUntilChanged(),  // Only if the value has changed
                rxjs.operators.switchMap(
                    txt => rxjs.ajax.ajax("/admin/" + lookupsrc + "/?search=" + txt)
                )
            )
            .subscribe(e => {
                list.empty()
                    .append($.map(e.response, data => {
                        var link = $("<a>" + data.name + "</a>");
                        link.click(() => addProductToList(container,data));
                        return $('<li>').append(link)
                    }));
            }, err => {
                list.empty().append($('<li>')).text('Error. ' + err);
            })
    })
})




function addQandA() {
    var lastQAndABlock = $(".qandablock").last().clone();
    var index = Number(lastQAndABlock.attr("data-qindex"));
    lastQAndABlock.attr("data-qindex",index + 1);
    lastQAndABlock.find("input[type=text]").val("");
    lastQAndABlock.find("textarea").val("");

    var html = lastQAndABlock[0].outerHTML;
    html = html.split("_" + index + "__").join("_" + (index + 1) + "__");//.replace("_" + index + "__", "_" + (index + 1) + "__");
    html = html.split("[" + index + "]").join("[" + (index + 1) + "]");//.replace("[" + index + "]", "[" + (index + 1) + "]");
    var newSect = $(html);

    $("#qanda").append(newSect);
}


function siteResourceAction(SiteResourceID, action) {

    if (action == "delete") {
        if (!confirm("Are you sure you want to delete this Image?")) {
            return;
        }
    }

    var obj = { "ID": SiteResourceID, "action": action };
    var post = JSON.stringify(obj);
    $.ajax(
    {
        type: "POST",
        url: "/Admin/SiteResources/",
        data: post,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            $(".siteresourcesinsert").html(result);

        },
        error: function (req, status, error) {
            //setMessage("Error addLink." + error, true);
        },
        returnObj: this
    });
}







function addPage() {

    var container = $(".pagesdiv");

    var pageid = null;

    if ($('#pages').length > 0) {
        pageid = "pages";
    }
    else {
        pageid = "pages";
    }

    var page = {
        name: $("#" + pageid + " option:selected").text(),
        ID: $("#" + pageid + " option:selected").val()

    };

    if (page.ID == "")
        return;

    $(container).append($("<span class='tag' id='pagelabel_" + page.ID + "'>" + page.name + " <a href='javascript:void(0)' onclick='removePage(" + page.ID + ");'>x</a></span>"));
    $(container).append($("<input type='hidden' id='page_" + page.ID + "' name='PageIDs' value='" + page.ID + "' />"));
}


function removePage(ID) {
    $("#pagelabel_" + ID).remove();
    $("#page_" + ID).remove();
}

function imageSave() {
    $("#showedit").val("true");
    $("form").submit();
}


function confirmAction(title, message, callback, err) {
    $("#confirmModalLabel").html(title);
    $("#confirmModal .modal-body").html(message);
    $("#confirmModal .btn-primary").unbind();
    $("#confirmModal [data-dismiss=modal]").unbind();
    $('#confirmModal').modal();
    $("#confirmModal .btn-primary").click(function () {
        $('#confirmModal').modal('hide');
        callback();
    });
    $("#confirmModal [data-dismiss=modal]").click(function () {
        if (err) err();
    });
}