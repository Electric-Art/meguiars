﻿
$(document).ready(function () {
    
    
    toolAccounts();


});

function toolAccounts()
{
    $(".loginbuttonjs").unbind("click");
    $(".registerbuttonjs").unbind("click");


    $(".loginbuttonjs").click(function () {

        $(".loginform").validate();

        if (!$(".loginform").valid())
            return;

        var model = {
            Email: $(".jsaccount .loginbox #Email").val(),
            Password: $(".jsaccount .loginbox #Password").val(),
            js: true
        };

        addFavouriteToModel(model);

        var post = {
            model: model,
            __RequestVerificationToken: $(".jsaccount .loginbox input[name=__RequestVerificationToken]").val()
        };

        $(this).val("Please wait...");

        $.ajax(
        {
            type: "POST",
            url: "/Account/Login/",
            data: post,
            //headers: headers,
            //contentType: 'application/json; charset=utf-8',
            success: function (result) {
                if (result == "logged") {
                    if ($("#accountnextstep").val() == "refreshparent")
                        parent.location.reload();
                    else
                        location.href = $("#accountnextstep").val();
                }
                else
                {
                    $(".loginreplace").html(result);
                    toolAccounts();
                }
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: this
        });

    });

    $(".registerbuttonjs").click(function () {

        $(".registerform").validate();

        if (!$(".registerform").valid())
            return;

        var model = {
            Email: $(".jsaccount .registerbox #Email").val(),
            Password: $(".jsaccount .registerbox #Password").val(),
            ConfirmPassword: $(".jsaccount .registerbox #ConfirmPassword").val(),
            Email: $(".jsaccount .registerbox #Email").val(),
            Password: $(".jsaccount .registerbox #Password").val(),
            ConfirmPassword: $(".jsaccount .registerbox #ConfirmPassword").val(),
            FirstName: $(".jsaccount .registerbox #FirstName").val(),
            LastName: $(".jsaccount .registerbox #LastName").val(),
            Gender: $(".jsaccount .registerbox #Gender").val(),
            DateOfBirthDay: $(".jsaccount .registerbox #DateOfBirthDay").val(),
            DateOfBirthMonth: $(".jsaccount .registerbox #DateOfBirthMonth").val(),
            DateOfBirthYear: $(".jsaccount .registerbox #DateOfBirthYear").val(),
            Channel: $(".jsaccount .registerbox #Channel").val(),
            fullregister: $(".jsaccount .registerbox #fullregister").val(),
            js: true
        };

        addFavouriteToModel(model);

        var post = {
            model: model,
            __RequestVerificationToken: $(".jsaccount .registerbox input[name=__RequestVerificationToken]").val()
        };

        $(this).val("Please wait...");

        $.ajax(
        {
            type: "POST",
            url: "/Account/Register/",
            data: post,
            //headers: headers,
            //contentType: 'application/json; charset=utf-8',
            success: function (result) {
                if (result == "registered") {
                    if ($("#accountnextstep").val() == "refreshparent")
                        parent.location.reload();
                    else
                        location.href = $("#accountnextstep").val();
                }
                else
                {
                    $(".registerreplace").html(result);
                    toolAccounts();
                }
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: this
        });

    });
}

function addFavouriteToModel(model)
{
    if ((typeof favourite != "undefined") && favourite) {
        if (favourite.ProductID)
            model.ProductID = favourite.ProductID;
        if (favourite.PageID)
            model.PageID = favourite.PageID;
    }
}



