﻿$(function () {
    
    $(".searchauto").autocomplete({
        minLength: 3,
        source: function (request, response) {
            var post = {
                search: request.term
            };
            $.ajax(
            {
                type: "POST",
                url: "/home/searchsuggest/",
                data: post,
                //headers: headers,
                //contentType: 'application/json; charset=utf-8',
                success: function (result) {

                    var products = (result.products.length > 0) ? [{ label: "Products", val: "", header: true }].concat(result.products) : [];
                    var pages = (result.pages.length > 0) ? [{ label: "Articles", val: "", header: true }].concat(result.pages) : [];

                    response(products.concat(pages));
                },
                error: function (req, status, error) {
                    //setMessage("Error addLink." + error, true);
                },
                returnObj: this
            });
        },
        select: function (event, ui) {
            if (ui.item.val != "")
                window.location = ui.item.value;

            event.preventDefault();
        },
        open: function (event, ui) {
            if ($(this).hasClass("searchauto1"))
            {
                var position = $("#searchauto1").offset();
                var width = $("#searchauto1").width();
                var autowidth = $("#ui-id-1").width();
                var x = position.left + width - autowidth + 22;

                var y = $(".navbar-fixed-top").height();

                console.log("opened");
                $('.ui-autocomplete').css({ 'left': x + 'px', 'top': y + 'px' });
            }
        }
    });


    var renderftn = function (ul, item) {
        var listItem = $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + item.label + "</a>")
            .appendTo(ul);

        if (item.header) {
            listItem.addClass("header");
        }

        return listItem;
    };

    if ($(".searchauto").data("ui-autocomplete"))
    {
        $(".searchauto").data("ui-autocomplete")._renderItem = renderftn;

        //$(".searchauto2").data("ui-autocomplete")._renderItem = renderftn;
    }
});


