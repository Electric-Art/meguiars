﻿$(function () {

    function setSuburbLabel(formName) {
        var txt = $("#" + formName + "_CountryID").val() == 1 ? "Suburb" : "Suburb/City";
        $("label[for=" + formName + "_Address3]").html(txt);
    }

    $("#CustomerDetails_CountryID").change(function () { setSuburbLabel("CustomerDetails"), handleCountryChange("pac-input-billing", "CustomerDetails_"); })
    $("#DeliveryDetails_CountryID").change(function () { setSuburbLabel("DeliveryDetails"), handleCountryChange("pac-input-delivery", "DeliveryDetails_");  })
    setSuburbLabel("CustomerDetails");
    setSuburbLabel("DeliveryDetails");
    $("#fillDeliveryBtn").change(function () {
        var copy = $('#fillDeliveryBtn').is(':checked');
        if (copy && fillDelivery()) {
            $(".delivery input, .delivery select").prop('readonly', true);
            $(".delivery").addClass("hidden-xs");
        }
        else {
            $(".delivery input, .delivery select").prop('readonly', false);
            $(".delivery").removeClass("hidden-xs");
        }
    });

    if ($("#newsletter").length) {
        $("#newsletter")[0].checked = $("#CustomerDetails_isMailer")[0].checked;
        $("#newsletter").change(function () {
            $("#CustomerDetails_isMailer")[0].checked = $("#newsletter")[0].checked;
        });
    }

    $("#DeliveryInstructionsConfirm").change(function () {
        var checked = $("#DeliveryInstructionsConfirm")[0].checked;
        if (checked)
        {
            $("#DeliveryDetails_LeaveOption")[0].options.item(0).text = "Please select";
            $("#DeliveryDetails_LeaveOption")[0].options.item(0).value = "Please select";
            $("#DeliveryDetails_LeaveOption").attr("disabled", false);
            if ($("#DeliveryDetails_LeaveOption").val() == "Enter your own")
            {
                $("#DeliveryDetails_DeliveryInstructions").show();
                $("#DeliveryDetails_DeliveryInstructions").val("If not home, leave parcel ");
            }
            else {
                $("#DeliveryDetails_DeliveryInstructions").hide();
                $("#DeliveryDetails_DeliveryInstructions").val("");
            }
        }
        else
        {
            resetLeaveSys();
        }
    });

    function resetLeaveSys() {
        $('#DeliveryInstructionsConfirm').prop('checked', false);
        $("#DeliveryDetails_LeaveOption")[0].options.item(0).text = "Accept terms above to make selection";
        $("#DeliveryDetails_LeaveOption")[0].options.item(0).value = "Accept terms above to make selection";
        $("#DeliveryDetails_LeaveOption").val("Accept terms above to make selection");
        $("#DeliveryDetails_LeaveOption").attr("disabled", true);
        $("#DeliveryDetails_DeliveryInstructions").val("");
        $("#DeliveryDetails_DeliveryInstructions").hide();
    }

    $("#DeliveryDetails_LeaveOption").change(function () {
        if ($(this).val() == "Enter your own")
        {
            $("#DeliveryDetails_DeliveryInstructions").show();
            $("#DeliveryDetails_DeliveryInstructions").val("If not home, leave parcel ");
        }
        else
        {
            $("#DeliveryDetails_DeliveryInstructions").hide();
            $("#DeliveryDetails_DeliveryInstructions").val("");
        }
    });

    $(".addreadonly").each(function () {
        $(this).prop("disabled", true);
    });

    $("#CustomerDetails_Postcode").inputFilter(function (value) {

        if ($("#CustomerDetails_isIntOnlyPostcode").val() == 'true')
        {
            return /^\d*$/.test(value);    // Allow digits only, using a RegExp
        }
        else {
            return true;
        }
    });
    $("#DeliveryDetails_Postcode").inputFilter(function (value) {

        if ($("#DeliveryDetails_isIntOnlyPostcode").val() == 'true') {
            return /^\d*$/.test(value);    // Allow digits only, using a RegExp
        }
        else {
            return true;
        }
    });
    var leaveHasChosen = false;

    $(".needsleavemessage").each(function () {
        var vwidth = ($(window).width() > 600 ? 600 : $(window).width() - 50);
        $("#dialogleaveauth").dialog({
            dialogClass: "alert",
            width: vwidth + "px",
            maxWidth: vwidth + "px",
            buttons: [
                {
                    text: "I accept the terms for Authority to Leave Parcel",
                    click: function () {
                        leaveHasChosen = true;
                        $(this).dialog("close");
                    }
                },
                {
                    text: "I do not accept",
                    click: function () {
                        leaveHasChosen = true;
                        $(this).dialog("close");
                        resetLeaveSys();
                    }
                }
            ],
            close: function (event, ui) {
                if (!leaveHasChosen)
                    resetLeaveSys();
            }
        });
        if ($(window).width() <= 767) {
            var wig = $(".ui-dialog.ui-widget");
            wig.css("top", (wig.offset().top + 150) + "px");
        }
        $('.ui-dialog').find('.ui-button.ui-button-text-only').addClass('formbuttonforced');
        $($('.ui-dialog').find('.ui-button.ui-button-text-only')[1]).addClass('formbuttonred');
    });


    $(".statetxt").each(function () {
        var block = $(this).parents(".accountdetail");
        var countrySel = block.find(".countryid");
        var stateSel = block.find(".statesel");
        var stateTxt = block.find(".statetxt");
        var runSwaps = function () {
            if (countrySel.val() == AustraliaID) {
                stateTxt.hide();
                stateSel.show();
                stateSel.val(stateTxt.val());
            }
            else {
                stateTxt.show();
                stateSel.hide();
            }
        }

        countrySel.change(function () {
            stateTxt.val("");
            runSwaps();
        });

        stateSel.change(function () {
            stateTxt.val(stateSel.val());
        });

        runSwaps();
    });


    var fillDelivery = function () {
        var preDeliveryCountry = $("#DeliveryDetails_CountryID").val();
        var toDeliveryCountry = $("#CustomerDetails_CountryID").val();
        var isAussi = $("#DeliveryDetails_CountryID").val() == 1;
        if (isAussi)
        {
            var preDeliveryState = $("#DeliveryDetails_Address4").val();
            var toDeliveryState = $("#CustomerDetails_Address4_Options").val();
            var isAussiPostcodeOrder = $("#DeliveryDetails_Postcode").attr("type") == "hidden";
            if (isAussiPostcodeOrder)
            {
                var preDeliveryPostCode = $("#DeliveryDetails_Postcode").val();
                var toDeliveryPostCode = $("#CustomerDetails_Postcode").val();
            }
        }

        var isAttemptedShippingChange = (!isAussi && preDeliveryCountry != toDeliveryCountry) ||
                                        (isAussi && !isAussiPostcodeOrder && (preDeliveryCountry != toDeliveryCountry || preDeliveryState != toDeliveryState)) ||
                                        (isAussi && isAussiPostcodeOrder && (preDeliveryCountry != toDeliveryCountry || preDeliveryState != toDeliveryState || preDeliveryPostCode != toDeliveryPostCode));


        if (isAttemptedShippingChange) {
            ///var appliesto = isAussi ? (isAussiPostcodeOrder ? "country, state or postcode" : "country or state") : "country";


            $.ajax(
                {
                    type: "GET",
                    url: "/Shop/GetAccountDetailStateChangeMessage/",
                    contentType: 'application/json; charset=utf-8',
                    success: function (result) {
                        $('.modalmessager .inner-content').html(result);
                        $('.modalmessager').modal({ show: true });
                        $('#fillDeliveryBtn').prop("checked", false)
                    },
                    error: function (req, status, error) {
                        console.log("ajax issue Change state");
                    }
                });


           
            
            return false;
        }

        $("#DeliveryDetails_FirstName").val($("#CustomerDetails_FirstName").val());
        $("#DeliveryDetails_Surname").val($("#CustomerDetails_Surname").val());
        $("#DeliveryDetails_Company").val($("#CustomerDetails_Company").val());
        $("#DeliveryDetails_Address1").val($("#CustomerDetails_Address1").val());
        $("#DeliveryDetails_Address2").val($("#CustomerDetails_Address2").val());
        $("#DeliveryDetails_Address3").val($("#CustomerDetails_Address3").val());
        if (!isAussi)
        {
            $("#DeliveryDetails_Address4").val($("#CustomerDetails_Address4").val());
            $("#DeliveryDetails_Address4_Options").val($("#CustomerDetails_Address4_Options").val());
        }
        $("#DeliveryDetails_Postcode").val($("#CustomerDetails_Postcode").val());
        $("#DeliveryDetails_Email").val($("#CustomerDetails_Email").val());
        $("#DeliveryDetails_Mobile").val($("#CustomerDetails_Mobile").val());

        return true;
    };

    

});


$(function () {
    $('.detailsform').submit(function () {
        if ($(this).valid()) {
            //gaStatWrite();
        } else {
            console.log('the form is not valid');
        }
    });
});

function countryIDToCode(countryID) {
    for (var i = 0; i < countries.length; i++) {
        if (countries[i].ID == countryID)
            return countries[i].Code;
    }
    return null;
}

function stateCodeToID(stateCode) {
    for (var i = 0; i < states.length; i++) {
        if (states[i].Code == stateCode)
            return states[i].ID;
    }
    return null;
}
(function ($) {
    $.fn.inputFilter = function (inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {

            
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
           
        });
    };
}(jQuery));


function handleCountryChange(inputId, viewPrefix) {
    ///console.log("switch country activated");
    const input = document.getElementById(inputId);
    var lookuprow = '.' + viewPrefix + 'Lookup';

    var isAdmin = window.location.pathname.toLowerCase().indexOf("/admin") == 0;
    
   
    $.ajax(
        {
            type: "GET",
            url: "/Shop/GetCountry/" + $("#" + viewPrefix + "CountryID").val(),
            contentType: 'application/json; charset=utf-8',
            success: function (result) {

                /*
                if (result.country.isAddressSearch == "false") {
                    $(lookuprow).hide();
                } else {
                    const options = {
                        componentRestrictions: { country: countryIDToCode($("#" + viewPrefix + "CountryID").val()) },
                        fields: ["address_components", "geometry", "icon", "name", "type"],
                        strictBounds: false,
                    };
                    SetupAddressLookup(input, options, viewPrefix);
                    $(lookuprow).show();
                } */
                $(lookuprow).hide();

                if (result.country.isIntPostCode) {
                    if (!isAdmin) {
                        var stripped = $('#' + viewPrefix + "Postcode").val().replace(/\D/g, '');///remove any non numbers
                        $('#' + viewPrefix + "Postcode").val(stripped);
                        ////$('#' + viewPrefix + "Postcode").val('reset by me');////reset the postcode field incase they have invalid entries
                    }
                    
                }
                
                $("#" + viewPrefix + "isIntOnlyPostcode").val(result.country.isIntPostCode);

            },
            error: function (req, status, error) {
                console.log("ajax issue Change state");
            }
        });

   
}

function alertStateChange() {

    $.ajax(
        {
            type: "GET",
            url: "/Shop/GetStateChangeMessage/",
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                if (!result) {
                    location.reload();
                    return;
                }
                $('.modalmessage .inner-content').html(result);
                $('.modalmessage').modal({ show: true });
            },
            error: function (req, status, error) {
                console.log("ajax issue Change state");
            }
        });
}


function handleAddressLookup(inputId, viewPrefix) {
    const input = document.getElementById(inputId);
    const options = {
        componentRestrictions: { country: countryIDToCode($("#" + viewPrefix + "CountryID").val()) },
        fields: ["address_components", "geometry", "icon", "name","type" ],
        strictBounds: false,
    };
    SetupAddressLookup(input, options, viewPrefix);
    
}


function SetupAddressLookup(input, options, viewPrefix) {
    console.log(options);


    const autocomplete = new google.maps.places.Autocomplete(input, options);
    /*
    autocomplete.setComponentRestrictions({
        country: ["us", "pr", "vi", "gu", "mp"],
    });
    */

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        var street = "";
        var suburb_city = "";
        var state = "";
        var postcode = "";
        var country = "";
        var company = "";
        var suburb = "";
        ///console.log(place);

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        // place.address_components are google.maps.GeocoderAddressComponent objects
        // which are documented at http://goo.gle/3l5i5Mr
        
        for (const type of place.types) {
            switch (type) {
                case "establishment": {
                    company = place.name;
                    break;
                }
            }
        }

        for (const component of place.address_components) {
            const componentType = component.types[0];
           ////console.log(componentType);

            switch (componentType) {

                case "subpremise": {
                    street = component.long_name + "/";
                    break;
                }

                case "street_number": {
                    street += component.long_name;
                    break;
                }

                case "route": {
                    street += " " + component.long_name;
                    break;
                }

                case "postal_code": {
                    postcode = component.long_name;
                    break;
                }

                case "postal_code_suffix": {
                    postcode += component.long_name;
                    break;
                }

                case "sublocality_level_1": {
                    suburb += component.long_name;
                    break;
                }

                case "locality": {
                    suburb_city += component.long_name;
                    break;
                }

                case "administrative_area_level_1": {
                    state = component.short_name;
                    break;
                }

                case "country": {
                    country = component.long_name;
                    break;
                }
         
            }
        }
        var gochange = true;


        var isAdmin = window.location.pathname.toLowerCase().indexOf("/shop") != 0;
      

        if (viewPrefix == "DeliveryDetails_" && country == "Australia") {
                ///check for state changes as this may affect the delivery costs
                if (postcode != $("#" + viewPrefix + "Postcode").val()) {
                    gochange = false;
                    if (!isAdmin) { alertStateChange(); } else { alert("Post code change detected!\n\nThe address selected does not match the selected shipping postcode");}
                }
            }
        


        if (gochange) {

            $("#" + viewPrefix + "Company").val(company);
            $("#" + viewPrefix + "Address1").val(street);

            $("#" + viewPrefix + "Address2").val(suburb);
            
            $("#" + viewPrefix + "Address3").val(suburb_city);
            $("#" + viewPrefix + "Postcode").val(postcode);
            
            if (country == "Australia") {
                $("#" + viewPrefix + "Address4_Options").val(stateCodeToID(state));
                $("#" + viewPrefix + "Address4").val(stateCodeToID(state));
            }
            else {
                $("#" + viewPrefix + "Address4").val(state);
            }
        }
        $(input).val('');///reset the search input box
        // $("#" + viewPrefix + "Address3").val(street);
        // console.log(address1);
        // After filling the form with address components from the Autocomplete
        // prediction, set cursor focus on the second address line to encourage
        // entry of subpremise information such as apartment, unit, or floor number.
        //address2Field.focus();
    }
    

    autocomplete.addListener("place_changed", fillInAddress);
}

function initMap() {
    ////console.log("places api ready")
    ///handleAddressLookup("pac-input-billing", "CustomerDetails_");
    ///handleAddressLookup("pac-input-delivery", "DeliveryDetails_");


    handleCountryChange("pac-input-billing", "CustomerDetails_");
    handleCountryChange("pac-input-delivery", "DeliveryDetails_"); 
}
