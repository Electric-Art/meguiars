﻿$(function () {

    var OrderID = $("#ChangeOrderID").val();


    $(".admin-addproduct input[type=button]").click(function () {
        var formObj = $(this).parents(".admin-addproduct");
        if (formObj.find("#NewProduct").val() == "")
            return;

        var iid = $(this).attr("data-basketID");
        var model = {
            ProductID: formObj.find("#NewProduct").val(),
            Quantity: formObj.find("#QuantityNew").val()
        };
        model.OrderID = OrderID;

        var post = JSON.stringify(model);
        $.ajax(
        {
            type: "POST",
            url: "/admin/orders/AddToBasket/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".basket > div").html(result);
                $(".admin-addproduct input[type=text]").val("");
                $(".admin-addproduct input[type=text]").change();
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            }
        });
    });

    $(".admin-deleteshipment").click(function () {

        var iid = $(this).attr("data-shipment-id");
        var model = {
            ShipmentID: iid
        };

        var shipDev = $(this).parent();
        bootbox.confirm("Are you sure you want to delete this shipment?", function (result) {
            if (!result) return;
            var post = JSON.stringify(model);
            $.ajax(
            {
                type: "POST",
                url: "/admin/orders/DeleteShipment/",
                data: post,
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    shipDev.remove();
                },
                error: function (req, status, error) {
                    //setMessage("Error addLink." + error, true);
                }
            });
        });
        return false;
    });

    $(".basket").on("click",".item-editprice-btn",function () {
        var iid = $(this).attr("data-basketID");

        var itemPriceField = $("input[data-basketid=" + iid + "].item-editprice");
        var isvis = itemPriceField.is(":visible");
        if (!isvis)
        {
            itemPriceField.show();
            $(this).html("update");
            return false;
        }

        var model = {
            basketid: iid,
            price: itemPriceField.val()
        };

        var post = JSON.stringify(model);
        $.ajax(
        {
            type: "POST",
            url: "/admin/orders/ChangeBasketPrice/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".basket > div").html(result);
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            }
        });
        return false;
    });
});