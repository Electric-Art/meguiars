﻿kawaApp.directive('hud', function () {
    function link(scope, element, attrs) {

        scope.$watch("actionMessage.on", function (value) {
            if (value)
            {
                element.show();
            }
            else
            {
                element.hide();
            }
        });

        scope.getLook = function () {
            return scope.actionMessage.status == null ? "action" : (scope.actionMessage.status ? "success" : "fail");
        };

        scope.getActionLook = function (action) {
            return action.look;
        };

        scope.runAction = function (action) {
            action.action();
        };

    }
    return {
        templateUrl: '/scripts/ang/views/hud.html',
        link: link
    };
});
