﻿kawaApp.controller('SettingsController', ['$scope', '$http', '$attrs', function ($scope, $http, $attrs) {
    $scope.actionMessage = { on: false };

    $scope.getSettings = function (setting, value) {

        $http.post("/admin/admin/getsettings/", null).
            success(function (data, status, headers, config) {
                $scope.settings = data;
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/admin/storesetting/");
            });

    }

    $scope.storeSetting = function (setting,value) {
        var model = { "setting": setting, "value": value};
        $http.post("/admin/admin/storesetting/", model).
            success(function (data, status, headers, config) {
                if (!data.success) {
                    $scope.actionMessage = {
                        on: true,
                        message: "There were problems. " + data.message,
                        status: false,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.actionMessage = { on: false, status: null };
                                }
                            }
                        ]
                    }
                }
                else {
                    $scope.actionMessage = {
                        on: true,
                        message: "Your updates have completed successfully.",
                        status: true,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.actionMessage = { on: false, status: null };
                                }
                            }
                        ]
                    }
                }
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/admin/storesetting/");
            });

    }

    $scope.getSettings();

}]);