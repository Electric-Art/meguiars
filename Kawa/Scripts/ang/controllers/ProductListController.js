﻿kawaApp.controller('ProductListController', ['$scope', '$http', '$window', function ($scope, $http, $window) {
    $scope.actionMessage = { on: false };
    $scope.totalProducts = 0;
    $scope.pagination = {};
    $scope.disclaimers = disclaimers;
    $scope.PageSystemType = {
        PRODUCT_DISCLAIMER_THERAPEUTIC : 10,
        PRODUCT_DISCLAIMER_EXTRA_WARNINGS : 11,
        PRODUCT_DISCLAIMER_WEIGHT_MANAGEMENT : 12,
        PRODUCT_DISCLAIMER_FOOD : 13,
        PRODUCT_DISCLAIMER_COSMETICS : 14
    }
    //$scope.productsPerPage = 25;
    $scope.bulk = {
        on: false,
        single: { quanity: 1 },
        volumePrices: []
    };
    $scope.bulkgoogcat = {
        on: false,
        googleCat: null,
        error:false
    };
    $scope.bulkbingcat = {
        on: false,
        bingCat: null,
        error: false
    };
    $scope.bulkshipdays = {
        on: false,
        shipDays: null,
        shipDaysMinimum: null,
        error: false
    };
    $scope.bulkhideblurb = {
        on: false,
        hideBlurb: false,
        error: false
    };
    $scope.bulkdisclaimers = {
        on: false,
        pageIds: [$scope.disclaimers[0].id],
        error: false
    };
    $scope.bulkoutofstock = {
        on: false,
        outOfStock: true,
        error: false
    };

    $scope.selectstate = { all: false };

    $scope.pageSizes = [10, 20, 30, 40, 50, 60, 70, 80, 100, 150, 200];
    $scope.productsPerPage = $scope.pageSizes[1];

    $scope.$watch('selectstate.all', function (newValue, oldValue) {
        if (newValue == oldValue)
            return;
        angular.forEach($scope.products, function (value, key) {
            value.selected = newValue;
        });
    });

    $scope.addBulk = function () {
        $scope.bulk.volumePrices.push({});
    };

    $scope.deleteBulk = function (price) {
        $scope.bulk.volumePrices.splice($scope.bulk.volumePrices.indexOf(price), 1);
    };

    $scope.toggleBulkUpdate = function () {
        $scope.bulk.on = !$scope.bulk.on;
    };

    $scope.runBulkUpdate = function () {
        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.variant.ID);
                $scope.updateNames.push(value.name);
            }
        });
        $scope.actionMessage = {
            on: true,
            message: "Your bulk update will be run on <b>" + $scope.updateNames.length + "</b> products.",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        var model = $scope.bulk;
                        model.variantIds = $scope.selectedIds;
                        $http.post("/admin/products/runBulkUpdate/", model).
                        success(function (data, status, headers, config) {
                            if (!data.success) {
                                $scope.errorList = data.errorList;
                                $scope.actionMessage = {
                                    on: true,
                                    message: "There were problems. ",
                                    status: false,
                                    actions: [
                                       {
                                           text: "ok", look: "confirm", action: function () {
                                               $scope.actionMessage = { on: false, status: null };
                                           }
                                       }
                                    ]
                                }
                            }
                            else {
                                $scope.errorList = [];
                                $scope.toggleBulkUpdate();
                                $scope.getProducts($scope.pagination.current);
                                $scope.actionMessage = {
                                    on: true,
                                    message: "Your updates have completed successfully.",
                                    status: true,
                                    actions: [
                                        {
                                            text: "ok", look: "confirm", action: function () {
                                                $scope.actionMessage = { on: false, status: null };
                                            }
                                        }
                                    ]
                                }
                            }
                        }).
                        error(function (data, status, headers, config) {
                            console.log("api failure - " + "/admin/products/runBulkUpdate/");
                        });
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                    }
                }
            ]
        }
    };

    $scope.testChangeAll = function () {
        angular.forEach($scope.products, function (value, key) {
            value.changed = true;
        });
        $scope.displayUpdates();
    };

    $scope.displayUpdates = function () {
        $scope.updates = [];
        $scope.updateNames = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.changed)
            {
                if (!(value.variant.ID > 0))
                    value.variant.name = value.name;
                $scope.updates.push({
                    ID: value.ID,
                    name: value.name,
                    variants: [value.variant]
                });
                $scope.updateNames.push(value.name);
            }
        });

        if ($scope.updates.length == 0)
        {
            $scope.actionMessage = { on: false, status: null };
            return;
        }

        $scope.actionMessage = {
            on: true,
            message: "Price updates will be made to the following products - <b>" + $scope.updateNames.join() + "</b>.",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        $scope.sendUpdates();
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                        $scope.getProducts($scope.pagination.current);
                    }
                }
            ]
        }
    };

    $scope.sendUpdates = function () {
        var model = $scope.updates;
        $http.post("/admin/products/saveproductchanges/", model).
        success(function (data, status, headers, config) {
            if(!data.success) {
                $scope.actionMessage = {
                    on: true,
                    message: "There were problems. " + data.message,
                    status: false,
                    actions: [
                       {
                           text: "ok", look: "confirm", action: function () {
                               $scope.displayUpdates();
                           }
                       }
                    ]
                }
            }
            else {
                $scope.actionMessage = {
                    on: true,
                    message: "Your updates have completed successfully.",
                    status: true,
                    actions: [
                        {
                            text: "ok", look: "confirm", action: function () {
                                $scope.actionMessage = { on: false, status: null };
                                //Refresh list but to same page
                                $scope.getProducts($scope.pagination.current);
                            }
                        }
                    ]
                }
            }
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/products/saveproductchanges/");
        });

    }

    $scope.toggleBulkGoogleUpdate = function () {
        $scope.bulkgoogcat.on = !$scope.bulkgoogcat.on;
    };

    $scope.runBulkGoogleCatUpdate = function () {

        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.name);
            }
        });
        $scope.actionMessage = {
            on: true,
            message: "Your bulk google category assignment will be run on <b>" + $scope.updateNames.length + "</b> product(s).",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        var model = $scope.bulkgoogcat;
                        model.productIds = $scope.selectedIds;
                        $http.post("/admin/products/runBulkGoogleCatAssignment/", model).
                        success(function (data, status, headers, config) {
                            if (!data.success) {
                                $scope.bulkgoogcat.error = data.error;
                                $scope.actionMessage = {
                                    on: true,
                                    message: "There were problems. ",
                                    status: false,
                                    actions: [
                                       {
                                           text: "ok", look: "confirm", action: function () {
                                               $scope.actionMessage = { on: false, status: null };
                                           }
                                       }
                                    ]
                                }
                            }
                            else {
                                $scope.bulkgoogcat.error = null;
                                $scope.toggleBulkGoogleUpdate();
                                $scope.getProducts($scope.pagination.current);
                                $scope.actionMessage = {
                                    on: true,
                                    message: "Your updates have completed successfully.",
                                    status: true,
                                    actions: [
                                        {
                                            text: "ok", look: "confirm", action: function () {
                                                $scope.actionMessage = { on: false, status: null };
                                            }
                                        }
                                    ]
                                }
                            }
                        }).
                        error(function (data, status, headers, config) {
                            console.log("api failure - " + "/admin/products/runBulkGoogleCatAssignment/");
                        });
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                    }
                }
            ]
        }
    };

    $scope.toggleBulkBingUpdate = function () {
        $scope.bulkbingcat.on = !$scope.bulkbingcat.on;
    };

    $scope.runBulkBingCatUpdate = function () {

        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.name);
            }
        });
        $scope.actionMessage = {
            on: true,
            message: "Your bulk bing category assignment will be run on <b>" + $scope.updateNames.length + "</b> product(s).",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        var model = $scope.bulkbingcat;
                        model.productIds = $scope.selectedIds;
                        $http.post("/admin/products/runBulkBingCatAssignment/", model).
                            success(function (data, status, headers, config) {
                                if (!data.success) {
                                    $scope.bulkbingcat.error = data.error;
                                    $scope.actionMessage = {
                                        on: true,
                                        message: "There were problems. ",
                                        status: false,
                                        actions: [
                                            {
                                                text: "ok", look: "confirm", action: function () {
                                                    $scope.actionMessage = { on: false, status: null };
                                                }
                                            }
                                        ]
                                    }
                                }
                                else {
                                    $scope.bulkbingcat.error = null;
                                    $scope.toggleBulkBingUpdate();
                                    $scope.getProducts($scope.pagination.current);
                                    $scope.actionMessage = {
                                        on: true,
                                        message: "Your updates have completed successfully.",
                                        status: true,
                                        actions: [
                                            {
                                                text: "ok", look: "confirm", action: function () {
                                                    $scope.actionMessage = { on: false, status: null };
                                                }
                                            }
                                        ]
                                    }
                                }
                            }).
                            error(function (data, status, headers, config) {
                                console.log("api failure - " + "/admin/products/runBulkBingCatAssignment/");
                            });
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                    }
                }
            ]
        }
    };

    $scope.toggleBulkShipDaysUpdate = function () {
        $scope.bulkshipdays.on = !$scope.bulkshipdays.on;
    };

    $scope.runBulkShipDaysUpdate = function () {

        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.name);
            }
        });
        $scope.actionMessage = {
            on: true,
            message: "Your bulk ship days assignment will be run on <b>" + $scope.updateNames.length + "</b> product(s).",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        var model = $scope.bulkshipdays;
                        model.productIds = $scope.selectedIds;
                        $http.post("/admin/products/runBulkShipDaysAssignment/", model).
                        success(function (data, status, headers, config) {
                            if (!data.success) {
                                $scope.bulkshipdays.error = data.error;
                                $scope.actionMessage = {
                                    on: true,
                                    message: "There were problems. ",
                                    status: false,
                                    actions: [
                                       {
                                           text: "ok", look: "confirm", action: function () {
                                               $scope.actionMessage = { on: false, status: null };
                                           }
                                       }
                                    ]
                                }
                            }
                            else {
                                $scope.bulkshipdays.error = null;
                                $scope.toggleBulkShipDaysUpdate();
                                $scope.getProducts($scope.pagination.current);
                                $scope.actionMessage = {
                                    on: true,
                                    message: "Your updates have completed successfully.",
                                    status: true,
                                    actions: [
                                        {
                                            text: "ok", look: "confirm", action: function () {
                                                $scope.actionMessage = { on: false, status: null };
                                            }
                                        }
                                    ]
                                }
                            }
                        }).
                        error(function (data, status, headers, config) {
                            console.log("api failure - " + "/admin/products/runBulkShipDaysAssignment/");
                        });
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                    }
                }
            ]
        }
    };

    $scope.toggleBulkDisclaimerUpdate = function () {
        $scope.bulkdisclaimers.on = !$scope.bulkdisclaimers.on;
    };

    $scope.runBulkDisclaimersUpdate = function () {
        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.name);
            }
        });
        $scope.actionMessage = {
            on: true,
            message: "Your bulk warning type(s) assignment will be run on <b>" + $scope.updateNames.length + "</b> product(s).",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        var model = $scope.bulkdisclaimers;
                        model.productIds = $scope.selectedIds;
                        $http.post("/admin/products/runBulkDisclaimersAssignment/", model).
                            success(function (data, status, headers, config) {
                                if (!data.success) {
                                    $scope.bulkdisclaimers.error = data.error;
                                    $scope.actionMessage = {
                                        on: true,
                                        message: "There were problems. ",
                                        status: false,
                                        actions: [
                                            {
                                                text: "ok", look: "confirm", action: function () {
                                                    $scope.actionMessage = { on: false, status: null };
                                                }
                                            }
                                        ]
                                    }
                                }
                                else {
                                    $scope.bulkdisclaimers.error = null;
                                    $scope.toggleBulkDisclaimerUpdate();
                                    $scope.getProducts($scope.pagination.current);
                                    $scope.actionMessage = {
                                        on: true,
                                        message: "Your updates have completed successfully.",
                                        status: true,
                                        actions: [
                                            {
                                                text: "ok", look: "confirm", action: function () {
                                                    $scope.actionMessage = { on: false, status: null };
                                                }
                                            }
                                        ]
                                    }
                                }
                            }).
                            error(function (data, status, headers, config) {
                                console.log("api failure - " + "/admin/products/runBulkDisclaimersUpdate/");
                            });
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                    }
                }
            ]
        }
    };

    $scope.getDisclaimerColourStyle = function (disclaimer) {
        switch (disclaimer.SystemPage) {
            case $scope.PageSystemType.PRODUCT_DISCLAIMER_THERAPEUTIC:
                return { 'background-color': '#fce03f', color:'#000' };
            case $scope.PageSystemType.PRODUCT_DISCLAIMER_EXTRA_WARNINGS:
                return { 'background-color': '#fcb93f', color: '#000' };
            case $scope.PageSystemType.PRODUCT_DISCLAIMER_WEIGHT_MANAGEMENT:
                return { 'background-color': '#d63ffc', color: '#FFF' };
            case $scope.PageSystemType.PRODUCT_DISCLAIMER_FOOD:
                return { 'background-color': '#00FF00', color: '#000' };
            case $scope.PageSystemType.PRODUCT_DISCLAIMER_COSMETICS:
                return { 'background-color': '#ff69b4', color: '#FFF' };
        }
    };

    $scope.toggleBulkOutofstockUpdate = function () {
        $scope.bulkoutofstock.on = !$scope.bulkoutofstock.on;
    };

    $scope.runBulkOutofstockUpdate = function () {
        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.name);
            }
        });
        $scope.actionMessage = {
            on: true,
            message: "Your bulk out of stock assignment will be run on <b>" + $scope.updateNames.length + "</b> product(s).",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        var model = $scope.bulkoutofstock;
                        model.productIds = $scope.selectedIds;
                        $http.post("/admin/products/runBulkOutofstockAssignment/", model).
                            success(function (data, status, headers, config) {
                                if (!data.success) {
                                    $scope.bulkoutofstock.error = data.error;
                                    $scope.actionMessage = {
                                        on: true,
                                        message: "There were problems. ",
                                        status: false,
                                        actions: [
                                            {
                                                text: "ok", look: "confirm", action: function () {
                                                    $scope.actionMessage = { on: false, status: null };
                                                }
                                            }
                                        ]
                                    }
                                }
                                else {
                                    $scope.bulkoutofstock.error = null;
                                    $scope.toggleBulkOutofstockUpdate();
                                    $scope.getProducts($scope.pagination.current);
                                    $scope.actionMessage = {
                                        on: true,
                                        message: "Your updates have completed successfully.",
                                        status: true,
                                        actions: [
                                            {
                                                text: "ok", look: "confirm", action: function () {
                                                    $scope.actionMessage = { on: false, status: null };
                                                }
                                            }
                                        ]
                                    }
                                }
                            }).
                            error(function (data, status, headers, config) {
                                console.log("api failure - " + "/admin/products/runBulkOutofstockUpdate/");
                            });
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                    }
                }
            ]
        }
    };

    $scope.getProducts = function (newPage,csv) {
        if (!newPage)
            $scope.pagination.current = 1;

        var filterModel = {
            TagID: $("#TagID").val(),
            BrandID: $("#BrandID").val(),
            search: $("#search").val(),
            searchKeyword: $("#searchKeyword").val(),
            outOfStock: $("#outOfStock").val(),
            preOrder: $("#preOrder").val(),
            offline: $("#offline").val(),
            googleCatStatus: $("#googleCatStatus").val(),
            bingCatStatus: $("#bingCatStatus").val(),
            hideBlurb: $("#hideBlurb").val(),
            austL: $("#austL").val(),
            DisclaimerID: $("#DisclaimerID").val(),
            stockNoStatus: $("#stockNoStatus").val(),
            quanitytRestriction: $("#quanitytRestriction").val(),
            page: newPage,
            pageSize: $scope.productsPerPage,
            csv: csv || false
        }
        $scope.loading = true;
        $http.post("/admin/products/getproductlist/", filterModel).
        success(function (data, status, headers, config) {
            $scope.loading = false;
            if (csv) {
                $window.open("data:text/csv;charset=utf-8," + encodeURIComponent(data));
            }
            else {
                $scope.products = data.list;
                $scope.totalProducts = data.count;
            }     
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/products/getproductlist/");
        });
    };

    $scope.getProducts(null);

    $scope.pageChanged = function (newPage) {
        $scope.getProducts(newPage);
    };

    $scope.updateMargin = function (item) {
        item.variant.priceMargin = ((item.variant.priceRRP - item.variant.price) / item.variant.priceRRP) * 100;
        item.changed = true;
        $scope.displayUpdates();
    };

    $scope.updatePrice = function (item) {
        item.variant.price = item.variant.priceRRP - (item.variant.priceMargin * item.variant.priceRRP / 100);
        item.changed = true;
        $scope.displayUpdates();
    };

    $scope.updateRRP = function (item) {
        item.variant.price = (item.variant.priceRRP - (item.variant.priceRRP * item.variant.priceMargin / 100));
        item.changed = true;
        $scope.displayUpdates();
    };

   
    $scope.modelErrHasStr = function (errorListStr) {
        if (angular.isUndefined($scope.errorList)) return false;
        if (angular.isUndefined($scope.errorList[errorListStr])) return false;
        if ($scope.errorList[errorListStr].length == 0) return false;
        return true;
    }

    $scope.modelErrMsgStr = function (errorListStr) {
        if (angular.isUndefined($scope.errorList)) return "";
        if (angular.isUndefined($scope.errorList[errorListStr])) return "";
        if ($scope.errorList[errorListStr].length == 0) return "";
        return $scope.errorList[errorListStr].join();
    }

    $scope.updateGtin = function (item) {
        item.changed = true;
        $scope.displayGtinUpdates();
    };

    $scope.displayGtinUpdates = function () {
        $scope.updatesGtin = [];
        $scope.updateNamesGtin = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.changed) {
                $scope.updatesGtin.push({ id: value.ID, name: value.name, gtin: value.gtin });
                $scope.updateNamesGtin.push(value.name);
            }
        });

        if ($scope.updatesGtin.length == 0) {
            $scope.actionMessage = { on: false, status: null };
            return;
        }

        $scope.actionMessage = {
            on: true,
            message: "Gtin updates will be made to the following products - <b>" + $scope.updateNamesGtin.join() + "</b>.",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        $scope.sendGtinUpdates();
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                        $scope.getProducts($scope.pagination.current);
                    }
                }
            ]
        }
    };

    $scope.sendGtinUpdates = function () {
        var model = $scope.updatesGtin;
        $http.post("/admin/products/saveproductgtinupdates/", model).
        success(function (data, status, headers, config) {
            if (!data.success) {
                $scope.actionMessage = {
                    on: true,
                    message: "There were problems. " + data.message,
                    status: false,
                    actions: [
                       {
                           text: "ok", look: "confirm", action: function () {
                               $scope.displayGtinUpdates();
                           }
                       }
                    ]
                }
            }
            else {
                $scope.actionMessage = {
                    on: true,
                    message: "Your updates have completed successfully.",
                    status: true,
                    actions: [
                        {
                            text: "ok", look: "confirm", action: function () {
                                $scope.actionMessage = { on: false, status: null };
                                $scope.getProducts($scope.pagination.current);
                            }
                        }
                    ]
                }
            }
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/products/saveproductgtinupdates/");
        });

    }

    $scope.updateAustL = function (item) {
        item.changed = true;
        $scope.displayAustLUpdates();
    };

    $scope.displayAustLUpdates = function () {
        $scope.updatesAustL = [];
        $scope.updateNamesAustL = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.changed) {
                $scope.updatesAustL.push({ id: value.ID, name: value.name, austL: value.austL });
                $scope.updateNamesAustL.push(value.name);
            }
        });

        if ($scope.updatesAustL.length == 0) {
            $scope.actionMessage = { on: false, status: null };
            return;
        }

        $scope.actionMessage = {
            on: true,
            message: "Aust L updates will be made to the following products - <b>" + $scope.updateNamesAustL.join() + "</b>.",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        $scope.sendAustLUpdates();
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                        $scope.getProducts($scope.pagination.current);
                    }
                }
            ]
        }
    };

    $scope.sendAustLUpdates = function () {
        var model = $scope.updatesAustL;
        $http.post("/admin/products/saveproductaustlupdates/", model).
            success(function (data, status, headers, config) {
                if (!data.success) {
                    $scope.actionMessage = {
                        on: true,
                        message: "There were problems. " + data.message,
                        status: false,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.displayAustLUpdates();
                                }
                            }
                        ]
                    }
                }
                else {
                    $scope.actionMessage = {
                        on: true,
                        message: "Your updates have completed successfully.",
                        status: true,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.actionMessage = { on: false, status: null };updateAustL
                                    $scope.getProducts($scope.pagination.current);
                                }
                            }
                        ]
                    }
                }
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/products/saveproductaustlupdates/");
            });

    }

    $scope.updateStockNo = function (item) {
        item.changed = true;
        $scope.displayStockNoUpdates();
    };

    $scope.displayStockNoUpdates = function () {
        $scope.updatesStockNo = [];
        $scope.updateNamesStockNo = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.changed) {
                $scope.updatesStockNo.push({ id: value.ID, name: value.name, VariantStockNo: value.VariantStockNo });
                $scope.updateNamesStockNo.push(value.name);
            }
        });

        if ($scope.updatesStockNo.length == 0) {
            $scope.actionMessage = { on: false, status: null };
            return;
        }

        $scope.actionMessage = {
            on: true,
            message: "Stock no updates will be made to the following products - <b>" + $scope.updateNamesStockNo.join() + "</b>.",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        $scope.sendStockNoUpdates();
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                        $scope.getProducts($scope.pagination.current);
                    }
                }
            ]
        }
    };

    $scope.sendStockNoUpdates = function () {
        var model = $scope.updatesStockNo;
        $http.post("/admin/products/saveproductstocknolupdates/", model).
            success(function (data, status, headers, config) {
                if (!data.success) {
                    $scope.actionMessage = {
                        on: true,
                        message: "There were problems. " + data.message,
                        status: false,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.displayStockNoUpdates();
                                }
                            }
                        ]
                    }
                }
                else {
                    $scope.actionMessage = {
                        on: true,
                        message: "Your updates have completed successfully.",
                        status: true,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.actionMessage = { on: false, status: null };
                                    $scope.getProducts($scope.pagination.current);
                                }
                            }
                        ]
                    }
                }
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/products/saveproductstocknolupdates/");
            });

    }

    $scope.updateOrderOverride = function (item) {
        item.changed = true;
        $scope.displayOrderOverrideUpdates();
    };

    $scope.displayOrderOverrideUpdates = function () {
        $scope.updatesOrderOverride = [];
        $scope.updateNamesOrderOverride = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.changed) {
                $scope.updatesOrderOverride.push({ id: value.ID, name: value.name, orderOverride: value.orderOverride });
                $scope.updateNamesOrderOverride.push(value.name);
            }
        });

        if ($scope.updatesOrderOverride.length == 0) {
            $scope.actionMessage = { on: false, status: null };
            return;
        }

        $scope.actionMessage = {
            on: true,
            message: "Order updates will be made to the following products - <b>" + $scope.updateNamesOrderOverride.join() + "</b>.",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        $scope.sendOrderOverrideUpdates();
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                        $scope.getProducts($scope.pagination.current);
                    }
                }
            ]
        }
    };

    $scope.sendOrderOverrideUpdates = function () {
        var model = $scope.updatesOrderOverride;
        $http.post("/admin/products/saveproductorderoverrideupdates/", model).
            success(function (data, status, headers, config) {
                if (!data.success) {
                    $scope.actionMessage = {
                        on: true,
                        message: "There were problems. " + data.message,
                        status: false,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.displayOrderOverrideUpdates();
                                }
                            }
                        ]
                    }
                }
                else {
                    $scope.actionMessage = {
                        on: true,
                        message: "Your updates have completed successfully.",
                        status: true,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.actionMessage = { on: false, status: null };
                                    $scope.getProducts($scope.pagination.current);
                                }
                            }
                        ]
                    }
                }
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/products/saveproductorderoverrideupdates/");
            });

    }

    $scope.toggleBulkHideBlurbUpdate = function () {
        $scope.bulkhideblurb.on = !$scope.bulkhideblurb.on;
    };

    $scope.runBulkHideBlurbUpdate = function () {

        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.products, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.name);
            }
        });
        $scope.actionMessage = {
            on: true,
            message: "Your bulk hide description assignment will be run on <b>" + $scope.updateNames.length + "</b> product(s).",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        var model = $scope.bulkhideblurb;
                        model.productIds = $scope.selectedIds;
                        $http.post("/admin/products/runbulkhideblurbassignment/", model).
                            success(function (data, status, headers, config) {
                                if (!data.success) {
                                    $scope.bulkhideblurb.error = data.error;
                                    $scope.actionMessage = {
                                        on: true,
                                        message: "There were problems. ",
                                        status: false,
                                        actions: [
                                            {
                                                text: "ok", look: "confirm", action: function () {
                                                    $scope.actionMessage = { on: false, status: null };
                                                }
                                            }
                                        ]
                                    }
                                }
                                else {
                                    $scope.bulkhideblurb.error = null;
                                    $scope.toggleBulkHideBlurbUpdate();
                                    $scope.getProducts($scope.pagination.current);
                                    $scope.actionMessage = {
                                        on: true,
                                        message: "Your updates have completed successfully.",
                                        status: true,
                                        actions: [
                                            {
                                                text: "ok", look: "confirm", action: function () {
                                                    $scope.actionMessage = { on: false, status: null };
                                                }
                                            }
                                        ]
                                    }
                                }
                            }).
                            error(function (data, status, headers, config) {
                                console.log("api failure - " + "/admin/products/runbulkhideblurbassignment/");
                            });
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                    }
                }
            ]
        }
    };
    

}]);
