﻿kawaApp.controller('OrderController', ['$scope', '$http', '$attrs', function ($scope, $http, $attrs) {
    $scope.actionMessage = { on: false };
    $scope.totalOrders = 0;
    $scope.action = "normal";
    $scope.statusSet = $attrs.statusset;
    $scope.shipment = {};
    $scope.indicators = { loading: false }
    $scope.pagination = {};

    $scope.statusOptions = {
        availableOptions: [{ ID: null, Name: "Update selected orders with status" }],
        selectedOption: { ID: null, Name: "Update selected orders with status" },
        signatures: [{ ID: null, Name: "Add a signature" }],
        selectedSignature: { ID: null, Name: "Add a signature" },
    }
    $scope.selectstate = { all: false };

    $scope.tinymceOptions = {
        selector: 'textarea',
        menubar: false,
        toolbar: 'bold italic underline formatselect bullist numlist | link',
        plugins: 'link,lists,autoresize',
        relative_urls: false,
    }

    $scope.pageSizes = [10, 20, 30, 40, 50, 60, 70, 80, 100, 150, 200];
    $scope.ordersPerPage = $scope.pageSizes[1];

    $scope.$watch('selectstate.all', function (newValue, oldValue) {
        if (newValue == oldValue)
            return;
        angular.forEach($scope.orders, function (value, key) {
            value.selected = newValue;
        });
    });

    $scope.getOrders = function (newPage) {
        if (!newPage)
            $scope.pagination.current = 1;

        var shipVals = [];
        $("input[name='PostedShippingMethods.ShippingMethodIDs']:checked").each(function () {
            shipVals.push($(this).val());
        });
        
        var filterModel = {
            OrderNo: $("#OrderNo").val(),
            Customer: $("#Customer").val(),
            Email: $("#Email").val(),
            Startdate: $("#Startdate").val(),
            Enddate: $("#Enddate").val(),
            TotalFrom: $("#TotalFrom").val(),
            TotalTo: $("#TotalTo").val(),
            StatusID: $("#StatusID").val(),
            VoucherCode: $("#VoucherCode").val(),
            CountryID: $("#CountryID").val(),
            isTaxExempt: $("#isTaxExempt").val(),
            isPrinted: $("#isPrinted").val(),
            PaymentType: $("#PaymentType").val(),
            PostedShippingMethods: { ShippingMethodIDs: shipVals },
            Products: $("#Products").val(),
            FlagLabel: $("#FlagLabel").val(),

            page: newPage,
            pageSize: $scope.ordersPerPage,
            statusSet: $scope.statusSet
        }
        $scope.indicators.loading = true;
        $http.post("/admin/orders/getorderlist/", filterModel).
        success(function (data, status, headers, config) {
            $scope.indicators.loading = false;
            $scope.orders = data.list;
            $scope.totalOrders = data.count;

            if ($scope.updatesCubicWeight && $scope.updatesCubicWeight.length > 0)
            {
                angular.forEach($scope.updatesCubicWeight, function (value, key) {
                    for (var i = 0; i < $scope.orders.length; i++) {
                        if (value.id == $scope.orders[i].ID)
                            $scope.orders[i].selected = true;
                    }
                });
                $scope.updatesCubicWeight = null;
            }
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/orders/getorderlist/");
        });
    };

    $scope.getOrders(null);

    $scope.pageChanged = function (newPage) {
        $scope.getOrders(newPage);
    };


    $http.post("/admin/orders/statusoptions/", null).
    success(function (data, status, headers, config) {
        $scope.statusOptions.availableOptions = $scope.statusOptions.availableOptions.concat(data);
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/orders/statusoptions/");
         });

    $http.post("/admin/orders/signatures/", null).
        success(function (data, status, headers, config) {
            $scope.statusOptions.signatures = $scope.statusOptions.signatures.concat(data);
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/orders/statusoptions/");
        });

    $scope.print = function () {
        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.orders, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.OrderNo);
            }
        });

        if ($scope.selectedIds.length == 0) {
            $scope.statusOptions.selectedOption = $scope.statusOptions.availableOptions[0];
            $scope.actionMessage = {
                on: true,
                message: "Sorry but you have no orders selected.",
                status: false,
                actions: [
                    {
                        text: "ok", look: "confirm", action: function () {
                            $scope.actionMessage = { on: false, status: null };
                        }
                    }
                ]
            }
            return;
        }

        var win = window.open("/admin/orders/ordersforprint?OrderIDs=" + $scope.selectedIds.join(), '_blank');
        win.focus();
    }

    $scope.setPrintedState = function (isPrinted) {
        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.orders, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.OrderNo);
            }
        });

        if ($scope.selectedIds.length == 0) {
            $scope.statusOptions.selectedOption = $scope.statusOptions.availableOptions[0];
            $scope.actionMessage = {
                on: true,
                message: "Sorry but you have no orders selected.",
                status: false,
                actions: [
                    {
                        text: "ok", look: "confirm", action: function () {
                            $scope.actionMessage = { on: false, status: null };
                        }
                    }
                ]
            }
            return;
        }

        $scope.actionMessage.processing = true;
        var model = {
            orderids: $scope.selectedIds,
            isprinted: isPrinted
        }

        $http.post("/admin/orders/setprintedstatusoforders/", model).
        success(function (data, status, headers, config) {
            $scope.getOrders($scope.pagination.current);
            $scope.actionMessage = {
                on: true,
                message: "The printed status change has completed successfully.",
                status: true,
                actions: [
                    {
                        text: "ok", look: "confirm", action: function () {
                            $scope.actionMessage = { on: false, status: null };
                        }
                    }
                ]
            }
        }).
        error(function (data, status, headers, config) {
            $scope.actionMessage = {
                on: true,
                message: "Sorry an error occured. " + data,
                status: false
            }
        });
    }

    $scope.statusChange = function()
    {
        if($scope.statusOptions.selectedOption.ID == null)
        {
            $scope.actionMessage = { on: false, status:null };
        }
        else {

            $scope.selectedIds = [];
            $scope.updateNames = [];
            angular.forEach($scope.orders, function (value, key) {
                if (value.selected) {
                    $scope.selectedIds.push(value.ID);
                    $scope.updateNames.push(value.OrderNo);
                }
            });

            if ($scope.selectedIds.length == 0)
            {
                $scope.statusOptions.selectedOption = $scope.statusOptions.availableOptions[0];
                $scope.actionMessage = {
                    on: true,
                    message: "Sorry but you have no orders selected.",
                    status: false
                }
                return;
            }
           
            $scope.actionMessage = {
                on: true,
                message: "Set the following orders to <b>" + $scope.statusOptions.selectedOption.Name + "</b> - " + $scope.updateNames.join() + ".",
                status: null,
                actions: [
                    {
                        text: "confirm", look: "confirm", action: function () {
                            $scope.changeStatus();
                            $scope.statusOptions.selectedOption = $scope.statusOptions.availableOptions[0];
                        }
                    },
                    {
                        text: "cancel", look: "cancel", action: function () {
                            $scope.actionMessage = { on: false, status: null };
                            $scope.statusOptions.selectedOption = $scope.statusOptions.availableOptions[0];
                        }
                    }
                ]
            }
        }
    }

    $scope.$watch('statusOptions.selectedSignature', function (newValue, oldValue) {
        if (newValue == oldValue)
            return;

        if (!$scope.statusOptions.message)
            $scope.statusOptions.message = "";

        if (oldValue && oldValue.ID > 0)
            $scope.statusOptions.message = $scope.statusOptions.message.replace(oldValue.Html, (newValue && newValue.ID > 0 ? newValue.Html : "" ));
        else if (newValue && newValue.ID > 0)
            $scope.statusOptions.message += newValue.Html;
    });

    $scope.changeStatus = function () {
        $scope.actionMessage.processing = true;
        var model = {
            orderids: $scope.selectedIds,
            statusid: $scope.statusOptions.selectedOption.ID,
            message: $scope.statusOptions.message
        }
        $http.post("/admin/orders/ChangeStatusOfOrders/", model).
        success(function (data, status, headers, config) {
            $scope.getOrders(null);
            $scope.actionMessage = {
                on: true,
                message: "The order status change has completed successfully.",
                status: true,
                actions: [
                    {
                        text: "ok", look: "confirm", action: function () {
                            $scope.statusOptions.message = "";
                            $scope.statusOptions.selectedSignature = $scope.statusOptions.signatures[0];
                            $scope.actionMessage = { on: false, status: null };
                        }
                    }
                ]
            }
        }).
        error(function (data, status, headers, config) {
            $scope.actionMessage = {
                on: true,
                message: "Sorry an error occured. " + data,
                status: false
            }
        });
    }

    $scope.toggleShipment = function () {
        $scope.action = $scope.action == "normal" ? "shipment" : "normal";

        if($scope.action == "shipment")
        {
            $scope.selectedIds = [];
            $scope.updateNames = [];
            angular.forEach($scope.orders, function (value, key) {
                if (value.selected) {
                    $scope.selectedIds.push(value.ID);
                    $scope.updateNames.push(value.OrderNo);
                }
            });
            if ($scope.selectedIds.length == 0)
            {
                $scope.actionMessage = {
                    on: true,
                    message: "You must select an order.",
                    status: false,
                    actions: [
                        {
                            text: "ok", look: "confirm", action: function () {
                                $scope.actionMessage = { on: false, status: null };
                            }
                        }
                    ]
                }
                $scope.action = "normal";
                return;
            }
            $scope.shipmentOrderNo = $scope.updateNames[0];
        }

    }

    $scope.isShipment = function () {
        return $scope.action == "shipment";
    }

    $scope.addShipment = function () {
        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.orders, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.OrderNo);
            }
        });
        if ($scope.selectedIds.length == 0) {
            $scope.actionMessage = {
                on: true,
                message: "You must select an order.",
                status: false,
                actions: [
                    {
                        text: "ok", look: "confirm", action: function () {
                            $scope.actionMessage = { on: false, status: null };
                        }
                    }
                ]
            }
            return;
        }

        $scope.shipment.OrderID = $scope.selectedIds[0];
        $scope.shipment.Comment = $scope.statusOptions.shipmentMessage;
        $scope.shipmentErr = {
            ShippingMethodID: !$scope.shipment.ShippingMethodID || $scope.shipment.ShippingMethodID == "",
            CarrierID: !$scope.shipment.CarrierID || $scope.shipment.CarrierID == "",
            TrackingNo: !$scope.shipment.TrackingNo || $scope.shipment.TrackingNo == ""
        };

        if ($scope.shipmentErr.ShippingMethodID || $scope.shipmentErr.CarrierID || $scope.shipmentErr.TrackingNo)
        {
            return;
        }

        $http.post("/admin/orders/AddShipment/", $scope.shipment).
        success(function (data, status, headers, config) {
            $scope.action = "normal";
            $scope.statusOptions.shipmentMessage = "";
            $scope.shipment = {};
            $scope.actionMessage = {
                on: true,
                message: "The order shipment has been added successfully.",
                status: true,
                actions: [
                    {
                        text: "ok", look: "confirm", action: function () {
                            $scope.actionMessage = { on: false, status: null };
                        }
                    }
                ]
            }
        }).
        error(function (data, status, headers, config) {
            $scope.actionMessage = {
                on: true,
                message: "Sorry an error occured. " + data,
                status: false
            }
        });

        $scope.action = "normal";
    }

    //Cubic Weight

    $scope.updateCubicWeight = function (item) {
        item.changed = true;
        $scope.displayCubicWeightUpdates();
    };

    $scope.displayCubicWeightUpdates = function () {
        $scope.updatesCubicWeight = [];
        $scope.updateNamesCubicWeight = [];
        angular.forEach($scope.orders, function (value, key) {
            if (value.changed) {
                $scope.updatesCubicWeight.push({ id: value.ID, name: value.name, cubicWeight: value.CubicWeight });
                $scope.updateNamesCubicWeight.push(value.OrderNo);
            }
        });

        if ($scope.updatesCubicWeight.length == 0) {
            $scope.actionMessage = { on: false, status: null };
            return;
        }

        $scope.actionMessage = {
            on: true,
            message: "Cubic weight updates will be made to the following orders - <b>" + $scope.updateNamesCubicWeight.join() + "</b>.",
            status: null,
            actions: [
                {
                    text: "confirm", look: "confirm", action: function () {
                        $scope.sendCubicWeightUpdates();
                    }
                },
                {
                    text: "cancel", look: "cancel", action: function () {
                        $scope.actionMessage = { on: false, status: null };
                        $scope.getOrders($scope.pagination.current);
                    }
                }
            ]
        }
    };

    $scope.sendCubicWeightUpdates = function () {
        var model = $scope.updatesCubicWeight;
        $http.post("/admin/orders/saveproductcubicweightupdates/", model).
        success(function (data, status, headers, config) {
            if (!data.success) {
                $scope.actionMessage = {
                    on: true,
                    message: "There were problems. " + data.message,
                    status: false,
                    actions: [
                       {
                           text: "ok", look: "confirm", action: function () {
                               $scope.displayCubicWeightUpdates();
                           }
                       }
                    ]
                }
            }
            else {
                $scope.getOrders($scope.pagination.current);
                $scope.actionMessage = {
                    on: true,
                    message: "Your updates have completed successfully.",
                    status: true,
                    actions: [
                        {
                            text: "ok", look: "confirm", action: function () {
                                $scope.actionMessage = { on: false, status: null };
                                
                            }
                        }
                    ]
                }
            }
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/orders/sendCubicWeightUpdates/");
        });

    }

    $scope.downloadLabels = function () {
        $scope.selectedIds = [];
        $scope.updateNames = [];
        angular.forEach($scope.orders, function (value, key) {
            if (value.selected) {
                $scope.selectedIds.push(value.ID);
                $scope.updateNames.push(value.OrderNo);
            }
        });

        if ($scope.selectedIds.length == 0) {
            $scope.statusOptions.selectedOption = $scope.statusOptions.availableOptions[0];
            $scope.actionMessage = {
                on: true,
                message: "Sorry but you have no orders selected.",
                status: false,
                actions: [
                    {
                        text: "ok", look: "confirm", action: function () {
                            $scope.actionMessage = { on: false, status: null };
                        }
                    }
                ]
            }
            return;
        }

        var win = window.open("/admin/orders/ordersforlabel?OrderIDs=" + $scope.selectedIds.join(), '_blank');
        win.focus();
    }

}]);
