﻿angApp.controller('SearchController', ['$scope', '$http', '$timeout', '$attrs', '$q', function ($scope, $http, $timeout, $attrs, $q) {
    $scope.startup = true;

    $scope.search = {
        searchStr: $attrs.searchstr
    };
    /*
    if(isSearch)
        return;
    */
    
    $(document).click(function () {
        if ($(".searchrow .cancel").is(":visible"))
            return;
        $scope.changeSearchTo("");
        $scope.$digest();
    });

    $(".searchrow .searchauto, .searchrow .results, .searchrow input[type=submit]").click(function (e) {
        e.stopPropagation();
    });
    
    $scope.viewMoreResults = function()
    {
        $(".searchrow form").submit();
    }

    $scope.changeSearchTo = function (str)
    {
        $scope.search.searchStr = str;
        
    }

    $scope.$watch('search.searchStr', function (newValue, oldValue) {
        
        if ($attrs.searchstr != "" && $scope.startup)
        {
            $scope.startup = false;
            return;
        }

        if ($scope.canceller)
            $scope.canceller.resolve();

        $scope.canceller = $q.defer();

        console.log('search - ' + $scope.search.searchStr);
        if ($scope.search.searchStr == null || $scope.search.searchStr.length <= 2)
        {
            $scope.search.results = null;
            return;
        }

        var requestPromise = $http({
            method: 'POST',
            url: '/search/searchsuggest/',
            timeout: $scope.canceller.promise,
            data:{ searchstr: $scope.search.searchStr }
        });

        requestPromise.
        then(function (response) {
                if (!response.data.isSearchanise)
                    console.log("Fallback from Searchanise");
                if (response.data.tags.lenth == 0 && response.data.products.length == 0) {
                    $scope.search.results = null;
                    return;
                }
                $scope.search.results = response.data;
            }, function (response) {
                console.log("api cancel or failure - " + "/search/searchsuggest/");
            });

        /*
        $http.post("/search/searchsuggest/", { searchstr: $scope.search.searchStr }).
        success(function (data, status, headers, config) {
            if (data.tags.lenth == 0 && data.products.length == 0) {
                $scope.search.results = null;
                return;
            }
            $scope.search.results = data;
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/search/searchsuggest/");
        });
        */
    });

    $scope.getBrands = function () {
        if (!$scope.search.results) return [];
        return $scope.search.results.tags.filter(function (x) { return x.Type == 4 });
    }

    $scope.getCats = function () {
        if (!$scope.search.results) return [];
        return $scope.search.results.tags.filter(function (x) { return x.Type != 4 });
    }

    $scope.cancel = function () {
        $scope.search.searchStr = "";
    }

    $scope.saveTagHit = function (TagID) {
        $scope.saveStat({ Type:0, TagID:TagID});
    }

    $scope.saveProductHit = function (ProductID) {
        $scope.saveStat({ Type: 0, ProductID: ProductID });
    }

    $scope.saveStat = function (stat) {

        $http.post("/search/savestat/", stat).
            success(function (data, status, headers, config) {
                //console.log(response);
            }).
            error(function (data, status, headers, config) {
                console.log("api cancel or failure - " + "/search/searchsuggest/");
            });
    }

    $scope.placeSearch = function (search) {
        $scope.search.searchStr = search;
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }

    $scope.selectTrendingSearch = function (url, id, objectType) {
        if (objectType == "PRODUCT")
            $scope.saveStat({ Type: 0, ProductID: id });
        else 
            $scope.saveStat({ Type: 0, TagID: id });

        window.location = url;
    }
}]);


$(function () {
    $('#searchauto1').data('holder', $('#searchauto1').attr('placeholder'));

    $('#searchauto1').focusin(function () {
        $(this).attr('placeholder', '');
    });
    $('#searchauto1').focusout(function () {
        $(this).attr('placeholder', $(this).data('holder'));
    });
})