﻿kawaApp.controller('VariantController', ['$scope', '$http', function ($scope, $http) {
    $scope.actionMessage = { on: false };
    $scope.variant = {};

    $scope.$watch('variant.isHidden', function (newValue, oldValue) {
        if (newValue == oldValue)
            return;
        if (!$scope.variant.isHidden && $scope.variant.preOrderShipDate)
            $scope.variant.preOrderShipDate = "";
    });

    $scope.$watch('variant.preOrderShipDate', function (newValue, oldValue) {
        if (newValue == oldValue)
            return;
        if (!$scope.variant.isHidden && $scope.variant.preOrderShipDate.length)
            $scope.variant.isHidden = true;
    });
    
    
    $scope.saveVariant = function () {

        $('#stdmsg').html('');
        $('#stdmsg').hide();
        $http.post("/admin/products/savevariant/", $scope.variant).
        success(function (data, status, headers, config) {
            if(data.success)
            {
                $scope.errorList = [];
                $scope.getVariant();
                $scope.actionMessage = {
                    on: true,
                    message: "Your updates have completed successfully.",
                    status: true,
                    actions: [
                        {
                            text: "ok", look: "confirm", action: function () {
                                $scope.actionMessage = { on: false, status: null };
                            }
                        }
                    ]
                }
            }
            else
            {
                $scope.errorList = data.errorList;
                $scope.actionMessage = {
                    on: true,
                    message: "There were problems.",
                    status: false,
                    actions: [
                       {
                           text: "ok", look: "confirm", action: function () {
                               $scope.actionMessage = { on: false, status: null };
                           }
                       }
                    ]
                }
            }
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/products/savevariant/");
        });
    }

    $scope.getVariant = function () {
        $http.post("/admin/products/getvariant/", { ProductID: ProductID }).
        success(function (data, status, headers, config) {
            $scope.variant = data;

            if($scope.variant.ID==0)
            {
                $scope.variant.price = "";
                $scope.variant.priceRRP = "";
                $scope.variant.priceMargin = "";
                $scope.variant.bulkPrices = [];
            }
            else
            {
                $scope.variant.preOrderShipDate = $scope.variant.preOrderShipDateStr;
            }
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/products/getvariant/");
        });
    }

    $scope.addBulk = function()
    {
        $scope.variant.bulkPrices.push({ VariantID: $scope.variant.ID });
    }

    $scope.deleteBulk = function (price)
    {
        $scope.variant.bulkPrices.splice($scope.variant.bulkPrices.indexOf(price), 1);
    }

    $scope.updateMargin = function (item) {
        if (item.VariantID)
            item.priceMargin = (($scope.variant.priceRRP - item.amount) / $scope.variant.priceRRP) * 100;
        else
            item.priceMargin = (($scope.variant.priceRRP - item.price) / $scope.variant.priceRRP) * 100;
    };

    $scope.updatePrice = function (item) {
        if (item.VariantID)
            item.amount = $scope.variant.priceRRP - (item.priceMargin * $scope.variant.priceRRP / 100);
        else
            item.price = $scope.variant.priceRRP - (item.priceMargin * $scope.variant.priceRRP / 100);
    };

    $scope.updateRRP = function (item) {
        item.price = ($scope.variant.priceRRP - ($scope.variant.priceRRP * $scope.variant.priceMargin / 100));

        angular.forEach($scope.variant.bulkPrices, function (value, key) {
            $scope.updatePrice(value);
        });
    };

    $scope.modelErrHas = function (errorList) {
        if (angular.isUndefined(errorList)) return false;
        if (errorList.length == 0) return false;
        return true;
    }

    $scope.modelErrClass = function(errorList)
    {
        return $scope.modelErrHas(errorList) ? "has-error" : "";
    }

    $scope.modelErrMsg = function (errorList) {
        if (angular.isUndefined(errorList)) return "";
        if (errorList.length == 0) return "";
        return errorList.join();
    }

    $scope.modelErrHasStr = function (errorListStr) {
        if (angular.isUndefined($scope.errorList)) return false;
        if (angular.isUndefined($scope.errorList[errorListStr])) return false;
        if ($scope.errorList[errorListStr].length == 0) return false;
        return true;
    }

    $scope.modelErrMsgStr = function (errorListStr) {
        if (angular.isUndefined($scope.errorList)) return "";
        if (angular.isUndefined($scope.errorList[errorListStr])) return "";
        if ($scope.errorList[errorListStr].length == 0) return "";
        return $scope.errorList[errorListStr].join();
    }


    $scope.getVariant();
}]);