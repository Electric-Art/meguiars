﻿var kawaApp = angular.module('kawaApp', ['angularUtils.directives.dirPagination', 'ui.tinymce', 'checklist-model']).
    filter('unsafe', ['$sce', function ($sce) {
        return function (val) {
            return val ? $sce.trustAsHtml(val) : "";
        };
    }]);
