﻿kawaApp.controller('PurchaseOrderController', ['$scope', '$http', '$attrs', function ($scope, $http, $attrs) {
    $scope.actionMessage = { on: false };
    $scope.totalPurchaseOrderItems = 0;
    $scope.indicators = { loading: false }
    $scope.statusSet = $attrs.statusset;
    $scope.pagination = {};

    $scope.statusOptions = statusOptions;
    $scope.bulkStatusOptions = {
        availableOptions: [{ ID: null, Name: "Update selected with status" }].concat(statusOptions),
        selectedOption: { ID: null, Name: "Update selected with status" },
        selectedSupplier: { ID: null, Name: "Update selected with supplier" },
    }
    $scope.selectstate = { all: false };

    $scope.pageSizes = [10, 20, 30, 40, 50, 60, 70, 80, 100, 150, 200];
    $scope.ordersPerPage = $scope.pageSizes[8];

    $scope.$watch('selectstate.all', function (newValue, oldValue) {
        if (newValue == oldValue)
            return;
        angular.forEach($scope.purchaseOrderItems, function (value, key) {
            value.selected = newValue;
        });
    });


    $scope.init = function () {

        $http.post("/admin/purchaseorders/getbulksuppliers", {}).
            success(function (data, status, headers, config) {
                $scope.bulkSuppliers = [{ ID: null, Name: "Update selected with supplier" }].concat(data);
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/orders/getorderlist/");
            });

        $scope.getPurchaseOrderItems(null);
    }

    $scope.getPurchaseOrderItems = function (newPage,cb) {
        if (!newPage)
            $scope.pagination.current = 1;
        var filterModel = {
            OrderNo: $("#OrderNo").val(),
            BrandID: $("#BrandID").val(),
            SupplierID: $("#SupplierID").val(),
            ProductName: $("#ProductName").val(),
            Startdate: $("#Startdate").val(),
            Enddate: $("#Enddate").val(),
            Status: $("#Status").val(),
            SupplierInvNo: $("#SupplierInvNo").val(),
            page: newPage,
            pageSize: $scope.ordersPerPage,
            statusSet: $scope.statusSet
        }
        $scope.indicators.loading = true;
        $http.post("/admin/purchaseorders/getpurchaseorderitemlist", filterModel).
            success(function (data, status, headers, config) {
                $scope.indicators.loading = false;
                $scope.purchaseOrderItems = data.list;
                $scope.totalPurchaseOrderItems = data.count;
                if(cb) cb();
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/orders/getorderlist/");
            });
    };

    

    $scope.pageChanged = function (newPage) {
        $scope.getPurchaseOrderItems(newPage);
    };

    $scope.getStatusColor = function (item) {
        switch (item.Status) {
            case 0: return "rgb(255,255,51)";
            case 1: return "rgb(68, 177, 245)";
            case 2: return "rgb(191, 118, 237)";
            case 3: return "rgb(255, 149, 34)";
            case 4: return "red";
            case 5: return "rgb(151, 207, 77)";
            case 6: return "rgb(75, 125, 5)";
        }
        return "#ccc";
    }

    $scope.new = function (OrderID) {
        $scope.purchaseOrderItems = [
            {
                ID: 0, Status: 0, CurrentStatus: { ID: 0 }, OrderID: OrderID || null
            }
        ].concat($scope.purchaseOrderItems);
        $scope.setEdit($scope.purchaseOrderItems[0]);
    }

    $scope.setEdit = function (item) {
        $scope.purchaseOrderItems.filter(function (x) { return x.edit; }).forEach(function (x) { x.edit = false; });
        item.edit = true;
        item.atEditStatus = item.Status;
        $scope.productChange(item);
        item.CurrentStatus = $scope.statusOptions.filter(function (x) { return x.ID == item.Status; })[0]
        setTimeout(function () {
            $(".editing .addanglookup").each(window.setLookups);
            $(".editing .add-picker").datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true
            });
        }, 800);
    }

    $scope.save = function (item,andNew) {
        if (!item.QuantityRequired || isNaN(item.QuantityRequired)) {
            alert("You must provide Qty Req in order to save;")
            return;
        }

        if (item.Status == 4 && (!item.QuantityRecieved || isNaN(item.QuantityRecieved))) {
            alert("You must provide Qty Rec in order to save as recieved.")
            return;
        }

        if (item.Status == 1 && !item.OrderedDate) {
            alert("You must provide an Ordered Date when saving a status of Ordered.")
            return;
        }

        if (item.Status == 1 && item.atEditStatus > 1) {
            item.atEditStatus = 1;
            alert("Please remember to save the new Ordered Date.")
            return;
        }

        $http.post("/admin/purchaseorders/savepurchaseorderitem", item).
            success(function (data, status, headers, config) {
                if (data.success) {
                    $scope.getPurchaseOrderItems($scope.pagination.current, function () {
                        if (andNew) {
                            $scope.new(item.OrderID);
                        }
                    });
                }
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/purchaseorders/savepurchaseorderitem");
            });
    }

    $scope.delete = function (item) {
        if(confirm("Are you sure you want to delete this item?")) {

            $http.post("/admin/purchaseorders/deletepurchaseorderitem", item).
                success(function (data, status, headers, config) {
                    if (data.success) {
                        $scope.getPurchaseOrderItems($scope.pagination.current);
                    }
                }).
                error(function (data, status, headers, config) {
                    console.log("api failure - " + "/admin/purchaseorders/deletepurchaseorderitem");
                });

        }
        
    }

    $scope.statusChange = function (item) {
        item.Status = item.CurrentStatus.ID;
        if (item.Status == 0 && item.atEditStatus > 0) {
            item.OrderedDate = null;
        }
    }

    $scope.statusSupplier = function (item) {
        item.SupplierID = item.CurrentSupplier.ID;
    }

    $scope.productChange = function (item) {
        $http.post("/admin/purchaseorders/getsuppliers", item).
            success(function (data, status, headers, config) {
                $scope.suppliers = [{ ID: null, Name: "not set" }].concat(data);
                if (!$scope.suppliers.length)
                    return;
                if (!$scope.suppliers.filter(function (x) { return x.ID == item.SupplierID; })) {
                    item.CurrentSupplier = $scope.suppliers[0];
                    item.SupplierID = $scope.suppliers[0].ID;
                }
                else {
                    item.CurrentSupplier = $scope.suppliers.filter(function (x) { return x.ID == item.SupplierID; })[0];
                }
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/orders/getorderlist/");
            });
    }

    $scope.$watch('letters', function (newValue, oldValue, scope) {
        //Do anything with $scope.letters
    });

    $scope.bulkStatusChangePrepare = function () {
        if ($scope.bulkStatusOptions.selectedOption.Name == "Ordered") {
            $scope.bulkStatusOptions.statusChangeDatePlaceholder = "Ordered Date";
        }
        if ($scope.bulkStatusOptions.selectedOption.Name == "OOS") {
            $scope.bulkStatusOptions.statusChangeDatePlaceholder = "OOS till Date";
        }
    }

    $scope.bulkStatusChange = function () {
        if ($scope.bulkStatusOptions.selectedOption.ID == null && $scope.bulkStatusOptions.selectedSupplier.ID == null && !$scope.bulkStatusOptions.SupplierInvNo) {
            //$scope.actionMessage = { on: false, status: null };
            $scope.actionMessage = {
                on: true,
                message: "There is nothing specified yet to do.",
                status: false
            }
            return;
        }
        else {

            $scope.selectedIds = [];
            $scope.updateNames = [];
            $scope.updateItems = [];
            angular.forEach($scope.purchaseOrderItems, function (value, key) {
                if (value.selected) {
                    $scope.selectedIds.push(value.ID);
                    $scope.updateItems.push(value);
                    $scope.updateNames.push("MGR-" + value.OrderID + "[" + (value.ProductName || value.ProductIDName) + "]");
                }
            });

            if ($scope.selectedIds.length == 0) {
                $scope.actionMessage = {
                    on: true,
                    message: "Sorry but you have no purchase order items selected.",
                    status: false
                }
                return;
            }

            if ($scope.updateItems.filter(function (x) { return x.ID == 0 || x.ID == "" || x.ID == null }).length) {
                $scope.actionMessage = {
                    on: true,
                    message: "You can only bulk update purchase order items that have already been saved.",
                    status: false
                }
                return;
            }

            if ($scope.bulkStatusOptions.selectedOption.Name == "Required" && $scope.updateItems.filter(function (x) { return x.QuantityRequired == "" || x.QuantityRequired == null }).length) {
                $scope.actionMessage = {
                    on: true,
                    message: "You must have a Qty Rec for all items selected before you make a bulk status change to 'Required'.",
                    status: false
                }
                return;
            }
            
            if ($scope.bulkStatusOptions.selectedOption.Name == "Ordered" && !$scope.bulkStatusOptions.statusChangeDate) {
                $scope.actionMessage = {
                    on: true,
                    message: "You must provide an Order Date make a bulk status change to 'Ordered'.",
                    status: false
                }
                return;
            }

            

            var message = "";
            if ($scope.bulkStatusOptions.selectedOption.ID != null) {
                message = "Set the following purchase order items to <b>" + $scope.bulkStatusOptions.selectedOption.Name + "</b> " + ($scope.bulkStatusOptions.statusChangeDate ? ("with a " + $scope.bulkStatusOptions.statusChangeDatePlaceholder + " of <b>" + $scope.bulkStatusOptions.statusChangeDate + "</b>") : "") + "<br/><br/>";
            }
            if ($scope.bulkStatusOptions.selectedSupplier.ID != null) {
                message += "Set the following purchase order items to supplier <b>" + $scope.bulkStatusOptions.selectedSupplier.Name + "</b><br/><br/>"; 
            }
            if ($scope.bulkStatusOptions.SupplierInvNo) {
                message += "Set the following purchase order items to supplier invoice no <b>" + $scope.bulkStatusOptions.SupplierInvNo + "</b><br/><br/>";
            }
            message += $scope.updateNames.join("<br/>") + "<br/>"
            
            $scope.actionMessage = {
                on: true,
                message: message,
                status: null,
                actions: [
                    {
                        text: "confirm", look: "confirm", action: function () {
                            $scope.actionMessage.processing = true;

                            if ($scope.bulkStatusOptions.selectedOption.ID != null) {
                                var model = {
                                    poids: $scope.selectedIds,
                                    status: $scope.bulkStatusOptions.selectedOption.ID,
                                    statusDate: $scope.bulkStatusOptions.statusChangeDate,
                                    SupplierInvNo: $scope.bulkStatusOptions.SupplierInvNo
                                }

                                $http.post("/admin/purchaseorders/bulkchangestatus/", model).
                                    success(function (data, status, headers, config) {
                                        $scope.actionMessage = {
                                            on: true,
                                            message: "The status change has completed successfully.",
                                            status: true,
                                            actions: [
                                                {
                                                    text: "ok", look: "confirm", action: function () {
                                                        $scope.actionMessage = { on: false, status: null };
                                                    }
                                                }
                                            ]
                                        }
                                        if ($scope.bulkStatusOptions.selectedSupplier.ID != null || $scope.bulkStatusOptions.SupplierInvNo) {
                                            $scope.commitSupplierChange();
                                        }
                                        else {
                                            $scope.bulkStatusOptions.selectedOption = $scope.bulkStatusOptions.availableOptions[0];
                                            $scope.bulkStatusOptions.selectedSupplier = $scope.bulkSuppliers[0];
                                            $scope.bulkStatusOptions.statusChangeDate = "";
                                            $scope.bulkStatusOptions.SupplierInvNo = null;
                                            $scope.getPurchaseOrderItems($scope.pagination.current);
                                        }
                                    }).
                                    error(function (data, status, headers, config) {
                                        wasError = true;
                                        $scope.actionMessage = {
                                            on: true,
                                            message: "Sorry an error occured. " + data,
                                            status: false
                                        }
                                    });
                            }
                            else if ($scope.bulkStatusOptions.selectedSupplier.ID != null || $scope.bulkStatusOptions.SupplierInvNo) {
                                $scope.commitSupplierChange();
                            }
                        }
                    },
                    {
                        text: "cancel", look: "cancel", action: function () {
                            $scope.actionMessage = { on: false, status: null };
                            $scope.bulkStatusOptions.selectedOption = $scope.bulkStatusOptions.availableOptions[0];
                            $scope.bulkStatusOptions.selectedSupplier = $scope.bulkSuppliers[0];
                            $scope.bulkStatusOptions.statusChangeDate = "";
                            $scope.bulkStatusOptions.SupplierInvNo = null;
                        }
                    }
                ]
            }
        }
    }

    $scope.commitSupplierChange = function () {
        var model = {
            poids: $scope.selectedIds,
            supplierID: $scope.bulkStatusOptions.selectedSupplier.ID,
            SupplierInvNo: $scope.bulkStatusOptions.SupplierInvNo
        }

        $http.post("/admin/purchaseorders/bulkchangesupplier/", model).
            success(function (data, status, headers, config) {
                if ($scope.bulkStatusOptions.selectedOption.ID != null) {
                    $scope.actionMessage.message += " The supplier change has completed successfully.";
                }
                else {
                    $scope.actionMessage = {
                        on: true,
                        message: "The supplier change has completed successfully.",
                        status: true,
                        actions: [
                            {
                                text: "ok", look: "confirm", action: function () {
                                    $scope.actionMessage = { on: false, status: null };
                                }
                            }
                        ]
                    }
                }
                $scope.bulkStatusOptions.selectedOption = $scope.bulkStatusOptions.availableOptions[0];
                $scope.bulkStatusOptions.selectedSupplier = $scope.bulkSuppliers[0];
                $scope.bulkStatusOptions.statusChangeDate = "";
                $scope.bulkStatusOptions.SupplierInvNo = null;
                $scope.getPurchaseOrderItems($scope.pagination.current);
            }).
            error(function (data, status, headers, config) {
                wasError = true;
                $scope.actionMessage = {
                    on: true,
                    message: "Sorry an error occured. " + data,
                    status: false
                }
            });
    }

    $scope.init();
}]);