﻿var angApp = angular.module('angApp',[]).
    filter('unsafe', ['$sce',function ($sce) {
        return function (val) {
            return val ? $sce.trustAsHtml(val) : "";
        };
    }]);