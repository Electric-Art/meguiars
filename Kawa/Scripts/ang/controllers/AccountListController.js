﻿kawaApp.controller('AccountListController', ['$scope', '$http', function ($scope, $http) {
    $scope.actionMessage = { on: false };
    $scope.totalAccounts = 0;
    //$scope.productsPerPage = 25;
    $scope.bulk = {
        on: false,
        single: { quanity: 1 },
        volumePrices: []
    };
    $scope.selectstate = { all: false };

    $scope.pageSizes = [10,20,30,40,50,60,70,80,100,150,200];
    $scope.accountsPerPage = $scope.pageSizes[1];

    $scope.$watch('selectstate.all', function (newValue, oldValue) {
        if (newValue == oldValue)
            return;
        angular.forEach($scope.accounts, function (value, key) {
            value.selected = newValue;
        });
    });

    $scope.getAccounts = function (newPage) {
        var filterModel = {
            searchFirstName: $("#searchFirstName").val(),
            searchLastName: $("#searchLastName").val(),
            searchEmail: $("#searchEmail").val(),
            page: newPage,
            pageSize: $scope.accountsPerPage
        }
        $scope.loading = true;
        $http.post("/admin/accounts/getaccountlist/", filterModel).
        success(function (data, status, headers, config) {
            $scope.loading = false;
            $scope.accounts = data.list || [];
            $scope.totalAccounts = data.count;
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/accounts/getaccountlist/");
        });
    };

    $scope.getAccounts(null);

    $scope.pageChanged = function (newPage) {
        $scope.getAccounts(newPage);
    };

    $scope.getLinkUrl = function (item) {
        return item.AccountID ? ("/admin/accounts/account/" + item.AccountID) : ("/admin/orders/order/" + item.LastOrderID);
    };

    $scope.getEditLinkText = function (item) {
        return item.AccountID ? "View Account" : "View Order";
    };

    $scope.select = function (item) {
        item.selected = true;
        
        $("#AccountID").val(item.AccountID);
        $("#LastOrderID").val(item.LastOrderID);

        $(".accountchooseform").submit();
    };

    $scope.selectGuest = function () {
        $("#AccountID").val("");
        $("#LastOrderID").val("");

        $(".accountchooseform").submit();
    };

}]);
