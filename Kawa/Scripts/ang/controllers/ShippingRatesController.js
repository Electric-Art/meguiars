﻿kawaApp.controller('ShippingRatesController', ['$scope', '$http', function ($scope, $http) {
    $scope.actionMessage = { on: false };

    $scope.locationSelection = { locations: locations, selectedLocation: locations[0] };

    $scope.saveRate = function (rate) {
        $http.post("/admin/shippingconfig/ShippingMethodRateSave/", rate).
        success(function (data, status, headers, config) {
            if(data.success)
            {
                $scope.getRates();
            }
            else
            {
                rate.isError = true;
                rate.errorList = data.errorList;
            }
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/shippingconfig/ShippingMethodRateSave/");
        });

    }

    $scope.deleteRate = function (rate) {
        bootbox.confirm("Are you sure?", function (result) {
            if (!result) return;
            $http.post("/admin/shippingconfig/ShippingMethodRateDelete/", rate).
            success(function (data, status, headers, config) {
                if(data.success)
                {
                    $scope.getRates();
                }
            }).
            error(function (data, status, headers, config) {
                console.log("api failure - " + "/admin/shippingconfig/ShippingMethodRateSave/");
            });
        });
    }

    $scope.getRates = function () {
        $http.post("/admin/shippingconfig/ShippingMethodRatesForCountry/", { ShippingMethodID: ShippingMethodID, LocationID: $scope.locationSelection.selectedLocation.ID }).
        success(function (data, status, headers, config) {
            $scope.shippingRates = data;
            $scope.shippingRates.push({ isEdit: true, ID:0, WeightDependency: "", Rate: "", ShippingMethodID: ShippingMethodID, LocationID: $scope.locationSelection.selectedLocation.ID })
        }).
        error(function (data, status, headers, config) {
            console.log("api failure - " + "/admin/shippingconfig/ShippingMethodRatesForCountry/");
        });
    }

    $scope.modelErrHas = function (errorList) {
        if (angular.isUndefined(errorList)) return false;
        if (errorList.count == 0) return false;
        return true;
    }

    $scope.modelErrClass = function(errorList)
    {
        return $scope.modelErrHas(errorList) ? "has-error" : "";
    }

    $scope.modelErrMsg = function (errorList) {
        if (angular.isUndefined(errorList)) return "";
        if (errorList.count == 0) return "";
        return errorList.join();
    }


    $scope.getRates();
}]);