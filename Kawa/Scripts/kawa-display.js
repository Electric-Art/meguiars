﻿$(function () {

    var sizeUps = function () {
        $(".searchrow .results").width($(window).width() >= 768 ? ($(".searchrow .searchauto").width() + 65) : "");
    }

    var mobMenuSetup = function () {
        //stick in the fixed 100% height behind the navbar but don't wrap it
        //$('#slide-nav.navbar-simple').after($('<div class="inverse" id="navbar-height-col"></div>'));
        //$('#navbar-height-col').append($("#mob-menu-main"));
        //$('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));
        // Enter your ids or classes
        var toggler = '.navbar-toggle';
        var pagewrapper = '#page-content';
        var navigationwrapper = '.navbar-fixed-top';
        var menuwidthratio = 0.8;
        var menuwidth = '80%'; // the menu inside the slide menu itself
        var slidewidth = '80%';
        var menuneg = '-80%';
        var slideneg = '-80%';

        $(".inverse").width($(window).width() * menuwidthratio);

        var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';

        var toggleSlideMenu = function()
        {
            var selected = $(toggler).hasClass('slide-active');
            $('#slidemenu').stop().animate({
                left: selected ? menuneg : '0px'
            });
            $('#navbar-height-col').stop().animate({
                left: selected ? slideneg : '0px'
            });
            $(pagewrapper).stop().animate({
                left: selected ? '0px' : slidewidth
            });
            $(navigationwrapper).stop().animate({
                left: selected ? '0px' : slidewidth
            });
            $(toggler).toggleClass('slide-active', !selected);
            $('#slidemenu').toggleClass('slide-active');
            $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');
        }

        $("#slide-nav").on("click", toggler, function (e) {
            toggleSlideMenu();
        });

        
        $(window).on("resize", function () {
            $(".inverse").width($(window).width() * menuwidthratio);
            if ($(window).width() > 767 && $('body').hasClass('slide-active')) {
                toggleSlideMenu();
            }
        });
    }

    $('.joinlink').click(function () {
        $('.modal').on('shown.bs.modal', function () {
            $(this).find('iframe').attr('src', '/Account/LoginOrRegister');
        })
        $('.modal').modal({ show: true });
        $('iframe').load(function () {
            $('.loading').hide();
        });
    });

    
   
    function handlebasketcountupdate() {
        $.ajax(
            {
                type: "POST",
                url: "/Shop/BasketCount/",
                contentType: 'application/json; charset=utf-8',
                success: function (result) {

                    $('.basketcounter').html(result);
                },
                error: function (req, status, error) {
                    console.log("ajax issue AddToBasketModal")
                }
            });
        
    }


    $(".maintag, .subcatmenu > div").hover(function () {
        $(".subcatmenu").find('*[data-tagid="' + $(this).attr("data-tagid") + '"]').show();
    },function () {
        $(".subcatmenu").find('*[data-tagid="' + $(this).attr("data-tagid") + '"]').hide();
    });

    
    runTagPopups();

    $("body").on("click", ".gotoproduct", function () {
        window.location.href = $(this).attr("data-produrl");
    });
    $("body").on("click", ".addtobasketalt", function () {
        var parentForm = $(this).parents("form");
        var qtySelect = parentForm.find("#basket_Quantity");

        var basket = {
            VariantID: parentForm.find("#basket_VariantID").val(),
            Quantity: qtySelect.length ? qtySelect.val() : 1,
            partial: true
        };

        var post = JSON.stringify(basket);

        $.ajax(
            {
                type: "POST",
                url: "/Shop/AddToBasket/",
                data: post,
                contentType: 'application/json; charset=utf-8',
                success: function (result) {

                    window.location = '/shop';
                },
                error: function (req, status, error) {
                    console.log("ajax issue AddToBasketModal")
                }
            });

    });
    $("body").on("click",".addtobasket", function () {
        var parentForm = $(this).parents("form");
        var qtySelect = parentForm.find("#basket_Quantity");

        var basket = {
            VariantID: parentForm.find("#basket_VariantID").val(),
            Quantity: qtySelect.length ? qtySelect.val() : 1,
            partial: true
        };

        var post = JSON.stringify(basket);

        ///console.log(basket);
        $.ajax(
        {
            type: "POST",
            url: "/Shop/AddToBasket/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                if (!result) {
                    location.reload();
                    return;
                }
                handlebasketcountupdate();
                $('.modalbasketadded .inner-content').html(result)
                $('.modalbasketadded').modal({ show: true });
                $('.basketitemcount').html($("#OrderSummary_BasketCount").val());
                $('.baskettotal').html($("#OrderSummary_Total").val());
            },
            error: function (req, status, error) {
                console.log("ajax issue AddToBasketModal")
            }
        });

    });

    $(".pagesizesel").change(function () {
        var path = $(this).attr("data-state").replace("pagesize=30&", "").replace("pagesize=60&", "").replace("pagesize=120&", "");
        ///console.log(path);
        window.location = path + "&pagesize=" + $(this).val();

    });

    sizeUps();
    mobMenuSetup();

    $(window).resize(function () {

        sizeUps();


    });
});

function runTagPopups()
{
    $(".adhoctag").hover(function () {

        if ($("div.adhoctagpopup[data-tagid=" + $(this).attr("data-tagid") + "]").length > 0)
        {
            var divToShow = $("div.adhoctagpopup[data-tagid=" + $(this).attr("data-tagid") + "]");
            var divToShowAnchor = divToShow.parent();
            divToShow.css({
                display: "block",
                position: "absolute",
                left: ($(this).offset().left) + "px",
                top: ($(this).offset().top + $(this).height() + 20) + "px"
            });
        }
        else{
            $("div.adhoctagpopupmob[data-tagid=" + $(this).attr("data-tagid") + "]").parents(".modaladhoctag").modal({ show: true });
        }

    },
    function () {
        $(".adhoctagpopup").hide();
    });

}




