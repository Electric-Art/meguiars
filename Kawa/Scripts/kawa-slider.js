﻿$(window).resize(function () {
    //setupSlider();
   
    for(var i=0;i<sliders.length;i++)
    {
        sliders[i].arrange();
    }
});

$(function () {
    setupSlider();
    // Instantiate the Bootstrap carousel
})

var sliders = [];

function setupSlider()
{
    $(".multi-item-carousel").each(function () {
        var caro = $(this);

        caro.maintagid = $(this).attr("data-tagid");
        caro.inner = caro.find('.carousel-inner');

        if (caro.maintagid == "0")
            caro.attr("data-pulled", true);

        caro.arrange = function () {
            var vwidth = $(window).width();
            var itemsPerSlide = 4;
            var bsCol = 3;
            if (vwidth < 768) {
                itemsPerSlide = 2;
                bsCol = 6;
            }
            else if (vwidth < 992) {
                itemsPerSlide = 3;
                bsCol = 4;
            }
            if (vwidth > 1200) {
                itemsPerSlide = 5;
                bsCol = 15;
            }

            var data = this.find('.data > div');
            var numOfSlides = Math.ceil(data.length / itemsPerSlide);
            var di = 0;
            this.inner.empty();
            //if (test) return;
            for (var i = 0; i < numOfSlides; i++) {
                var slide = $("<div class='item'></div>");
                if (i == 0)
                    slide.addClass("active");
                for (var y = 0; y < itemsPerSlide; y++) {
                    if (di < data.length) {
                        var item = $(data[di]).clone();
                        item.addClass("col-xs-" + bsCol);
                        slide.append(item);
                    }
                    di++;
                }
                this.inner.append(slide);
            };

            var object = $(this).data("bs.carousel");

            runTagPopups();
        };

        caro.arrange();
        
        $.fn.carousel.Constructor.DEFAULTS.interval = false;
       
        var result = caro.carousel({
            interval: false,
            pause: false,
            wrap: false 
        });

        $(caro).bind('slide.bs.carousel', function (e) {
            console.log('slide event!');
            //$(this).carousel("pause");
        });

        sliders.push(caro);

        caro.find('.intersection').click(function () {
            var name = $(this).html();
            var post = { MainTagID: caro.maintagid, IntersectionTagID: $(this).attr("data-tagid") };
            $.ajax(
            {
                type: "POST",
                url: "/Categories/GetProductsForTag/",
                data: post,
                success: function (result) {
                    caro.find('.data').html(result);
                    caro.find('.filtername').html(name);
                    caro.find('.frame li').removeClass("sel");
                    caro.find('.frame a[data-tagid=' + post.IntersectionTagID + ']').parent().addClass("sel");
                    caro.attr("data-pulled", true);
                    caro.arrange();
                },
                error: function (req, status, error) {
                    //setMessage("Error addLink." + error, true);
                },
                returnObj: this
            });
            return false;
        });

        caro.find('.mainsection').click(function () {
            var post = { MainTagID: caro.maintagid };
            $.ajax(
            {
                type: "POST",
                url: "/Categories/GetProductsForTag/",
                data: post,
                success: function (result) {
                    caro.find('.data').html(result);
                    caro.find('.filtername').html("Filter by category");
                    caro.find('.frame li').removeClass("sel");
                    caro.attr("data-pulled", true);
                    setupSlider();
                },
                error: function (req, status, error) {
                    //setMessage("Error addLink." + error, true);
                },
                returnObj: this
            });
            return false;
        });

        if (caro.attr("data-clickadded") == "true") return;

        caro.find('.right.carousel-control').click(function () {
            if (caro.attr("data-pulled") == "true") return;
            var nextButton = $(this);
            var post = { MainTagID: caro.maintagid };
            $.ajax(
                {
                    type: "POST",
                    url: "/Categories/GetProductsForSlider/",
                    data: post,
                    success: function (result) {
                        caro.find('.data').append(result);
                        setupSlider();
                        caro.attr("data-pulled", true);
                        setTimeout(function () {
                            nextButton.click();
                        }, 500);
                    },
                    error: function (req, status, error) {
                        //setMessage("Error addLink." + error, true);
                    },
                    returnObj: this
                });
            return false;
        });
        caro.attr("data-clickadded", true);

    });

    $(".centeredtags").each(function () {
        var $frame = $(this);
        var $wrap = $frame.parent();

        $frame.sly(false);

        // Call Sly on frame
        $frame.sly({
            horizontal: 1,
            itemNav: 'centered',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 1,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 1,
            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Buttons
            prev: $wrap.find('.prev'),
            next: $wrap.find('.next')
        });
    });
}

