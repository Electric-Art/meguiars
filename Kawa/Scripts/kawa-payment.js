﻿
(function ($) {
    $.fn.inputFilter = function (inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
                this.value = "";
            }
        });
    };
}(jQuery));

$(function () {

    if (!isAdminOrder)
        isAdminOrder = false;

    /*
    $("#EWAY_CARDNUMBER").inputFilter(function (value) {
        console.log('test1', value);
        console.log('test2', /[^0-9]/.test(value))
        return !/[^0-9]/.test(value);    // Allow digits only, using a RegExp
    });
    */

    var input = document.getElementById('EWAY_CARDNUMBER');
    input.addEventListener('keyup', function (e) {
        //console.log('test1', this.value);
        this.value = this.value.replace(/[^0-9]/, '');
    });

    $("#savecrd").click(function () {
        var post = JSON.stringify({ saveCard: $("#savecrd").is(':checked') });

        $.ajax(
        {
            type: "POST",
            url: "/Shop/SaveCard/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                console.log("Save card setting run - " + result);
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: null
        });
    });

    $(".payment-new-card-form .formbutton").click(function () {
        var form =  $(".payment-new-card-form form");
        var cardval = $(".payment-new-card-form input[name=EWAY_CARDNUMBER]").val();
        var isSurchargeON = $(".payment-new-card-form").attr("data-issuron") == "true";
        var isAmex = isSurchargeON && ( cardval.indexOf("34") == 0 || cardval.indexOf("37") == 0 );
        if (isAmex && form.attr("data-isamex") != "true")
        {
            $("#dialog").dialog({
                dialogClass: "alert",
                buttons: [
                    {
                        text: "OK",
                        click: function () {
                            getNewPostData(true, form);
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Cancel",
                        click: function () {
                            console.log("did not accept amex charge");
                            $(this).dialog("close");
                        }
                    }
                ]
            });
        }
        else if (!isAmex && form.attr("data-isamex") != "false")
        {
            getNewPostData(false, form);
        }
        else
        {
            form.submit();
        }
    });
   
 
    $('form.cc').submit(function () {
        if ($(this).valid()) {
            $(".payforms").hide();
            $(".paywait").show();
            $(".cart-menu-out").hide();
        }
    });

    $(".paypal").each(function () {
        $(this).submit();
    });

    var getNewPostData = function(isAmex, form)
    {
        var post = JSON.stringify({ code: form.attr("data-code"), isAmex: isAmex, isAdmin: isAdminOrder == true });
        $.ajax(
        {
            type: "POST",
            url: "/Shop/UpdatePostUrl/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                if (result.success) {
                    form.attr("action", result.FormActionURL);
                    form.find("input[name=EWAY_ACCESSCODE]").val(result.AccessCode);
                    form.attr("data-isamex", isAmex);
                    console.log("UpdatePostUrl - success");
                    form.submit();
                }
                else {
                    console.log("UpdatePostUrl - " + result.message);
                }
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: null
        });
    }

    $(".payment-token-card-form .formbutton").click(function () {
        var form = $(".payment-token-card-form form");
        var isAmex = form.attr("data-isamex") == "true";
        if (isAmex)  {
            $("#dialog").dialog({
                dialogClass: "alert",
                buttons: [
                    {
                        text: "OK",
                        click: function () {
                            informOfAmexTokenUse(form);
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "Cancel",
                        click: function () {
                            console.log("did not accept amex charge");
                            $(this).dialog("close");
                        }
                    }
                ]
            });
        }
        else {
            form.submit();
        }
    });

    var informOfAmexTokenUse = function (form) {
        var post = JSON.stringify({ code: form.attr("data-code") });
        $.ajax(
        {
            type: "POST",
            url: "/Shop/AmexTokenCardUsed/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                if (result.success) {
                    console.log("AmexTokenCardUsed - success");
                    form.submit();
                }
                else {
                    console.log("AmexTokenCardUsed - " + result.message);
                }
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: null
        });
    }

});

$.validator.addMethod('creditcardmonth', function (value, element, params) {
    var isMonthNo = value > 0 && value < 13;
    return isMonthNo;
}, '');

$.validator.unobtrusive.adapters.add('creditcardmonth', {}, function (options) {
    options.rules['creditcardmonth'] = true;
    options.messages['creditcardmonth'] = options.message;
});

$.validator.addMethod('creditcardyear', function (value, element, params) {
    //TODO Use this year
    var isYearNo = value >= 15;
    return isYearNo;
}, '');

$.validator.unobtrusive.adapters.add('creditcardyear', {}, function (options) {
    options.rules['creditcardyear'] = true;
    options.messages['creditcardyear'] = options.message;
});


$.validator.addMethod('creditcarddate', function (value, element, params) {
    if ($("#EWAY_CARDEXPIRYMONTH").val() != "" && $("#EWAY_CARDEXPIRYYEAR").val() != "")
        return isValidDate($("#EWAY_CARDEXPIRYMONTH").val() + "/01/20" + $("#EWAY_CARDEXPIRYYEAR").val());
    return true;
}, '');

$.validator.unobtrusive.adapters.add('creditcarddate', {}, function (options) {
    options.rules['creditcarddate'] = true;
    options.messages['creditcarddate'] = options.message;
});


// Validates that the input string is a valid date formatted as "mm/dd/yyyy"
function isValidDate(dateString) {
    // First check for the pattern
    if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[1], 10);
    var month = parseInt(parts[0], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if (year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    // Adjust for leap years
    if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    if (!(day > 0 && day <= monthLength[month - 1]))
        return false;

    var date = new Date(year,month-1,day);
    var thismonth = new Date();
    thismonth.setMonth(thismonth.getMonth() - 1);
    return (date >= thismonth);
};

