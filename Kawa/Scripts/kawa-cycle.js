﻿$('.cycle-slideshow').on('cycle-initialized', function (event, opts) {
    setTimeout(sizeControls, 400);
});

$(window).resize(function () {
    setTimeout(sizeControls, 400);

    //setupSlider();
});

function sizeControls()
{
    $(".cycle-slideshow-controls img").css('top', $(".cycle-slide-active").height() / 2 - $(".cycle-slideshow-controls img").height() / 2 - 25);
    $(".cycle-slideshow-controls").show();
}

