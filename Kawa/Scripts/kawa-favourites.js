﻿$(function () {

    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();


    $(".fav").each(function () {

        $(this).click(function () {

            var thisbutton = $(this);
            thisbutton.html("Adding...");
            var objectis = thisbutton.attr("data-objectis");
            var id = thisbutton.attr("data-id");
            var model = null;
            switch (objectis) {
                case "product":
                    model = { ProductID: id };
                    break;
                case "page":
                    model = { PageID: id };
                    break;
            };

            var post = JSON.stringify(model);

            $.ajax(
            {
                type: "POST",
                url: "/Home/ToggleFavourite/",
                data: post,
                contentType: 'application/json; charset=utf-8',
                    success: function (result) {

                        if (result.data == "added")
                        {
                            thisbutton.html("WISHLIST REMOVE");
                            thisbutton.addClass("favourited");
                        }
                        else {
                            thisbutton.html("ADD TO WISHLIST");
                            thisbutton.removeClass("favourited");
                        }
                  
                },
                error: function (req, status, error) {
                    //setMessage("Error addLink." + error, true);
                },
                returnObj: null
            });

            return false;
        });

    });


});