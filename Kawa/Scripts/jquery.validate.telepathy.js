﻿/* Replaces date js validation that does not like Australian or NZ format */

$(function () {

    $.validator.methods.date = function (value, element) {
        return this.optional(element) || $.datepicker.parseDate('dd/mm/yy', value);
    }

});