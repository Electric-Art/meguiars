﻿var setLookups = function () {

    var hidden = $(this);
    var lookupsrc = hidden.attr("data-lookup");
    var lookupoffine = hidden.attr("data-lookup-productoffline");
    var display = hidden.attr("data-lookup-display");
    var value = hidden.attr("data-lookup-value");
    var placeholder = hidden.attr("data-lookup-placeholder") || "type to find";
    var txtbox = $("<input type='text' id='" + hidden.attr("id") + "_Lookup' name='" + hidden.attr("name") + "_Lookup' class='form-control' placeholder='" + placeholder + "' />");

    if (value && value != "") {
        txtbox.val(value);
        txtbox.addClass("has-val")
    }

    hidden.after(txtbox)

    var hound = new Bloodhound({
        identify: function (obj) {
            return obj.ID;
        },
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace(display),
        queryTokenizer: Bloodhound.tokenizers.obj.whitespace(display),
        //prefetch: '/admin/SearchProducts/',
        remote: {
            url: "/admin/" + lookupsrc + "/?search=%QUERY" + (lookupoffine ? "&offline=true":""),
            wildcard: '%QUERY',
            cache: false
        },
        limit: 10
    });

    $(txtbox).typeahead(null,
        {
            name: 'my-dataset-' + lookupsrc,
            display: display,
            source: hound,
            limit: 10
        });


    $(txtbox).bind('typeahead:select', function (ev, suggestion) {
        console.log('Selection: ' + suggestion);
        hidden.val(suggestion.ID);
        hidden.trigger('input'); // Use for Chrome/Firefox/Edge
        hidden.trigger('change'); // Use for Chrome/Firefox/Edge + IE11
        $(this).addClass("has-val");

        $("#QuantityNew").each(function () {
            var topQuantity = suggestion.stock || 100;
            $(this).find("option").remove();
            for (var i = 1; i <= topQuantity; i++){
                $(this).append("<option val='" + i + "'>" + i + "</option>")
            }
        });
    });

    txtbox.change(function () {
        if ($(this).val() == "")
            $(hidden).val("");

        if ($(hidden).val == "")
            $(this).addClass("has-val");
        else
            $(this).removeClass("has-val");
    });

    txtbox.keypress(function () {
        $(this).removeClass("has-val");
    });
}


$(".addlookup").each(setLookups);


