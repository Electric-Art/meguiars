﻿$(function () {

    var replaceAll = function (str, search, replacement) {
        return str.split(search).join(replacement);
    };

    var reIndex = function () {
        var i = 0;
        $(".repeated-form").each(function () {
            var entity = $(this).attr("data-entity-name");
            var oldIndex = $(this).attr("data-repeated-index");
            var html = replaceAll(this.outerHTML, entity + "[" + oldIndex + "]", entity + "[" + i + "]");
            html = replaceAll(html, entity + "_" + oldIndex + "__", entity + "_" + i + "__");
            this.outerHTML = html;
            $(this).attr("data-repeated-index", i);
            i++;
        });
    }
    
    var template = $($(".repeated-form")[0]).clone();
    $(".repeated-form.istemplate").remove();

    $(".repeated-form-addbutton").click(function () {
        var nextIndex = $(".repeated-form").length;
        var html = replaceAll(template[0].outerHTML, "[0]", "[" + nextIndex + "]");
        html = replaceAll(html, "_0__", "_" + nextIndex + "__");
        html = replaceAll(html, "select2ToBe", "select2");
        var newObj = $(html);
        newObj.attr("data-repeated-index", nextIndex);
        newObj.find("select").val("");
        newObj.find("input[type=text]").val("");
        newObj.find("#Contacts_" + nextIndex + "__ID").val("0");
        $(".repeated-form-box").append(newObj);
        if (window.pvLoadOptionImages)
            window.pvLoadOptionImages();
    });

   $(".repeated-form-box").on("click", ".repeated-form-deletebutton", function () {
       var btn = $(this);
       if (confirm('Are you sure you wish delete this item?')) {
           $("#" + btn.attr("data-selector")).remove();
           reIndex();
       }
    });

    if (window.pvLoadOptionImages) window.pvLoadOptionImages();
})