﻿$(function () {

    $('.modalspecial').modal({ show: true });

    $('.modalspecial').on('hidden.bs.modal', function () {
        $.ajax(
        {
            type: "POST",
            url: "/Account/SpecialClosed/",
            success: function (result) {
                console.log("special suppressed");
            },
            error: function (req, status, error) {
                console.log("special suppressed error");
            },
        });
    })

    $(".specialform input[type=button]").click(function () {

        var form = $(this).parents(".specialform form");
        /*
        $(form).validate();

        if (!$(form).valid())
            return;
            */

        var model = {
            Email: form.find("#Email").val()
        };

        $.ajax(
        {
            type: "POST",
            url: "/Account/SpecialRegister/",
            data: model,
            //headers: headers,
            //contentType: 'application/json; charset=utf-8',
            success: function (result) {
                if (result.success) {
                    $(this.returnObj).parents(".specialform").hide();
                    var formcomplete = $(this.returnObj).parents(".special").find(".formcomplete");
                    formcomplete.find(".formresult").html(result.message);
                    formcomplete.show();
                }
                else {
                    $(this.returnObj).parents(".specialform").show();
                    $(this.returnObj).parents(".specialform").find(".validation-summary-errors").html(result.errors);
                }
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: form
        });

    });

})

