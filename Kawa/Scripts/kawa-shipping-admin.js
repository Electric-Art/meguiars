﻿$(function () {
    $(".add-scope-button-country").click(function () {
        var obj = { LocationID: $("#Location_ID").val(), CountryID: $("#CountryID").val() };
        var post = JSON.stringify(obj);
        $.ajax(
        {
            type: "POST",
            url: "/Admin/ShippingConfig/LocationScopeAddCountry",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".locationscopeitems").html(result);
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: this
        });
    });

    $(".add-scope-button-state").click(function () {
        var obj = { LocationID: $("#Location_ID").val(), StateID: $("#StateID").val() };
        var post = JSON.stringify(obj);
        $.ajax(
        {
            type: "POST",
            url: "/Admin/ShippingConfig/LocationScopeAddState",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".locationscopeitems").html(result);
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: this
        });
    });

    $(".add-scope-button-postcode").click(function () {
        var obj = { LocationID: $("#Location_ID").val(), FromPostcode: $("#FromPostcode").val(), ToPostcode: $("#ToPostcode").val() };
        var post = JSON.stringify(obj);
        $.ajax(
        {
            type: "POST",
            url: "/Admin/ShippingConfig/LocationScopeAddPostcodeRange",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".locationscopeitems").html(result);
                $("#FromPostcode").val("");
                $("#ToPostcode").val("");
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: this
        });
    });

    $(".locationscopeitems").on("click", ".remove-scope-button", function () {
        var obj = { LocationID: $("#Location_ID").val(), ObjectID: $(this).attr("data-id") };
        var post = JSON.stringify(obj);
        $.ajax(
        {
            type: "POST",
            url: "/Admin/ShippingConfig/LocationScopeRemove" + $(this).attr("data-entity"),
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".locationscopeitems").html(result);
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: this
        });
    });

    $(".locationscopeitems").on("click", ".remove-scope-button-postcode", function () {
        var obj = { LocationPostcodeRangeID: $(this).attr("data-id") };
        var post = JSON.stringify(obj);
        $.ajax(
        {
            type: "POST",
            url: "/Admin/ShippingConfig/LocationScopeRemove" + $(this).attr("data-entity"),
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".locationscopeitems").html(result);
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            },
            returnObj: this
        });
    });
});