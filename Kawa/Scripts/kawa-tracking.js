﻿$(function () {
    fbq('track', 'ViewContent', {
        value: parseFloat($("#trackdata").attr("data-track-price")),
        currency: 'AUD',
        content_ids: $("#trackdata").attr("data-track-id"),
        content_type: 'product',
    });

    //decommissioned
    $('.addtocartform').submit(function () {
        var productid = $(this).find("#ProductID").val();
        var quantity = $(this).find("#Quantity").val();
        var name = $(".productname").html();
        gaStatAddToCart(productid, quantity, name);
        return true;
    });
});

function gaStatAddToCart(productid, quantity, name) {
    ga('require', 'ec');
    ga('ec:addProduct', {
        'id': productid,
        'name': name,
        //'price': product.price,
        'quantity': quantity
    });
    ga('ec:setAction', 'add');
    ga('send', 'event', 'UX', 'click', 'add to cart');     // Send data using an event.
}