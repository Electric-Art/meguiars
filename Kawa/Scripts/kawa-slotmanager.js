﻿$(function () {

    $(".slot").change(function()
    {
        if ($(this).val() == "")
        {
            alert("You must choose an article or module for a featured slot.");
            return;
        }

        var model = {
            Spot: $(this).attr("dataspot"),
            PageID: $(this).val()
        };

        var post = JSON.stringify(model);
        $.ajax(
        {
            type: "POST",
            url: "/Admin/SaveSlot/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                console.log("all good - show tick");
                $("#slot_" + result.data.Spot + "_success").show();

                setTimeout(function (spot) {
                    $("#slot_" + spot + "_success").hide();
                },3000, result.data.Spot);
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            }
        });

    });

})