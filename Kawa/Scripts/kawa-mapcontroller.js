﻿var lastBounds = null;
var nextBounds = null;
var markers = new Array();
var lastTimeOut = 0;
var MAPZOOM_HALT = 1000;
var cancelNext = false;
var stores = null;

function showLastLoc() {
    showLocations(nextBounds, true);
}

function showLocations(bounds, force) {
    if (cancelNext) {
        cancelNext = false;
        return;
    }

    if (!force) {
        if (lastBounds != null)
        {
            var eq = lastBounds.equals(bounds);
            var cont = lastBounds.contains(bounds.getNorthEast()) && lastBounds.contains(bounds.getSouthWest());
            if (eq || cont)
                return;
        }
        nextBounds = bounds;
        clearTimeout(lastTimeOut);
        lastTimeOut = setTimeout("showLastLoc()", MAPZOOM_HALT);
        return;
    }

    clearTimeout(lastTimeOut);

    lastBounds = bounds;

    var i = 0;
    for (i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }

    markers = new Array();

    var northEast = bounds.getNorthEast();
    var southWest = bounds.getSouthWest();

    var obj = { "nelat": northEast.lat(), "nelng": northEast.lng(), "swlat": southWest.lat(), "swlng": southWest.lng(), "state": document.getElementById('state').value };
    var post = JSON.stringify(obj);

    $.ajax(
    {
        type: "POST",
        url: "/stores/showlocations/",
        data: post,
        contentType: 'application/json; charset=utf-8',
        success: function (result) {
            //$("#prodlistcontainer").html(result);
            stores = result.data;
            $(".liststore").remove();
            var i = 0;
            for (i = 0; i < stores.length; i++) {
                var loc = new google.maps.LatLng(stores[i].lat, stores[i].lng);
                var marker = new google.maps.Marker({
                    map: map,
                    position: loc,
                    title: stores[i].name,
                    icon: '/content/images/pin.png'
                });
                attachInfoWindow(marker, stores[i]);
                markers.push(marker);

                $("#map-stores .inner").append("<div class=\"liststore\" data-marker-index=\"" + i + "\">" + stores[i].name + "</div>");
            }
            $(".liststore").click(function () {
                var markerIndex = Number($(this).attr("data-marker-index"));
                cancelNext = true;
                new google.maps.InfoWindow(
                      {
                          content: storeContent(stores[markerIndex]),
                          size: new google.maps.Size(50, 50)
                      }).open(map, markers[markerIndex]);
            });
        },
        error: function (req, status, error) {
            alert("Error addLink." + error);
        },
        returnObj: this
    });

}

function storeContent(store) {
    var message = "<div class='store'>";

    message += "<div class='storename'>" + store.name + "</div><br/>";
    if (store.shopPosition != null && store.shopPosition != "")
        message += store.shopPosition + "<br/>";
    if (store.streetAddress != null && store.streetAddress != "")
        message += store.streetAddress + "<br/>";
    if (store.suburb != null && store.suburb != "")
        message += store.suburb + "<br/>";
    if (store.state != null && store.state != "")
        message += store.state + "<br/>";

    if (store.state != null && store.state != "")
        message += "<br/><div class='storephone'>ph. " + store.phone + "</div><br/>";

    message += "</div>";
    return message;
}

function attachInfoWindow(marker, store) {
    var infowindow = new google.maps.InfoWindow(
      {
          content: storeContent(store),
          size: new google.maps.Size(50, 50)
      });
      google.maps.event.addListener(marker, 'click', function () {
          cancelNext = true;
          infowindow.open(map, marker);
      });
}

function codeAddress() {
    var address = document.getElementById('pcode').value + ", Australia";
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.setZoom(15);
            showLocations(map.getBounds(),true);


        } else {
            //alert('Sorry we could not find your area: ' + status);
        }
    });
}

function codeState() {

    if (document.getElementById('state').value == "")
        return;

    var address = document.getElementById('state').value + ", Australia";
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.setZoom(6);
            showLocations(map.getBounds(),true);
            /*
            var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
            });
            */
        } else {
           // alert('Sorry we could not find your area: ' + status);
        }
    });
}


var geocoder;
var map;

function initialize() {
    geocoder = new google.maps.Geocoder();

    var style = {};
    style['featureType'] = 'poi.business';
    style['elementType'] = 'labels';
    style['stylers'] = [{ 'visibility': 'off' }];
    //styles.push(style);

    var mapOptions = {
        center: new google.maps.LatLng(-25, 133),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [style]
    };
    map = new google.maps.Map(document.getElementById("map-canvas"),
    mapOptions);
    google.maps.event.addListener(map, 'center_changed', function () {
        showLocations(map.getBounds(), false);
    });
    google.maps.event.addListener(map, 'zoom_changed', function () {
        showLocations(map.getBounds(), false);
    });

    if($("#pcode").val() != $("#pcode").attr("title"))
    {
        codeAddress();
    }
}
google.maps.event.addDomListener(window, 'load', initialize);

$(function () {

    $('#pcode').bind("enterKey", function (e) {
        //codeAddress();
    });
    $('#pcode').keyup(function (e) {
        if (e.keyCode == 13) {
            $('#pcode').blur();
            codeAddress();


        }
    });
    $('#pcode').focus(function () {
        if ($(this).val() == $(this).attr("title")) {
            $(this).val("");
            $(this).css("color", "#000");
        }
    }).blur(function () {
        if ($(this).val() == "") {
            $(this).val($(this).attr("title"));
            $(this).css("color", "#333333");
        }
    });

    $('#state').focus(function () {
        if ($(this).val() == $(this).attr("title")) {
            $(this).val("");
            $(this).css("color", "#000");
        }
    }).blur(function () {
        if ($(this).val() == "") {
            $(this).val($(this).attr("title"));
            $(this).css("color", "#333333");
        }
    });
});
