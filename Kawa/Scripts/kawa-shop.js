﻿$(function () {

    var isAdmin = window.location.pathname.toLowerCase().indexOf("/shop") != 0;
    var controller = isAdmin ? "admin/orders" : "shop";

    if (isAdmin)
        var OrderID = $("#ChangeOrderID").val();

    var recalculateShipping = function () {
        console.log("recalc shipping");
        if ($("#CountryID").val() != "1") {
            $("#Postcode").val('');
        }
        var model = {
            CountryID: $("#CountryID").val(),
            StateID: $("#StateID").length ? $("#StateID").val() : null,
            Postcode: $("#Postcode").length ? $("#Postcode").val() : null,
            ShippingMethodID: $("input[name='ShippingMethodID']:checked").val(),
            AAFT: $("#AdminAdHocFreightTotal").val()
        };
        if (isAdmin) model.OrderID = OrderID;

        var post = JSON.stringify(model);
        $.ajax(
        {
            type: "POST",
            url: "/" + controller + "/ChangeBasketShipping/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".basket > div").html(result);
                setupPostCodeAutoDetectChanges();
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            }
        });
    }


    function delay(callback, ms) {
        var timer = 0;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }

    function setupPostCodeAutoDetectChanges() {
        $('#Postcode').keyup(delay(function (e) {
            ////console.log('Time elapsed!', this.value);
            if ($("#Postcode").val().length >= 4) {
                recalculateShipping();
            }
        }, 800));
    }
    setupPostCodeAutoDetectChanges();


    var popCount = function () {
        $(".basketitemcount").html($("#itemcount").val());
        $(".baskettotal").html($("#baskettotal").val());
    }

    $(".basket").on("change",".item-quantity-dropdown",function () {
        var iid = $(this).attr("data-basketID");
        var model = {
            ID: iid,
            Quantity: $("#item-quantity-" + iid).val()
        };
        if (isAdmin) model.OrderID = OrderID;

        var post = JSON.stringify(model);
        $.ajax(
        {
            type: "POST",
            url: "/" + controller + "/ChangeBasket/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".basket > div").html(result);
                popCount(); 
                setupPostCodeAutoDetectChanges();
                handlebasketcountupdate();
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            }
        });
    });


    function handlebasketcountupdate() {
        $.ajax(
            {
                type: "POST",
                url: "/Shop/BasketCount/",
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    $('.basketcounter').html(result);
                },
                error: function (req, status, error) {
                    console.log("ajax issue AddToBasketModal")
                }
            });

    }
    $(".basket").on("click",".item-remove-button",function () {
        var iid = $(this).attr("data-basketID");
        var model = {
            ID: iid,
            Quantity: 0
        };
        if (isAdmin) model.OrderID = OrderID;

        var post = JSON.stringify(model);
        $.ajax(
        {
            type: "POST",
            url: "/" + controller + "/ChangeBasket/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".basket > div").html(result);
                
                setupPostCodeAutoDetectChanges();
                handlebasketcountupdate();
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            }
        });
        return false;
    });

    $(".basket").on("change", "#CountryID", function () {
        recalculateShipping();
    });

    $(".basket").on("change", "#StateID", function () {
        recalculateShipping();
    });

    $(".basket").on("click", "#PostcodeSet", function () {
        recalculateShipping();
    });

    $(".basket").on("change", ".shipping-radio", function () {
        recalculateShipping();
    });

    $(".basket").on("click", "#update-as", function () {
        recalculateShipping();
    });

    $(".basket").on("click", "#shipping-settooriginal", function () {
        $("#AdminAdHocFreightTotal").val($("#FreightOriginalAmount").val());
        recalculateShipping();
    });

    $(".basket").on("click", "#continuebtn", function () {
        var accepted = $('#terms').is(':checked');
        if (!accepted) {
            $("#dialog").dialog({
                dialogClass: "alert",
                buttons: [
                    {
                        text: "OK",
                        click: function () {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            return false;
        }
        $(".checkform form").submit();
        return true;
    });

   

    $(".basket").on("click", "#continueissuebutton", function () {
        $("#dialogshippingissue").dialog({
            dialogClass: "alert",
            buttons: [
                {
                    text: "OK",
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            ]
        });
        return false;
    });

    $(".basket").on("click", "#voucherredeem", function () {

        if (vouchercode == "")
            return;
        var vouchinstalled = $("#vouchercode").hasClass("vouchinstalled");
        var vouchercode = $("#vouchercode").val();
        var model = {
            code: vouchercode
        };
        if (vouchinstalled)
            model.clearThisPromotion = true;

        if (isAdmin)
        {
            var adminmodel = {
                voucher : model,
                OrderID : OrderID
            };
            model = adminmodel;
        }
        var post = JSON.stringify(model);
        $.ajax(
        {
            type: "POST",
            url: "/" + controller + "/AddVoucher/",
            data: post,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                $(".basket > div").html(result);
                setupPostCodeAutoDetectChanges();
                if ($("#promotiondialogshow").length > 0) {
                    var message = $("#promotiondialogshow").html();
                    bootbox.dialog({
                        title: "Your Voucher",
                        message: message
                    });
                }
            },
            error: function (req, status, error) {
                //setMessage("Error addLink." + error, true);
            }
        });
    });

    $( ".basket" ).on( "click", ".shopterms", function () {
        var src = $(this).attr("href") + "?ismodal=true";
        $('.modal.js-account-popup').on('shown.bs.modal', function () {
            $(this).find('iframe').attr('src', src);
        })
        $('.modal.js-account-popup').modal({ show: true });
        $('iframe').load(function () {
            $('.loading').hide();
        });
        return false;
    });

});

function specialResend(code) {
    $.ajax(
    {
        type: "POST",
        url: "/Account/SpecialResend/" + code,
        success: function (result) {
            $(".mailer-special-resend").html(result.message);
        },
        error: function (req, status, error) {
            //setMessage("Error addLink." + error, true);
        },
        returnObj: null
    });
    return false;
}

