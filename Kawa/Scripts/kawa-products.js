﻿$(function () {

    $("body").on("click",".stocknotice",function () {
        var src = $(this).attr("data-href");
        $('.modal.js-account-popup').on('shown.bs.modal', function () {
            $(this).find('iframe').attr('src', src);
        })
        $('.modal.js-account-popup').modal({ show: true });
        $('.modal.js-account-popup iframe').load(function () {
            $('.loading').hide();
        });
        return false;
    });

});