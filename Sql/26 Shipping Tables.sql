/*
   Wednesday, 9 March 20166:40:18 PM
   User: 
   Server: localhost
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.ShippingMethod
	(
	ID int NOT NULL IDENTITY (1, 1),
	Name varchar(255) NOT NULL,
	DeliveryTime varchar(255) NULL,
	TrackingUrl varchar(255) NULL,
	isHidden bit NOT NULL,
	createDate datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.ShippingMethod ADD CONSTRAINT
	DF_ShippingMethod_isHidden DEFAULT 0 FOR isHidden
GO
ALTER TABLE dbo.ShippingMethod ADD CONSTRAINT
	PK_ShippingMethod PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ShippingMethod SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Location
	(
	ID int NOT NULL IDENTITY (1, 1),
	Name varchar(255) NOT NULL,
	Code varchar(50) NULL,
	isHidden bit NOT NULL,
	createDate datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Location ADD CONSTRAINT
	DF_Location_isHidden DEFAULT 0 FOR isHidden
GO
ALTER TABLE dbo.Location ADD CONSTRAINT
	PK_Location PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Location SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.ShippingRate
	(
	ID int NOT NULL IDENTITY (1, 1),
	ShippingMethodID int NOT NULL,
	LocationID int NULL,
	Type int NOT NULL,
	Rate money NOT NULL,
	WeightDependency decimal(8, 2) NULL,
	CostDependency money NULL,
	isHidden bit NOT NULL,
	createDate datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.ShippingRate ADD CONSTRAINT
	DF_ShippingRate_isHidden DEFAULT 0 FOR isHidden
GO
ALTER TABLE dbo.ShippingRate ADD CONSTRAINT
	PK_ShippingRate PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.ShippingRate ADD CONSTRAINT
	FK_ShippingRate_ShippingMethod FOREIGN KEY
	(
	ShippingMethodID
	) REFERENCES dbo.ShippingMethod
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ShippingRate ADD CONSTRAINT
	FK_ShippingRate_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ShippingRate SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Country ADD
	isHidden bit NOT NULL CONSTRAINT DF_Country_isHidden DEFAULT 0,
	Code varchar(50) NULL,
	CodeA3 varchar(50) NULL,
	CodeN3 varchar(50) NULL,
	RegionCode varchar(50) NULL
GO
ALTER TABLE dbo.Country SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Location_Country
	(
	LocationID int NOT NULL,
	CountryID int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Location_Country ADD CONSTRAINT
	PK_Location_Country PRIMARY KEY CLUSTERED 
	(
	LocationID,
	CountryID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Location_Country ADD CONSTRAINT
	FK_Location_Country_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Location_Country ADD CONSTRAINT
	FK_Location_Country_Country FOREIGN KEY
	(
	CountryID
	) REFERENCES dbo.Country
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Location_Country SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.State
	(
	ID int NOT NULL IDENTITY (1, 1),
	Name varchar(255) NOT NULL,
	CountryID int NOT NULL,
	Code varchar(50) NULL,
	isHidden bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.State ADD CONSTRAINT
	DF_State_isHidden DEFAULT 0 FOR isHidden
GO
ALTER TABLE dbo.State ADD CONSTRAINT
	PK_State PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.State ADD CONSTRAINT
	FK_State_Country FOREIGN KEY
	(
	CountryID
	) REFERENCES dbo.Country
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.State SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Location_State
	(
	LocationID int NOT NULL,
	StateID int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Location_State ADD CONSTRAINT
	PK_Location_State PRIMARY KEY CLUSTERED 
	(
	LocationID,
	StateID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Location_State ADD CONSTRAINT
	FK_Location_State_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Location_State ADD CONSTRAINT
	FK_Location_State_State FOREIGN KEY
	(
	StateID
	) REFERENCES dbo.State
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Location_State SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
