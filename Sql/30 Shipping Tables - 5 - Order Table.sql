/*
   Wednesday, March 16, 201610:20:45 AM
   User: 
   Server: localhost
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Country SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Location SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ShippingMethod SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order] ADD
	ShippingMethod int NULL,
	LocationID int NULL,
	CountryID int NULL
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_ShippingMethod FOREIGN KEY
	(
	ShippingMethod
	) REFERENCES dbo.ShippingMethod
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Country FOREIGN KEY
	(
	CountryID
	) REFERENCES dbo.Country
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
