/*
   Monday, 23 May 20164:10:51 PM
   User: 
   Server: localhost
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ShippingMethod SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD
	ShippingMethodID int NULL
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_ShippingMethod FOREIGN KEY
	(
	ShippingMethodID
	) REFERENCES dbo.ShippingMethod
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
