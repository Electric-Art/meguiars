/*
   Wednesday, May 04, 201610:32:52 AM
   User: 
   Server: localhost
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Location
GO
ALTER TABLE dbo.Location SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Variant
GO
ALTER TABLE dbo.Variant SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Tag
GO
ALTER TABLE dbo.Tag SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Product
GO
ALTER TABLE dbo.Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Promotion
	(
	ID int NOT NULL IDENTITY (1, 1),
	name varchar(500) NULL,
	code varchar(50) NULL,
	ProductID int NULL,
	VariantID int NULL,
	Type int NOT NULL,
	percentOff float(53) NULL,
	amountOff money NULL,
	Status int NOT NULL,
	createDate datetime NOT NULL,
	usedDate datetime NULL,
	OrderID int NULL,
	isHidden int NOT NULL,
	SessionKey varchar(50) NULL,
	expiryDate datetime NULL,
	TagID int NULL,
	notes varchar(MAX) NULL,
	SystemType int NULL,
	minimumSpend money NULL,
	minimumWeight decimal(8, 2) NULL,
	LocationID int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Promotion SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_Promotion ON
GO
IF EXISTS(SELECT * FROM dbo.Promotion)
	 EXEC('INSERT INTO dbo.Tmp_Promotion (ID, code, ProductID, VariantID, Type, percentOff, amountOff, Status, createDate, usedDate, OrderID, isHidden, SessionKey, expiryDate, TagID, notes, SystemType, minimumSpend, minimumWeight, LocationID)
		SELECT ID, code, ProductID, VariantID, Type, percentOff, amountOff, Status, createDate, usedDate, OrderID, isHidden, SessionKey, expiryDate, TagID, notes, SystemType, minimumSpend, minimumWeight, LocationID FROM dbo.Promotion WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Promotion OFF
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Order
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Promotion
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Promotion1
GO
DROP TABLE dbo.Promotion
GO
EXECUTE sp_rename N'dbo.Tmp_Promotion', N'Promotion', 'OBJECT' 
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	PK_Promotion PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Tag FOREIGN KEY
	(
	TagID
	) REFERENCES dbo.Tag
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Promotion FOREIGN KEY
	(
	VoucherPromotionID
	) REFERENCES dbo.Promotion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Promotion1 FOREIGN KEY
	(
	PromotionID
	) REFERENCES dbo.Promotion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
