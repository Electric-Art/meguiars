/****** Object:  StoredProcedure [dbo].[kawa_Cache_Best_Sellers]    Script Date: 28/05/2018 1:58:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[kawa_Cache_Best_Sellers] 
	-- Add the parameters for the stored procedure here
	@Tag1ID int,
	@Tag2ID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@Tag2ID is null)
		begin
		-- Insert statements for procedure here
		delete from [dbo].[ListCache] where [dbo].[ListCache].Tag1ID = @Tag1ID and @Tag2ID is null

		insert into [dbo].[ListCache] (ProductID, Tag1ID, createDate)
		Select top 25 ProductID, @Tag1ID, getdate() 
		from
		(select ProductID = (select ProductID from Variant where ID=VariantID), Quantities = Sum(Quantity)
		from Basket
		where OrderID in
		(select ID
		from [dbo].[Order] AS [t11]
		WHERE (([t11].[SystemStatus] = 2) OR ([t11].[SystemStatus] = 4)) AND ([t11].[OrderDate] >=  dateadd(month, -1, getdate()))) 
		group by variantID
		) 
		as [T0]
		where (NOT (EXISTS(
			SELECT NULL AS [EMPTY]
			FROM [dbo].[Tag_Product] AS [t6]
			WHERE (EXISTS(
				SELECT NULL AS [EMPTY]
				FROM [dbo].[Tag_Tag] AS [t7]
				WHERE ([t7].[TagID] = @Tag1ID) AND ([t7].[RelatedTagID] = [t6].[TagID])
				)) AND ([t6].[ProductID] = [t0].ProductID)
			)))
		order by Quantities desc
		end
	else
		begin 
		-- Insert statements for procedure here
		delete from [dbo].[ListCache] where [dbo].[ListCache].Tag1ID = @Tag1ID and [dbo].[ListCache].Tag2ID = @Tag2ID

		insert into [dbo].[ListCache] (ProductID, Tag1ID, tag2ID, createDate)
		Select top 25 ProductID, @Tag1ID, @Tag2ID, getdate() 
		from
		(select ProductID = (select ProductID from Variant where ID=VariantID), Quantities = Sum(Quantity)
		from Basket
		where OrderID in
		(select ID
		from [dbo].[Order] AS [t11]
		WHERE (([t11].[SystemStatus] = 2) OR ([t11].[SystemStatus] = 4)) AND ([t11].[OrderDate] >=  dateadd(month, -1, getdate()))) 
		group by variantID
		) 
		as [T0]
		where 
		
		(EXISTS(
		SELECT NULL AS [EMPTY]
		FROM [dbo].[Tag_Product] AS [t6]
		INNER JOIN [dbo].[Tag] AS [t7] ON [t7].[ID] = [t6].[TagID]
		WHERE (([t6].[TagID] = @Tag2ID) OR ([t7].[ParentID] = @Tag2ID)) AND ([t6].[ProductID] = [t0].[ProductID])
		)) 

		AND 
		
		(NOT (EXISTS(
			SELECT NULL AS [EMPTY]
			FROM [dbo].[Tag_Product] AS [t6]
			WHERE (EXISTS(
				SELECT NULL AS [EMPTY]
				FROM [dbo].[Tag_Tag] AS [t7]
				WHERE ([t7].[TagID] = @Tag1ID) AND ([t7].[RelatedTagID] = [t6].[TagID])
				)) AND ([t6].[ProductID] = [t0].ProductID)
			)))
		order by Quantities desc

		end
END

