/*
   Thursday, March 16, 201710:47:14 AM
   User: 
   Server: localhost\SQL2014
   Database: dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.MailChimpCache
	(
	ID int NOT NULL IDENTITY (1, 1),
	Email varchar(500) COLLATE Latin1_General_CI_AS NOT NULL,
	EmailType varchar(500) NOT NULL,
	Timestamp datetime NULL,
	TimestampOptIn datetime NULL,
	createDate datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.MailChimpCache ADD CONSTRAINT
	PK_MailChimpCache PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.MailChimpCache SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
