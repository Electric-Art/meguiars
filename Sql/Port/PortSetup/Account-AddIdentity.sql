/*
   Friday, July 01, 20161:56:40 PM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Account
	DROP CONSTRAINT DF_Account_saveCreditCard
GO
CREATE TABLE dbo.Tmp_Account
	(
	ID int NOT NULL IDENTITY (1, 1),
	UserName varchar(50) NOT NULL,
	createDate datetime NOT NULL,
	isAdmin bit NOT NULL,
	isTrade bit NOT NULL,
	isBlocked bit NOT NULL,
	creditCardToken varchar(500) NULL,
	saveCreditCard bit NOT NULL,
	firstName varchar(500) NULL,
	lastName varchar(500) NULL,
	gender int NULL,
	dateOfBirth datetime NULL,
	channel varchar(500) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Account SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Account ADD CONSTRAINT
	DF_Account_saveCreditCard DEFAULT ((1)) FOR saveCreditCard
GO
SET IDENTITY_INSERT dbo.Tmp_Account ON
GO
IF EXISTS(SELECT * FROM dbo.Account)
	 EXEC('INSERT INTO dbo.Tmp_Account (ID, UserName, createDate, isAdmin, isTrade, isBlocked, creditCardToken, saveCreditCard, firstName, lastName, gender, dateOfBirth, channel)
		SELECT ID, UserName, createDate, isAdmin, isTrade, isBlocked, creditCardToken, saveCreditCard, firstName, lastName, gender, dateOfBirth, channel FROM dbo.Account WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Account OFF
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Account
GO
ALTER TABLE dbo.Favourite
	DROP CONSTRAINT FK_Favourite_Account
GO
DROP TABLE dbo.Account
GO
EXECUTE sp_rename N'dbo.Tmp_Account', N'Account', 'OBJECT' 
GO
ALTER TABLE dbo.Account ADD CONSTRAINT
	PK_Account PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Favourite ADD CONSTRAINT
	FK_Favourite_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Favourite SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
