/*
   Monday, July 04, 20169:18:52 AM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Order
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Variant
GO
ALTER TABLE dbo.Variant SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT DF_Basket_noTax
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT DF_Basket_Notes
GO
CREATE TABLE dbo.Tmp_Basket
	(
	ID int NOT NULL,
	VariantID int NOT NULL,
	Price money NOT NULL,
	Weight decimal(8, 2) NULL,
	SessionKey varchar(255) NOT NULL,
	OrderID int NULL,
	Quantity int NOT NULL,
	ShipPrice money NULL,
	CountryID int NULL,
	NoTax bit NOT NULL,
	Notes varchar(2000) NULL,
	CreateDate datetime NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Basket SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Basket ADD CONSTRAINT
	DF_Basket_noTax DEFAULT ((0)) FOR NoTax
GO
ALTER TABLE dbo.Tmp_Basket ADD CONSTRAINT
	DF_Basket_Notes DEFAULT ((0)) FOR Notes
GO
IF EXISTS(SELECT * FROM dbo.Basket)
	 EXEC('INSERT INTO dbo.Tmp_Basket (ID, VariantID, Price, Weight, SessionKey, OrderID, Quantity, ShipPrice, CountryID, NoTax, Notes, CreateDate)
		SELECT ID, VariantID, Price, Weight, SessionKey, OrderID, Quantity, ShipPrice, CountryID, NoTax, Notes, CreateDate FROM dbo.Basket WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.Basket
GO
EXECUTE sp_rename N'dbo.Tmp_Basket', N'Basket', 'OBJECT' 
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	PK_Basket PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
