/*
   Friday, July 01, 20168:53:18 AM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT FK_Variant_Product
GO
ALTER TABLE dbo.Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT DF_Variant_noGst
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT DF_Variant_createDate
GO
CREATE TABLE dbo.Tmp_Variant
	(
	ID int NOT NULL IDENTITY (1, 1),
	ProductID int NOT NULL,
	name varchar(500) NOT NULL,
	sku varchar(50) NULL,
	price money NOT NULL,
	priceRRP money NULL,
	priceMargin float(53) NULL,
	weight decimal(8, 2) NULL,
	minQuantity int NULL,
	maxQuantity int NULL,
	noTax bit NOT NULL,
	stockNo int NULL,
	shipDays int NULL,
	createDate datetime NOT NULL,
	modifiedDate datetime NULL,
	isHidden int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Variant SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Variant ADD CONSTRAINT
	DF_Variant_noGst DEFAULT ((0)) FOR noTax
GO
ALTER TABLE dbo.Tmp_Variant ADD CONSTRAINT
	DF_Variant_createDate DEFAULT (getdate()) FOR createDate
GO
SET IDENTITY_INSERT dbo.Tmp_Variant ON
GO
IF EXISTS(SELECT * FROM dbo.Variant)
	 EXEC('INSERT INTO dbo.Tmp_Variant (ID, ProductID, name, sku, price, priceRRP, priceMargin, weight, minQuantity, maxQuantity, noTax, stockNo, shipDays, createDate, modifiedDate, isHidden)
		SELECT ID, ProductID, name, sku, price, priceRRP, priceMargin, weight, minQuantity, maxQuantity, noTax, stockNo, shipDays, createDate, modifiedDate, isHidden FROM dbo.Variant WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Variant OFF
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Variant
GO
ALTER TABLE dbo.Price
	DROP CONSTRAINT FK_Price_Variant
GO
ALTER TABLE dbo.StockNotice
	DROP CONSTRAINT FK_StockNotice_Variant
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Variant
GO
DROP TABLE dbo.Variant
GO
EXECUTE sp_rename N'dbo.Tmp_Variant', N'Variant', 'OBJECT' 
GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	PK_Variant PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	FK_Variant_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StockNotice ADD CONSTRAINT
	FK_StockNotice_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.StockNotice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Price ADD CONSTRAINT
	FK_Price_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Price SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Basket SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
