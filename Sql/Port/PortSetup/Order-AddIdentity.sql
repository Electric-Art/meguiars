/*
   Friday, July 01, 20162:11:27 PM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Account
GO
ALTER TABLE dbo.Account SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Status
GO
ALTER TABLE dbo.Status SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Country
GO
ALTER TABLE dbo.Country SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Location
GO
ALTER TABLE dbo.Location SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_ShippingMethod
GO
ALTER TABLE dbo.ShippingMethod SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_OrderDate
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_TaxTotal
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_InvoiceTotal_1
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_idx_1
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_Printed
GO
CREATE TABLE dbo.Tmp_Order
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderDate datetime NOT NULL,
	OrderTotal money NOT NULL,
	SystemStatus int NOT NULL,
	StatusID int NULL,
	ProcessDate datetime NULL,
	CompleteDate datetime NULL,
	BasketTotal money NOT NULL,
	FreightTotal money NOT NULL,
	TaxTotal money NOT NULL,
	Invoice text COLLATE Latin1_General_CI_AS NULL,
	InvoiceTotal money NULL,
	idx int NULL,
	AcknowledgeDate datetime NULL,
	FeedbackRequestDate datetime NULL,
	FeedbackResponseDate datetime NULL,
	AdminNotes varchar(MAX) COLLATE Latin1_General_CI_AS NULL,
	Code uniqueidentifier NOT NULL,
	SessionKey varchar(50) NOT NULL,
	AgentData varchar(MAX) NULL,
	PromotionID int NULL,
	AccountID int NULL,
	ShippingMethodID int NULL,
	LocationID int NULL,
	CountryID int NULL,
	StateID int NULL,
	SystemNotes varchar(MAX) NULL,
	PaymentType int NULL,
	CustomerNotes varchar(MAX) NULL,
	VoucherPromotionID int NULL,
	Weight decimal(8, 2) NULL,
	Printed bit NOT NULL,
	TotalBeforeSurcharge money NULL,
	AmexSurcharge money NULL,
	AdminAdHocFreightTotal money NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Order SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_OrderDate DEFAULT (getdate()) FOR OrderDate
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_TaxTotal DEFAULT ((0)) FOR TaxTotal
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_InvoiceTotal_1 DEFAULT ((0)) FOR InvoiceTotal
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_idx_1 DEFAULT ((0)) FOR idx
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_Printed DEFAULT ((0)) FOR Printed
GO
SET IDENTITY_INSERT dbo.Tmp_Order ON
GO
IF EXISTS(SELECT * FROM dbo.[Order])
	 EXEC('INSERT INTO dbo.Tmp_Order (ID, OrderDate, OrderTotal, SystemStatus, StatusID, ProcessDate, CompleteDate, BasketTotal, FreightTotal, TaxTotal, Invoice, InvoiceTotal, idx, AcknowledgeDate, FeedbackRequestDate, FeedbackResponseDate, AdminNotes, Code, SessionKey, AgentData, PromotionID, AccountID, ShippingMethodID, LocationID, CountryID, StateID, SystemNotes, PaymentType, CustomerNotes, VoucherPromotionID, Weight, Printed, TotalBeforeSurcharge, AmexSurcharge, AdminAdHocFreightTotal)
		SELECT ID, OrderDate, OrderTotal, SystemStatus, StatusID, ProcessDate, CompleteDate, BasketTotal, FreightTotal, TaxTotal, Invoice, InvoiceTotal, idx, AcknowledgeDate, FeedbackRequestDate, FeedbackResponseDate, AdminNotes, Code, SessionKey, AgentData, PromotionID, AccountID, ShippingMethodID, LocationID, CountryID, StateID, SystemNotes, PaymentType, CustomerNotes, VoucherPromotionID, Weight, Printed, TotalBeforeSurcharge, AmexSurcharge, AdminAdHocFreightTotal FROM dbo.[Order] WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Order OFF
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Order
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Promotion
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Promotion1
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Order
GO
ALTER TABLE dbo.OrderTax
	DROP CONSTRAINT FK_OrderTax_Order
GO
ALTER TABLE dbo.AccountDetail
	DROP CONSTRAINT FK_AccountDetail_Order
GO
ALTER TABLE dbo.OrderPayment
	DROP CONSTRAINT FK_OrderPayment_Order
GO
DROP TABLE dbo.[Order]
GO
EXECUTE sp_rename N'dbo.Tmp_Order', N'Order', 'OBJECT' 
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	PK_Order PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_ShippingMethod FOREIGN KEY
	(
	ShippingMethodID
	) REFERENCES dbo.ShippingMethod
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Country FOREIGN KEY
	(
	CountryID
	) REFERENCES dbo.Country
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Status FOREIGN KEY
	(
	StatusID
	) REFERENCES dbo.Status
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OrderPayment ADD CONSTRAINT
	FK_OrderPayment_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.OrderPayment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AccountDetail ADD CONSTRAINT
	FK_AccountDetail_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AccountDetail SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OrderTax ADD CONSTRAINT
	FK_OrderTax_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.OrderTax SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Basket SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Promotion FOREIGN KEY
	(
	VoucherPromotionID
	) REFERENCES dbo.Promotion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Promotion1 FOREIGN KEY
	(
	PromotionID
	) REFERENCES dbo.Promotion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
