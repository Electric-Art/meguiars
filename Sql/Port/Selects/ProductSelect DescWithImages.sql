select top 10

-- Product

ID = cscart_products.product_id, 
Name = [cscart_product_descriptions].[product],
Blurb = [cscart_product_descriptions].[full_description],
sku = cscart_products.product_code, 
createDate = cscart_products.updated_timestamp, --TRANSFORM from unix timestamp
isHidden = cscart_products.status,

-- Variant

price = (select [price]  from [Dve-test7].[dbo].[cscart_product_prices] where [cscart_product_prices].product_id=[cscart_products].product_id and lower_limit=1),
priceRRP = cscart_products.[list_price],
weight = cscart_products.weight,
minQuantity = cscart_products.min_qty, --TRANSFORM 0 to null
maxQuantity = cscart_products.max_qty, --TRANSFORM 0 to null
noTax = cscart_products.tax_ids, --TRANSFORM '' to no tax
createDate = cscart_products.updated_timestamp, --TRANSFORM from unix timestamp
isHidden = cscart_products.status, --TRANSFORM D -> true

-- Tags
free_shipping = cscart_products.free_shipping, --TRANSFORM from "N" "Y"
is_vegetarian = cscart_products.is_vegetarian,
is_vegan = cscart_products.is_vegan,


filename = (
select top 1 [image_path] 
from [Dve-test7].[dbo].[cscart_images_links] 
join [Dve-test7].[dbo].[cscart_images] 
on [cscart_images_links].detailed_id = [cscart_images].image_id  
where [object_id]=[cscart_products].product_id and [object_type] = 'product'
)

from [Dve-test7].[dbo].cscart_products
join [Dve-test7].[dbo].[cscart_product_descriptions] 
on cscart_products.Product_id = [cscart_product_descriptions].Product_id


/*
where [cscart_products].[product_id]=6747
price = cscart_products.[list_price] - (cscart_products.[list_price] * [price_percentage] / 100),
*/