SELECT TOP 1000

-- ORDER
ID = cscart_orders.order_id,
OrderDate = cscart_orders.timestamp,
OrderTotal = cscart_orders.total,
SystemStatus =  cscart_orders.status, -- TRANSFORM TO NEW STATUSES
StatusID =  cscart_orders.status, -- TRANSFORM TO NEW STATUSES
BasketTotal = cscart_orders.subtotal,
FreightTotal = cscart_orders.shipping_cost,
TaxTotal = 'MNBI',
Code = NEWID(),
AgentData = cscart_orders.ip_address,
AccountID = cscart_orders.user_id,  -- TRANSFORM  999999 & 0 -> null
ShippingMethod = (select shipping from [Dve-test7].[dbo].[cscart_shipping_descriptions] where shipping_id=shipping_ids),
PaymentType = (select payment from [Dve-test7].[dbo].[cscart_payment_descriptions] where [cscart_payment_descriptions].payment_id=[cscart_orders].payment_id),
Weight = 'MNBI',
AmexSurcharge = cscart_orders.amx_surch,

-- CUSTOMER DETAILS
Cust_OrderID = cscart_orders.order_id,
Cust_FirstName = cscart_orders.b_firstname,
Cust_Surname = cscart_orders.b_lastname,
Cust_Phone = cscart_orders.b_phone,
Cust_Email = cscart_orders.email,
Cust_Address1 = cscart_orders.b_address,
Cust_Address2 = cscart_orders.b_address_2,
Cust_Address3 = cscart_orders.b_city,
Cust_Address4 = cscart_orders.b_state,
Cust_Postcode = cscart_orders.b_zipcode,
Cust_CountryID = cscart_orders.b_country, --TRANSFORM Code --> ID
Cust_Type = 0,
Cust_Company = cscart_orders.company,
Cust_IsMailer = 0,

-- DELIVERY DETAILS
Del_OrderID = cscart_orders.order_id,
Del_FirstName = cscart_orders.s_firstname,
Del_Surname = cscart_orders.s_lastname,
Del_Phone = cscart_orders.s_phone,
Del_Email = cscart_orders.email,
Del_Address1 = cscart_orders.s_address,
Del_Address2 = cscart_orders.s_address_2,
Del_Address3 = cscart_orders.s_city,
Del_Address4 = cscart_orders.s_state,
Del_Postcode = cscart_orders.s_zipcode,
Del_CountryID = cscart_orders.s_country, --TRANSFORM Code --> ID
Del_Type = 1,
Del_Company = cscart_orders.company,
Del_DeliveryInstructions = cscart_orders.notes,
Del_IsMailer = 0
      
FROM [Dve-test7].[dbo].[cscart_orders]
order by order_id desc