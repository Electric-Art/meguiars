
select files, hits = count(*)
from
(
select 

files = (
select count(*)
from [Dve-test7].[dbo].[cscart_images_links] 
join [Dve-test7].[dbo].[cscart_images] 
on [cscart_images_links].detailed_id = [cscart_images].image_id  
where [object_id]=[cscart_products].product_id and [object_type] = 'product'
)

from [Dve-test7].[dbo].cscart_products
) t
group by files


select 

product_id,
name = (select [cscart_product_descriptions].product from [Dve-test7].[dbo].[cscart_product_descriptions] where [cscart_product_descriptions].product_id = [cscart_products].product_id)

from [Dve-test7].[dbo].cscart_products
where
(
select count(*)
from [Dve-test7].[dbo].[cscart_images_links] 
join [Dve-test7].[dbo].[cscart_images] 
on [cscart_images_links].detailed_id = [cscart_images].image_id  
where [object_id]=[cscart_products].product_id and [object_type] = 'product'
) > 1