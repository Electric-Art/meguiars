/****** Script for SelectTopNRows command from SSMS  ******/
SELECT
      [status]
     ,count(*) as hits
	 ,statusname = (select description from [Dve-test7].[dbo].[cscart_status_descriptions] where status = [Dve-test7].[dbo].[cscart_orders].status and  type = 'O')
  FROM [Dve-test7].[dbo].[cscart_orders]
group by [status]