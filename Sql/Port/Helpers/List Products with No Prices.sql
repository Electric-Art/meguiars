select -- top 10
ID = cscart_products.product_id, 
ProductID = cscart_products.product_id, 
Name = [cscart_product_descriptions].[product],
price = (select [price]  from [Dve-test7].[dbo].[cscart_product_prices] where [cscart_product_prices].product_id=[cscart_products].product_id and lower_limit=1),
priceRRP = cscart_products.[list_price],
weight = cscart_products.weight,
minQuantity = case when cscart_products.min_qty = 0 then null else cscart_products.min_qty end, --TRANSFORM 0 to null
maxQuantity = case when cscart_products.max_qty = 0 then null else cscart_products.max_qty end, --TRANSFORM 0 to null
noTax = case when cscart_products.tax_ids = '' then 1 else 0 end, --TRANSFORM '' to no tax
createDate = dateadd(S, case when cscart_products.timestamp = 0 then 1355230800 else cscart_products.timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
modifieddate = dateadd(S, case when cscart_products.updated_timestamp = 0 then 1440060778 else cscart_products.updated_timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
isHidden = case when cscart_products.status = 'D' then 1 else 0 end --TRANSFORM D -> true
from [Dve-test7].[dbo].cscart_products
join [Dve-test7].[dbo].[cscart_product_descriptions] 
on cscart_products.Product_id = [cscart_product_descriptions].Product_id
where (select [price]  from [Dve-test7].[dbo].[cscart_product_prices] where [cscart_product_prices].product_id=[cscart_products].product_id and lower_limit=1) is null