/*
Select ID, 
CountryID = (Select CountryId from AccountDetail where OrderID=[order].ID and Type=1),
StateID = (Select ID from State where name = (Select address4 from AccountDetail where OrderID=[order].ID and Type=1 and CountryID=1) collate SQL_Latin1_General_CP1_CI_AS)
from [order]
where id = 97337
*/

update [order]
set 
CountryID = (Select CountryId from AccountDetail where OrderID=[order].ID and Type=1),
StateID = (Select ID from State where name = (Select address4 from AccountDetail where OrderID=[order].ID and Type=1 and CountryID=1) collate SQL_Latin1_General_CP1_CI_AS)

where id <= 97424
