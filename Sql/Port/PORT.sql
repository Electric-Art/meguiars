/*
   Thursday, June 30, 20165:40:21 PM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Product
	DROP CONSTRAINT DF_Product_createDate
GO
ALTER TABLE dbo.Product
	DROP CONSTRAINT DF_Product_isHidden
GO
CREATE TABLE dbo.Tmp_Product
	(
	ID int NOT NULL,
	name varchar(500) NOT NULL,
	nameUrl varchar(500) NULL,
	titleName varchar(500) NULL,
	menuName varchar(500) NULL,
	intro varchar(MAX) NULL,
	blurb varchar(MAX) NULL,
	activeIngredients varchar(MAX) NULL,
	formulation varchar(MAX) NULL,
	dosage varchar(MAX) NULL,
	warnings varchar(MAX) NULL,
	specifications varchar(MAX) NULL,
	freeFrom varchar(MAX) NULL,
	interactions varchar(MAX) NULL,
	availability varchar(MAX) NULL,
	sku varchar(50) NULL,
	price money NULL,
	specialPrice money NULL,
	aliases varchar(MAX) NULL,
	aliasesUrl varchar(MAX) NULL,
	createDate datetime NOT NULL,
	modifiedDate datetime NULL,
	isHidden int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Product SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Product ADD CONSTRAINT
	DF_Product_createDate DEFAULT (getdate()) FOR createDate
GO
ALTER TABLE dbo.Tmp_Product ADD CONSTRAINT
	DF_Product_isHidden DEFAULT ((0)) FOR isHidden
GO
IF EXISTS(SELECT * FROM dbo.Product)
	 EXEC('INSERT INTO dbo.Tmp_Product (ID, name, nameUrl, titleName, menuName, intro, blurb, activeIngredients, formulation, dosage, warnings, specifications, freeFrom, interactions, availability, sku, price, specialPrice, aliases, aliasesUrl, createDate, modifiedDate, isHidden)
		SELECT ID, name, nameUrl, titleName, menuName, intro, blurb, activeIngredients, formulation, dosage, warnings, specifications, freeFrom, interactions, availability, sku, price, specialPrice, aliases, aliasesUrl, createDate, modifiedDate, isHidden FROM dbo.Product WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.ListCache
	DROP CONSTRAINT FK_ListCache_Product
GO
ALTER TABLE dbo.ProductRelation
	DROP CONSTRAINT FK_ProductRelation_Product
GO
ALTER TABLE dbo.ProductRelation
	DROP CONSTRAINT FK_ProductRelation_Product1
GO
ALTER TABLE dbo.Tag_Product
	DROP CONSTRAINT FK_Tag_Product_Product
GO
ALTER TABLE dbo.Symptom_Product
	DROP CONSTRAINT FK_Symptom_Product_Product
GO
ALTER TABLE dbo.QandA
	DROP CONSTRAINT FK_QandA_Product
GO
ALTER TABLE dbo.Ingredient
	DROP CONSTRAINT FK_Ingredient_Product
GO
ALTER TABLE dbo.SiteResource
	DROP CONSTRAINT FK_SiteResource_Product
GO
ALTER TABLE dbo.Favourite
	DROP CONSTRAINT FK_Favourite_Product
GO
ALTER TABLE dbo.Page
	DROP CONSTRAINT FK_Page_Product
GO
ALTER TABLE dbo.StockNotice
	DROP CONSTRAINT FK_StockNotice_Product
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Product
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT FK_Variant_Product
GO
DROP TABLE dbo.Product
GO
EXECUTE sp_rename N'dbo.Tmp_Product', N'Product', 'OBJECT' 
GO
ALTER TABLE dbo.Product ADD CONSTRAINT
	PK_Product PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	FK_Variant_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Variant SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StockNotice ADD CONSTRAINT
	FK_StockNotice_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.StockNotice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Page ADD CONSTRAINT
	FK_Page_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Page SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Favourite ADD CONSTRAINT
	FK_Favourite_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Favourite SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SiteResource ADD CONSTRAINT
	FK_SiteResource_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.SiteResource SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ingredient ADD CONSTRAINT
	FK_Ingredient_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Ingredient SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.QandA ADD CONSTRAINT
	FK_QandA_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.QandA SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Symptom_Product ADD CONSTRAINT
	FK_Symptom_Product_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Symptom_Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag_Product ADD CONSTRAINT
	FK_Tag_Product_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Tag_Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ProductRelation ADD CONSTRAINT
	FK_ProductRelation_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductRelation ADD CONSTRAINT
	FK_ProductRelation_Product1 FOREIGN KEY
	(
	RelatedProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductRelation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ListCache ADD CONSTRAINT
	FK_ListCache_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ListCache SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
/*
   Friday, July 01, 20168:51:20 AM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT FK_Variant_Product
GO
ALTER TABLE dbo.Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT DF_Variant_noGst
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT DF_Variant_createDate
GO
CREATE TABLE dbo.Tmp_Variant
	(
	ID int NOT NULL,
	ProductID int NOT NULL,
	name varchar(500) NOT NULL,
	sku varchar(50) NULL,
	price money NOT NULL,
	priceRRP money NULL,
	priceMargin float(53) NULL,
	weight decimal(8, 2) NULL,
	minQuantity int NULL,
	maxQuantity int NULL,
	noTax bit NOT NULL,
	stockNo int NULL,
	shipDays int NULL,
	createDate datetime NOT NULL,
	modifiedDate datetime NULL,
	isHidden int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Variant SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Variant ADD CONSTRAINT
	DF_Variant_noGst DEFAULT ((0)) FOR noTax
GO
ALTER TABLE dbo.Tmp_Variant ADD CONSTRAINT
	DF_Variant_createDate DEFAULT (getdate()) FOR createDate
GO
IF EXISTS(SELECT * FROM dbo.Variant)
	 EXEC('INSERT INTO dbo.Tmp_Variant (ID, ProductID, name, sku, price, priceRRP, priceMargin, weight, minQuantity, maxQuantity, noTax, stockNo, shipDays, createDate, modifiedDate, isHidden)
		SELECT ID, ProductID, name, sku, price, priceRRP, priceMargin, weight, minQuantity, maxQuantity, noTax, stockNo, shipDays, createDate, modifiedDate, isHidden FROM dbo.Variant WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Variant
GO
ALTER TABLE dbo.Price
	DROP CONSTRAINT FK_Price_Variant
GO
ALTER TABLE dbo.StockNotice
	DROP CONSTRAINT FK_StockNotice_Variant
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Variant
GO
DROP TABLE dbo.Variant
GO
EXECUTE sp_rename N'dbo.Tmp_Variant', N'Variant', 'OBJECT' 
GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	PK_Variant PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	FK_Variant_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StockNotice ADD CONSTRAINT
	FK_StockNotice_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.StockNotice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Price ADD CONSTRAINT
	FK_Price_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Price SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Basket SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
/*
   Friday, July 01, 20161:55:59 PM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Account
	DROP CONSTRAINT DF_Account_saveCreditCard
GO
CREATE TABLE dbo.Tmp_Account
	(
	ID int NOT NULL,
	UserName varchar(50) NOT NULL,
	createDate datetime NOT NULL,
	isAdmin bit NOT NULL,
	isTrade bit NOT NULL,
	isBlocked bit NOT NULL,
	creditCardToken varchar(500) NULL,
	saveCreditCard bit NOT NULL,
	firstName varchar(500) NULL,
	lastName varchar(500) NULL,
	gender int NULL,
	dateOfBirth datetime NULL,
	channel varchar(500) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Account SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Account ADD CONSTRAINT
	DF_Account_saveCreditCard DEFAULT ((1)) FOR saveCreditCard
GO
IF EXISTS(SELECT * FROM dbo.Account)
	 EXEC('INSERT INTO dbo.Tmp_Account (ID, UserName, createDate, isAdmin, isTrade, isBlocked, creditCardToken, saveCreditCard, firstName, lastName, gender, dateOfBirth, channel)
		SELECT ID, UserName, createDate, isAdmin, isTrade, isBlocked, creditCardToken, saveCreditCard, firstName, lastName, gender, dateOfBirth, channel FROM dbo.Account WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Account
GO
ALTER TABLE dbo.Favourite
	DROP CONSTRAINT FK_Favourite_Account
GO
DROP TABLE dbo.Account
GO
EXECUTE sp_rename N'dbo.Tmp_Account', N'Account', 'OBJECT' 
GO
ALTER TABLE dbo.Account ADD CONSTRAINT
	PK_Account PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Favourite ADD CONSTRAINT
	FK_Favourite_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Favourite SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
/*
   Friday, July 01, 20162:10:38 PM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Account
GO
ALTER TABLE dbo.Account SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Status
GO
ALTER TABLE dbo.Status SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Country
GO
ALTER TABLE dbo.Country SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Location
GO
ALTER TABLE dbo.Location SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_ShippingMethod
GO
ALTER TABLE dbo.ShippingMethod SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_OrderDate
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_TaxTotal
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_InvoiceTotal_1
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_idx_1
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_Printed
GO
CREATE TABLE dbo.Tmp_Order
	(
	ID int NOT NULL,
	OrderDate datetime NOT NULL,
	OrderTotal money NOT NULL,
	SystemStatus int NOT NULL,
	StatusID int NULL,
	ProcessDate datetime NULL,
	CompleteDate datetime NULL,
	BasketTotal money NOT NULL,
	FreightTotal money NOT NULL,
	TaxTotal money NOT NULL,
	Invoice text COLLATE Latin1_General_CI_AS NULL,
	InvoiceTotal money NULL,
	idx int NULL,
	AcknowledgeDate datetime NULL,
	FeedbackRequestDate datetime NULL,
	FeedbackResponseDate datetime NULL,
	AdminNotes varchar(MAX) COLLATE Latin1_General_CI_AS NULL,
	Code uniqueidentifier NOT NULL,
	SessionKey varchar(50) NOT NULL,
	AgentData varchar(MAX) NULL,
	PromotionID int NULL,
	AccountID int NULL,
	ShippingMethodID int NULL,
	LocationID int NULL,
	CountryID int NULL,
	StateID int NULL,
	SystemNotes varchar(MAX) NULL,
	PaymentType int NULL,
	CustomerNotes varchar(MAX) NULL,
	VoucherPromotionID int NULL,
	Weight decimal(8, 2) NULL,
	Printed bit NOT NULL,
	TotalBeforeSurcharge money NULL,
	AmexSurcharge money NULL,
	AdminAdHocFreightTotal money NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Order SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_OrderDate DEFAULT (getdate()) FOR OrderDate
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_TaxTotal DEFAULT ((0)) FOR TaxTotal
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_InvoiceTotal_1 DEFAULT ((0)) FOR InvoiceTotal
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_idx_1 DEFAULT ((0)) FOR idx
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_Printed DEFAULT ((0)) FOR Printed
GO
IF EXISTS(SELECT * FROM dbo.[Order])
	 EXEC('INSERT INTO dbo.Tmp_Order (ID, OrderDate, OrderTotal, SystemStatus, StatusID, ProcessDate, CompleteDate, BasketTotal, FreightTotal, TaxTotal, Invoice, InvoiceTotal, idx, AcknowledgeDate, FeedbackRequestDate, FeedbackResponseDate, AdminNotes, Code, SessionKey, AgentData, PromotionID, AccountID, ShippingMethodID, LocationID, CountryID, StateID, SystemNotes, PaymentType, CustomerNotes, VoucherPromotionID, Weight, Printed, TotalBeforeSurcharge, AmexSurcharge, AdminAdHocFreightTotal)
		SELECT ID, OrderDate, OrderTotal, SystemStatus, StatusID, ProcessDate, CompleteDate, BasketTotal, FreightTotal, TaxTotal, Invoice, InvoiceTotal, idx, AcknowledgeDate, FeedbackRequestDate, FeedbackResponseDate, AdminNotes, Code, SessionKey, AgentData, PromotionID, AccountID, ShippingMethodID, LocationID, CountryID, StateID, SystemNotes, PaymentType, CustomerNotes, VoucherPromotionID, Weight, Printed, TotalBeforeSurcharge, AmexSurcharge, AdminAdHocFreightTotal FROM dbo.[Order] WITH (HOLDLOCK TABLOCKX)')
GO
ALTER TABLE dbo.OrderPayment
	DROP CONSTRAINT FK_OrderPayment_Order
GO
ALTER TABLE dbo.AccountDetail
	DROP CONSTRAINT FK_AccountDetail_Order
GO
ALTER TABLE dbo.OrderTax
	DROP CONSTRAINT FK_OrderTax_Order
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Order
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Order
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Promotion
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Promotion1
GO
ALTER TABLE dbo.Shipment
	DROP CONSTRAINT FK_Shipment_Order
GO
DROP TABLE dbo.[Order]
GO
EXECUTE sp_rename N'dbo.Tmp_Order', N'Order', 'OBJECT' 
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	PK_Order PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_ShippingMethod FOREIGN KEY
	(
	ShippingMethodID
	) REFERENCES dbo.ShippingMethod
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Country FOREIGN KEY
	(
	CountryID
	) REFERENCES dbo.Country
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Status FOREIGN KEY
	(
	StatusID
	) REFERENCES dbo.Status
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Shipment ADD CONSTRAINT
	FK_Shipment_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Shipment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Promotion FOREIGN KEY
	(
	VoucherPromotionID
	) REFERENCES dbo.Promotion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Promotion1 FOREIGN KEY
	(
	PromotionID
	) REFERENCES dbo.Promotion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Basket SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OrderTax ADD CONSTRAINT
	FK_OrderTax_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.OrderTax SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AccountDetail ADD CONSTRAINT
	FK_AccountDetail_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AccountDetail SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OrderPayment ADD CONSTRAINT
	FK_OrderPayment_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.OrderPayment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

print '########   IDENTITIES REMOVED   #######'

SELECT 
TagID, 
ProductID
Into [Port_TagAssignments]
FROM [Tag_Product]

delete from QandA
delete from Ingredient
delete from ProductRelation
delete from Symptom_Product
delete from ListCache
update Page set ProductID=null where ProductID is not null
delete from Favourite where ProductID is not null
delete from StockNotice
delete from Promotion where ProductID is not null
delete from Tag_Product
delete from SiteResource where ProductID is not null
delete from Basket
delete from Price
delete from Variant
delete from Product
delete from OrderPayment
delete from OrderTax
delete from Favourite
delete from Promotion where OrderID is not null
delete from Shipment
delete from AccountDetail
delete from [Order]
delete from Account

IF not EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Port'
      AND Object_ID = Object_ID(N'Product'))
	BEGIN
		ALTER TABLE dbo.[Product] ADD
			Port bit NULL

		
		PRINT 'Port field added'
	END
Else
	BEGIN
		PRINT 'Port field already there'
	END

go


/*
cscart_products
done [out_of_stock_cnt]
done      ,[clearance_cnt]
	  we need to wipe all clearance assignments then before the final por
done	  Save from product ID now
Print 'DO MAY UPDATES ABOVE!!!'
*/

-- Product
insert into Product (ID,name,intro,blurb,Specifications,sku,createDate, modifiedDate, isHidden, Port)
select -- top 10
ID = cscart_products.product_id, 
Name = [cscart_product_descriptions].[product],
Intro = [cscart_product_descriptions].[short_description],
Blurb = [cscart_product_descriptions].[full_description],
Specifications = [cscart_product_descriptions].[meta_description],
sku = cscart_products.product_code, 
createDate = dateadd(S, case when cscart_products.timestamp = 0 then 1355230800 else cscart_products.timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
modifieddate = dateadd(S, case when cscart_products.updated_timestamp = 0 then 1440060778 else cscart_products.updated_timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
isHidden = case when cscart_products.status = 'A' then 0 else 1 end,
Port = 1
from [dve-port].[dbo].cscart_products
join [dve-port].[dbo].[cscart_product_descriptions] 
on cscart_products.Product_id = [cscart_product_descriptions].Product_id

-- Unknown Product
insert into Product (ID,name,blurb,sku,createDate, modifiedDate, isHidden, Port)
select -- top 10
ID = (select top 1 ID from Product order by ID Desc)+1, 
Name = 'Unknown Product',
Blurb = 'Product placeholder for order items that found no product',
sku = 'UnknownProduct', 
createDate = GETDATE(),	
modifieddate = GETDATE(),	
isHidden = 1,
Port = 1


-- Variant
insert into Variant(ID, ProductID, name, price,priceRRP,weight,minQuantity,maxQuantity,noTax,createDate,modifieddate,isHidden)
select -- top 10
ID = cscart_products.product_id, 
ProductID = cscart_products.product_id, 
Name = [cscart_product_descriptions].[product],
price = (select [price]  from [dve-port].[dbo].[cscart_product_prices] where [cscart_product_prices].product_id=[cscart_products].product_id and lower_limit=1),
priceRRP = cscart_products.[list_price],
weight = cscart_products.weight,
minQuantity = case when cscart_products.min_qty = 0 then null else cscart_products.min_qty end, --TRANSFORM 0 to null
maxQuantity = case when cscart_products.max_qty = 0 then null else cscart_products.max_qty end, --TRANSFORM 0 to null
noTax = case when cscart_products.tax_ids = '' then 1 else 0 end, --TRANSFORM '' to no tax
createDate = dateadd(S, case when cscart_products.timestamp = 0 then 1355230800 else cscart_products.timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
modifieddate = dateadd(S, case when cscart_products.updated_timestamp = 0 then 1440060778 else cscart_products.updated_timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
isHidden = case when cscart_products.out_of_stock_cnt=1 then 1 else 0 end --TRANSFORM out_of_stock_cnt
from [dve-port].[dbo].cscart_products
join [dve-port].[dbo].[cscart_product_descriptions] 
on cscart_products.Product_id = [cscart_product_descriptions].Product_id
where (select [price]  from [dve-port].[dbo].[cscart_product_prices] where [cscart_product_prices].product_id=[cscart_products].product_id and lower_limit=1) is not null

-- Unknown Variant
insert into Variant(ID, ProductID, name, price,priceRRP,weight,noTax,createDate,modifieddate,isHidden)
select -- top 10
ID = (select ID from Product where sku = 'UnknownProduct'), 
ProductID = (select ID from Product where sku = 'UnknownProduct'), 
Name = 'Unknown Product',
price = 0,
priceRRP = 0,
weight = 0,
noTax = 0,
createDate = GETDATE(), 
modifieddate = GETDATE(), 
isHidden = 1

-- Prices
insert into Price(VariantID,amount,quantity)
SELECT variantid = [product_id],
      amount = [price],
      quantity = [lower_limit]
  FROM [dve-port].[dbo].[cscart_product_prices]
  where lower_limit > 1 and exists(select * from Variant where Variant.ID = [dve-port].[dbo].[cscart_product_prices].product_id)


-- SiteResources
insert into SiteResource (Filename,SiteResourceType, ObjectId, ObjectType, ProductID, OrderNo, allowPostCard)
select -- top 10
Filename = (
			select top 1 [image_path] 
			from [dve-port].[dbo].[cscart_images_links] 
			join [dve-port].[dbo].[cscart_images] 
			on [cscart_images_links].detailed_id = [cscart_images].image_id  
			where [object_id]=[cscart_products].product_id and [object_type] = 'product'
			),
SiteResourceType = 0,
ObjectId = cscart_products.product_id,
ObjectType = 1,
ProductID =  cscart_products.product_id,
OrderNo = 1,
allowPostCard = 0
from [dve-port].[dbo].cscart_products
where (
		select top 1 [image_path] 
		from [dve-port].[dbo].[cscart_images_links] 
		join [dve-port].[dbo].[cscart_images] 
		on [cscart_images_links].detailed_id = [cscart_images].image_id  
		where [object_id]=[cscart_products].product_id and [object_type] = 'product'
	) is not null


DECLARE @free_shipping_id int; 
select @free_shipping_id = ID from Tag where SystemTag = 8
insert into Tag_Product (TagID,ProductID,Type)
select 
TagID = @free_shipping_id,
ProductID = cscart_products.product_id,
Type = 0
from [dve-port].[dbo].cscart_products
where free_shipping = 'Y'

DECLARE @is_vegetarian_id int; 
select @is_vegetarian_id = ID from Tag where SystemTag = 9

insert into Tag_Product (TagID,ProductID,Type)
select 
TagID = @is_vegetarian_id,
ProductID = cscart_products.product_id,
Type = 0
from [dve-port].[dbo].cscart_products
where is_vegetarian = 'Y'

DECLARE @is_vegan_id int; 
select @is_vegan_id = ID from Tag where SystemTag = 10
insert into Tag_Product (TagID,ProductID,Type)
select 
TagID = @is_vegan_id,
ProductID = cscart_products.product_id,
Type = 0
from [dve-port].[dbo].cscart_products
where is_vegan = 'Y'

DECLARE @clearance_id int; 
select @clearance_id = ID from Tag where SystemTag = 7
delete from Tag_Product where TagID=@clearance_id
insert into Tag_Product (TagID,ProductID,Type)
select 
TagID = @clearance_id,
ProductID = cscart_products.product_id,
Type = 0
from [dve-port].[dbo].cscart_products
where [clearance_cnt] = 1


insert into Tag_Product (TagID,ProductID,Type)
select 
TagID,
ProductID,
Type = 0
from [Port_TagAssignments]
where not exists(select * from Tag_Product where [Port_TagAssignments].TagID = Tag_Product.TagID and [Port_TagAssignments].ProductID = Tag_Product.ProductID)
and exists(select * from Product where Product.ID = [Port_TagAssignments].ProductID)

DROP TABLE [Port_TagAssignments]


-- Replaces

update Product
set name = REPLACE(name,'���','''') 
where name like '%���%'

update Product
set Specifications = REPLACE(Specifications,'���','''') 
where Specifications like '%���%'

update Product
set Intro = REPLACE(Intro,'���','''') 
where Intro like '%���%'

update SiteResource
set FileName = REPLACE(FileName,'���','''') 
where FileName like '%���%'

update Product
set name = REPLACE(name,'+�a+�','cai') 
where name like '%+�a+�%'

update Product
set Specifications = REPLACE(Specifications,'+�a+�','cai') 
where Specifications like '%+�a+�%'

update Product
set Intro = REPLACE(Intro,'+�a+�','cai') 
where Intro like '%+�a+�%'

update SiteResource
set FileName = REPLACE(FileName,'+�a+�','cai') 
where FileName like '%+�a+�%'


print '####### Product Port Done #######'

IF OBJECT_ID('Port_Users', 'U') IS NOT NULL 
	DROP TABLE [dbo].[Port_Users]
GO

CREATE TABLE [dbo].[Port_Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[email] [varchar](50) NOT NULL,
	[pass] [varchar](32) NULL,
	[salt] [varchar](10) NULL,
	[hasPorted] [bit] NOT NULL,
	[portDate] [datetime] NULL,
 CONSTRAINT [PK_Port_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


insert into Account (ID,UserName,createdate,isAdmin,isTrade,isBlocked,firstname,lastname)
select -- top 10
ID = cscart_users.user_id,
UserName = cscart_users.email,
createdate = dateadd(S, cscart_users.timestamp, '1970-01-01'),
isAdmin = 0,
isTrade = 0,
isBlocked = case when cscart_users.status = 'A' then 0 else 1 end, -- TRANSFORM A -> false
firstname = cscart_users.firstname,
lastname = cscart_users.lastname
from [dve-port].[dbo].cscart_users
where user_type='C'
--order by user_id desc

print '####### Accounts Port Done #######'

insert into [Port_Users] ([AccountID],[email], [pass],[salt],[hasPorted])
select -- top 10
[AccountID] = cscart_users.user_id,
[email] = cscart_users.email,
[pass] = cscart_users.password,
[salt] = cscart_users.salt,
[hasPorted] = 0

from [dve-port].[dbo].cscart_users
where user_type='C'
--order by user_id desc

print '####### Accounts Pass Port Done #######'

update Account
set UserName = 'dup_z_' + UserName
where ID in (
select
AccountID = (select top 1 ID from Account where UserName=T0.UserName order by ID)
from
(select UserName
from Account
group by UserName
having count(*)>1) as T0)

print '####### Accounts Duplicate Updates Done #######'



CREATE TABLE [dbo].[Port_StatusAssign](
	[status] [nchar](1) COLLATE Latin1_General_CI_AS NOT NULL ,
	[SystemStatus] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[Note] [varchar](50) COLLATE Latin1_General_CI_AS NULL,
 CONSTRAINT [PK_Port_StatusAssign] PRIMARY KEY CLUSTERED 
(
	[status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'A', 2, 11, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'B', 2, 9, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'C', 4, 4, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'D', 3, 10, N'Declined')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'E', 2, 26, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'F', 3, 10, N'Failed/Declined')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'G', 2, 13, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'H', 1, 15, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'I', 3, 10, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'J', 2, 16, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'K', 2, 17, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'L', 3, 10, N'Cancelled - Fraud')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'M', 2, 20, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'N', 3, 10, N'Cancelled - Unknown')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'O', 1, 5, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'P', 2, 3, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'Q', 3, 10, N'Cancelled - Unknown')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'R', 3, 10, N'Cancelled - Product Discontinued')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'S', 3, 10, N'Cancelled - Unavailable')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'U', 3, 10, N'Cancelled - Unknown')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'V', 2, 23, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'W', 3, 10, N'Cancelled - Unknown')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'X', 2, 17, N'Picking')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'Z', 3, 10, N'Cancelled - Duplicate Order')
GO


IF not EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Port'
      AND Object_ID = Object_ID(N'Order'))
	BEGIN
		ALTER TABLE dbo.[Order] ADD
			Port bit NULL
		PRINT 'Port field added'
	END
Else
	BEGIN
		PRINT 'Port field already there'
	END

go

insert into [order](ID,OrderDate,OrderTotal,SystemStatus,StatusID,SystemNotes,BasketTotal,FreightTotal,TaxTotal,SessionKey,Code,AgentData,AccountID,ShippingMethodID,PaymentType,Weight,AmexSurcharge,Port)
SELECT --TOP 10
-- ORDER
ID = cscart_orders.order_id,
OrderDate = dateadd(S, cscart_orders.timestamp, '1970-01-01'),
OrderTotal = cscart_orders.total,
SystemStatus =  (select [SystemStatus] from [Port_StatusAssign] where [Port_StatusAssign].status = cscart_orders.status), -- TRANSFORM TO NEW STATUSES
StatusID =  (select [StatusID] from [Port_StatusAssign] where [Port_StatusAssign].status = cscart_orders.status), -- TRANSFORM TO NEW STATUSES
SystemNotes = (select [Note] from [Port_StatusAssign] where [Port_StatusAssign].status = cscart_orders.status),
BasketTotal = cscart_orders.subtotal,
FreightTotal = cscart_orders.shipping_cost,
TaxTotal = cscart_orders.total / 11, -- Can't find so divide by 11 for gst component of full amount
SessionKey = NEWID(),
Code = NEWID(),
AgentData = cscart_orders.ip_address,
AccountID = case	when (cscart_orders.user_id = 999999) THEN null
					when (cscart_orders.user_id = 0) THEN null  -- TRANSFORM  999999 & 0 -> null
					when not exists(select * from Account where Account.ID=cscart_orders.user_id) THEN null
					else cscart_orders.user_id
						end,
ShippingMethodID = case when (ISNUMERIC(shipping_ids) = 1) THEN 
						case 
							when CAST(shipping_ids AS INT) = 1 then 1
							when CAST(shipping_ids AS INT) = 3 then 1
							when CAST(shipping_ids AS INT) = 4 then 1
							when CAST(shipping_ids AS INT) = 5 then 4
							when CAST(shipping_ids AS INT) = 6 then 3
							when CAST(shipping_ids AS INT) = 7 then 5
							when CAST(shipping_ids AS INT) = 9 then 2
							else 1
						end
					ELSE 1 END,
PaymentType =  
						case 
							when payment_id = 6 then 0
							when payment_id = 1 then 0
							when payment_id = 13 then 1
							when payment_id = 7 then 2
							when payment_id = 12 then 2
							when payment_id = 9 then 2
							when payment_id = 2 then 0
							when payment_id = 10 then 2
							when payment_id = 3 then 2
							when payment_id = 8 then 2
							when payment_id = 5 then 2
							when payment_id = 4 then 0
							when payment_id = 14 then 0
							when payment_id = 11 then 2
							else null
						end,
Weight = null, --'MNBI'
AmexSurcharge = case when cscart_orders.amx_surch=0 then null
					 else cscart_orders.amx_surch
					 end,
Port = 1
FROM [dve-port].[dbo].[cscart_orders]


insert into [AccountDetail](OrderID,FirstName,Surname,Mobile,Email,Address1,Address2,Address3,Address4,Postcode,CountryID,Country,Type,Company,IsMailer)
-- CUSTOMER DETAILS
select -- top 10
Cust_OrderID = cscart_orders.order_id,
Cust_FirstName = cscart_orders.b_firstname,
Cust_Surname = cscart_orders.b_lastname,
Cust_Phone = cscart_orders.b_phone,
Cust_Email = cscart_orders.email,
Cust_Address1 = cscart_orders.b_address,
Cust_Address2 = cscart_orders.b_address_2,
Cust_Address3 = cscart_orders.b_city,
Cust_Address4 = case when exists(select state_id from [dve-port].[dbo].[cscart_states] where code=[b_state] and [country_code]=[b_country])
				then (select [state] from [dve-port].[dbo].[cscart_state_descriptions] where state_id = (select state_id from [dve-port].[dbo].[cscart_states] where code=[b_state] and [country_code]=[b_country]))
				else [b_state]
				end,
Cust_Postcode = cscart_orders.b_zipcode,
Cust_CountryID = (Select ID from Country where name=(SELECT [country] FROM [dve-port].[dbo].[cscart_country_descriptions] where [code] = cscart_orders.b_country) COLLATE SQL_Latin1_General_CP1_CI_AS),
Cust_Country = (Select name from Country where name=(SELECT [country] FROM [dve-port].[dbo].[cscart_country_descriptions] where [code] = cscart_orders.b_country) COLLATE SQL_Latin1_General_CP1_CI_AS),
Cust_Type = 0,
Cust_Company = cscart_orders.company,
Cust_IsMailer = case when (exists(select * FROM [DveMaillist].[dbo].[members_export_b309886323] where ["Email Address"]=cscart_orders.email COLLATE SQL_Latin1_General_CP1_CI_AS)) then 1 else 0 end
FROM [dve-port].[dbo].[cscart_orders]


insert into [AccountDetail](OrderID,FirstName,Surname,Mobile,Email,Address1,Address2,Address3,Address4,Postcode,CountryID,Country,Type,Company,DeliveryInstructions,IsMailer)
-- CUSTOMER DETAILS
select -- top 10
Del_OrderID = cscart_orders.order_id,
Del_FirstName = cscart_orders.s_firstname,
Del_Surname = cscart_orders.s_lastname,
Del_Phone = cscart_orders.s_phone,
Del_Email = cscart_orders.email,
Del_Address1 = cscart_orders.s_address,
Del_Address2 = cscart_orders.s_address_2,
Del_Address3 = cscart_orders.s_city,
Del_Address4 = case when exists(select state_id from [dve-port].[dbo].[cscart_states] where code=[s_state] and [country_code]=[s_country])
				then (select [state] from [dve-port].[dbo].[cscart_state_descriptions] where state_id = (select state_id from [dve-port].[dbo].[cscart_states] where code=[s_state] and [country_code]=[s_country]))
				else [s_state]
				end,
Del_Postcode = cscart_orders.s_zipcode,
Del_CountryID = (Select ID from Country where name=(SELECT [country] FROM [dve-port].[dbo].[cscart_country_descriptions] where [code] = cscart_orders.s_country) COLLATE SQL_Latin1_General_CP1_CI_AS),
Del_Country = (Select name from Country where name=(SELECT [country] FROM [dve-port].[dbo].[cscart_country_descriptions] where [code] = cscart_orders.s_country) COLLATE SQL_Latin1_General_CP1_CI_AS),
Del_Type = 1,
Del_Company = cscart_orders.company,
Del_DeliveryInstructions = cscart_orders.notes,
Del_IsMailer = 0
FROM [dve-port].[dbo].[cscart_orders]


DECLARE @unknownid int; 
select @unknownid = ID from Product where sku = 'UnknownProduct'

insert into [Basket] (OrderID,VariantID,Price,Quantity,Sessionkey,CreateDate)
--Basket
SELECT --TOP 10
--ID = [item_id],
OrderID = [order_id],

VariantID = case when (exists(select * from [variant] where ID=[product_id])) then [product_id]
				 when (exists(select * from [product] where sku=[cscart_order_details].product_code COLLATE SQL_Latin1_General_CP1_CI_AS)) then (select top 1 ID from [product] where sku=[cscart_order_details].product_code COLLATE SQL_Latin1_General_CP1_CI_AS)
				 else @unknownid
			end
,
Price = [price],
Quantity = [amount],
Sessionkey =  (select Sessionkey from [order] where [order].ID = [order_id]),
CreateDate = (select OrderDate from [order] where [order].ID = [order_id]) 
FROM [dve-port].[dbo].[cscart_order_details]




DROP TABLE [Port_StatusAssign]


print '####### Order Port Done #######'

/*
   Thursday, June 30, 20165:46:32 PM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Product
	DROP CONSTRAINT DF_Product_createDate
GO
ALTER TABLE dbo.Product
	DROP CONSTRAINT DF_Product_isHidden
GO
CREATE TABLE dbo.Tmp_Product
	(
	ID int NOT NULL IDENTITY (1, 1),
	name varchar(500) NOT NULL,
	nameUrl varchar(500) NULL,
	titleName varchar(500) NULL,
	menuName varchar(500) NULL,
	intro varchar(MAX) NULL,
	blurb varchar(MAX) NULL,
	activeIngredients varchar(MAX) NULL,
	formulation varchar(MAX) NULL,
	dosage varchar(MAX) NULL,
	warnings varchar(MAX) NULL,
	specifications varchar(MAX) NULL,
	freeFrom varchar(MAX) NULL,
	interactions varchar(MAX) NULL,
	availability varchar(MAX) NULL,
	sku varchar(50) NULL,
	price money NULL,
	specialPrice money NULL,
	aliases varchar(MAX) NULL,
	aliasesUrl varchar(MAX) NULL,
	createDate datetime NOT NULL,
	modifiedDate datetime NULL,
	isHidden int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Product SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Product ADD CONSTRAINT
	DF_Product_createDate DEFAULT (getdate()) FOR createDate
GO
ALTER TABLE dbo.Tmp_Product ADD CONSTRAINT
	DF_Product_isHidden DEFAULT ((0)) FOR isHidden
GO
SET IDENTITY_INSERT dbo.Tmp_Product ON
GO
IF EXISTS(SELECT * FROM dbo.Product)
	 EXEC('INSERT INTO dbo.Tmp_Product (ID, name, nameUrl, titleName, menuName, intro, blurb, activeIngredients, formulation, dosage, warnings, specifications, freeFrom, interactions, availability, sku, price, specialPrice, aliases, aliasesUrl, createDate, modifiedDate, isHidden)
		SELECT ID, name, nameUrl, titleName, menuName, intro, blurb, activeIngredients, formulation, dosage, warnings, specifications, freeFrom, interactions, availability, sku, price, specialPrice, aliases, aliasesUrl, createDate, modifiedDate, isHidden FROM dbo.Product WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Product OFF
GO
ALTER TABLE dbo.ListCache
	DROP CONSTRAINT FK_ListCache_Product
GO
ALTER TABLE dbo.ProductRelation
	DROP CONSTRAINT FK_ProductRelation_Product
GO
ALTER TABLE dbo.ProductRelation
	DROP CONSTRAINT FK_ProductRelation_Product1
GO
ALTER TABLE dbo.Tag_Product
	DROP CONSTRAINT FK_Tag_Product_Product
GO
ALTER TABLE dbo.Symptom_Product
	DROP CONSTRAINT FK_Symptom_Product_Product
GO
ALTER TABLE dbo.QandA
	DROP CONSTRAINT FK_QandA_Product
GO
ALTER TABLE dbo.Ingredient
	DROP CONSTRAINT FK_Ingredient_Product
GO
ALTER TABLE dbo.SiteResource
	DROP CONSTRAINT FK_SiteResource_Product
GO
ALTER TABLE dbo.Favourite
	DROP CONSTRAINT FK_Favourite_Product
GO
ALTER TABLE dbo.Page
	DROP CONSTRAINT FK_Page_Product
GO
ALTER TABLE dbo.StockNotice
	DROP CONSTRAINT FK_StockNotice_Product
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Product
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT FK_Variant_Product
GO
DROP TABLE dbo.Product
GO
EXECUTE sp_rename N'dbo.Tmp_Product', N'Product', 'OBJECT' 
GO
ALTER TABLE dbo.Product ADD CONSTRAINT
	PK_Product PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	FK_Variant_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Variant SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StockNotice ADD CONSTRAINT
	FK_StockNotice_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.StockNotice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Page ADD CONSTRAINT
	FK_Page_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Page SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Favourite ADD CONSTRAINT
	FK_Favourite_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Favourite SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SiteResource ADD CONSTRAINT
	FK_SiteResource_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.SiteResource SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ingredient ADD CONSTRAINT
	FK_Ingredient_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Ingredient SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.QandA ADD CONSTRAINT
	FK_QandA_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.QandA SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Symptom_Product ADD CONSTRAINT
	FK_Symptom_Product_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Symptom_Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag_Product ADD CONSTRAINT
	FK_Tag_Product_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Tag_Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ProductRelation ADD CONSTRAINT
	FK_ProductRelation_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductRelation ADD CONSTRAINT
	FK_ProductRelation_Product1 FOREIGN KEY
	(
	RelatedProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductRelation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ListCache ADD CONSTRAINT
	FK_ListCache_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ListCache SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/*
   Friday, July 01, 20168:53:18 AM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT FK_Variant_Product
GO
ALTER TABLE dbo.Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT DF_Variant_noGst
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT DF_Variant_createDate
GO
CREATE TABLE dbo.Tmp_Variant
	(
	ID int NOT NULL IDENTITY (1, 1),
	ProductID int NOT NULL,
	name varchar(500) NOT NULL,
	sku varchar(50) NULL,
	price money NOT NULL,
	priceRRP money NULL,
	priceMargin float(53) NULL,
	weight decimal(8, 2) NULL,
	minQuantity int NULL,
	maxQuantity int NULL,
	noTax bit NOT NULL,
	stockNo int NULL,
	shipDays int NULL,
	createDate datetime NOT NULL,
	modifiedDate datetime NULL,
	isHidden int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Variant SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Variant ADD CONSTRAINT
	DF_Variant_noGst DEFAULT ((0)) FOR noTax
GO
ALTER TABLE dbo.Tmp_Variant ADD CONSTRAINT
	DF_Variant_createDate DEFAULT (getdate()) FOR createDate
GO
SET IDENTITY_INSERT dbo.Tmp_Variant ON
GO
IF EXISTS(SELECT * FROM dbo.Variant)
	 EXEC('INSERT INTO dbo.Tmp_Variant (ID, ProductID, name, sku, price, priceRRP, priceMargin, weight, minQuantity, maxQuantity, noTax, stockNo, shipDays, createDate, modifiedDate, isHidden)
		SELECT ID, ProductID, name, sku, price, priceRRP, priceMargin, weight, minQuantity, maxQuantity, noTax, stockNo, shipDays, createDate, modifiedDate, isHidden FROM dbo.Variant WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Variant OFF
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Variant
GO
ALTER TABLE dbo.Price
	DROP CONSTRAINT FK_Price_Variant
GO
ALTER TABLE dbo.StockNotice
	DROP CONSTRAINT FK_StockNotice_Variant
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Variant
GO
DROP TABLE dbo.Variant
GO
EXECUTE sp_rename N'dbo.Tmp_Variant', N'Variant', 'OBJECT' 
GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	PK_Variant PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	FK_Variant_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.StockNotice ADD CONSTRAINT
	FK_StockNotice_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.StockNotice SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Price ADD CONSTRAINT
	FK_Price_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Price SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Variant FOREIGN KEY
	(
	VariantID
	) REFERENCES dbo.Variant
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Basket SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
/*
   Friday, July 01, 20161:56:40 PM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Account
	DROP CONSTRAINT DF_Account_saveCreditCard
GO
CREATE TABLE dbo.Tmp_Account
	(
	ID int NOT NULL IDENTITY (1, 1),
	UserName varchar(50) NOT NULL,
	createDate datetime NOT NULL,
	isAdmin bit NOT NULL,
	isTrade bit NOT NULL,
	isBlocked bit NOT NULL,
	creditCardToken varchar(500) NULL,
	saveCreditCard bit NOT NULL,
	firstName varchar(500) NULL,
	lastName varchar(500) NULL,
	gender int NULL,
	dateOfBirth datetime NULL,
	channel varchar(500) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Account SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Account ADD CONSTRAINT
	DF_Account_saveCreditCard DEFAULT ((1)) FOR saveCreditCard
GO
SET IDENTITY_INSERT dbo.Tmp_Account ON
GO
IF EXISTS(SELECT * FROM dbo.Account)
	 EXEC('INSERT INTO dbo.Tmp_Account (ID, UserName, createDate, isAdmin, isTrade, isBlocked, creditCardToken, saveCreditCard, firstName, lastName, gender, dateOfBirth, channel)
		SELECT ID, UserName, createDate, isAdmin, isTrade, isBlocked, creditCardToken, saveCreditCard, firstName, lastName, gender, dateOfBirth, channel FROM dbo.Account WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Account OFF
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Account
GO
ALTER TABLE dbo.Favourite
	DROP CONSTRAINT FK_Favourite_Account
GO
DROP TABLE dbo.Account
GO
EXECUTE sp_rename N'dbo.Tmp_Account', N'Account', 'OBJECT' 
GO
ALTER TABLE dbo.Account ADD CONSTRAINT
	PK_Account PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Favourite ADD CONSTRAINT
	FK_Favourite_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Favourite SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
/*
   Friday, July 01, 20162:11:27 PM
   User: 
   Server: localhost
   Database: DveLive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Account
GO
ALTER TABLE dbo.Account SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Status
GO
ALTER TABLE dbo.Status SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Country
GO
ALTER TABLE dbo.Country SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Location
GO
ALTER TABLE dbo.Location SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_ShippingMethod
GO
ALTER TABLE dbo.ShippingMethod SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_OrderDate
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_TaxTotal
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_InvoiceTotal_1
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_idx_1
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_Printed
GO
CREATE TABLE dbo.Tmp_Order
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderDate datetime NOT NULL,
	OrderTotal money NOT NULL,
	SystemStatus int NOT NULL,
	StatusID int NULL,
	ProcessDate datetime NULL,
	CompleteDate datetime NULL,
	BasketTotal money NOT NULL,
	FreightTotal money NOT NULL,
	TaxTotal money NOT NULL,
	Invoice text COLLATE Latin1_General_CI_AS NULL,
	InvoiceTotal money NULL,
	idx int NULL,
	AcknowledgeDate datetime NULL,
	FeedbackRequestDate datetime NULL,
	FeedbackResponseDate datetime NULL,
	AdminNotes varchar(MAX) COLLATE Latin1_General_CI_AS NULL,
	Code uniqueidentifier NOT NULL,
	SessionKey varchar(50) NOT NULL,
	AgentData varchar(MAX) NULL,
	PromotionID int NULL,
	AccountID int NULL,
	ShippingMethodID int NULL,
	LocationID int NULL,
	CountryID int NULL,
	StateID int NULL,
	SystemNotes varchar(MAX) NULL,
	PaymentType int NULL,
	CustomerNotes varchar(MAX) NULL,
	VoucherPromotionID int NULL,
	Weight decimal(8, 2) NULL,
	Printed bit NOT NULL,
	TotalBeforeSurcharge money NULL,
	AmexSurcharge money NULL,
	AdminAdHocFreightTotal money NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Order SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_OrderDate DEFAULT (getdate()) FOR OrderDate
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_TaxTotal DEFAULT ((0)) FOR TaxTotal
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_InvoiceTotal_1 DEFAULT ((0)) FOR InvoiceTotal
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_idx_1 DEFAULT ((0)) FOR idx
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_Printed DEFAULT ((0)) FOR Printed
GO
SET IDENTITY_INSERT dbo.Tmp_Order ON
GO
IF EXISTS(SELECT * FROM dbo.[Order])
	 EXEC('INSERT INTO dbo.Tmp_Order (ID, OrderDate, OrderTotal, SystemStatus, StatusID, ProcessDate, CompleteDate, BasketTotal, FreightTotal, TaxTotal, Invoice, InvoiceTotal, idx, AcknowledgeDate, FeedbackRequestDate, FeedbackResponseDate, AdminNotes, Code, SessionKey, AgentData, PromotionID, AccountID, ShippingMethodID, LocationID, CountryID, StateID, SystemNotes, PaymentType, CustomerNotes, VoucherPromotionID, Weight, Printed, TotalBeforeSurcharge, AmexSurcharge, AdminAdHocFreightTotal)
		SELECT ID, OrderDate, OrderTotal, SystemStatus, StatusID, ProcessDate, CompleteDate, BasketTotal, FreightTotal, TaxTotal, Invoice, InvoiceTotal, idx, AcknowledgeDate, FeedbackRequestDate, FeedbackResponseDate, AdminNotes, Code, SessionKey, AgentData, PromotionID, AccountID, ShippingMethodID, LocationID, CountryID, StateID, SystemNotes, PaymentType, CustomerNotes, VoucherPromotionID, Weight, Printed, TotalBeforeSurcharge, AmexSurcharge, AdminAdHocFreightTotal FROM dbo.[Order] WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Order OFF
GO
ALTER TABLE dbo.OrderPayment
	DROP CONSTRAINT FK_OrderPayment_Order
GO
ALTER TABLE dbo.AccountDetail
	DROP CONSTRAINT FK_AccountDetail_Order
GO
ALTER TABLE dbo.OrderTax
	DROP CONSTRAINT FK_OrderTax_Order
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Order
GO
ALTER TABLE dbo.Promotion
	DROP CONSTRAINT FK_Promotion_Order
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Promotion
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Promotion1
GO
ALTER TABLE dbo.Shipment
	DROP CONSTRAINT FK_Shipment_Order
GO
DROP TABLE dbo.[Order]
GO
EXECUTE sp_rename N'dbo.Tmp_Order', N'Order', 'OBJECT' 
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	PK_Order PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_ShippingMethod FOREIGN KEY
	(
	ShippingMethodID
	) REFERENCES dbo.ShippingMethod
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Country FOREIGN KEY
	(
	CountryID
	) REFERENCES dbo.Country
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Status FOREIGN KEY
	(
	StatusID
	) REFERENCES dbo.Status
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Shipment ADD CONSTRAINT
	FK_Shipment_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Shipment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Promotion ADD CONSTRAINT
	FK_Promotion_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Promotion FOREIGN KEY
	(
	VoucherPromotionID
	) REFERENCES dbo.Promotion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Promotion1 FOREIGN KEY
	(
	PromotionID
	) REFERENCES dbo.Promotion
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Promotion SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Basket SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OrderTax ADD CONSTRAINT
	FK_OrderTax_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.OrderTax SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AccountDetail ADD CONSTRAINT
	FK_AccountDetail_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AccountDetail SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OrderPayment ADD CONSTRAINT
	FK_OrderPayment_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.OrderPayment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


print '####### Identities RE-Added #######'

/****** Object:  StoredProcedure [dbo].[kawa_Port_Aliases]    Script Date: 3/08/2016 7:05:11 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[kawa_Port_Aliases]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
      productID = [object_id],
      alias = name
	FROM [dve-port].[dbo].[cscart_seo_names]
	where type='p'
	order by [object_id] desc
END


print '####### Full Port Done #######'
