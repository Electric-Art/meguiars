IF not EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Port'
      AND Object_ID = Object_ID(N'Account'))
	BEGIN
		ALTER TABLE dbo.[Account] ADD
			Port bit NULL
		PRINT 'Port field added'
	END
Else
	BEGIN
		PRINT 'Port field already there'
	END

IF not EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Port_Pass'
      AND Object_ID = Object_ID(N'Account'))
	BEGIN
		ALTER TABLE dbo.[Account] ADD
			Port_Pass varchar(32) NULL
		PRINT 'Port_Pass field added'
	END
Else
	BEGIN
		PRINT 'Port_Pass field already there'
	END

IF not EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Port_PassSalt'
      AND Object_ID = Object_ID(N'Account'))
	BEGIN
		ALTER TABLE dbo.[Account] ADD
			Port_PassSalt varchar(10) NULL
		PRINT 'Port_PassSalt field added'
	END
Else
	BEGIN
		PRINT 'Port_PassSalt field already there'
	END


insert into Account (ID,UserName,createdate,isAdmin,isTrade,isBlocked,firstname,lastname,Port, Port_Pass,Port_PassSalt)
select -- top 10
ID = cscart_users.user_id,
UserName = cscart_users.email,
createdate = dateadd(S, cscart_users.timestamp, '1970-01-01'),
isAdmin = 0,
isTrade = 0,
isBlocked = case when cscart_users.status = 'A' then 0 else 1 end, -- TRANSFORM A -> false
firstname = cscart_users.firstname,
lastname = cscart_users.lastname,
port = 1,
password = cscart_users.password,
passwordhash = cscart_users.salt

from [Dve-test7].[dbo].cscart_users
where user_type='C'
order by user_id desc