SELECT 
TagID, 
ProductID = Cast((select specifications from [Product] where Product.ID = [Tag_Product].ProductID) as int)
Into [Port_TagAssignments]
FROM [Tag_Product]
where (select specifications from [Product] where Product.ID = [Tag_Product].ProductID) is not null


delete from QandA
delete from Ingredient
delete from ProductRelation
delete from Symptom_Product
update Page set ProductID=null where ProductID is not null
delete from Favourite where ProductID is not null
delete from StockNotice
delete from Promotion where ProductID is not null
delete from Tag_Product
delete from SiteResource where ProductID is not null
delete from Basket
delete from Price
delete from Variant
delete from Product
delete from OrderPayment
delete from OrderTax
delete from Favourite
delete from Promotion where OrderID is not null
delete from AccountDetail
delete from [Order]
delete from Account

IF not EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Port'
      AND Object_ID = Object_ID(N'Product'))
	BEGIN
		ALTER TABLE dbo.[Product] ADD
			Port bit NULL
		PRINT 'Port field added'
	END
Else
	BEGIN
		PRINT 'Port field already there'
	END

-- Product
insert into Product (ID,name,blurb,sku,createDate, modifiedDate, isHidden, Port)
select -- top 10
ID = cscart_products.product_id, 
Name = [cscart_product_descriptions].[product],
Blurb = [cscart_product_descriptions].[full_description],
sku = cscart_products.product_code, 
createDate = dateadd(S, case when cscart_products.timestamp = 0 then 1355230800 else cscart_products.timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
modifieddate = dateadd(S, case when cscart_products.updated_timestamp = 0 then 1440060778 else cscart_products.updated_timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
isHidden = case when cscart_products.status = 'A' then 0 else 1 end,
Port = 1
from [Dve-test7].[dbo].cscart_products
join [Dve-test7].[dbo].[cscart_product_descriptions] 
on cscart_products.Product_id = [cscart_product_descriptions].Product_id

-- Unknown Product
insert into Product (ID,name,blurb,sku,createDate, modifiedDate, isHidden, Port)
select -- top 10
ID = (select top 1 ID from Product order by ID Desc)+1, 
Name = 'Unknown Product',
Blurb = 'Product placeholder for order items that found no product',
sku = 'UnknownProduct', 
createDate = GETDATE(),	
modifieddate = GETDATE(),	
isHidden = 1,
Port = 1


-- Variant
insert into Variant(ID, ProductID, name, price,priceRRP,weight,minQuantity,maxQuantity,noTax,createDate,modifieddate,isHidden)
select -- top 10
ID = cscart_products.product_id, 
ProductID = cscart_products.product_id, 
Name = [cscart_product_descriptions].[product],
price = (select [price]  from [Dve-test7].[dbo].[cscart_product_prices] where [cscart_product_prices].product_id=[cscart_products].product_id and lower_limit=1),
priceRRP = cscart_products.[list_price],
weight = cscart_products.weight,
minQuantity = case when cscart_products.min_qty = 0 then null else cscart_products.min_qty end, --TRANSFORM 0 to null
maxQuantity = case when cscart_products.max_qty = 0 then null else cscart_products.max_qty end, --TRANSFORM 0 to null
noTax = case when cscart_products.tax_ids = '' then 1 else 0 end, --TRANSFORM '' to no tax
createDate = dateadd(S, case when cscart_products.timestamp = 0 then 1355230800 else cscart_products.timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
modifieddate = dateadd(S, case when cscart_products.updated_timestamp = 0 then 1440060778 else cscart_products.updated_timestamp end, '1970-01-01'), --TRANSFORM from unix timestamp
isHidden = case when cscart_products.status = 'D' then 1 else 0 end --TRANSFORM D -> true
from [Dve-test7].[dbo].cscart_products
join [Dve-test7].[dbo].[cscart_product_descriptions] 
on cscart_products.Product_id = [cscart_product_descriptions].Product_id
where (select [price]  from [Dve-test7].[dbo].[cscart_product_prices] where [cscart_product_prices].product_id=[cscart_products].product_id and lower_limit=1) is not null

-- Unknown Variant
insert into Variant(ID, ProductID, name, price,priceRRP,weight,noTax,createDate,modifieddate,isHidden)
select -- top 10
ID = (select ID from Product where sku = 'UnknownProduct'), 
ProductID = (select ID from Product where sku = 'UnknownProduct'), 
Name = 'Unknown Product',
price = 0,
priceRRP = 0,
weight = 0,
noTax = 0,
createDate = GETDATE(), 
modifieddate = GETDATE(), 
isHidden = 1


-- SiteResources
insert into SiteResource (Filename,SiteResourceType, ObjectId, ObjectType, ProductID, OrderNo, allowPostCard)
select -- top 10
Filename = (
			select top 1 [image_path] 
			from [Dve-test7].[dbo].[cscart_images_links] 
			join [Dve-test7].[dbo].[cscart_images] 
			on [cscart_images_links].detailed_id = [cscart_images].image_id  
			where [object_id]=[cscart_products].product_id and [object_type] = 'product'
			),
SiteResourceType = 0,
ObjectId = cscart_products.product_id,
ObjectType = 1,
ProductID =  cscart_products.product_id,
OrderNo = 1,
allowPostCard = 0
from [Dve-test7].[dbo].cscart_products
where (
		select top 1 [image_path] 
		from [Dve-test7].[dbo].[cscart_images_links] 
		join [Dve-test7].[dbo].[cscart_images] 
		on [cscart_images_links].detailed_id = [cscart_images].image_id  
		where [object_id]=[cscart_products].product_id and [object_type] = 'product'
	) is not null


DECLARE @free_shipping_id int; 
select @free_shipping_id = ID from Tag where SystemTag = 8
insert into Tag_Product (TagID,ProductID,Type)
select 
TagID = @free_shipping_id,
ProductID = cscart_products.product_id,
Type = 0
from [Dve-test7].[dbo].cscart_products
where free_shipping = 'Y'

DECLARE @is_vegetarian_id int; 
select @is_vegetarian_id = ID from Tag where SystemTag = 9

insert into Tag_Product (TagID,ProductID,Type)
select 
TagID = @is_vegetarian_id,
ProductID = cscart_products.product_id,
Type = 0
from [Dve-test7].[dbo].cscart_products
where is_vegetarian = 'Y'

DECLARE @is_vegan_id int; 
select @is_vegan_id = ID from Tag where SystemTag = 10
insert into Tag_Product (TagID,ProductID,Type)
select 
TagID = @is_vegan_id,
ProductID = cscart_products.product_id,
Type = 0
from [Dve-test7].[dbo].cscart_products
where is_vegan = 'Y'


insert into Tag_Product (TagID,ProductID,Type)
select 
TagID,
ProductID,
Type = 0
from [Port_TagAssignments]
where not exists(select * from Tag_Product where [Port_TagAssignments].TagID = Tag_Product.TagID and [Port_TagAssignments].ProductID = Tag_Product.ProductID)
and exists(select * from Product where Product.ID = [Port_TagAssignments].ProductID)

DROP TABLE [Port_TagAssignments]
