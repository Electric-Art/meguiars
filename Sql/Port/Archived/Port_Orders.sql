CREATE TABLE [dbo].[Port_StatusAssign](
	[status] [nchar](1) NOT NULL,
	[SystemStatus] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[Note] [varchar](50) NULL,
 CONSTRAINT [PK_Port_StatusAssign] PRIMARY KEY CLUSTERED 
(
	[status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'A', 2, 11, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'B', 2, 9, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'C', 4, 4, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'D', 3, 10, N'Declined')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'E', 2, 26, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'F', 3, 10, N'Failed/Declined')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'G', 2, 13, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'H', 1, 15, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'I', 3, 10, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'J', 2, 16, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'K', 2, 17, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'L', 3, 10, N'Cancelled - Fraud')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'M', 2, 20, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'N', 3, 10, N'Cancelled - Unknown')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'O', 1, 5, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'P', 2, 3, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'Q', 3, 10, N'Cancelled - Unknown')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'R', 3, 10, N'Cancelled - Product Discontinued')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'S', 3, 10, N'Cancelled - Unavailable')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'U', 3, 10, N'Cancelled - Unknown')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'V', 2, 23, NULL)
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'W', 3, 10, N'Cancelled - Unknown')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'X', 2, 17, N'Picking')
GO
INSERT [dbo].[Port_StatusAssign] ([status], [SystemStatus], [StatusID], [Note]) VALUES (N'Z', 3, 10, N'Cancelled - Duplicate Order')
GO


IF not EXISTS(
    SELECT *
    FROM sys.columns 
    WHERE Name      = N'Port'
      AND Object_ID = Object_ID(N'Order'))
	BEGIN
		ALTER TABLE dbo.[Order] ADD
			Port bit NULL
		PRINT 'Port field added'
	END
Else
	BEGIN
		PRINT 'Port field already there'
	END


insert into [order](ID,OrderDate,OrderTotal,SystemStatus,StatusID,SystemNotes,BasketTotal,FreightTotal,TaxTotal,SessionKey,Code,AgentData,AccountID,ShippingMethodID,PaymentType,Weight,AmexSurcharge,Port)
SELECT --TOP 10
-- ORDER
ID = cscart_orders.order_id,
OrderDate = dateadd(S, cscart_orders.timestamp, '1970-01-01'),
OrderTotal = cscart_orders.total,
SystemStatus =  (select [SystemStatus] from [Port_StatusAssign] where [Port_StatusAssign].status = cscart_orders.status), -- TRANSFORM TO NEW STATUSES
StatusID =  (select [StatusID] from [Port_StatusAssign] where [Port_StatusAssign].status = cscart_orders.status), -- TRANSFORM TO NEW STATUSES
SystemNotes = (select [Note] from [Port_StatusAssign] where [Port_StatusAssign].status = cscart_orders.status),
BasketTotal = cscart_orders.subtotal,
FreightTotal = cscart_orders.shipping_cost,
TaxTotal = cscart_orders.total, -- Can't find
SessionKey = NEWID(),
Code = NEWID(),
AgentData = cscart_orders.ip_address,
AccountID = case	when (cscart_orders.user_id = 999999) THEN null
					when (cscart_orders.user_id = 0) THEN null  -- TRANSFORM  999999 & 0 -> null
					when not exists(select * from Account where Account.ID=cscart_orders.user_id) THEN null
					else cscart_orders.user_id
						end,
ShippingMethodID = case when (ISNUMERIC(shipping_ids) = 1) THEN 
						case 
							when CAST(shipping_ids AS INT) = 1 then 1
							when CAST(shipping_ids AS INT) = 3 then 1
							when CAST(shipping_ids AS INT) = 4 then 1
							when CAST(shipping_ids AS INT) = 5 then 4
							when CAST(shipping_ids AS INT) = 6 then 3
							when CAST(shipping_ids AS INT) = 7 then 5
							when CAST(shipping_ids AS INT) = 9 then 2
							else null
						end
					ELSE NULL END,
PaymentType =  
						case 
							when payment_id = 6 then 0
							when payment_id = 1 then 0
							when payment_id = 13 then 1
							when payment_id = 7 then 2
							when payment_id = 12 then 2
							when payment_id = 9 then 2
							when payment_id = 2 then 0
							when payment_id = 10 then 2
							when payment_id = 3 then 2
							when payment_id = 8 then 2
							when payment_id = 5 then 2
							when payment_id = 4 then 0
							when payment_id = 14 then 0
							when payment_id = 11 then 2
							else null
						end,
Weight = null, --'MNBI'
AmexSurcharge = cscart_orders.amx_surch,
Port = 1
FROM [Dve-test7].[dbo].[cscart_orders]


insert into [AccountDetail](OrderID,FirstName,Surname,Phone,Email,Address1,Address2,Address3,Address4,Postcode,CountryID,Country,Type,Company,IsMailer)
-- CUSTOMER DETAILS
select -- top 10
Cust_OrderID = cscart_orders.order_id,
Cust_FirstName = cscart_orders.b_firstname,
Cust_Surname = cscart_orders.b_lastname,
Cust_Phone = cscart_orders.b_phone,
Cust_Email = cscart_orders.email,
Cust_Address1 = cscart_orders.b_address,
Cust_Address2 = cscart_orders.b_address_2,
Cust_Address3 = cscart_orders.b_city,
Cust_Address4 = cscart_orders.b_state,
Cust_Postcode = cscart_orders.b_zipcode,
Cust_CountryID = (Select ID from Country where name=(SELECT [country] FROM [Dve-test7].[dbo].[cscart_country_descriptions] where [code] = cscart_orders.b_country)),
Cust_Country = b_country,
Cust_Type = 0,
Cust_Company = cscart_orders.company,
Cust_IsMailer = case when (exists(select * FROM [DveMaillist].[dbo].[members_export_b309886323] where ["Email Address"]=cscart_orders.email)) then 1 else 0 end
FROM [Dve-test7].[dbo].[cscart_orders]


insert into [AccountDetail](OrderID,FirstName,Surname,Phone,Email,Address1,Address2,Address3,Address4,Postcode,CountryID,Country,Type,Company,DeliveryInstructions,IsMailer)
-- CUSTOMER DETAILS
select -- top 10
Del_OrderID = cscart_orders.order_id,
Del_FirstName = cscart_orders.s_firstname,
Del_Surname = cscart_orders.s_lastname,
Del_Phone = cscart_orders.s_phone,
Del_Email = cscart_orders.email,
Del_Address1 = cscart_orders.s_address,
Del_Address2 = cscart_orders.s_address_2,
Del_Address3 = cscart_orders.s_city,
Del_Address4 = cscart_orders.s_state,
Del_Postcode = cscart_orders.s_zipcode,
Del_CountryID = (Select ID from Country where name=(SELECT [country] FROM [Dve-test7].[dbo].[cscart_country_descriptions] where [code] = cscart_orders.b_country)),
Del_Country = b_country,
Del_Type = 1,
Del_Company = cscart_orders.company,
Del_DeliveryInstructions = cscart_orders.notes,
Del_IsMailer = 0
FROM [Dve-test7].[dbo].[cscart_orders]


DECLARE @unknownid int; 
select @unknownid = ID from Product where sku = 'UnknownProduct'

insert into [Basket] (OrderID,VariantID,Price,Quantity,Sessionkey,CreateDate)
--Basket
SELECT --TOP 10
--ID = [item_id],
OrderID = [order_id],

VariantID = case when (exists(select * from [variant] where ID=[product_id])) then [product_id]
				 when (exists(select * from [product] where sku=[cscart_order_details].product_code)) then (select top 1 ID from [product] where sku=[cscart_order_details].product_code)
				 else @unknownid
			end
,
Price = [price],
Quantity = [amount],
Sessionkey =  (select Sessionkey from [order] where [order].ID = [order_id]),
CreateDate = (select OrderDate from [order] where [order].ID = [order_id]) 
FROM [Dve-test7].[dbo].[cscart_order_details]




DROP TABLE [Port_StatusAssign]