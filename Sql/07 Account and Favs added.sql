/*
   Thursday, November 05, 20154:00:45 PM
   User: 
   Server: localhost
   Database: Kawa2
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Account
	(
	ID int NOT NULL IDENTITY (1, 1),
	UserName varchar(50) NOT NULL,
	createDate datetime NOT NULL,
	isAdmin bit NOT NULL,
	isTrade bit NOT NULL,
	isBlocked bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Account ADD CONSTRAINT
	PK_Account PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Account SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Page SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
/****** Object:  Table [dbo].[Favourite]    Script Date: 11/05/2015 17:22:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Favourite](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[ProductID] [int] NULL,
	[PageID] [int] NULL,
	[createDate] [datetime] NOT NULL,
	[Type] [int] NOT NULL,
 CONSTRAINT [PK_Favourite] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Favourite]  WITH CHECK ADD  CONSTRAINT [FK_Favourite_Account] FOREIGN KEY([AccountID])
REFERENCES [dbo].[Account] ([ID])
GO

ALTER TABLE [dbo].[Favourite] CHECK CONSTRAINT [FK_Favourite_Account]
GO

ALTER TABLE [dbo].[Favourite]  WITH CHECK ADD  CONSTRAINT [FK_Favourite_Page] FOREIGN KEY([PageID])
REFERENCES [dbo].[Page] ([ID])
GO

ALTER TABLE [dbo].[Favourite] CHECK CONSTRAINT [FK_Favourite_Page]
GO

ALTER TABLE [dbo].[Favourite]  WITH CHECK ADD  CONSTRAINT [FK_Favourite_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO

ALTER TABLE [dbo].[Favourite] CHECK CONSTRAINT [FK_Favourite_Product]
GO
GO
COMMIT
