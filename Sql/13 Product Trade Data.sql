/*
   Wednesday, December 16, 20153:58:06 PM
   User: 
   Server: localhost
   Database: Kawa2
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Page ADD
	ProductID int NULL
GO
ALTER TABLE dbo.Page SET (LOCK_ESCALATION = TABLE)
GO

GO
ALTER TABLE dbo.Page ADD CONSTRAINT
	FK_Page_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO



COMMIT
