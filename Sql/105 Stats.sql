
/****** Object:  Table [dbo].[Stat]    Script Date: 23/05/2018 6:13:43 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Stat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ProductID] [int] NULL,
	[TagID] [int] NULL,
 CONSTRAINT [PK_Stat_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Stat] ADD  CONSTRAINT [DF_Stat_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[Stat]  WITH CHECK ADD  CONSTRAINT [FK_Stat_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO

ALTER TABLE [dbo].[Stat] CHECK CONSTRAINT [FK_Stat_Product]
GO

ALTER TABLE [dbo].[Stat]  WITH CHECK ADD  CONSTRAINT [FK_Stat_Tag] FOREIGN KEY([TagID])
REFERENCES [dbo].[Tag] ([ID])
GO

ALTER TABLE [dbo].[Stat] CHECK CONSTRAINT [FK_Stat_Tag]
GO


/****** Object:  StoredProcedure [dbo].[kawa_Cache_Stat_Trending]    Script Date: 23/05/2018 6:14:00 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[kawa_Cache_Stat_Trending] 
	-- Add the parameters for the stored procedure here
	@Tag1ID int,
	@StatType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	  delete from [dbo].[ListCache] where [dbo].[ListCache].Tag1ID = @Tag1ID

	  insert into  [dbo].[ListCache] (Tag1ID, ProductID, Tag2ID, createDate)
	  select top 20 @Tag1ID, ProductID, TagID, getDate()
	  from Stat
	  where type = @StatType and CreateDate >= DATEADD(week,-1,GETDATE())
	  group by ProductID, TagID
	  order by count(*) desc

END
GO



