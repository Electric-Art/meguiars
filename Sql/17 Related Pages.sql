/*
   Monday, January 11, 20162:01:25 PM
   User: 
   Server: localhost
   Database: Kawa2
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Page SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.PageRelation
	(
	ID int NOT NULL IDENTITY (1, 1),
	PageID int NOT NULL,
	RelatedPageID int NOT NULL,
	Type int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.PageRelation ADD CONSTRAINT
	PK_PageRelation PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.PageRelation ADD CONSTRAINT
	FK_PageRelation_Page FOREIGN KEY
	(
	PageID
	) REFERENCES dbo.Page
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PageRelation ADD CONSTRAINT
	FK_PageRelation_Page1 FOREIGN KEY
	(
	RelatedPageID
	) REFERENCES dbo.Page
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.PageRelation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
