UPDATE [order] 
SET VoucherPromotionID = ( select p.ID
  from [order] o
  left join OrderPayment op on o.ID = op.OrderID and op.PaymentReference = 'Promotion'
  left join promotion p on p.code = op.PaymentCode collate Latin1_General_CI_AS
  where o.ID in (select orderID from OrderPayment where PaymentReference = 'Promotion')
                and [order].ID = o.ID )
				where VoucherPromotionID is null and ID in ( select o.ID
  from [order] o
  left join OrderPayment op on o.ID = op.OrderID and op.PaymentReference = 'Promotion'
  left join promotion p on p.code = op.PaymentCode collate Latin1_General_CI_AS
  where o.ID in (select orderID from OrderPayment where PaymentReference = 'Promotion'))