/*
   Thursday, 18 January 201811:14:55 a.m.
   User: 
   Server: MAC10\SQL2014
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Signature
	(
	ID int NOT NULL IDENTITY (1, 1),
	Name varchar(255) NOT NULL,
	Html varchar(MAX) NOT NULL,
	Colour varchar(50) NULL,
	OrderNo int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Signature ADD CONSTRAINT
	PK_Signature PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Signature SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
