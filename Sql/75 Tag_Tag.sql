/*
   Monday, 11 July 201611:44:36 AM
   User: 
   Server: localhost
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tag_Tag
	(
	ID int NOT NULL IDENTITY (1, 1),
	TagID int NOT NULL,
	RelatedTagID int NOT NULL,
	Type int NOT NULL,
	orderNo int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tag_Tag ADD CONSTRAINT
	PK_Tag_Tag PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Tag_Tag ADD CONSTRAINT
	FK_Tag_Tag_Tag FOREIGN KEY
	(
	TagID
	) REFERENCES dbo.Tag
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Tag_Tag ADD CONSTRAINT
	FK_Tag_Tag_Tag1 FOREIGN KEY
	(
	RelatedTagID
	) REFERENCES dbo.Tag
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Tag_Tag SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
