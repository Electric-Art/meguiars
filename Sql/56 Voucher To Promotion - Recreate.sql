/****** Object:  Table [dbo].[Promotion]    Script Date: 3/05/2016 4:25:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Promotion](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NOT NULL,
	[ProductID] [int] NULL,
	[VariantID] [int] NULL,
	[Type] [int] NOT NULL,
	[percentOff] [float] NULL,
	[amountOff] [money] NULL,
	[Status] [int] NOT NULL,
	[createDate] [datetime] NOT NULL,
	[usedDate] [datetime] NULL,
	[OrderID] [int] NULL,
	[isHidden] [int] NOT NULL,
	[SessionKey] [varchar](50) NULL,
	[expiryDate] [datetime] NULL,
	[TagID] [int] NULL,
	[notes] [varchar](max) NULL,
	[SystemType] [int] NULL,
	[minimumSpend] [money] NULL,
	[minimumWeight] [decimal](8,2) NULL,
	[LocationID] [int] NULL
 CONSTRAINT [PK_Promotion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Promotion]  WITH CHECK ADD  CONSTRAINT [FK_Promotion_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([ID])
GO

ALTER TABLE [dbo].[Promotion] CHECK CONSTRAINT [FK_Promotion_Order]
GO

ALTER TABLE [dbo].[Promotion]  WITH CHECK ADD  CONSTRAINT [FK_Promotion_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO

ALTER TABLE [dbo].[Promotion] CHECK CONSTRAINT [FK_Promotion_Product]
GO

ALTER TABLE [dbo].[Promotion]  WITH CHECK ADD  CONSTRAINT [FK_Promotion_Tag] FOREIGN KEY([TagID])
REFERENCES [dbo].[Tag] ([ID])
GO

ALTER TABLE [dbo].[Promotion] CHECK CONSTRAINT [FK_Promotion_Tag]
GO

ALTER TABLE [dbo].[Promotion]  WITH CHECK ADD  CONSTRAINT [FK_Promotion_Variant] FOREIGN KEY([VariantID])
REFERENCES [dbo].[Variant] ([ID])
GO

ALTER TABLE [dbo].[Promotion] CHECK CONSTRAINT [FK_Promotion_Variant]
GO


ALTER TABLE [dbo].[Promotion]  WITH CHECK ADD  CONSTRAINT [FK_Promotion_Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([ID])
GO

ALTER TABLE [dbo].[Promotion] CHECK CONSTRAINT [FK_Promotion_Location]
GO


