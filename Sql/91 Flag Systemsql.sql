/****** Object:  Table [dbo].[Flag]    Script Date: 2/05/2017 9:04:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Flag](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[Type] [int] NOT NULL,
	[Labels] [varchar](50) NULL,
	[Notes] [varchar](max) NULL,
	[isHidden] [int] NOT NULL,
	[createDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Flag] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[FlagTrigger]    Script Date: 2/05/2017 9:04:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[FlagTrigger](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FlagID] [int] NOT NULL,
	[FirstName] [varchar](255) NULL,
	[Surname] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Address1] [varchar](1000) NULL,
	[Address2] [varchar](500) NULL,
	[Address3] [varchar](500) NULL,
	[Address4] [varchar](500) NULL,
	[Postcode] [varchar](50) NULL,
	[CountryID] [int] NULL,
 CONSTRAINT [PK_FlagTrigger] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[FlagTrigger]  WITH CHECK ADD  CONSTRAINT [FK_FlagTrigger_Country] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Country] ([ID])
GO

ALTER TABLE [dbo].[FlagTrigger] CHECK CONSTRAINT [FK_FlagTrigger_Country]
GO

ALTER TABLE [dbo].[FlagTrigger]  WITH CHECK ADD  CONSTRAINT [FK_FlagTrigger_Flag] FOREIGN KEY([FlagID])
REFERENCES [dbo].[Flag] ([ID])
GO

ALTER TABLE [dbo].[FlagTrigger] CHECK CONSTRAINT [FK_FlagTrigger_Flag]
GO


