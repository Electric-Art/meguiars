/*
   Thursday, October 01, 201510:30:15 AM
   User: 
   Server: localhost
   Database: Kawa2
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag
	DROP CONSTRAINT DF_Tag_isHidden
GO
ALTER TABLE dbo.Tag
	DROP CONSTRAINT DF_Tag_OrderByType
GO
CREATE TABLE dbo.Tmp_Tag
	(
	ID int NOT NULL IDENTITY (1, 1),
	name varchar(500) NOT NULL,
	Type int NOT NULL,
	ParentID int NULL,
	SystemTag int NULL,
	orderNo int NULL,
	isHidden int NOT NULL,
	OrderByType int NOT NULL,
	nameUrl varchar(500) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Tag SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Tag ADD CONSTRAINT
	DF_Tag_isHidden DEFAULT ((0)) FOR isHidden
GO
ALTER TABLE dbo.Tmp_Tag ADD CONSTRAINT
	DF_Tag_OrderByType DEFAULT ((0)) FOR OrderByType
GO
SET IDENTITY_INSERT dbo.Tmp_Tag ON
GO
IF EXISTS(SELECT * FROM dbo.Tag)
	 EXEC('INSERT INTO dbo.Tmp_Tag (ID, name, Type, ParentID, SystemTag, orderNo, isHidden, OrderByType)
		SELECT ID, name, Type, ParentID, SystemTag, orderNo, isHidden, OrderByType FROM dbo.Tag WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Tag OFF
GO
ALTER TABLE dbo.Tag
	DROP CONSTRAINT FK_Tag_Tag
GO
ALTER TABLE dbo.Tag_Product
	DROP CONSTRAINT FK_Tag_Product_Tag
GO
ALTER TABLE dbo.Tag_Page
	DROP CONSTRAINT FK_Tag_Page_Tag
GO
DROP TABLE dbo.Tag
GO
EXECUTE sp_rename N'dbo.Tmp_Tag', N'Tag', 'OBJECT' 
GO
ALTER TABLE dbo.Tag ADD CONSTRAINT
	PK_Tag PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Tag ADD CONSTRAINT
	FK_Tag_Tag FOREIGN KEY
	(
	ParentID
	) REFERENCES dbo.Tag
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag_Page ADD CONSTRAINT
	FK_Tag_Page_Tag FOREIGN KEY
	(
	TagID
	) REFERENCES dbo.Tag
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Tag_Page SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag_Product ADD CONSTRAINT
	FK_Tag_Product_Tag FOREIGN KEY
	(
	TagID
	) REFERENCES dbo.Tag
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Tag_Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
