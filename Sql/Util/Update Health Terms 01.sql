DECLARE 
@findstr VARCHAR(MAX), 
@replacestr VARCHAR(MAX),
@LoopCounter int

CREATE TABLE #Temp
( 
	findstr VARCHAR(MAX), 
	replacestr VARCHAR(MAX)
)

insert into #Temp (findstr,replacestr) values ('weightloss','weight management')
insert into #Temp (findstr,replacestr) values ('Weightloss','Weight management')
insert into #Temp (findstr,replacestr) values ('weight loss','weight management')
insert into #Temp (findstr,replacestr) values ('Weight loss','Weight management')
insert into #Temp (findstr,replacestr) values ('Weight Loss','Weight Management')
insert into #Temp (findstr,replacestr) values ('lose weight','assist weight management')
insert into #Temp (findstr,replacestr) values ('Lose weight','assist weight management')
insert into #Temp (findstr,replacestr) values ('fat loss','weight management')
insert into #Temp (findstr,replacestr) values ('Fat loss','Weight management')
insert into #Temp (findstr,replacestr) values ('burn fat','assist weight management')
insert into #Temp (findstr,replacestr) values ('Burn fat','Assist weight management')
insert into #Temp (findstr,replacestr) values ('Vitamin supplements should not replace a balanced diet.','')
insert into #Temp (findstr,replacestr) values ('vitamin supplements should not replace a balanced diet.','')
insert into #Temp (findstr,replacestr) values ('Vitamin supplements should not replace a balanced diet','')
insert into #Temp (findstr,replacestr) values ('vitamin supplements should not replace a balanced diet','')
insert into #Temp (findstr,replacestr) values ('Vitamin Supplements should not replace a balanced diet.','')
insert into #Temp (findstr,replacestr) values ('Vitamin Supplements should not replace a balanced diet','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist, consult a professional.','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist, consult a professional.','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist, consult a professional','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist, consult a professional','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist consult a professional','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist consult a professional','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist, consult a healthcare professional.','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist, consult a healthcare professional.','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist, consult a healthcare professional','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist, consult a healthcare professional','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist see a professional.','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist see a professional.','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist see a professional','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist see a professional','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist see a healthcare professional.','')
insert into #Temp (findstr,replacestr) values ('If symptoms persist see a healthcare professional','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist see a healthcare professional.','')
insert into #Temp (findstr,replacestr) values ('if symptoms persist see a healthcare professional','')
insert into #Temp (findstr,replacestr) values ('This product may interfere with certain other medications you may be taking.','')
insert into #Temp (findstr,replacestr) values ('this product may interfere with certain other medications you may be taking.','')
insert into #Temp (findstr,replacestr) values ('This product may interfere with certain other medications you may be taking','')
insert into #Temp (findstr,replacestr) values ('this product may interfere with certain other medications you may be taking','')
insert into #Temp (findstr,replacestr) values ('medication','therapeutic')
insert into #Temp (findstr,replacestr) values ('Medication','Therapeutic')
insert into #Temp (findstr,replacestr) values ('healthcare','health')
insert into #Temp (findstr,replacestr) values ('Healthcare','Health')
insert into #Temp (findstr,replacestr) values ('health care','health')
insert into #Temp (findstr,replacestr) values ('Health care','Health')
insert into #Temp (findstr,replacestr) values ('Health Care','Health')
insert into #Temp (findstr,replacestr) values ('rheumatoid arthritis','joint inflammation')
insert into #Temp (findstr,replacestr) values ('Rheumatoid arthritis','Joint inflammation')
insert into #Temp (findstr,replacestr) values ('Rheumatoid Arthritis','Joint inflammation')
insert into #Temp (findstr,replacestr) values ('rheumatoid osteoarthritis','joint inflammation')
insert into #Temp (findstr,replacestr) values ('Rheumatoid osteoarthritis','Joint inflammation')
insert into #Temp (findstr,replacestr) values ('Rheumatoid Osteoarthritis','Joint Inflammation')
insert into #Temp (findstr,replacestr) values ('rheumatic','inflammatory')
insert into #Temp (findstr,replacestr) values ('Rheumatic','Inflammatory')
insert into #Temp (findstr,replacestr) values ('osteoarthritis','joint degeneration')
insert into #Temp (findstr,replacestr) values ('Osteoarthritis','Joint degeneration')
insert into #Temp (findstr,replacestr) values ('arthritis','joint degeneration')
insert into #Temp (findstr,replacestr) values ('Arthritis','Joint degeneration')
insert into #Temp (findstr,replacestr) values ('arthritic','joint degeneration')
insert into #Temp (findstr,replacestr) values ('Arthritic','Joint degeneration')
insert into #Temp (findstr,replacestr) values ('medical','health')
insert into #Temp (findstr,replacestr) values ('Medical','Health')
insert into #Temp (findstr,replacestr) values ('medicine','therapeutic')
insert into #Temp (findstr,replacestr) values ('Medicine','Therapeutic')
insert into #Temp (findstr,replacestr) values ('medication','therapeutic')
insert into #Temp (findstr,replacestr) values ('Medication','Therapeutic')
insert into #Temp (findstr,replacestr) values ('doctor','professional')
insert into #Temp (findstr,replacestr) values ('Doctor','Professional')
insert into #Temp (findstr,replacestr) values ('practitioner','professional')
insert into #Temp (findstr,replacestr) values ('Practitioner','Professional')
insert into #Temp (findstr,replacestr) values ('muscle growth','muscle')
insert into #Temp (findstr,replacestr) values ('Muscle growth','Muscle')
insert into #Temp (findstr,replacestr) values ('enhance','improve')
insert into #Temp (findstr,replacestr) values ('Enhance','Improve')
insert into #Temp (findstr,replacestr) values ('enhancer','improver')
insert into #Temp (findstr,replacestr) values ('Enhancer','Improver')
insert into #Temp (findstr,replacestr) values ('anabolic','growth')
insert into #Temp (findstr,replacestr) values ('Anabolic','Growth')
insert into #Temp (findstr,replacestr) values ('flu ','viral infection ')
insert into #Temp (findstr,replacestr) values ('flu,','viral infection,')
insert into #Temp (findstr,replacestr) values ('flu.','viral infection.')
insert into #Temp (findstr,replacestr) values ('Flu ','Viral infection ')
insert into #Temp (findstr,replacestr) values ('Flu,','Viral infection,')
insert into #Temp (findstr,replacestr) values ('Flu.','Viral infection.')
insert into #Temp (findstr,replacestr) values ('flus ','viral infections ')
insert into #Temp (findstr,replacestr) values ('flus,','viral infections,')
insert into #Temp (findstr,replacestr) values ('flus.','viral infections.')
insert into #Temp (findstr,replacestr) values ('Flus ','Viral infections ')
insert into #Temp (findstr,replacestr) values ('Flus,','Viral infections,')
insert into #Temp (findstr,replacestr) values ('Flus.','Viral infections.')
insert into #Temp (findstr,replacestr) values ('high blood pressure','cardiovasular issues')
insert into #Temp (findstr,replacestr) values ('High blood pressure','Cardiovasular issues')
insert into #Temp (findstr,replacestr) values ('Lowers blood pressure','assists cardiovascular health')
insert into #Temp (findstr,replacestr) values ('lowers blood pressure','Assists cardiovascular health')
insert into #Temp (findstr,replacestr) values ('blood pressure','Cardiovasular health')
insert into #Temp (findstr,replacestr) values ('Blood Pressure','cardiovasular health')
insert into #Temp (findstr,replacestr) values ('shark','marine')
insert into #Temp (findstr,replacestr) values ('Shark','Marine')
insert into #Temp (findstr,replacestr) values ('insomnia','sleeplessness')
insert into #Temp (findstr,replacestr) values ('Insomnia','Sleeplessness')
insert into #Temp (findstr,replacestr) values ('prescribed','recommended')
insert into #Temp (findstr,replacestr) values ('Prescribed','Recommended')
insert into #Temp (findstr,replacestr) values ('surgery procedure','health procedure')
insert into #Temp (findstr,replacestr) values ('surgery','health procedure')
insert into #Temp (findstr,replacestr) values ('Surgery','Health procedure')
insert into #Temp (findstr,replacestr) values ('disease','condition')
insert into #Temp (findstr,replacestr) values ('Disease','Condition')
insert into #Temp (findstr,replacestr) values ('diseases','conditions')
insert into #Temp (findstr,replacestr) values ('Diseases','Conditions')
insert into #Temp (findstr,replacestr) values ('pharmaceuticals','therapeutic')
insert into #Temp (findstr,replacestr) values ('Pharmaceuticals','Therapeutic')
insert into #Temp (findstr,replacestr) values ('pharmaceutical','therapeutic')
insert into #Temp (findstr,replacestr) values ('Pharmaceutical','Therapeutic')


DECLARE myCursor99 CURSOR
FOR
SELECT findstr, replacestr
FROM #Temp

OPEN myCursor99

SELECT @LoopCounter = 0

FETCH NEXT FROM myCursor99 INTO  @findstr, @replacestr

WHILE @@FETCH_STATUS = 0
  BEGIN
	-- Some Code
	update Product
	set blurb = REPLACE(blurb COLLATE SQL_Latin1_General_CP1_CS_AS,  @findstr, @replacestr),
	specifications = REPLACE(specifications COLLATE SQL_Latin1_General_CP1_CS_AS,  @findstr, @replacestr)
	where googleCat is not null
	SELECT @LoopCounter = @LoopCounter + 1
	FETCH NEXT FROM myCursor99 INTO  @findstr, @replacestr
  END
CLOSE myCursor99
DEALLOCATE myCursor99
DROP TABLE #Temp
SELECT 'Loops incurred: ' + CONVERT(varchar(15),@LoopCounter)
GO

