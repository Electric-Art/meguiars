exec sp_executesql N'SELECT [t1].[ID], [t1].[OrderID], [t1].[FirstName], [t1].[Surname], [t1].[Phone], [t1].[Email], [t1].[Address1], [t1].[Address2], [t1].[Address3], [t1].[Address4], [t1].[Postcode], [t1].[CountryID], [t1].[Type], [t1].[isHidden], [t1].[GUID], [t1].[Company], [t1].[Country], [t1].[isMailer], [t1].[Mobile], [t1].[DeliveryInstructions], [t1].[value] AS [LeaveAuth]
FROM (
    SELECT [t0].[ID], [t0].[OrderID], [t0].[FirstName], [t0].[Surname], [t0].[Phone], [t0].[Email], [t0].[Address1], [t0].[Address2], [t0].[Address3], [t0].[Address4], [t0].[Postcode], [t0].[CountryID], [t0].[Type], [t0].[isHidden], [t0].[GUID], [t0].[Company], [t0].[Country], [t0].[isMailer], [t0].[Mobile], [t0].[DeliveryInstructions], 
        (CASE 
            WHEN [t0].[LeaveAuth] IS NOT NULL THEN 
                (CASE 
                    WHEN [t0].[LeaveAuth] = @p0 THEN 1
                    WHEN NOT ([t0].[LeaveAuth] = @p0) THEN 0
                    ELSE NULL
                 END)
            ELSE CONVERT(Int,NULL)
         END) AS [value]
    FROM [dbo].[AccountDetail] AS [t0]
    ) AS [t1]
WHERE [t1].[OrderID] = @p1',N'@p0 int,@p1 int',@p0=1,@p1=205928