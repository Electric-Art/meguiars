exec sp_executesql N'SELECT [t12].[ID], [t12].[OrderDate], [t12].[OrderTotal], [t12].[StatusID], [t12].[SystemStatus], [t12].[ProcessDate], [t12].[CompleteDate], [t12].[BasketTotal], [t12].[FreightTotal], [t12].[TaxTotal], [t12].[Invoice], [t12].[InvoiceTotal], [t12].[idx], [t12].[AcknowledgeDate], [t12].[FeedbackRequestDate], [t12].[FeedbackResponseDate], [t12].[AdminNotes], [t12].[Code], [t12].[SessionKey], [t12].[AgentData], [t12].[PromotionID], [t12].[AccountID], [t12].[ShippingMethodID], [t12].[LocationID], [t12].[CountryID], [t12].[StateID], [t12].[Postcode], [t12].[SystemNotes], [t12].[PaymentType], [t12].[CustomerNotes], [t12].[VoucherPromotionID], [t12].[Weight], [t12].[Printed], [t12].[TotalBeforeSurcharge], [t12].[AmexSurcharge], [t12].[AdminAdHocFreightTotal], [t12].[IsHidden], [t12].[CubicWeight], [t12].[FreightWouldHaveBeenTotal], [t12].[FreightOriginalAmount], [t12].[IsAdminOrder], [t12].[value] AS [VoucherCode], [t13].[ID] AS [ID4], [t13].[OrderID], [t13].[TaxType], [t13].[Amount], (
    SELECT COUNT(*)
    FROM [dbo].[OrderTax] AS [t15]
    WHERE [t15].[OrderID] = [t12].[ID]
    ) AS [value], [t12].[value2], [t12].[test], [t12].[ID2], [t12].[Name], [t12].[SetSystemStatus], [t12].[EmailSubject], [t12].[EmailHeader], [t12].[Color], [t12].[isNotifyCustomer], [t12].[isNotifyStaff], [t12].[InventoryEffect], [t12].[isPayOrderAgain], [t12].[OrderNo], [t12].[CreateDate], [t12].[Notes], [t12].[StatusSet], [t12].[RefactorToID], [t12].[value3] AS [StatusSet2], [t12].[value4] AS [SearchOrderNo], [t12].[value5] AS [SearchCustomer], [t12].[value6] AS [SearchEmail]
FROM (
    SELECT [t0].[ID], [t0].[OrderDate], [t0].[OrderTotal], [t0].[StatusID], [t0].[SystemStatus], [t0].[ProcessDate], [t0].[CompleteDate], [t0].[BasketTotal], [t0].[FreightTotal], [t0].[TaxTotal], [t0].[Invoice], [t0].[InvoiceTotal], [t0].[idx], [t0].[AcknowledgeDate], [t0].[FeedbackRequestDate], [t0].[FeedbackResponseDate], [t0].[AdminNotes], [t0].[Code], [t0].[SessionKey], [t0].[AgentData], [t0].[PromotionID], [t0].[AccountID], [t0].[ShippingMethodID], [t0].[LocationID], [t0].[CountryID], [t0].[StateID], [t0].[Postcode], [t0].[SystemNotes], [t0].[PaymentType], [t0].[CustomerNotes], [t0].[VoucherPromotionID], [t0].[Weight], [t0].[Printed], [t0].[TotalBeforeSurcharge], [t0].[AmexSurcharge], [t0].[AdminAdHocFreightTotal], [t0].[IsHidden], [t0].[CubicWeight], [t0].[FreightWouldHaveBeenTotal], [t0].[FreightOriginalAmount], [t0].[IsAdminOrder], 
        (CASE 
            WHEN [t0].[VoucherPromotionID] IS NOT NULL THEN CONVERT(NVarChar(MAX),[t3].[code])
            WHEN EXISTS(
                SELECT NULL AS [EMPTY]
                FROM [dbo].[Promotion] AS [t4]
                WHERE [t4].[OrderID] = [t0].[ID]
                ) THEN CONVERT(NVarChar(MAX),(
                SELECT [t6].[code]
                FROM (
                    SELECT TOP (1) [t5].[code]
                    FROM [dbo].[Promotion] AS [t5]
                    WHERE [t5].[OrderID] = [t0].[ID]
                    ORDER BY [t5].[ID]
                    ) AS [t6]
                ))
            ELSE NULL
         END) AS [value], 
        (CASE 
            WHEN [t0].[StatusID] IS NOT NULL THEN 1
            ELSE 0
         END) AS [value2], [t2].[test], [t2].[ID] AS [ID2], [t2].[Name], [t2].[SetSystemStatus], [t2].[EmailSubject], [t2].[EmailHeader], [t2].[Color], [t2].[isNotifyCustomer], [t2].[isNotifyStaff], [t2].[InventoryEffect], [t2].[isPayOrderAgain], [t2].[OrderNo], [t2].[CreateDate], [t2].[Notes], [t2].[StatusSet], [t2].[RefactorToID], 
        (CASE 
            WHEN [t0].[StatusID] IS NOT NULL THEN [t2].[StatusSet]
            ELSE NULL
         END) AS [value3], 
        (CASE 
            WHEN [t0].[SystemStatus] > @p0 THEN @p1 + (CONVERT(NVarChar,[t0].[ID]))
            ELSE @p2 + (CONVERT(NVarChar,[t0].[ID]))
         END) AS [value4], 
        (CASE 
            WHEN EXISTS(
                SELECT NULL AS [EMPTY]
                FROM [dbo].[AccountDetail] AS [t7]
                WHERE ([t7].[Type] = @p3) AND ([t7].[OrderID] = [t0].[ID])
                ) THEN CONVERT(NVarChar(MAX),(((
                SELECT [t8].[FirstName]
                FROM [dbo].[AccountDetail] AS [t8]
                WHERE ([t8].[Type] = @p4) AND ([t8].[OrderID] = [t0].[ID])
                )) + @p5) + ((
                SELECT [t9].[Surname]
                FROM [dbo].[AccountDetail] AS [t9]
                WHERE ([t9].[Type] = @p6) AND ([t9].[OrderID] = [t0].[ID])
                )))
            ELSE NULL
         END) AS [value5], 
        (CASE 
            WHEN EXISTS(
                SELECT NULL AS [EMPTY]
                FROM [dbo].[AccountDetail] AS [t10]
                WHERE ([t10].[Type] = @p7) AND ([t10].[OrderID] = [t0].[ID])
                ) THEN CONVERT(NVarChar(MAX),(
                SELECT [t11].[Email]
                FROM [dbo].[AccountDetail] AS [t11]
                WHERE ([t11].[Type] = @p8) AND ([t11].[OrderID] = [t0].[ID])
                ))
            ELSE NULL
         END) AS [value6], [t3].[ID] AS [ID3]
    FROM [dbo].[Order] AS [t0]
    LEFT OUTER JOIN (
        SELECT 1 AS [test], [t1].[ID], [t1].[Name], [t1].[SetSystemStatus], [t1].[EmailSubject], [t1].[EmailHeader], [t1].[Color], [t1].[isNotifyCustomer], [t1].[isNotifyStaff], [t1].[InventoryEffect], [t1].[isPayOrderAgain], [t1].[OrderNo], [t1].[CreateDate], [t1].[Notes], [t1].[StatusSet], [t1].[RefactorToID]
        FROM [dbo].[Status] AS [t1]
        ) AS [t2] ON [t2].[ID] = [t0].[StatusID]
    LEFT OUTER JOIN [dbo].[Promotion] AS [t3] ON [t3].[ID] = [t0].[VoucherPromotionID]
    ) AS [t12]
LEFT OUTER JOIN [dbo].[OrderTax] AS [t13] ON [t13].[OrderID] = [t12].[ID]
WHERE ([t12].[ID] = @p9) AND (((
    SELECT COUNT(*)
    FROM [dbo].[AccountDetail] AS [t14]
    WHERE [t14].[OrderID] = [t12].[ID]
    )) > @p10)
ORDER BY [t12].[ID], [t12].[ID2], [t12].[ID3], [t13].[ID]',N'@p0 int,@p1 nvarchar(4000),@p2 nvarchar(4000),@p3 int,@p4 int,@p5 nvarchar(4000),@p6 int,@p7 int,@p8 int,@p9 int,@p10 int',@p0=0,@p1=N'DVE-',@p2=N'PRG-',@p3=0,@p4=0,@p5=N' ',@p6=0,@p7=0,@p8=0,@p9=205928,@p10=0



exec sp_executesql N'SELECT TOP (1) [t0].[ID], [t0].[OrderID], [t0].[AccountDetailID], [t0].[PaymentStatus], [t0].[PaymentReference], [t0].[PaymentCode], [t0].[PaymentDate], [t0].[FullResponse], [t0].[ResponseText], [t0].[Type], [t0].[Service]
FROM [dbo].[OrderPayment] AS [t0]
WHERE [t0].[OrderID] = @x1
ORDER BY [t0].[ID] DESC',N'@x1 int',@x1=205928


exec sp_executesql N'SELECT [t0].[ID], [t0].[OrderID], [t0].[ShippingMethodID], [t0].[CarrierID], [t0].[TrackingNo], [t0].[Comment], [t0].[createDate], [t1].[Name], [t2].[Name] AS [Name2], [t2].[Url]
FROM [dbo].[Shipment] AS [t0]
INNER JOIN [dbo].[ShippingMethod] AS [t1] ON [t1].[ID] = [t0].[ShippingMethodID]
INNER JOIN [dbo].[Carrier] AS [t2] ON [t2].[ID] = [t0].[CarrierID]
WHERE [t0].[OrderID] = @x1',N'@x1 int',@x1=205928