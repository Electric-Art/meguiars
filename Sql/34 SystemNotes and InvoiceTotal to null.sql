/*
   Wednesday, March 23, 20169:23:27 AM
   User: 
   Server: localhost
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Account
GO
ALTER TABLE dbo.Account SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Status
GO
ALTER TABLE dbo.Status SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Country
GO
ALTER TABLE dbo.Country SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Location
GO
ALTER TABLE dbo.Location SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_ShippingMethod
GO
ALTER TABLE dbo.ShippingMethod SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_OrderDate
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_InvoiceTotal_1
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT DF_Order_idx_1
GO
CREATE TABLE dbo.Tmp_Order
	(
	ID int NOT NULL IDENTITY (1, 1),
	OrderDate datetime NOT NULL,
	OrderTotal money NOT NULL,
	SystemStatus int NOT NULL,
	StatusID int NULL,
	ProcessDate datetime NULL,
	CompleteDate datetime NULL,
	BasketTotal money NOT NULL,
	FreightTotal money NOT NULL,
	Invoice text COLLATE Latin1_General_CI_AS NULL,
	InvoiceTotal money NULL,
	idx int NULL,
	AcknowledgeDate datetime NULL,
	FeedbackRequestDate datetime NULL,
	FeedbackResponseDate datetime NULL,
	AdminNotes varchar(MAX) COLLATE Latin1_General_CI_AS NULL,
	Code uniqueidentifier NOT NULL,
	SessionKey varchar(50) NOT NULL,
	AgentData varchar(MAX) NULL,
	VoucherID int NULL,
	AccountID int NULL,
	ShippingMethodID int NULL,
	LocationID int NULL,
	CountryID int NULL,
	SystemNotes varchar(MAX) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Order SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_OrderDate DEFAULT (getdate()) FOR OrderDate
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_InvoiceTotal_1 DEFAULT ((0)) FOR InvoiceTotal
GO
ALTER TABLE dbo.Tmp_Order ADD CONSTRAINT
	DF_Order_idx_1 DEFAULT ((0)) FOR idx
GO
SET IDENTITY_INSERT dbo.Tmp_Order ON
GO
IF EXISTS(SELECT * FROM dbo.[Order])
	 EXEC('INSERT INTO dbo.Tmp_Order (ID, OrderDate, OrderTotal, SystemStatus, StatusID, ProcessDate, CompleteDate, BasketTotal, FreightTotal, Invoice, InvoiceTotal, idx, AcknowledgeDate, FeedbackRequestDate, FeedbackResponseDate, AdminNotes, Code, SessionKey, AgentData, VoucherID, AccountID, ShippingMethodID, LocationID, CountryID)
		SELECT ID, OrderDate, OrderTotal, SystemStatus, StatusID, ProcessDate, CompleteDate, BasketTotal, FreightTotal, Invoice, InvoiceTotal, idx, AcknowledgeDate, FeedbackRequestDate, FeedbackResponseDate, AdminNotes, Code, SessionKey, AgentData, VoucherID, AccountID, ShippingMethodID, LocationID, CountryID FROM dbo.[Order] WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Order OFF
GO
ALTER TABLE dbo.Voucher
	DROP CONSTRAINT FK_Voucher_Order
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Voucher
GO
ALTER TABLE dbo.AccountDetail
	DROP CONSTRAINT FK_AccountDetail_Order
GO
ALTER TABLE dbo.Basket
	DROP CONSTRAINT FK_Basket_Order
GO
ALTER TABLE dbo.OrderPayment
	DROP CONSTRAINT FK_OrderPayment_Order
GO
DROP TABLE dbo.[Order]
GO
EXECUTE sp_rename N'dbo.Tmp_Order', N'Order', 'OBJECT' 
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	PK_Order PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_ShippingMethod FOREIGN KEY
	(
	ShippingMethodID
	) REFERENCES dbo.ShippingMethod
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Location FOREIGN KEY
	(
	LocationID
	) REFERENCES dbo.Location
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Country FOREIGN KEY
	(
	CountryID
	) REFERENCES dbo.Country
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Status FOREIGN KEY
	(
	StatusID
	) REFERENCES dbo.Status
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Account FOREIGN KEY
	(
	AccountID
	) REFERENCES dbo.Account
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.OrderPayment ADD CONSTRAINT
	FK_OrderPayment_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.OrderPayment SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Basket ADD CONSTRAINT
	FK_Basket_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Basket SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.AccountDetail ADD CONSTRAINT
	FK_AccountDetail_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.AccountDetail SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Voucher ADD CONSTRAINT
	FK_Voucher_Order FOREIGN KEY
	(
	OrderID
	) REFERENCES dbo.[Order]
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Voucher FOREIGN KEY
	(
	VoucherID
	) REFERENCES dbo.Voucher
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Voucher SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
