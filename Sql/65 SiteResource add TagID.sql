/*
   Monday, May 09, 20164:21:20 PM
   User: 
   Server: localhost
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SiteResource ADD
	TagID int NULL
GO
ALTER TABLE dbo.SiteResource ADD CONSTRAINT
	FK_SiteResource_Tag FOREIGN KEY
	(
	TagID
	) REFERENCES dbo.Tag
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.SiteResource SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

go

delete
  FROM [SiteResource]
  where [IngredientID] is not null or [ObjectType]=2
  
  go
