/*
   Friday, October 23, 201510:29:00 AM
   User: 
   Server: localhost
   Database: Kawa2
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Product
	DROP CONSTRAINT DF_Product_createDate
GO
ALTER TABLE dbo.Product
	DROP CONSTRAINT DF_Product_isHidden
GO
CREATE TABLE dbo.Tmp_Product
	(
	ID int NOT NULL IDENTITY (1, 1),
	name varchar(500) NOT NULL,
	titleName varchar(500) NULL,
	menuName varchar(500) NULL,
	intro varchar(MAX) NULL,
	blurb varchar(MAX) NULL,
	activeIngredients varchar(MAX) NULL,
	formulation varchar(MAX) NULL,
	dosage varchar(MAX) NULL,
	warnings varchar(MAX) NULL,
	specifications varchar(MAX) NULL,
	freeFrom varchar(MAX) NULL,
	interactions varchar(MAX) NULL,
	availability varchar(MAX) NULL,
	sku varchar(50) NULL,
	price money NULL,
	specialPrice money NULL,
	createDate datetime NOT NULL,
	modifiedDate datetime NULL,
	isHidden int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Product SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Product ADD CONSTRAINT
	DF_Product_createDate DEFAULT (getdate()) FOR createDate
GO
ALTER TABLE dbo.Tmp_Product ADD CONSTRAINT
	DF_Product_isHidden DEFAULT ((0)) FOR isHidden
GO
SET IDENTITY_INSERT dbo.Tmp_Product ON
GO
IF EXISTS(SELECT * FROM dbo.Product)
	 EXEC('INSERT INTO dbo.Tmp_Product (ID, name, titleName, menuName, blurb, activeIngredients, dosage, warnings, interactions, availability, sku, price, specialPrice, createDate, modifiedDate, isHidden)
		SELECT ID, name, titleName, menuName, blurb, activeIngredients, dosage, warnings, interactions, availability, sku, price, specialPrice, createDate, modifiedDate, isHidden FROM dbo.Product WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Product OFF
GO
ALTER TABLE dbo.Symptom_Product
	DROP CONSTRAINT FK_Symptom_Product_Product
GO
ALTER TABLE dbo.Tag_Product
	DROP CONSTRAINT FK_Tag_Product_Product
GO
ALTER TABLE dbo.ProductRelation
	DROP CONSTRAINT FK_ProductRelation_Product
GO
ALTER TABLE dbo.ProductRelation
	DROP CONSTRAINT FK_ProductRelation_Product1
GO
ALTER TABLE dbo.Variant
	DROP CONSTRAINT FK_Variant_Product
GO
ALTER TABLE dbo.Voucher
	DROP CONSTRAINT FK_Voucher_Product
GO
DROP TABLE dbo.Product
GO
EXECUTE sp_rename N'dbo.Tmp_Product', N'Product', 'OBJECT' 
GO
ALTER TABLE dbo.Product ADD CONSTRAINT
	PK_Product PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Voucher ADD CONSTRAINT
	FK_Voucher_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Voucher SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Variant ADD CONSTRAINT
	FK_Variant_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Variant SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ProductRelation ADD CONSTRAINT
	FK_ProductRelation_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductRelation ADD CONSTRAINT
	FK_ProductRelation_Product1 FOREIGN KEY
	(
	RelatedProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.ProductRelation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Tag_Product ADD CONSTRAINT
	FK_Tag_Product_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Tag_Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Symptom_Product ADD CONSTRAINT
	FK_Symptom_Product_Product FOREIGN KEY
	(
	ProductID
	) REFERENCES dbo.Product
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Symptom_Product SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
