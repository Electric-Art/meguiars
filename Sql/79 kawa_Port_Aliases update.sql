/****** Object:  StoredProcedure [dbo].[kawa_Port_Aliases]    Script Date: 8/2/2016 1:21:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[kawa_Port_Aliases]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
      productID = [object_id],
      alias = name
	FROM [Dve-test9].[dbo].[cscart_seo_names]
	where type='p'
	order by [object_id] desc
END
