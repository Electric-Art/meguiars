
/****** Object:  Table [dbo].[QandA]    Script Date: 10/27/2015 13:59:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[QandA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[question] [varchar](max) NOT NULL,
	[answer] [varchar](max) NOT NULL,
	[ProductID] [int] NULL,
	[OrderNo] [int] NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_QandA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[QandA]  WITH CHECK ADD  CONSTRAINT [FK_QandA_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ID])
GO

ALTER TABLE [dbo].[QandA] CHECK CONSTRAINT [FK_QandA_Product]
GO


