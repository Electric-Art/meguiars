/*
   Friday, December 04, 201510:29:28 AM
   User: 
   Server: localhost
   Database: Kawa2
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Account ADD
	saveCreditCard bit NOT NULL CONSTRAINT DF_Account_saveCreditCard DEFAULT 1,
	firstName varchar(500) NULL,
	lastName varchar(500) NULL,
	gender int NULL,
	dateOfBirth datetime NULL,
	channel varchar(500) NULL
GO
ALTER TABLE dbo.Account SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
