/*
   Monday, March 21, 201610:31:46 AM
   User: 
   Server: localhost
   Database: Dve
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Status
	DROP CONSTRAINT DF_Status_CreateDate
GO
CREATE TABLE dbo.Tmp_Status
	(
	ID int NOT NULL IDENTITY (1, 1),
	Name varchar(50) NOT NULL,
	SetStatus int NULL,
	EmailSubject varchar(500) NULL,
	EmailHeader varchar(MAX) NULL,
	Color varchar(50) NULL,
	isNotifyCustomer bit NOT NULL,
	isNotifyStaff bit NOT NULL,
	InventoryEffect int NOT NULL,
	isPayOrderAgain bit NOT NULL,
	OrderNo int NOT NULL,
	CreateDate datetime NOT NULL,
	Notes varchar(MAX) NULL,
	StatusSet int NULL,
	RefactorToID int NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_Status SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_Status ADD CONSTRAINT
	DF_Status_CreateDate DEFAULT (getdate()) FOR CreateDate
GO
SET IDENTITY_INSERT dbo.Tmp_Status ON
GO
IF EXISTS(SELECT * FROM dbo.Status)
	 EXEC('INSERT INTO dbo.Tmp_Status (ID, Name, SetStatus, EmailSubject, EmailHeader, Color, isNotifyCustomer, isNotifyStaff, InventoryEffect, isPayOrderAgain, OrderNo, CreateDate, Notes, RefactorToID)
		SELECT ID, Name, SetStatus, EmailSubject, EmailHeader, Color, isNotifyCustomer, isNotifyStaff, InventoryEffect, isPayOrderAgain, OrderNo, CreateDate, Notes, RefactorToID FROM dbo.Status WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Status OFF
GO
ALTER TABLE dbo.[Order]
	DROP CONSTRAINT FK_Order_Status
GO
ALTER TABLE dbo.Status
	DROP CONSTRAINT FK_Status_Status
GO
DROP TABLE dbo.Status
GO
EXECUTE sp_rename N'dbo.Tmp_Status', N'Status', 'OBJECT' 
GO
ALTER TABLE dbo.Status ADD CONSTRAINT
	PK_Status PRIMARY KEY CLUSTERED 
	(
	ID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Status ADD CONSTRAINT
	FK_Status_Status FOREIGN KEY
	(
	RefactorToID
	) REFERENCES dbo.Status
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.[Order] ADD CONSTRAINT
	FK_Order_Status FOREIGN KEY
	(
	StatusID
	) REFERENCES dbo.Status
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.[Order] SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
