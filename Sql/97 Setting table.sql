
/****** Object:  Table [dbo].[Setting]    Script Date: 12/12/2017 2:19:04 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Setting](
	[ID] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[val_int] [int] NULL,
	[val_string] [varchar](500) NULL,
	[val_decimal] [decimal](18, 2) NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


